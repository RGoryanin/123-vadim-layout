<?php
    $_rnd = rand(1e8,1e9);
?><!DOCTYPE HTML>
<html prefix="og: http://ogp.me/ns#">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="/libs/jquery-ui-1.12.1.custom/jquery-ui.min.css"></link>
    <link rel="stylesheet" type="text/css" href="/css/style.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/style.v2.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/style.new.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/icons.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/pickup-window.css?rnd=<?php echo $_rnd; ?>"></link>
    <script type="text/javascript" src="/js/jquery-3.1.1.js"></script>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script type="text/javascript" src="/js/shop-selector.js"></script>
</head>

<body data-is-iml-city="0" data-is-sdek-city="0">
    <header>
        <div class="filters-block">
            <ul id="-filters">
                <li><input type="checkbox" id='-filter-1' checked value='zakazberry'><label for='-filter-1'>
                    <img src='/i/carriers/zakazberry.png'> Zakazberry
                </label></li>
                <li><input type="checkbox" id='-filter-2' value='sdek'><label for='-filter-2'>
                    <img src='/i/carriers/sdek.png'> СДЭК
                </label></li>
                <li><input type="checkbox" id='-filter-3' value='pickpoint'><label for='-filter-3'>
                    <img src='/i/carriers/pickpoint.png'> PickPoint
                </label></li>
                <li><input type="checkbox" id='-filter-4' value='iml'><label for='-filter-4'>
                    <img src='/i/carriers/iml.png'> IML
                </label></li>
                <li><input type="checkbox" id='-filter-5' value='boxberry'><label for='-filter-5'>
                    <img src='/i/carriers/boxberry.png'> Boxberry
                </label></li>
                <li><input type="checkbox" id='-filter-6' value='free' class="-srv"><label for='-filter-6'>
                    <img src='/i/carriers/free-icon.png'> Бесплатно
                </label></li>
                <li><input type="checkbox" id='-filter-7' value='bycard' class="-srv"><label for='-filter-7'>
                    <img src='/i/carriers/card-icon.png'> Оплата картой
                </label></li>
                <li><input type="checkbox" id='-filter-8' value='today' class="-srv"><label for='-filter-8'>
                    <img src='/i/carriers/today-icon.png'> Сегодня-завтра
                </label></li>
            </ul>
            <a href="#" class="filter-switcher blue-button">Фильтр</a>
            <div class="search-shop">
                <input type="text" placeholder="Поиск по 100 пунктам"><a href="#" class="-search blue-button">Найти</a>
            </div>
        </div>
        <div class="map-type">
            <ul>
                <li><input type="radio" name="map-type-radio" checked id="map-type-1" data-block="#map-view"><label for="map-type-1">
                        <b>Карта</b>
                    </label></li>
                <li><input type="radio" name="map-type-radio" id="map-type-2" data-block="#list-view"><label for="map-type-2">
                        <b>Список</b>
                    </label></li>
                <li><input type="radio" name="map-type-radio" id="map-type-3" data-block="#metro-view"><label for="map-type-3">
                        <b>Метро</b>
                    </label></li>
            </ul>
        </div>
    </header>
    
    <main>
        <section id="map-view" class="active" data-center="[55.753994,37.622093]" data-zoom="13">
        </section>
        <section id="list-view">
            <div class="list-header">
                <ul>
                    <li>Название</li>
                    <li>Адрес</li>
                    <li>Метро</li>
                    <li>Стоимость</li>
                    <li>Режим работы</li>
                    <li>&nbsp;</li>
                </ul>
            </div>
            <div class="list-items">
                <ul>
                    <li>
                        <span class="title">123.ru</span>
                        <span class="address">Москва, пр-т Мира, д. 91, к.  1</span>
                        <span class="metro line-3">Алексеевская</span>
                        <span class="price">Бесплатно</span>
                        <span class="open">Круглосуточно<br>
                                           <b>Можно забрать:</b> сегодня<br>
                                           и в течение 5 дней
                        </span>
                        <span class="set"><a href="#" class="blue-button">Заберу здесь</a></span>
                    </li>
                </ul>
            </div>
        </section>
        <section id="metro-view"></section>
    </main>
    
</body>
</html>