var Catalog = function() {
	//this.init();
};

Catalog.prototype = {
	
	init: function() {
		var self = this;
		
		self.$mainEl       = $('.catalog._main');
		self.$mainBtn      = self.$mainEl.find('.catalog__btn');
		self.$mainMain     = self.$mainEl.find('.catalog__main ul');
		self.$mainSub      = self.$mainEl.find('.catalog__sub div[data-id]');
		
		self.$mobileEl     = $('.catalog._mobile');
		self.$mobileBtn    = self.$mobileEl.find('.catalog__btn');
		self.$mobileBack   = self.$mobileEl.find('.catalog__back');
		self.$mobilePages  = self.$mobileEl.find('.catalog__pages');
		
		self.mainInit();
		self.mobileInit();
	},
	
	mainInit: function() {
		var self = this;
		
		self.timerMain     = null;
		self.timerSub      = null;
		
		// открыть
		
		self.$mainEl.on( 'mouseenter', function() {
			if ( self.timerMain ) {
				clearTimeout( self.timerMain );
			}
			self.timerMain = setTimeout( function() {
				self.$mainEl.addClass('_active');
			}, 100 );
		} );
		
		// закрыть
		
		self.$mainEl.on( 'mouseleave', function() {
			if ( self.timerMain ) {
				clearTimeout( self.timerMain );
			}
			self.timerMain = setTimeout( function() {
				self.$mainEl.removeClass('_active');
			}, 100 );
		} );
		
		// выбрать раздел
		
		self.$mainMain.on( 'mouseenter', 'a', function() {
			if ( $( this ).hasClass('_active') ) {
				return;
			}
			var id = $( this ).hasClass('_childs')
				? $( this ).attr('data-id')
				: 0;
			if ( id ) {
				self.timerSub = setTimeout( function() {
					self.mainSelect( id );
				}, 300 );
			}
		} );
		
		self.$mainMain.on( 'mouseleave', 'a', function() {
			if ( self.timerSub ) {
				clearTimeout( self.timerSub );
			}
		} );
		
		// делаем сразу активным первый раздел
		
		self.mainSelect( self.$mainMain.find('a').attr('data-id') );
		
		// при скроле
		
		$(window).on( 'scroll', function() {
			self.mainSubReposition();
		} );
	},
	
	/**
	 * выбрать подкатегорию по id (первого уровня)
	 */
	mainSelect: function( id ) {
		this.$mainMain.find('a').removeClass( '_active' );
		this.$mainMain.find('a[data-id="' + id + '"]').addClass( '_active' );
		this.$mainSub.removeClass( '_active' );
		this.$mainSub.filter('[data-id="' + id + '"]').addClass( '_active' );
		this.mainSubReposition();
	},
	
	/**
	 * смещает подразделы при скроле
	 */
	mainSubReposition: function() {
		
		if ( ! this.$mainEl.is('._active') ) {
			return;
		}
		
		var
			self = this,
			top = 0,
			offset = window.pageYOffset || document.documentElement.scrollTop,
			
			block = self.$mainSub.filter('._active').get(0),
			blockHeight = block.clientHeight,
			
			parent = block.parentNode,
			parentHeight = parent.clientHeight,
			parentTop = parent.getBoundingClientRect().top - document.body.getBoundingClientRect().top,
			parentBottom = parentTop + parentHeight
		;
		
		if ( offset >= parentTop && offset <= parentBottom ) {
			var s = getComputedStyle( parent );
			top = Math.min(
				offset - parentTop,
				parentHeight - parseFloat( s.paddingTop ) - parseFloat( s.paddingBottom ) - blockHeight
			);
		}

		block.style.top = top + 'px';
	},
	
	mobileInit: function() {
		var self = this;
		
		self.history = [];
		
		// открыть
		
		self.$mobileBtn.on( 'click', function() {
			self.mobileSelect();
			self.$mobileEl.addClass('_active');
			$('html, body').addClass('_lock');
			return false;
		} );
		
		// закрыть/назад
		
		self.$mobileBack.on( 'click', function() {
			if ( 0 === self.history.length ) {
				self.$mobileEl.removeClass('_active');
				$('html, body').removeClass('_lock');
			}
			else {
				self.history.pop();
				self.mobileSelect( self.history[ self.history.length - 1 ], 'prev' );
			}
			return false;
		} );
		
		// выбрать
		
		self.$mobilePages.on( 'click', 'a', function() {
			var id = $( this ).hasClass('_childs')
				? $( this ).attr('data-id')
				: 0;
			if ( id ) {
				self.history.push( id );
				self.mobileSelect( id, 'next' );
				return false;
			}
		} );
	},
	
	/**
	 * выбор пункта меню на мобильном (любого уровня)
	 * клонируем по id из десктопного меню,
	 * если id не передан - клонируем главное меню
	 */
	mobileSelect: function( id, anim ) {
		var
			self = this,
			$content = id
				? self.$mainSub.filter('[data-id="' + id + '"]').children().clone()
				: self.$mainMain.clone().addClass('_img')
		;
		
		$content = $content.wrapAll( '<div class="catalog__page"></div>' ).parent();
		
		if ( 'prev' === anim  ) {
			self.$mobilePages.children().not('._current').remove();
			self.$mobilePages.append( $content.addClass('_prev') );
			setTimeout( function() {
				self.$mobilePages.find('._current').removeClass('_current').addClass('_next');
				self.$mobilePages.find('._prev').removeClass('_prev').addClass('_current');
			}, 0 );
		}
		else if ( 'next' === anim ) {
			self.$mobilePages.children().not('._current').remove();
			self.$mobilePages.append( $content.addClass('_next') );
			setTimeout( function() {
				self.$mobilePages.find('._current').removeClass('_current').addClass('_prev');
				self.$mobilePages.find('._next').removeClass('_next').addClass('_current');
			}, 0 );
		}
		else {
			self.$mobilePages.empty().append( $content.addClass('_current') );
		}
	}
};

$( function() {
	new Catalog();
	

} );