<?php

    header ("Content-Type: application/json; charset=utf-8", true);
    header ("Cache-Control: no-cache, no-store, must-revalidate", true);
    header ("Pragma: no-cache", true);
    header ("Expires: 0", true);
    $acc = preg_replace('/(\n|\r)/', '', file_get_contents("acc-side.php"));
    
?>{"success":true,"accessories":"<?php echo addslashes($acc); ?>","services":"<div class=\"pc-accessories\"><div class=\"accessories container responsive-products\"><div class=\"clearfix\"></div></div></div>","accessoriesCount":204,"servicesCount":0}