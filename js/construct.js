$(function($){
    $(document).on('click touchstart','.js-add-cstr-elem',function(e){
        e.preventDefault();

        var btn = $(this);
        var parent = btn.closest('.construct-item');
        $('#construct').window('open',{
           magnet: parent,
           position: 'magnet',
           background: true,
       }, function(win){
           win.width(parent.outerWidth());
       });

       reloadNanoSlider();
    });

    if($('#construct').length > 0){
        $( window ).resize(function(){console.log('lol'); reloadNanoSlider();});
    }

    function reloadNanoSlider(){
        var w       = screen.width,
            big     = $('#construct .js-nano-for-big-screens'),
            small   = $('#construct .js-nano-for-small-screens'),
            wcss    = 768;
            if(w <= wcss && (big.parent('.nano-content') !== 'undefined') ){
               // console.log('1');
                $("#construct .nano").nanoScroller({ destroy: true });
                big.unwrap('.nano-content');
                big.unwrap('.nano');
                small.wrap('<div class="nano"><div class="nano-content"></div></div>');
                $("#construct .nano").nanoScroller({ 'sliderMaxHeight' : 70 });
            }else{
                if(w > wcss && (small.parent('.nano-content') !== 'undefined') ){
                //console.log('2');

                    $("#construct .nano").nanoScroller({ destroy: true });
                    small.unwrap('.nano-content');
                    small.unwrap('.nano');
                    big.wrap('<div class="nano"><div class="nano-content"></div></div>');
                    $("#construct .nano").nanoScroller({ 'sliderMaxHeight' : 70 });
                }
            }
    }



    $('#construct').on('click touchstart','.sttr-pop-menu a', function(e){
       e.preventDefault();
       alert('По клику сортируется список эдементов $("#construct .cstr-pop-list"); Не забудьте перезапустить нано скролл');
    });

    $('#construct').on('click touchstart','.construct-add-btn', function(e){
       e.preventDefault();
       alert('По клику закрывается окно, контент элемента встает на место блока $(".construct-item") в котором была нажата кнопка для вызова модального окна');
    });

    if($('.cstr-items-list').length > 0){
        var container  = $('.cstr-items-list');
        $(document).on('scroll',function(){
            var width      = screen.width;

            if(width <= 768){
                var scroll     = $('body').scrollTop(),
                    contHeight = container.innerHeight(),
                    contOffset = container.offset().top,
                    height     = screen.height,
                    fixHeight = container.find('.cstr-count').outerHeight() + 175;

//                console.log('offset: '+contOffset);
//                console.log('Scroll: '+scroll);
//                console.log('height: '+contHeight);

                if(scroll < (contHeight + fixHeight - height)){

                    container.addClass('fixed-count');
                }else{
                    container.removeClass('fixed-count');
                }

            }

        });

    }

});