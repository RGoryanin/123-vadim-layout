<?php

$authenticated = false;
				
if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
	$user = $_SERVER['PHP_AUTH_USER'];
	$pass = $_SERVER['PHP_AUTH_PW'];
	if ($user == "123" && $pass == "enter") {
		$authenticated = true;
	}
}

if (!$authenticated) {
	header('WWW-Authenticate: Basic realm="Restricted Area"');
	header('HTTP/1.1 401 Unauthorized');
	echo ("Access denied.");
	exit();
}

?><?php

    $_rnd = rand(1e6,1e9);
    
?><!DOCTYPE HTML>
<html prefix="og: http://ogp.me/ns#">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Смартфоны — купить и выбрать из 958 моделей — отзывы, характеристики, лучшие цены в интернет-магазине 123.ru</title>
    <meta content="width=device-width" name="viewport">
    <meta name="keywords" content="Смартфоны, цена, стоимость, купить, заказать, характеристики">
    <meta name="description" content="Смартфоны — 958 моделей от 2 340 р. купить в интернет-магазине www.123.ru. ✔ Официальная гарантия. ✔ Без предоплаты. ✔ Отправляем в день заказа. ➤ Реальные отзывы, полное описание и характеристики. ☎ Номер горячей линии 8 (495) 225-9-123.">
    <meta name='yandex-verification' content='5963dc5637ee1578' />
    <meta name="yandex-verification" content="a73fb8e52f1a0972" />
    <meta name="yandex-verification" content="8b521d40af40902c" />
    <meta name="google-site-verification" content="ifHMSh15xugtEKNlZukNw3XTsUaDsOcYcx6A3zRKiBo" />
    <meta name="referer" content="http://red.123.ru/" />
    <link rel="canonical" href="https://red.123.ru/smartfoni_plansheti_gadjeti/telefoniya/" />
    <link rel="next" href="/smartfoni_plansheti_gadjeti/telefoniya/page-2/" />
    <link rel="stylesheet" type="text/css" href="/libs/jquery-ui-1.12.1.custom/jquery-ui.min.css"></link>
    <link rel="stylesheet" type="text/css" href="/css/style.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/style.shoppilot.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/style.v2.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/style.new.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/icons.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/catalog.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/search_results.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/pages.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/forms.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/lk.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/selections.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/product-card-v2.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/construct.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/main.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/cart.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/sitemap.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/contacts.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/responsive.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/lk.responsive.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/product-card.responsive.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/style.responsive.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/main.responsive.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/pages.responsive.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/search_results.responsive.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/selections.responsive.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/cart.responsive.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/js/jquery.bxslider/jquery.bxslider.css"></link>
    <link rel="stylesheet" type="text/css" href="/libs/datepicker/datepicker123.css"></link>
    <link rel="stylesheet" type="text/css" href="/libs/nanoscroller/nanoscroller.css"></link>
    <link rel="stylesheet" type="text/css" href="/libs/slick/slick.css"></link>
    <link rel="stylesheet" type="text/css" href="/css/catalog-new.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/catalogue-resp.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="shortcut icon" href="/i/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="/i/favicon.png" type="image/png" />
    <meta property="fb:admins" content="100001870821652" />
    <meta property="fb:app_id" content="1509412935940835" />
    <meta property="og:locale" content="ru_RU" />
    <meta property="og:title" content="Смартфоны — купить и выбрать из 958 моделей — отзывы, характеристики, лучшие цены в интернет-магазине 123.ru" />
    <meta property="og:description" content="Смартфоны — 958 моделей от 2 340 р. купить в интернет-магазине www.123.ru. ✔ Официальная гарантия. ✔ Без предоплаты. ✔ Отправляем в день заказа. ➤ Реальные отзывы, полное описание и характеристики. ☎ Номер горячей линии 8 (495) 225-9-123." />
    <meta property="og:image:url" content="https://www.123.ru/categories/telefoniya_130_122.jpg" />
    <meta property="og:url" content="http://red.123.ru/smartfoni_plansheti_gadjeti/telefoniya/" />
    <script type="text/javascript">
        var _gaq = window._gaq || [];
        window.onerror = function(msg, url, line) {
            var preventErrorAlert = true;
            _gaq.push(['_trackEvent', 'JS Error', msg, navigator.userAgent + ' -> ' + url + " : " + line, 0, true]);
            return preventErrorAlert;
        };
    </script>
    <script type="text/javascript">
        dataLayer = [{
            "pageType": "category",
            "clientID": "1461201598.1541171317",
            "userID": 0,
            "auth": 0,
            "city": "Москва",
            "ecommerce": {
                "impressions": [{
                    "id": "80054955",
                    "name": "Смартфон ASUS ZenFone 3 Max ZC553KL 16 Гб серый (90AX00D2-M01780)",
                    "category": "Смартфоны",
                    "price": 10280,
                    "position": 1,
                    "list": "Список товаров"
                }, {
                    "id": "80053717",
                    "name": "Смартфон Alcatel Pixi 4 Plus Power 5023F 16 Гб белый (5023F-2BALRU2)",
                    "category": "Смартфоны",
                    "price": 5200,
                    "position": 2,
                    "list": "Список товаров"
                }, {
                    "id": "80188188",
                    "name": "Смартфон Honor 7C 32 Гб синий (51092MNT)",
                    "category": "Смартфоны",
                    "price": 10600,
                    "position": 3,
                    "list": "Список товаров"
                }, {
                    "id": "80188194",
                    "name": "Смартфон Honor 7X 64 Гб синий (BND-L21 51091YTY)",
                    "category": "Смартфоны",
                    "price": 16150,
                    "position": 4,
                    "list": "Список товаров"
                }, {
                    "id": "80185424",
                    "name": "Смартфон Samsung Galaxy J8 2018 32 Гб черный (SM-J810FZKDSER)",
                    "category": "Смартфоны",
                    "price": 14510,
                    "position": 5,
                    "list": "Список товаров"
                }, {
                    "id": "80053617",
                    "name": "Смартфон Fly FS408 Stratus 8 8 Гб черный",
                    "category": "Смартфоны",
                    "price": 2520,
                    "position": 6,
                    "list": "Список товаров"
                }, {
                    "id": "80071130",
                    "name": "Смартфон Huawei P smart 32 Гб золотистый (51092DPM)",
                    "category": "Смартфоны",
                    "price": 12610,
                    "position": 7,
                    "list": "Список товаров"
                }, {
                    "id": "80056332",
                    "name": "Смартфон Vivo Y65 16 Гб золотистый Y65_Gold_Vivo 1719",
                    "category": "Смартфоны",
                    "price": 9020,
                    "position": 8,
                    "list": "Список товаров"
                }, {
                    "id": "80140966",
                    "name": "Смартфон Doogee X53 16Gb Black Смартфон",
                    "category": "Смартфоны",
                    "price": 4540,
                    "position": 9,
                    "list": "Список товаров"
                }, {
                    "id": "8958654",
                    "name": "Смартфон Samsung SM-G532 Galaxy J2 Prime 8 Гб серебристый (SM-G532FZSDSER)",
                    "category": "Смартфоны",
                    "price": 6860,
                    "position": 10,
                    "list": "Список товаров"
                }, {
                    "id": "8973755",
                    "name": "Смартфон Philips Xenium X818 32 Гб черный (867000139401)",
                    "category": "Смартфоны",
                    "price": 9990,
                    "position": 11,
                    "list": "Список товаров"
                }, {
                    "id": "80004335",
                    "name": "Смартфон ZTE Blade A520 16 Гб синий BLADEA520BLUE",
                    "category": "Смартфоны",
                    "price": 5250,
                    "position": 12,
                    "list": "Список товаров"
                }, {
                    "id": "80106767",
                    "name": "Смартфон Alcatel 3C 5026D 16 Гб металлик черный (5026D-2AALRU1)",
                    "category": "Смартфоны",
                    "price": 5550,
                    "position": 13,
                    "list": "Список товаров"
                }, {
                    "id": "80098279",
                    "name": "Смартфон Huawei P20 Lite 64 Гб синий (51092GYT)",
                    "category": "Смартфоны",
                    "price": 16280,
                    "position": 14,
                    "list": "Список товаров"
                }, {
                    "id": "80140956",
                    "name": "Смартфон Huawei Y6 Prime 2018 16 Гб золотистый 51092KQG",
                    "category": "Смартфоны",
                    "price": 8200,
                    "position": 15,
                    "list": "Список товаров"
                }]
            }
        }];
    </script>
    <script type="text/javascript">
        var dataLayer = window.dataLayer = window.dataLayer || [];
        dataLayer.push({
            'ab_variation': "new"
        });
    </script>
    <script type="text/javascript">
        var dataLayer = window.dataLayer = window.dataLayer || [];
        dataLayer.push({
            expId: "f-KHvCBDR7aR2kQYbEsj_Q",
            expVar: 2
        });
    </script>
    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-KBL5ZV');
    </script>
    <!-- End Google Tag Manager -->
</head>

<body id="body" class="catalog-page bootstrap-extends -new-cat" data-is-dev="1" data-city-name="Москва" data-is-search-engine="0" data-geo-city-id="15238" data-is-iml-city="0" data-is-sdek-city="0">
    <div style="display: none;">
        <!-- Top100 (Kraken) Widget --><span id="top100_widget"></span>
        <!-- END Top100 (Kraken) Widget -->
        <!-- Top100 (Kraken) Counter -->
        <script>
            (function(w, d, c) {
                (w[c] = w[c] || []).push(function() {
                    var options = {
                        project: 2802743,
                        element: 'top100_widget',
                    };
                    try {
                        w.top100Counter = new top100(options);
                    } catch (e) {}
                });
                var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function() {
                        n.parentNode.insertBefore(s, n);
                    };
                s.type = "text/javascript";
                s.async = true;
                s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//st.top100.ru/top100/top100.js";
                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else {
                    f();
                }
            })(window, document, "_top100q");
        </script>
        <noscript> <img src="//counter.rambler.ru/top100.cnt?pid=2802743" alt="Топ-100" /> </noscript>
        <!-- END Top100 (Kraken) Counter -->
    </div>
    <script type="text/javascript">
        var rrPartnerId = "52e0e8141e994426487779d9";
        var rrApi = {};
        var rrApiOnReady = [];
        rrApi.addToBasket = rrApi.order = rrApi.categoryView = rrApi.view =
            rrApi.recomMouseDown = rrApi.recomAddToCart = rrApi.recomTrack = function() {};
        (function(d) {
            var ref = d.getElementsByTagName('script')[0];
            var apiJs, apiJsId = 'rrApi-jssdk';
            if (d.getElementById(apiJsId)) return;
            apiJs = d.createElement('script');
            apiJs.id = apiJsId;
            apiJs.async = true;
            apiJs.src = "//cdn.retailrocket.ru/Content/JavaScript/tracking.js";
            ref.parentNode.insertBefore(apiJs, ref);
        }(document));
    </script>
    <script type="text/javascript">
        var digiScript = document.createElement('script');
        digiScript.src = '//cdn.diginetica.net/123/client.js?ts=' + Date.now();
        digiScript.defer = true;
        digiScript.async = true;
        document.body.appendChild(digiScript);
    </script>
    <header class="second third">
        <div class="top-wrapp top-wrapp-second">
            <div class="container">
                <div class="content-cnt">
                    <section class="top-header">
                        <div class="wrapp-top">
                            <ul class="link-menu">
                                <li class="_js-dismiss">
                                    <div class="select-city"><a href="#" data-window="#cities" data-background="true" data-responsive="true" id="#citysel">Москва<svg  version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="8px" height="7.41px" viewBox="6 8.59 12 7.41" enable-background="new 6 8.59 12 7.41" xml:space="preserve" fill="#ffffff"><path fill="black" d="M16.59,8.59L12,13.17L7.41,8.59L6,10l6,6l6-6L16.59,8.59z"></path><path fill="none" d="M0,0h24v24H0V0z"></path></svg></a></div>
                                </li>
                                <li>
                                    <div><a href="/ncontact/">Контакты</a></div>
                                </li>
                                <li class="with-drop">
                                    <div class="link"><a href="#">Покупателям</a>
                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="8px" height="7.41px" viewBox="6 8.59 12 7.41" enable-background="new 6 8.59 12 7.41" xml:space="preserve" fill="#8891c2">
                                            <path d="M16.59,8.59L12,13.17L7.41,8.59L6,10l6,6l6-6L16.59,8.59z"></path>
                                            <path fill="none" d="M0,0h24v24H0V0z"></path>
                                        </svg>
                                    </div>
                                    <ul class="submenu">
                                        <li><a href="/about/delivery.php/">Доставка</a></li>
                                        <li><a href="/pay/">Оплата</a></li>
                                        <li><a href="/credit/">Покупка в кредит</a></li>
                                        <li><a href="/sovest/">Рассрочка с картой Совесть</a></li>
                                        <li><a href="/halva/">Рассрочка с картой Халва</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <div class=""><a href="/warranty/">Гарантия</a></div>
                                </li>
                                <li class="with-drop">
                                    <div class="link"><a href="#">Оптовым и B2B клиентам</a>
                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="8px" height="7.41px" viewBox="6 8.59 12 7.41" enable-background="new 6 8.59 12 7.41" xml:space="preserve" fill="#8891c2">
                                            <path d="M16.59,8.59L12,13.17L7.41,8.59L6,10l6,6l6-6L16.59,8.59z"></path>
                                            <path fill="none" d="M0,0h24v24H0V0z"></path>
                                        </svg>
                                    </div>
                                    <ul class="submenu">
                                        <li><a href="/b2b/">Опт и B2B: Почему мы?</a></li>
                                        <li><a href="/api/">API - новый импульс для ваших продаж!</a></li>
                                        <li><a href="//st.123.ru/files/123_All_Dealer.zip">Скачать прайс <img src="/i/zip.svg" width="20" height="20" style="position: relative; top: -3px; left: 5px;" /></a></li>
                                    </ul>
                                </li>
                                <li>
                                    <div class=""><a href="/franchise/">Франчайзинг</a></div>
                                </li>
                                <li>
                                    <div class=""><a href="/quick-pay/">Оплатить</a></div>
                                </li>
                            </ul>
                            <ul class="user-menu">
                                <li class="wr__tel"><a href="tel:8 (495) 225-9-123">8 (495) 225-9-123</a></li>
                                <li class="wr__callback"><a href="#" data-window="#callback" data-background="true" data-position="right" data-autofocus="true">Обратный звонок</a></li>
                                <li class="wr__auth"><a href="#" data-window="#login" data-position="right" data-autofocus="true" data-background="true">Войти</a></li>
                                <li class="wr__reg"><a href="#" data-window="#login" data-position="right" data-autofocus="true" data-background="true" data-openspoiler="#spoiler-register" data-closespoiler="#spoiler-login">Регистрация</a></li>
                            </ul>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div class="bottom-wrapp">
            <div class="container">
                <div class="content-cnt">
                    <section class="bottom-header">
                        <div class="wrapp-l-t-m">
                            <div class="l-t">
                                <a class="logo" href="/"><img src="/i/main_logo_white.svg" alt=""></a>
                                <a href="javascript:void(0)" class="catalog__btn"><img src="/i/icon_arrow_down_white.svg" height="15px"></a>
                            </div>
                            <div class="l-t-m"><a href="javascript:void(0)" class="catalog__btn">Каталог</a>
                                <div id="header-catalog" class="category">
                                    <div class="catalog__content">
                                        <div class="catalog__main">
                                            <ul>
                                                <li>
                                                    <a href="/smartfoni_plansheti_gadjeti/" class="_childs" data-id="3">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 66.18 69.94">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="lifestyle">
                                                                    <path class="cls-1" d="M30,8H20a2,2,0,0,0,0,4H30a2,2,0,0,0,0-4Z" />
                                                                    <path class="cls-1" d="M50,22h0c-.43-.07-.85.09-1.28,0C49.14,22.2,49.55,22,50,22Z" />
                                                                    <path class="cls-1" d="M35.93,60a9.1,9.1,0,0,0,0,1.44c0-.48,0-1,0-1.44Z" />
                                                                    <path class="cls-1" d="M6,60a2,2,0,0,1-2-2V6A2,2,0,0,1,6,4H44a2,2,0,0,1,2,2V22h0c.93.17,1.83-.19,2.72,0-.76-.37-1.51,0-2.53,0,1,0,1.77-.37,2.53,0,.42.09.85-.07,1.28,0h0V6a6,6,0,0,0-6-6H6A6,6,0,0,0,0,6V58a6,6,0,0,0,6,6H35.93c0-.85,0-1.7,0-2.56a9.1,9.1,0,0,1,0-1.44Z" />
                                                                    <path class="cls-1" d="M50,22c-.45,0-.86.2-1.28,0-.9-.19-1.79.17-2.72,0" />
                                                                    <path class="cls-1" d="M35.93,60h0c0,.48,0,1,0,1.44,0,.18,0,.35,0,.53Z" />
                                                                    <path class="cls-1" d="M35.93,64V62c0-.18,0-.35,0-.53,0,.85,0,1.71,0,2.56h0c-.18-.68.15-1.35,0-2Z" />
                                                                    <path class="cls-1" d="M35.93,60v2c.15.68-.18,1.35,0,2" />
                                                                    <circle class="cls-1" cx="25" cy="53" r="3" />
                                                                    <path class="cls-1" d="M54.49,26.29h-7.8a1.56,1.56,0,1,0,0,3.12h7.8a1.56,1.56,0,0,0,0-3.12Z" />
                                                                    <path class="cls-1" d="M61.5,20.06H39.68A4.69,4.69,0,0,0,35,24.73V65.27a4.69,4.69,0,0,0,4.68,4.68H61.5a4.69,4.69,0,0,0,4.68-4.68V24.73A4.69,4.69,0,0,0,61.5,20.06Zm1.56,45.21a1.56,1.56,0,0,1-1.56,1.56H39.68a1.56,1.56,0,0,1-1.56-1.56V24.73a1.56,1.56,0,0,1,1.56-1.56H61.5a1.56,1.56,0,0,1,1.56,1.56Z" />
                                                                    <circle class="cls-1" cx="50.59" cy="61.37" r="2.34" />
                                                                </g>
                                                            </g>
                                                        </svg>Смартфоны, планшеты, гаджеты</a>
                                                </li>
                                                <li>
                                                    <a href="/kompyutery_noutbuki_soft/" class="_childs" data-id="146">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 36">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="lifestyle">
                                                                    <circle class="cls-1" cx="10" cy="26" r="2" />
                                                                    <path class="cls-1" d="M62,0H26a2,2,0,0,0-2,2V26a2,2,0,0,0,2,2H42v4H38a2,2,0,0,0,0,4H50a2,2,0,0,0,0-4H46V28H62a2,2,0,0,0,2-2V2A2,2,0,0,0,62,0ZM60,24H28V4H60Z" />
                                                                    <path class="cls-1" d="M18,0H2A2,2,0,0,0,0,2V34a2,2,0,0,0,2,2H18a2,2,0,0,0,2-2V2A2,2,0,0,0,18,0ZM4,10H16v2H4ZM16,4V6H4V4Zm0,28H4V16H16Z" />
                                                                </g>
                                                            </g>
                                                        </svg>Компьютеры, ноутбуки, аксессуары</a>
                                                </li>
                                                <li>
                                                    <a href="/komplektuyuschie_dlya_pk/" class="_childs" data-id="41">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="communication">
                                                                    <path class="cls-1" d="M40,20H24a4,4,0,0,0-4,4V40a4,4,0,0,0,4,4H40a4,4,0,0,0,4-4V24A4,4,0,0,0,40,20Zm0,20H24V24H40Z" />
                                                                    <path class="cls-1" d="M62,26a2,2,0,0,0,0-4H56V18h6a2,2,0,0,0,0-4H56a6,6,0,0,0-6-6V2a2,2,0,1,0-4,0V8H42V2a2,2,0,1,0-4,0V8H34V2a2,2,0,1,0-4,0V8H26V2a2,2,0,1,0-4,0V8H18V2a2,2,0,1,0-4,0V8a6,6,0,0,0-6,6H2a2,2,0,0,0,0,4H8v4H2a2,2,0,0,0,0,4H8v4H2a2,2,0,0,0,0,4H8v4H2a2,2,0,0,0,0,4H8v4H2a2,2,0,0,0,0,4H8a6,6,0,0,0,6,6v6a2,2,0,1,0,4,0V56h4v6a2,2,0,1,0,4,0V56h4v6a2,2,0,1,0,4,0V56h4v6a2,2,0,1,0,4,0V56h4v6a2,2,0,1,0,4,0V56a6,6,0,0,0,6-6h6a2,2,0,0,0,0-4H56V42h6a2,2,0,0,0,0-4H56V34h6a2,2,0,0,0,0-4H56V26ZM52,50a2,2,0,0,1-2,2H14a2,2,0,0,1-2-2V14a2,2,0,0,1,2-2H50a2,2,0,0,1,2,2Z" />
                                                                </g>
                                                            </g>
                                                        </svg>Комплектующие для ПК</a>
                                                </li>
                                                <li>
                                                    <a href="/komputernaya_periferiya/" class="_childs" data-id="341">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 64">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="lifestyle">
                                                                    <path class="cls-1" d="M38,0a2,2,0,0,0-2,2V8a4,4,0,0,1-8,0A8,8,0,0,0,12,8V20.16A14,14,0,0,0,0,34V50a14,14,0,0,0,28,0V34A14,14,0,0,0,16,20.16V8a4,4,0,0,1,8,0A8,8,0,0,0,40,8V2A2,2,0,0,0,38,0ZM24,34V50A10,10,0,0,1,4,50V34a10,10,0,0,1,20,0Z" />
                                                                    <path class="cls-1" d="M12,32v4a2,2,0,0,0,4,0V32a2,2,0,0,0-4,0Z" />
                                                                </g>
                                                            </g>
                                                        </svg>Компьютерная периферия</a>
                                                </li>
                                                <li>
                                                    <a href="/kompyutery_noutbuki_soft/kibersport/" class="_childs" data-id="15553">
                                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 511.999 511.999" style="enable-background:new 0 0 511.999 511.999;" xml:space="preserve">
                                                            <g>
                                                                <g>
                                                                    <path d="M509.273,298.297l-32.658-116.62c-6.306-22.534-21.891-41.531-42.76-52.122c-20.87-10.592-45.406-11.956-67.32-3.739c-16.022,6.013-29.628,16.583-39.338,30.558c0,0-0.046,0.037-0.14,0.037H185.45c-0.085,0-0.13-0.03-0.134-0.03c-9.707-13.981-23.309-24.551-39.341-30.567c-21.915-8.211-46.453-6.85-67.321,3.742c-20.864,10.59-36.448,29.587-42.754,52.119L3.242,298.296c-10.789,38.513,6.053,75.722,40.048,88.478c8.556,3.208,17.548,4.722,26.513,4.722c29.164-0.001,58.001-16.026,70.304-41.855l15.67-32.794c9.656,12.936,25.042,20.962,41.742,20.962c22.775,0,42.735-14.858,49.559-36.094h18.302c6.823,21.236,26.777,36.094,49.548,36.094c16.709,0,32.102-8.035,41.756-20.98l15.708,32.8c7.92,16.626,21.687,29.581,38.764,36.476c17.078,6.896,35.982,7.134,53.232,0.668c17.246-6.47,31.332-19.08,39.663-35.506C512.38,334.845,514.235,316.034,509.273,298.297z M485.259,341.736c-5.94,11.709-15.98,20.698-28.27,25.309c-12.294,4.607-25.77,4.438-37.944-0.477c-12.173-4.916-21.986-14.149-27.641-26.02l-21.904-45.739c-2.412-5.037-7.578-8.087-13.16-7.805c-5.561,0.295-10.355,3.873-12.212,9.115c-4.37,12.334-16.105,20.621-29.199,20.621c-14.772,0-27.553-10.5-30.388-24.968c-1.264-6.447-6.942-11.127-13.501-11.127h-29.617c-6.56,0-12.238,4.68-13.503,11.129c-2.835,14.466-15.621,24.966-30.4,24.966c-13.091,0-24.824-8.284-29.197-20.614c-1.859-5.243-6.655-8.82-12.218-9.113c-5.575-0.299-10.749,2.775-13.157,7.816l-21.856,45.738c-11.385,23.899-44.281,36.271-70.401,26.478c-23.884-8.961-35.05-34.895-27.159-63.067l32.659-116.624c4.79-17.115,16.154-30.969,32.001-39.012c8.977-4.556,18.61-6.851,28.3-6.851c7.418,0,14.871,1.344,22.086,4.049c12,4.504,22.178,12.407,29.432,22.856c3.95,5.687,10.47,9.082,17.44,9.082h141.606c6.969,0,13.487-3.392,17.435-9.075c7.265-10.455,17.448-18.361,29.443-22.863c16.64-6.238,34.534-5.244,50.386,2.8c15.849,8.043,27.215,21.898,32.005,39.014l32.657,116.62C492.521,316.618,491.197,330.028,485.259,341.736z" />
                                                                </g>
                                                            </g>
                                                            <g>
                                                                <g>
                                                                    <circle cx="396.962" cy="187.214" r="13.31" />
                                                                </g>
                                                            </g>
                                                            <g>
                                                                <g>
                                                                    <circle cx="396.962" cy="250.064" r="13.311" />
                                                                </g>
                                                            </g>
                                                            <g>
                                                                <g>
                                                                    <circle cx="365.537" cy="218.639" r="13.311" />
                                                                </g>
                                                            </g>
                                                            <g>
                                                                <g>
                                                                    <circle cx="428.388" cy="218.639" r="13.311" />
                                                                </g>
                                                            </g>
                                                            <g>
                                                                <g>
                                                                    <path d="M147.177,208.102h-18.653v-18.653c0-5.818-4.716-10.535-10.535-10.535c-5.818,0-10.535,4.716-10.535,10.535v18.653H88.801c-5.818,0-10.535,4.716-10.535,10.535c0,5.818,4.716,10.535,10.535,10.535h18.653v18.653c0,5.818,4.716,10.535,10.535,10.535c5.818,0,10.535-4.716,10.535-10.535v-18.653h18.653c5.818,0,10.535-4.716,10.535-10.535C157.711,212.818,152.995,208.102,147.177,208.102z" />
                                                                </g>
                                                            </g>
                                                            <g>
                                                                <g>
                                                                    <path d="M232.033,208.102h-11.524c-5.818,0-10.535,4.716-10.535,10.535c0,5.818,4.716,10.535,10.535,10.535h11.524c5.818,0,10.535-4.716,10.535-10.535C242.567,212.818,237.851,208.102,232.033,208.102z" />
                                                                </g>
                                                            </g>
                                                            <g>
                                                                <g>
                                                                    <path d="M291.993,208.102h-11.524c-5.818,0-10.535,4.716-10.535,10.535c0,5.818,4.716,10.535,10.535,10.535h11.524c5.818,0,10.535-4.716,10.535-10.535C302.528,212.818,297.811,208.102,291.993,208.102z" />
                                                                </g>
                                                            </g>
                                                            <g>
                                                                <g>
                                                                    <path d="M197.52,264.538c-11.696,0-21.211,9.515-21.211,21.211c0,11.697,9.515,21.212,21.211,21.212c11.697,0,21.212-9.515,21.212-21.212C218.731,274.053,209.216,264.538,197.52,264.538z M197.52,291.159c-2.982,0-5.409-2.426-5.409-5.41c0-2.982,2.426-5.409,5.409-5.409c2.983,0,5.41,2.426,5.41,5.409C202.929,288.732,200.503,291.159,197.52,291.159z" />
                                                                </g>
                                                            </g>
                                                            <g>
                                                                <g>
                                                                    <path d="M314.927,264.538c-11.696,0-21.211,9.515-21.211,21.211c0,11.697,9.515,21.212,21.211,21.212c11.696,0,21.211-9.515,21.211-21.212C336.138,274.053,326.623,264.538,314.927,264.538z M314.927,291.159c-2.982,0-5.409-2.426-5.409-5.41c0-2.982,2.426-5.409,5.409-5.409s5.409,2.426,5.409,5.409C320.336,288.732,317.91,291.159,314.927,291.159z" />
                                                                </g>
                                                            </g>
                                                            <g></g>
                                                            <g></g>
                                                            <g></g>
                                                            <g></g>
                                                            <g></g>
                                                            <g></g>
                                                            <g></g>
                                                            <g></g>
                                                            <g></g>
                                                            <g></g>
                                                            <g></g>
                                                            <g></g>
                                                            <g></g>
                                                            <g></g>
                                                            <g></g>
                                                        </svg>Киберспорт</a>
                                                </li>
                                                <li>
                                                    <a href="/communication-equipment/" class="_childs" data-id="353">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 56">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="communication">
                                                                    <circle class="cls-1" cx="10" cy="42" r="2" />
                                                                    <circle class="cls-1" cx="16" cy="42" r="2" />
                                                                    <circle class="cls-1" cx="22" cy="42" r="2" />
                                                                    <path class="cls-1" d="M58,28H52V16a2,2,0,0,0-4,0V28H6a6,6,0,0,0-6,6V46a6,6,0,0,0,6,6h6v2a2,2,0,0,0,4,0V52H48v2a2,2,0,0,0,4,0V52h6a6,6,0,0,0,6-6V34A6,6,0,0,0,58,28ZM4,46V34a2,2,0,0,1,2-2H40V48H6A2,2,0,0,1,4,46Zm56,0a2,2,0,0,1-2,2H44V32H58a2,2,0,0,1,2,2Z" />
                                                                    <path class="cls-1" d="M50,6a8,8,0,0,0-8,8,2,2,0,0,0,4,0,4,4,0,0,1,8,0,2,2,0,0,0,4,0A8,8,0,0,0,50,6Z" />
                                                                    <path class="cls-1" d="M38,16a2,2,0,0,0,2-2,10,10,0,0,1,20,0,2,2,0,0,0,4,0,14,14,0,0,0-28,0A2,2,0,0,0,38,16Z" />
                                                                </g>
                                                            </g>
                                                        </svg>Сетевое оборудование</a>
                                                </li>
                                                <li>
                                                    <a href="/audio-video_i_tv/" class="_childs" data-id="84">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 48">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="media">
                                                                    <path class="cls-1" d="M58,8H41a2,2,0,0,1-2-2,6,6,0,0,0-6-6H19a6,6,0,0,0-6,6,2,2,0,0,1-2,2H6a6,6,0,0,0-6,6V42a6,6,0,0,0,6,6H58a6,6,0,0,0,6-6V14A6,6,0,0,0,58,8Zm2,34a2,2,0,0,1-2,2H6a2,2,0,0,1-2-2V14a2,2,0,0,1,2-2h5a6,6,0,0,0,6-6,2,2,0,0,1,2-2H33a2,2,0,0,1,2,2,6,6,0,0,0,6,6H58a2,2,0,0,1,2,2Z" />
                                                                    <path class="cls-1" d="M27,16A12,12,0,1,0,39,28,12,12,0,0,0,27,16Zm0,20a8,8,0,1,1,8-8A8,8,0,0,1,27,36Z" />
                                                                    <circle class="cls-1" cx="50" cy="20" r="4" />
                                                                </g>
                                                            </g>
                                                        </svg>Телевизоры, фото, аудио/видео техника</a>
                                                </li>
                                                <li>
                                                    <a href="/vse_dlya_doma_i_sada/" class="_childs" data-id="7">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 40">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="eco">
                                                                    <path class="cls-1" d="M63.73,9A2,2,0,0,0,62,8H54a2,2,0,0,0-1.34.51L45.46,15,44,1.78A2,2,0,0,0,42,0H18a2,2,0,0,0-2,1.78L15.54,6H14a14,14,0,0,0-1.56,27.91L12,37.78A2,2,0,0,0,14,40H46a2,2,0,0,0,1.85-1.24L63.74,11A2,2,0,0,0,63.73,9ZM4,20A10,10,0,0,1,14,10h1.1L12.88,29.93A10,10,0,0,1,4,20ZM16.23,36,19.79,4H40.21l3.56,32Zm31.08-4.32L46,19.89,54.77,12h3.79Z" />
                                                                </g>
                                                            </g>
                                                        </svg>Все для дома и сада</a>
                                                </li>
                                                <li>
                                                    <a href="/remont_i_stroitelstvo/" class="_childs" data-id="9">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 64">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="real_estate" data-name="real estate">
                                                                    <path class="cls-1" d="M46,0H12A6,6,0,0,0,6,6V8a6,6,0,0,0-6,6V25a2,2,0,0,0,1.43,1.92L27,34.49V41H26a2,2,0,0,0-2,2V62a2,2,0,0,0,2,2h6a2,2,0,0,0,2-2V43a2,2,0,0,0-2-2H31V33a2,2,0,0,0-1.43-1.91h0L4,23.51V14a2,2,0,0,1,2-2v2a6,6,0,0,0,6,6H46a6,6,0,0,0,6-6V6A6,6,0,0,0,46,0ZM30,60H28V45h2ZM48,14a2,2,0,0,1-2,2H12a2,2,0,0,1-2-2V6a2,2,0,0,1,2-2H46a2,2,0,0,1,2,2Z" />
                                                                </g>
                                                            </g>
                                                        </svg>Ремонт и строительство</a>
                                                </li>
                                                <li>
                                                    <a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/" class="_childs" data-id="10">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 46">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="food_and_drink" data-name="food and drink">
                                                                    <path class="cls-1" d="M56,14H50a2,2,0,0,0,0,4h6a2,2,0,0,0,0-4Z" />
                                                                    <path class="cls-1" d="M56,8H50a2,2,0,0,0,0,4h6a2,2,0,0,0,0-4Z" />
                                                                    <path class="cls-1" d="M58,0H6A6,6,0,0,0,0,6V36a6,6,0,0,0,6,6v2a2,2,0,0,0,4,0V42H54v2a2,2,0,0,0,4,0V42a6,6,0,0,0,6-6V6A6,6,0,0,0,58,0Zm2,36a2,2,0,0,1-2,2H6a2,2,0,0,1-2-2V6A2,2,0,0,1,6,4H58a2,2,0,0,1,2,2Z" />
                                                                    <path class="cls-1" d="M17.4,28.69a1,1,0,0,0,1.41,0l14-14a1,1,0,0,0-1.41-1.41l-14,14A1,1,0,0,0,17.4,28.69Z" />
                                                                    <path class="cls-1" d="M31.4,21.29l-6,6a1,1,0,1,0,1.41,1.41l6-6a1,1,0,0,0-1.41-1.41Z" />
                                                                    <path class="cls-1" d="M18,21a1,1,0,0,0,.71-.29l6-6a1,1,0,0,0-1.41-1.41l-6,6A1,1,0,0,0,18,21Z" />
                                                                    <circle class="cls-1" cx="53" cy="32" r="2" />
                                                                    <circle class="cls-1" cx="53" cy="26" r="2" />
                                                                    <path class="cls-1" d="M44,6H8A2,2,0,0,0,6,8V34a2,2,0,0,0,2,2H44a2,2,0,0,0,2-2V8A2,2,0,0,0,44,6ZM42,32H10V10H42Z" />
                                                                </g>
                                                            </g>
                                                        </svg>Климатическая, крупная и мелкая бытовая техника</a>
                                                </li>
                                                <li>
                                                    <a href="/car-electronics/" class="_childs" data-id="61">
                                                        <svg width="31px" height="22px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31 22">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="transport">
                                                                    <path class="cls-1" d="M24.703125,13.5625 C25.5062188,13.5625 26.15625,12.9119844 26.15625,12.109375 C26.15625,11.3067656 25.5062188,10.65625 24.703125,10.65625 C23.901,10.65625 23.25,11.3067656 23.25,12.109375 C23.25,12.9119844 23.901,13.5625 24.703125,13.5625"></path>
                                                                    <path class="cls-1" d="M6.296875,13.5625 C7.09996875,13.5625 7.75,12.9119844 7.75,12.109375 C7.75,11.3067656 7.09996875,10.65625 6.296875,10.65625 C5.49475,10.65625 4.84375,11.3067656 4.84375,12.109375 C4.84375,12.9119844 5.49475,13.5625 6.296875,13.5625"></path>
                                                                    <path class="cls-1" d="M4.84375,8.71875 L26.15625,8.71875 L26.1581875,8.71875 C27.7600156,8.71971875 29.0625,10.0226875 29.0625,11.625 L29.0625,15.5 L1.9375,15.5 L1.9375,11.625 C1.9375,10.0222031 3.24095312,8.71875 4.84375,8.71875 Z M8.18496875,3.69723437 C8.54679687,2.61223438 9.49375,1.9375 10.65625,1.9375 L13.5625,1.9375 C13.5625,2.47225 13.9965,2.90625 14.53125,2.90625 L16.46875,2.90625 C17.0039844,2.90625 17.4375,2.47225 17.4375,1.9375 L20.34375,1.9375 C21.50625,1.9375 22.4541719,2.61223438 22.816,3.69723437 L23.8443281,6.78125 L7.15664062,6.78125 L8.18496875,3.69723437 Z M23.734375,19.375 L25.671875,19.375 L25.671875,17.4375 L23.734375,17.4375 L23.734375,19.375 Z M5.328125,19.375 L7.265625,19.375 L7.265625,17.4375 L5.328125,17.4375 L5.328125,19.375 Z M27.8951563,7.10965625 L28.3867969,6.78125 L30.03125,6.78125 C30.5664844,6.78125 31,6.34725 31,5.8125 C31,5.27726562 30.5664844,4.84375 30.03125,4.84375 L28.1039219,4.84375 C27.9159844,4.84132812 27.7246562,4.894125 27.5565781,5.0065 L25.7057813,6.24020312 L24.6537188,3.08401562 C24.0293594,1.2109375 22.3379219,0 20.34375,0 L10.65625,0 C8.66304687,0 6.97160937,1.2109375 6.34725,3.08401562 L5.2951875,6.24020312 L3.44826562,5.00989063 L3.44826562,5.00940625 L3.44342187,5.0065 C3.29035937,4.90429688 3.10484375,4.84375 2.90625,4.84375 L0.96875,4.84375 C0.434,4.84375 0,5.27726562 0,5.8125 C0,6.34725 0.434,6.78125 0.96875,6.78125 L2.61320312,6.78125 L3.1058125,7.10965625 C1.2923125,7.8100625 0,9.567375 0,11.625 L0,16.46875 C0,17.0035 0.434,17.4375 0.96875,17.4375 L3.390625,17.4375 L3.390625,20.34375 C3.390625,20.8789844 3.824625,21.3125 4.359375,21.3125 L8.234375,21.3125 C8.76960937,21.3125 9.203125,20.8789844 9.203125,20.34375 L9.203125,17.4375 L21.796875,17.4375 L21.796875,20.34375 C21.796875,20.8789844 22.230875,21.3125 22.765625,21.3125 L26.640625,21.3125 C27.1758594,21.3125 27.609375,20.8789844 27.609375,20.34375 L27.609375,17.4375 L30.03125,17.4375 C30.5664844,17.4375 31,17.0035 31,16.46875 L31,11.625 C31,9.567375 29.7086563,7.8100625 27.8951563,7.10965625 Z"></path>
                                                                    <path class="cls-1" d="M12.109375,13.078125 L18.890625,13.078125 C19.4258594,13.078125 19.859375,12.644125 19.859375,12.109375 C19.859375,11.5741406 19.4258594,11.140625 18.890625,11.140625 L12.109375,11.140625 C11.574625,11.140625 11.140625,11.5741406 11.140625,12.109375 C11.140625,12.644125 11.574625,13.078125 12.109375,13.078125"></path>
                                                                </g>
                                                            </g>
                                                        </svg>Всё для авто</a>
                                                </li>
                                                <li>
                                                    <a href="/kids/" class="_childs" data-id="12969">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 54">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="kids">
                                                                    <path class="cls-1" d="M48.56,22,48,15.91V8a8,8,0,0,0-8-8H34a2,2,0,0,0,0,4h6a4,4,0,0,1,4,4v6c-9.32.29-19.3,2.65-26.54,8-2.46-.63-3.85-3.3-4.3-6.05H15a2,2,0,0,0,0-4H7a2,2,0,0,0,0,4H9.13c.44,3.56,2,7.1,5,8.94a24,24,0,0,0-5.66,9.19,10,10,0,1,0,4.18.24C17.25,22.9,31.38,18.42,44.18,18l.4,4.35a16,16,0,1,0,4-.35ZM16,44a6,6,0,1,1-6-6h0A6,6,0,0,1,16,44Zm32,6a12,12,0,0,1-3.06-23.59l.76,8.33a4,4,0,1,0,4-.36L48.92,26A12,12,0,0,1,48,50Z" />
                                                                </g>
                                                            </g>
                                                        </svg>Товары для детей</a>
                                                </li>
                                                <li>
                                                    <a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/" class="_childs" data-id="11">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 56 64">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="shopping_and_biz" data-name="shopping and biz">
                                                                    <path class="cls-1" d="M34,24.84V22h9a7,7,0,0,0,7-6.25,8,8,0,1,0-4.05,0A3,3,0,0,1,43,18H34V14a2,2,0,0,0-2-2H22a2,2,0,0,0-2,2V24.84C8.5,27.65,0,37.19,0,48.5a22.45,22.45,0,0,0,1.57,8.23A2,2,0,0,0,3.43,58H6v4a2,2,0,0,0,2,2H46a2,2,0,0,0,2-2V58h2.57a2,2,0,0,0,1.86-1.27A22.42,22.42,0,0,0,54,48.5C54,37.19,45.5,27.65,34,24.84ZM44,8a4,4,0,1,1,4,4A4,4,0,0,1,44,8ZM24,24V16h6v8ZM49.16,54H46a2,2,0,0,0-2,2v4H10V56a2,2,0,0,0-2-2H4.84A18.34,18.34,0,0,1,4,48.5C4,37.2,14.32,28,27,28s23,9.2,23,20.5A18.35,18.35,0,0,1,49.16,54Z" />
                                                                </g>
                                                            </g>
                                                        </svg>Красота и здоровье, косметика, бытовая химия</a>
                                                </li>
                                                <li>
                                                    <a href="/office-seti/" class="_childs" data-id="12769">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 56 64">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="design_tools" data-name="design tools">
                                                                    <path class="cls-1" d="M50,40H38V36a2,2,0,0,0-2-2H34V22.39a12,12,0,1,0-12,0V34H20a2,2,0,0,0-2,2v4H6a6,6,0,0,0-6,6v6a6,6,0,0,0,6,6,6,6,0,0,0,6,6H44a6,6,0,0,0,6-6,6,6,0,0,0,6-6V46A6,6,0,0,0,50,40ZM24.8,19.33a8,8,0,1,1,6.4,0A2,2,0,0,0,30,21.16V34H26V21.16A2,2,0,0,0,24.8,19.33ZM22,38H34v2H22ZM44,60H12a2,2,0,0,1-2-2H46A2,2,0,0,1,44,60Zm8-8a2,2,0,0,1-2,2H6a2,2,0,0,1-2-2V46a2,2,0,0,1,2-2H50a2,2,0,0,1,2,2Z" />
                                                                </g>
                                                            </g>
                                                        </svg>Офис, канцелярия</a>
                                                </li>
                                                <li>
                                                    <a href="/chay_kofe/" class="_childs" data-id="701">
                                                        <svg width="26px" height="31px" viewBox="0 0 26 31" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                            <defs>
                                                                <polygon id="path-1" points="7.75906196e-06 0 25.9595556 0 25.9595556 30.8148148 7.75906196e-06 30.8148148"></polygon>
                                                            </defs>
                                                            <g id="Artboard" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <g id="Group-3">
                                                                    <mask id="mask-2" fill="white">
                                                                        <use xlink:href="#path-1"></use>
                                                                    </mask>
                                                                    <g id="Clip-2"></g>
                                                                    <path class="cls-1" d="M22.2976481,13.2402593 C20.7626852,15.899963 18.4828704,17.8542963 16.2709444,18.5211481 C16.1982407,18.3434815 16.1159074,18.1672593 16.0355,17.9900741 C16.4510185,17.5779259 16.8463148,17.0983704 17.1828704,16.5148148 C18.3273519,14.5325556 18.2917222,12.3995926 18.2565741,10.3345185 C18.2536852,10.1173704 18.2474259,9.90262963 18.2416481,9.68981481 C18.1987963,8.10092593 18.1617222,6.72918519 19.0736481,5.14896296 C19.9523519,3.62651852 21.3982407,2.93125926 22.1912407,2.65007407 C24.5856481,4.41759259 24.6723148,9.126 22.2976481,13.2402593 M14.8688704,26.4713704 C14.7687222,25.5883333 14.5067963,24.6128519 13.9468333,23.6426667 C12.8028333,21.6604074 10.9375741,20.6247407 9.13298148,19.6227778 C8.94327778,19.5168519 8.75405556,19.4142963 8.5657963,19.3127037 C7.1695,18.5558148 5.96290741,17.901963 5.05194444,16.3227037 C4.16987037,14.7954444 4.29457407,13.1901852 4.44864815,12.3649259 C4.93398148,12.1516296 5.4665,12.0394444 6.04187037,12.0394444 C8.69290741,12.0394444 11.757537,14.2638889 13.668537,17.5745556 C15.4837222,20.7186296 15.8587963,24.2088889 14.8688704,26.4713704 M3.66190741,23.3537778 C1.8472037,20.2101852 1.47261111,16.7199259 2.46157407,14.456963 C2.56172222,15.3404815 2.82412963,16.315963 3.38361111,17.2856667 C4.56709259,19.3358148 6.13335185,20.1851481 7.64809259,21.0060741 C7.83057407,21.1047778 8.01353704,21.203963 8.19890741,21.307 C9.84605556,22.2213333 11.4012407,23.0851111 12.2785,24.6056296 C13.1605741,26.1324074 13.0358704,27.7376667 12.8817963,28.5634074 C12.3969444,28.7767037 11.8639444,28.8888889 11.2885741,28.8888889 C8.63753704,28.8888889 5.57290741,26.6644444 3.66190741,23.3537778 M12.2910185,7.46103704 C14.0840556,4.35403704 16.8935,2.20422222 19.4232037,1.95144444 C18.6985741,2.48155556 17.9729815,3.20233333 17.4053148,4.186 C16.2218333,6.23711111 16.2695,8.01859259 16.3166852,9.74181481 C16.322463,9.94837037 16.3282407,10.1563704 16.3316111,10.3691852 C16.3629074,12.2527407 16.3927593,14.0318148 15.5150185,15.5518519 C15.3811667,15.7834444 15.2323889,15.9914444 15.0763889,16.185963 C13.8876111,14.2648519 12.3747963,12.7048519 10.7382407,11.647037 C10.9871667,10.2728889 11.497537,8.83518519 12.2910185,7.46103704 M22.8412407,0.765074074 C22.8345,0.760740741 22.8277593,0.756407407 22.820537,0.752555556 C21.9519444,0.253740741 20.9755,0 19.9176852,0 C16.5497222,0 12.9010556,2.55088889 10.6226852,6.49807407 C9.82342593,7.88281481 9.27694444,9.31666667 8.96109259,10.7187407 C7.99090741,10.3282593 7.00531481,10.1135185 6.04187037,10.1135185 C4.98790741,10.1135185 4.01435185,10.3662963 3.14816667,10.8622222 C3.1342037,10.8694444 3.1212037,10.8776296 3.1082037,10.8853333 C-0.550574074,13.0207037 -1.05275926,19.0372963 1.99357407,24.3167407 C4.27194444,28.2639259 7.92061111,30.8148148 11.2885741,30.8148148 C12.3512037,30.8148148 13.3310185,30.5596296 14.2025,30.056963 C14.2337963,30.0386667 14.2612407,30.0145926 14.2920556,29.9958148 C14.2968704,29.9924444 14.3021667,29.9895556 14.3060185,29.9861852 C17.0480556,28.3164074 17.9614259,24.4313333 16.8819444,20.3493333 C19.5546481,19.526 22.1811296,17.2957778 23.9659815,14.2032222 C27.0152037,8.91896296 26.5096481,2.89514815 22.8412407,0.765074074" fill="#1A1919" mask="url(#mask-2)"></path>
                                                                </g>
                                                            </g>
                                                        </svg>Чай/Кофе</a>
                                                </li>
                                                <li>
                                                    <a href="/uslugi/" class="_childs" data-id="12341">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 54">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="shopping_and_biz" data-name="shopping and biz">
                                                                    <path class="cls-1" d="M55.06,8.77A2.68,2.68,0,0,0,53.18,8a4.25,4.25,0,0,0-3.31,2.12c-1.23,1.72-2.15,4.4-1.43,6.48a41.15,41.15,0,0,0,1.64,3.92c-1.21.05-2.84.72-4.07,3.51-.41.92-1,2.15-1.6,3.5-3.3,7.15-4.4,9.75-4.4,11,0,3.38,2.3,12.84,2.56,13.9A2,2,0,0,0,44.5,54h10a2,2,0,0,0,2-2V44C58.73,40.19,64,29.59,64,26.46S56.92,10.57,55.06,8.77ZM52.8,42.37a2,2,0,0,0-.3,1.05V50H46.08c-.79-3.38-2-9.2-2.08-11.35.22-1.19,2.7-6.54,4-9.44.63-1.37,1.21-2.62,1.63-3.56a3.87,3.87,0,0,1,.64-1.07,4.82,4.82,0,0,1,1.11,1.2l-2.89,6.3a2,2,0,1,0,3.63,1.67l3.33-7.27a2,2,0,0,0,.1-.29c.68-1.83-.33-4-1.61-6.79a42.64,42.64,0,0,1-1.75-4.1A4.19,4.19,0,0,1,53,12.6c2.16,3.16,6.84,12.1,7,13.86C60,28.2,55.58,37.87,52.8,42.37Z" />
                                                                    <path class="cls-1" d="M18,24c-1.23-2.78-2.87-3.46-4.07-3.51a41.41,41.41,0,0,0,1.64-3.92c.72-2.09-.2-4.76-1.43-6.48A4.25,4.25,0,0,0,10.82,8a2.68,2.68,0,0,0-1.88.77C7.08,10.57,0,23.19,0,26.46S5.27,40.19,7.5,44v8a2,2,0,0,0,2,2h10a2,2,0,0,0,1.94-1.52C21.7,51.41,24,41.95,24,38.57c0-1.29-1.1-3.89-4.4-11C19,26.19,18.41,25,18,24Zm-.07,26H11.5V43.42a2,2,0,0,0-.3-1.05C8.42,37.87,4,28.2,4,26.49c.14-1.79,4.83-10.73,7-13.89a4.19,4.19,0,0,1,.79,2.71A42.66,42.66,0,0,1,10,19.41c-1.29,2.75-2.29,5-1.61,6.79a2,2,0,0,0,.1.29l3.33,7.27a2,2,0,1,0,3.63-1.67l-2.89-6.3a5.08,5.08,0,0,1,1.09-1.22,3.71,3.71,0,0,1,.65,1.09c.42.94,1,2.19,1.63,3.56,1.34,2.89,3.81,8.24,4,9.44C20,40.8,18.71,46.62,17.92,50Z" />
                                                                    <path class="cls-1" d="M45,22V8a2,2,0,0,0-1-1.75l-11-6a2,2,0,0,0-1.92,0l-11,6a2,2,0,0,0-1,1.75H19V22a2,2,0,0,0,1,1.75l11,6a2,2,0,0,0,1.92,0l11-6A2,2,0,0,0,45,22ZM32,4.28,38.84,8,32,11.72,25.25,8ZM32,25.72l-9-4.91V11.33l8.08,4.43a2,2,0,0,0,1.92,0l8-4.38v9.44Z" />
                                                                </g>
                                                            </g>
                                                        </svg>Услуги</a>
                                                </li>
                                                <li>
                                                    <a href="/123ru-ucenca/" class="_childs" data-id="15394">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="shopping_and_biz" data-name="shopping and biz">
                                                                    <path class="cls-1" d="M35.61,34.6a7,7,0,1,0,9.9,0A7,7,0,0,0,35.61,34.6Zm7.07,7.07a3,3,0,1,1,0-4.24A3,3,0,0,1,42.68,41.68Z" />
                                                                    <path class="cls-1" d="M29.95,28.95a7,7,0,1,0-9.9,0A7,7,0,0,0,29.95,28.95Zm-7.07-7.07a3,3,0,1,1,0,4.24A3,3,0,0,1,22.88,21.88Z" />
                                                                    <path class="cls-1" d="M43.69,20.86a2,2,0,0,0-2.83,0l-19,19a2,2,0,1,0,2.83,2.83l19-19A2,2,0,0,0,43.69,20.86Z" />
                                                                    <path class="cls-1" d="M60,32l3.64-5.58a2,2,0,0,0-.64-2.8l-5.79-3.5.8-6.57a2,2,0,0,0-1.78-2.23l-6.81-.71L47.24,4.31a2,2,0,0,0-2.54-1.22L38.22,5.33,33.4.58a2,2,0,0,0-2.81,0L25.77,5.33,19.3,3.08a2,2,0,0,0-2.54,1.22L14.55,10.6l-6.81.71A2,2,0,0,0,6,13.54l.8,6.57L1,23.61a2,2,0,0,0-.64,2.8L4,32,.32,37.58A2,2,0,0,0,1,40.39l5.79,3.5L6,50.46a2,2,0,0,0,1.78,2.23l6.81.71,2.21,6.29a2,2,0,0,0,2.54,1.23l6.47-2.25,4.82,4.75a2,2,0,0,0,2.81,0l4.82-4.75,6.47,2.25a2,2,0,0,0,2.54-1.23l2.21-6.29,6.81-.71A2,2,0,0,0,58,50.46l-.8-6.57L63,40.39a2,2,0,0,0,.64-2.8ZM56,33.09,59.19,38l-5.12,3.1a2,2,0,0,0-1,2l.71,5.84-6,.63a2,2,0,0,0-1.68,1.33l-2,5.6-5.76-2a2,2,0,0,0-2.06.46L32,59.19l-4.3-4.24a2,2,0,0,0-2.06-.46l-5.76,2-2-5.6a2,2,0,0,0-1.68-1.33l-6-.63.71-5.84a2,2,0,0,0-1-2L4.81,38,8,33.09a2,2,0,0,0,0-2.19L4.81,26l5.12-3.1a2,2,0,0,0,1-2l-.71-5.84,6-.63a2,2,0,0,0,1.68-1.32l2-5.61,5.76,2a2,2,0,0,0,2.06-.47L32,4.81l4.3,4.24a2,2,0,0,0,2.06.47l5.76-2,2,5.61a2,2,0,0,0,1.68,1.32l6,.63-.71,5.84a2,2,0,0,0,1,2L59.19,26,56,30.91A2,2,0,0,0,56,33.09Z" />
                                                                </g>
                                                            </g>
                                                        </svg>123.RU:Уценка</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="catalog__sub"><i class="close-menu material-icons">close</i>
                                            <div data-id="3" data-items-per-column="10">
                                                <ul>
                                                    <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/" data-id="122">Смартфоны&nbsp;<span>534</span></a></li>
                                                    <li><a href="/smartfoni_plansheti_gadjeti/sotovie-telefoni/" data-id="12766">Телефоны&nbsp;<span>169</span></a></li>
                                                    <li><a href="/smartfoni_plansheti_gadjeti/apple/" data-id="681">Продукция Apple&nbsp;<span>1233</span></a>
                                                        <div data-id="681">
                                                            <ul>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/apple/macbook/" data-id="682" data-products-count="54">Ноутбуки Apple</a></li>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/apple/imac/" data-id="683" data-products-count="13">iMac</a></li>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/apple/mac-mini/" data-id="3343" data-products-count="3">Mac Mini</a></li>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/apple/mac-pro/" data-id="3344" data-products-count="1">Mac Pro</a></li>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/apple/ipad/" data-id="684" data-products-count="52">iPad</a></li>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/apple/telefony_apple/" data-id="3350" data-products-count="43">iPhone</a></li>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/apple/ipod/" data-id="686" data-products-count="10">iPod</a></li>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/apple/apple-tv/" data-id="12646" data-products-count="3">Apple TV</a></li>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/apple/ustroystva_vvoda_apple/" data-id="3682" data-products-count="9">Устройства ввода Apple</a></li>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/apple/aksessuf/" data-id="3358" data-products-count="352">Аксессуары Apple</a></li>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/apple/sumki_i_chely_dlya_apple/" data-id="3621" data-products-count="690">Сумки,чехлы и защитные пленки для Apple</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/smartfoni_plansheti_gadjeti/clipboards/" data-id="149">Планшеты&nbsp;<span>207</span></a></li>
                                                    <li><a href="/smartfoni_plansheti_gadjeti/gadjeti/" data-id="12767">Гаджеты&nbsp;<span>30</span></a>
                                                        <div data-id="12767">
                                                            <ul>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/gadjeti/electronnaya-sigareta/" data-id="15233" data-products-count="15">Электронные сигареты</a></li>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/gadjeti/monokolesa/" data-id="12478" data-products-count="11">Электросамокаты и моноколеса</a></li>
                                                                <li><a href="/smartfoni_plansheti_gadjeti/gadjeti/">Все гаджеты&nbsp;<span>30</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/smartfoni_plansheti_gadjeti/smart_chasi/" data-id="5">Smart-часы&nbsp;<span>92</span></a></li>
                                                    <li><a href="/smartfoni_plansheti_gadjeti/ebooks/" data-id="132">Электронные книги&nbsp;<span>5</span></a></li>
                                                    <li><a href="/smartfoni_plansheti_gadjeti/dect_telefoniya/" data-id="125">DECT-телефоны&nbsp;<span>111</span></a></li>
                                                    <li><a href="/smartfoni_plansheti_gadjeti/ofisnie_telefoni/" data-id="4031">Проводные телефоны&nbsp;<span>73</span></a></li>
                                                    <li><a href="/smartfoni_plansheti_gadjeti/aksesuari_dlya_smartfonov_planshetov/" data-id="4">Аксесуары для смартфонов/планшетов&nbsp;<span>3503</span></a>
                                                        <div data-id="4">
                                                            <ul>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/aksesuari_dlya_smartfonov_planshetov/aksessuary_dlya_telefonov/" data-id="126" data-products-count="2644">Аксессуары для телефонов</a></li>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/aksesuari_dlya_smartfonov_planshetov/aksessuary_dlya_planshetov/" data-id="321" data-products-count="858">Аксессуары для планшетов</a></li>
                                                                <li><a href="/smartfoni_plansheti_gadjeti/aksesuari_dlya_smartfonov_planshetov/">Все аксесуары для смартфонов/планшетов&nbsp;<span>3503</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/smartfoni_plansheti_gadjeti/3gmodemy/" data-id="4033">Модемы 3G/4G&nbsp;<span>20</span></a></li>
                                                    <li><a href="/smartfoni_plansheti_gadjeti/radiostancii/" data-id="11816">Радиостанции&nbsp;<span>6</span></a></li>
                                                </ul>
                                            </div>
                                            <div data-id="146" data-items-per-column="8">
                                                <ul>
                                                    <li><a href="/kompyutery_noutbuki_soft/notebooks/" data-id="147">Ноутбуки&nbsp;<span>1946</span></a></li>
                                                    <li><a href="/kompyutery_noutbuki_soft/ultrabuki/" data-id="861">Ультрабуки&nbsp;<span>49</span></a></li>
                                                    <li><a href="/kompyutery_noutbuki_soft/desktopy/" data-id="13408">Системные блоки&nbsp;<span>331</span></a></li>
                                                    <li><a href="/kompyutery_noutbuki_soft/monitory/" data-id="351">Мониторы&nbsp;<span>833</span></a></li>
                                                    <li><a href="/kompyutery_noutbuki_soft/monoblocks/" data-id="150">Моноблоки&nbsp;<span>502</span></a></li>
                                                    <li><a href="/kompyutery_noutbuki_soft/nettopy/" data-id="151">Неттопы&nbsp;<span>205</span></a></li>
                                                    <li><a href="/kompyutery_noutbuki_soft/maining-ferma/" data-id="15774">Фермы&nbsp;<span>19</span></a></li>
                                                    <li><a href="/kompyutery_noutbuki_soft/aksessuary_notebook/" data-id="153">Аксессуары для ноутбуков, ультрабуков&nbsp;<span>3378</span></a>
                                                        <div data-id="153">
                                                            <ul>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/aksessuary_notebook/usb-fleshki/" data-id="11322" data-products-count="581">USB флешки</a></li>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/aksessuary_notebook/setevye_adaptery_pitaniya_i_zaryadnye_us/" data-id="160" data-products-count="234">Зарядные устройства для ноутбуков</a></li>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/aksessuary_notebook/dok_stantsiya/" data-id="161" data-products-count="30">Док-станция</a></li>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/aksessuary_notebook/cooling-stand-for-notebooks/" data-id="327" data-products-count="72">Подставки для ноутбуков</a></li>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/aksessuary_notebook/sumki_i_chehly_dlya_noutbukov/" data-id="11862" data-products-count="680">Сумки и чехлы для ноутбуков</a></li>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/aksessuary_notebook/trosy_bezopasnosti/" data-id="163" data-products-count="26">Тросы безопасности</a></li>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/aksessuary_notebook/interfeysnye_kabeli/" data-id="156" data-products-count="539">Интерфейсные кабели</a></li>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/aksessuary_notebook/chistyaschie_sredstva/" data-id="157" data-products-count="152">Чистящие средства</a></li>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/aksessuary_notebook/setevye_filtry/" data-id="688" data-products-count="752">Сетевые фильтры</a></li>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/aksessuary_notebook/kontroler_dly_noutbuka/" data-id="3688" data-products-count="11">Контроллеры для ноутбуков</a></li>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/aksessuary_notebook/akkukmulyatory_dlya_noutbukov/" data-id="329" data-products-count="281">Аккумуляторы для ноутбуков</a></li>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/aksessuary_notebook/3g_modemi_lte/" data-id="11807" data-products-count="20">3G модемы и LTE</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/kompyutery_noutbuki_soft/software/" data-id="155">Программное обеспечение&nbsp;<span>44</span></a>
                                                        <div data-id="155">
                                                            <ul>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/software/antivirusnye_programmy/" data-id="331" data-products-count="36">Антивирусные программы</a></li>
                                                                <li><a href="/kompyutery_noutbuki_soft/software/">Все программное обеспечение&nbsp;<span>44</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div data-id="41" data-items-per-column="16">
                                                <ul>
                                                    <li><a href="/komplektuyuschie_dlya_pk/videokarti/" data-id="12482">Видеокарты&nbsp;<span>411</span></a></li>
                                                    <li><a href="/komplektuyuschie_dlya_pk/protsessory/" data-id="421">Процессоры&nbsp;<span>176</span></a></li>
                                                    <li><a href="/komplektuyuschie_dlya_pk/operativnaya-pamyat/" data-id="422">Оперативная память&nbsp;<span>853</span></a>
                                                        <div data-id="422">
                                                            <ul>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/operativnaya-pamyat/dimm_ddr_dlya_pc/" data-id="541" data-products-count="675">Оперативная память для компьютера</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/operativnaya-pamyat/so-dimm_dlya_noutbuka/" data-id="542" data-products-count="178">Оперативная память для ноутбука</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/komplektuyuschie_dlya_pk/materinskie_platy/" data-id="42">Материнские платы&nbsp;<span>351</span></a></li>
                                                    <li><a href="/komplektuyuschie_dlya_pk/zhestkie_diski/" data-id="481">Жесткие диски&nbsp;<span>1231</span></a>
                                                        <div data-id="481">
                                                            <ul>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/zhestkie_diski/hdd_25/" data-id="3588" data-products-count="42">Жесткие диски для ноутбуков</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/zhestkie_diski/hdd_35/" data-id="3587" data-products-count="149">Жесткие диски для компьютера</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/zhestkie_diski/zhestkie_diski_hdd/" data-id="483" data-products-count="409">Серверные жесткие диски</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/zhestkie_diski/ssd-diski/" data-id="12547" data-products-count="393">SSD диски</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/zhestkie_diski/aksessuary_dlya_hdd/" data-id="485" data-products-count="238">Аксессуары для жестких дисков</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/komplektuyuschie_dlya_pk/bloki-pitaniya/" data-id="12507">Блоки питания&nbsp;<span>485</span></a></li>
                                                    <li><a href="/komplektuyuschie_dlya_pk/korpusa/" data-id="543">Корпуса для компьютеров&nbsp;<span>681</span></a></li>
                                                    <li><a href="/komplektuyuschie_dlya_pk/kontrollery/" data-id="621">Контроллеры&nbsp;<span>107</span></a></li>
                                                    <li><a href="/komplektuyuschie_dlya_pk/kulery_dlya_kompyutera/" data-id="545">Системы охлаждения&nbsp;<span>618</span></a>
                                                        <div data-id="545">
                                                            <ul>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/kulery_dlya_kompyutera/sistemy-ohlazhdeniya/" data-id="626" data-products-count="268">Охлаждение для корпусов</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/kulery_dlya_kompyutera/ohlajdenie_dlya_processora/" data-id="12483" data-products-count="300">Охлаждение для процессоров</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/kulery_dlya_kompyutera/termopasty-termoklei/" data-id="14478" data-products-count="34">Термопасты, термоклеи</a></li>
                                                                <li><a href="/komplektuyuschie_dlya_pk/kulery_dlya_kompyutera/">Все системы охлаждения&nbsp;<span>618</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/komplektuyuschie_dlya_pk/dvd-rw-bd-rw/" data-id="661">Оптические накопители&nbsp;<span>158</span></a>
                                                        <div data-id="661">
                                                            <ul>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/dvd-rw-bd-rw/nacopiteli_dlya_pc/" data-id="662" data-products-count="17">Оптические накопители для ПК</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/dvd-rw-bd-rw/optical-storage/" data-id="664" data-products-count="35">Внешние оптические накопители</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/dvd-rw-bd-rw/kartridery/" data-id="665" data-products-count="101">Картридеры</a></li>
                                                                <li><a href="/komplektuyuschie_dlya_pk/dvd-rw-bd-rw/">Все оптические накопители&nbsp;<span>158</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/komplektuyuschie_dlya_pk/networkcards/" data-id="546">Сетевые карты&nbsp;<span>59</span></a></li>
                                                    <li><a href="/komplektuyuschie_dlya_pk/zvukovye_karty/" data-id="547">Звуковые карты&nbsp;<span>28</span></a></li>
                                                    <li><a href="/komplektuyuschie_dlya_pk/tv-tuneri/" data-id="1601">TV-тюнеры&nbsp;<span>1</span></a>
                                                        <div data-id="1601">
                                                            <ul>
                                                                <li><a href="/komplektuyuschie_dlya_pk/tv-tuneri/">Все tv-тюнеры&nbsp;<span>1</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/komplektuyuschie_dlya_pk/servernoe-oborudovanie/" data-id="549">Серверное оборудование&nbsp;<span>2051</span></a>
                                                        <div data-id="549">
                                                            <ul>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/servernoe-oborudovanie/servernye_korpusa/" data-id="551" data-products-count="156">Серверные корпуса</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/servernoe-oborudovanie/materinskie_platy/" data-id="552" data-products-count="94">Серверные материнские платы</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/servernoe-oborudovanie/server-processors/" data-id="553" data-products-count="158">Серверные процессоры</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/servernoe-oborudovanie/servers-memory/" data-id="554" data-products-count="134">Серверная оперативная память</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/servernoe-oborudovanie/servernie_kontrollery/" data-id="557" data-products-count="112">Серверные контроллеры</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/servernoe-oborudovanie/accessories-for-servers/" data-id="558" data-products-count="658">Аксессуары для серверов</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/servernoe-oborudovanie/servers/" data-id="1806" data-products-count="352">Серверы</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/servernoe-oborudovanie/severnie-shkafi/" data-id="12586" data-products-count="259">Серверные шкафы</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/servernoe-oborudovanie/servernie-bloki-pitaniya/" data-id="13288" data-products-count="80">Блоки питания</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/servernoe-oborudovanie/diskovie-polky/" data-id="15107" data-products-count="48">Дисковые полки</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/komplektuyuschie_dlya_pk/kabeli-i-shleifi/" data-id="12529">Кабели и переходники&nbsp;<span>809</span></a>
                                                        <div data-id="12529">
                                                            <ul>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/kabeli-i-shleifi/kabeli-i-perehodniki-usb/" data-id="12588" data-products-count="654">Кабели и переходники USB</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/kabeli-i-shleifi/kabeli-pitaniya/" data-id="12601" data-products-count="85">Кабели питания</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/kabeli-i-shleifi/shleifi-i-perehodniki/" data-id="12604" data-products-count="70">Шлейфы и переходники</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div data-id="341" data-items-per-column="17">
                                                <ul>
                                                    <li><a href="/komputernaya_periferiya/input-devices-mouse-steering-wheels-dzhostik/" data-id="344">Устройства ввода&nbsp;<span>1227</span></a>
                                                        <div data-id="344">
                                                            <ul>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/input-devices-mouse-steering-wheels-dzhostik/mouse/" data-id="349" data-products-count="789">Мыши</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/input-devices-mouse-steering-wheels-dzhostik/keyboards/" data-id="348" data-products-count="274">Клавиатуры</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/input-devices-mouse-steering-wheels-dzhostik/komplekt_klaviatura_mysh/" data-id="350" data-products-count="86">Комплект клавиатура + мышь</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/input-devices-mouse-steering-wheels-dzhostik/graphic-tablets1/" data-id="354" data-products-count="48">Графические планшеты</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/input-devices-mouse-steering-wheels-dzhostik/pulty_du/" data-id="4028" data-products-count="25">Пульты ДУ</a></li>
                                                                <li><a href="/komputernaya_periferiya/input-devices-mouse-steering-wheels-dzhostik/">Все устройства ввода&nbsp;<span>1227</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/komputernaya_periferiya/storage-media/" data-id="355">Носители информации&nbsp;<span>1358</span></a>
                                                        <div data-id="355">
                                                            <ul>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/storage-media/sistemi_hraneniya_dannih_shd/" data-id="54" data-products-count="51">Системы хранения данных (СХД)</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/storage-media/external-hard-drives/" data-id="381" data-products-count="248">Внешние жесткие диски</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/storage-media/flash-drives1/" data-id="382" data-products-count="581">USB Флешки</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/storage-media/diski_i_diskety/" data-id="383" data-products-count="62">Диски и дискеты</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/storage-media/memory-cards/" data-id="384" data-products-count="260">Карты памяти</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/storage-media/portmone_i_konverti/" data-id="11499" data-products-count="55">Конверты и портмоне для дисков</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/storage-media/cardreaders/" data-id="13430" data-products-count="101">Картридеры</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/komputernaya_periferiya/zaschita_pitaniya/" data-id="342">Защита питания&nbsp;<span>1836</span></a>
                                                        <div data-id="342">
                                                            <ul>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/zaschita_pitaniya/istochniki_bespereboynogo_pitaniya/" data-id="343" data-products-count="487">Источники бесперебойного питания (ИБП)</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/zaschita_pitaniya/setevye_filtry/" data-id="379" data-products-count="752">Сетевые фильтры и удлинители</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/zaschita_pitaniya/stabilizatory_napryazheniya/" data-id="105" data-products-count="266">Стабилизаторы напряжения</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/zaschita_pitaniya/akkumulyatory/" data-id="102" data-products-count="213">Батареи для ИБП</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/zaschita_pitaniya/komplekty_dlya_montazha/" data-id="3675" data-products-count="118">Комплекты для монтажа</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/komputernaya_periferiya/ustroystva_pechati/" data-id="352">Устройства печати, расходные материалы&nbsp;<span>7177</span></a>
                                                        <div data-id="352">
                                                            <ul>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/ustroystva_pechati/mfu/" data-id="666" data-products-count="282">МФУ</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/ustroystva_pechati/printery/" data-id="641" data-products-count="188">Принтеры</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/ustroystva_pechati/kopiry/" data-id="669" data-products-count="15">Копиры</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/ustroystva_pechati/plottery/" data-id="14913" data-products-count="21">Плоттеры</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/ustroystva_pechati/skanery/" data-id="670" data-products-count="77">Сканеры</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/ustroystva_pechati/optsii/" data-id="668" data-products-count="323">Опции для принтеров, копиров и МФУ</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/ustroystva_pechati/pechatnye_nositeli/" data-id="3741" data-products-count="535">Печатные носители</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/ustroystva_pechati/toners_chernila_zip/" data-id="3761" data-products-count="860">Расходные материалы для принтеров</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/ustroystva_pechati/plenka_dlya_laminirovaniya/" data-id="12485" data-products-count="100">Пленка для ламинирования</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/ustroystva_pechati/cartridji/" data-id="12506" data-products-count="4534">Картриджи</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/ustroystva_pechati/rashodka-dlya-broshuratorov/" data-id="12508" data-products-count="203">Расходка для брошюраторов</a></li>
                                                                <li><a href="/komputernaya_periferiya/ustroystva_pechati/">Все устройства печати, расходные материалы&nbsp;<span>7177</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/komputernaya_periferiya/akusticheskie_sistemy/" data-id="356">Акустические системы&nbsp;<span>743</span></a>
                                                        <div data-id="356">
                                                            <ul>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/akusticheskie_sistemy/akusticheskie_sistemy_2_0/" data-id="385" data-products-count="247">Акустические системы 2.0</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/akusticheskie_sistemy/akusticheskie_sistemy_2_1/" data-id="386" data-products-count="144">Акустические системы 2.1</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/akusticheskie_sistemy/portativnye_kolonki/" data-id="633" data-products-count="343">Портативные колонки</a></li>
                                                                <li><a href="/komputernaya_periferiya/akusticheskie_sistemy/">Все акустические системы&nbsp;<span>743</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/komputernaya_periferiya/usb_razvetviteli/" data-id="3359">USB хабы и разветвители&nbsp;<span>179</span></a></li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/komputernaya_periferiya/webcam/" data-id="361">Вэб-камеры&nbsp;<span>69</span></a></li>
                                                    <li><a href="/komputernaya_periferiya/garnitury/" data-id="362">Наушники и гарнитуры для компьютера&nbsp;<span>1174</span></a></li>
                                                    <li><a href="/komputernaya_periferiya/mikrofony/" data-id="3696">Микрофоны&nbsp;<span>51</span></a></li>
                                                    <li><a href="/komputernaya_periferiya/batareyki/" data-id="4026">Батарейки и аккумуляторы&nbsp;<span>369</span></a></li>
                                                    <li><a href="/komputernaya_periferiya/kardridery/" data-id="158">Карт-ридеры&nbsp;<span>101</span></a></li>
                                                    <li><a href="/komputernaya_periferiya/usb_igrushki/" data-id="11388">USB игрушки&nbsp;<span>1</span></a></li>
                                                    <li><a href="/komputernaya_periferiya/kovriki_dlya_mishsi/" data-id="12484">Коврики для мыши&nbsp;<span>172</span></a></li>
                                                </ul>
                                            </div>
                                            <div data-id="15553" data-items-per-column="3">
                                                <ul>
                                                    <li><a href="/kompyutery_noutbuki_soft/kibersport/igrovye-noutbuki/" data-id="15581">Игровые ноутбуки&nbsp;<span>360</span></a></li>
                                                    <li><a href="/kompyutery_noutbuki_soft/kibersport/igrovye-monobloki/" data-id="15582">Игровые моноблоки&nbsp;<span>8</span></a></li>
                                                    <li><a href="/kompyutery_noutbuki_soft/kibersport/igrovye-garnitury-i-mikrofony/" data-id="15573">Игровые гарнитуры и микрофоны&nbsp;<span>154</span></a></li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/kompyutery_noutbuki_soft/kibersport/manipulyatory-igrovye/" data-id="15574">Манипуляторы&nbsp;<span>488</span></a>
                                                        <div data-id="15574">
                                                            <ul>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/kibersport/manipulyatory-igrovye/gejmpady-dzhojstiki-ruli/" data-id="15575" data-products-count="92">Геймпады, джойстики, рули</a></li>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/kibersport/manipulyatory-igrovye/igrovye-klaviatury-i-komplekty/" data-id="15576" data-products-count="111">Игровые клавиатуры и комплекты</a></li>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/kibersport/manipulyatory-igrovye/igrovye-myshi/" data-id="15577" data-products-count="206">Игровые мыши</a></li>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/kibersport/manipulyatory-igrovye/igrovye-kovriki-dlya-myshek/" data-id="15578" data-products-count="79">Коврики для мышек</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/kompyutery_noutbuki_soft/kibersport/gejmerskie-kresla/" data-id="15579">Геймерские кресла&nbsp;<span>70</span></a></li>
                                                </ul>
                                            </div>
                                            <div data-id="353" data-items-per-column="18">
                                                <ul>
                                                    <li><a href="/communication-equipment/besprovodnoe_setevoe_oborudovanie/" data-id="49">Беспроводное сетевое оборудование&nbsp;<span>640</span></a>
                                                        <div data-id="49">
                                                            <ul>
                                                                <li class="leaf"><a href="/communication-equipment/besprovodnoe_setevoe_oborudovanie/tochki_dosutpa/" data-id="373" data-products-count="381">Wi-Fi точки доступа</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/besprovodnoe_setevoe_oborudovanie/wi-fi-retraslyatory/" data-id="15109" data-products-count="23">Wi-Fi ретрансляторы</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/besprovodnoe_setevoe_oborudovanie/adapnety-poe/" data-id="15494" data-products-count="35">Адаптеры PoE</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/besprovodnoe_setevoe_oborudovanie/adaptery/" data-id="371" data-products-count="113">Адаптеры</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/besprovodnoe_setevoe_oborudovanie/powerline/" data-id="3697" data-products-count="24">Оборудование Powerline</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/besprovodnoe_setevoe_oborudovanie/3gmodemy/" data-id="3706" data-products-count="20">Модемы 3G/4G</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/besprovodnoe_setevoe_oborudovanie/wi_fi_antenny/" data-id="369" data-products-count="39">Антенны</a></li>
                                                                <li><a href="/communication-equipment/besprovodnoe_setevoe_oborudovanie/">Все беспроводное сетевое оборудование&nbsp;<span>640</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/communication-equipment/provodnoe_setevoe_oborudovanie/" data-id="50">Проводное сетевое оборудование&nbsp;<span>633</span></a>
                                                        <div data-id="50">
                                                            <ul>
                                                                <li class="leaf"><a href="/communication-equipment/provodnoe_setevoe_oborudovanie/kommutatory_ethernet/" data-id="367" data-products-count="392">Коммутаторы</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/provodnoe_setevoe_oborudovanie/kommutatori_poe/" data-id="713" data-products-count="152">Коммутаторы PoE</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/provodnoe_setevoe_oborudovanie/routers-wifi/" data-id="372" data-products-count="67">Маршрутизаторы</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/provodnoe_setevoe_oborudovanie/adsl_oborudovanie/" data-id="368" data-products-count="22">Модемы</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/communication-equipment/passivnoe_setevoe_oborudovanie/" data-id="51">Пассивное сетевое оборудование&nbsp;<span>2709</span></a>
                                                        <div data-id="51">
                                                            <ul>
                                                                <li class="leaf"><a href="/communication-equipment/passivnoe_setevoe_oborudovanie/instrumenty/" data-id="365" data-products-count="221">Инструменты</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/passivnoe_setevoe_oborudovanie/external-network-storage/" data-id="366" data-products-count="157">Внешние сетевые хранилища</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/passivnoe_setevoe_oborudovanie/krepej_soedeniteli_kabelya/" data-id="647" data-products-count="388">Крепеж, соеденители кабеля</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/passivnoe_setevoe_oborudovanie/kabel_dlya_montaja_sistem_svyazi_i_signalizacii/" data-id="655" data-products-count="99">Кабель для монтажа систем связи и сигнализации</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/passivnoe_setevoe_oborudovanie/kabeli_patchcord/" data-id="363" data-products-count="696">Кабели сетевые(Патч-корд)</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/passivnoe_setevoe_oborudovanie/konnektori/" data-id="12134" data-products-count="30">Коннекторы</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/passivnoe_setevoe_oborudovanie/rozetki-setevie/" data-id="13313" data-products-count="60">Розетки сетевые</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/passivnoe_setevoe_oborudovanie/kabeli-v-buhtah/" data-id="13488" data-products-count="178">Кабель в бухтах</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/passivnoe_setevoe_oborudovanie/markeri_dlya_kabelya/" data-id="474" data-products-count="22">Маркеры для кабеля</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/passivnoe_setevoe_oborudovanie/kommunikacionnie-shkafi/" data-id="13289" data-products-count="850">Коммуникационные шкафы</a></li>
                                                                <li><a href="/communication-equipment/passivnoe_setevoe_oborudovanie/">Все пассивное сетевое оборудование&nbsp;<span>2709</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/communication-equipment/ip-phone/" data-id="3699">IP телефония&nbsp;<span>138</span></a>
                                                        <div data-id="3699">
                                                            <ul>
                                                                <li class="leaf"><a href="/communication-equipment/ip-phone/ip-phone/" data-id="376" data-products-count="88">IP-телефоны</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/ip-phone/bloki_pitaniya_voip/" data-id="3700" data-products-count="16">Блоки питания для IP телефонов</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/ip-phone/garnitury_dlya_voip/" data-id="3701" data-products-count="16">Гарнитуры для IP телефонов</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/ip-phone/shlyuzy_voip/" data-id="3704" data-products-count="17">Шлюзы для IP телефонии</a></li>
                                                                <li><a href="/communication-equipment/ip-phone/">Все ip телефония&nbsp;<span>138</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/communication-equipment/perekluchateli_perehodniki_moduli/" data-id="52">Переключатели, переходники, модули&nbsp;<span>420</span></a>
                                                        <div data-id="52">
                                                            <ul>
                                                                <li class="leaf"><a href="/communication-equipment/perekluchateli_perehodniki_moduli/kvm/" data-id="364" data-products-count="181">KVM-переключатели</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/perekluchateli_perehodniki_moduli/moduli_sfp/" data-id="375" data-products-count="142">Модули SFP</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/perekluchateli_perehodniki_moduli/mediamosty/" data-id="374" data-products-count="18">Медиамосты</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/perekluchateli_perehodniki_moduli/mediakonvertery/" data-id="370" data-products-count="69">Медиаконвертеры</a></li>
                                                                <li><a href="/communication-equipment/perekluchateli_perehodniki_moduli/">Все переключатели, переходники, модули&nbsp;<span>420</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/communication-equipment/prochee_setevoe_oborudovanie/" data-id="53">Прочее сетевое оборудование&nbsp;<span>177</span></a>
                                                        <div data-id="53">
                                                            <ul>
                                                                <li class="leaf"><a href="/communication-equipment/prochee_setevoe_oborudovanie/oborudovanie-dlya-peredachi-signala/" data-id="15293" data-products-count="58">Оборудование для передачи сигнала</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/prochee_setevoe_oborudovanie/setevye-ustrojstva-razlichnogo-naznacheniya/" data-id="15354" data-products-count="74">Сетевые устройства различного назначения</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/prochee_setevoe_oborudovanie/gradozashita/" data-id="15698" data-products-count="14">Грозозащита</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/prochee_setevoe_oborudovanie/videousiliteli-i-videokommutatory/" data-id="15814" data-products-count="31">Видеоусилители и видеокоммутаторы</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/communication-equipment/setevie_vidoeregistratori/" data-id="11482">Видеонаблюдение&nbsp;<span>840</span></a>
                                                        <div data-id="11482">
                                                            <ul>
                                                                <li class="leaf"><a href="/communication-equipment/setevie_vidoeregistratori/gotovie_komplekti/" data-id="55" data-products-count="26">Готовые комплекты</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/setevie_vidoeregistratori/kameri_videonabludeniya/" data-id="57" data-products-count="111">Камеры видеонаблюдения</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/setevie_vidoeregistratori/ip_camers/" data-id="4023" data-products-count="360">IP камеры</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/setevie_vidoeregistratori/imitaciya_videonabludeniya/" data-id="56" data-products-count="13">Имитация видеонаблюдения</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/setevie_vidoeregistratori/domofoni_i_videoglazki/" data-id="59" data-products-count="70">Домофоны и видеоглазки</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/setevie_vidoeregistratori/videoregistratori/" data-id="60" data-products-count="105">Видеорегистраторы</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/setevie_vidoeregistratori/kronshteyni_dlya_kamer_videonabludeniya/" data-id="62" data-products-count="76">Кронштейны для камер видеонаблюдения</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/setevie_vidoeregistratori/aksessuari_dlya_videonabludeniya/" data-id="58" data-products-count="48">Аксессуары для видеонаблюдения</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/setevie_vidoeregistratori/bloki_pitaniya_dlya_videokamer/" data-id="66" data-products-count="31">Блоки питания для видеокамер</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div data-id="84" data-items-per-column="19">
                                                <ul>
                                                    <li><a href="/audio-video_i_tv/televizori/" data-id="12481">Телевизоры&nbsp;<span>532</span></a></li>
                                                    <li><a href="/audio-video_i_tv/aksessuary_dlya_televizorov/" data-id="85">Аксессуары для телевизоров&nbsp;<span>956</span></a>
                                                        <div data-id="85">
                                                            <ul>
                                                                <li class="leaf"><a href="/audio-video_i_tv/aksessuary_dlya_televizorov/perehodniki_i_konnektori_dlya_tv/" data-id="397" data-products-count="160">Переходники и коннекторы для TV</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/aksessuary_dlya_televizorov/kronshteyny/" data-id="108" data-products-count="453">Кронштейны</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/aksessuary_dlya_televizorov/pulti_du/" data-id="4241" data-products-count="32">Пульты ДУ</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/aksessuary_dlya_televizorov/cleaning_goods/" data-id="4245" data-products-count="152">Чистящие средства</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/aksessuary_dlya_televizorov/tv_tyunery/" data-id="89" data-products-count="45">TV-тюнеры</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/aksessuary_dlya_televizorov/antenny/" data-id="15613" data-products-count="114">Антенны</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/audio-video_i_tv/igrovye_pristavki/" data-id="145">Игровые приставки&nbsp;<span>11</span></a></li>
                                                    <li><a href="/audio-video_i_tv/proektsionnoe_oborudovanie/" data-id="92">Проекционное оборудование&nbsp;<span>384</span></a>
                                                        <div data-id="92">
                                                            <ul>
                                                                <li class="leaf"><a href="/audio-video_i_tv/proektsionnoe_oborudovanie/proektsionnoe_oborudovanie/" data-id="116" data-products-count="128">Проекторы</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/proektsionnoe_oborudovanie/ekrany_na_shtative/" data-id="111" data-products-count="199">Экраны для проекторов</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/proektsionnoe_oborudovanie/aksessuary_dlja_proekcionnogo_oborudovanija/" data-id="4027" data-products-count="53">Кронштейны для проекторов</a></li>
                                                                <li><a href="/audio-video_i_tv/proektsionnoe_oborudovanie/">Все проекционное оборудование&nbsp;<span>384</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/audio-video_i_tv/mediaplayers/" data-id="90">Мультимедийные плееры&nbsp;<span>9</span></a></li>
                                                    <li><a href="/audio-video_i_tv/kabeli_dlja_video/" data-id="4021">Кабели для видео&nbsp;<span>874</span></a>
                                                        <div data-id="4021">
                                                            <ul>
                                                                <li class="leaf"><a href="/audio-video_i_tv/kabeli_dlja_video/kabeli-hdmi-hdmi/" data-id="13550" data-products-count="404">HDMI-HDMI</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/kabeli_dlja_video/kabeli-hdmi-dvi/" data-id="13551" data-products-count="59">HDMI-DVI</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/kabeli_dlja_video/kabeli-2rca-2rca/" data-id="13553" data-products-count="21">2RCA-2RCA</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/kabeli_dlja_video/kabeli-3rca-3rca/" data-id="13552" data-products-count="19">3RCA-3RCA</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/kabeli_dlja_video/kabeli-vga-vga/" data-id="13554" data-products-count="131">VGA-VGA</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/kabeli_dlja_video/kabeli-dvi-dvi/" data-id="13557" data-products-count="39">DVI-DVI</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/kabeli_dlja_video/kabeli-hdmi-microhdmi/" data-id="13558" data-products-count="40">HDMI - microHDMI</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/kabeli_dlja_video/kabeli-displayport/" data-id="13560" data-products-count="119">DisplayPort</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/kabeli_dlja_video/dvi-vga/" data-id="15633" data-products-count="13">DVI-VGA</a></li>
                                                                <li><a href="/audio-video_i_tv/kabeli_dlja_video/">Все кабели для видео&nbsp;<span>874</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/audio-video_i_tv/aksessuary_dlya_igrovykh_pristavok/" data-id="3676">Аксессуары для игровых приставок&nbsp;<span>3</span></a>
                                                        <div data-id="3676">
                                                            <ul>
                                                                <li><a href="/audio-video_i_tv/aksessuary_dlya_igrovykh_pristavok/">Все аксессуары для игровых приставок&nbsp;<span>3</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/audio-video_i_tv/audio-technika/" data-id="12768">Аудио техника&nbsp;<span>1016</span></a>
                                                        <div data-id="12768">
                                                            <ul>
                                                                <li class="leaf"><a href="/audio-video_i_tv/audio-technika/mp3/" data-id="143" data-products-count="42">MP3-плееры</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/audio-technika/naushniki/" data-id="11861" data-products-count="569">Наушники</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/audio-technika/muzykalnye_tsentry/" data-id="96" data-products-count="70">Музыкальные центры</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/audio-technika/magnitoly/" data-id="98" data-products-count="88">Магнитолы</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/audio-technika/domashnie_kinoteatry/" data-id="95" data-products-count="11">Домашние кинотеатры</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/audio-technika/radiobudilniki/" data-id="11225" data-products-count="152">Радиобудильники</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/audio-technika/hi_fi_akustika/" data-id="3673" data-products-count="28">Hi-Fi акустика</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/audio-technika/proigrovateli/" data-id="3641" data-products-count="29">Проигрыватели DVD и Blu-Ray</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/audio-technika/diktofony/" data-id="131" data-products-count="14">Диктофоны</a></li>
                                                                <li><a href="/audio-video_i_tv/audio-technika/">Все аудио техника&nbsp;<span>1016</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/audio-video_i_tv/tsifrovoe_tv/" data-id="88">Оборудование для спутникового тв&nbsp;<span>10</span></a></li>
                                                    <li><a href="/audio-video_i_tv/kabeli_antennye_i_televizionnye/" data-id="87">Кабели антенные и телевизионные&nbsp;<span>52</span></a></li>
                                                    <li><a href="/audio-video_i_tv/kabeli_dlya_audio/" data-id="94">Кабели для аудио&nbsp;<span>356</span></a></li>
                                                    <li><a href="/audio-video_i_tv/elektronika_i_foto/" data-id="63">Фото и видео&nbsp;<span>347</span></a>
                                                        <div data-id="63">
                                                            <ul>
                                                                <li class="leaf"><a href="/audio-video_i_tv/elektronika_i_foto/soap/" data-id="76" data-products-count="12">Компактные фотоаппараты</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/elektronika_i_foto/action-camera/" data-id="4024" data-products-count="26">Экшен-камеры</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/elektronika_i_foto/lenses/" data-id="107" data-products-count="51">Объективы</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/elektronika_i_foto/aksessuary_dlya_fotoapparatov/" data-id="121" data-products-count="96">Аксессуары для фотоаппаратов</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/elektronika_i_foto/kamery_videonablyudeniya/" data-id="13309" data-products-count="150">Камеры видеонаблюдения</a></li>
                                                                <li><a href="/audio-video_i_tv/elektronika_i_foto/">Все фото и видео&nbsp;<span>347</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div data-id="7" data-items-per-column="19">
                                                <ul>
                                                    <li><a href="/vse_dlya_doma_i_sada/sad_i_ogorod/" data-id="8">Сад и огород&nbsp;<span>1412</span></a>
                                                        <div data-id="8">
                                                            <ul>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/sad_i_ogorod/saovaya_technica/" data-id="11703" data-products-count="1068">Садовая техника</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/sad_i_ogorod/fonari/" data-id="11679" data-products-count="229">Фонари</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/sad_i_ogorod/sadovyi-inventary/" data-id="15533" data-products-count="83">Садовый инвентарь</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/sad_i_ogorod/ystroystva_dlya_borbi_s_vreditelyami/" data-id="646" data-products-count="32">Устройства для борьбы с вредителями</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/vse_dlya_doma_i_sada/posuda/" data-id="11362">Посуда&nbsp;<span>1511</span></a>
                                                        <div data-id="11362">
                                                            <ul>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/nozhi_tochilki/" data-id="11375" data-products-count="116">Кухонные ножи</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/chayniki/" data-id="457" data-products-count="46">Чайники</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/vse_dlya_chaya/" data-id="11372" data-products-count="177">Все для чая</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/nabory_posudy/" data-id="11364" data-products-count="74">Наборы посуды</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/kastruli/" data-id="11365" data-products-count="201">Кастрюли</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/kovshi/" data-id="11366" data-products-count="33">Ковши</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/kryshki/" data-id="11367" data-products-count="38">Крышки</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/skovorody/" data-id="11368" data-products-count="293">Сковороды</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/soteiniki/" data-id="11369" data-products-count="45">Сотейники</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/kuhonnyi_pribor/" data-id="11371" data-products-count="250">Кухонные приборы</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/stolovie_pribori/" data-id="297" data-products-count="16">Столовые приборы</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/protivni/" data-id="11486" data-products-count="14">Противни</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/farfor/" data-id="300" data-products-count="79">Фарфор</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/formi_dlya_zapekaniz/" data-id="11487" data-products-count="79">Формы для запекания</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/doski_razdelochnie/" data-id="11601" data-products-count="28">Доски разделочные</a></li>
                                                                <li><a href="/vse_dlya_doma_i_sada/posuda/">Все посуда&nbsp;<span>1511</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/vse_dlya_doma_i_sada/turizm_i_otdih/" data-id="12397">Туризм и отдых&nbsp;<span>576</span></a>
                                                        <div data-id="12397">
                                                            <ul>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/turizm_i_otdih/termosi-termokrujki/" data-id="14481" data-products-count="300">Термосы, термокружки и сумки-термосы</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/turizm_i_otdih/dorojnie-i-sportivnie-sumki/" data-id="12399" data-products-count="68">Дорожные и спортивные сумки</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/turizm_i_otdih/mebel-dlya-aktivnogo-otdiha/" data-id="12400" data-products-count="14">Мебель для активного отдыха</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/turizm_i_otdih/nabor-dlya-piknika/" data-id="12401" data-products-count="39">Наборы для пикника</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/turizm_i_otdih/shampuri-nabori-shampurov/" data-id="12869" data-products-count="19">Шампуры, наборы шампуров</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/turizm_i_otdih/mangali-koptilni/" data-id="12872" data-products-count="23">Мангалы.Коптильни</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/turizm_i_otdih/grili-reshetki-jarovni/" data-id="12873" data-products-count="52">Грили.Решетки.Жаровни</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/turizm_i_otdih/vse-dlya-rozjiga/" data-id="12874" data-products-count="19">Все для розжига</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/turizm_i_otdih/biotualety/" data-id="14485" data-products-count="22">Биотуалеты</a></li>
                                                                <li><a href="/vse_dlya_doma_i_sada/turizm_i_otdih/">Все туризм и отдых&nbsp;<span>576</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/vse_dlya_doma_i_sada/lyustry_i_svetilniki/" data-id="15113">Люстры и светильники&nbsp;<span>30767</span></a>
                                                        <div data-id="15113">
                                                            <ul>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/lyustry_i_svetilniki/lustry/" data-id="15173" data-products-count="7412">Люстры</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/lyustry_i_svetilniki/svetilniki/" data-id="15193" data-products-count="11997">Светильники</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/lyustry_i_svetilniki/bra_i_podsvetki/" data-id="15115" data-products-count="3134">Бра и подсветки</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/lyustry_i_svetilniki/lajtboksy/" data-id="15123" data-products-count="139">Лайтбоксы</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/lyustry_i_svetilniki/spoty/" data-id="15128" data-products-count="2438">Споты</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/lyustry_i_svetilniki/lampy_nastolnye/" data-id="15132" data-products-count="1668">Лампы настольные</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/lyustry_i_svetilniki/torshery/" data-id="15139" data-products-count="486">Торшеры</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/lyustry_i_svetilniki/lampy/" data-id="15140" data-products-count="1570">Лампы</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/lyustry_i_svetilniki/ulichnye_svetilniki/" data-id="15155" data-products-count="1923">Уличные светильники</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/vse_dlya_doma_i_sada/tv_shop/" data-id="216">ТВ-ШОП&nbsp;<span>1304</span></a>
                                                        <div data-id="216">
                                                            <ul>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/tv_shop/suveniri/" data-id="261" data-products-count="61">Сувениры</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/tv_shop/tovari_dlya_doma/" data-id="262" data-products-count="174">Товары для дома</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/tv_shop/tovari_dlya_krasoti_i_zdorovya/" data-id="272" data-products-count="234">Товары для красоты и здоровья</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/tv_shop/tovari_dlya_kuhni/" data-id="281" data-products-count="127">Товары для кухни</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/tv_shop/tovari_dlya_fitnesa_i_sporta/" data-id="292" data-products-count="235">Товары для фитнеса и спорта</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/tv_shop/aksessuari/" data-id="217" data-products-count="285">Аксессуары</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/tv_shop/detskiy_transport/" data-id="246" data-products-count="188">Детский транспорт</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div data-id="9" data-items-per-column="71">
                                                <ul>
                                                    <li><a href="/remont_i_stroitelstvo/electrika/" data-id="12808">Электрика&nbsp;<span>3591</span></a>
                                                        <div data-id="12808">
                                                            <ul>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/electrika/knopki_tumbleri_vikluchateli/" data-id="359" data-products-count="396">Кнопки, тумблеры, выключатели</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/electrika/neodimovie_magniti/" data-id="697" data-products-count="21">Неодимовые магниты</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/electrika/datchik-dvijeniya/" data-id="12809" data-products-count="25">Датчики движения</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/electrika/zvonki/" data-id="12810" data-products-count="76">Звонки</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/electrika/kabel-kanali-gofririvannie-trubi/" data-id="12811" data-products-count="387">Кабель, кабель-каналы, гофрированные трубы</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/electrika/rozetki-vilkuchateli-dimmeri/" data-id="12817" data-products-count="1759">Розетки, выключатели, диммеры</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/electrika/electrika-soputstvushie-tovari/" data-id="12827" data-products-count="77">Сопутствующие товары</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/electrika/shitki-avtomati-schetchiki/" data-id="12839" data-products-count="499">Щитки, автоматика, счетчики</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/electrika/koroba-uly-plintusy/" data-id="15110" data-products-count="142">Короба, углы и плинтусы</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/electrika/komplektuyushhie_k_elektricheskim_shkafam/" data-id="15255" data-products-count="182">Комплектующие к электрическим шкафам</a></li>
                                                                <li><a href="/remont_i_stroitelstvo/electrika/">Все электрика&nbsp;<span>3591</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/" data-id="77">Электроинструмент&nbsp;<span>3132</span></a>
                                                        <div data-id="77">
                                                            <ul>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/dreli_perforatori_otboinie_molotki/" data-id="11664" data-products-count="463">Дрели. Перфораторы. Отбойные молотки</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/shurupoverti_vintoverti_gaikoverti/" data-id="11681" data-products-count="474">Шуруповерты. Винтоверты. Гайковерты</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/pili_i_lobziki/" data-id="11670" data-products-count="460">Пилы и лобзики</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/shlifovalnyye_i_polirovalnyye_mashiny/" data-id="244" data-products-count="502">Шлифовальные и полировальные машины</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/feny_i_termopistolety/" data-id="11697" data-products-count="126">Фены и термопистолеты</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/graveri/" data-id="11662" data-products-count="31">Граверы</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/izmeritelnii_instrument/" data-id="11668" data-products-count="539">Измерительный инструмент</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/kraskopulti/" data-id="11669" data-products-count="71">Краскопульты</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/rubanki/" data-id="11676" data-products-count="70">Рубанки</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/payalniki/" data-id="395" data-products-count="105">Паяльники</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/stepleri_stroitelnie/" data-id="11677" data-products-count="28">Степлеры строительные</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/tochila/" data-id="11678" data-products-count="81">Точила</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/frezeri/" data-id="11680" data-products-count="55">Фрезеры</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/promishlennie_pilesosi/" data-id="11741" data-products-count="41">Промышленные пылесосы</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/vityajnie-ventilyatory/" data-id="13530" data-products-count="86">Вытяжные вентиляторы</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/remont_i_stroitelstvo/pnevmaticheskoe_oborudovanie/" data-id="419">Пневматическое оборудование&nbsp;<span>210</span></a>
                                                        <div data-id="419">
                                                            <ul>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/pnevmaticheskoe_oborudovanie/kompressori/" data-id="418" data-products-count="132">Компрессоры</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/pnevmaticheskoe_oborudovanie/pistoleti/" data-id="425" data-products-count="40">Пистолеты</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/pnevmaticheskoe_oborudovanie/shlifmashinki/" data-id="427" data-products-count="22">Шлифмашинки</a></li>
                                                                <li><a href="/remont_i_stroitelstvo/pnevmaticheskoe_oborudovanie/">Все пневматическое оборудование&nbsp;<span>210</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/remont_i_stroitelstvo/stroitelnoe_oborudovanie/" data-id="447">Строительное оборудование&nbsp;<span>79</span></a>
                                                        <div data-id="447">
                                                            <ul>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/stroitelnoe_oborudovanie/betonomeshalki/" data-id="448" data-products-count="27">Бетономешалки</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/stroitelnoe_oborudovanie/vibropliti_i_vibrotrambovki/" data-id="449" data-products-count="26">Виброплиты и вибротрамбовки</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/stroitelnoe_oborudovanie/motopompi/" data-id="450" data-products-count="26">Мотопомпы</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/remont_i_stroitelstvo/stanki/" data-id="451">Станки&nbsp;<span>141</span></a>
                                                        <div data-id="451">
                                                            <ul>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/stanki/stanki_tochilnie/" data-id="455" data-products-count="81">Станки точильные</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/stanki/stanki_sverilnie/" data-id="477" data-products-count="24">Станки сверильные</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/stanki/stanki_tokarnie/" data-id="478" data-products-count="14">Станки токарные</a></li>
                                                                <li><a href="/remont_i_stroitelstvo/stanki/">Все станки&nbsp;<span>141</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/" data-id="11686">Расходные материалы и аксессуары&nbsp;<span>7682</span></a>
                                                        <div data-id="11686">
                                                            <ul>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/vedra_stroitelnie/" data-id="659" data-products-count="11">Ведра строительные</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/akkumulyatori_dlya_elektroinstrumenta/" data-id="416" data-products-count="202">Аккумуляторы для электроинструмента</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/cepi/" data-id="678" data-products-count="62">Цепи</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/skobi_dlya_stroitelnogo_steplera/" data-id="679" data-products-count="57">Скобы для строительного степлера</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/buri/" data-id="11687" data-products-count="508">Буры</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/metizi_i_krepejnie_izdeliya/" data-id="687" data-products-count="594">Метизы и крепежные изделия</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/promishlennaya_himiya/" data-id="433" data-products-count="42">Промышленная химия</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/kordshetki/" data-id="695" data-products-count="21">Кордщетки</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/cepi/" data-id="456" data-products-count="62">Цепи</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/izolenta/" data-id="459" data-products-count="102">Изолента</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/skobi_dlya_stroitelnogo_steplera/" data-id="471" data-products-count="57">Скобы для строительного степлера</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/aksessuary_dlya_elektroinstrumenta/" data-id="11688" data-products-count="508">Аксессуары для электроинструмента</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/zubila/" data-id="11689" data-products-count="136">Зубила</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/diski_frezy_krugi/" data-id="11690" data-products-count="2383">Диски.Фрезы.Круги</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/nabory_bit_i_sverel/" data-id="11691" data-products-count="548">Наборы бит и сверл</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/osnastka_k_sadovoy_tekhnike/" data-id="11693" data-products-count="117">Оснастка к садовой технике</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/pilki_i_polotna/" data-id="11694" data-products-count="470">Пилки и полотна</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/sverla/" data-id="11695" data-products-count="1083">Сверла</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/shlifovalnyye_lenty_i_listy/" data-id="11696" data-products-count="399">Шлифовальные ленты и листы</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/sredstva_individualnoy_zashiti/" data-id="460" data-products-count="320">Средства индивидуальной защиты</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/remont_i_stroitelstvo/ruchnoy_instrument/" data-id="12049">Ручной инструмент&nbsp;<span>2951</span></a>
                                                        <div data-id="12049">
                                                            <ul>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/ruchnoy_instrument/nabori_torcevih_kluchey/" data-id="392" data-products-count="226">Наборы торцевых ключей</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/ruchnoy_instrument/pinceti/" data-id="394" data-products-count="26">Пинцеты</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/ruchnoy_instrument/gubcevii-instrument/" data-id="12687" data-products-count="534">Губцевый инструмент</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/ruchnoy_instrument/otvertki/" data-id="415" data-products-count="362">Отвертки</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/ruchnoy_instrument/pistolet_dlya_montajnoy_peni/" data-id="417" data-products-count="12">Пистолет для монтажной пены</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/ruchnoy_instrument/noji_i_lezviya/" data-id="463" data-products-count="178">Ножи и лезвия</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/ruchnoy_instrument/pili_ruchnie/" data-id="464" data-products-count="71">Пилы ручные</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/ruchnoy_instrument/ydarniy_i_richajniy_instrument/" data-id="480" data-products-count="265">Ударный и рычажный инструмент</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/ruchnoy_instrument/cluchi/" data-id="12686" data-products-count="531">Ключи</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/ruchnoy_instrument/pistoleti_dlya_germetika/" data-id="491" data-products-count="12">Пистолеты для герметика</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/ruchnoy_instrument/lestnici-stremyanki-verstaki/" data-id="12688" data-products-count="41">Лестницы, стремянки, верстаки</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/ruchnoy_instrument/sumki-i-yashiki-dlya-instrumenta/" data-id="12690" data-products-count="174">Сумки и ящики для инструментов</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/ruchnoy_instrument/shtukaturno-molyarnii-instrument/" data-id="12691" data-products-count="439">Штукатурно-малярный инструмент</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/ruchnoy_instrument/lopaty/" data-id="15259" data-products-count="75">Лопаты</a></li>
                                                                <li><a href="/remont_i_stroitelstvo/ruchnoy_instrument/">Все ручной инструмент&nbsp;<span>2951</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/remont_i_stroitelstvo/injenernaya-santehnika/" data-id="14833">Инженерная сантехника&nbsp;<span>2799</span></a>
                                                        <div data-id="14833">
                                                            <ul>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/kotly-otopleniya/" data-id="15693" data-products-count="107">Котлы отопления</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/gorelki/" data-id="640" data-products-count="17">Горелки</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/rulonnaya_teploizolyaciya/" data-id="696" data-products-count="16">Рулонная теплоизоляция</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/spesitely/" data-id="15753" data-products-count="187">Смесители</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/kuhonnye-moiky/" data-id="15594" data-products-count="180">Мойки</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/teplie-poli/" data-id="13549" data-products-count="246">Теплые полы</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/baki_membrannye_i_emkosti/" data-id="14835" data-products-count="60">Баки мембранные и емкости</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/vodozapornaya_armatura/" data-id="14836" data-products-count="275">Водозапорная арматура</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/kotelnoe_i_nasosnoe_oborudovanie/" data-id="14855" data-products-count="303">Котельное и насосное оборудование</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/krepezh_i_instrument/" data-id="14861" data-products-count="15">Крепеж и инструмент</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/podvodka_gibkaya/" data-id="14867" data-products-count="54">Подводка гибкая</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/radiatory_otopleniya/" data-id="14870" data-products-count="392">Радиаторы отопления</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/rezbovye_fitingi_i_kollektory/" data-id="14889" data-products-count="336">Резьбовые фитинги и коллекторы</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/shetchiky-kip/" data-id="14954" data-products-count="44">Счетчики, КИП, предохранительно-регулир. арматура</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/teplyj_pol/" data-id="14971" data-products-count="81">Теплый пол</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/truby_metallopolimernye_i_fitingi/" data-id="14977" data-products-count="171">Трубы металлополимерные и фитинги</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/truby_polipropilenovye_i_fitingi/" data-id="14983" data-products-count="221">Трубы полипропиленовые и фитинги</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/truby_stalnye_i_fitingi/" data-id="14986" data-products-count="77">Трубы стальные и фитинги</a></li>
                                                                <li><a href="/remont_i_stroitelstvo/injenernaya-santehnika/">Все инженерная сантехника&nbsp;<span>2799</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/remont_i_stroitelstvo/silovaya_technica/" data-id="11700">Силовая техника&nbsp;<span>540</span></a>
                                                        <div data-id="11700">
                                                            <ul>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/silovaya_technica/elektrodi/" data-id="431" data-products-count="132">Электроды</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/silovaya_technica/generatori/" data-id="11701" data-products-count="124">Генераторы</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/silovaya_technica/svarochnie_apparati/" data-id="11702" data-products-count="204">Сварочные аппараты</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/silovaya_technica/svarochnie_maski/" data-id="14753" data-products-count="80">Сварочные маски</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/remont_i_stroitelstvo/avtogarajnoe_oborudovanie/" data-id="434">Автогаражное оборудование&nbsp;<span>224</span></a>
                                                        <div data-id="434">
                                                            <ul>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/avtogarajnoe_oborudovanie/balonnie_kluchi/" data-id="436" data-products-count="21">Балонные ключи</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/avtogarajnoe_oborudovanie/diagnosticheskoe_oborudovanie/" data-id="438" data-products-count="12">Диагностическое оборудование</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/avtogarajnoe_oborudovanie/domkrati/" data-id="439" data-products-count="108">Домкраты</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/avtogarajnoe_oborudovanie/instrument_dlya_montajno_demontajnih_rabot_semniki/" data-id="440" data-products-count="20">Инструмент для монтажно-демонтажных работ (съемники)</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/avtogarajnoe_oborudovanie/oborudovanie_dlya_gsm/" data-id="442" data-products-count="31">Оборудование для ГСМ</a></li>
                                                                <li><a href="/remont_i_stroitelstvo/avtogarajnoe_oborudovanie/">Все автогаражное оборудование&nbsp;<span>224</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/" data-id="500">Запчасти для инструмента&nbsp;<span>860</span></a>
                                                        <div data-id="500">
                                                            <ul>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/kolca_porshnevie/" data-id="528" data-products-count="49">Кольца поршневые</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/kolca_stopornie/" data-id="529" data-products-count="18">Кольца стопорные</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/korpusi_prochie/" data-id="530" data-products-count="11">Корпусы прочие</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/krishki_probki/" data-id="531" data-products-count="28">Крышки, пробки</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/podshipniki/" data-id="570" data-products-count="13">Подшипники</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/prokladki/" data-id="575" data-products-count="13">Прокладки</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/prujini/" data-id="576" data-products-count="36">Пружины</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/rotori/" data-id="584" data-products-count="63">Роторы</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/svechi_zajiganiya/" data-id="588" data-products-count="23">Свечи зажигания</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/statori/" data-id="593" data-products-count="38">Статоры</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/trubki_i_shlangi/" data-id="599" data-products-count="25">Трубки и шланги</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/cpg_i_komplektuushie/" data-id="603" data-products-count="14">ЦПГ и комплектующие</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/shaybi/" data-id="604" data-products-count="32">Шайбы</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/shesterni/" data-id="609" data-products-count="52">Шестерни</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/shetki_ugolnie/" data-id="623" data-products-count="60">Щетки угольные</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/shetkoderjateli/" data-id="624" data-products-count="13">Щеткодержатели</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/zapchasti_prochie/" data-id="503" data-products-count="151">Запчасти прочие</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/zaryadnie_ustroystva/" data-id="504" data-products-count="32">Зарядные устройства</a></li>
                                                                <li><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/">Все запчасти для инструмента&nbsp;<span>860</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/remont_i_stroitelstvo/nabory_instrumenta/" data-id="242">Наборы инструментов&nbsp;<span>311</span></a></li>
                                                </ul>
                                            </div>
                                            <div data-id="10" data-items-per-column="32">
                                                <ul>
                                                    <li><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/" data-id="75">Климатическая техника&nbsp;<span>2536</span></a>
                                                        <div data-id="75">
                                                            <ul>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/polotencesushiteli/" data-id="393" data-products-count="27">Полотенцесушители</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/teplogeneratori/" data-id="700" data-products-count="19">Теплогенераторы</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/maslyannye-radistory/" data-id="240" data-products-count="146">Масляные радиаторы</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/teploventilyatory-teplovie-pushki/" data-id="241" data-products-count="273">Тепловентиляторы.Тепловые пушки</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/vityajnie-ventilyatory/" data-id="13528" data-products-count="86">Вытяжные вентиляторы</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/ventilyatory/" data-id="231" data-products-count="11">Вентиляторы</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/electrokamini/" data-id="11141" data-products-count="16">Электрокамины</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/konvectory/" data-id="238" data-products-count="241">Конвекторы</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/ifrakrasnye-obogrevateli/" data-id="229" data-products-count="142">Инфракрасные обогреватели</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/teplovye-zavesy/" data-id="237" data-products-count="57">Тепловые завесы</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/split-sistemy/" data-id="228" data-products-count="211">Сплит-системы</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/mobilnye_kondicionery/" data-id="232" data-products-count="16">Мобильные кондиционеры</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/osushiteli_vozdiha/" data-id="11432" data-products-count="13">Осушитель воздуха</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/uvlajniteli/" data-id="230" data-products-count="157">Увлажнители воздуха</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/vodonagrevateli/" data-id="235" data-products-count="717">Водонагреватели</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/ochestiteli-vozduha/" data-id="236" data-products-count="63">Очистители воздуха</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/pogodnie_stancii/" data-id="12441" data-products-count="50">Погодные станции</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/teplie-poli/" data-id="13548" data-products-count="246">Теплые полы</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/termoregulyatory/" data-id="14815" data-products-count="45">Терморегуляторы</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/krupnaya-bitovaya-tehnika/" data-id="12181">Крупная бытовая техника&nbsp;<span>2408</span></a>
                                                        <div data-id="12181">
                                                            <ul>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/krupnaya-bitovaya-tehnika/aksessuari_dlya_bitivoi_tehniki/" data-id="14794" data-products-count="104">Аксессуары для бытовой техники</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/krupnaya-bitovaya-tehnika/stiralnie-mashini/" data-id="12185" data-products-count="340">Стиральные машины</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/krupnaya-bitovaya-tehnika/sushilnie-mashini/" data-id="12189" data-products-count="12">Сушильные машины</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/krupnaya-bitovaya-tehnika/posudomoechnie-mashini/" data-id="12184" data-products-count="64">Посудомоечные машины</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/krupnaya-bitovaya-tehnika/gazovie-plati/" data-id="12183" data-products-count="384">Газовые плиты</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/krupnaya-bitovaya-tehnika/electricheskie-pliti/" data-id="12191" data-products-count="161">Электрические плиты</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/krupnaya-bitovaya-tehnika/vityajki/" data-id="12182" data-products-count="418">Вытяжки</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/krupnaya-bitovaya-tehnika/bitovaya-tehnika-holodilniki/" data-id="12190" data-products-count="723">Холодильники</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/krupnaya-bitovaya-tehnika/morozilnie-kameri/" data-id="12192" data-products-count="181">Морозильные камеры</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/krupnaya-bitovaya-tehnika/vinnie_shkafi/" data-id="13284" data-products-count="21">Винные шкафы</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/bytovaya_texnika_vstraivaemaya/" data-id="12201">Бытовая техника встраиваемая&nbsp;<span>1592</span></a>
                                                        <div data-id="12201">
                                                            <ul>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/bytovaya_texnika_vstraivaemaya/varochnye_paneli/" data-id="12202" data-products-count="569">Варочные панели</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/bytovaya_texnika_vstraivaemaya/duxovki/" data-id="12213" data-products-count="302">Духовые шкафы</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/bytovaya_texnika_vstraivaemaya/vytyazhki/" data-id="12210" data-products-count="583">Вытяжки</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/bytovaya_texnika_vstraivaemaya/vstraivaemye_stiralnye_mashiny/" data-id="12208" data-products-count="13">Встраиваемые стиральные машины</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/bytovaya_texnika_vstraivaemaya/vstraivaemye_posudomoechnye_mashiny/" data-id="12207" data-products-count="50">Встраиваемые посудомоечные машины</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/bytovaya_texnika_vstraivaemaya/vstraivaemye_mikrovolnovye_pechi/" data-id="12206" data-products-count="27">Встраиваемые микроволновые печи</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/bytovaya_texnika_vstraivaemaya/vstraeviemie-holodilniki/" data-id="12949" data-products-count="47">Встраиваемые холодильники</a></li>
                                                                <li><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/bytovaya_texnika_vstraivaemaya/">Все бытовая техника встраиваемая&nbsp;<span>1592</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/" data-id="69">Техника для кухни&nbsp;<span>4382</span></a>
                                                        <div data-id="69">
                                                            <ul>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/aerogrill/" data-id="195" data-products-count="92">Аэрогрили и сушки</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/chayniki/" data-id="194" data-products-count="850">Чайники</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/termopoty/" data-id="187" data-products-count="93">Термопоты</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/vakuumnii_upakovschiki/" data-id="14735" data-products-count="43">Вакуумные упаковщики</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/aksessuari_dlya_vakuumnih_upakovschikov/" data-id="14736" data-products-count="87">Аксессуары для вакуумных упаковщиков</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/kofevarki_geyzernie/" data-id="698" data-products-count="49">Кофеварки гейзерные</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/prigotovlenie_kofe/" data-id="210" data-products-count="191">Кофеварки</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/aksi-kofevarki/" data-id="12301" data-products-count="36">Аксессуары для кофеварок</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/kofemolki/" data-id="461" data-products-count="96">Кофемолки</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/kappuchinatory/" data-id="283" data-products-count="22">Капучинаторы</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/multivarki/" data-id="191" data-products-count="117">Мультиварки</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/aksessuary_dlya_multivarok/" data-id="301" data-products-count="35">Аксессуары для мультиварок</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/mikrovolnovye_pechi/" data-id="200" data-products-count="291">Микроволновые печи</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/miksery/" data-id="203" data-products-count="144">Миксеры</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/kuhooye-kombainy/" data-id="186" data-products-count="66">Кухонные комбайны</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/aksessuary-dlya-kuhonnih-kombainov/" data-id="14673" data-products-count="20">Аксессуары к кухонным комбайнам</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/myasorubki/" data-id="201" data-products-count="201">Мясорубки</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/askessuary-dlya-myasorubok/" data-id="14482" data-products-count="17">Аксессуары для мясорубок</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/blendery/" data-id="189" data-products-count="403">Блендеры</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/parovarki/" data-id="192" data-products-count="17">Пароварки</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/hlebopechi/" data-id="193" data-products-count="44">Хлебопечки</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/kuhonnye_vesy/" data-id="205" data-products-count="237">Кухонные весы</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/tostery/" data-id="196" data-products-count="133">Тостеры</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/sokovyzhimalki/" data-id="199" data-products-count="94">Соковыжималки</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/filtry_dlya_vody/" data-id="183" data-products-count="104">Фильтры для воды</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/kartridjy-dlya-vody/" data-id="14693" data-products-count="142">Картриджи для фильтров</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/pribory_dlya_hot_dogov/" data-id="206" data-products-count="382">Приборы для приготовления хот-догов</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/yogurt/" data-id="208" data-products-count="30">Йогуртницы\\Мороженицы</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/elektrogrili/" data-id="185" data-products-count="119">Электрогрили\\Шашлычницы</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/varochnye_plitki/" data-id="209" data-products-count="177">Варочные плитки</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/yacavarky/" data-id="14793" data-products-count="20">Яйцеварки</a></li>
                                                                <li><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/">Все техника для кухни&nbsp;<span>4382</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/" data-id="73">Техника для дома&nbsp;<span>1994</span></a>
                                                        <div data-id="73">
                                                            <ul>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/aksessuari_dlya_vannoy_komnati/" data-id="694" data-products-count="54">Аксессуары для ванной комнаты</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/pylesosy/" data-id="224" data-products-count="400">Пылесосы</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/roboti-pilesosi/" data-id="12786" data-products-count="110">Роботы-пылесосы и электровеники</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/aksessuary_dlya_pylesosov/" data-id="227" data-products-count="211">Аксессуары для пылесосов</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/utyugi/" data-id="13431" data-products-count="347">Утюги</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/otparivateli/" data-id="223" data-products-count="121">Отпариватели</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/chasy_budilniki/" data-id="226" data-products-count="65">Часы-будильники</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/uchod_za_odezdoi_i_obuv/" data-id="11436" data-products-count="21">Уход за одеждой и обувью</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/sumki-holodilniki/" data-id="627" data-products-count="83">Сумки-холодильники</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/sheinie-mashini/" data-id="12002" data-products-count="329">Швейные машины</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/hoz-inventar/" data-id="12081" data-products-count="182">Хозяйственный инвентарь</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/chasi/" data-id="12486" data-products-count="57">Часы</a></li>
                                                                <li><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/">Все техника для дома&nbsp;<span>1994</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div data-id="61" data-items-per-column="16">
                                                <ul>
                                                    <li><a href="/car-electronics/avtomobilnie-shini/" data-id="12221">Шины&nbsp;<span>2968</span></a>
                                                        <div data-id="12221">
                                                            <ul>
                                                                <li class="leaf"><a href="/car-electronics/avtomobilnie-shini/letnie/" data-id="298" data-products-count="624">Летние</a></li>
                                                                <li class="leaf"><a href="/car-electronics/avtomobilnie-shini/zimnie/" data-id="299" data-products-count="775">Зимние</a></li>
                                                                <li class="leaf"><a href="/car-electronics/avtomobilnie-shini/vsesezonnie_shini/" data-id="338" data-products-count="59">Всесезонные шины</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/car-electronics/avtomobilnie-diski/" data-id="12222">Диски&nbsp;<span>341</span></a>
                                                        <div data-id="12222">
                                                            <ul>
                                                                <li class="leaf"><a href="/car-electronics/avtomobilnie-diski/litie_diski/" data-id="357" data-products-count="15">Литые диски</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/car-electronics/avtohimiya_i_avtokosmetika/" data-id="78">Автохимия и автокосметика&nbsp;<span>788</span></a>
                                                        <div data-id="78">
                                                            <ul>
                                                                <li class="leaf"><a href="/car-electronics/avtohimiya_i_avtokosmetika/masla_i_smazki/" data-id="79" data-products-count="323">Масла и смазки</a></li>
                                                                <li class="leaf"><a href="/car-electronics/avtohimiya_i_avtokosmetika/avtokosmetika/" data-id="97" data-products-count="60">Автокосметика</a></li>
                                                                <li class="leaf"><a href="/car-electronics/avtohimiya_i_avtokosmetika/avtohimiya_potrebitelskaya/" data-id="99" data-products-count="149">Автохимия потребительская</a></li>
                                                                <li class="leaf"><a href="/car-electronics/avtohimiya_i_avtokosmetika/avtohimiya_professionalnaya/" data-id="120" data-products-count="78">Автохимия профессиональная</a></li>
                                                                <li class="leaf"><a href="/car-electronics/avtohimiya_i_avtokosmetika/konsistentnie_smazki/" data-id="139" data-products-count="21">Консистентные смазки</a></li>
                                                                <li class="leaf"><a href="/car-electronics/avtohimiya_i_avtokosmetika/lodochnaya_programma/" data-id="140" data-products-count="26">Лодочная программа</a></li>
                                                                <li class="leaf"><a href="/car-electronics/avtohimiya_i_avtokosmetika/motocikletnaya_programma/" data-id="162" data-products-count="86">Мотоциклетная программа</a></li>
                                                                <li class="leaf"><a href="/car-electronics/avtohimiya_i_avtokosmetika/specialnie_jidkosti/" data-id="204" data-products-count="28">Специальные жидкости</a></li>
                                                                <li><a href="/car-electronics/avtohimiya_i_avtokosmetika/">Все автохимия и автокосметика&nbsp;<span>788</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/car-electronics/avtomagnitoli/" data-id="11791">Автомагнитолы&nbsp;<span>166</span></a></li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/car-electronics/navigatory/" data-id="72">Автонавигаторы&nbsp;<span>19</span></a></li>
                                                    <li><a href="/car-electronics/videoregistratory/" data-id="80">Видеорегистраторы&nbsp;<span>113</span></a></li>
                                                    <li><a href="/car-electronics/radar-detectors/" data-id="110">Радары-детекторы&nbsp;<span>27</span></a></li>
                                                    <li><a href="/car-electronics/parkovochnye_radary/" data-id="3664">Парктроники&nbsp;<span>15</span></a></li>
                                                    <li><a href="/car-electronics/avtokompressory/" data-id="109">Компрессоры&nbsp;<span>80</span></a></li>
                                                    <li><a href="/car-electronics/invertory/" data-id="113">Автомобильные инверторы&nbsp;<span>33</span></a></li>
                                                    <li><a href="/car-electronics/avtoakustika/" data-id="112">Автоакустика&nbsp;<span>256</span></a>
                                                        <div data-id="112">
                                                            <ul>
                                                                <li class="leaf"><a href="/car-electronics/avtoakustika/usiliteli_zvuka/" data-id="3672" data-products-count="39">Автомобильные усилители звука</a></li>
                                                                <li class="leaf"><a href="/car-electronics/avtoakustika/akkustucheskie-kabeli/" data-id="12587" data-products-count="22">Акустические кабели</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/car-electronics/minimoyki/" data-id="3662">Минимойки&nbsp;<span>56</span></a></li>
                                                    <li><a href="/car-electronics/aksessuari_dlya_minimoek/" data-id="643">Аксессуары для минимоек&nbsp;<span>61</span></a></li>
                                                    <li><a href="/car-electronics/aksessuari_i_oborudovanie/" data-id="71">Аксессуары и оборудование&nbsp;<span>679</span></a>
                                                        <div data-id="71">
                                                            <ul>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/avtomobilnoe_osveshenie/" data-id="390" data-products-count="25">Автомобильное освещение</a></li>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/avtomobilnie_predohraniteli/" data-id="391" data-products-count="20">Автомобильные предохранители</a></li>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/stekloochistiteli/" data-id="432" data-products-count="61">Стеклоочистители</a></li>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/alkotestery/" data-id="68" data-products-count="11">Алкотестеры</a></li>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/fm_transmittery/" data-id="70" data-products-count="19">FM трансмиттеры</a></li>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/auto_zaryadnye_ustroyatva/" data-id="74" data-products-count="175">Автомобильные зарядные устройства</a></li>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/kamery_zadnego_vida/" data-id="3663" data-products-count="19">Камеры заднего вида</a></li>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/avtomobilnye_monitory_televizory/" data-id="3666" data-products-count="14">Автомобильные мониторы, телевизоры</a></li>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/avtomobilnye_pylesosy/" data-id="3667" data-products-count="30">Пылесосы для авто</a></li>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/avtomobilnye_kholodilniki/" data-id="3669" data-products-count="83">Автомобильные холодильники</a></li>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/avtoazvetviteli/" data-id="3671" data-products-count="30">Разветвители</a></li>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/manometri/" data-id="11419" data-products-count="25">Автомобильные манометры</a></li>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/puskozaryadnie_ustroistva/" data-id="11527" data-products-count="73">Пускозарядные устройства</a></li>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/other/" data-id="12527" data-products-count="83">Прочее</a></li>
                                                                <li><a href="/car-electronics/aksessuari_i_oborudovanie/">Все аксессуары и оборудование&nbsp;<span>679</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div data-id="12969" data-items-per-column="68">
                                                <ul>
                                                    <li><a href="/kids/detskie-kolyaski/" data-id="13160">Детские коляски&nbsp;<span>3365</span></a>
                                                        <div data-id="13160">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/detskie-kolyaski/vse-kolyaski/" data-id="14031" data-products-count="1275">Все коляски</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-kolyaski/dlya-novorozhdennyh/" data-id="13161" data-products-count="30">Коляски для новорожденных</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-kolyaski/kolyaski-2-v-1/" data-id="13164" data-products-count="486">Коляски 2 в 1</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-kolyaski/kolyaski-3-v-1/" data-id="13165" data-products-count="158">Коляски 3 в 1</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-kolyaski/kolyaski-trosli/" data-id="13999" data-products-count="165">Коляски-трости</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-kolyaski/progulochnye/" data-id="13162" data-products-count="395">Прогулочные коляски</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-kolyaski/kolyaski-dlya-dvoih-detei/" data-id="13167" data-products-count="41">Коляски для двоих детей</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-kolyaski/aksessuari-dlya-kolyasok/" data-id="13954" data-products-count="713">Аксессуары для колясок</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-kolyaski/complektuyshie-dlya-kolyasok/" data-id="13948" data-products-count="102">Комплектующие колясок</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/detskij-transport/" data-id="13193">Детский транспорт&nbsp;<span>874</span></a>
                                                        <div data-id="13193">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/detskij-transport/samokaty/" data-id="13200" data-products-count="492">Детские самокаты</a></li>
                                                                <li class="leaf"><a href="/kids/detskij-transport/velobalansiry/" data-id="13198" data-products-count="39">Беговелы (велобалансиры) для детей</a></li>
                                                                <li class="leaf"><a href="/kids/detskij-transport/velosipedy-dvukhkolesnye/" data-id="13196" data-products-count="46">Двухколесные велосипеды для детей</a></li>
                                                                <li class="leaf"><a href="/kids/detskij-transport/trehkolesnye-velosipedy/" data-id="13197" data-products-count="98">Детские трехколесные велосипеды</a></li>
                                                                <li class="leaf"><a href="/kids/detskij-transport/pedalnaya-mashina/" data-id="13195" data-products-count="18">Детская педальная машина</a></li>
                                                                <li class="leaf"><a href="/kids/detskij-transport/roliki/" data-id="13205" data-products-count="31">Детские ролики</a></li>
                                                                <li class="leaf"><a href="/kids/detskij-transport/electromobili/" data-id="13194" data-products-count="19">Электромобили и мопеды для детей</a></li>
                                                                <li class="leaf"><a href="/kids/detskij-transport/aksessuary-dlya-velosipedy/" data-id="13199" data-products-count="50">Аксессуары для детских велосипедов</a></li>
                                                                <li class="leaf"><a href="/kids/detskij-transport/detskie-shlemy/" data-id="13219" data-products-count="26">Детские шлемы</a></li>
                                                                <li class="leaf"><a href="/kids/detskij-transport/skejtbordy/" data-id="14992" data-products-count="53">Скейтборды</a></li>
                                                                <li><a href="/kids/detskij-transport/">Все детский транспорт&nbsp;<span>874</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/avtokresla/" data-id="13169">Детские автокресла&nbsp;<span>1295</span></a>
                                                        <div data-id="13169">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/avtokresla/vse-avokresla/" data-id="14032" data-products-count="585">Все автокресла</a></li>
                                                                <li class="leaf"><a href="/kids/avtokresla/gruppa-0-plus-s-rojeniya/" data-id="13973" data-products-count="97">Группа 0+ (0-13кг/от рождения до 1 года)</a></li>
                                                                <li class="leaf"><a href="/kids/avtokresla/gruppa-0-1/" data-id="13974" data-products-count="117">Группа 0+/1 (0-18кг/от рождения до 3.5-4 лет)</a></li>
                                                                <li class="leaf"><a href="/kids/avtokresla/gruppa-0-1-2/" data-id="13975" data-products-count="12">Группа 0+/1/2 (0-25 кг/от рождения до 7 лет)</a></li>
                                                                <li class="leaf"><a href="/kids/avtokresla/gruppa-1/" data-id="13976" data-products-count="63">Группа 1 (9-18 кг/от 1 до 3.5-4 лет)</a></li>
                                                                <li class="leaf"><a href="/kids/avtokresla/gruppa-1-2/" data-id="13977" data-products-count="19">Группа 1/2 (от 9-25кг/от 1 до7 лет)</a></li>
                                                                <li class="leaf"><a href="/kids/avtokresla/gruppa-1-2-3/" data-id="13978" data-products-count="157">Группа 1/2/3 (от 9-36кг/от 1 до 12 лет)</a></li>
                                                                <li class="leaf"><a href="/kids/avtokresla/gruppa-2-3/" data-id="13979" data-products-count="110">Группа 2/3 (от 15-36кг/от 3 до 12 лет)</a></li>
                                                                <li class="leaf"><a href="/kids/avtokresla/aksessuary-dlya-avtokresel/" data-id="13174" data-products-count="125">Аксессуары для автокресел</a></li>
                                                                <li><a href="/kids/avtokresla/">Все детские автокресла&nbsp;<span>1295</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/yhod_za_detmi/" data-id="215">Уход за детьми&nbsp;<span>978</span></a>
                                                        <div data-id="215">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/yhod_za_detmi/kosmetika-i-bitovaya-himiya/" data-id="12970" data-products-count="518">Косметика и бытовая химия</a></li>
                                                                <li class="leaf"><a href="/kids/yhod_za_detmi/podguzniki/" data-id="12971" data-products-count="173">Подгузники, трусики, пеленки</a></li>
                                                                <li class="leaf"><a href="/kids/yhod_za_detmi/kupanie-rebenka/" data-id="13011" data-products-count="287">Купание ребенка</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/lego-konstruktory/" data-id="14028">Товары Lego&nbsp;<span>437</span></a>
                                                        <div data-id="14028">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/lego-konstruktory/konstruktor-lego/" data-id="14233" data-products-count="215">Конструкторы LEGO</a></li>
                                                                <li><a href="/kids/lego-konstruktory/">Все товары lego&nbsp;<span>437</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/detskie-igrushki/" data-id="13045">Детские игрушки&nbsp;<span>22144</span></a>
                                                        <div data-id="13045">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/konstruktory-dlya-detej/" data-id="13057" data-products-count="3947">Конструкторы для детей</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/interaktivnye-igrushki/" data-id="13051" data-products-count="949">Интерактивные игрушки</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/myagkie-detskie-igrushki/" data-id="13049" data-products-count="973">Мягкие игрушки</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/nabory-dlya-devochek/" data-id="13089" data-products-count="634">Игровые наборы для девочек</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/kosmetika-ukrasheniya/" data-id="14069" data-products-count="225">Косметика и украшения для девочек</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/kukly/" data-id="13052" data-products-count="2934">Куклы</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/nabory-dlya-malchikov/" data-id="13090" data-products-count="916">Игровые наборы для мальчиков</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/igrushechnyi-transport/" data-id="14116" data-products-count="3388">Транспортная игрушка</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/radioupravlyaemye-igrushki/" data-id="13054" data-products-count="1002">Радиоуправляемые игрушки</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/roboty/" data-id="13055" data-products-count="339">Игрушки "Роботы"</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/igrushechnoe-oruzhie/" data-id="13056" data-products-count="576">Игрушечное оружие</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/detskaya_bytovaya_texnika/" data-id="13432" data-products-count="126">Детская бытовая техника</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/music/" data-id="13083" data-products-count="447">Музыкальные инструменты</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/igrushki-dlya-malyshey/" data-id="13048" data-products-count="3318">Игрушки для малышей</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/razvivayushie_igrushki/" data-id="14270" data-products-count="770">Развивающие игрушки для малышей</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/rolgames/" data-id="13075" data-products-count="1021">Детские сюжетно - ролевые игры</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/figurki-detskie/" data-id="13093" data-products-count="491">Детские фигурки</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/igrushki-dlja-ulicy/" data-id="13088" data-products-count="51">Игрушки для улицы</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/hranenie-igrushek/" data-id="13688" data-products-count="31">Хранение игрушек</a></li>
                                                                <li><a href="/kids/detskie-igrushki/">Все детские игрушки&nbsp;<span>22144</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/derevjannye-igrushki/" data-id="13091">Деревянные игрушки&nbsp;<span>447</span></a>
                                                        <div data-id="13091">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/derevjannye-igrushki/derevyannie-kubiki-pazli-konstruktory/" data-id="14454" data-products-count="213">Деревянные кубики, пазлы, конструкторы для детей</a></li>
                                                                <li class="leaf"><a href="/kids/derevjannye-igrushki/razvivayshie-igrushki-iz-dereva/" data-id="14460" data-products-count="166">Развивающие игрушки из дерева</a></li>
                                                                <li class="leaf"><a href="/kids/derevjannye-igrushki/nastolnie-igrri-iz-dereva/" data-id="14447" data-products-count="19">Настольные игры из дерева для детей</a></li>
                                                                <li class="leaf"><a href="/kids/derevjannye-igrushki/derevyannie-piramidki-dlya-detei/" data-id="14452" data-products-count="18">Деревянные пирамидки для детей</a></li>
                                                                <li class="leaf"><a href="/kids/derevjannye-igrushki/prochie-derevyannie-igrushki/" data-id="14451" data-products-count="11">Прочие</a></li>
                                                                <li><a href="/kids/derevjannye-igrushki/">Все деревянные игрушки&nbsp;<span>447</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/kormlenie/" data-id="13110">Питание и кормление&nbsp;<span>1146</span></a>
                                                        <div data-id="13110">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/kormlenie/butylochki-dlya-kormleniya/" data-id="13156" data-products-count="194">Бутылочки для кормления</a></li>
                                                                <li class="leaf"><a href="/kids/kormlenie/poilniki/" data-id="14501" data-products-count="137">Поильники</a></li>
                                                                <li class="leaf"><a href="/kids/kormlenie/aksessuary-dlya-butylochek-i-poilnikov/" data-id="14502" data-products-count="25">Аксессуары для бутылочек и поильников</a></li>
                                                                <li class="leaf"><a href="/kids/kormlenie/soski/" data-id="13154" data-products-count="159">Соски</a></li>
                                                                <li class="leaf"><a href="/kids/kormlenie/pustyshki/" data-id="13153" data-products-count="285">Пустышки</a></li>
                                                                <li class="leaf"><a href="/kids/kormlenie/aksessuary-dlya-pustyshek-i-sosok/" data-id="14503" data-products-count="53">Аксессуары для пустышек и сосок</a></li>
                                                                <li class="leaf"><a href="/kids/kormlenie/dlya-mytya-butylochek/" data-id="13151" data-products-count="24">Все для мытья бутылочек</a></li>
                                                                <li class="leaf"><a href="/kids/kormlenie/posuda-dlya-kormleniya/" data-id="13152" data-products-count="188">Детская посуда для кормления</a></li>
                                                                <li class="leaf"><a href="/kids/kormlenie/slyunyavchiki-nagrudniki/" data-id="13157" data-products-count="52">Слюнявчики и нагрудники</a></li>
                                                                <li class="leaf"><a href="/kids/kormlenie/kontejnery-pakety-dlya-hraneniya/" data-id="14504" data-products-count="26">Контейнеры, пакеты для хранения</a></li>
                                                                <li><a href="/kids/kormlenie/">Все питание и кормление&nbsp;<span>1146</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/kids/dlya-beremennih/" data-id="12997">Товары для беременных и кормящих&nbsp;<span>59</span></a>
                                                        <div data-id="12997">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/dlya-beremennih/molokootsosy/" data-id="14490" data-products-count="18">Молокоотсосы</a></li>
                                                                <li class="leaf"><a href="/kids/dlya-beremennih/prokladki-dlya-grudi/" data-id="14492" data-products-count="12">Прокладки для груди</a></li>
                                                                <li><a href="/kids/dlya-beremennih/">Все товары для беременных и кормящих&nbsp;<span>59</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/stulchiki-dlya-kormleniya-i-aksessuary/" data-id="13111">Стульчики для кормления и аксессуары к ним&nbsp;<span>190</span></a>
                                                        <div data-id="13111">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/stulchiki-dlya-kormleniya-i-aksessuary/aksessuary/" data-id="13133" data-products-count="36">Аксессуары к стульчикам для кормления</a></li>
                                                                <li class="leaf"><a href="/kids/stulchiki-dlya-kormleniya-i-aksessuary/stulchiki-dlya-kormleniya/" data-id="14030" data-products-count="154">Стульчики для кормления</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/detskie-perenoski/" data-id="13175">Детские переноски&nbsp;<span>80</span></a>
                                                        <div data-id="13175">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/detskie-perenoski/sumki-i-korzini-dlya-perenoski/" data-id="13176" data-products-count="12">Сумки и корзины для переноски</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-perenoski/rukzak-kenguru/" data-id="13993" data-products-count="47">Рюкзаки-кенгуру</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-perenoski/konverty-na-vupisky/" data-id="14130" data-products-count="20">Конверты на выписку</a></li>
                                                                <li><a href="/kids/detskie-perenoski/">Все детские переноски&nbsp;<span>80</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/hodunki-i-prigunki/" data-id="13897">Ходунки и прыгунки&nbsp;<span>43</span></a>
                                                        <div data-id="13897">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/hodunki-i-prigunki/hodunki/" data-id="13898" data-products-count="43">Ходунки</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/detskaya-komnata/" data-id="13017">Мебель для детской комнаты&nbsp;<span>2464</span></a>
                                                        <div data-id="13017">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/detskaya-komnata/kolibely/" data-id="13822" data-products-count="72">Колыбели</a></li>
                                                                <li class="leaf"><a href="/kids/detskaya-komnata/krovati/" data-id="13825" data-products-count="1665">Кровати</a></li>
                                                                <li class="leaf"><a href="/kids/detskaya-komnata/komodi/" data-id="13850" data-products-count="337">Комоды</a></li>
                                                                <li class="leaf"><a href="/kids/detskaya-komnata/shkafi/" data-id="13861" data-products-count="25">Шкафы</a></li>
                                                                <li class="leaf"><a href="/kids/detskaya-komnata/shezlongi/" data-id="13900" data-products-count="11">Шезлонги</a></li>
                                                                <li class="leaf"><a href="/kids/detskaya-komnata/kresla-kachalki/" data-id="13901" data-products-count="11">Кресла-качалки</a></li>
                                                                <li class="leaf"><a href="/kids/detskaya-komnata/kacheli/" data-id="13902" data-products-count="15">Качели</a></li>
                                                                <li class="leaf"><a href="/kids/detskaya-komnata/nochniki_i_proektory/" data-id="14268" data-products-count="32">Ночники и проекторы</a></li>
                                                                <li class="leaf"><a href="/kids/detskaya-komnata/bezopasnost-malisha/" data-id="13903" data-products-count="81">Безопасность малыша</a></li>
                                                                <li class="leaf"><a href="/kids/detskaya-komnata/stoli-i-parti/" data-id="13872" data-products-count="184">Столы и парты</a></li>
                                                                <li class="leaf"><a href="/kids/detskaya-komnata/polki/" data-id="13929" data-products-count="18">Полки</a></li>
                                                                <li><a href="/kids/detskaya-komnata/">Все мебель для детской комнаты&nbsp;<span>2464</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/dlya-ulicy/" data-id="13159">Спорт и отдых&nbsp;<span>1946</span></a>
                                                        <div data-id="13159">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/dlya-ulicy/leto/" data-id="13206" data-products-count="786">Для лета</a></li>
                                                                <li class="leaf"><a href="/kids/dlya-ulicy/zima/" data-id="13181" data-products-count="508">Для зимы</a></li>
                                                                <li class="leaf"><a href="/kids/dlya-ulicy/sport-games/" data-id="13180" data-products-count="258">Спортивные детские игры</a></li>
                                                                <li class="leaf"><a href="/kids/dlya-ulicy/domiki-palatki/" data-id="13218" data-products-count="181">Детские домики - палатки</a></li>
                                                                <li class="leaf"><a href="/kids/dlya-ulicy/velosipedy/" data-id="14226" data-products-count="208">Велосипеды и беговелы</a></li>
                                                                <li><a href="/kids/dlya-ulicy/">Все спорт и отдых&nbsp;<span>1946</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/school-art/" data-id="13220">Творчество и хобби&nbsp;<span>4153</span></a>
                                                        <div data-id="13220">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/school-art/3druchka/" data-id="15013" data-products-count="42">3D Ручки</a></li>
                                                                <li class="leaf"><a href="/kids/school-art/rukodelie/" data-id="13238" data-products-count="461">Рукоделие</a></li>
                                                                <li class="leaf"><a href="/kids/school-art/lepka/" data-id="13230" data-products-count="720">Все для лепки</a></li>
                                                                <li class="leaf"><a href="/kids/school-art/nabory/" data-id="13244" data-products-count="1274">Наборы для творчества</a></li>
                                                                <li class="leaf"><a href="/kids/school-art/nabory-khudozhestvennye/" data-id="13276" data-products-count="536">Художественные наборы</a></li>
                                                                <li class="leaf"><a href="/kids/school-art/pazly-dlja-detej/" data-id="13228" data-products-count="876">Пазлы</a></li>
                                                                <li class="leaf"><a href="/kids/school-art/mozaika/" data-id="14154" data-products-count="35">Мозаика</a></li>
                                                                <li class="leaf"><a href="/kids/school-art/sbornie-modeli/" data-id="14051" data-products-count="202">Масштабные сборные модели</a></li>
                                                                <li><a href="/kids/school-art/">Все творчество и хобби&nbsp;<span>4153</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/dlya-prazdnika/" data-id="13629">Товары для праздника&nbsp;<span>4586</span></a>
                                                        <div data-id="13629">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/dlya-prazdnika/novyi-god/" data-id="13630" data-products-count="2109">Новый Год</a></li>
                                                                <li class="leaf"><a href="/kids/dlya-prazdnika/karnavalnye-kostumy/" data-id="13633" data-products-count="401">Карнавальные костюмы</a></li>
                                                                <li class="leaf"><a href="/kids/dlya-prazdnika/mylnye-puzyri/" data-id="13631" data-products-count="51">Мыльные пузыри</a></li>
                                                                <li class="leaf"><a href="/kids/dlya-prazdnika/atributy-prazdnika/" data-id="13632" data-products-count="491">Атрибуты для праздника</a></li>
                                                                <li class="leaf"><a href="/kids/dlya-prazdnika/servirovka-stola-svechi/" data-id="13634" data-products-count="309">Сервировка стола и свечи</a></li>
                                                                <li class="leaf"><a href="/kids/dlya-prazdnika/podarochnye-pakety/" data-id="13635" data-products-count="685">Подарочные пакеты</a></li>
                                                                <li class="leaf"><a href="/kids/dlya-prazdnika/novogonie-suveniry/" data-id="15374" data-products-count="536">Новогодние сувениры</a></li>
                                                                <li><a href="/kids/dlya-prazdnika/">Все товары для праздника&nbsp;<span>4586</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/razvitie-shkola/" data-id="13768">Развивающие пособия и игры&nbsp;<span>1328</span></a>
                                                        <div data-id="13768">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/razvitie-shkola/detskie-planshety/" data-id="13224" data-products-count="20">Детские компьютеры и планшеты</a></li>
                                                                <li class="leaf"><a href="/kids/razvitie-shkola/nabory-opytov/" data-id="14147" data-products-count="348">Исследования, опыты и эксперименты</a></li>
                                                                <li class="leaf"><a href="/kids/razvitie-shkola/metodika-umnitca/" data-id="14148" data-products-count="64">Методика развития Умница</a></li>
                                                                <li class="leaf"><a href="/kids/razvitie-shkola/obuchayushhie-materialy/" data-id="13226" data-products-count="238">Обучающие материалы для детей</a></li>
                                                                <li class="leaf"><a href="/kids/razvitie-shkola/razvitie/" data-id="13225" data-products-count="48">Обучающие плакаты для детей</a></li>
                                                                <li class="leaf"><a href="/kids/razvitie-shkola/posobiya-dlya-detei/" data-id="13279" data-products-count="89">Учимся читать, писать, считать</a></li>
                                                                <li class="leaf"><a href="/kids/razvitie-shkola/detskie-nabori/" data-id="13223" data-products-count="513">Развивающие настольные игры</a></li>
                                                                <li><a href="/kids/razvitie-shkola/">Все развивающие пособия и игры&nbsp;<span>1328</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/detskie-knigi/" data-id="13277">Книги для детей и родителей&nbsp;<span>1375</span></a>
                                                        <div data-id="13277">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/detskie-knigi/dlya-malyshej/" data-id="13278" data-products-count="486">Книги для малышей</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-knigi/govoryashie-knigi/" data-id="14078" data-products-count="206">Говорящие книги</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-knigi/dlya-doshkolnika/" data-id="13280" data-products-count="160">Книги для дошкольника</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-knigi/dlya-shkolnika/" data-id="13281" data-products-count="89">Книги для школьника</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-knigi/knigi-dlya-dosuga/" data-id="13638" data-products-count="252">Книги для досуга</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-knigi/raskraski/" data-id="13248" data-products-count="166">Раскраски для детей</a></li>
                                                                <li><a href="/kids/detskie-knigi/">Все книги для детей и родителей&nbsp;<span>1375</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/nabori-focusi/" data-id="13222">Наборы юного фокусника&nbsp;<span>38</span></a></li>
                                                    <li><a href="/kids/nastolnye-igry-dlya-detej/" data-id="13221">Настольные игры&nbsp;<span>1271</span></a>
                                                        <div data-id="13221">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/nastolnye-igry-dlya-detej/igra-morskoi-boi/" data-id="14056" data-products-count="11">Морской бой</a></li>
                                                                <li class="leaf"><a href="/kids/nastolnye-igry-dlya-detej/hasbro-mattel-games/" data-id="14058" data-products-count="75">Игры Hasbro и Mattel</a></li>
                                                                <li class="leaf"><a href="/kids/nastolnye-igry-dlya-detej/game-hobby-world/" data-id="14059" data-products-count="13">Игры Hobby World</a></li>
                                                                <li class="leaf"><a href="/kids/nastolnye-igry-dlya-detej/nastolnye_igry_magellan/" data-id="14991" data-products-count="34">Игры Magellan</a></li>
                                                                <li class="leaf"><a href="/kids/nastolnye-igry-dlya-detej/logicheskie-nastolnye-igry/" data-id="14396" data-products-count="44">Логические игры</a></li>
                                                                <li class="leaf"><a href="/kids/nastolnye-igry-dlya-detej/igry-dlya-malyshei/" data-id="14063" data-products-count="106">Игры для малышей</a></li>
                                                                <li class="leaf"><a href="/kids/nastolnye-igry-dlya-detej/igry-dlya-kompanii/" data-id="14062" data-products-count="397">Игры для компании</a></li>
                                                                <li class="leaf"><a href="/kids/nastolnye-igry-dlya-detej/golovolomki-dlja-detej/" data-id="13229" data-products-count="146">Головоломки для детей</a></li>
                                                                <li class="leaf"><a href="/kids/nastolnye-igry-dlya-detej/nastolnye-igry-brodilki/" data-id="14397" data-products-count="131">Настольные игры бродилки</a></li>
                                                                <li class="leaf"><a href="/kids/nastolnye-igry-dlya-detej/loto_domino_shashki_i_shahmaty/" data-id="14269" data-products-count="193">Лото, домино, шашки и шахматы</a></li>
                                                                <li class="leaf"><a href="/kids/nastolnye-igry-dlya-detej/sportivnye_nastolnye_igry/" data-id="14541" data-products-count="105">Спортивные настольные игры</a></li>
                                                                <li><a href="/kids/nastolnye-igry-dlya-detej/">Все настольные игры&nbsp;<span>1271</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/kids/school/" data-id="14128">Школьные товары&nbsp;<span>1334</span></a>
                                                        <div data-id="14128">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/school/globusy/" data-id="13820" data-products-count="40">Глобусы</a></li>
                                                                <li class="leaf"><a href="/kids/school/molberty/" data-id="13264" data-products-count="206">Мольберты и доски для детей</a></li>
                                                                <li class="leaf"><a href="/kids/school/rukzaki/" data-id="13257" data-products-count="769">Ранцы, рюкзаки и сумки</a></li>
                                                                <li class="leaf"><a href="/kids/school/meshki-dlya-obuvi/" data-id="14133" data-products-count="50">Мешки для обуви</a></li>
                                                                <li class="leaf"><a href="/kids/school/penaly/" data-id="13258" data-products-count="261">Пеналы и папки</a></li>
                                                                <li><a href="/kids/school/">Все школьные товары&nbsp;<span>1334</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/bitovaya-tehnika-i-electronika/" data-id="13134">Бытовая техника и электроника&nbsp;<span>30</span></a>
                                                        <div data-id="13134">
                                                            <ul>
                                                                <li><a href="/kids/bitovaya-tehnika-i-electronika/">Все бытовая техника и электроника&nbsp;<span>30</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/odezhda-dlja-detej/" data-id="13095">Одежда для детей&nbsp;<span>690</span></a>
                                                        <div data-id="13095">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/odezhda-dlja-detej/karnaval/" data-id="14076" data-products-count="401">Карнавальные костюмы</a></li>
                                                                <li class="leaf"><a href="/kids/odezhda-dlja-detej/verhnyaya-odezhda-dlya-detej/" data-id="13098" data-products-count="93">Верхняя одежда для детей</a></li>
                                                                <li class="leaf"><a href="/kids/odezhda-dlja-detej/vyazannyj-trikotazh/" data-id="13099" data-products-count="13">Вязаный трикотаж для детей</a></li>
                                                                <li class="leaf"><a href="/kids/odezhda-dlja-detej/dlya-novorozhdennyh/" data-id="13102" data-products-count="165">Одежда для новорожденных</a></li>
                                                                <li class="leaf"><a href="/kids/odezhda-dlja-detej/progulochnye-konverty/" data-id="13100" data-products-count="13">Прогулочные конверты</a></li>
                                                                <li><a href="/kids/odezhda-dlja-detej/">Все одежда для детей&nbsp;<span>690</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/sumki-i-rukzaki/" data-id="13178">Сумки и рюкзаки&nbsp;<span>91</span></a>
                                                        <div data-id="13178">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/sumki-i-rukzaki/sumki-dlya-mami-papi/" data-id="13994" data-products-count="32">Сумки для мамы и папы</a></li>
                                                                <li class="leaf"><a href="/kids/sumki-i-rukzaki/detskie-rukzachki/" data-id="13996" data-products-count="41">Детские рюкзачки</a></li>
                                                                <li><a href="/kids/sumki-i-rukzaki/">Все сумки и рюкзаки&nbsp;<span>91</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/kresla-kachalki-dlyu-mamu/" data-id="14348">Кресла-качалки для мамы&nbsp;<span>23</span></a></li>
                                                </ul>
                                            </div>
                                            <div data-id="11" data-items-per-column="9">
                                                <ul>
                                                    <li><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/" data-id="67">Красота и здоровье&nbsp;<span>1294</span></a>
                                                        <div data-id="67">
                                                            <ul>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/feny/" data-id="170" data-products-count="229">Фены</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/fen-shetki/" data-id="13328" data-products-count="42">Фены-щетки</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/schiptsy_dlya_zavivki/" data-id="178" data-products-count="127">Щипцы для завивки</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/elektrobigudi/" data-id="171" data-products-count="26">Электробигуди/Стайлеры</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/vypryamiteli_dlya_volos/" data-id="175" data-products-count="92">Выпрямители для волос</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/pribory_dlya_ukladki_volos/" data-id="179" data-products-count="127">Приборы для укладки волос</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/sushilki_dlya_ruk/" data-id="167" data-products-count="35">Сушилки для рук</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/massazhery/" data-id="169" data-products-count="45">Массажёры</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/elektrobritva/" data-id="172" data-products-count="115">Электробритвы</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/aksessuari-dlya-electrobritv/" data-id="12770" data-products-count="49">Аксессуары для электробритв</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/zubnye_schetki/" data-id="173" data-products-count="33">Зубные щетки</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/nasadki-dlya-zubnih-shetok/" data-id="14473" data-products-count="30">Насадки для зубных щеток</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/epilyatory/" data-id="177" data-products-count="70">Эпиляторы</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/mahinki-dlya-strijki/" data-id="12666" data-products-count="252">Машинки для стрижки</a></li>
                                                                <li><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/">Все красота и здоровье&nbsp;<span>1294</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/bytovaya-ximiya-i-kosmetika/" data-id="15877">Бытовая химия и косметика&nbsp;<span>6612</span></a>
                                                        <div data-id="15877">
                                                            <ul>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/bytovaya-ximiya-i-kosmetika/bytovaya-ximiya/" data-id="15855" data-products-count="876">Бытовая химия</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/bytovaya-ximiya-i-kosmetika/dekorativnaya-kosmetika/" data-id="15894" data-products-count="671">Декоративная косметика</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/bytovaya-ximiya-i-kosmetika/lichnyj-uxod-i-gigiena/" data-id="15940" data-products-count="3776">Личный уход и гигиена</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/bytovaya-ximiya-i-kosmetika/galantereya/" data-id="15884" data-products-count="731">Галантерея</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/bytovaya-ximiya-i-kosmetika/domashnee-xozyajstvo/" data-id="15929" data-products-count="558">Домашнее хозяйство</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/napolnye_vesy/" data-id="174">Напольные весы&nbsp;<span>257</span></a></li>
                                                    <li><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/gidromassazhnye_vanny/" data-id="181">Гидромассажные ванны&nbsp;<span>9</span></a></li>
                                                    <li><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/manikyurnye_nabory/" data-id="182">Маникюрные наборы&nbsp;<span>47</span></a></li>
                                                </ul>
                                            </div>
                                            <div data-id="12769" data-items-per-column="10">
                                                <ul>
                                                    <li><a href="/office-seti/ofisnaya_tehnika/" data-id="801">Офисная техника&nbsp;<span>340</span></a>
                                                        <div data-id="801">
                                                            <ul>
                                                                <li class="leaf"><a href="/office-seti/ofisnaya_tehnika/schreder/" data-id="4061" data-products-count="206">Уничтожители бумаги</a></li>
                                                                <li class="leaf"><a href="/office-seti/ofisnaya_tehnika/laminatory/" data-id="4062" data-products-count="62">Ламинаторы</a></li>
                                                                <li class="leaf"><a href="/office-seti/ofisnaya_tehnika/broshuratory/" data-id="4063" data-products-count="39">Брошюровщики</a></li>
                                                                <li class="leaf"><a href="/office-seti/ofisnaya_tehnika/rezaky/" data-id="15495" data-products-count="25">Резаки</a></li>
                                                                <li><a href="/office-seti/ofisnaya_tehnika/">Все офисная техника&nbsp;<span>340</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/office-seti/kancelyarskie-tovari/" data-id="12381">Канцелярские товары&nbsp;<span>7315</span></a>
                                                        <div data-id="12381">
                                                            <ul>
                                                                <li class="leaf"><a href="/office-seti/kancelyarskie-tovari/magnitno_markernie_doski/" data-id="638" data-products-count="33">Магнитно маркерные доски</a></li>
                                                                <li class="leaf"><a href="/office-seti/kancelyarskie-tovari/kartoteki_podvesnaya_registratura/" data-id="667" data-products-count="37">Картотеки, подвесная регистратура</a></li>
                                                                <li class="leaf"><a href="/office-seti/kancelyarskie-tovari/biznes-suveniri/" data-id="13568" data-products-count="118">Бизнес сувениры</a></li>
                                                                <li class="leaf"><a href="/office-seti/kancelyarskie-tovari/izdelie-iz-bumagi/" data-id="12382" data-products-count="1240">Изделия из бумаги</a></li>
                                                                <li class="leaf"><a href="/office-seti/kancelyarskie-tovari/nastolnie-prinadlejnosti/" data-id="12387" data-products-count="1274">Настольные принадлежности</a></li>
                                                                <li class="leaf"><a href="/office-seti/kancelyarskie-tovari/papki-faili-sistemi-arhivacii/" data-id="12445" data-products-count="932">Папки, файлы, системы архивации</a></li>
                                                                <li class="leaf"><a href="/office-seti/kancelyarskie-tovari/pishushie-prinadlejnosti/" data-id="12458" data-products-count="3285">Пишущие принадлежности</a></li>
                                                                <li class="leaf"><a href="/office-seti/kancelyarskie-tovari/tablichki/" data-id="12473" data-products-count="201">Таблички</a></li>
                                                                <li class="leaf"><a href="/office-seti/kancelyarskie-tovari/kraski/" data-id="14138" data-products-count="178">Краски и кисти</a></li>
                                                                <li><a href="/office-seti/kancelyarskie-tovari/">Все канцелярские товары&nbsp;<span>7315</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/office-seti/rfid-oborudovanie/" data-id="15260">RFID-оборудование&nbsp;<span>18</span></a>
                                                        <div data-id="15260">
                                                            <ul>
                                                                <li><a href="/office-seti/rfid-oborudovanie/">Все rfid-оборудование&nbsp;<span>18</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/office-seti/audio-videokonferencii-i-kongress-oborudovanie/" data-id="15733">Аудио-видеоконференции и конгресс оборудование&nbsp;<span>11</span></a></li>
                                                    <li><a href="/office-seti/kamery_videonablyudeniya/" data-id="101">Камеры видеонаблюдения&nbsp;<span>150</span></a></li>
                                                    <li><a href="/office-seti/stoli_komputernie/" data-id="11496">Офисная мебель&nbsp;<span>138</span></a></li>
                                                    <li><a href="/office-seti/calkulyatori/" data-id="11804">Калькуляторы&nbsp;<span>141</span></a></li>
                                                    <li><a href="/office-seti/aksessuari_dlya_videonabludeniya/" data-id="11469">Аксессуары для видеонаблюдения&nbsp;<span>156</span></a></li>
                                                </ul>
                                            </div>
                                            <div data-id="701" data-items-per-column="3">
                                                <ul>
                                                    <li><a href="/chay_kofe/chay/" data-id="702">Чай&nbsp;<span>88</span></a>
                                                        <div data-id="702">
                                                            <ul>
                                                                <li class="leaf"><a href="/chay_kofe/chay/cherniy_chay/" data-id="705" data-products-count="56">Черный чай</a></li>
                                                                <li class="leaf"><a href="/chay_kofe/chay/aromatizirovanniy_chay/" data-id="706" data-products-count="32">Ароматизированный чай</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/chay_kofe/kofe/" data-id="703">Кофе&nbsp;<span>185</span></a>
                                                        <div data-id="703">
                                                            <ul>
                                                                <li class="leaf"><a href="/chay_kofe/kofe/kofe_v_zernah/" data-id="707" data-products-count="98">Кофе в зёрнах</a></li>
                                                                <li class="leaf"><a href="/chay_kofe/kofe/kofe_v_kapsulah_i_chaldah/" data-id="708" data-products-count="56">Кофе в капсулах и чалдах</a></li>
                                                                <li class="leaf"><a href="/chay_kofe/kofe/kofe_molotiy/" data-id="709" data-products-count="29">Кофе молотый</a></li>
                                                                <li><a href="/chay_kofe/kofe/">Все кофе&nbsp;<span>185</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/chay_kofe/kakao/" data-id="712">Какао&nbsp;<span>11</span></a></li>
                                                </ul>
                                            </div>
                                            <div data-id="12341" data-items-per-column="4">
                                                <ul>
                                                    <li><a href="/uslugi/diagnostika-oborudovaniya/" data-id="14936">Диагностика оборудования, сборка, модернизация&nbsp;<span>26</span></a></li>
                                                    <li><a href="/uslugi/nakleika-plenok/" data-id="14943">Наклейка пленок&nbsp;<span>2</span></a></li>
                                                    <li><a href="/uslugi/proverka-na-bitie-pixely/" data-id="14935">Проверка на битые пиксели&nbsp;<span>6</span></a></li>
                                                    <li><a href="/uslugi/rabota-s-nasitelyami-informacii/" data-id="14937">Работа с носителями информации&nbsp;<span>4</span></a></li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/uslugi/ustanovka-paketov-programm/" data-id="14938">Установка пакетов программ&nbsp;<span>6</span></a>
                                                        <div data-id="14938">
                                                            <ul>
                                                                <li><a href="/uslugi/ustanovka-paketov-programm/">Все установка пакетов программ&nbsp;<span>6</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/uslugi/ustanovka-po/" data-id="14934">Установка ПО&nbsp;<span>11</span></a></li>
                                                    <li><a href="/uslugi/proche-uslugy/" data-id="14939">Прочее&nbsp;<span>9</span></a></li>
                                                </ul>
                                            </div>
                                            <div data-id="15394" data-items-per-column="6">
                                                <ul>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_kompyutery_noutbuki_soft/" data-id="15436">Уценка: Компьютеры, ноутбуки, софт&nbsp;<span>9</span></a></li>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_kids/" data-id="15453">Уценка: Товары для детей&nbsp;<span>47</span></a></li>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_komputernaya_periferiya/" data-id="15454">Уценка: Компьютерная периферия&nbsp;<span>19</span></a></li>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_komplektuyuschie_dlya_pk/" data-id="15455">Уценка: Комплектующие для ПК&nbsp;<span>70</span></a></li>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_apple/" data-id="15456">Уценка: Продукция Apple&nbsp;<span>3</span></a></li>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_telefoniya/" data-id="15457">Уценка: Смартфоны и гаджеты&nbsp;<span>13</span></a></li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_audio-video_i_tv/" data-id="15458">Уценка: Телевизоры и видео&nbsp;<span>2</span></a></li>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_audio-technika/" data-id="15459">Уценка: Аудио техника&nbsp;<span>1</span></a></li>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_office-seti/" data-id="15461">Уценка: Офис и сети&nbsp;<span>31</span></a></li>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_bytovaya_tehnika/" data-id="15462">Уценка: Товары и техника для дома&nbsp;<span>26</span></a></li>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_klimaticheskaya_tehnika/" data-id="15464">Уценка: Климатическая техника&nbsp;<span>9</span></a></li>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_krupnaya-bitovaya-tehnika/" data-id="15466">Уценка: Крупная бытовая техника&nbsp;<span>2</span></a></li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_bytovaya_texnika_vstraivaemaya/" data-id="15467">Уценка: Бытовая техника встраиваемая&nbsp;<span>2</span></a></li>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_car-electronics/" data-id="15469">Уценка: Всё для авто&nbsp;<span>10</span></a></li>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_instrumenti_sad_ogorod/" data-id="15471">Уценка: Инструменты.Сад.Огород&nbsp;<span>4</span></a></li>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_injenernaya-santehnika/" data-id="15472">Уценка: Инженерная сантехника&nbsp;<span>2</span></a></li>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_neremont/" data-id="15473">Уценка: Неремонтопригодные товары&nbsp;<span>78</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="bottom-main-header">
                            <div class="srch-city">
                                <div class="wrapp-search-field">
                                    <form action="/search/" method="get">
                                        <label for="searchinput"></label>
                                        <input class="input-base" id="searchinput" name="q" type="text" placeholder="Поиск по 164 082 товарам в наличии">
                                        <div class="srch-city__link">
                                            <ul>
                                                <li><a href="/kompyutery_noutbuki_soft/notebooks/">ноутбуки</a></li>
                                                <li><a href="/komplektuyuschie_dlya_pk/videokarti/">видеокарты</a></li>
                                                <li><a href="/komplektuyuschie_dlya_pk/materinskie_platy/">материнские платы</a></li>
                                                <li><a href="/kompyutery_noutbuki_soft/monitory/">мониторы</a></li>
                                                <li><a href="/communication-equipment/besprovodnoe_setevoe_oborudovanie/tochki_dosutpa/">WI-FI – роутеры</a></li>
                                                <li class="last"><a href="/komputernaya_periferiya/zaschita_pitaniya/istochniki_bespereboynogo_pitaniya/">ИБП</a></li>
                                            </ul>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="cart-camp">
                                <div class="compare"><a href="#" data-window="#empty-compare" data-position="right" data-autofocus="true" data-background="true">Сравнение</a><span class="count"></span></div>
                                <div class="favorites"><a href="#" data-window="#login" data-position="right" data-autofocus="true" data-background="true" data-popuptitle="Авторизуйтесь и добавьте товар в избранное для отслеживания">Избранное</a><span class="count"></span></div>
                                <div class="add-to-cart empty cart-box">
                                    <a href="/ordering/" class="standart-btn" id="cartlink">
                                        <div class="icon-cart">
                                            <svg width="20px" height="16px" viewBox="0 0 20 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                <defs></defs>
                                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <g id="Group" transform="translate(1.000000, 1.000000)" stroke="#2673C2" stroke-width="2">
                                                        <path d="M11.5,0.5 L13.5,3.5" id="Line" stroke-linecap="square"></path>
                                                        <path d="M4.5,0.5 L6.5,3.5" id="Line" stroke-linecap="square" transform="translate(5.500000, 2.000000) scale(-1, 1) translate(-5.500000, -2.000000) "></path>
                                                        <path d="M0.5,4 L17.5,4 C17.5,6.66666667 17.1666667,8.83333333 16.5,10.5 C15.8333333,12.1666667 15.1666667,13.3333333 14.5,14 L3.4875078,14 C2.99319543,13.7499048 2.33069283,12.5832381 1.5,10.5 C0.669307173,8.41676188 0.33597384,6.25009521 0.5,4 Z" id="Path-2"></path>
                                                    </g>
                                                </g>
                                            </svg><span class="global-count-cart"></span></div><span class="cart-cash">0<span class="currency"> р.</span></span>
                                    </a>
                                    <div class="modal-add-position">
                                        <div class="header"><span>Корзина</span>&nbsp;<span class="c-gray">0 товаров</span><a href="#" data-action="close"><i class="material-icons">close</i></a></div>
                                        <div class="body">
                                            <div class="window-cart-items"></div>
                                            <div class="total"><span>Всего товаров на сумму:</span>
                                                <div class="price"><span>0</span><span class="cur">р.</span></div>
                                            </div>
                                        </div>
                                        <div class="bottom"><a href="https://www.123.ru/ordering/" class="btn">оформить заказ</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mobile__header">
                            <div class="btn__mob-menu">
                                <div class="wrapp-hamb"><span></span></div>
                            </div>
                            <div class="img">
                                <a href="/"><img src="/i/main_logo.svg" height="40px"></a>
                            </div>
                            <div class="search-cart">
                                <div class="btn_search"><i class="material-icons">search</i></div>
                                <div class="btn__cart">
                                    <a href="/ordering/">
                                        <svg width="25px" height="21px" viewBox="0 0 20 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                            <defs></defs>
                                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <g id="Group" transform="translate(1.000000, 1.000000)" stroke="#797979" stroke-width="2">
                                                    <path d="M11.5,0.5 L13.5,3.5" id="Line" stroke-linecap="square"></path>
                                                    <path d="M4.5,0.5 L6.5,3.5" id="Line" stroke-linecap="square" transform="translate(5.500000, 2.000000) scale(-1, 1) translate(-5.500000, -2.000000) "></path>
                                                    <path d="M0.5,4 L17.5,4 C17.5,6.66666667 17.1666667,8.83333333 16.5,10.5 C15.8333333,12.1666667 15.1666667,13.3333333 14.5,14 L3.4875078,14 C2.99319543,13.7499048 2.33069283,12.5832381 1.5,10.5 C0.669307173,8.41676188 0.33597384,6.25009521 0.5,4 Z" id="Path-2"></path>
                                                </g>
                                            </g>
                                        </svg><span class="global_count">0</span></a>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div class="mobile-site-menu">
            <ul>
                <li><a href="#" data-window="#cities" data-background="true" data-responsive="true" class="link-sel w-a" id="#citysel">Ваш город: Москва</a></li>
                <li>
                    <div class="callback-mob"><a class="tel" href="tel:8 (495) 225-9-123">8 (495) 225-9-123</a><a href="#" class="call_back" data-window="#callback" data-background="true" data-position="right" data-autofocus="true">Обратный звонок</a></div>
                </li>
                <li><a href="#" data-window="#login" data-position="right" data-autofocus="true" data-background="true" class="link-sel w w-a w-cab">Вход в личный кабинет</a></li>
                <li class="catalog"><a class="w w-a w-cat" href="/catalog/">Каталог</a></li>
                <li><a class="w w-a w-camp" href="#">Сравнение </a></li>
                <li><a class="w w-a w-fav" href="#" data-window="#login" data-position="right" data-autofocus="true" data-background="true" data-popuptitle="Авторизуйтесь и добавьте товар в избранное для отслеживания">Избранное </a></li>
                <li><a class="w" href="/about/contacts/">Контакты</a></li>
                <li><a class="w w-a w-d-d" href="#">Покупателям</a>
                    <ul class="submenu">
                        <li><a href="/about/delivery.php/">Доставка</a></li>
                        <li><a href="/pay/">Оплата</a></li>
                        <li><a href="/credit/">Покупка в кредит</a></li>
                        <li><a href="/sovest/">Рассрочка с картой Совесть</a></li>
                        <li><a href="/halva/">Рассрочка с картой Халва</a></li>
                    </ul>
                </li>
                <li><a class="w" href="/warranty/">Гарантия</a></li>
                <li><a class="w" href="/b2b/">Корпоративным клиентам</a></li>
                <li><a class="w" href="/supplier/">Поставщикам</a></li>
                <li><a class="w w-pay" href="/quick-pay/">Оплатить</a></li>
            </ul>
        </div>
    </header>
    <div class="header-delimiter"></div>
    <div class="search-box-new">
        <form class="form-search search-hidden" action="/search/" method="get">
            <label for="searchinput">Поиск</label>
            <input type="text" name="q" id="searchinput" placeholder=""><i class="icon icon-close"></i></form>
    </div>
    <div class="catalog-link-container"><a href="#" class="catalog-header-link">Каталог товаров</a></div>
    <div class="clearfix"></div>
    <section class="breadcrumbs container hidden-lg hidden-md hidden-sm -mob">
        <a href="#...">Смартфоны и гаджеты</a>
    </section>
    <section class="breadcrumbs container hidden-xs">
        <ul itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="http://red.123.ru/smartfoni_plansheti_gadjeti/" title="Смартфоны, планшеты, гаджеты"><span itemprop="name">Смартфоны, планшеты, гаджеты</span></a><i class="icon ic-arrow-left"></i>
                <meta itemprop="position" content="1" />
            </li>
            <li><span itemprop="name">Смартфоны</span></li>
        </ul>
    </section>
    <section class="container catalog-header bg-white-xs">
        <h1 data-full-content="Смартфоны&amp;nbsp;&lt;em&gt;958&lt;/em&gt;">Смартфоны</h1>
    </section>
    <section class="catalog-products container pb50 bg-white-xs" id="goods">
        <div class="-cat-flex-cont">
            <div class="column col-lg-20 col-md-20 col-sm-30 col-xs-100 hidden-xs bg-white">
                <div id="filter-sidebar">
                    <div class="filter-box only-mobile accordion noselect"><a href="#filter-orders" data-toggle="accordion" class="opener">Сортировка <i class="icon ic-arrow-down"></i></a>
                        <div id="filter-orders">
                            <ul class='clearstyle checkbox-group radio-mode'>
                                <li class="asc" data-link="?sort=popularity&dir=asc" data-sort="popularity" data-direction="asc">
                                    <input type="radio" name="order-type[]" value="popularity-asc" /><span class="title">по популярности</span></li>
                                <li class="asc" data-link="?sort=price&dir=asc" data-sort="price" data-direction="asc">
                                    <input type="radio" name="order-type[]" value="price-asc" /><span class="title">по цене</span></li>
                                <li class="desc" data-link="?sort=price&dir=desc" data-sort="price" data-direction="desc">
                                    <input type="radio" name="order-type[]" value="price-desc" /><span class="title">по цене</span></li>
                            </ul>
                        </div>
                    </div>
                    
                    <div class="-search-block">
                        <a href="#" class='-block-expander'>Расширенный поиск</a>
                        <div id="-search-section">
                            <aside class="groups">
                                <h2>Расширенный поиск</h2>
                                <h3><a href='#' class='-close-block'>Смартфоны (67)</a></h3>
                                <ul class="group-items">
                                    <li><a href="#" class='-scrl-to' data-to='x-bra'><span>Бренд</span><em>45</em></a></li>
                                    <li><a href="#" class='-scrl-to' data-to='x-sys'><span>Операционная система</span><em>116</em></a></li>
                                    <li><a href="#" class='-scrl-to' data-to='x-dia'><span>Диагональ экрана</span><em>25</em></a></li>
                                    <li><a href="#" class='-scrl-to' data-to='x-rat'><span>Рейтинг</span><em>125</em></a></li>
                                    <li><a href="#" class='-scrl-to' data-to='x-pri'><span>Цена</span><em>186</em></a></li>
                                    <li><a href="#" class='-scrl-to' data-to='x-di2'><span>Диагональ экрана</span><em>45</em></a></li>
                                    <li><a href="#" class='-scrl-to' data-to='x-res'><span>Разрешение экрана</span><em>23</em></a></li>
                                    <li><a href="#" class='-scrl-to' data-to='x-ram'><span>Оперативная память</span><em>41</em></a></li>
                                    <li><a href="#" class='-scrl-to' data-to='x-rom'><span>Встроенная память</span><em>54</em></a></li>
                                    <li><a href="#" class='-scrl-to' data-to='x-pow'><span>Емкость аккумулятора</span><em>5</em></a></li>
                                    <li><a href="#" class='-scrl-to' data-to='x-pos'><span>Возможности</span><em>353</em></a></li>
                                    <li><a href="#" class='-scrl-to' data-to='x-del'><span>Доставка</span><em>25</em></a></li>
                                </ul>
                                <div class="-footer">
                                    <a href='#' class='-apply'>подобрать</a>
                                </div>
                            </aside>
                            <aside class="list">
                                <div class="-block" data-id='f-32901' data-target='x-bra'>
                                    <h5>Бренд <a href='#' class='-reset'>Сбросить</a></h5>
                                    <ul>
                                        <li data-val='1'><span>LG</span><em>151</em></li>
                                        <li data-val='2' class="checked"><span>Apple</span><em>142</em></li>
                                        <li data-val='3'><span>Samsung</span><em>109</em></li>
                                        <li data-val='4'><span>Xiaomi</span><em>108</em></li>
                                        <li data-val='5'><span>Meizu</span><em>37</em></li>
                                        <li data-val='6'><span>SONY</span><em>33</em></li>
                                        <li data-val='7'><span>Lenovo</span><em>33</em></li>
                                        <li data-val='8'><span>ASUS</span><em>30</em></li>
                                        <li data-val='9'><span>Alcatel</span><em>30</em></li>
                                        <li data-val='10'><span>ARK</span><em>11</em></li>
                                        <li data-val='11'><span>Black Fox</span><em>2</em></li>
                                        <li data-val='12'><span>BlackBerry</span><em>1</em></li>
                                        <li data-val='13'><span>BQ</span><em>27</em></li>
                                        <li data-val='14'><span>Caterpillar</span><em>1</em></li>
                                        <li data-val='15'><span>Digma</span><em>3</em></li>
                                        <li data-val='16'><span>Doogee</span><em>30</em></li>
                                        <li data-val='17'><span>Elephone</span><em>2</em></li>
                                        <li data-val='18'><span>Fly</span><em>14</em></li>
                                        <li data-val='19'><span>Honor</span><em>16</em></li>
                                        <li data-val='20'><span>HTC</span><em>14</em></li>
                                        <li data-val='21'><span>Huawei</span><em>21</em></li>
                                        <li data-val='22'><span>Irbis</span><em>1</em></li>
                                        <li data-val='23'><span>Jinga</span><em>5</em></li>
                                        <li data-val='24'><span>KENEKSI</span><em>2</em></li>
                                    </ul>
                                </div>
                                <div class="-block" data-id='f-32902' data-target='x-sys'>
                                    <h5>Операционная система <a href='#' class='-reset hidden'>Сбросить</a></h5>
                                    <ul>
                                        <li data-val='1'><span>Android 6</span><em>51</em></li>
                                        <li data-val='2'><span>Android 7</span><em>12</em></li>
                                        <li data-val='3'><span>Android 8</span><em>209</em></li>
                                        <li data-val='4'><span>iOS</span><em>32</em></li>
                                        <li data-val='5'><span>Windows Phone</span><em>37</em></li>
                                    </ul>
                                </div>
                                <div class="-block" data-id='f-32901' data-target='x-dia'>
                                    <h5>Диагональ экрана <a href='#' class='-reset'>Сбросить</a></h5>
                                    <ul>
                                        <li data-val='1' class="checked"><span>3"</span><em>151</em></li>
                                        <li data-val='2'><span>3.5"</span><em>142</em></li>
                                        <li data-val='3'><span>4"</span><em>109</em></li>
                                        <li data-val='4'><span>4.5"</span><em>108</em></li>
                                        <li data-val='5'><span>5"</span><em>37</em></li>
                                        <li data-val='6'><span>5.5"</span><em>33</em></li>
                                        <li data-val='7'><span>6"</span><em>33</em></li>
                                        <li data-val='8'><span>6.5"</span><em>30</em></li>
                                        <li data-val='9'><span>7"</span><em>30</em></li>
                                    </ul>
                                </div>

                                <div class="-block" data-id='f-32901' data-target='x-rat'>
                                    <h5>Рейтинг <a href='#' class='-reset hidden'>Сбросить</a></h5>
                                    <ul>
                                        <li data-val='1'><span><i class="-stars -l1"></i></span><em>151</em></li>
                                        <li data-val='2'><span><i class="-stars -l2"></i></span><em>51</em></li>
                                        <li data-val='3'><span><i class="-stars -l3"></i></span><em>1</em></li>
                                        <li data-val='4'><span><i class="-stars -l4"></i></span><em>51</em></li>
                                        <li data-val='5'><span><i class="-stars -l5"></i></span><em>151</em></li>
                                    </ul>
                                </div>

                                <div class="-block" data-id='f-32901' data-target='x-pri'>
                                    <h5>Цена <a href='#' class='-reset hidden'>Сбросить</a></h5>
                                    <ul>
                                        <li data-val='1'><span>до 5000 <i class="rub">a</i></span><em>151</em></li>
                                        <li data-val='2'><span>5000–10000 <i class="rub">a</i></span><em>142</em></li>
                                        <li data-val='3'><span>10000–15000 <i class="rub">a</i></span><em>109</em></li>
                                        <li data-val='4'><span>15000–20000 <i class="rub">a</i></span><em>108</em></li>
                                        <li data-val='5'><span>20000–25000 <i class="rub">a</i></span><em>37</em></li>
                                        <li data-val='6'><span>25000–30000 <i class="rub">a</i></span><em>33</em></li>
                                        <li data-val='7'><span>30000–35000 <i class="rub">a</i></span><em>33</em></li>
                                        <li data-val='8'><span>35000–50000 <i class="rub">a</i></span><em>30</em></li>
                                        <li data-val='9'><span>50000–70000 <i class="rub">a</i></span><em>30</em></li>
                                        <li data-val='10'><span>70000–100000 <i class="rub">a</i></span><em>11</em></li>
                                        <li data-val='11'><span>от 100000 <i class="rub">a</i></span><em>2</em></li>
                                    </ul>
                                </div>

                            </aside>
                        </div>
                    </div>
                    
                    <div class="filter-box accordion noselect"><a href="#filter-39411" data-toggle="accordion" class="opener collapsed">Цена <i class="icon ic-arrow-down"></i></a>
                        <div id="filter-39411" class="spoiler collapsed">
                            <ul class="clearstyle checkbox-group">
                                <li class=""><input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/operacionnaya_sistema-android/" name="prop_39411_9515" id="prop_39411_9515" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/operacionnaya_sistema-android/">до 10000 <i class="rub">a</i></a></span>&nbsp;<span class="count">(23)</span></li>
                                <li class=""><input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/operacionnaya_sistema-android/" name="prop_39411_9515" id="prop_39411_9515" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/operacionnaya_sistema-android/">10000–20000 <i class="rub">a</i></a></span>&nbsp;<span class="count">(112)</span></li>
                                <li class=""><input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/operacionnaya_sistema-android/" name="prop_39411_9515" id="prop_39411_9515" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/operacionnaya_sistema-android/">20000–40000 <i class="rub">a</i></a></span>&nbsp;<span class="count">(54)</span></li>
                                <li class=""><input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/operacionnaya_sistema-android/" name="prop_39411_9515" id="prop_39411_9515" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/operacionnaya_sistema-android/">40000–100000 <i class="rub">a</i></a></span>&nbsp;<span class="count">(7)</span></li>

                                <li class=""><input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/operacionnaya_sistema-android/" name="prop_39411_9515" id="prop_39411_9515" />
                                    <div id="filter-price">
                                        <div class="range on-apply-button" data-min="2340" data-max="200000" data-start="2340" data-finish="200000" query-pattern="?pr=price-from:price-to" data-name="price"></div>
                                        <div class="slide-hint" style="display: none;">Найдено товаров: <span>1024</span></div>
                                        <div class="mobile-select-block">
                                            <div class="values">
                                                <input type="number" class="mobile-min" placeholder="От">
                                                <input type="number" class="mobile-max" placeholder="До">
                                            </div>
                                            <ul class="clearstyle checkbox-group radio-mode">
                                                <li>
                                                    <input type="radio" name="price" value="2340,51000" /><span class="title">2 340&nbsp;&#151; 51 000 р.</span></li>
                                                <li>
                                                    <input type="radio" name="price" value="51000,100000" /><span class="title">51 000&nbsp;&#151; 100 000 р.</span></li>
                                                <li>
                                                    <input type="radio" name="price" value="100000,149000" /><span class="title">100 000&nbsp;&#151; 149 000 р.</span></li>
                                                <li>
                                                    <input type="radio" name="price" value="149000,200000" /><span class="title">&gt;  149 000 р.</span></li>
                                            </ul>
                                        </div>
                                        <div class="-apply-element">
                                            <a href="#">применить</a>
                                        </div>
                                    </div>
                                </li>

                            </ul>
                            <div class='filter-footer'></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    
<!--                    
                    <div class="filter-box accordion noselect"><a href="#filter-price" data-toggle="accordion" class="opener">Цена, р.<i class="icon ic-arrow-down"></i></a>
                        <div id="filter-price">
                            <div class="range" data-min="2340" data-max="200000" data-start="2340" data-finish="200000" query-pattern="?pr=price-from:price-to" data-name="price"></div>
                            <div class="slide-hint" style="display: none;">Найдено товаров: <span>1024</span></div>
                            <div class="mobile-select-block">
                                <div class="values">
                                    <input type="number" class="mobile-min" placeholder="От">
                                    <input type="number" class="mobile-max" placeholder="До">
                                </div>
                                <ul class="clearstyle checkbox-group radio-mode">
                                    <li>
                                        <input type="radio" name="price" value="2340,51000" /><span class="title">2 340&nbsp;&#151; 51 000 р.</span></li>
                                    <li>
                                        <input type="radio" name="price" value="51000,100000" /><span class="title">51 000&nbsp;&#151; 100 000 р.</span></li>
                                    <li>
                                        <input type="radio" name="price" value="100000,149000" /><span class="title">100 000&nbsp;&#151; 149 000 р.</span></li>
                                    <li>
                                        <input type="radio" name="price" value="149000,200000" /><span class="title">&gt;  149 000 р.</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
-->
                    <div class="filter-box accordion noselect"><a href="#filter-13483" data-toggle="accordion" class="opener">Бренд <i class="icon ic-arrow-down"></i></a>
                        <div id="filter-13483" class="spoiler">
                            <ul class="clearstyle checkbox-group">
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-lg/" name="prop_13483_80039" id="prop_13483_80039" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-lg/">LG</a></span>&nbsp;<span class="count">(151)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-apple/" name="prop_13483_79192" id="prop_13483_79192" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-apple/">Apple</a></span>&nbsp;<span class="count">(142)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-samsung/" name="prop_13483_80486" id="prop_13483_80486" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-samsung/">Samsung</a></span>&nbsp;<span class="count">(109)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-xiaomi/" name="prop_13483_149980" id="prop_13483_149980" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-xiaomi/">Xiaomi</a></span>&nbsp;<span class="count">(108)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-meizu/" name="prop_13483_80132" id="prop_13483_80132" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-meizu/">Meizu</a></span>&nbsp;<span class="count">(37)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-sony/" name="prop_13483_80576" id="prop_13483_80576" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-sony/">SONY</a></span>&nbsp;<span class="count">(33)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-lenovo/" name="prop_13483_80027" id="prop_13483_80027" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-lenovo/">Lenovo</a></span>&nbsp;<span class="count">(33)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-asus/" name="prop_13483_79221" id="prop_13483_79221" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-asus/">ASUS</a></span>&nbsp;<span class="count">(30)</span></li>
                            </ul>
                            <ul class="clearstyle checkbox-group spoiler collapsed __showMore">
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-alcatel/" name="prop_13483_79158" id="prop_13483_79158" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-alcatel/">Alcatel</a></span>&nbsp;<span class="count">(30)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-ark/" name="prop_13483_79206" id="prop_13483_79206" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-ark/">ARK</a></span>&nbsp;<span class="count">(11)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-black_fox/" name="prop_13483_168660" id="prop_13483_168660" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-black_fox/">Black Fox</a></span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-blackberry/" name="prop_13483_79299" id="prop_13483_79299" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-blackberry/">BlackBerry</a></span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-bq/" name="prop_13483_97950" id="prop_13483_97950" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-bq/">BQ</a></span>&nbsp;<span class="count">(27)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-caterpillar/" name="prop_13483_79366" id="prop_13483_79366" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-caterpillar/">Caterpillar</a></span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-digma/" name="prop_13483_79504" id="prop_13483_79504" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-digma/">Digma</a></span>&nbsp;<span class="count">(3)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-doogee/" name="prop_13483_230413" id="prop_13483_230413" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-doogee/">Doogee</a></span>&nbsp;<span class="count">(30)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-elephone/" name="prop_13483_232879" id="prop_13483_232879" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-elephone/">Elephone</a></span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-fly/" name="prop_13483_79655" id="prop_13483_79655" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-fly/">Fly</a></span>&nbsp;<span class="count">(14)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-honor/" name="prop_13483_279426" id="prop_13483_279426" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-honor/">Honor</a></span>&nbsp;<span class="count">(16)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-htc/" name="prop_13483_79815" id="prop_13483_79815" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-htc/">HTC</a></span>&nbsp;<span class="count">(14)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-huawei/" name="prop_13483_79816" id="prop_13483_79816" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-huawei/">Huawei</a></span>&nbsp;<span class="count">(21)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-irbis/" name="prop_13483_79897" id="prop_13483_79897" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-irbis/">Irbis</a></span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-jinga/" name="prop_13483_208023" id="prop_13483_208023" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-jinga/">Jinga</a></span>&nbsp;<span class="count">(5)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-keneksi/" name="prop_13483_89779" id="prop_13483_89779" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-keneksi/">KENEKSI</a></span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-micromax/" name="prop_13483_197816" id="prop_13483_197816" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-micromax/">Micromax</a></span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-motorola/" name="prop_13483_80192" id="prop_13483_80192" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-motorola/">Motorola</a></span>&nbsp;<span class="count">(13)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-neffos/" name="prop_13483_210521" id="prop_13483_210521" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-neffos/">Neffos</a></span>&nbsp;<span class="count">(7)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-nokia/" name="prop_13483_80245" id="prop_13483_80245" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-nokia/">NOKIA</a></span>&nbsp;<span class="count">(11)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-oneplus/" name="prop_13483_231941" id="prop_13483_231941" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-oneplus/">OnePlus</a></span>&nbsp;<span class="count">(6)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-oppo/" name="prop_13483_80282" id="prop_13483_80282" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-oppo/">Oppo</a></span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-philips/" name="prop_13483_80331" id="prop_13483_80331" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-philips/">Philips</a></span>&nbsp;<span class="count">(19)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-prestigio/" name="prop_13483_80382" id="prop_13483_80382" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-prestigio/">Prestigio</a></span>&nbsp;<span class="count">(12)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-texet/" name="prop_13483_80686" id="prop_13483_80686" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-texet/">Texet</a></span>&nbsp;<span class="count">(4)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-vertex/" name="prop_13483_80799" id="prop_13483_80799" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-vertex/">Vertex</a></span>&nbsp;<span class="count">(28)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-vivo/" name="prop_13483_80833" id="prop_13483_80833" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-vivo/">Vivo</a></span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-wileyfox/" name="prop_13483_281282" id="prop_13483_281282" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-wileyfox/">Wileyfox</a></span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-yezz/" name="prop_13483_214285" id="prop_13483_214285" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-yezz/">Yezz</a></span>&nbsp;<span class="count">(6)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/brand-zte/" name="prop_13483_80932" id="prop_13483_80932" /><span class="title"><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-zte/">ZTE</a></span>&nbsp;<span class="count">(24)</span></li>
                            </ul><div class='filter-footer'><a href="#" class="more collapsed" data-toggle="showmore" data-class="__showMore" data-endtext="свернуть">показать еще</a></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="filter-box accordion noselect"><a href="#filter-3941112" data-toggle="accordion" class="opener collapsed">Рейтинг <i class="icon ic-arrow-down"></i></a>
                        <div id="filter-3941112" class="spoiler collapsed">
                            <ul class="clearstyle checkbox-group">
                                <li class=""><input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/operacionnaya_sistema-android/" name="prop_39411_9515" id="prop_39411_9515" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/operacionnaya_sistema-android/"><em class='-stars -l1'></em></a></span>&nbsp;<span class="count">(123)</span></li>
                                <li class=""><input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/operacionnaya_sistema-android/" name="prop_39411_9515" id="prop_39411_9515" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/operacionnaya_sistema-android/"><em class='-stars -l2'></em></a></span>&nbsp;<span class="count">(75)</span></li>
                                <li class=""><input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/operacionnaya_sistema-android/" name="prop_39411_9515" id="prop_39411_9515" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/operacionnaya_sistema-android/"><em class='-stars -l3'></em></a></span>&nbsp;<span class="count">(32)</span></li>
                                <li class=""><input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/operacionnaya_sistema-android/" name="prop_39411_9515" id="prop_39411_9515" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/operacionnaya_sistema-android/"><em class='-stars -l4'></em></a></span>&nbsp;<span class="count">(115)</span></li>
                                <li class=""><input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/operacionnaya_sistema-android/" name="prop_39411_9515" id="prop_39411_9515" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/operacionnaya_sistema-android/"><em class='-stars -l5'></em></a></span>&nbsp;<span class="count">(453)</span></li>
                            </ul>
                            <div class='filter-footer'></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="filter-box accordion noselect"><a href="#filter-3941" data-toggle="accordion" class="opener collapsed">Операционная система <i class="icon ic-arrow-down"></i></a>
                        <div id="filter-3941" class="spoiler collapsed">
                            <ul class="clearstyle checkbox-group">
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/operacionnaya_sistema-android/" name="prop_3941_9515" id="prop_3941_9515" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/operacionnaya_sistema-android/">Android</a></span>&nbsp;<span class="count">(800)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/operacionnaya_sistema-apple_ios/" name="prop_3941_9517" id="prop_3941_9517" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/operacionnaya_sistema-apple_ios/">Apple iOS</a></span>&nbsp;<span class="count">(143)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=3941:206073" name="prop_3941_206073" id="prop_3941_206073" /><span class="title">Cyanogen</span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=3941:206371" name="prop_3941_206371" id="prop_3941_206371" /><span class="title">Ubuntu</span>&nbsp;<span class="count">(1)</span></li>
                            </ul>
                            <div class='filter-footer'></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="filter-box accordion noselect"><a href="#filter-1685" data-toggle="accordion" class="opener collapsed">Диагональ экрана <i class="icon ic-arrow-down"></i></a>
                        <div id="filter-1685" class="spoiler collapsed">
                            <ul class="clearstyle checkbox-group">
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-4/" name="prop_1685_195041" id="prop_1685_195041" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-4/">4"</a></span>&nbsp;<span class="count">(36)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-4_5/" name="prop_1685_195185" id="prop_1685_195185" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-4_5/">4.5"</a></span>&nbsp;<span class="count">(17)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-4_6/" name="prop_1685_195203" id="prop_1685_195203" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-4_6/">4.6"</a></span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-4_7/" name="prop_1685_195222" id="prop_1685_195222" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-4_7/">4.7"</a></span>&nbsp;<span class="count">(50)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5/" name="prop_1685_195040" id="prop_1685_195040" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5/">5"</a></span>&nbsp;<span class="count">(190)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_1/" name="prop_1685_195303" id="prop_1685_195303" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_1/">5.1"</a></span>&nbsp;<span class="count">(4)</span></li>
                            </ul>
                            <ul class="clearstyle checkbox-group spoiler collapsed __showMore">
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_2/" name="prop_1685_195197" id="prop_1685_195197" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_2/">5.2"</a></span>&nbsp;<span class="count">(53)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_3/" name="prop_1685_195215" id="prop_1685_195215" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_3/">5.3"</a></span>&nbsp;<span class="count">(15)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_46/" name="prop_1685_195446" id="prop_1685_195446" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_46/">5.46"</a></span>&nbsp;<span class="count">(4)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_5/" name="prop_1685_195135" id="prop_1685_195135" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_5/">5.5"</a></span>&nbsp;<span class="count">(221)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_7/" name="prop_1685_195331" id="prop_1685_195331" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_7/">5.7"</a></span>&nbsp;<span class="count">(51)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-6/" name="prop_1685_195217" id="prop_1685_195217" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-6/">6"</a></span>&nbsp;<span class="count">(46)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-6_4/" name="prop_1685_195195" id="prop_1685_195195" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-6_4/">6.4"</a></span>&nbsp;<span class="count">(4)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-6_5/" name="prop_1685_195189" id="prop_1685_195189" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-6_5/">6.5"</a></span>&nbsp;<span class="count">(9)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-4_93/" name="prop_1685_211431" id="prop_1685_211431" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-4_93/">4.93"</a></span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_9/" name="prop_1685_220832" id="prop_1685_220832" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_9/">5.9"</a></span>&nbsp;<span class="count">(4)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-6_8/" name="prop_1685_223306" id="prop_1685_223306" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-6_8/">6.8"</a></span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_15/" name="prop_1685_223614" id="prop_1685_223614" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_15/">5.15"</a></span>&nbsp;<span class="count">(9)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-6_44/" name="prop_1685_225195" id="prop_1685_225195" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-6_44/">6.44"</a></span>&nbsp;<span class="count">(5)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-6_2/" name="prop_1685_231752" id="prop_1685_231752" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-6_2/">6.2"</a></span>&nbsp;<span class="count">(16)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_8/" name="prop_1685_231758" id="prop_1685_231758" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_8/">5.8"</a></span>&nbsp;<span class="count">(22)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-6_3/" name="prop_1685_240442" id="prop_1685_240442" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-6_3/">6.3"</a></span>&nbsp;<span class="count">(3)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_93/" name="prop_1685_245206" id="prop_1685_245206" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_93/">5.93"</a></span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_99/" name="prop_1685_245883" id="prop_1685_245883" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_99/">5.99"</a></span>&nbsp;<span class="count">(22)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_6/" name="prop_1685_246466" id="prop_1685_246466" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_6/">5.6"</a></span>&nbsp;<span class="count">(8)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_65/" name="prop_1685_248877" id="prop_1685_248877" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_65/">5.65"</a></span>&nbsp;<span class="count">(6)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_45/" name="prop_1685_260613" id="prop_1685_260613" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_45/">5.45"</a></span>&nbsp;<span class="count">(11)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_84/" name="prop_1685_261805" id="prop_1685_261805" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_84/">5.84"</a></span>&nbsp;<span class="count">(9)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-6_1/" name="prop_1685_261843" id="prop_1685_261843" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-6_1/">6.1"</a></span>&nbsp;<span class="count">(23)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_72/" name="prop_1685_263622" id="prop_1685_263622" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_72/">5.72"</a></span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_73/" name="prop_1685_263803" id="prop_1685_263803" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_73/">5.73"</a></span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-4_95/" name="prop_1685_265570" id="prop_1685_265570" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-4_95/">4.95"</a></span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_25/" name="prop_1685_270011" id="prop_1685_270011" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-5_25/">5.25"</a></span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-6_28/" name="prop_1685_277713" id="prop_1685_277713" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-6_28/">6.28"</a></span>&nbsp;<span class="count">(4)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-6_23/" name="prop_1685_281299" id="prop_1685_281299" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/diagonal_ekrana-6_23/">6,23"</a></span>&nbsp;<span class="count">(1)</span></li>
                            </ul><div class='filter-footer'><a href="#" class="more collapsed" data-toggle="showmore" data-class="__showMore" data-endtext="свернуть">показать еще</a></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="filter-box accordion noselect"><a href="#filter-20225" data-toggle="accordion" class="opener collapsed">Соотношение сторон <i class="icon ic-arrow-down"></i></a>
                        <div id="filter-20225" class="spoiler collapsed">
                            <ul class="clearstyle checkbox-group">
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=20225:261245" name="prop_20225_261245" id="prop_20225_261245" /><span class="title">16:9</span>&nbsp;<span class="count">(326)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=20225:263804" name="prop_20225_263804" id="prop_20225_263804" /><span class="title">17:9</span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=20225:261246" name="prop_20225_261246" id="prop_20225_261246" /><span class="title">18:9</span>&nbsp;<span class="count">(150)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=20225:266444" name="prop_20225_266444" id="prop_20225_266444" /><span class="title">18.5:9</span>&nbsp;<span class="count">(19)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=20225:261807" name="prop_20225_261807" id="prop_20225_261807" /><span class="title">19:9</span>&nbsp;<span class="count">(22)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=20225:294602" name="prop_20225_294602" id="prop_20225_294602" /><span class="title">19.5:9</span>&nbsp;<span class="count">(38)</span></li>
                            </ul>
                            <div class='filter-footer'></div><div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="filter-box accordion noselect"><a href="#filter-19986" data-toggle="accordion" class="opener collapsed">Безрамочный <i class="icon ic-arrow-down"></i></a>
                        <div id="filter-19986" class="spoiler collapsed">
                            <ul class="clearstyle checkbox-group">
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/bezramochniy-da/" name="prop_19986_246868" id="prop_19986_246868" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/bezramochniy-da/">да</a></span>&nbsp;<span class="count">(254)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19986:246869" name="prop_19986_246869" id="prop_19986_246869" /><span class="title">нет</span>&nbsp;<span class="count">(504)</span></li>
                            </ul>
                            <div class='filter-footer'></div><div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="filter-box accordion noselect"><a href="#filter-1686" data-toggle="accordion" class="opener collapsed">Разрешение экрана <i class="icon ic-arrow-down"></i></a>
                        <div id="filter-1686" class="spoiler collapsed">
                            <ul class="clearstyle checkbox-group">
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=1686:195043" name="prop_1686_195043" id="prop_1686_195043" /><span class="title">800x480</span>&nbsp;<span class="count">(18)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=1686:195044" name="prop_1686_195044" id="prop_1686_195044" /><span class="title">854x480</span>&nbsp;<span class="count">(47)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=1686:195045" name="prop_1686_195045" id="prop_1686_195045" /><span class="title">960x540</span>&nbsp;<span class="count">(9)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=1686:195046" name="prop_1686_195046" id="prop_1686_195046" /><span class="title">1136x640</span>&nbsp;<span class="count">(21)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=1686:195047" name="prop_1686_195047" id="prop_1686_195047" /><span class="title">1280x720</span>&nbsp;<span class="count">(218)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=1686:195048" name="prop_1686_195048" id="prop_1686_195048" /><span class="title">1334x750</span>&nbsp;<span class="count">(41)</span></li>
                            </ul>
                            <ul class="clearstyle checkbox-group spoiler collapsed __showMore">
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=1686:195049" name="prop_1686_195049" id="prop_1686_195049" /><span class="title">1920x1080</span>&nbsp;<span class="count">(204)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=1686:195050" name="prop_1686_195050" id="prop_1686_195050" /><span class="title">2560x1440</span>&nbsp;<span class="count">(26)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=1686:195051" name="prop_1686_195051" id="prop_1686_195051" /><span class="title">3840x2160</span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=1686:224652" name="prop_1686_224652" id="prop_1686_224652" /><span class="title">480 x 854</span>&nbsp;<span class="count">(3)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=1686:231753" name="prop_1686_231753" id="prop_1686_231753" /><span class="title">2960x1440</span>&nbsp;<span class="count">(23)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=1686:237310" name="prop_1686_237310" id="prop_1686_237310" /><span class="title">2040x1080</span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=1686:240692" name="prop_1686_240692" id="prop_1686_240692" /><span class="title">2436x1125</span>&nbsp;<span class="count">(12)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=1686:245207" name="prop_1686_245207" id="prop_1686_245207" /><span class="title">2160x1080</span>&nbsp;<span class="count">(63)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=1686:245646" name="prop_1686_245646" id="prop_1686_245646" /><span class="title">2880x1440</span>&nbsp;<span class="count">(14)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=1686:245884" name="prop_1686_245884" id="prop_1686_245884" /><span class="title">1440x720</span>&nbsp;<span class="count">(60)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=1686:246467" name="prop_1686_246467" id="prop_1686_246467" /><span class="title">2220x1080</span>&nbsp;<span class="count">(9)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=1686:247892" name="prop_1686_247892" id="prop_1686_247892" /><span class="title">1620x1080</span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=1686:260615" name="prop_1686_260615" id="prop_1686_260615" /><span class="title">960x480</span>&nbsp;<span class="count">(18)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=1686:260656" name="prop_1686_260656" id="prop_1686_260656" /><span class="title">1280x640</span>&nbsp;<span class="count">(5)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=1686:261806" name="prop_1686_261806" id="prop_1686_261806" /><span class="title">2280x1080</span>&nbsp;<span class="count">(14)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=1686:261833" name="prop_1686_261833" id="prop_1686_261833" /><span class="title">2244х1080</span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=1686:261844" name="prop_1686_261844" id="prop_1686_261844" /><span class="title">2240х1080</span>&nbsp;<span class="count">(3)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=1686:265719" name="prop_1686_265719" id="prop_1686_265719" /><span class="title">3120 х1440</span>&nbsp;<span class="count">(3)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=1686:266025" name="prop_1686_266025" id="prop_1686_266025" /><span class="title">1480 x720</span>&nbsp;<span class="count">(8)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=1686:270151" name="prop_1686_270151" id="prop_1686_270151" /><span class="title">2246x1080</span>&nbsp;<span class="count">(4)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=1686:294606" name="prop_1686_294606" id="prop_1686_294606" /><span class="title">2688x1242</span>&nbsp;<span class="count">(9)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=1686:295360" name="prop_1686_295360" id="prop_1686_295360" /><span class="title">1792x828</span>&nbsp;<span class="count">(17)</span></li>
                            </ul><div class='filter-footer'><a href="#" class="more collapsed" data-toggle="showmore" data-class="__showMore" data-endtext="свернуть">показать еще</a></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="filter-box accordion noselect"><a href="#filter-19985" data-toggle="accordion" class="opener collapsed">Технология изготовления экрана <i class="icon ic-arrow-down"></i></a>
                        <div id="filter-19985" class="spoiler collapsed">
                            <ul class="clearstyle checkbox-group">
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19985:246860" name="prop_19985_246860" id="prop_19985_246860" /><span class="title">TFT</span>&nbsp;<span class="count">(53)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19985:246861" name="prop_19985_246861" id="prop_19985_246861" /><span class="title">AMOLED</span>&nbsp;<span class="count">(66)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19985:246864" name="prop_19985_246864" id="prop_19985_246864" /><span class="title">IPS</span>&nbsp;<span class="count">(517)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19985:246865" name="prop_19985_246865" id="prop_19985_246865" /><span class="title">LTPS</span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19985:246866" name="prop_19985_246866" id="prop_19985_246866" /><span class="title">Super AMOLED</span>&nbsp;<span class="count">(50)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19985:246867" name="prop_19985_246867" id="prop_19985_246867" /><span class="title">TN</span>&nbsp;<span class="count">(24)</span></li>
                            </ul>
                            <ul class="clearstyle checkbox-group spoiler collapsed __showMore">
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19985:248133" name="prop_19985_248133" id="prop_19985_248133" /><span class="title">OLED</span>&nbsp;<span class="count">(27)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19985:249410" name="prop_19985_249410" id="prop_19985_249410" /><span class="title">Super LCD</span>&nbsp;<span class="count">(12)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19985:260528" name="prop_19985_260528" id="prop_19985_260528" /><span class="title">Triluminos</span>&nbsp;<span class="count">(6)</span></li>
                            </ul><div class='filter-footer'><a href="#" class="more collapsed" data-toggle="showmore" data-class="__showMore" data-endtext="свернуть">показать еще</a></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="filter-box accordion noselect"><a href="#filter-2909" data-toggle="accordion" class="opener collapsed">Оперативная память <i class="icon ic-arrow-down"></i></a>
                        <div id="filter-2909" class="spoiler collapsed">
                            <ul class="clearstyle checkbox-group">
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/operativnaya_pamyat-512_mb/" name="prop_2909_195130" id="prop_2909_195130" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/operativnaya_pamyat-512_mb/">512 Мб</a></span>&nbsp;<span class="count">(20)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/operativnaya_pamyat-768_mb/" name="prop_2909_195337" id="prop_2909_195337" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/operativnaya_pamyat-768_mb/">768 Мб</a></span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/operativnaya_pamyat-1_gb/" name="prop_2909_195055" id="prop_2909_195055" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/operativnaya_pamyat-1_gb/">1 Гб</a></span>&nbsp;<span class="count">(155)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/operativnaya_pamyat-1_5_gb/" name="prop_2909_197081" id="prop_2909_197081" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/operativnaya_pamyat-1_5_gb/">1.5 ГБ</a></span>&nbsp;<span class="count">(19)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/operativnaya_pamyat-4_gb/" name="prop_2909_196191" id="prop_2909_196191" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/operativnaya_pamyat-4_gb/">4 Гб</a></span>&nbsp;<span class="count">(148)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/operativnaya_pamyat-3_gb/" name="prop_2909_209979" id="prop_2909_209979" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/operativnaya_pamyat-3_gb/">3 Гб</a></span>&nbsp;<span class="count">(159)</span></li>
                            </ul>
                            <ul class="clearstyle checkbox-group spoiler collapsed __showMore">
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/operativnaya_pamyat-2_gb/" name="prop_2909_210426" id="prop_2909_210426" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/operativnaya_pamyat-2_gb/">2 Гб</a></span>&nbsp;<span class="count">(168)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/operativnaya_pamyat-6_gb/" name="prop_2909_226697" id="prop_2909_226697" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/operativnaya_pamyat-6_gb/">6 Гб</a></span>&nbsp;<span class="count">(34)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/operativnaya_pamyat-8_gb/" name="prop_2909_240885" id="prop_2909_240885" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/operativnaya_pamyat-8_gb/">8 Гб</a></span>&nbsp;<span class="count">(6)</span></li>
                            </ul><div class='filter-footer'><a href="#" class="more collapsed" data-toggle="showmore" data-class="__showMore" data-endtext="свернуть">показать еще</a></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="filter-box accordion noselect"><a href="#filter-1662" data-toggle="accordion" class="opener collapsed">Встроенная память <i class="icon ic-arrow-down"></i></a>
                        <div id="filter-1662" class="spoiler collapsed">
                            <ul class="clearstyle checkbox-group">
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/vstroennaya_pamyat-4_gb/" name="prop_1662_196061" id="prop_1662_196061" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/vstroennaya_pamyat-4_gb/">4 Гб</a></span>&nbsp;<span class="count">(17)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/vstroennaya_pamyat-8_gb/" name="prop_1662_196066" id="prop_1662_196066" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/vstroennaya_pamyat-8_gb/">8 Гб</a></span>&nbsp;<span class="count">(131)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/vstroennaya_pamyat-16_gb/" name="prop_1662_196060" id="prop_1662_196060" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/vstroennaya_pamyat-16_gb/">16 Гб</a></span>&nbsp;<span class="count">(236)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/vstroennaya_pamyat-32_gb/" name="prop_1662_196062" id="prop_1662_196062" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/vstroennaya_pamyat-32_gb/">32 Гб</a></span>&nbsp;<span class="count">(218)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/vstroennaya_pamyat-64_gb/" name="prop_1662_196063" id="prop_1662_196063" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/vstroennaya_pamyat-64_gb/">64 Гб</a></span>&nbsp;<span class="count">(153)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/vstroennaya_pamyat-128_gb/" name="prop_1662_196064" id="prop_1662_196064" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/vstroennaya_pamyat-128_gb/">128 Гб</a></span>&nbsp;<span class="count">(60)</span></li>
                            </ul>
                            <ul class="clearstyle checkbox-group spoiler collapsed __showMore">
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/vstroennaya_pamyat-256_gb/" name="prop_1662_207006" id="prop_1662_207006" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/vstroennaya_pamyat-256_gb/">256 Гб</a></span>&nbsp;<span class="count">(38)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/vstroennaya_pamyat-512_gb/" name="prop_1662_294604" id="prop_1662_294604" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/vstroennaya_pamyat-512_gb/">512 Гб</a></span>&nbsp;<span class="count">(6)</span></li>
                            </ul><div class='filter-footer'><a href="#" class="more collapsed" data-toggle="showmore" data-class="__showMore" data-endtext="свернуть">показать еще</a></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="filter-box accordion noselect"><a href="#filter-19987" data-toggle="accordion" class="opener collapsed">Количество мегапикселей основной камеры <i class="icon ic-arrow-down"></i></a>
                        <div id="filter-19987" class="spoiler collapsed">
                            <ul class="clearstyle checkbox-group">
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:247331" name="prop_19987_247331" id="prop_19987_247331" /><span class="title">2 Мп</span>&nbsp;<span class="count">(5)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:247361" name="prop_19987_247361" id="prop_19987_247361" /><span class="title">5 Мп</span>&nbsp;<span class="count">(63)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:247391" name="prop_19987_247391" id="prop_19987_247391" /><span class="title">8 Мп</span>&nbsp;<span class="count">(105)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:278744" name="prop_19987_278744" id="prop_19987_278744" /><span class="title">8/5 Мп</span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:260652" name="prop_19987_260652" id="prop_19987_260652" /><span class="title">8/8 Мп</span>&nbsp;<span class="count">(5)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:247872" name="prop_19987_247872" id="prop_19987_247872" /><span class="title">12/12 Мп</span>&nbsp;<span class="count">(73)</span></li>
                            </ul>
                            <ul class="clearstyle checkbox-group spoiler collapsed __showMore">
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:247431" name="prop_19987_247431" id="prop_19987_247431" /><span class="title">12 Мп</span>&nbsp;<span class="count">(150)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:266735" name="prop_19987_266735" id="prop_19987_266735" /><span class="title">12/5 Мп</span>&nbsp;<span class="count">(19)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:261578" name="prop_19987_261578" id="prop_19987_261578" /><span class="title">12/13 Мп</span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:247880" name="prop_19987_247880" id="prop_19987_247880" /><span class="title">12/2 Мп</span>&nbsp;<span class="count">(3)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:259774" name="prop_19987_259774" id="prop_19987_259774" /><span class="title">12/8 Мп</span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:270615" name="prop_19987_270615" id="prop_19987_270615" /><span class="title">12/20 Мп</span>&nbsp;<span class="count">(4)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:247434" name="prop_19987_247434" id="prop_19987_247434" /><span class="title">12.3 Мп</span>&nbsp;<span class="count">(4)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:247441" name="prop_19987_247441" id="prop_19987_247441" /><span class="title">13 Мп</span>&nbsp;<span class="count">(181)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:248033" name="prop_19987_248033" id="prop_19987_248033" /><span class="title">13/5 Мп</span>&nbsp;<span class="count">(3)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:248882" name="prop_19987_248882" id="prop_19987_248882" /><span class="title">13/2 Мп</span>&nbsp;<span class="count">(18)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:260657" name="prop_19987_260657" id="prop_19987_260657" /><span class="title">13/8 Мп</span>&nbsp;<span class="count">(4)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:250586" name="prop_19987_250586" id="prop_19987_250586" /><span class="title">13/13 Мп</span>&nbsp;<span class="count">(11)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:247471" name="prop_19987_247471" id="prop_19987_247471" /><span class="title">16 Мп</span>&nbsp;<span class="count">(44)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:266445" name="prop_19987_266445" id="prop_19987_266445" /><span class="title">16/5 Мп</span>&nbsp;<span class="count">(6)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:248425" name="prop_19987_248425" id="prop_19987_248425" /><span class="title">16/20 Мп</span>&nbsp;<span class="count">(6)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:279430" name="prop_19987_279430" id="prop_19987_279430" /><span class="title">16/24 Мп</span>&nbsp;<span class="count">(4)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:248047" name="prop_19987_248047" id="prop_19987_248047" /><span class="title">16/8 Мп</span>&nbsp;<span class="count">(3)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:247874" name="prop_19987_247874" id="prop_19987_247874" /><span class="title">16/2 Мп</span>&nbsp;<span class="count">(6)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:248134" name="prop_19987_248134" id="prop_19987_248134" /><span class="title">16/13 Мп</span>&nbsp;<span class="count">(8)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:265720" name="prop_19987_265720" id="prop_19987_265720" /><span class="title">16/16 Мп</span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:247501" name="prop_19987_247501" id="prop_19987_247501" /><span class="title">19 Мп</span>&nbsp;<span class="count">(9)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:247878" name="prop_19987_247878" id="prop_19987_247878" /><span class="title">20/12 Мп</span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:247521" name="prop_19987_247521" id="prop_19987_247521" /><span class="title">21 Мп</span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:247531" name="prop_19987_247531" id="prop_19987_247531" /><span class="title">22 Мп</span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:247867" name="prop_19987_247867" id="prop_19987_247867" /><span class="title">23 Мп</span>&nbsp;<span class="count">(11)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:261846" name="prop_19987_261846" id="prop_19987_261846" /><span class="title">40/20 Мп</span>&nbsp;<span class="count">(3)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19987:263805" name="prop_19987_263805" id="prop_19987_263805" /><span class="title">12/23 Мп</span>&nbsp;<span class="count">(1)</span></li>
                            </ul><div class='filter-footer'><a href="#" class="more collapsed" data-toggle="showmore" data-class="__showMore" data-endtext="свернуть">показать еще</a></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="filter-box accordion noselect"><a href="#filter-19988" data-toggle="accordion" class="opener collapsed">Количество мегапикселей фронтальной камеры <i class="icon ic-arrow-down"></i></a>
                        <div id="filter-19988" class="spoiler collapsed">
                            <ul class="clearstyle checkbox-group">
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19988:246872" name="prop_19988_246872" id="prop_19988_246872" /><span class="title">0</span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19988:247533" name="prop_19988_247533" id="prop_19988_247533" /><span class="title">0.3 Мп</span>&nbsp;<span class="count">(12)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19988:247542" name="prop_19988_247542" id="prop_19988_247542" /><span class="title">1.2 Мп</span>&nbsp;<span class="count">(27)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19988:247543" name="prop_19988_247543" id="prop_19988_247543" /><span class="title">1.3 Мп</span>&nbsp;<span class="count">(6)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19988:247550" name="prop_19988_247550" id="prop_19988_247550" /><span class="title">2 Мп</span>&nbsp;<span class="count">(80)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19988:247570" name="prop_19988_247570" id="prop_19988_247570" /><span class="title">4 Мп</span>&nbsp;<span class="count">(2)</span></li>
                            </ul>
                            <ul class="clearstyle checkbox-group spoiler collapsed __showMore">
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19988:247580" name="prop_19988_247580" id="prop_19988_247580" /><span class="title">5 Мп</span>&nbsp;<span class="count">(280)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19988:260654" name="prop_19988_260654" id="prop_19988_260654" /><span class="title">5/5 Мп</span>&nbsp;<span class="count">(4)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19988:247600" name="prop_19988_247600" id="prop_19988_247600" /><span class="title">7 Мп</span>&nbsp;<span class="count">(92)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19988:247610" name="prop_19988_247610" id="prop_19988_247610" /><span class="title">8 Мп</span>&nbsp;<span class="count">(142)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19988:247660" name="prop_19988_247660" id="prop_19988_247660" /><span class="title">13 Мп</span>&nbsp;<span class="count">(19)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19988:247876" name="prop_19988_247876" id="prop_19988_247876" /><span class="title">13/2 Мп</span>&nbsp;<span class="count">(3)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19988:247690" name="prop_19988_247690" id="prop_19988_247690" /><span class="title">16 Мп</span>&nbsp;<span class="count">(59)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19988:249279" name="prop_19988_249279" id="prop_19988_249279" /><span class="title">16/8 Мп</span>&nbsp;<span class="count">(5)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19988:247730" name="prop_19988_247730" id="prop_19988_247730" /><span class="title">20 Мп</span>&nbsp;<span class="count">(8)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19988:248037" name="prop_19988_248037" id="prop_19988_248037" /><span class="title">20/8 Мп</span>&nbsp;<span class="count">(3)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19988:247869" name="prop_19988_247869" id="prop_19988_247869" /><span class="title">24 Мп</span>&nbsp;<span class="count">(15)</span></li>
                            </ul><div class='filter-footer'><a href="#" class="more collapsed" data-toggle="showmore" data-class="__showMore" data-endtext="свернуть">показать еще</a></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="filter-box accordion noselect"><a href="#filter-15991" data-toggle="accordion" class="opener collapsed">Емкость аккумулятора <i class="icon ic-arrow-down"></i></a>
                        <div id="filter-15991" class="spoiler collapsed">
                            <ul class="clearstyle checkbox-group">
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195086" name="prop_15991_195086" id="prop_15991_195086" /><span class="title">3000 мАч</span>&nbsp;<span class="count">(150)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195134" name="prop_15991_195134" id="prop_15991_195134" /><span class="title">2500 мАч</span>&nbsp;<span class="count">(17)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195136" name="prop_15991_195136" id="prop_15991_195136" /><span class="title">3430 мАч</span>&nbsp;<span class="count">(3)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195180" name="prop_15991_195180" id="prop_15991_195180" /><span class="title">2300 мАч</span>&nbsp;<span class="count">(20)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195191" name="prop_15991_195191" id="prop_15991_195191" /><span class="title">4000 мАч</span>&nbsp;<span class="count">(52)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195192" name="prop_15991_195192" id="prop_15991_195192" /><span class="title">3300 мАч</span>&nbsp;<span class="count">(51)</span></li>
                            </ul>
                            <ul class="clearstyle checkbox-group spoiler collapsed __showMore">
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195193" name="prop_15991_195193" id="prop_15991_195193" /><span class="title">2000 мАч</span>&nbsp;<span class="count">(23)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195194" name="prop_15991_195194" id="prop_15991_195194" /><span class="title">1800 мАч</span>&nbsp;<span class="count">(7)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195199" name="prop_15991_195199" id="prop_15991_195199" /><span class="title">2900 мАч</span>&nbsp;<span class="count">(6)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195204" name="prop_15991_195204" id="prop_15991_195204" /><span class="title">2700 мАч</span>&nbsp;<span class="count">(16)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195210" name="prop_15991_195210" id="prop_15991_195210" /><span class="title">3100 мАч</span>&nbsp;<span class="count">(7)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195214" name="prop_15991_195214" id="prop_15991_195214" /><span class="title">2600 мАч</span>&nbsp;<span class="count">(16)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195220" name="prop_15991_195220" id="prop_15991_195220" /><span class="title">2400 мАч</span>&nbsp;<span class="count">(14)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195312" name="prop_15991_195312" id="prop_15991_195312" /><span class="title">2800 мАч</span>&nbsp;<span class="count">(15)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195318" name="prop_15991_195318" id="prop_15991_195318" /><span class="title">2100 мАч</span>&nbsp;<span class="count">(4)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195345" name="prop_15991_195345" id="prop_15991_195345" /><span class="title">1900 мАч</span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195350" name="prop_15991_195350" id="prop_15991_195350" /><span class="title">5000 мАч</span>&nbsp;<span class="count">(16)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195357" name="prop_15991_195357" id="prop_15991_195357" /><span class="title">1630 мАч</span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195375" name="prop_15991_195375" id="prop_15991_195375" /><span class="title">2200 мАч</span>&nbsp;<span class="count">(20)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195377" name="prop_15991_195377" id="prop_15991_195377" /><span class="title">1500 мАч</span>&nbsp;<span class="count">(8)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195404" name="prop_15991_195404" id="prop_15991_195404" /><span class="title">1560 мАч</span>&nbsp;<span class="count">(3)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195427" name="prop_15991_195427" id="prop_15991_195427" /><span class="title">2540 мАч</span>&nbsp;<span class="count">(11)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195461" name="prop_15991_195461" id="prop_15991_195461" /><span class="title">2050 мАч</span>&nbsp;<span class="count">(9)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195557" name="prop_15991_195557" id="prop_15991_195557" /><span class="title">1450 мАч</span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195566" name="prop_15991_195566" id="prop_15991_195566" /><span class="title">1600 мАч</span>&nbsp;<span class="count">(3)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195580" name="prop_15991_195580" id="prop_15991_195580" /><span class="title">1730 мАч</span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195764" name="prop_15991_195764" id="prop_15991_195764" /><span class="title">4400 мАч</span>&nbsp;<span class="count">(7)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195781" name="prop_15991_195781" id="prop_15991_195781" /><span class="title">2420 мАч</span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195832" name="prop_15991_195832" id="prop_15991_195832" /><span class="title">2150 мАч</span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:195992" name="prop_15991_195992" id="prop_15991_195992" /><span class="title">1300 мАч</span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:196211" name="prop_15991_196211" id="prop_15991_196211" /><span class="title">2750 мАч</span>&nbsp;<span class="count">(7)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:202375" name="prop_15991_202375" id="prop_15991_202375" /><span class="title">1750 мАч</span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:202866" name="prop_15991_202866" id="prop_15991_202866" /><span class="title">2650 мАч</span>&nbsp;<span class="count">(3)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:203246" name="prop_15991_203246" id="prop_15991_203246" /><span class="title">3340 мАч</span>&nbsp;<span class="count">(4)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:203995" name="prop_15991_203995" id="prop_15991_203995" /><span class="title">3200 мАч</span>&nbsp;<span class="count">(7)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:206064" name="prop_15991_206064" id="prop_15991_206064" /><span class="title">3120 мАч</span>&nbsp;<span class="count">(6)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:206649" name="prop_15991_206649" id="prop_15991_206649" /><span class="title">3620 мАч</span>&nbsp;<span class="count">(4)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:206691" name="prop_15991_206691" id="prop_15991_206691" /><span class="title">2470 мАч</span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:207251" name="prop_15991_207251" id="prop_15991_207251" /><span class="title">1940 мАч</span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:207591" name="prop_15991_207591" id="prop_15991_207591" /><span class="title">2125 мАч</span>&nbsp;<span class="count">(5)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:209834" name="prop_15991_209834" id="prop_15991_209834" /><span class="title">3600 мАч</span>&nbsp;<span class="count">(14)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:214063" name="prop_15991_214063" id="prop_15991_214063" /><span class="title">3500 мАч</span>&nbsp;<span class="count">(26)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:214284" name="prop_15991_214284" id="prop_15991_214284" /><span class="title">4100 мАч</span>&nbsp;<span class="count">(27)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:217085" name="prop_15991_217085" id="prop_15991_217085" /><span class="title">2620 мАч</span>&nbsp;<span class="count">(3)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:217993" name="prop_15991_217993" id="prop_15991_217993" /><span class="title">2580 мАч</span>&nbsp;<span class="count">(6)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:220182" name="prop_15991_220182" id="prop_15991_220182" /><span class="title">2870 мАч</span>&nbsp;<span class="count">(4)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:220469" name="prop_15991_220469" id="prop_15991_220469" /><span class="title">2250 мАч</span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:220833" name="prop_15991_220833" id="prop_15991_220833" /><span class="title">3400 мАч</span>&nbsp;<span class="count">(11)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:221191" name="prop_15991_221191" id="prop_15991_221191" /><span class="title">3010 мАч</span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:221884" name="prop_15991_221884" id="prop_15991_221884" /><span class="title">2850 мАч</span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:223091" name="prop_15991_223091" id="prop_15991_223091" /><span class="title">3060 мАч</span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:223627" name="prop_15991_223627" id="prop_15991_223627" /><span class="title">4050 мАч</span>&nbsp;<span class="count">(8)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:224661" name="prop_15991_224661" id="prop_15991_224661" /><span class="title">2630 мАч</span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:225196" name="prop_15991_225196" id="prop_15991_225196" /><span class="title">4850 мАч</span>&nbsp;<span class="count">(4)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:225472" name="prop_15991_225472" id="prop_15991_225472" /><span class="title">3900 мАч</span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:227219" name="prop_15991_227219" id="prop_15991_227219" /><span class="title">3080 мАч</span>&nbsp;<span class="count">(22)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:228691" name="prop_15991_228691" id="prop_15991_228691" /><span class="title">2460 мАч</span>&nbsp;<span class="count">(3)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:229195" name="prop_15991_229195" id="prop_15991_229195" /><span class="title">2350 мАч</span>&nbsp;<span class="count">(5)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:229650" name="prop_15991_229650" id="prop_15991_229650" /><span class="title">3800 мАч</span>&nbsp;<span class="count">(5)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:229796" name="prop_15991_229796" id="prop_15991_229796" /><span class="title">2130 мАч</span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:230533" name="prop_15991_230533" id="prop_15991_230533" /><span class="title">3070 мАч</span>&nbsp;<span class="count">(6)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:232537" name="prop_15991_232537" id="prop_15991_232537" /><span class="title">2730 мАч</span>&nbsp;<span class="count">(4)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:237192" name="prop_15991_237192" id="prop_15991_237192" /><span class="title">4500 мАч</span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:238096" name="prop_15991_238096" id="prop_15991_238096" /><span class="title">3350 мАч</span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:239176" name="prop_15991_239176" id="prop_15991_239176" /><span class="title">5300 мАч</span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:241162" name="prop_15991_241162" id="prop_15991_241162" /><span class="title">4070 мАч</span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:241251" name="prop_15991_241251" id="prop_15991_241251" /><span class="title">2950 мАч</span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:242209" name="prop_15991_242209" id="prop_15991_242209" /><span class="title">5050 мАч</span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:242216" name="prop_15991_242216" id="prop_15991_242216" /><span class="title">3360 мАч</span>&nbsp;<span class="count">(7)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:246677" name="prop_15991_246677" id="prop_15991_246677" /><span class="title">3230 мАч</span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:247893" name="prop_15991_247893" id="prop_15991_247893" /><span class="title">3505 мАч</span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:249280" name="prop_15991_249280" id="prop_15991_249280" /><span class="title">3580 мАч</span>&nbsp;<span class="count">(3)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:259699" name="prop_15991_259699" id="prop_15991_259699" /><span class="title">3930 мАч</span>&nbsp;<span class="count">(3)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:260533" name="prop_15991_260533" id="prop_15991_260533" /><span class="title">3180 мАч</span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:261595" name="prop_15991_261595" id="prop_15991_261595" /><span class="title">5580 мАч</span>&nbsp;<span class="count">(3)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:262047" name="prop_15991_262047" id="prop_15991_262047" /><span class="title">12000 мАч</span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:270738" name="prop_15991_270738" id="prop_15991_270738" /><span class="title">6000 мАч</span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:281477" name="prop_15991_281477" id="prop_15991_281477" /><span class="title">5180 мАч</span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:295755" name="prop_15991_295755" id="prop_15991_295755" /><span class="title">2942 мАч</span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=15991:296596" name="prop_15991_296596" id="prop_15991_296596" /><span class="title">2990 мАч</span>&nbsp;<span class="count">(1)</span></li>
                            </ul><div class='filter-footer'><a href="#" class="more collapsed" data-toggle="showmore" data-class="__showMore" data-endtext="свернуть">показать еще</a></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="filter-box accordion noselect"><a href="#filter-19989" data-toggle="accordion" class="opener collapsed">Защищенность <i class="icon ic-arrow-down"></i></a>
                        <div id="filter-19989" class="spoiler collapsed">
                            <ul class="clearstyle checkbox-group">
                                <li class=" multiline limit">
                                    <input type="checkbox" multiline limit data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/zashishennost-zashita_ot_pili_i_vlagi/" name="prop_19989_247753" id="prop_19989_247753" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/zashishennost-zashita_ot_pili_i_vlagi/">защита от пыли и влаги</a></span>&nbsp;<span class="count">(180)</span></li>
                                <li class=" multiline limit">
                                    <input type="checkbox" multiline limit data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/zashishennost-ukreplenniy_korpus/" name="prop_19989_247754" id="prop_19989_247754" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/zashishennost-ukreplenniy_korpus/">укрепленный корпус</a></span>&nbsp;<span class="count">(8)</span></li>
                                <li class=" multiline limit">
                                    <input type="checkbox" multiline limit data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19989:247755" name="prop_19989_247755" id="prop_19989_247755" /><span class="title">защитное покрытие экрана от царапин</span>&nbsp;<span class="count">(262)</span></li>
                                <li class=" multiline limit">
                                    <input type="checkbox" multiline limit data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=19989:247756" name="prop_19989_247756" id="prop_19989_247756" /><span class="title">олеофобное покрытие</span>&nbsp;<span class="count">(61)</span></li>
                            </ul>
                            <div class='filter-footer'></div><div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="filter-box accordion noselect"><a href="#filter-4021" data-toggle="accordion" class="opener collapsed">Возможности <i class="icon ic-arrow-down"></i></a>
                        <div id="filter-4021" class="spoiler collapsed">
                            <ul class="clearstyle checkbox-group">
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=4021:195845" name="prop_4021_195845" id="prop_4021_195845" /><span class="title">2 симкарты</span>&nbsp;<span class="count">(709)</span></li>
                                <li class=" multiline limit">
                                    <input type="checkbox" multiline limit data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=4021:195846" name="prop_4021_195846" id="prop_4021_195846" /><span class="title">защита от пыли и влаги</span>&nbsp;<span class="count">(164)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=4021:195847" name="prop_4021_195847" id="prop_4021_195847" /><span class="title">NFC</span>&nbsp;<span class="count">(347)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/vozmojnosti-lte/" name="prop_4021_195848" id="prop_4021_195848" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/vozmojnosti-lte/">LTE</a></span>&nbsp;<span class="count">(733)</span></li>
                                <li class=" multiline limit">
                                    <input type="checkbox" multiline limit data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=4021:195849" name="prop_4021_195849" id="prop_4021_195849" /><span class="title">слот для карты памяти</span>&nbsp;<span class="count">(678)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=4021:195850" name="prop_4021_195850" id="prop_4021_195850" /><span class="title">Wi-Fi</span>&nbsp;<span class="count">(827)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/vozmojnosti-gps/" name="prop_4021_195851" id="prop_4021_195851" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/vozmojnosti-gps/">GPS</a></span>&nbsp;<span class="count">(825)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/vozmojnosti-3g/" name="prop_4021_195852" id="prop_4021_195852" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/vozmojnosti-3g/">3G</a></span>&nbsp;<span class="count">(798)</span></li>
                            </ul>
                            <ul class="clearstyle checkbox-group spoiler collapsed __showMore">
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=4021:242213" name="prop_4021_242213" id="prop_4021_242213" /><span class="title">4G</span>&nbsp;<span class="count">(24)</span></li>
                            </ul><div class='filter-footer'><a href="#" class="more collapsed" data-toggle="showmore" data-class="__showMore" data-endtext="свернуть">показать еще</a></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="filter-box accordion noselect"><a href="#filter-18085" data-toggle="accordion" class="opener collapsed">Количество SIM-карт <i class="icon ic-arrow-down"></i></a>
                        <div id="filter-18085" class="spoiler collapsed">
                            <ul class="clearstyle checkbox-group">
                                <li class="">
                                    <input type="checkbox" data-link="/smartfoni_plansheti_gadjeti/telefoniya/?f=18085:210636" name="prop_18085_210636" id="prop_18085_210636" /><span class="title">1</span>&nbsp;<span class="count">(124)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/kolichestvo_sim_kart-2/" name="prop_18085_210637" id="prop_18085_210637" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/kolichestvo_sim_kart-2/">2</a></span>&nbsp;<span class="count">(696)</span></li>
                            </ul>
                            <div class='filter-footer'></div><div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="filter-box accordion noselect"><a href="#filter-4524" data-toggle="accordion" class="opener collapsed">Цвет <i class="icon ic-arrow-down"></i></a>
                        <div id="filter-4524" class="spoiler collapsed">
                            <ul class="clearstyle checkbox-group">
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-beliy/" name="prop_4524_11876" id="prop_4524_11876" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-beliy/">белый</a></span>&nbsp;<span class="count">(112)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-goluboy/" name="prop_4524_11877" id="prop_4524_11877" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-goluboy/">голубой</a></span>&nbsp;<span class="count">(13)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-grafitoviy/" name="prop_4524_11878" id="prop_4524_11878" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-grafitoviy/">графитовый</a></span>&nbsp;<span class="count">(6)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-jeltiy/" name="prop_4524_16769" id="prop_4524_16769" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-jeltiy/">жёлтый</a></span>&nbsp;<span class="count">(6)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-zeleniy/" name="prop_4524_11879" id="prop_4524_11879" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-zeleniy/">зеленый</a></span>&nbsp;<span class="count">(6)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-zolotistiy/" name="prop_4524_11880" id="prop_4524_11880" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-zolotistiy/">золотистый</a></span>&nbsp;<span class="count">(189)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-korichneviy/" name="prop_4524_11881" id="prop_4524_11881" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-korichneviy/">коричневый</a></span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-krasniy/" name="prop_4524_11882" id="prop_4524_11882" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-krasniy/">красный</a></span>&nbsp;<span class="count">(30)</span></li>
                            </ul>
                            <ul class="clearstyle checkbox-group spoiler collapsed __showMore">
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-metallik/" name="prop_4524_11883" id="prop_4524_11883" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-metallik/">металлик</a></span>&nbsp;<span class="count">(9)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-oranjeviy/" name="prop_4524_11884" id="prop_4524_11884" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-oranjeviy/">оранжевый</a></span>&nbsp;<span class="count">(2)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-rozoviy/" name="prop_4524_11885" id="prop_4524_11885" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-rozoviy/">розовый</a></span>&nbsp;<span class="count">(15)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-serebristiy/" name="prop_4524_11886" id="prop_4524_11886" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-serebristiy/">серебристый</a></span>&nbsp;<span class="count">(76)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-seriy/" name="prop_4524_11887" id="prop_4524_11887" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-seriy/">серый</a></span>&nbsp;<span class="count">(74)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-siniy/" name="prop_4524_11888" id="prop_4524_11888" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-siniy/">синий</a></span>&nbsp;<span class="count">(84)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-fioletoviy/" name="prop_4524_11889" id="prop_4524_11889" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-fioletoviy/">фиолетовый</a></span>&nbsp;<span class="count">(5)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-cherniy/" name="prop_4524_11892" id="prop_4524_11892" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-cherniy/">черный</a></span>&nbsp;<span class="count">(301)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-shampan/" name="prop_4524_25608" id="prop_4524_25608" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-shampan/">шампань</a></span>&nbsp;<span class="count">(3)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-titan/" name="prop_4524_89597" id="prop_4524_89597" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-titan/">титан</a></span>&nbsp;<span class="count">(13)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-medniy/" name="prop_4524_162554" id="prop_4524_162554" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-medniy/">медный</a></span>&nbsp;<span class="count">(3)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-platina/" name="prop_4524_195759" id="prop_4524_195759" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-platina/">платина</a></span>&nbsp;<span class="count">(3)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-koralloviy/" name="prop_4524_220472" id="prop_4524_220472" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-koralloviy/">коралловый</a></span>&nbsp;<span class="count">(3)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-rozovoe_zoloto/" name="prop_4524_223070" id="prop_4524_223070" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-rozovoe_zoloto/">розовое золото</a></span>&nbsp;<span class="count">(15)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-cherniy_oniks/" name="prop_4524_223603" id="prop_4524_223603" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-cherniy_oniks/">черный оникс</a></span>&nbsp;<span class="count">(6)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-grafit/" name="prop_4524_226302" id="prop_4524_226302" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-grafit/">графит</a></span>&nbsp;<span class="count">(3)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-temno_seriy/" name="prop_4524_226308" id="prop_4524_226308" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-temno_seriy/">темно-серый</a></span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-jeltiy_topaz/" name="prop_4524_231754" id="prop_4524_231754" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-jeltiy_topaz/">желтый топаз</a></span>&nbsp;<span class="count">(3)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-cherniy_brilliant/" name="prop_4524_231755" id="prop_4524_231755" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-cherniy_brilliant/">черный бриллиант</a></span>&nbsp;<span class="count">(5)</span></li>
                                <li class=" multiline limit">
                                    <input type="checkbox" multiline limit data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-misticheskiy_ametist/" name="prop_4524_231757" id="prop_4524_231757" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-misticheskiy_ametist/">мистический аметист</a></span>&nbsp;<span class="count">(3)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-temno_siniy/" name="prop_4524_237957" id="prop_4524_237957" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-temno_siniy/">темно-синий</a></span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-siniy_sapfir/" name="prop_4524_241947" id="prop_4524_241947" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-siniy_sapfir/">синий сапфир</a></span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-burgundi/" name="prop_4524_249052" id="prop_4524_249052" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-burgundi/">бургунди</a></span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-korolevskiy_rubin/" name="prop_4524_259346" id="prop_4524_259346" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-korolevskiy_rubin/">королевский рубин</a></span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-ledyanaya_platina/" name="prop_4524_281327" id="prop_4524_281327" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-ledyanaya_platina/">ледяная платина</a></span>&nbsp;<span class="count">(1)</span></li>
                                <li class="">
                                    <input type="checkbox" data-link="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-sumerechniy/" name="prop_4524_281558" id="prop_4524_281558" /><span class="title"><a href="/filters/smartfoni_plansheti_gadjeti/telefoniya/cvet-sumerechniy/">сумеречный</a></span>&nbsp;<span class="count">(1)</span></li>
                            </ul><div class='filter-footer'><a href="#" class="more collapsed" data-toggle="showmore" data-class="__showMore" data-endtext="свернуть">показать еще</a></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="filter-box actions"><a href="#" data-link="/smartfoni_plansheti_gadjeti/telefoniya/" class="btn btn-block reset">сбросить</a><a href="#" data-link="" class="btn btn-primary display">показать <strong><i>958</i> товаров</strong></a></div>
                </div>
            </div>
            <!-- top -->
            <div class="column col-lg-80 col-md-80 col-sm-70 col-xs-100 -items-aside">
                <div class="-mobile-pan">
                    <div class="-buttons">
                        <a href="#" class="show-orders">Сортировать</a>
                        <a href="#" class="show-filters">Фильтры</a>
                    </div>
                    <div class="top-bar selected-filters"></div>
                </div>
                <div class="top-ban">
                    <a href="#"><img src='/img/cat-bn.jpg' class="desktop"><img src='/img/cat-bn-768.jpg' class="laptop"></a>
                </div>
                <div class="top-brands">
                    <h4>Бренды</h4>
                    <nav>
                        <a href="#" class="-prev"></a>
                        <a href="#" class="-next"></a>
                        <div class="-track">
                            <div class='-b'><a href="#" style="background-image: url('/img/c-br-01.png');"></a></div>
                            <div class='-b'><a href="#" style="background-image: url('/img/c-br-02.png');"></a></div>
                            <div class='-b'><a href="#" style="background-image: url('/img/c-br-03.png');"></a></div>
                            <div class='-b'><a href="#" style="background-image: url('/img/c-br-04.png');"></a></div>
                            <div class='-b'><a href="#" style="background-image: url('/img/c-br-05.png');"></a></div>
                            <div class='-b'><a href="#" style="background-image: url('/img/c-br-06.png');"></a></div>
                            <div class='-b'><a href="#" style="background-image: url('/img/c-br-07.png');"></a></div>
                            <div class='-b'><a href="#" style="background-image: url('/img/c-br-08.png');"></a></div>
                            <div class='-b'><a href="#" style="background-image: url('/img/c-br-09.png');"></a></div>
                            <div class='-b'><a href="#" style="background-image: url('/img/c-br-10.png');"></a></div>
                            <div class='-b'><a href="#" style="background-image: url('/img/c-br-01.png');"></a></div>
                            <div class='-b'><a href="#" style="background-image: url('/img/c-br-02.png');"></a></div>
                            <div class='-b'><a href="#" style="background-image: url('/img/c-br-03.png');"></a></div>
                            <div class='-b'><a href="#" style="background-image: url('/img/c-br-04.png');"></a></div>
                            <div class='-b'><a href="#" style="background-image: url('/img/c-br-05.png');"></a></div>
                            <div class='-b'><a href="#" style="background-image: url('/img/c-br-06.png');"></a></div>
                            <div class='-b'><a href="#" style="background-image: url('/img/c-br-07.png');"></a></div>
                            <div class='-b'><a href="#" style="background-image: url('/img/c-br-08.png');"></a></div>
                            <div class='-b'><a href="#" style="background-image: url('/img/c-br-09.png');"></a></div>
                            <div class='-b'><a href="#" style="background-image: url('/img/c-br-10.png');"></a></div>
                        </div>
                    </nav>
                </div>
                <div class="bg-white">
                    <div class="row top-bar">
                        <div class="open-xs-filter-wrapper hidden-lg hidden-md hidden-sm"><a href="#" id="filter-opener" class="open-xs-filter ">Фильтр</a></div>
                        <div class="clearfix"></div>
                        <div class="col-lg-6 col-md-6 col-sm-12 sort-block">
                            <div class="toolbar sort animate-links fleft visible-lg visible-md hidden-sm hidden-xs hide-in-js catalog-sorting"><!--<span>Сортировать по</span>
                            <div class="-selector">
                                <a href='#' class='expander desc'>популярности</a>
                                <ul class="values">
                                    <li><a href='#' class='asc'>популярности</a></li>
                                    <li><a href='#' class='desc'>цене</a></li>
                                </ul>
                            </div>
                            --></div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 hidden-xs hidden-xs hide-in-js sort-block frg"><!--<div class="toolbar fright "><span>Вид</span><a href="#" class="btn btn-default btn-icon active js-catalog-grid" typeView='1' data-url="?view=tiles" data-view="tiles"><i class="icon ic-grid"></i></a><a href="#" class="btn btn-default btn-icon js-catalog-rows" typeView='2' data-url="?view=list" data-view="list"><i class="icon ic-list"></i></a></div>--></div>
                        <div class="row top-bar selected-filters"></div>
                    </div>
                    <div class="bottom-bar row">
                        <ul class="popular-sets">
                            <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/32gb/">32 гб</a></li>
                            <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/4-duyma/">4 дюйма</a></li>
                            <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/5-duymov/">5 дюймов</a></li>
                            <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/64gb/">64 гб</a></li>
                            <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/8-yadernye-smartfony/">8 ядерные</a></li>
                            <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-lenovo/vibe-x/">Lenovo Vibe X</a></li>
                            <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/32gb/">32 гб</a></li>
                            <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/4-duyma/">4 дюйма</a></li>
                            <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/5-duymov/">5 дюймов</a></li>
                            <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/64gb/">64 гб</a></li>
                            <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/8-yadernye-smartfony/">8 ядерные</a></li>
                            <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-lenovo/vibe-x/">Lenovo Vibe X</a></li>
                            <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/32gb/">32 гб</a></li>
                            <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/4-duyma/">4 дюйма</a></li>
                            <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/5-duymov/">5 дюймов</a></li>
                            <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/64gb/">64 гб</a></li>
                            <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/8-yadernye-smartfony/">8 ядерные</a></li>
                            <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-lenovo/vibe-x/">Lenovo Vibe X</a></li>
                            <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/5-duymov/">5 дюймов</a></li>
                            <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/64gb/">64 гб</a></li>
                            <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/8-yadernye-smartfony/">8 ядерные</a></li>
                            <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-lenovo/vibe-x/">Lenovo Vibe X</a></li>
                            <li class="-toggle"><a href="#" data-expanded="свернуть" data-collapsed="показать еще"></a></li>
                        </ul>
                        
                        <div class="row top-bar selected-filters"></div>
                    </div>
                </div>
                <!-- END top -->

                <div id="products-list" x-class="products-rows">
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80054955" data-product-external-id="729842">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/gadgets/communicators/brand-asus/smartfon_asus_zenfone_3_max_zc553kl_16_gb_seriy_art80054955/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80054955.jpg" class="lazy-img" alt="Смартфон ASUS ZenFone 3 Max ZC553KL 16 Гб серый (90AX00D2-M01780)" title="Смартфон ASUS ZenFone 3 Max ZC553KL 16 Гб серый (90AX00D2-M01780)" /></a>
                            <div class="-thumbs">
                                <img src='http://www.123.ru/l_pics/80054955.jpg' data-full='http://www.123.ru/l_pics/80054955.jpg' class='selected'>
                                <img src='http://www.123.ru/l_pics/80053717.jpg' data-full='http://www.123.ru/l_pics/80053717.jpg'>
                                <img src='http://www.123.ru/l_pics/80188188.jpg' data-full='http://www.123.ru/l_pics/80188188.jpg'>
                            </div>

                        </div>
                        <ul class="stickers">
                            <li class="x-sale"><span>-17%</span></li>
                        </ul>
                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/asus.png)'></span>
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">23</a></strong>
                                    </div>
                                </div>
                                <span class='-bonus'>+30 баллов</span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/gadgets/communicators/brand-asus/smartfon_asus_zenfone_3_max_zc553kl_16_gb_seriy_art80054955/" class="title">Смартфон ASUS ZenFone 3 Max ZC553KL 16 Гб серый (90AX00D2-M01780) Смартфон ASUS ZenFone 3 Max ZC553KL 16 Гб серый (90AX00D2-M01780) Смартфон ASUS ZenFone 3 Max ZC553KL 16 Гб серый (90AX00D2-M01780)</a>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Арт.</em><span>80053717</span></li>
                                <li><em>Бренд</em><span>ASUS</span></li>
                                <li><em>Цвет</em><span>серый</span></li>
                                <li><em>Материал корпуса</em><span>металл</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>5.5"</span></li>
                                <li><em>Разрешение экрана</em><span>1920x1080</span></li>
                                <li><em>Видеопроцессор</em><span>Adreno 505</span></li>
                                <li><em>Оперативная память</em><span>2 Гб</span></li>
                                <li><em>Поддержка ГЛОНАСС</em><span>да</span></li>
                                <li><em>Количество мегапикселей основной камеры</em><span>16 Мп</span></li>
                                <li><em>Емкость аккумулятора</em><span>4100 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты<br />LTE<br />слот для карты памяти<br />Wi-Fi<br />GPS<br />3G</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price">
                                <strong>10 280 <i class="rub">a</i></strong>
                                <strong class="old-price">12 390 <i class="rub">a</i></strong>
                                <div class='-item-actions'>
                                    <a href="#" class="-add-to-favorites" data-action="add-to-favorites" data-id="<?php echo rand(1e3,1e8); ?>"></a>
                                    <a href="#" class="-add-to-compare" data-action="add-to-compare" data-id="<?php echo rand(1e3,1e8); ?>"></a>
                                </div>
                            </div>
                            <div class="sp-inline-rating sp-inline-rating-container-80054955"></div>
                                <div class="buy-block">
                                    <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                <a href="#" data-link="/basket/add/?id=80054955" data-product-id="80054955" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80054955">в корзину</a>
                            </div>
                        </div>
                    </div>
                    <div class="product-item -unavailable cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80053717">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-alcatel/smartfon_alcatel_pixi_4_plus_power_5023f_16_gb_beliy_art80053717/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80053717.jpg" class="lazy-img" alt="Смартфон Alcatel Pixi 4 Plus Power 5023F 16 Гб белый (5023F-2BALRU2)" title="Смартфон Alcatel Pixi 4 Plus Power 5023F 16 Гб белый (5023F-2BALRU2)" /></a>
                        </div>

                        <div class="info">
                            <div class="-rt">
                              <!--
                                <span class="-brand-line" style='background-image: url(/img/samsung.png)'></span>
                              -->
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
                                <span></span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-alcatel/smartfon_alcatel_pixi_4_plus_power_5023f_16_gb_beliy_art80053717/" class="title">Смартфон Alcatel Pixi 4 Plus Power 5023F 16 Гб белый (5023F-2BALRU2)</a>
                                <div class="product-rate">
                                    <div class="sp-inline-rating sp-inline-rating-container-80053717"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Арт.</em><span>80053717</span></li>
                                <li><em>Бренд</em><span>Alcatel</span></li>
                                <li><em>Цвет</em><span>белый</span></li>
                                <li><em>Материал корпуса</em><span>пластик</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>5.5"</span></li>
                                <li><em>Разрешение экрана</em><span>1280x720</span></li>
                                <li><em>Видеопроцессор</em><span>Mali-400 MP2</span></li>
                                <li><em>Оперативная память</em><span>1 Гб</span></li>
                                <li><em>Емкость аккумулятора</em><span>5000 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты<br />слот для карты памяти<br />Wi-Fi<br />GPS<br />3G</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>5 200 <i class="rub">a</i></strong><strong class="old-price">5 860 <i class="rub">a</i></strong>
                                <div class='-item-actions'>
                                    <a href="#" class="-add-to-favorites" data-action="add-to-favorites" data-id="<?php echo rand(1e3,1e8); ?>"></a>
                                    <a href="#" class="-add-to-compare" data-action="add-to-compare" data-id="<?php echo rand(1e3,1e8); ?>"></a>
                                </div>
                            </div>
                            <div class="sp-inline-rating sp-inline-rating-container-80053717"></div>
                            <div class="buy-block">
                                    <a href="#" data-product-id="80053717" data-window='#avail-notify' data-position="fixed" data-background='true' class="btn-primary animate-all js-buyBtn">Сообщить о наличии</a>
                                <div class="board"></div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80188188" data-product-external-id="862902">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/smartfoni_plansheti_gadjeti/telefoniya/_art80188188/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80188188.jpg" class="lazy-img" alt="Смартфон Honor 7C 32 Гб синий (51092MNT)" title="Смартфон Honor 7C 32 Гб синий (51092MNT)" /></a>
                        </div>

                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/asus.png)'></span>
<!--
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
-->
                                <span class='-bonus'>+30 баллов</span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/smartfoni_plansheti_gadjeti/telefoniya/_art80188188/" class="title">Смартфон Honor 7C 32 Гб синий (51092MNT)</a>
                                <div class="product-rate">
                                    <div class="sp-inline-rating sp-inline-rating-container-80188188"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Арт.</em><span>80053717</span></li>
                                <li><em>Бренд</em><span>Honor</span></li>
                                <li><em>Цвет</em><span>синий</span></li>
                                <li><em>Материал корпуса</em><span>пластик</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>5.7"</span></li>
                                <li><em>Разрешение экрана</em><span>1440x720</span></li>
                                <li><em>Видеопроцессор</em><span>Adreno 505</span></li>
                                <li><em>Оперативная память</em><span>3 Гб</span></li>
                                <li><em>Поддержка ГЛОНАСС</em><span>да</span></li>
                                <li><em>Количество мегапикселей основной камеры</em><span>13/2 Мп</span></li>
                                <li><em>Емкость аккумулятора</em><span>3000 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты<br />LTE<br />слот для карты памяти<br />Wi-Fi<br />GPS<br />3G</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>10 600 <i class="rub">a</i></strong><strong class="old-price">11 350 <i class="rub">a</i></strong>
                                <div class='-item-actions'>
                                    <a href="#" class="-add-to-favorites" data-action="add-to-favorites" data-id="<?php echo rand(1e3,1e8); ?>"></a>
                                    <a href="#" class="-add-to-compare" data-action="add-to-compare" data-id="<?php echo rand(1e3,1e8); ?>"></a>
                                </div>
                            </div>
                            <div class="sp-inline-rating sp-inline-rating-container-80188188"></div>
                            <div class="buy-block">
                                <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                    <a href="#" data-link="/basket/add/?id=80188188" data-product-id="80188188" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80188188">в корзину</a>
                                <div class="board"></div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80188194" data-product-external-id="862908">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/smartfoni_plansheti_gadjeti/telefoniya/_art80188194/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80188194.jpg" class="lazy-img" alt="Смартфон Honor 7X 64 Гб синий (BND-L21 51091YTY)" title="Смартфон Honor 7X 64 Гб синий (BND-L21 51091YTY)" /></a>
                        </div>

                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/samsung.png)'></span>
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
                                <span></span>
                            </div>

                            <div class="title-block"><a href='#' class='-expander'></a><a href="/smartfoni_plansheti_gadjeti/telefoniya/_art80188194/" class="title">Смартфон Honor 7X 64 Гб синий (BND-L21 51091YTY)</a>
                                <div class="product-rate">
                                    <div class="sp-inline-rating sp-inline-rating-container-80188194"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Арт.</em><span>80053717</span></li>
                                <li><em>Бренд</em><span>Honor</span></li>
                                <li><em>Цвет</em><span>синий</span></li>
                                <li><em>Материал корпуса</em><span>металл</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>5.9"</span></li>
                                <li><em>Разрешение экрана</em><span>2160x1080</span></li>
                                <li><em>Видеопроцессор</em><span>Mali-T830 MP2</span></li>
                                <li><em>Оперативная память</em><span>4 Гб</span></li>
                                <li><em>Поддержка ГЛОНАСС</em><span>да</span></li>
                                <li><em>Количество мегапикселей основной камеры</em><span>16/2 Мп</span></li>
                                <li><em>Емкость аккумулятора</em><span>3340 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты<br />LTE<br />слот для карты памяти<br />Wi-Fi<br />GPS<br />3G</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>16 150 <i class="rub">a</i></strong><strong class="old-price">17 500 <i class="rub">a</i></strong>
                                <div class='-item-actions'>
                                    <a href="#" class="-add-to-favorites" data-action="add-to-favorites" data-id="<?php echo rand(1e3,1e8); ?>"></a>
                                    <a href="#" class="-add-to-compare" data-action="add-to-compare" data-id="<?php echo rand(1e3,1e8); ?>"></a>
                                </div>
                            </div>
                            <div class="sp-inline-rating sp-inline-rating-container-80188194"></div>
                            <div class="buy-block">
                                <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                    <a href="#" data-link="/basket/add/?id=80188194" data-product-id="80188194" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80188194">в корзину</a>
                                <div class="board"></div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80185424" data-product-external-id="860138">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-samsung/_art80185424/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80185424.jpg" class="lazy-img" alt="Смартфон Samsung Galaxy J8 2018 32 Гб черный (SM-J810FZKDSER)" title="Смартфон Samsung Galaxy J8 2018 32 Гб черный (SM-J810FZKDSER)" /></a>
                        </div>

                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/samsung.png)'></span>
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
                                <span></span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-samsung/_art80185424/" class="title">Смартфон Samsung Galaxy J8 2018 32 Гб черный (SM-J810FZKDSER)</a>
                                <div class="product-rate">
                                    <div class="sp-inline-rating sp-inline-rating-container-80185424"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Арт.</em><span>80053717</span></li>
                                <li><em>Бренд</em><span>Samsung</span></li>
                                <li><em>Цвет</em><span>черный</span></li>
                                <li><em>Материал корпуса</em><span>пластик</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>6"</span></li>
                                <li><em>Разрешение экрана</em><span>1480 x720</span></li>
                                <li><em>Видеопроцессор</em><span>Adreno 506</span></li>
                                <li><em>Оперативная память</em><span>3 Гб</span></li>
                                <li><em>Поддержка ГЛОНАСС</em><span>да</span></li>
                                <li><em>Количество мегапикселей основной камеры</em><span>16/5 Мп</span></li>
                                <li><em>Емкость аккумулятора</em><span>3500 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты<br />LTE<br />слот для карты памяти<br />Wi-Fi<br />GPS<br />3G</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>14 510 <i class="rub">a</i></strong><strong class="old-price">15 460 <i class="rub">a</i></strong>
                                <div class='-item-actions'>
                                    <a href="#" class="-add-to-favorites" data-action="add-to-favorites" data-id="<?php echo rand(1e3,1e8); ?>"></a>
                                    <a href="#" class="-add-to-compare" data-action="add-to-compare" data-id="<?php echo rand(1e3,1e8); ?>"></a>
                                </div>
                            </div>
                            <div class="sp-inline-rating sp-inline-rating-container-80185424"></div>
                            <div class="buy-block">
                                <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                    <a href="#" data-link="/basket/add/?id=80185424" data-product-id="80185424" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80185424">в корзину</a>
                                <div class="board"></div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80053617" data-product-external-id="728464">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/gadgets/communicators/brand-fly/smartfon_fly_fs408_stratus_8_8_gb_cherniy_art80053617/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80053617.jpg" class="lazy-img" alt="Смартфон Fly FS408 Stratus 8 8 Гб черный" title="Смартфон Fly FS408 Stratus 8 8 Гб черный" /></a>
                        </div>

                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/samsung.png)'></span>
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
                                <span></span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/gadgets/communicators/brand-fly/smartfon_fly_fs408_stratus_8_8_gb_cherniy_art80053617/" class="title">Смартфон Fly FS408 Stratus 8 8 Гб черный</a>
                                <div class="product-rate">
                                    <div class="art">Арт. 80053617</div>
                                    <div class="sp-inline-rating sp-inline-rating-container-80053617"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Бренд</em><span>Fly</span></li>
                                <li><em>Цвет</em><span>черный</span></li>
                                <li><em>Материал корпуса</em><span>пластик</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>4"</span></li>
                                <li><em>Разрешение экрана</em><span>854x480</span></li>
                                <li><em>Видеопроцессор</em><span>Mali-400 MP2</span></li>
                                <li><em>Оперативная память</em><span>512 Мб</span></li>
                                <li><em>Поддержка ГЛОНАСС</em><span>нет</span></li>
                                <li><em>Количество мегапикселей основной камеры</em><span>2 Мп</span></li>
                                <li><em>Емкость аккумулятора</em><span>1300 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты<br />слот для карты памяти<br />Wi-Fi<br />GPS<br />3G</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>2 520 <i class="rub">a</i></strong><strong class="old-price">2 740 <i class="rub">a</i></strong>
                                <div class='-item-actions'>
                                    <a href="#" class="-add-to-favorites" data-action="add-to-favorites" data-id="<?php echo rand(1e3,1e8); ?>"></a>
                                    <a href="#" class="-add-to-compare" data-action="add-to-compare" data-id="<?php echo rand(1e3,1e8); ?>"></a>
                                </div>
                            </div>
                            <div class="sp-inline-rating sp-inline-rating-container-80053617"></div>
                            <div class="buy-block">
                                <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                    <a href="#" data-link="/basket/add/?id=80053617" data-product-id="80053617" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80053617">в корзину</a>
                                <div class="board"></div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80071130" data-product-external-id="746448">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/gadgets/communicators/brand-huawei/smartfon_huawei_p_smart_32_gb_zolotistiy_art80071130/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80071130.jpg" class="lazy-img" alt="Смартфон Huawei P smart 32 Гб золотистый (51092DPM)" title="Смартфон Huawei P smart 32 Гб золотистый (51092DPM)" /></a>
                        </div>
                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/samsung.png)'></span>
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
                                <span></span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/gadgets/communicators/brand-huawei/smartfon_huawei_p_smart_32_gb_zolotistiy_art80071130/" class="title">Смартфон Huawei P smart 32 Гб золотистый (51092DPM)</a>
                                <div class="product-rate">
                                    <div class="art">Арт. 80071130</div>
                                    <div class="sp-inline-rating sp-inline-rating-container-80071130"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Бренд</em><span>Huawei</span></li>
                                <li><em>Цвет</em><span>золотистый</span></li>
                                <li><em>Материал корпуса</em><span>металл<br />стекло</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>5.65"</span></li>
                                <li><em>Разрешение экрана</em><span>2160x1080</span></li>
                                <li><em>Видеопроцессор</em><span>Mali-T830 MP2</span></li>
                                <li><em>Оперативная память</em><span>3 Гб</span></li>
                                <li><em>Поддержка ГЛОНАСС</em><span>да</span></li>
                                <li><em>Количество мегапикселей основной камеры</em><span>13/2 Мп</span></li>
                                <li><em>Емкость аккумулятора</em><span>3000 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты<br />NFC<br />LTE<br />слот для карты памяти<br />Wi-Fi<br />GPS<br />3G</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>12 610 <i class="rub">a</i></strong><strong class="old-price">13 390 <i class="rub">a</i></strong>
                                <div class='-item-actions'>
                                    <a href="#" class="-add-to-favorites" data-action="add-to-favorites" data-id="<?php echo rand(1e3,1e8); ?>"></a>
                                    <a href="#" class="-add-to-compare" data-action="add-to-compare" data-id="<?php echo rand(1e3,1e8); ?>"></a>
                                </div>
                            </div>
                            <div class="sp-inline-rating sp-inline-rating-container-80071130"></div>
                            <div class="buy-block">
                                <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                    <a href="#" data-link="/basket/add/?id=80071130" data-product-id="80071130" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80071130">в корзину</a>
                                <div class="board"></div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80056332" data-product-external-id="731245">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/gadgets/communicators/brand-vivo/smartfon_vivo_y65_16_gb_zolotistiy_art80056332/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80056332.jpg" class="lazy-img" alt="Смартфон Vivo Y65 16 Гб золотистый Y65_Gold_Vivo 1719" title="Смартфон Vivo Y65 16 Гб золотистый Y65_Gold_Vivo 1719" /></a>
                        </div>
                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/samsung.png)'></span>
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
                                <span></span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/gadgets/communicators/brand-vivo/smartfon_vivo_y65_16_gb_zolotistiy_art80056332/" class="title">Смартфон Vivo Y65 16 Гб золотистый Y65_Gold_Vivo 1719</a>
                                <div class="product-rate">
                                    <div class="art">Арт. 80056332</div>
                                    <div class="sp-inline-rating sp-inline-rating-container-80056332"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Бренд</em><span>Vivo</span></li>
                                <li><em>Цвет</em><span>золотистый</span></li>
                                <li><em>Материал корпуса</em><span>пластик<br />металл</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>5.5"</span></li>
                                <li><em>Разрешение экрана</em><span>1280x720</span></li>
                                <li><em>Видеопроцессор</em><span>Adreno 308</span></li>
                                <li><em>Оперативная память</em><span>3 Гб</span></li>
                                <li><em>Поддержка ГЛОНАСС</em><span>да</span></li>
                                <li><em>Количество мегапикселей основной камеры</em><span>13 Мп</span></li>
                                <li><em>Емкость аккумулятора</em><span>3000 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты<br />LTE<br />слот для карты памяти<br />Wi-Fi<br />GPS<br />3G<br />4G</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>9 020 <i class="rub">a</i></strong><strong class="old-price">11 470 <i class="rub">a</i></strong>
                                <div class='-item-actions'>
                                    <a href="#" class="-add-to-favorites" data-action="add-to-favorites" data-id="<?php echo rand(1e3,1e8); ?>"></a>
                                    <a href="#" class="-add-to-compare" data-action="add-to-compare" data-id="<?php echo rand(1e3,1e8); ?>"></a>
                                </div>

                            </div>
                            <div class="sp-inline-rating sp-inline-rating-container-80056332"></div>
                            <div class="buy-block">
                                <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                    <a href="#" data-link="/basket/add/?id=80056332" data-product-id="80056332" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80056332">в корзину</a>
                                <div class="board"></div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80140966" data-product-external-id="815652">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-doogee/_art80140966/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80140966.jpg" class="lazy-img" alt="Смартфон Doogee X53 16Gb Black Смартфон" title="Смартфон Doogee X53 16Gb Black Смартфон" /></a>
                        </div>
                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/samsung.png)'></span>
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
                                <span></span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-doogee/_art80140966/" class="title">Смартфон Doogee X53 16Gb Black Смартфон</a>
                                <div class="product-rate">
                                    <div class="art">Арт. 80140966</div>
                                    <div class="sp-inline-rating sp-inline-rating-container-80140966"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Бренд</em><span>Doogee</span></li>
                                <li><em>Цвет</em><span>черный</span></li>
                                <li><em>Материал корпуса</em><span>металл</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>5.3"</span></li>
                                <li><em>Разрешение экрана</em><span>960x480</span></li>
                                <li><em>Видеопроцессор</em><span>Mali-400 MP1</span></li>
                                <li><em>Оперативная память</em><span>1 Гб</span></li>
                                <li><em>Поддержка ГЛОНАСС</em><span>нет</span></li>
                                <li><em>Количество мегапикселей основной камеры</em><span>5 Мп</span></li>
                                <li><em>Емкость аккумулятора</em><span>2200 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты<br />слот для карты памяти<br />Wi-Fi<br />GPS<br />3G</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>4 540 <i class="rub">a</i></strong><strong class="old-price">4 780 <i class="rub">a</i></strong>
                                <div class='-item-actions'>
                                    <a href="#" class="-add-to-favorites" data-action="add-to-favorites" data-id="<?php echo rand(1e3,1e8); ?>"></a>
                                    <a href="#" class="-add-to-compare" data-action="add-to-compare" data-id="<?php echo rand(1e3,1e8); ?>"></a>
                                </div>

                            </div>
                            <div class="sp-inline-rating sp-inline-rating-container-80140966"></div>
                            <div class="buy-block">
                                <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                    <a href="#" data-link="/basket/add/?id=80140966" data-product-id="80140966" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80140966">в корзину</a>
                                <div class="board"></div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="8958654" data-product-external-id="634612">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/gadgets/communicators/brand-samsung/smartfon_samsung_sm_g532_galaxy_j2_prime_8_gb_serebristiy_art8958654/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/8958654.jpg" class="lazy-img" alt="Смартфон Samsung SM-G532 Galaxy J2 Prime 8 Гб серебристый (SM-G532FZSDSER)" title="Смартфон Samsung SM-G532 Galaxy J2 Prime 8 Гб серебристый (SM-G532FZSDSER)" /></a>
                        </div>
                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/samsung.png)'></span>
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
                                <span></span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/gadgets/communicators/brand-samsung/smartfon_samsung_sm_g532_galaxy_j2_prime_8_gb_serebristiy_art8958654/" class="title">Смартфон Samsung SM-G532 Galaxy J2 Prime 8 Гб серебристый (SM-G532FZSDSER)</a>
                                <div class="product-rate">
                                    <div class="art">Арт. 8958654</div>
                                    <div class="sp-inline-rating sp-inline-rating-container-8958654"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Бренд</em><span>Samsung</span></li>
                                <li><em>Цвет</em><span>серебристый</span></li>
                                <li><em>Материал корпуса</em><span>пластик</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>5"</span></li>
                                <li><em>Разрешение экрана</em><span>960x540</span></li>
                                <li><em>Оперативная память</em><span>1.5 ГБ</span></li>
                                <li><em>Поддержка ГЛОНАСС</em><span>да</span></li>
                                <li><em>Количество мегапикселей основной камеры</em><span>8 Мп</span></li>
                                <li><em>Емкость аккумулятора</em><span>2600 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты<br />LTE<br />слот для карты памяти<br />Wi-Fi<br />GPS<br />3G</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>6 860 <i class="rub">a</i></strong><strong class="old-price">7 990 <i class="rub">a</i></strong>
                                <div class='-item-actions'>
                                    <a href="#" class="-add-to-favorites" data-action="add-to-favorites" data-id="<?php echo rand(1e3,1e8); ?>"></a>
                                    <a href="#" class="-add-to-compare" data-action="add-to-compare" data-id="<?php echo rand(1e3,1e8); ?>"></a>
                                </div>

                            </div>
                            <div class="sp-inline-rating sp-inline-rating-container-8958654"></div>
                            <div class="buy-block">
                                <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                    <a href="#" data-link="/basket/add/?id=8958654" data-product-id="8958654" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-8958654">в корзину</a>
                                <div class="board"></div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="8973755" data-product-external-id="649897">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/gadgets/communicators/brand-philips/smartfon_philips_xenium_x818_32_gb_cherniy_art8973755/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/8973755.jpg" class="lazy-img" alt="Смартфон Philips Xenium X818 32 Гб черный (867000139401)" title="Смартфон Philips Xenium X818 32 Гб черный (867000139401)" /></a>
                        </div>
                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/samsung.png)'></span>
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
                                <span></span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/gadgets/communicators/brand-philips/smartfon_philips_xenium_x818_32_gb_cherniy_art8973755/" class="title">Смартфон Philips Xenium X818 32 Гб черный (867000139401)</a>
                                <div class="product-rate">
                                    <div class="art">Арт. 8973755</div>
                                    <div class="sp-inline-rating sp-inline-rating-container-8973755"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Бренд</em><span>Philips</span></li>
                                <li><em>Цвет</em><span>черный</span></li>
                                <li><em>Материал корпуса</em><span>металл</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>5.5"</span></li>
                                <li><em>Разрешение экрана</em><span>1920x1080</span></li>
                                <li><em>Видеопроцессор</em><span>Mali-T860 MP2</span></li>
                                <li><em>Оперативная память</em><span>3 Гб</span></li>
                                <li><em>Емкость аккумулятора</em><span>3900 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты<br />LTE<br />слот для карты памяти<br />Wi-Fi<br />GPS<br />3G</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>9 990 <i class="rub">a</i></strong><strong class="old-price">14 850 <i class="rub">a</i></strong></div>
                            <div class="sp-inline-rating sp-inline-rating-container-8973755"></div>
                            <div class="buy-block">
                                <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                    <a href="#" data-link="/basket/add/?id=8973755" data-product-id="8973755" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-8973755">в корзину</a>
                                <div class="board"></div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80004335" data-product-external-id="678123">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/gadgets/communicators/brand-zte/smartfon_zte_blade_a520_16_gb_siniy_art80004335/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80004335.jpg" class="lazy-img" alt="Смартфон ZTE Blade A520 16 Гб синий BLADEA520BLUE" title="Смартфон ZTE Blade A520 16 Гб синий BLADEA520BLUE" /></a>
                        </div>
                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/samsung.png)'></span>
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
                                <span></span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/gadgets/communicators/brand-zte/smartfon_zte_blade_a520_16_gb_siniy_art80004335/" class="title">Смартфон ZTE Blade A520 16 Гб синий BLADEA520BLUE</a>
                                <div class="product-rate">
                                    <div class="art">Арт. 80004335</div>
                                    <div class="sp-inline-rating sp-inline-rating-container-80004335"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Бренд</em><span>ZTE</span></li>
                                <li><em>Цвет</em><span>синий</span></li>
                                <li><em>Материал корпуса</em><span>пластик</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>5"</span></li>
                                <li><em>Разрешение экрана</em><span>1280x720</span></li>
                                <li><em>Видеопроцессор</em><span>Mali-T720 MP2</span></li>
                                <li><em>Оперативная память</em><span>2 Гб</span></li>
                                <li><em>Количество мегапикселей основной камеры</em><span>8 Мп</span></li>
                                <li><em>Емкость аккумулятора</em><span>2400 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты<br />LTE<br />слот для карты памяти<br />Wi-Fi<br />GPS<br />3G</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>5 250 <i class="rub">a</i></strong><strong class="old-price">5 580 <i class="rub">a</i></strong></div>
                            <div class="sp-inline-rating sp-inline-rating-container-80004335"></div>
                            <div class="buy-block">
                                <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                    <a href="#" data-link="/basket/add/?id=80004335" data-product-id="80004335" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80004335">в корзину</a>
                                <div class="board"></div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80106767" data-product-external-id="781621">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/gadgets/communicators/brand-alcatel/smartfon_alcatel_3c_5026d_16_gb_metallik_art80106767/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80106767.jpg" class="lazy-img" alt="Смартфон Alcatel 3C 5026D 16 Гб металлик черный (5026D-2AALRU1)" title="Смартфон Alcatel 3C 5026D 16 Гб металлик черный (5026D-2AALRU1)" /></a>
                        </div>
                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/samsung.png)'></span>
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
                                <span></span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/gadgets/communicators/brand-alcatel/smartfon_alcatel_3c_5026d_16_gb_metallik_art80106767/" class="title">Смартфон Alcatel 3C 5026D 16 Гб металлик черный (5026D-2AALRU1)</a>
                                <div class="product-rate">
                                    <div class="art">Арт. 80106767</div>
                                    <div class="sp-inline-rating sp-inline-rating-container-80106767"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Бренд</em><span>Alcatel</span></li>
                                <li><em>Цвет</em><span>металлик<br />черный</span></li>
                                <li><em>Материал корпуса</em><span>пластик</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>6"</span></li>
                                <li><em>Разрешение экрана</em><span>1440x720</span></li>
                                <li><em>Видеопроцессор</em><span>Mali-400 MP</span></li>
                                <li><em>Оперативная память</em><span>1 Гб</span></li>
                                <li><em>Поддержка ГЛОНАСС</em><span>нет</span></li>
                                <li><em>Количество мегапикселей основной камеры</em><span>8 Мп</span></li>
                                <li><em>Емкость аккумулятора</em><span>3000 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты<br />слот для карты памяти<br />Wi-Fi<br />GPS<br />3G</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>5 550 <i class="rub">a</i></strong><strong class="old-price">6 990 <i class="rub">a</i></strong></div>
                            <div class="sp-inline-rating sp-inline-rating-container-80106767"></div>
                            <div class="buy-block">
                                <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                    <a href="#" data-link="/basket/add/?id=80106767" data-product-id="80106767" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80106767">в корзину</a>
                                <div class="board"></div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80098279" data-product-external-id="773133">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/gadgets/communicators/brand-huawei/smartfon_huawei_p20_lite_64_gb_siniy_art80098279/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80098279.jpg" class="lazy-img" alt="Смартфон Huawei P20 Lite 64 Гб синий (51092GYT)" title="Смартфон Huawei P20 Lite 64 Гб синий (51092GYT)" /></a>
                        </div>
                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/samsung.png)'></span>
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
                                <span></span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/gadgets/communicators/brand-huawei/smartfon_huawei_p20_lite_64_gb_siniy_art80098279/" class="title">Смартфон Huawei P20 Lite 64 Гб синий (51092GYT)</a>
                                <div class="product-rate">
                                    <div class="art">Арт. 80098279</div>
                                    <div class="sp-inline-rating sp-inline-rating-container-80098279"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Бренд</em><span>Huawei</span></li>
                                <li><em>Цвет</em><span>синий</span></li>
                                <li><em>Материал корпуса</em><span>стекло</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>5.84"</span></li>
                                <li><em>Разрешение экрана</em><span>2280x1080</span></li>
                                <li><em>Видеопроцессор</em><span>Mali-T830 MP2</span></li>
                                <li><em>Оперативная память</em><span>4 Гб</span></li>
                                <li><em>Поддержка ГЛОНАСС</em><span>да</span></li>
                                <li><em>Количество мегапикселей основной камеры</em><span>16/2 Мп</span></li>
                                <li><em>Емкость аккумулятора</em><span>3000 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты<br />NFC<br />LTE<br />слот для карты памяти<br />Wi-Fi<br />GPS<br />3G</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>16 280 <i class="rub">a</i></strong><strong class="old-price">18 810 <i class="rub">a</i></strong></div>
                            <div class="sp-inline-rating sp-inline-rating-container-80098279"></div>
                            <div class="buy-block">
                                <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                    <a href="#" data-link="/basket/add/?id=80098279" data-product-id="80098279" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80098279">в корзину</a>
                                <div class="board"></div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80140956" data-product-external-id="815642">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-huawei/_art80140956/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80140956.jpg" class="lazy-img" alt="Смартфон Huawei Y6 Prime 2018 16 Гб золотистый 51092KQG" title="Смартфон Huawei Y6 Prime 2018 16 Гб золотистый 51092KQG" /></a>
                        </div>
                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/samsung.png)'></span>
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
                                <span></span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-huawei/_art80140956/" class="title">Смартфон Huawei Y6 Prime 2018 16 Гб золотистый 51092KQG</a>
                                <div class="product-rate">
                                    <div class="art">Арт. 80140956</div>
                                    <div class="sp-inline-rating sp-inline-rating-container-80140956"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Бренд</em><span>Huawei</span></li>
                                <li><em>Цвет</em><span>золотистый</span></li>
                                <li><em>Материал корпуса</em><span>н/д</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>5.7"</span></li>
                                <li><em>Разрешение экрана</em><span>1440x720</span></li>
                                <li><em>Видеопроцессор</em><span>Adreno 308</span></li>
                                <li><em>Оперативная память</em><span>2 Гб</span></li>
                                <li><em>Поддержка ГЛОНАСС</em><span>да</span></li>
                                <li><em>Количество мегапикселей основной камеры</em><span>13 Мп</span></li>
                                <li><em>Емкость аккумулятора</em><span>3000 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты<br />LTE<br />слот для карты памяти<br />Wi-Fi<br />GPS<br />3G</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>8 200 <i class="rub">a</i></strong></div>
                            <div class="sp-inline-rating sp-inline-rating-container-80140956"></div>
                            <div class="buy-block">
                                <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                    <a href="#" data-link="/basket/add/?id=80140956" data-product-id="80140956" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80140956">в корзину</a>
                                <div class="board"></div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="8839982" data-product-external-id="512525">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/gadgets/communicators/brand-philips/smartfon_philips_s616_16_gb_cherniy_art8839982/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/8839982.jpg" class="lazy-img" alt="Смартфон Philips S616 16 Гб черный Dark Grey" title="Смартфон Philips S616 16 Гб черный Dark Grey" /></a>
                        </div>
                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/samsung.png)'></span>
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
                                <span></span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/gadgets/communicators/brand-philips/smartfon_philips_s616_16_gb_cherniy_art8839982/" class="title">Смартфон Philips S616 16 Гб черный Dark Grey</a>
                                <div class="product-rate">
                                    <div class="art">Арт. 8839982</div>
                                    <div class="sp-inline-rating sp-inline-rating-container-8839982"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Бренд</em><span>Philips</span></li>
                                <li><em>Цвет</em><span>черный</span></li>
                                <li><em>Материал корпуса</em><span>пластик</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>5.5"</span></li>
                                <li><em>Разрешение экрана</em><span>1920x1080</span></li>
                                <li><em>Видеопроцессор</em><span>Mali-T720</span></li>
                                <li><em>Оперативная память</em><span>2 Гб</span></li>
                                <li><em>Количество мегапикселей основной камеры</em><span>13 Мп</span></li>
                                <li><em>Емкость аккумулятора</em><span>3000 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты<br />LTE<br />слот для карты памяти<br />Wi-Fi<br />GPS<br />3G</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>7 990 <i class="rub">a</i></strong><strong class="old-price">13 680 <i class="rub">a</i></strong></div>
                            <div class="sp-inline-rating sp-inline-rating-container-8839982"></div>
                            <div class="buy-block">
                                <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                    <a href="#" data-link="/basket/add/?id=8839982" data-product-id="8839982" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-8839982">в корзину</a>
                                <div class="board"></div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80213145" data-product-external-id="887860">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-apple/_art80213145/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80213145.jpg" class="lazy-img" alt="Смартфон Apple iPhone SE &quot;Как новый&quot; 128 Гб серый (FP862RU/A)" title="Смартфон Apple iPhone SE &quot;Как новый&quot; 128 Гб серый (FP862RU/A)" /></a>
                        </div>
                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/samsung.png)'></span>
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
                                <span></span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-apple/_art80213145/" class="title">Смартфон Apple iPhone SE &quot;Как новый&quot; 128 Гб серый (FP862RU/A)</a>
                                <div class="product-rate">
                                    <div class="art">Арт. 80213145</div>
                                    <div class="sp-inline-rating sp-inline-rating-container-80213145"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Бренд</em><span>Apple</span></li>
                                <li><em>Цвет</em><span>серый</span></li>
                                <li><em>Материал корпуса</em><span>металл<br />стекло</span></li>
                                <li><em>Операционная система</em><span>Apple iOS</span></li>
                                <li><em>Диагональ экрана</em><span>4"</span></li>
                                <li><em>Разрешение экрана</em><span>1136x640</span></li>
                                <li><em>Поддержка ГЛОНАСС</em><span>да</span></li>
                                <li><em>Количество мегапикселей основной камеры</em><span>12 Мп</span></li>
                                <li><em>Возможности</em><span>NFC<br />LTE<br />Wi-Fi<br />GPS<br />3G</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>20 330 <i class="rub">a</i></strong></div>
                            <div class="sp-inline-rating sp-inline-rating-container-80213145"></div>
                            <div class="buy-block">
                                <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                    <a href="#" data-link="/basket/add/?id=80213145" data-product-id="80213145" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80213145">в корзину</a>
                                <div class="board"></div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80126854" data-product-external-id="801707">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/gadgets/communicators/brand-samsung/smartfon_samsung_galaxy_a6_2018_siniy_6_32_gb_nfc_lte_wi_fi_gps_3g_sm_a605f_art80126854/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80126854.jpg" class="lazy-img" alt="Смартфон Samsung Galaxy A6+ 2018 32 Гб синий SM-A605F" title="Смартфон Samsung Galaxy A6+ 2018 32 Гб синий SM-A605F" /></a>
                        </div>
                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/samsung.png)'></span>
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
                                <span></span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/gadgets/communicators/brand-samsung/smartfon_samsung_galaxy_a6_2018_siniy_6_32_gb_nfc_lte_wi_fi_gps_3g_sm_a605f_art80126854/" class="title">Смартфон Samsung Galaxy A6+ 2018 32 Гб синий SM-A605F</a>
                                <div class="product-rate">
                                    <div class="art">Арт. 80126854</div>
                                    <div class="sp-inline-rating sp-inline-rating-container-80126854"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Бренд</em><span>Samsung</span></li>
                                <li><em>Цвет</em><span>синий</span></li>
                                <li><em>Материал корпуса</em><span>пластик<br />металл</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>6"</span></li>
                                <li><em>Разрешение экрана</em><span>2220x1080</span></li>
                                <li><em>Видеопроцессор</em><span>Adreno 506</span></li>
                                <li><em>Оперативная память</em><span>3 Гб</span></li>
                                <li><em>Поддержка ГЛОНАСС</em><span>да</span></li>
                                <li><em>Количество мегапикселей основной камеры</em><span>16/5 Мп</span></li>
                                <li><em>Емкость аккумулятора</em><span>3500 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты<br />NFC<br />LTE<br />слот для карты памяти<br />Wi-Fi<br />GPS<br />3G</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>16 150 <i class="rub">a</i></strong><strong class="old-price">21 990 <i class="rub">a</i></strong></div>
                            <div class="sp-inline-rating sp-inline-rating-container-80126854"></div>
                            <div class="buy-block">
                                <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                    <a href="#" data-link="/basket/add/?id=80126854" data-product-id="80126854" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80126854">в корзину</a>
                                <div class="board"></div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80133416" data-product-external-id="808270">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/gadgets/communicators/brand-alcatel/_art80133416/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80133416.jpg" class="lazy-img" alt="Смартфон Alcatel 3X 5058I 32 Гб металлик черный 5058I-2AALRU1" title="Смартфон Alcatel 3X 5058I 32 Гб металлик черный 5058I-2AALRU1" /></a>
                        </div>
                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/samsung.png)'></span>
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
                                <span></span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/gadgets/communicators/brand-alcatel/_art80133416/" class="title">Смартфон Alcatel 3X 5058I 32 Гб металлик черный 5058I-2AALRU1</a>
                                <div class="product-rate">
                                    <div class="art">Арт. 80133416</div>
                                    <div class="sp-inline-rating sp-inline-rating-container-80133416"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Бренд</em><span>Alcatel</span></li>
                                <li><em>Цвет</em><span>металлик<br />черный</span></li>
                                <li><em>Материал корпуса</em><span>пластик<br />металл</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>5.7"</span></li>
                                <li><em>Разрешение экрана</em><span>1440x720</span></li>
                                <li><em>Видеопроцессор</em><span>PowerVR GE8100</span></li>
                                <li><em>Оперативная память</em><span>3 Гб</span></li>
                                <li><em>Поддержка ГЛОНАСС</em><span>нет</span></li>
                                <li><em>Количество мегапикселей основной камеры</em><span>13/5 Мп</span></li>
                                <li><em>Емкость аккумулятора</em><span>3000 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты<br />LTE<br />слот для карты памяти<br />Wi-Fi<br />GPS<br />3G</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>6 720 <i class="rub">a</i></strong><strong class="old-price">9 990 <i class="rub">a</i></strong></div>
                            <div class="sp-inline-rating sp-inline-rating-container-80133416"></div>
                            <div class="buy-block">
                                <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                    <a href="#" data-link="/basket/add/?id=80133416" data-product-id="80133416" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80133416">в корзину</a>
                                <div class="board"></div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80089825" data-product-external-id="764679">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/gadgets/communicators/brand-neffos/smartfon_neffos_c7_16_gb_seriy_art80089825/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80089825.jpg" class="lazy-img" alt="Смартфон Neffos C7 16 Гб серый (TP910A24RU)" title="Смартфон Neffos C7 16 Гб серый (TP910A24RU)" /></a>
                        </div>
                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/samsung.png)'></span>
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
                                <span></span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/gadgets/communicators/brand-neffos/smartfon_neffos_c7_16_gb_seriy_art80089825/" class="title">Смартфон Neffos C7 16 Гб серый (TP910A24RU)</a>
                                <div class="product-rate">
                                    <div class="art">Арт. 80089825</div>
                                    <div class="sp-inline-rating sp-inline-rating-container-80089825"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Бренд</em><span>Neffos</span></li>
                                <li><em>Цвет</em><span>серый</span></li>
                                <li><em>Материал корпуса</em><span>пластик</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>5.5"</span></li>
                                <li><em>Разрешение экрана</em><span>1280x720</span></li>
                                <li><em>Видеопроцессор</em><span>Mali-T860 MP2</span></li>
                                <li><em>Оперативная память</em><span>2 Гб</span></li>
                                <li><em>Поддержка ГЛОНАСС</em><span>да</span></li>
                                <li><em>Количество мегапикселей основной камеры</em><span>13 Мп</span></li>
                                <li><em>Емкость аккумулятора</em><span>3060 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты<br />LTE<br />слот для карты памяти<br />Wi-Fi<br />GPS<br />3G</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>6 560 <i class="rub">a</i></strong><strong class="old-price">7 380 <i class="rub">a</i></strong></div>
                            <div class="sp-inline-rating sp-inline-rating-container-80089825"></div>
                            <div class="buy-block">
                                <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                    <a href="#" data-link="/basket/add/?id=80089825" data-product-id="80089825" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80089825">в корзину</a>
                                <div class="board"></div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80008216" data-product-external-id="682081">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/gadgets/communicators/brand-alcatel/smartfon_alcatel_u5_5044d_8_gb_seriy_art80008216/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80008216.jpg" class="lazy-img" alt="Смартфон Alcatel U5 5044D 8 Гб серый (5044D-2AALRU1)" title="Смартфон Alcatel U5 5044D 8 Гб серый (5044D-2AALRU1)" /></a>
                        </div>
                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/samsung.png)'></span>
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
                                <span></span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/gadgets/communicators/brand-alcatel/smartfon_alcatel_u5_5044d_8_gb_seriy_art80008216/" class="title">Смартфон Alcatel U5 5044D 8 Гб серый (5044D-2AALRU1)</a>
                                <div class="product-rate">
                                    <div class="art">Арт. 80008216</div>
                                    <div class="sp-inline-rating sp-inline-rating-container-80008216"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Бренд</em><span>Alcatel</span></li>
                                <li><em>Цвет</em><span>серый</span></li>
                                <li><em>Материал корпуса</em><span>пластик</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>5"</span></li>
                                <li><em>Разрешение экрана</em><span>854x480</span></li>
                                <li><em>Видеопроцессор</em><span>Mali-T720 MP2</span></li>
                                <li><em>Оперативная память</em><span>1 Гб</span></li>
                                <li><em>Количество мегапикселей основной камеры</em><span>5 Мп</span></li>
                                <li><em>Емкость аккумулятора</em><span>2050 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты<br />LTE<br />слот для карты памяти<br />Wi-Fi<br />GPS<br />3G</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>4 330 <i class="rub">a</i></strong><strong class="old-price">4 890 <i class="rub">a</i></strong></div>
                            <div class="sp-inline-rating sp-inline-rating-container-80008216"></div>
                            <div class="buy-block">
                                <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                    <a href="#" data-link="/basket/add/?id=80008216" data-product-id="80008216" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80008216">в корзину</a>
                                <div class="board"></div>
                            </div>

                        </div>
                    </div>
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80203102" data-product-external-id="877817">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-nokia/_art80203102/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80203102.jpg" class="lazy-img" alt="Смартфон NOKIA 5.1 16 Гб черный (TA-1075)" title="Смартфон NOKIA 5.1 16 Гб черный (TA-1075)" /></a>
                        </div>
                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/samsung.png)'></span>
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
                                <span></span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-nokia/_art80203102/" class="title">Смартфон NOKIA 5.1 16 Гб черный (TA-1075)</a>
                                <div class="product-rate">
                                    <div class="art">Арт. 80203102</div>
                                    <div class="sp-inline-rating sp-inline-rating-container-80203102"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Бренд</em><span>NOKIA</span></li>
                                <li><em>Цвет</em><span>черный</span></li>
                                <li><em>Материал корпуса</em><span>алюминий</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>5.5"</span></li>
                                <li><em>Разрешение экрана</em><span>2160x1080</span></li>
                                <li><em>Видеопроцессор</em><span>Mali-T860 MP2</span></li>
                                <li><em>Оперативная память</em><span>2 Гб</span></li>
                                <li><em>Поддержка ГЛОНАСС</em><span>да</span></li>
                                <li><em>Количество мегапикселей основной камеры</em><span>16 Мп</span></li>
                                <li><em>Емкость аккумулятора</em><span>3000 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>12 110 <i class="rub">a</i></strong><strong class="old-price">12 830 <i class="rub">a</i></strong></div>
                            <div class="sp-inline-rating sp-inline-rating-container-80203102"></div>
                            <div class="buy-block">
                                <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                    <a href="#" data-link="/basket/add/?id=80203102" data-product-id="80203102" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80203102">в корзину</a>
                                <div class="board"></div>
                            </div>

                        </div>
                    </div>
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80025886" data-product-external-id="699986">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/gadgets/communicators/brand-philips/smartfon_philips_xenium_s327_8_gb_siniy_art80025886/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80025886.jpg" class="lazy-img" alt="Смартфон Philips Xenium S327 8 Гб синий (867000145531)" title="Смартфон Philips Xenium S327 8 Гб синий (867000145531)" /></a>
                        </div>
                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/samsung.png)'></span>
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
                                <span></span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/gadgets/communicators/brand-philips/smartfon_philips_xenium_s327_8_gb_siniy_art80025886/" class="title">Смартфон Philips Xenium S327 8 Гб синий (867000145531)</a>
                                <div class="product-rate">
                                    <div class="art">Арт. 80025886</div>
                                    <div class="sp-inline-rating sp-inline-rating-container-80025886"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Бренд</em><span>Philips</span></li>
                                <li><em>Цвет</em><span>синий</span></li>
                                <li><em>Материал корпуса</em><span>пластик</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>5.5"</span></li>
                                <li><em>Разрешение экрана</em><span>1280x720</span></li>
                                <li><em>Видеопроцессор</em><span>Mali-T720 MP2</span></li>
                                <li><em>Оперативная память</em><span>1 Гб</span></li>
                                <li><em>Количество мегапикселей основной камеры</em><span>8 Мп</span></li>
                                <li><em>Емкость аккумулятора</em><span>3000 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты<br />LTE<br />слот для карты памяти<br />Wi-Fi<br />GPS<br />3G</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>4 860 <i class="rub">a</i></strong><strong class="old-price">6 020 <i class="rub">a</i></strong></div>
                            <div class="sp-inline-rating sp-inline-rating-container-80025886"></div>
                            <div class="buy-block">
                                <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                    <a href="#" data-link="/basket/add/?id=80025886" data-product-id="80025886" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80025886">в корзину</a>
                                <div class="board"></div>
                            </div>

                        </div>
                    </div>
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80104412" data-product-external-id="779266">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/gadgets/communicators/brand-xiaomi/smartfon_xiaomi_redmi_5_16_gb_cherniy_art80104412/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80104412.jpg" class="lazy-img" alt="Смартфон Xiaomi Redmi 5 16 Гб черный" title="Смартфон Xiaomi Redmi 5 16 Гб черный" /></a>
                        </div>
                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/samsung.png)'></span>
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
                                <span></span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/gadgets/communicators/brand-xiaomi/smartfon_xiaomi_redmi_5_16_gb_cherniy_art80104412/" class="title">Смартфон Xiaomi Redmi 5 16 Гб черный</a>
                                <div class="product-rate">
                                    <div class="art">Арт. 80104412</div>
                                    <div class="sp-inline-rating sp-inline-rating-container-80104412"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Бренд</em><span>Xiaomi</span></li>
                                <li><em>Цвет</em><span>черный</span></li>
                                <li><em>Материал корпуса</em><span>металл</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>5.7"</span></li>
                                <li><em>Разрешение экрана</em><span>1440x720</span></li>
                                <li><em>Видеопроцессор</em><span>Adreno 506</span></li>
                                <li><em>Оперативная память</em><span>2 Гб</span></li>
                                <li><em>Поддержка ГЛОНАСС</em><span>да</span></li>
                                <li><em>Количество мегапикселей основной камеры</em><span>12 Мп</span></li>
                                <li><em>Емкость аккумулятора</em><span>3300 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты<br />LTE<br />слот для карты памяти<br />Wi-Fi<br />GPS<br />3G</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>9 680 <i class="rub">a</i></strong><strong class="old-price">10 070 <i class="rub">a</i></strong></div>
                            <div class="sp-inline-rating sp-inline-rating-container-80104412"></div>
                            <div class="buy-block">
                                <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                    <a href="#" data-link="/basket/add/?id=80104412" data-product-id="80104412" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80104412">в корзину</a>
                                <div class="board"></div>
                            </div>

                        </div>
                    </div>
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80059382" data-product-external-id="734340">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/gadgets/communicators/brand-zte/smartfon_zte_blade_a610_16_gb_seriy_art80059382/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80059382.jpg" class="lazy-img" alt="Смартфон ZTE Blade A610 16 Гб серый" title="Смартфон ZTE Blade A610 16 Гб серый" /></a>
                        </div>
                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/samsung.png)'></span>
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
                                <span></span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/gadgets/communicators/brand-zte/smartfon_zte_blade_a610_16_gb_seriy_art80059382/" class="title">Смартфон ZTE Blade A610 16 Гб серый</a>
                                <div class="product-rate">
                                    <div class="art">Арт. 80059382</div>
                                    <div class="sp-inline-rating sp-inline-rating-container-80059382"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Бренд</em><span>ZTE</span></li>
                                <li><em>Цвет</em><span>серый</span></li>
                                <li><em>Материал корпуса</em><span>пластик</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>5"</span></li>
                                <li><em>Разрешение экрана</em><span>1280x720</span></li>
                                <li><em>Оперативная память</em><span>2 Гб</span></li>
                                <li><em>Поддержка ГЛОНАСС</em><span>да</span></li>
                                <li><em>Количество мегапикселей основной камеры</em><span>13 Мп</span></li>
                                <li><em>Емкость аккумулятора</em><span>4000 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты<br />защита от пыли и влаги<br />LTE<br />слот для карты памяти<br />Wi-Fi<br />GPS<br />3G</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>6 160 <i class="rub">a</i></strong><strong class="old-price">8 130 <i class="rub">a</i></strong></div>
                            <div class="sp-inline-rating sp-inline-rating-container-80059382"></div>
                            <div class="buy-block">
                                <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                    <a href="#" data-link="/basket/add/?id=80059382" data-product-id="80059382" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80059382">в корзину</a>
                                <div class="board"></div>
                            </div>

                        </div>
                    </div>
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80029174" data-product-external-id="703461">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/gadgets/communicators/brand-motorola/smartfon_motorola_g5s_32_gb_zolotistiy_art80029174/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80029174.jpg" class="lazy-img" alt="Смартфон Motorola G5S 32 Гб золотистый (PA7W0022RU)" title="Смартфон Motorola G5S 32 Гб золотистый (PA7W0022RU)" /></a>
                        </div>
                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/samsung.png)'></span>
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
                                <span></span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/gadgets/communicators/brand-motorola/smartfon_motorola_g5s_32_gb_zolotistiy_art80029174/" class="title">Смартфон Motorola G5S 32 Гб золотистый (PA7W0022RU)</a>
                                <div class="product-rate">
                                    <div class="art">Арт. 80029174</div>
                                    <div class="sp-inline-rating sp-inline-rating-container-80029174"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Бренд</em><span>Motorola</span></li>
                                <li><em>Цвет</em><span>золотистый</span></li>
                                <li><em>Материал корпуса</em><span>металл</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>5.2"</span></li>
                                <li><em>Разрешение экрана</em><span>1920x1080</span></li>
                                <li><em>Видеопроцессор</em><span>Adreno 505</span></li>
                                <li><em>Оперативная память</em><span>3 Гб</span></li>
                                <li><em>Поддержка ГЛОНАСС</em><span>да</span></li>
                                <li><em>Количество мегапикселей основной камеры</em><span>16 Мп</span></li>
                                <li><em>Емкость аккумулятора</em><span>2000 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты<br />LTE<br />слот для карты памяти<br />Wi-Fi<br />GPS<br />3G</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>9 180 <i class="rub">a</i></strong><strong class="old-price">10 150 <i class="rub">a</i></strong></div>
                            <div class="sp-inline-rating sp-inline-rating-container-80029174"></div>
                            <div class="buy-block">
                                <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                    <a href="#" data-link="/basket/add/?id=80029174" data-product-id="80029174" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80029174">в корзину</a>
                                <div class="board"></div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80058648" data-product-external-id="733621">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/gadgets/communicators/brand-samsung/smartfon_samsung_galaxy_a8_2018_32_gb_siniy_art80058648/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80058648.jpg" class="lazy-img" alt="Смартфон Samsung Galaxy A8 (2018) 32 Гб синий (SM-A530FZBDSER)" title="Смартфон Samsung Galaxy A8 (2018) 32 Гб синий (SM-A530FZBDSER)" /></a>
                        </div>
                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/samsung.png)'></span>
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
                                <span></span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/gadgets/communicators/brand-samsung/smartfon_samsung_galaxy_a8_2018_32_gb_siniy_art80058648/" class="title">Смартфон Samsung Galaxy A8 (2018) 32 Гб синий (SM-A530FZBDSER)</a>
                                <div class="product-rate">
                                    <div class="art">Арт. 80058648</div>
                                    <div class="sp-inline-rating sp-inline-rating-container-80058648"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Бренд</em><span>Samsung</span></li>
                                <li><em>Цвет</em><span>синий</span></li>
                                <li><em>Материал корпуса</em><span>металл<br />стекло</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>5.6"</span></li>
                                <li><em>Разрешение экрана</em><span>2220x1080</span></li>
                                <li><em>Видеопроцессор</em><span>Mali-G71</span></li>
                                <li><em>Оперативная память</em><span>4 Гб</span></li>
                                <li><em>Поддержка ГЛОНАСС</em><span>да</span></li>
                                <li><em>Количество мегапикселей основной камеры</em><span>16 Мп</span></li>
                                <li><em>Емкость аккумулятора</em><span>3000 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты<br />NFC<br />LTE<br />слот для карты памяти<br />Wi-Fi<br />GPS</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>20 280 <i class="rub">a</i></strong><strong class="old-price">24 990 <i class="rub">a</i></strong></div>
                            <div class="sp-inline-rating sp-inline-rating-container-80058648"></div>
                            <div class="buy-block">
                                <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                    <a href="#" data-link="/basket/add/?id=80058648" data-product-id="80058648" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80058648">в корзину</a>
                                <div class="board"></div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80164677" data-product-external-id="839387">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-ark/_art80164677/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80164677.jpg" class="lazy-img" alt="Смартфон ARK Benefit S505 8 Гб черный" title="Смартфон ARK Benefit S505 8 Гб черный" /></a>
                        </div>
                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/samsung.png)'></span>
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
                                <span></span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-ark/_art80164677/" class="title">Смартфон ARK Benefit S505 8 Гб черный</a>
                                <div class="product-rate">
                                    <div class="art">Арт. 80164677</div>
                                    <div class="sp-inline-rating sp-inline-rating-container-80164677"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Бренд</em><span>ARK</span></li>
                                <li><em>Цвет</em><span>черный</span></li>
                                <li><em>Материал корпуса</em><span>пластик</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>5"</span></li>
                                <li><em>Разрешение экрана</em><span>854x480</span></li>
                                <li><em>Видеопроцессор</em><span>Mali-400 MP2</span></li>
                                <li><em>Оперативная память</em><span>1 Гб</span></li>
                                <li><em>Поддержка ГЛОНАСС</em><span>нет</span></li>
                                <li><em>Количество мегапикселей основной камеры</em><span>5 Мп</span></li>
                                <li><em>Емкость аккумулятора</em><span>2000 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты<br />слот для карты памяти<br />Wi-Fi<br />GPS<br />3G</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>3 190 <i class="rub">a</i></strong><strong class="old-price">3 400 <i class="rub">a</i></strong></div>
                            <div class="sp-inline-rating sp-inline-rating-container-80164677"></div>
                            <div class="buy-block">
                                <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                    <a href="#" data-link="/basket/add/?id=80164677" data-product-id="80164677" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80164677">в корзину</a>
                                <div class="board"></div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80062360" data-product-external-id="737413">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/gadgets/communicators/brand-xiaomi/smartfon_xiaomi_redmi_note_5a_prime_32_gb_zolotistiy_art80062360/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80062360.jpg" class="lazy-img" alt="Смартфон Xiaomi Redmi Note 5A Prime 32 Гб золотистый" title="Смартфон Xiaomi Redmi Note 5A Prime 32 Гб золотистый" /></a>
                        </div>
                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/samsung.png)'></span>
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
                                <span></span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/gadgets/communicators/brand-xiaomi/smartfon_xiaomi_redmi_note_5a_prime_32_gb_zolotistiy_art80062360/" class="title">Смартфон Xiaomi Redmi Note 5A Prime 32 Гб золотистый</a>
                                <div class="product-rate">
                                    <div class="art">Арт. 80062360</div>
                                    <div class="sp-inline-rating sp-inline-rating-container-80062360"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Бренд</em><span>Xiaomi</span></li>
                                <li><em>Цвет</em><span>золотистый</span></li>
                                <li><em>Материал корпуса</em><span>пластик<br />металл</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>5.5"</span></li>
                                <li><em>Разрешение экрана</em><span>1280x720</span></li>
                                <li><em>Видеопроцессор</em><span>Adreno 505</span></li>
                                <li><em>Оперативная память</em><span>3 Гб</span></li>
                                <li><em>Поддержка ГЛОНАСС</em><span>да</span></li>
                                <li><em>Количество мегапикселей основной камеры</em><span>13 Мп</span></li>
                                <li><em>Емкость аккумулятора</em><span>3080 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты<br />LTE<br />слот для карты памяти<br />Wi-Fi<br />GPS</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>10 280 <i class="rub">a</i></strong><strong class="old-price">12 620 <i class="rub">a</i></strong></div>
                            <div class="sp-inline-rating sp-inline-rating-container-80062360"></div>
                            <div class="buy-block">
                                <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                    <a href="#" data-link="/basket/add/?id=80062360" data-product-id="80062360" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80062360">в корзину</a>
                                <div class="board"></div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80148022" data-product-external-id="822710">
                        <div class="hover"></div>
                        <div class="image-container">
                            <a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-vertex/_art80148022/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80148022.jpg" class="lazy-img" alt="Смартфон Vertex Impress Frost 16 Гб черный VRX-VFRST-BLK" title="Смартфон Vertex Impress Frost 16 Гб черный VRX-VFRST-BLK" /></a>
                        </div>
                        <div class="info">
                            <div class="-rt">
                                <span class="-brand-line" style='background-image: url(/img/samsung.png)'></span>
                                <div class="-rating ">
                                    <div class='sp-product-inline-rating-widget'>
                                        <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div>
                                        <strong><a href="#">13</a></strong>
                                    </div>
                                </div>
                                <span></span>
                            </div>
                            <div class="title-block"><a href='#' class='-expander'></a><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-vertex/_art80148022/" class="title">Смартфон Vertex Impress Frost 16 Гб черный VRX-VFRST-BLK</a>
                                <div class="product-rate">
                                    <div class="art">Арт. 80148022</div>
                                    <div class="sp-inline-rating sp-inline-rating-container-80148022"></div>
                                </div>
                            </div>
                            <ul class="product-params clearstyle">
                                <li><em>Бренд</em><span>Vertex</span></li>
                                <li><em>Цвет</em><span>черный</span></li>
                                <li><em>Материал корпуса</em><span>пластик</span></li>
                                <li><em>Операционная система</em><span>Android</span></li>
                                <li><em>Диагональ экрана</em><span>5.5"</span></li>
                                <li><em>Разрешение экрана</em><span>1280x720</span></li>
                                <li><em>Видеопроцессор</em><span>Mali-T720 MP2</span></li>
                                <li><em>Оперативная память</em><span>2 Гб</span></li>
                                <li><em>Поддержка ГЛОНАСС</em><span>нет</span></li>
                                <li><em>Количество мегапикселей основной камеры</em><span>8 Мп</span></li>
                                <li><em>Емкость аккумулятора</em><span>2700 мАч</span></li>
                                <li><em>Возможности</em><span>2 симкарты<br />NFC<br />LTE<br />слот для карты памяти<br />Wi-Fi<br />GPS<br />3G</span></li>
                            </ul>
                        </div>
                        <div class="price-block">
                            <div class="price"><strong>6 640 <i class="rub">a</i></strong></div>
                            <div class="sp-inline-rating sp-inline-rating-container-80148022"></div>
                            <div class="buy-block">
                                <div class="countbox">
                                    <a href="#" class="minus"></a>
                                    <input type="text" name="count" value="1" data-min="1" data-max="4" data-hold="true" readonly="true">
                                    <a href="#" class="plus"></a>
                                </div>
                                    <a href="#" data-link="/basket/add/?id=80148022" data-product-id="80148022" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80148022">в корзину</a>
                                <div class="board"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pages-navigation">
                    <div class="show-more" data-link="/smartfoni_plansheti_gadjeti/telefoniya/page-2/"><a href="#">Показать еще <span>30</span> товаров</a></div>
                    <ul class="pages-list">
                        <li class="current"><span>1</span></li>
                        <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/page-2/" data-pager="2" data-items-count="30">2</a></li>
                        <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/page-3/" data-pager="3" data-items-count="30">3</a></li>
                        <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/page-4/" data-pager="4" data-items-count="30">4</a></li>
                        <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/page-5/" data-pager="5" data-items-count="30">5</a></li>
                        <li class="more"><a href="#">...</a></li>
                        <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/page-32/" data-pager="32" data-items-count="28">32</a></li>
                        <li class="arrow right">
                            <a href="/smartfoni_plansheti_gadjeti/telefoniya/page-2/" data-pager="2" data-items-count="30"></a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="clearfix"></div>
        </div>
    </section>
    <section class="container bottom-elements">
        <div class="popular-brands-list">
            <h4>Популярные бренды</h4>
            <ul>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-alcatel/">Alcatel</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-apple/">Apple</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-ark/">ARK</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-asus/">ASUS</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-black_fox/">Black Fox</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-bq/">BQ</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-caterpillar/">Caterpillar</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-digma/">Digma</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-doogee/">Doogee</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-elephone/">Elephone</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-fly/">Fly</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-honor/">Honor</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-htc/">HTC</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-huawei/">Huawei</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-irbis/">Irbis</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-lenovo/">Lenovo</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-lg/">LG</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-meizu/">Meizu</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-micromax/">Micromax</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-motorola/">Motorola</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-neffos/">Neffos</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-nokia/">NOKIA</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-oneplus/">OnePlus</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-oppo/">Oppo</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-oukitel/">Oukitel</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-philips/">Philips</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-prestigio/">Prestigio</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-samsung/">Samsung</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-sony/">SONY</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-tecno/">Tecno</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-texet/">Texet</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-vertex/">Vertex</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-vivo/">Vivo</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-xiaomi/">Xiaomi</a></li>
                <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/brand-zte/">ZTE</a></li>
                <li class="show-more"><a href="#">показать еще</a></li>
            </ul>
        </div>
        <div class="page-description-text">
            <h2>Предлагаем купить смартфоны и гаджеты в интернет-магазине 123.ru</h2>
            <br />
            <br /> &#x27a4; Смартфоны купить в интернет-магазине 123.ru: — официальная гарантия — забирай заказ в своем городе — более 958 моделей по лучшей цене в Москве и России. Продажа только сертифицированных на территории РФ товаров. Более 6500 пунктов самовывоза по всей стране, оповещение о статусе заказа через SMS, персональный консультант ответит на все вопросы. Постоянные акции и скидки, стоимость товаров ниже, чем у конкурентов. Мы поможем выбрать удобный и дешевый способ доставки. Смотрите фото, читайте описание и характеристики, только настоящие отзывы в каталоге нашего гипермаркета. С нами действительно недорого!</div>
    </section>
    <div class="window box-shadow" id="filter-window"><a href="#" data-action="close" class="open-xs-filter ">Фильтр и сортировка <em></em></a>
        <div class="appendData"></div>
    </div>
    <div data-retailrocket-markup-block="599eda3dc7d01041d82b9179" data-products-price="price69" data-category-id="Смартфоны планшеты гаджеты/Смартфоны"></div>
    <section id="advantages">
        <div class="container">
            <div class="content-cnt">
                <div class="wrapp-advantages">
                    <div class="item">
                        <div class="image"><img alt="image" src="/i/icon_avnt3.svg" style="height:77px"></div>
                        <p>Официальная
                            <br> гарантия</p>
                    </div>
                    <div class="item">
                        <div class="image"><img alt="image" src="/i/icon_avnt4.svg" style="height:77px"></div>
                        <p>Бонусная программа</p>
                    </div>
                    <div class="item">
                        <div class="image"><img alt="image" src="/i/icon_avnt1.svg" style="height:77px"></div>
                        <p>6779 пунктов
                            <br />самовывоза</p>
                    </div>
                    <div class="item">
                        <div class="image"><img alt="image" src="/i/icon_avnt5.svg" style="height:77px"></div>
                        <p>Курьерская доставка в
                            <br> день заказа</p>
                    </div>
                    <div class="item">
                        <div class="image"><img alt="image" src="/i/icon_avnt2.svg" style="height:77px"></div>
                        <p>Более 164 тысяч
                            <br> товаров</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
    <script type="text/javascript">
        rrApiOnReady.push(function() {
            rrApi.categoryView("Смартфоны планшеты гаджеты/Смартфоны");
        })
    </script>
    <footer>
        <div class="container">
            <div class="content-cnt">
                <div class="wrapp-footer">
                    <div class="footer-top">
                        <div class="subm">
                            <p class="title">Интернет-магазин</p>
                            <ul>
                                <li><a href="/about/">О нас</a></li>
                                <li><a href="/why/">Почему мы</a></li>
                                <li><a href="/about/corporate.php/">Оптовым и корпоративным клиентам</a></li>
                                <li><a href="/franchise/">Франчайзинг</a></li>
                                <li><a href="/supplier/">Для поставщиков</a></li>
                                <li><a href="/vacancy/">Вакансии</a></li>
                                <li><a href="/actions/">Акции</a></li>
                                <li><a href="/news/">Новости</a></li>
                            </ul>
                        </div>
                        <div class="subm">
                            <p class="title">Преимущества 123.ру</p>
                            <ul>
                                <li><a href="/credit/">Купить в кредит</a></li>
                                <li><a href="/sovest/">Карта рассрочки Совесть</a></li>
                                <li><a href="/halva/">Карта рассрочки Халва</a></li>
                                <li><a href="/pay/">Способы оплаты</a></li>
                            </ul>
                        </div>
                        <div class="subm">
                            <p class="title">Наши услуги</p>
                            <ul>
                                <li><a href="/warranty/">Гарантия</a></li>
                                <li><a href="/about/delivery.php/">Доставка</a></li>
                                <li><a href="/sborka/">Сборка ПК</a></li>
                            </ul>
                        </div>
                        <div>
                            <ul class="social-link">
                                <li>
                                    <a href="https://www.facebook.com/123.ru"><img src="/i/social_facebook_circle.svg" /></a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/123_shop"><img src="/i/social_twitter_circle.svg" /></a>
                                </li>
                                <li>
                                    <a href="https://vk.com/razdvatri_shop"><img src="/i/social_vk_circle.svg" /></a>
                                </li>
                                <li>
                                    <a href="https://www.instagram.com/123.ru_official/"><img src="/i/social_instagram_circle.svg" /></a>
                                </li>
                                <li>
                                    <a href="https://ok.ru/razdvatrishop"><img src="/i/social_ok_circle.svg" /></a>
                                </li>
                                <li>
                                    <a href="https://t.me/official123ru"><img src="/i/social_telegram_circle.svg" /></a>
                                </li>
                            </ul><a class="phone" href="tel:8(495)225 9 123">8 (495) 225 9 123</a><a class="phone" href="tel:8(800)333 9 123">8 (800) 333 9 123</a><a class="call-backform" href="#modal" data-modal="#bottom-popup">Обратная связь</a><a class="email-footer" href="mailto:info@123.ru">info@123.ru</a><a class="partner-q" href="/supplier/">По вопросам сотрудничества</a></div>
                    </div>
                    <div class="footer-bottom">
                        <div>
                            <p class="correct">
                                <div class="hide-me" data-hide-uid="5be5cd217a7043.78255819"></div>
                            </p>
                        </div>
                        <div>
                            <a class="sad-client" href="/bad/">
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="17.995px" height="18px" viewBox="3.005 4 17.995 18" enable-background="new 3.005 4 17.995 18" xml:space="preserve">
                                    <path d="M12,4c4.971,0,9,4.03,9,9c0,4.971-4.029,9-9,9c-4.97,0-9-4.029-9-9C3,8.03,7.03,4,12,4z M12,5.8c-3.977,0-7.2,3.223-7.2,7.2c0,3.977,3.223,7.199,7.2,7.199c3.977,0,7.199-3.223,7.199-7.199C19.199,9.023,15.977,5.8,12,5.8z M12,13.9c-0.497,0-0.9-0.402-0.9-0.9V9.4c0-0.498,0.404-0.9,0.9-0.9c0.498,0,0.9,0.403,0.9,0.9V13C12.9,13.498,12.498,13.9,12,13.9zM12,17.5c-0.497,0-0.9-0.402-0.9-0.9c0-0.497,0.404-0.9,0.9-0.9c0.498,0,0.9,0.403,0.9,0.9C12.9,17.098,12.498,17.5,12,17.5z" />
                                </svg>Пожаловаться</a>
                        </div>
                        <div>
                            <a href="http://iconix.ru/" target="_blank" class="partner iconix"></a>
                            <a href="https://www.iemcommunity.ru" target="_blank" class="partner ultima-iem"></a><a class="" href="/catalog/">Карта сайта</a><span class="copy">123.ru &copy; 2010-2018</span></div>
                    </div>
                    <div class="footer-result">
                        <p>© 2010-2018. Все права защищены. Компания www.123.ru — ваш гипермаркет электроники.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="catalog _mobile"><a href="javascript:void(0)" class="catalog__btn">Каталог</a>
        <div class="catalog__content"><a href="javascript:void(0)" class="catalog__back">Назад</a>
            <div class="catalog__pages">
                <!-- .catalog__page -->
            </div>
        </div>
    </div>
    <div class="bottom-fixed">
        <div class="footer-bar bg-white">
            <div class="footer-bar-top tabs-list "><a class="footbar-tab js-can-deactivate review" href="#tab-review"><span><i class="icon ic-mess"></i><span class="hidden-sm hidden-xs">Обратная связь</span></span></a>
                <div class="footbar-tab bay hidden-sm hidden-xs"><span><a href="/quick-pay/" class="btn btn-primary" onclick="return redirect('/quick-pay/');">оплата заказа</a></span></div><a class="footbar-tab js-can-deactivate tracked nobefore" href="#tab-tracked"><span><i class="icon ic-search-ht hidden-lg hidden-md"></i><span class="hidden-sm hidden-xs">Отслеживаемые</span> <span class="cl-blue">0</span></span></a><a class="footbar-tab js-can-deactivate look" href="#tab-look"><span><i class="icon ic-clock hidden-lg hidden-md"></i><span class="hidden-sm hidden-xs">Просмотренные</span> <span class="cl-blue">0</span></span></a><a class="footbar-tab js-can-deactivate diagram" href="#tab-diagram"><span><i class="icon ic-compare"></i><span class="cl-blue">0</span></span></a><a class="footbar-tab js-can-deactivate shopcart" href="#tab-shopcart"><span><i class="icon ic-cart"></i><span class="cl-blue">0</span></span></a></div>
            <div class="footer-bar-bottom">
                <div class="tabs-viewport">
                    <div class="tab tab-slider" id="tab-review">
                        <div class="footer-feedback">
                            <div class="form-elements">
                                <div class="col-lg-4 form-bad hide-in-js"><!--<div class="success">Ваш жалоба зарегистрирована!</div><div class="errors">Невозможно зарегистрировать жалобу, попробуйте еще раз</div><form class="form-elements" data-action="/bad/create/" method="post" id="form-abuse-footer2"><input type="hidden" name="sluid" value="db24cc5423085b5b4f68e0869b747a14490d4fa3a6ef607f809dcefe2525f7ef" /><div class="h3">Зарегистрировать жалобу</div><p>Жалуйтесь без колебаний. Разберемся, виновного накажем.</p><label for="email" class="require"><span>Ваш E-Mail для ответа на жалобу:</span><input name="email" type="text" value=""></label><label for="phone" class="require"><span>Телефон для связи</span><input name="phone" type="tel" class="masked" value="" mask="+7 (999) 999-99-99" mask-placeholder="_" placeholder="+7 (___) ___-__-__" /></label><label for="comment" class="require"><span>Жалоба</span><textarea rows="5" name="comment"></textarea></label><a href="#" class="btn btn-primary btn-submit">Отправить</a><div class="require-text">Поля, обязательные для заполнения</div></form>--></div>
                                <div class="col-lg-4 form-phone hide-in-js"><!--<div class="success">Ваш обратный звонок зарегистрирован!</div><div class="errors">Невозможно заказать звонок, попробуйте еще раз</div><form class="form-elements" id="form-callback-footer2" data-action="/callback/" method="post"><input type="hidden" name="sluid" value="db24cc5423085b5b4f68e0869b747a14490d4fa3a6ef607f809dcefe2525f7ef" /><div class="h3">Обратный звонок</div><p>Укажите свой контактный телефон, и мы перезвоним вам в ближайшие несколько минут:</p><label for="phone" class="require"><span>Телефон для связи</span><input type="tel" name="phone" class="masked" mask="+7 (999) 999-99-99" mask-placeholder="_" placeholder="+7 (___) ___-__-__" value="" /></label><a href="#" class="btn btn-primary btn-submit">Отправить</a><div class="require-text">Поля, обязательные для заполнения</div></form>--></div>
                                <div class="col-lg-4 form-social">
                                    <div class="h3">Мы в социальных сетях</div><a href="https://ru-ru.facebook.com/123.ru" class="btn fs-fb"><i></i>123.RU в Facebook</a>
                                    <br><a href="https://vk.com/club21883822" class="btn fs-vk"><i></i>123.RU в Вконтакте</a>
                                    <br><a href="https://twitter.com/123_shop" class="btn fs-tw"><i></i>123.RU в Twitter</a>
                                    <br><a href="http://www.youtube.com/user/www123ru" class="btn fs-yt"><i></i>123.RU на YouTube</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="tab tab-slider inprogress" id="tab-tracked" data-source="/footer-panel/tracking/"></div>
                    <div class="tab tab-slider inprogress" id="tab-look" data-source="/footer-panel/viewed/"></div>
                    <div class="tab tab-slider inprogress" id="tab-diagram" data-source="/footer-panel/compare/"></div>
                    <div class="tab tab-slider inprogress" id="tab-shopcart" data-source="/footer-panel/basket/"></div>
                </div>
                <div class="hidden-lg hidden-md footbar-mob-btn"><a href="/quick-pay/" class="btn btn-primary">оплата заказа</a></div>
                <button type="button" class="fb-arrows fba-prev slick-prev">Prev</button>
                <button type="button" class="fb-arrows fba-next slick-next">Next</button>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="window box-shadow" id="login">
        <div class="hide-me" data-hide-uid="5be5cd218095d1.59317263"></div>
    </div>
    <div class="window box-shadow avail-notify" id="avail-notify">
        <div class="header"><span>Оповещение о наличии товара</span><a href="#" data-action="close"><i class="icon ic-close"></i></a></div>
        <div class="body">
            <p>Введите свой адрес эл.почты и мы Вас оповестим, когда товар появится в наличии</p>
            <div class='-form'>
                <div class='line'><label for='f-name'>Введите Ваше имя</label><input type='text' placeholder='Иван' id='f-name'></div>
                <div class='line'><label for='f-email'>Введите Ваш E-mail</label><input type='text' placeholder='E-mail' id='f-email'></div>
                <div class='line -final'><label></label><a href='#' class='-send'>Оповестите меня</a></div>
            </div>
            <div class="info">
                Нажимая кнопку "Оповестите меня", я подтверждаю свое согласие на получение информации 
                об оформлении и получении заказа, согласие на обработку персональных данных 
                в соответствии с указаным <a href="#">здесь</a> текстом. 
            </div>
        </div>
    </div>
    <div class="window box-shadow cities-new" id="cities">
        <div class="header"><span>Укажите ваш город</span><a href="#" data-action="close"><i class="icon ic-close"></i></a></div>
        <form class="bottom bg-darkblue2 form-elements" action="#" method="POST">
            <div class="fake-placeholder"><span>Доставка по всей России<span class="hidden-xs">, например в город <b>Москва</b></span></span>
                <input type="text" name="search-city" /><a href="javascript:void(0)" class="btn btn-primary">Сохранить</a></div>
        </form>
        <div class="body city-list">
            <div class="row">
                <div class="col-lg-12 relative cities-big noselect">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="small-text">Крупные города:</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-lg-12">
                            <ul class="clearstyle">
                                <li><a href="#" data-type="г" data-id="15238" data-code="7700000000000" data-city-name="Москва" data-return-url="http://red.123.ru/smartfoni_plansheti_gadjeti/telefoniya/">Москва</a></li>
                                <li><a href="https://spb.123.ru/smartfoni_plansheti_gadjeti/telefoniya/" data-type="г" data-id="4364" data-code="7800000000000" data-city-name="Санкт-Петербург">Санкт-Петербург</a></li>
                                <li><a href="#" data-type="г" data-id="2480" data-code="2200000100000" data-city-name="Барнаул" data-return-url="http://red.123.ru/smartfoni_plansheti_gadjeti/telefoniya/">Барнаул</a></li>
                                <li><a href="https://vgg.123.ru/smartfoni_plansheti_gadjeti/telefoniya/" data-type="г" data-id="6153" data-code="3400000100000" data-city-name="Волгоград">Волгоград</a></li>
                                <li><a href="https://vrn.123.ru/smartfoni_plansheti_gadjeti/telefoniya/" data-type="г" data-id="6490" data-code="3600000100000" data-city-name="Воронеж">Воронеж</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-lg-12">
                            <ul class="clearstyle">
                                <li><a href="https://ekb.123.ru/smartfoni_plansheti_gadjeti/telefoniya/" data-type="г" data-id="6104" data-code="6600000100000" data-city-name="Екатеринбург">Екатеринбург</a></li>
                                <li><a href="https://kzn.123.ru/smartfoni_plansheti_gadjeti/telefoniya/" data-type="г" data-id="14055" data-code="1600000100000" data-city-name="Казань">Казань</a></li>
                                <li><a href="#" data-type="г" data-id="18474" data-code="2300000100000" data-city-name="Краснодар" data-return-url="http://red.123.ru/smartfoni_plansheti_gadjeti/telefoniya/">Краснодар</a></li>
                                <li><a href="https://kry.123.ru/smartfoni_plansheti_gadjeti/telefoniya/" data-type="г" data-id="284" data-code="2400000100000" data-city-name="Красноярск">Красноярск</a></li>
                                <li><a href="https://nnov.123.ru/smartfoni_plansheti_gadjeti/telefoniya/" data-type="г" data-id="4187" data-code="5200000100000" data-city-name="Нижний Новгород">Нижний Новгород</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-lg-12">
                            <ul class="clearstyle">
                                <li><a href="https://nsk.123.ru/smartfoni_plansheti_gadjeti/telefoniya/" data-type="г" data-id="16465" data-code="5400000100000" data-city-name="Новосибирск">Новосибирск</a></li>
                                <li><a href="https://omsk.123.ru/smartfoni_plansheti_gadjeti/telefoniya/" data-type="г" data-id="10024" data-code="5500000100000" data-city-name="Омск">Омск</a></li>
                                <li><a href="https://prm.123.ru/smartfoni_plansheti_gadjeti/telefoniya/" data-type="г" data-id="6444" data-code="5900000100000" data-city-name="Пермь">Пермь</a></li>
                                <li><a href="https://rnd.123.ru/smartfoni_plansheti_gadjeti/telefoniya/" data-type="г" data-id="6188" data-code="6100000100000" data-city-name="Ростов-на-Дону">Ростов-на-Дону</a></li>
                                <li><a href="https://sam.123.ru/smartfoni_plansheti_gadjeti/telefoniya/" data-type="г" data-id="20348" data-code="6300000100000" data-city-name="Самара">Самара</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-lg-12">
                            <ul class="clearstyle">
                                <li><a href="https://yfa.123.ru/smartfoni_plansheti_gadjeti/telefoniya/" data-type="г" data-id="10554" data-code="0200000100000" data-city-name="Уфа">Уфа</a></li>
                                <li><a href="https://chl.123.ru/smartfoni_plansheti_gadjeti/telefoniya/" data-type="г" data-id="5530" data-code="7400000100000" data-city-name="Челябинск">Челябинск</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix" style="height: 25px"></div>
        </div>
    </div>
    <div class="window box-shadow" id="cart">
        <div class="header"><span>Корзина</span>&nbsp;<span class="c-gray">0 товаров</span><a href="#" data-action="close" class="visible-xs"><i class="icon ic-close"></i></a></div>
        <div class="body">
            <ul class="window-cart-items clearstyle"></ul>
            <div class="total"><span>Всего товаров на сумму:</span>
                <div class="price c-blue"><span>0</span> <span class="rub">a</span></div>
            </div>
        </div>
        <div class="bottom"><a href="/ordering/" class="btn btn-fw btn-primary btn-lg">оформить заказ</a></div>
    </div>
    <div class="window box-shadow" id="callback">
        <div class="hide-me" data-hide-uid="5be5cd219aac99.00259555"></div>
    </div>
    <div class="window box-shadow" id="report">
        <div class="hide-me" data-hide-uid="5be5cd219b9320.96740656"></div>
    </div>
    <div class="window box-shadow" id="empty-compare">
        <div class="hide-me" data-hide-uid="5be5cd219bfc34.74523451"></div>
    </div>
    <div id="bottom-popup" class="modal__wr center-p">
        <div class="container">
            <div class="modal__content fade__wr wrapp-bottom">
                <div class="wrapp-popup">
                    <div class="close-popup"><span></span><span></span></div>
                    <div class="body-popup">
                        <div class="callback-side">
                            <div class="form-phone">
                                <p class="title">Заказать обратный звонок</p>
                                <p class="do-this-slogan">Укажите ваш контактный телефон и мы перезвоним вам в течение нескольких минут!</p>
                                <div class="success">Ваш обратный звонок зарегистрирован!</div>
                                <div class="errors">Невозможно заказать звонок, попробуйте еще раз</div>
                                <form id="form-callback-footer" data-action="/callback/" method="post">
                                    <input type="hidden" name="sluid" value="db24cc5423085b5b4f68e0869b747a14490d4fa3a6ef607f809dcefe2525f7ef" />
                                    <div class="line">
                                        <div class="inpt-wr">
                                            <label for="callbackName-footer">Ваше имя *</label>
                                            <input class="input-base" type="text" name="callbackName-footer" placeholder="Введите ваше имя">
                                        </div>
                                        <div class="inpt-wr">
                                            <label for="callbackPhone-footer">Номер телефона *</label>
                                            <input class="input-base masked" type="tel" id="callbackPhone-footer" name="phone" placeholder="+7 (___) ___-__-__" mask="+7 (999) 999-99-99" mask-placeholder="_" placeholder="+7 (___) ___-__-__" value="" />
                                        </div>
                                    </div>
                                    <div class="submit-hint">Нажимая кнопку "заказать звонок" я соглашаюсь с <a href="/publicoffer/#privacy" target="_blank">политикой конфиденциальности</a></div>
                                    <button class="btn btn-primary btn-submit" type="submit">Заказать звонок</button>
                                </form>
                            </div>
                            <div class="req-text"><span>* – поля обязательные для заполнения</span></div>
                        </div>
                        <div class="angry-side form-bad">
                            <p class="title">Зарегистрировать жалобу</p>
                            <p class="do-this-slogan">Жалуйтесь без колебаний! Разберемся быстро, виновного накажем.</p>
                            <div class="success">Ваш жалоба зарегистрирована!</div>
                            <div class="errors">Невозможно зарегистрировать жалобу, попробуйте еще раз</div>
                            <form data-action="/bad/create/" method="post" id="form-abuse-footer">
                                <input type="hidden" name="sluid" value="db24cc5423085b5b4f68e0869b747a14490d4fa3a6ef607f809dcefe2525f7ef" />
                                <div class="line">
                                    <div class="inpt-wr">
                                        <label for="sadEmail-footer">Ваше email для ответа на жалобу *</label>
                                        <input class="input-base" type="email" name="email" id="sadEmail-footer" placeholder="Введите ваш email" value="" />
                                    </div>
                                    <div class="inpt-wr">
                                        <label for="sadPhone-footer">Номер телефона *</label>
                                        <input class="input-base masked" type="tel" id="sadPhone-footer" name="phone" value="" mask="+7 (999) 999-99-99" mask-placeholder="_" placeholder="+7 (___) ___-__-__" />
                                    </div>
                                </div>
                                <div class="line">
                                    <div class="inpt-wr-full">
                                        <label for="sadText-footer">Жалоба *</label>
                                        <textarea name="comment" id="sadText-footer" placeholder="Введите вашу жалобу"></textarea>
                                    </div>
                                </div>
                                <div class="submit-hint">Нажимая кнопку "отправить" я соглашаюсь с <a href="/publicoffer/#privacy" target="_blank">политикой конфиденциальности</a></div>
                                <button class="btn btn-primary btn-submit" type="submit">Отправить</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="ga-ecommerce-data" data-page-type="category" data-client-id="1461201598.1541171317" data-auth="0" data-user-id="0" data-city="Москва">
        <div class="impression" data-id="80054955" data-name="Смартфон ASUS ZenFone 3 Max ZC553KL 16 Гб серый (90AX00D2-M01780)" data-category="Смартфоны" data-price="10280" data-position="1" data-list="Список товаров"></div>
        <div class="impression" data-id="80053717" data-name="Смартфон Alcatel Pixi 4 Plus Power 5023F 16 Гб белый (5023F-2BALRU2)" data-category="Смартфоны" data-price="5200" data-position="2" data-list="Список товаров"></div>
        <div class="impression" data-id="80188188" data-name="Смартфон Honor 7C 32 Гб синий (51092MNT)" data-category="Смартфоны" data-price="10600" data-position="3" data-list="Список товаров"></div>
        <div class="impression" data-id="80188194" data-name="Смартфон Honor 7X 64 Гб синий (BND-L21 51091YTY)" data-category="Смартфоны" data-price="16150" data-position="4" data-list="Список товаров"></div>
        <div class="impression" data-id="80185424" data-name="Смартфон Samsung Galaxy J8 2018 32 Гб черный (SM-J810FZKDSER)" data-category="Смартфоны" data-price="14510" data-position="5" data-list="Список товаров"></div>
        <div class="impression" data-id="80053617" data-name="Смартфон Fly FS408 Stratus 8 8 Гб черный" data-category="Смартфоны" data-price="2520" data-position="6" data-list="Список товаров"></div>
        <div class="impression" data-id="80071130" data-name="Смартфон Huawei P smart 32 Гб золотистый (51092DPM)" data-category="Смартфоны" data-price="12610" data-position="7" data-list="Список товаров"></div>
        <div class="impression" data-id="80056332" data-name="Смартфон Vivo Y65 16 Гб золотистый Y65_Gold_Vivo 1719" data-category="Смартфоны" data-price="9020" data-position="8" data-list="Список товаров"></div>
        <div class="impression" data-id="80140966" data-name="Смартфон Doogee X53 16Gb Black Смартфон" data-category="Смартфоны" data-price="4540" data-position="9" data-list="Список товаров"></div>
        <div class="impression" data-id="8958654" data-name="Смартфон Samsung SM-G532 Galaxy J2 Prime 8 Гб серебристый (SM-G532FZSDSER)" data-category="Смартфоны" data-price="6860" data-position="10" data-list="Список товаров"></div>
        <div class="impression" data-id="8973755" data-name="Смартфон Philips Xenium X818 32 Гб черный (867000139401)" data-category="Смартфоны" data-price="9990" data-position="11" data-list="Список товаров"></div>
        <div class="impression" data-id="80004335" data-name="Смартфон ZTE Blade A520 16 Гб синий BLADEA520BLUE" data-category="Смартфоны" data-price="5250" data-position="12" data-list="Список товаров"></div>
        <div class="impression" data-id="80106767" data-name="Смартфон Alcatel 3C 5026D 16 Гб металлик черный (5026D-2AALRU1)" data-category="Смартфоны" data-price="5550" data-position="13" data-list="Список товаров"></div>
        <div class="impression" data-id="80098279" data-name="Смартфон Huawei P20 Lite 64 Гб синий (51092GYT)" data-category="Смартфоны" data-price="16280" data-position="14" data-list="Список товаров"></div>
        <div class="impression" data-id="80140956" data-name="Смартфон Huawei Y6 Prime 2018 16 Гб золотистый 51092KQG" data-category="Смартфоны" data-price="8200" data-position="15" data-list="Список товаров"></div>
        <div class="impression" data-id="8839982" data-name="Смартфон Philips S616 16 Гб черный Dark Grey" data-category="Смартфоны" data-price="7990" data-position="16" data-list="Список товаров"></div>
        <div class="impression" data-id="80213145" data-name="Смартфон Apple iPhone SE &quot;Как новый&quot; 128 Гб серый (FP862RU/A)" data-category="Смартфоны" data-price="20330" data-position="17" data-list="Список товаров"></div>
        <div class="impression" data-id="80126854" data-name="Смартфон Samsung Galaxy A6+ 2018 32 Гб синий SM-A605F" data-category="Смартфоны" data-price="16150" data-position="18" data-list="Список товаров"></div>
        <div class="impression" data-id="80133416" data-name="Смартфон Alcatel 3X 5058I 32 Гб металлик черный 5058I-2AALRU1" data-category="Смартфоны" data-price="6720" data-position="19" data-list="Список товаров"></div>
        <div class="impression" data-id="80089825" data-name="Смартфон Neffos C7 16 Гб серый (TP910A24RU)" data-category="Смартфоны" data-price="6560" data-position="20" data-list="Список товаров"></div>
        <div class="impression" data-id="80008216" data-name="Смартфон Alcatel U5 5044D 8 Гб серый (5044D-2AALRU1)" data-category="Смартфоны" data-price="4330" data-position="21" data-list="Список товаров"></div>
        <div class="impression" data-id="80203102" data-name="Смартфон NOKIA 5.1 16 Гб черный (TA-1075)" data-category="Смартфоны" data-price="12110" data-position="22" data-list="Список товаров"></div>
        <div class="impression" data-id="80025886" data-name="Смартфон Philips Xenium S327 8 Гб синий (867000145531)" data-category="Смартфоны" data-price="4860" data-position="23" data-list="Список товаров"></div>
        <div class="impression" data-id="80104412" data-name="Смартфон Xiaomi Redmi 5 16 Гб черный" data-category="Смартфоны" data-price="9680" data-position="24" data-list="Список товаров"></div>
        <div class="impression" data-id="80059382" data-name="Смартфон ZTE Blade A610 16 Гб серый" data-category="Смартфоны" data-price="6160" data-position="25" data-list="Список товаров"></div>
        <div class="impression" data-id="80029174" data-name="Смартфон Motorola G5S 32 Гб золотистый (PA7W0022RU)" data-category="Смартфоны" data-price="9180" data-position="26" data-list="Список товаров"></div>
        <div class="impression" data-id="80058648" data-name="Смартфон Samsung Galaxy A8 (2018) 32 Гб синий (SM-A530FZBDSER)" data-category="Смартфоны" data-price="20280" data-position="27" data-list="Список товаров"></div>
        <div class="impression" data-id="80164677" data-name="Смартфон ARK Benefit S505 8 Гб черный" data-category="Смартфоны" data-price="3190" data-position="28" data-list="Список товаров"></div>
        <div class="impression" data-id="80062360" data-name="Смартфон Xiaomi Redmi Note 5A Prime 32 Гб золотистый" data-category="Смартфоны" data-price="10280" data-position="29" data-list="Список товаров"></div>
        <div class="impression" data-id="80148022" data-name="Смартфон Vertex Impress Frost 16 Гб черный VRX-VFRST-BLK" data-category="Смартфоны" data-price="6640" data-position="30" data-list="Список товаров"></div>
    </div>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function(d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    window.dataLayerYa = window.dataLayerYa || [];
                    dataLayerYa.push({
                        "ecommerce": {
                            "currencyCode": "RUB"
                        }
                    });
                    w.yaCounter2207821 = new Ya.Metrika({
                        "id": 2207821,
                        "webvisor": "true",
                        "clickmap": "true",
                        "trackLinks": "true",
                        "accurateTrackBounce": "true",
                        "ecommerce": "dataLayerYa"
                    });
                } catch (e) {}
            });
            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function() {
                    n.parentNode.insertBefore(s, n);
                };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";
            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript>
        <div><img src="//mc.yandex.ru/watch/2207821" style="position:absolute; left:-9999px;" alt="" /></div>
    </noscript>
    <script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true"></script>
    <script type="text/javascript">
        window.criteo_q = window.criteo_q || [];
        window.criteo_q.push({
            "event": "setAccount",
            "account": 16192
        }, {
            "event": "setSiteType",
            "type": "d"
        }, {
            "event": "viewList",
            "item": ["80054955", "80053717", "80188188"]
        });
    </script>
    <div id="criteo-data" data-product-prefix="msk"></div>
    <script type="text/javascript">
        dataLayer = dataLayer || [];
        dataLayer.push({
            "ecomm_totalvalue": 0,
            "ecomm_prodid": 0
        });
    </script>
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KBL5ZV" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
    <script type="text/javascript" src="//api.flocktory.com/v2/loader.js?site_id=66" async="async"></script>
    <div class="i-flocktory" data-fl-action="track-category-view" data-fl-category-id="122"></div>
    <script type="text/javascript" id="advcakeAsync">
        window.advcake_push_data = window.advcake_push_data || function(advcake_data_array) {};
        window.advcake_data = {
            "pageType": 3,
            "currentCategory": {
                "id": 122,
                "name": "Смартфоны"
            },
            "products": [{
                "id": "80054955",
                "name": "Смартфон ASUS ZenFone 3 Max ZC553KL 16 Гб серый (90AX00D2-M01780)"
            }, {
                "id": "80053717",
                "name": "Смартфон Alcatel Pixi 4 Plus Power 5023F 16 Гб белый (5023F-2BALRU2)"
            }, {
                "id": "80188188",
                "name": "Смартфон Honor 7C 32 Гб синий (51092MNT)"
            }, {
                "id": "80188194",
                "name": "Смартфон Honor 7X 64 Гб синий (BND-L21 51091YTY)"
            }, {
                "id": "80185424",
                "name": "Смартфон Samsung Galaxy J8 2018 32 Гб черный (SM-J810FZKDSER)"
            }, {
                "id": "80053617",
                "name": "Смартфон Fly FS408 Stratus 8 8 Гб черный"
            }, {
                "id": "80071130",
                "name": "Смартфон Huawei P smart 32 Гб золотистый (51092DPM)"
            }, {
                "id": "80056332",
                "name": "Смартфон Vivo Y65 16 Гб золотистый Y65_Gold_Vivo 1719"
            }, {
                "id": "80140966",
                "name": "Смартфон Doogee X53 16Gb Black Смартфон"
            }, {
                "id": "8958654",
                "name": "Смартфон Samsung SM-G532 Galaxy J2 Prime 8 Гб серебристый (SM-G532FZSDSER)"
            }, {
                "id": "8973755",
                "name": "Смартфон Philips Xenium X818 32 Гб черный (867000139401)"
            }, {
                "id": "80004335",
                "name": "Смартфон ZTE Blade A520 16 Гб синий BLADEA520BLUE"
            }, {
                "id": "80106767",
                "name": "Смартфон Alcatel 3C 5026D 16 Гб металлик черный (5026D-2AALRU1)"
            }, {
                "id": "80098279",
                "name": "Смартфон Huawei P20 Lite 64 Гб синий (51092GYT)"
            }, {
                "id": "80140956",
                "name": "Смартфон Huawei Y6 Prime 2018 16 Гб золотистый 51092KQG"
            }, {
                "id": "8839982",
                "name": "Смартфон Philips S616 16 Гб черный Dark Grey"
            }, {
                "id": "80213145",
                "name": "Смартфон Apple iPhone SE \"Как новый\" 128 Гб серый (FP862RU/A)"
            }, {
                "id": "80126854",
                "name": "Смартфон Samsung Galaxy A6+ 2018 32 Гб синий SM-A605F"
            }, {
                "id": "80133416",
                "name": "Смартфон Alcatel 3X 5058I 32 Гб металлик черный 5058I-2AALRU1"
            }, {
                "id": "80089825",
                "name": "Смартфон Neffos C7 16 Гб серый (TP910A24RU)"
            }, {
                "id": "80008216",
                "name": "Смартфон Alcatel U5 5044D 8 Гб серый (5044D-2AALRU1)"
            }, {
                "id": "80203102",
                "name": "Смартфон NOKIA 5.1 16 Гб черный (TA-1075)"
            }, {
                "id": "80025886",
                "name": "Смартфон Philips Xenium S327 8 Гб синий (867000145531)"
            }, {
                "id": "80104412",
                "name": "Смартфон Xiaomi Redmi 5 16 Гб черный"
            }, {
                "id": "80059382",
                "name": "Смартфон ZTE Blade A610 16 Гб серый"
            }, {
                "id": "80029174",
                "name": "Смартфон Motorola G5S 32 Гб золотистый (PA7W0022RU)"
            }, {
                "id": "80058648",
                "name": "Смартфон Samsung Galaxy A8 (2018) 32 Гб синий (SM-A530FZBDSER)"
            }, {
                "id": "80164677",
                "name": "Смартфон ARK Benefit S505 8 Гб черный"
            }, {
                "id": "80062360",
                "name": "Смартфон Xiaomi Redmi Note 5A Prime 32 Гб золотистый"
            }, {
                "id": "80148022",
                "name": "Смартфон Vertex Impress Frost 16 Гб черный VRX-VFRST-BLK"
            }]
        };
        window.advcake_push_data(window.advcake_data);
    </script>
    <script type="text/javascript">
        (function() {
            var ra = document.createElement('script');
            ra.type = 'text/javascript';
            ra.async = true;
            ra.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'bn.adblender.ru/c/123ru/all.js?' + Math.floor((new Date()).valueOf() / 1000 / 3600);
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ra, s);
        })();
    </script>
    <!--LiveInternet counter-->
    <script type="text/javascript">
        <!--
        document.write("<div style='display:none;'><a rel='nofollow' href='http://www.liveinternet.ru/click' " +
                "target=_blank><img src='//counter.yadro.ru/hit?t44.6;r" +
                escape(document.referrer) + ((typeof(screen) == "undefined") ? "" :
                    ";s" + screen.width + "*" + screen.height + "*" + (screen.colorDepth ?
                        screen.colorDepth : screen.pixelDepth)) + ";u" + escape(document.URL) +
                ";" + Math.random() +
                "' alt='' title='LiveInternet' " +
                "border='0' width='31' height='31'><\/a></div>")
            //-->
    </script>
    <!--/LiveInternet-->
    <div class="window box-shadow" id="pay-menu">
        <div class="body box-side">
            <ul class="clearstyle">
                <li><a href="/pay/">Всё об оплате</a></li>
                <li><a href="/credit/">Покупка в кредит</a></li>
                <li><a href="/sovest/" class="">Рассрочка с картой Совесть</a></li>
                <li><a href="/halva/">Рассрочка от Халвы</a></li>
            </ul>
        </div>
    </div>
    <div class="-upd window box-shadow" id="basket-preview">
        <div class="header"><span>Товар добавлен в корзину</span>
            <a href="#" data-action="close">
                <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 12 12">
                    <svg xmlns:xlink="http://www.w3.org/1999/xlink" height="12" width="12" version="1.1" viewBox="0 0 12 12" id="close" y="0">
                        <desc></desc>
                        <g id="close-category_adaptive" fill-rule="evenodd" fill="none">
                            <g id="close-05_Category-ot-320" fill="#9B9B9B" transform="translate(-92 -376)">
                                <g transform="translate(0 360)">
                                    <g transform="translate(92 13)">
                                        <path id="close-Fill-20" d="m5.9998 10.692l-4.0537 4.054c-0.3876 0.388-1.0122 0.391-1.4097-0.006l-0.27654-0.277c-0.39099-0.391-0.39594-1.02-0.00627-1.409l4.0537-4.0542-4.0537-4.0537c-0.38754-0.3876-0.39127-1.0122 0.00626-1.4097l0.27656-0.2765c0.39099-0.391 1.02-0.396 1.4097-0.0063l4.0537 4.0537 4.0542-4.0537c0.387-0.3875 1.012-0.3913 1.409 0.0063l0.277 0.2765c0.391 0.391 0.396 1.02 0.006 1.4097l-4.0533 4.0537 4.0533 4.0542c0.388 0.387 0.392 1.012-0.006 1.409l-0.277 0.277c-0.391 0.391-1.02 0.396-1.409 0.006l-4.0542-4.054z"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                </svg>
            </a>
        </div>
        <div class="body">
            <div class="items">
                <ul class="window-cart-items clearstyle"></ul>
                <div class="buttons"><a href="/ordering/" class="btn-yellow">Перейти в корзину</a><a href="#" data-action="close" class="btn-primary">Продолжить покупки</a>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="recommendations loading" data-reload="1"></div>
        </div>
    </div>
    <script>
        (function() {
            window._shoppilot = window._shoppilot || [];
            _shoppilot.push(["_addStyles", "widgets"]);
            _shoppilot.push(["_addProductWidget", "listing-product-reviews", "#sp-listing-product-reviews-container"]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80054955", {
                "product_id": "80054955"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80053717", {
                "product_id": "80053717"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80188188", {
                "product_id": "80188188"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80188194", {
                "product_id": "80188194"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80185424", {
                "product_id": "80185424"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80053617", {
                "product_id": "80053617"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80071130", {
                "product_id": "80071130"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80056332", {
                "product_id": "80056332"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80140966", {
                "product_id": "80140966"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-8958654", {
                "product_id": "8958654"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-8973755", {
                "product_id": "8973755"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80004335", {
                "product_id": "80004335"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80106767", {
                "product_id": "80106767"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80098279", {
                "product_id": "80098279"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80140956", {
                "product_id": "80140956"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-8839982", {
                "product_id": "8839982"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80213145", {
                "product_id": "80213145"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80126854", {
                "product_id": "80126854"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80133416", {
                "product_id": "80133416"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80089825", {
                "product_id": "80089825"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80008216", {
                "product_id": "80008216"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80203102", {
                "product_id": "80203102"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80025886", {
                "product_id": "80025886"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80104412", {
                "product_id": "80104412"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80059382", {
                "product_id": "80059382"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80029174", {
                "product_id": "80029174"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80058648", {
                "product_id": "80058648"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80164677", {
                "product_id": "80164677"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80062360", {
                "product_id": "80062360"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80148022", {
                "product_id": "80148022"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-80054955", {
                "product_id": "80054955"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-80053717", {
                "product_id": "80053717"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-80188188", {
                "product_id": "80188188"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-80188194", {
                "product_id": "80188194"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-80185424", {
                "product_id": "80185424"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-80053617", {
                "product_id": "80053617"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-80071130", {
                "product_id": "80071130"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-80056332", {
                "product_id": "80056332"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-80140966", {
                "product_id": "80140966"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-8958654", {
                "product_id": "8958654"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-8973755", {
                "product_id": "8973755"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-80004335", {
                "product_id": "80004335"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-80106767", {
                "product_id": "80106767"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-80098279", {
                "product_id": "80098279"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-80140956", {
                "product_id": "80140956"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-8839982", {
                "product_id": "8839982"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-80213145", {
                "product_id": "80213145"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-80126854", {
                "product_id": "80126854"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-80133416", {
                "product_id": "80133416"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-80089825", {
                "product_id": "80089825"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-80008216", {
                "product_id": "80008216"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-80203102", {
                "product_id": "80203102"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-80025886", {
                "product_id": "80025886"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-80104412", {
                "product_id": "80104412"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-80059382", {
                "product_id": "80059382"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-80029174", {
                "product_id": "80029174"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-80058648", {
                "product_id": "80058648"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-80164677", {
                "product_id": "80164677"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-80062360", {
                "product_id": "80062360"
            }]);
            _shoppilot.push(["_addProductWidget", "good-review-teaser", "#sp-good-review-teaser-container-80148022", {
                "product_id": "80148022"
            }]);
            window._shoppilotParams = {};
            window._shoppilotParams.store_id = '5aae2c2d339215000a5f8261';
            window._shoppilotParams.theme_id = 'default';
        })();
    </script>
    <script type="text/javascript">
        var seoContent = {
            "5be5cd217a7043.78255819": "0JLRiyDQvNC+0LbQtdGC0LUg0YHQvtC+0LHRidC40YLRjCDQviDQvdC10YLQvtGH0L3QvtGB0YLQuCDQsiDQvtC/0LjRgdCw0L3QuNC4IOKAlCDQstGL0LTQtdC70LjRgtC1INC10ZEg0Lgg0L3QsNC20LzQuNGC0LUgPHNwYW4+U0hJRlQrRU5URVI8L3NwYW4+",
            "5be5cd218095d1.59317263": "PGRpdiBpZD0ic3BvaWxlci1sb2dpbiIgY2xhc3M9InRvb2dsZS1zcG9pbGVyIj48ZGl2IGNsYXNzPSJoZWFkZXIiPjxzcGFuPtCS0YXQvtC0PC9zcGFuPjxhIGhyZWY9IiMiIGRhdGEtYWN0aW9uPSJjbG9zZSI+PGkgY2xhc3M9Imljb24gaWMtY2xvc2UiPjwvaT48L2E+PC9kaXY+PGRpdiBjbGFzcz0iYm9keSBib3gtc2lkZSBib3gtdG9wIj48ZGl2IGNsYXNzPSJlcnJvcnMiPtCd0LXQstC+0LfQvNC+0LbQvdC+INCw0LLRgtC+0YDQuNC30L7QstCw0YLRjNGB0Y8sINC/0L7Qv9GA0L7QsdGD0LnRgtC1INC10YnQtSDRgNCw0Lc8L2Rpdj48Zm9ybSBjbGFzcz0iZm9ybS1lbGVtZW50cyBmb3JtLWxvZ2luIiBpZD0iZm9ybS1sb2dpbiIgZGF0YS1hY3Rpb249Ii9sb2dpbi9hdXRoLyIgbWV0aG9kPSJwb3N0Ij48aW5wdXQgdHlwZT0iaGlkZGVuIiBuYW1lPSJzbHVpZCIgdmFsdWU9ImRiMjRjYzU0MjMwODViNWI0ZjY4ZTA4NjliNzQ3YTE0NDkwZDRmYTNhNmVmNjA3ZjgwOWRjZWZlMjUyNWY3ZWYiIC8+PGxhYmVsIGZvcj0ibG9naW4iPjxzcGFuPtCa0L7QtCDQutC70LjQtdC90YLQsCAvINCi0LXQu9C10YTQvtC9IC8gRS1tYWlsPC9zcGFuPjxpbnB1dCB0eXBlPSJ0ZXh0IiBuYW1lPSJsb2dpbiIgLz48L2xhYmVsPjxsYWJlbCBmb3I9InBhc3N3b3JkIj48c3Bhbj7Qn9Cw0YDQvtC70Yw8L3NwYW4+PGlucHV0IHR5cGU9InBhc3N3b3JkIiBuYW1lPSJwYXNzd29yZCIgLz48L2xhYmVsPjxkaXYgY2xhc3M9InJlY292ZXJ5Ij48ZGl2IGNsYXNzPSJyZWNvdmVyeS1zdWNjZXNzIj7Qn9Cw0YDQvtC70Ywg0LLQvtGB0YHRgtCw0L3QvtCy0LvQtdC9PC9kaXY+PGRpdiBjbGFzcz0icmVjb3ZlcnktZXJyb3JzIj7QndC10LLQvtC30LzQvtC20L3QviDQstC+0YHRgdGC0LDQvdC+0LLQuNGC0Ywg0L/QsNGA0L7Qu9GMLCDQv9C+0L/RgNC+0LHRg9C50YLQtSDQtdGJ0LUg0YDQsNC3PC9kaXY+PGEgaHJlZj0iIyIgY2xhc3M9InBzZXVkbyBjLWJsdWUgbGluayI+0J/QvtC70YPRh9C40YLRjCDQv9Cw0YDQvtC70Ywg0L/QviBTTVM8L2E+PC9kaXY+PGRpdiBjbGFzcz0idGNlbnRlciI+PGEgaHJlZj0iIyIgY2xhc3M9ImJ0biBidG4tZncgYnRuLXByaW1hcnkgYnRuLWxnIGJ0bi1zdWJtaXQiPtCy0L7QudGC0Lg8L2E+PGlucHV0IHR5cGU9InN1Ym1pdCIgc3R5bGU9ImRpc3BsYXk6IG5vbmU7IiAvPjxhIGhyZWY9IiMiIGNsYXNzPSJiYXNlIHJlZ2lzdGVyIiBkYXRhLW9wZW5zcG9pbGVyPSIjc3BvaWxlci1yZWdpc3RlciIgZGF0YS1jbG9zZXNwb2lsZXI9IiNzcG9pbGVyLWxvZ2luIj7QoNC10LPQuNGB0YLRgNCw0YbQuNGPPC9hPjwvZGl2PjwvZm9ybT48L2Rpdj48L2Rpdj48ZGl2IGlkPSJzcG9pbGVyLXJlZ2lzdGVyIiBjbGFzcz0idG9vZ2xlLXNwb2lsZXIgZG4iPjxkaXYgY2xhc3M9ImhlYWRlciI+PHNwYW4+0KDQtdCz0LjRgdGC0YDQsNGG0LjRjzwvc3Bhbj48YSBocmVmPSIjIiBkYXRhLWFjdGlvbj0iY2xvc2UiPjxpIGNsYXNzPSJpY29uIGljLWNsb3NlIj48L2k+PC9hPjwvZGl2PjxkaXYgY2xhc3M9ImJvZHkgYm94LXNpZGUgYm94LXRvcCI+PGRpdiBjbGFzcz0iZXJyb3JzIj7QndC10LLQvtC30LzQvtC20L3QviDQt9Cw0YDQtdCz0LjRgdGC0YDQuNGA0L7QstCw0YLRjNGB0Y8sINC/0L7Qv9GA0L7QsdGD0LnRgtC1INC10YnQtSDRgNCw0Lc8L2Rpdj48Zm9ybSBjbGFzcz0iZm9ybS1lbGVtZW50cyBmb3JtLWxvZ2luIiBpZD0iZm9ybS1yZWdpc3RlciIgZGF0YS1hY3Rpb249Ii9yZWdpc3Rlci8iIG1ldGhvZD0icG9zdCI+PGlucHV0IHR5cGU9ImhpZGRlbiIgbmFtZT0ic2x1aWQiIHZhbHVlPSJkYjI0Y2M1NDIzMDg1YjViNGY2OGUwODY5Yjc0N2ExNDQ5MGQ0ZmEzYTZlZjYwN2Y4MDlkY2VmZTI1MjVmN2VmIiAvPjxsYWJlbCBmb3I9InItbG9naW4iPjxzcGFuPtCk0JjQnjwvc3Bhbj48aW5wdXQgdHlwZT0idGV4dCIgbmFtZT0iZmlvIiAvPjwvbGFiZWw+PGxhYmVsIGZvcj0ici1wYXNzd29yZCI+PHNwYW4+0KLQtdC70LXRhNC+0L08L3NwYW4+PGlucHV0IG5hbWU9InBob25lIiB0eXBlPSJ0ZWwiIGNsYXNzPSJtYXNrZWQiIG1hc2s9Iis3ICg5OTkpIDk5OS05OS05OSIgbWFzay1wbGFjZWhvbGRlcj0iXyIgcGxhY2Vob2xkZXI9Iis3IChfX18pIF9fXy1fXy1fXyIgLz48L2xhYmVsPjxsYWJlbCBmb3I9InItZW1haWwiPjxzcGFuPkVtYWlsPC9zcGFuPjxpbnB1dCB0eXBlPSJ0ZXh0IiBuYW1lPSJlbWFpbCIgLz48L2xhYmVsPjxkaXYgY2xhc3M9InRjZW50ZXIiPjxhIGhyZWY9IiMiIGNsYXNzPSJidG4gYnRuLWZ3IGJ0bi1wcmltYXJ5IGJ0bi1sZyBidG4tc3VibWl0Ij7Qt9Cw0YDQtdCz0LjRgdGC0YDQuNGA0L7QstCw0YLRjNGB0Y88L2E+PGlucHV0IHR5cGU9InN1Ym1pdCIgc3R5bGU9ImRpc3BsYXk6IG5vbmU7IiAvPjxhIGhyZWY9IiMiIGNsYXNzPSJiYXNlIHJlZ2lzdGVyIiBkYXRhLW9wZW5zcG9pbGVyPSIjc3BvaWxlci1sb2dpbiIgZGF0YS1jbG9zZXNwb2lsZXI9IiNzcG9pbGVyLXJlZ2lzdGVyIj7QktC+0LnRgtC4PC9hPjwvZGl2PjwvZm9ybT48L2Rpdj48L2Rpdj4=",
            "5be5cd219aac99.00259555": "PGRpdiBjbGFzcz0iaGVhZGVyIj48c3Bhbj7Ql9Cw0LrQsNC30LDRgtGMINC30LLQvtC90L7Qujwvc3Bhbj48YSBocmVmPSIjIiBkYXRhLWFjdGlvbj0iY2xvc2UiPjxpIGNsYXNzPSJpY29uIGljLWNsb3NlIj48L2k+PC9hPjwvZGl2PjxkaXYgY2xhc3M9ImJvZHkgYm94LXNpZGUgYm94LXRvcCI+PGRpdiBjbGFzcz0ic3VjY2VzcyI+0JLQsNGIINC+0LHRgNCw0YLQvdGL0Lkg0LfQstC+0L3QvtC6INC30LDRgNC10LPQuNGB0YLRgNC40YDQvtCy0LDQvSE8L2Rpdj48ZGl2IGNsYXNzPSJlcnJvcnMiPtCd0LXQstC+0LfQvNC+0LbQvdC+INC30LDQutCw0LfQsNGC0Ywg0LfQstC+0L3QvtC6LCDQv9C+0L/RgNC+0LHRg9C50YLQtSDQtdGJ0LUg0YDQsNC3PC9kaXY+PGZvcm0gY2xhc3M9ImZvcm0tZWxlbWVudHMgZm9ybS1sb2dpbiIgaWQ9ImZvcm0tY2FsbGJhY2siIGRhdGEtYWN0aW9uPSIvY2FsbGJhY2svIiBtZXRob2Q9InBvc3QiPjxpbnB1dCB0eXBlPSJoaWRkZW4iIG5hbWU9InNsdWlkIiB2YWx1ZT0iZGIyNGNjNTQyMzA4NWI1YjRmNjhlMDg2OWI3NDdhMTQ0OTBkNGZhM2E2ZWY2MDdmODA5ZGNlZmUyNTI1ZjdlZiIgLz48bGFiZWwgZm9yPSJsb2dpbiI+PHNwYW4+0KLQtdC70LXRhNC+0L0g0LTQu9GPINGB0LLRj9C30Lg8L3NwYW4+PGlucHV0IHR5cGU9InRlbCIgbmFtZT0icGhvbmUiIGNsYXNzPSJtYXNrZWQiIG1hc2s9Iis3ICg5OTkpIDk5OS05OS05OSIgbWFzay1wbGFjZWhvbGRlcj0iXyIgcGxhY2Vob2xkZXI9Iis3IChfX18pIF9fXy1fXy1fXyIgdmFsdWU9IiIgLz48L2xhYmVsPjxkaXYgY2xhc3M9InRjZW50ZXIiIHN0eWxlPSJwYWRkaW5nOiAwIDAgMjVweDsiPjxkaXYgY2xhc3M9InN1Ym1pdC1oaW50Ij7QndCw0LbQuNC80LDRjyDQutC90L7Qv9C60YMgItC30LDQutCw0LfQsNGC0Ywg0LfQstC+0L3QvtC6IiDRjyDRgdC+0LPQu9Cw0YjQsNGO0YHRjCDRgSA8YSBocmVmPSIvcHVibGljb2ZmZXIvI3ByaXZhY3kiIHRhcmdldD0iX2JsYW5rIj7Qv9C+0LvQuNGC0LjQutC+0Lkg0LrQvtC90YTQuNC00LXQvdGG0LjQsNC70YzQvdC+0YHRgtC4PC9hPjwvZGl2PjxhIGhyZWY9IiMiIGNsYXNzPSJidG4gYnRuLWZ3IGJ0bi1wcmltYXJ5IGJ0bi1sZyBidG4tc3VibWl0Ij7Qt9Cw0LrQsNC30LDRgtGMINC30LLQvtC90L7QujwvYT48L2Rpdj48L2Zvcm0+PC9kaXY+",
            "5be5cd219b9320.96740656": "PGRpdiBjbGFzcz0iaGVhZGVyIj48c3Bhbj7QktGLINC80L7QttC10YLQtSDRgdC+0L7QsdGJ0LjRgtGMINC+INC90LXRgtC+0YfQvdC+0YHRgtC4INCyINC+0L/QuNGB0LDQvdC40Lg6PC9zcGFuPjxhIGhyZWY9IiMiIGRhdGEtYWN0aW9uPSJjbG9zZSI+PGkgY2xhc3M9Imljb24gaWMtY2xvc2UiPjwvaT48L2E+PC9kaXY+PGRpdiBjbGFzcz0iYm9keSBib3gtc2lkZSBib3gtdG9wIj48ZGl2IGNsYXNzPSJzdWNjZXNzIj7QktCw0YjQtSDQvtCx0YDQsNGJ0LXQvdC40LUg0LfQsNGA0LXQs9C40YHRgtGA0LjRgNC+0LLQsNC90L4hPC9kaXY+PGRpdiBjbGFzcz0iZXJyb3JzIj7QndC10LLQvtC30LzQvtC20L3QviDQt9Cw0YDQtdCz0LjRgdGC0YDQuNGA0L7QstCw0YLRjCDQvtCx0YDQsNGJ0LXQvdC40LUsINC/0L7Qv9GA0L7QsdGD0LnRgtC1INC10YnQtSDRgNCw0Lc8L2Rpdj48Zm9ybSBjbGFzcz0iZm9ybS1lbGVtZW50cyBmb3JtLWxvZ2luIiBkYXRhLWFjdGlvbj0iL2JhZC9oZWxwZGVzay8iIG1ldGhvZD0icG9zdCIgaWQ9ImZvcm0tY29udGVudC1hYnVzZSI+PGlucHV0IHR5cGU9ImhpZGRlbiIgbmFtZT0ic2x1aWQiIHZhbHVlPSJkYjI0Y2M1NDIzMDg1YjViNGY2OGUwODY5Yjc0N2ExNDQ5MGQ0ZmEzYTZlZjYwN2Y4MDlkY2VmZTI1MjVmN2VmIiAvPjxsYWJlbCBmb3I9InNpdGVwYWdlIj48c3Bhbj7QkNC00YDQtdGBINGB0YLRgNCw0L3QuNGG0Ys8L3NwYW4+PGlucHV0IHR5cGU9InRleHQiIG5hbWU9InNpdGVwYWdlIiAvPjwvbGFiZWw+PGxhYmVsIGZvcj0iZXJyb3JtZXNzYWdlIj48c3Bhbj7QntGI0LjQsdC60LA8L3NwYW4+PHRleHRhcmVhIG5hbWU9ImVycm9ybWVzc2FnZSIgcGxhY2Vob2xkZXI9ItCe0L/QuNGI0LjRgtC1INC+0YjQuNCx0LrRgyI+PC90ZXh0YXJlYT48L2xhYmVsPjxsYWJlbCBmb3I9ImVycm9yY29tbWVudCI+PHNwYW4+0JrQvtC80LzQtdC90YLQsNGA0LjQuTwvc3Bhbj48dGV4dGFyZWEgbmFtZT0iZXJyb3Jjb21tZW50IiBwbGFjZWhvbGRlcj0i0JTQvtC/0L7Qu9C90LjRgtC10LvRjNC90LDRjyDQuNC90YTQvtGA0LzQsNGG0LjRjyI+PC90ZXh0YXJlYT48L2xhYmVsPjxsYWJlbCBmb3I9ImxvZ2luIj48c3Bhbj5FLW1haWw8L3NwYW4+PGlucHV0IHR5cGU9InRleHQiIG5hbWU9ImxvZ2luIiB2YWx1ZT0iIi8+PC9sYWJlbD48ZGl2IGNsYXNzPSJzdWJtaXQtaGludCI+0J3QsNC20LjQvNCw0Y8g0LrQvdC+0L/QutGDICLQvtGC0L/RgNCw0LLQuNGC0YwiINGPINGB0L7Qs9C70LDRiNCw0Y7RgdGMINGBIDxhIGhyZWY9Ii9wdWJsaWNvZmZlci8jcHJpdmFjeSIgdGFyZ2V0PSJfYmxhbmsiPtC/0L7Qu9C40YLQuNC60L7QuSDQutC+0L3RhNC40LTQtdC90YbQuNCw0LvRjNC90L7RgdGC0Lg8L2E+PC9kaXY+PGRpdiBjbGFzcz0idGNlbnRlciIgc3R5bGU9InBhZGRpbmc6IDAgMCAyNXB4OyI+PGEgaHJlZj0iIyIgY2xhc3M9ImJ0biBidG4tZncgYnRuLXByaW1hcnkgYnRuLWxnIGJ0bi1zdWJtaXQiPtC+0YLQv9GA0LDQstC40YLRjDwvYT48L2Rpdj48L2Zvcm0+PC9kaXY+",
            "5be5cd219bfc34.74523451": "PGRpdiBjbGFzcz0iaGVhZGVyIj48c3Bhbj7QodGA0LDQstC90LXQvdC40LU8L3NwYW4+PGEgaHJlZj0iIyIgZGF0YS1hY3Rpb249ImNsb3NlIj48aSBjbGFzcz0iaWNvbiBpYy1jbG9zZSI+PC9pPjwvYT48L2Rpdj48ZGl2IGNsYXNzPSJib2R5IGJveC1zaWRlIGJveC10b3AiPjxwPtCU0L7QsdCw0LLRjNGC0LUg0YLQvtCy0LDRgNGLINC00LvRjyDRgdGA0LDQstC90LXQvdC40Y88L3A+PGRpdiBjbGFzcz0idGNlbnRlciIgc3R5bGU9InBhZGRpbmc6IDI1cHggMCAyNXB4IDA7Ij48YSBocmVmPSIvY2F0YWxvZy8iIGNsYXNzPSJidG4gYnRuLWZ3IGJ0bi1wcmltYXJ5IGJ0bi1sZyBidG4tc3VibWl0Ij7Qv9C10YDQtdC50YLQuCDQsiDQutCw0YLQsNC70L7QszwvYT48L2Rpdj48L2Rpdj4="
        };
    </script>
    <script type="text/javascript" src="/js/jquery-3.1.1.js"></script>
    <script type="text/javascript" src="/js/underscore-min.js"></script>
    <script type="text/javascript" src="/js/jquery.dotdotdot.js"></script>
    <script type="text/javascript" src="/js/jquery.inputmask.min.js"></script>
    <script type="text/javascript" src="/js/base64.js?rnd=<?php echo $_rnd; ?>"></script>
    <script type="text/javascript" src="/js/main.v2.js?rnd=<?php echo $_rnd; ?>"></script>
    <script type="text/javascript" src="/js/main.js?rnd=<?php echo $_rnd; ?>"></script>
    <script type="text/javascript" src="/js/main.new.js?rnd=<?php echo $_rnd; ?>"></script>
    <script type="text/javascript" src="/js/lozad.js?rnd=<?php echo $_rnd; ?>"></script>
    <script type="text/javascript" src="/js/lk.js?rnd=<?php echo $_rnd; ?>"></script>
    <script type="text/javascript" src="/js/product-card-v2.js?rnd=<?php echo $_rnd; ?>"></script>
    <script type="text/javascript" src="/js/cart.js?rnd=<?php echo $_rnd; ?>"></script>
    <script type="text/javascript" src="/js/construct.js?rnd=<?php echo $_rnd; ?>"></script>
    <script type="text/javascript" src="/js/contacts.js?rnd=<?php echo $_rnd; ?>"></script>
    <script type="text/javascript" src="/js/search_results.js?rnd=<?php echo $_rnd; ?>"></script>
    <script type="text/javascript" src="/js/ga.helper.js?rnd=<?php echo $_rnd; ?>"></script>
    <script type="text/javascript" src="/js/creditline.helper.js?rnd=<?php echo $_rnd; ?>"></script>
    <script type="text/javascript" src="/js/criteo.helper.js?rnd=<?php echo $_rnd; ?>"></script>
    <script type="text/javascript" src="/js/rr.helper.js?rnd=<?php echo $_rnd; ?>"></script>
    <script type="text/javascript" src="/js/flocktory.helper.js?rnd=<?php echo $_rnd; ?>"></script>
    <script type="text/javascript" src="/js/catalogue-2.js?rnd=<?php echo $_rnd; ?>"></script>
    <script type="text/javascript" src="/libs/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/js/jquery.bxslider/jquery.bxslider.js"></script>
    <script type="text/javascript" src="/js/lib/nouislider.min.js"></script>
    <script type="text/javascript" src="/js/lib/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/lib/slick.min.js"></script>
    <script type="text/javascript" src="/js/lib/inputmask.js"></script>
    <script type="text/javascript" src="/js/lib/swiper.min.js"></script>
    <script type="text/javascript" src="/libs/datepicker/datepicker.min.js"></script>
    <script type="text/javascript" src="/libs/nanoscroller/jquery.nanoscroller.min.js"></script>
    <style type="text/css">
        @keyframes progress-bar-stripes {
            from {
                background-position: 40px 0
            }
            to {
                background-position: 0 0
            }
        }
        
        .progress {
            overflow: hidden;
            height: 20px;
            margin-bottom: 20px;
            background-color: #f5f5f5;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
            box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1)
        }
        
        .progress-bar {
            float: left;
            width: 0%;
            height: 100%;
            font-size: 12px;
            line-height: 20px;
            color: #fff;
            text-align: center;
            background-color: #337ab7;
            -webkit-box-shadow: inset 0 -1px 0 rgba(0, 0, 0, 0.15);
            box-shadow: inset 0 -1px 0 rgba(0, 0, 0, 0.15);
            -webkit-transition: width .6s ease;
            -o-transition: width .6s ease;
            transition: width .6s ease
        }
        
        .progress-striped .progress-bar,
        .progress-bar-striped {
            background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
            background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
            background-image: linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
            -webkit-background-size: 40px 40px;
            background-size: 40px 40px
        }
        
        .progress.active .progress-bar,
        .progress-bar.active {
            -webkit-animation: progress-bar-stripes 2s linear infinite;
            -o-animation: progress-bar-stripes 2s linear infinite;
            animation: progress-bar-stripes 2s linear infinite
        }
        
        .progress-bar-success {
            background-color: #5cb85c
        }
        
        .progress-striped .progress-bar-success {
            background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
            background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
            background-image: linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent)
        }
        
        .progress-bar-info {
            background-color: #5bc0de
        }
        
        .progress-striped .progress-bar-info {
            background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
            background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
            background-image: linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent)
        }
        
        .progress-bar-warning {
            background-color: #f0ad4e
        }
        
        .progress-striped .progress-bar-warning {
            background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
            background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
            background-image: linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent)
        }
        
        .progress-bar-danger {
            background-color: #d9534f
        }
        
        .progress-striped .progress-bar-danger {
            background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
            background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
            background-image: linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent)
        }
        
        .modal-open {
            overflow: hidden
        }
        
        .modal {
            display: none;
            overflow: hidden;
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 1050;
            -webkit-overflow-scrolling: touch;
            outline: 0
        }
        
        .modal.fade .modal-dialog {
            -webkit-transform: translate(0, -25%);
            -ms-transform: translate(0, -25%);
            -o-transform: translate(0, -25%);
            transform: translate(0, -25%);
            -webkit-transition: -webkit-transform 0.3s ease-out;
            -o-transition: -o-transform 0.3s ease-out;
            transition: transform 0.3s ease-out
        }
        
        .modal.in .modal-dialog {
            -webkit-transform: translate(0, 0);
            -ms-transform: translate(0, 0);
            -o-transform: translate(0, 0);
            transform: translate(0, 0)
        }
        
        .modal-open .modal {
            overflow-x: hidden;
            overflow-y: auto
        }
        
        .modal-dialog {
            position: relative;
            width: auto;
            margin: 10px
        }
        
        .modal-content {
            position: relative;
            background-color: #fff;
            border: 1px solid #999;
            border: 1px solid rgba(0, 0, 0, 0.2);
            border-radius: 6px;
            -webkit-box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
            box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
            -webkit-background-clip: padding-box;
            background-clip: padding-box;
            outline: 0
        }
        
        .modal-backdrop {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 1040;
            background-color: #000
        }
        
        .modal-backdrop.fade {
            opacity: 0;
            filter: alpha(opacity=0)
        }
        
        .modal-backdrop.in {
            opacity: .5;
            filter: alpha(opacity=50)
        }
        
        .modal-header {
            padding: 15px;
            border-bottom: 1px solid #e5e5e5
        }
        
        .modal-header .close {
            margin-top: -2px
        }
        
        .modal-title {
            margin: 0;
            line-height: 1.42857143
        }
        
        .modal-body {
            position: relative;
            padding: 15px
        }
        
        .modal-footer {
            padding: 15px;
            text-align: right;
            border-top: 1px solid #e5e5e5
        }
        
        .modal-footer .btn+.btn {
            margin-left: 5px;
            margin-bottom: 0
        }
        
        .modal-footer .btn-group .btn+.btn {
            margin-left: -1px
        }
        
        .modal-footer .btn-block+.btn-block {
            margin-left: 0
        }
        
        .modal-scrollbar-measure {
            position: absolute;
            top: -9999px;
            width: 50px;
            height: 50px;
            overflow: scroll
        }
    </style>
    <div id="-mobile-orders" class="left-panel">
        <a href='#' class='close'></a>
        <h3>Сортировать по</h3>
        <ul class='-radio-set'>
            <li class='checked'>от дешевых к дорогим</li>
            <li>от дорогих к дешевым</li>
            <li>по популярности</li>
            <li>по рейтингу</li>
        </ul>
    </div>
    <div id="-mobile-filters" class="left-panel">
        <a href='#' class='close'></a>
        <h3>Фильтры<em>0</em><a href='#' class='-apply'>Показать</a><a href='#' class='-reset'>Сбросить</a></h3>
        <div class="top-bar selected-filters"></div>
        <div class="-filters-list"></div>
        <div class="-accessories">
            <ul class="-side-items">
                    <li><a class="active" href="#" data-group-id="1566" data-group-link="/compatibles/8965143/blok_pitaniya_1563/">Блок питания<span class="c-blue">47</span></a></li>
                    <li class="group"><a href="#" data-group-id="1565" data-group-link="/compatibles/8965143/iphone5accessories_1565/">Аксессуары для iPhone 5<span class="c-blue">47</span></a>
                        <ul>
                            <li><a href="#" data-group-id="1230922" data-group-link="#">Крепления на велосипед</a><span class="c-blue">13</span></li>
                            <li><a href="#" data-group-id="1230923" data-group-link="#">Средства по уходу</a><span class="c-blue">2</span></li>
                            <li><a href="#" data-group-id="1230924" data-group-link="#">Крепления в авто</a><span class="c-blue">28</span></li>
                        </ul>
                    </li>
                    <li><a href="#" class="" data-group-id="44" data-group-link="/compatibles/8965143/kabeli_dlya_monitora_44/">Кабели для монитора<span class="c-blue">157</span></a></li>
                    <li><a href="#" data-group-id="15660" data-group-link="/compatibles/8965143/blok_pitaniya_1563/">Блок питания<span class="c-blue">47</span></a></li>
                    <li class="group"><a href="#" data-group-id="1565" data-group-link="/compatibles/8965143/iphone5accessories_1565/">Аксессуары для iPhone 5<span class="c-blue">47</span></a>
                        <ul>
                            <li><a href="#" data-group-id="1230922" data-group-link="#">Крепления на велосипед</a><span class="c-blue">13</span></li>
                            <li><a href="#" data-group-id="1230923" data-group-link="#">Средства по уходу</a><span class="c-blue">2</span></li>
                            <li><a href="#" data-group-id="1230924" data-group-link="#">Крепления в авто</a><span class="c-blue">28</span></li>
                        </ul>
                    </li>
                    <li><a href="#" class="" data-group-id="44" data-group-link="/compatibles/8965143/kabeli_dlya_monitora_44/">Кабели для монитора<span class="c-blue">157</span></a></li>
                    <li><a href="#" data-group-id="15661" data-group-link="/compatibles/8965143/blok_pitaniya_1563/">Блок питания<span class="c-blue">47</span></a></li>
                    <li class="group"><a href="#" data-group-id="1565" data-group-link="/compatibles/8965143/iphone5accessories_1565/">Аксессуары для iPhone 5<span class="c-blue">47</span></a>
                        <ul>
                            <li><a href="#" data-group-id="1230922" data-group-link="#">Крепления на велосипед</a><span class="c-blue">13</span></li>
                            <li><a href="#" data-group-id="1230923" data-group-link="#">Средства по уходу</a><span class="c-blue">2</span></li>
                            <li><a href="#" data-group-id="1230924" data-group-link="#">Крепления в авто</a><span class="c-blue">28</span></li>
                        </ul>
                    </li>
                    <li><a href="#" class="" data-group-id="44" data-group-link="/compatibles/8965143/kabeli_dlya_monitora_44/">Кабели для монитора<span class="c-blue">157</span></a></li>
                    <li><a href="#" data-group-id="15662" data-group-link="/compatibles/8965143/blok_pitaniya_1563/">Блок питания<span class="c-blue">47</span></a></li>
                    <li class="group"><a href="#" data-group-id="1565" data-group-link="/compatibles/8965143/iphone5accessories_1565/">Аксессуары для iPhone 5<span class="c-blue">47</span></a>
                        <ul>
                            <li><a href="#" data-group-id="1230922" data-group-link="#">Крепления на велосипед</a><span class="c-blue">13</span></li>
                            <li><a href="#" data-group-id="1230923" data-group-link="#">Средства по уходу</a><span class="c-blue">2</span></li>
                            <li><a href="#" data-group-id="1230924" data-group-link="#">Крепления в авто</a><span class="c-blue">28</span></li>
                        </ul>
                    </li>
                    <li><a href="#" class="" data-group-id="44" data-group-link="/compatibles/8965143/kabeli_dlya_monitora_44/">Кабели для монитора<span class="c-blue">157</span></a></li>
                </ul>
        </div>
    </div>
    <a href="#" id="top-scroller"></a>
</body>

</html>