$(document).ready(function() {
	GaEcommerceHelper.init();
});

var GaEcommerceHelper = {

	init: function() {
		if (0 < $('#ga-ecommerce-data').length) {
			this.dataContainer = $('#ga-ecommerce-data');
			this.trackPage();
		}
	},

	trackPage: function() {
		var pageType = this.getPageType();
		switch (pageType) {
			case 'main':
				//this.trackMainPage();
				break;
			case 'category':
			case 'tags':
			case 'brand':
			case 'filter':
			case 'search':
			case 'compatibles':
			case 'promo':
				//this.trackCategoryPage();
				break;
			case 'product':
				//this.trackProductPage();
				break;
			case 'basket':
				//this.trackCartPage();
				break;
			case 'order':
				this.trackOrderPage();
				break;
			case 'portal':
			case 'static':
			case 'contacts':
				//this.trackGenericPage();
				break;
		}

		this.trackNonImpressions();
		this.trackPromoActions();
	},

	trackGenericPage: function() {
		var obj = this.createDataObject();
		this.sendDataObject(obj);
	},

	trackMainPage: function() {
		var obj = this.createDataObject();
		this.sendDataObject(obj);
	},

	trackCategoryPage: function() {
		var obj = this.createDataObject();
		this.sendDataObject(obj);
	},

	trackProductPage: function() {
		var obj = this.createDataObject();
		obj.dimension18 = this.getAttr('availability');
		obj.ecommerce.detail = {
			products: [{
				id: this.getAttr('product-id'),
				name: this.getAttr('product-name'),
				category: this.getAttr('product-category'),
				price: this.getAttr('product-price'),
				dimension18: this.getAttr('availability')
			}]
		};
		this.sendDataObject(obj);
	},

	trackCartPage: function() {
		var obj = this.createDataObject();
		obj.ecommerce.checkout = {
			step: 1,
			products: []
		};
		this.dataContainer.find('div.basket-product').each(function() {
			var row = $(this);
			obj.ecommerce.checkout.products.push({
				id: row.attr('data-id'),
				name: row.attr('data-name'),
				category: row.attr('data-category'),
				price: row.attr('data-price'),
				quantity: row.attr('data-quantity')
			});
		});
		this.sendDataObject(obj);
	},

	trackNonImpressions: function() {
		var impressions = this.getImpressions();
		if (15 < impressions.length) {
			var obj = {
				event: 'Non_impressions',
				eventCategory: 'Non-Interactions',
				eventAction: 'show',
				eventLabel: 'products',
				ecommerce: {
					impressions: impressions.slice(15)
				}
			};
			this.sendDataObject(obj);
		}
	},

	trackNonImpressionsFromBlock: function(blockIdentity) {
		var impressions = this.getImpressions(blockIdentity);
		if (0 < impressions.length) {
			var obj = {
				event: 'Non_impressions',
				eventCategory: 'Non-Interactions',
				eventAction: 'show',
				eventLabel: 'products',
				ecommerce: {
					impressions: impressions
				}
			};
			this.sendDataObject(obj);
		}
	},

	addProductToCart: function(id, name, category, price, quantity) {
		var obj = this.createDataObject();
		obj.event = 'addToCart';
		obj.ecommerce.currencyCode = 'RUB';
		obj.ecommerce.add = {
			products: [{
				id: id,
				name: name,
				category: category,
				price: price,
				quantity: quantity || 1
			}]
		}
		this.sendDataObject(obj);
	},

	removeProductFromCart: function(id, name, category, price, quantity) {
		if ((null == quantity) || (0 == quantity)) {
			return;
		}
		var obj = this.createDataObject();
		obj.event = 'removeFromCart';
		obj.ecommerce.currencyCode = 'RUB';
		obj.ecommerce.remove = {
			products: [{
				id: id,
				name: name,
				category: category,
				price: price,
				quantity: quantity || 1
			}]
		}
		this.sendDataObject(obj);
	},

	clickProductLink: function(link, id, name, category, price, quantity, actionField) {
		var obj = {
			event: 'productClick',
			ecommerce: {
				click: {
					actionField: { list: actionField || 'generic' },
					products: [{
						id: id,
						name: name,
						category: category,
						price: price,
						quantity: quantity || 1
					}]
				},
				eventCallback: function() {
					document.location = link;
				}
			}
		};
		this.sendDataObject(obj);
	},

	trackPromoActions: function() {
		var actions = this.getPromoActions();
		if (0 < actions.length) {
			var obj = this.createDataObject();
			obj.event = 'promoView';
			obj.ecommerce.promoView = {
				promotions: actions
			};
			if (obj.ecommerce.impressions) {
				delete obj.ecommerce.impressions;
			}
			this.sendDataObject(obj);
		}
	},

	clickPromo: function(link, id, name, creative, position) {
		var obj = this.createDataObject();
		obj.event = 'promotionClick';
		obj.ecommerce.promoClick = {
			promotions: [{
				id: id,
				name: name,
				creative: creative,
				position: position
			}]
		};
		obj.eventCallback = function() {
			document.location = link;
		};
		if (obj.ecommerce.impressions) {
			delete obj.ecommerce.impressions;
		}
		this.sendDataObject(obj);
		if ((navigator.appVersion.indexOf("Mac")!=-1)) {
			document.location = link;
		}
	},

	trackQuickOrderPage: function(order) {
		var obj = this.createDataObject();
		obj.event = 'order';
		obj.type_purchase = 'one_click';
		obj.ecommerce.purchase = {
			actionField: {
				id: order.id,
				revenue: order.amount,
				tax: 0.00,
				shipping: 0.00,
				coupon: ''
			},
			products: []
		};
		for (var i = 0; i < order.products.length; i++) {
			var product = order.products[i];
			obj.ecommerce.purchase.products.push({
				id: product.id,
				name: product.name,
				category: product.categoryName,
				price: product.price,
				quantity: product.quantity
			});
		}
		this.sendDataObject(obj);
	},

	trackOrderPage: function() {
		var obj = this.createDataObject();
		obj.event = 'order';
		obj.type_purchase = this.getAttr('purchase-type');
		obj.ecommerce.purchase = {
			actionField: {
				id: this.getAttr('order-id'),
				revenue: this.getAttr('order-revenue'),
				tax: this.getAttr('order-tax'),
				shipping: this.getAttr('order-shipping'),
				coupon: this.getAttr('order-coupon')
			},
			products: []
		};
		this.dataContainer.find('div.order-product').each(function() {
			var row = $(this);
			obj.ecommerce.purchase.products.push({
				id: row.attr('data-id'),
				name: row.attr('data-name'),
				category: row.attr('data-category'),
				price: row.attr('data-price'),
				quantity: row.attr('data-quantity')
			});
		});
		this.sendDataObject(obj);
	},

	createDataObject: function() {
		var obj = {
			pageType: this.getPageType(),
			clientID: this.getClientId(),
			userID: this.getUserId(),
			auth: this.getAuth(),
			city: this.getCity(),
			ecommerce: {}
		};
		var impressions = this.getImpressions();
		if (0 < impressions.length) {
			obj.ecommerce.impressions = impressions.slice(0, 15);
		}
		return obj;
	},

	getAttr: function(attrName) {
		return this.dataContainer.attr('data-' + attrName);
	},

	getPageType: function() {
		return this.getAttr('page-type');
	},

	getClientId: function() {
		return this.getAttr('client-id');
	},

	getUserId: function() {
		return this.getAttr('user-id');
	},

	getAuth: function() {
		return this.getAttr('auth');
	},

	getCity: function() {
		return this.getAttr('city');
	},

	getImpressions: function(blockIdentity) {
		var container = this.dataContainer;
		if (blockIdentity) {
			container = $(blockIdentity);
		}

		var list = [];
		container.find('div.impression').each(function() {
			var product = $(this);
			var obj = {
				id: product.attr('data-id'),
				name: product.attr('data-name'),
				category: product.attr('data-category'),
				price: product.attr('data-price'),
				position: product.attr('data-position'),
				list: product.attr('data-list')
			};
			list.push(obj);
		});
		return list;
	},

	getPromoActions: function() {
		var list = [];
		this.dataContainer.find('div.promo-action').each(function() {
			var action = $(this);
			var obj = {
				id: action.attr('data-id'),
				name: action.attr('data-name'),
				creative: action.attr('data-creative'),
				position: action.attr('data-position')
			};
			list.push(obj);
		});
		return list;
	},

	sendDataObject: function(dataObject) {
		console.log('ga push', dataObject);
		dataLayer.push(dataObject);
	}

};