<div class="pc-accessories">
    <div class="accessories container responsive-products">
        <div class="tags-dropdown">
            <div class="dropdown-select dropdown-border"><a href="#" data-toggle="dropdown">Блок питания<span class="c-blue">47</span><i class="icon ic-dropdown"></i></a>
                <ul class="search-tags animate-links dropdown-menu extended -side-items">
                    <li><a class="active" href="#" data-group-id="1566" data-group-link="/compatibles/8965143/blok_pitaniya_1563/">Блок питания<span class="c-blue">47</span></a></li>
                    <li class="group"><a href="#" data-group-id="1565" data-group-link="/compatibles/8965143/iphone5accessories_1565/">Аксессуары для iPhone 5</a><span class="c-blue">47</span>
                        <ul>
                            <li><a href="#" data-group-id="1230922" data-group-link="#">Крепления на велосипед</a><span class="c-blue">13</span></li>
                            <li><a href="#" data-group-id="1230923" data-group-link="#">Средства по уходу</a><span class="c-blue">2</span></li>
                            <li><a href="#" data-group-id="1230924" data-group-link="#">Крепления в авто</a><span class="c-blue">28</span></li>
                        </ul>
                    </li>
                    <li><a href="#" class="" data-group-id="44" data-group-link="/compatibles/8965143/kabeli_dlya_monitora_44/">Кабели для монитора<span class="c-blue">157</span></a></li>
                    <li><a href="#" data-group-id="15660" data-group-link="/compatibles/8965143/blok_pitaniya_1563/">Блок питания<span class="c-blue">47</span></a></li>
                    <li class="group"><a href="#" data-group-id="1565" data-group-link="/compatibles/8965143/iphone5accessories_1565/">Аксессуары для iPhone 5</a><span class="c-blue">47</span>
                        <ul>
                            <li><a href="#" data-group-id="1230922" data-group-link="#">Крепления на велосипед</a><span class="c-blue">13</span></li>
                            <li><a href="#" data-group-id="1230923" data-group-link="#">Средства по уходу</a><span class="c-blue">2</span></li>
                            <li><a href="#" data-group-id="1230924" data-group-link="#">Крепления в авто</a><span class="c-blue">28</span></li>
                        </ul>
                    </li>
                    <li><a href="#" class="" data-group-id="44" data-group-link="/compatibles/8965143/kabeli_dlya_monitora_44/">Кабели для монитора<span class="c-blue">157</span></a></li>
                    <li><a href="#" data-group-id="15661" data-group-link="/compatibles/8965143/blok_pitaniya_1563/">Блок питания<span class="c-blue">47</span></a></li>
                    <li class="group"><a href="#" data-group-id="1565" data-group-link="/compatibles/8965143/iphone5accessories_1565/">Аксессуары для iPhone 5</a><span class="c-blue">47</span>
                        <ul>
                            <li><a href="#" data-group-id="1230922" data-group-link="#">Крепления на велосипед</a><span class="c-blue">13</span></li>
                            <li><a href="#" data-group-id="1230923" data-group-link="#">Средства по уходу</a><span class="c-blue">2</span></li>
                            <li><a href="#" data-group-id="1230924" data-group-link="#">Крепления в авто</a><span class="c-blue">28</span></li>
                        </ul>
                    </li>
                    <li><a href="#" class="" data-group-id="44" data-group-link="/compatibles/8965143/kabeli_dlya_monitora_44/">Кабели для монитора<span class="c-blue">157</span></a></li>
                    <li><a href="#" data-group-id="15662" data-group-link="/compatibles/8965143/blok_pitaniya_1563/">Блок питания<span class="c-blue">47</span></a></li>
                    <li class="group"><a href="#" data-group-id="1565" data-group-link="/compatibles/8965143/iphone5accessories_1565/">Аксессуары для iPhone 5</a><span class="c-blue">47</span>
                        <ul>
                            <li><a href="#" data-group-id="1230922" data-group-link="#">Крепления на велосипед</a><span class="c-blue">13</span></li>
                            <li><a href="#" data-group-id="1230923" data-group-link="#">Средства по уходу</a><span class="c-blue">2</span></li>
                            <li><a href="#" data-group-id="1230924" data-group-link="#">Крепления в авто</a><span class="c-blue">28</span></li>
                        </ul>
                    </li>
                    <li><a href="#" class="" data-group-id="44" data-group-link="/compatibles/8965143/kabeli_dlya_monitora_44/">Кабели для монитора<span class="c-blue">157</span></a></li>
                </ul>
            </div>
            <div class="dropdown-select dropdown-border -scnd"><a href="#" data-toggle="dropdown">По популярности<i class="icon ic-dropdown"></i></a>
                <ul class="search-tags animate-links dropdown-menu extended">
                    <li><a class="active" href="#" data-group-id="125616" data-group-link="/compatibles/8965143/blok_pitaniya_1563/">По популярности</a></li>
                    <li><a href="#" data-group-id="125626" data-group-link="/compatibles/8965143/blok_pitaniya_1563/">По цене</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="pc-accessories-products">
        <section class="catalog-products container pb50 bg-white-xs" id="goods">
            <div class="bg-white">
                <div class="column col-lg-20 col-md-20 col-sm-30 col-xs-100 hidden-xs">
                    <div id="filter-sidebar">
                        <div class="filter-box static">
                            <label class="categories">Нужные аксессуары<br>
                                <small>Подбери свой</small>
                            </label>
                            <ul class="clearstyle categories-list list-a">
                                <li><a href="#" class="active" data-group-id="1563" data-group-link="/compatibles/8965143/blok_pitaniya_1563/">Блок питания</a><span class="c-blue">47</span></li>
                                <li class="group"><a href="#" data-group-id="1565" data-group-link="/compatibles/8965143/iphone5accessories_1565/">Аксессуары для iPhone 5</a><span class="c-blue">47</span>
                                    <ul>
                                        <li><a href="#" data-group-id="1230922" data-group-link="#">Крепления на велосипед</a><span class="c-blue">13</span></li>
                                        <li><a href="#" data-group-id="1230923" data-group-link="#">Средства по уходу</a><span class="c-blue">2</span></li>
                                        <li><a href="#" data-group-id="1230924" data-group-link="#">Крепления в авто</a><span class="c-blue">28</span></li>
                                    </ul>
                                </li>
                                <li><a href="#" class="" data-group-id="44" data-group-link="/compatibles/8965143/kabeli_dlya_monitora_44/">Кабели для монитора</a><span class="c-blue">157</span></li>
                                <li><a href="#" data-group-id="15630" data-group-link="/compatibles/8965143/blok_pitaniya_1563/">Блок питания</a><span class="c-blue">47</span></li>
                                <li class="group"><a href="#" data-group-id="1565" data-group-link="/compatibles/8965143/iphone5accessories_1565/">Аксессуары для iPhone 5</a><span class="c-blue">47</span>
                                    <ul>
                                        <li><a href="#" data-group-id="1230922" data-group-link="#">Крепления на велосипед</a><span class="c-blue">13</span></li>
                                        <li><a href="#" data-group-id="1230923" data-group-link="#">Средства по уходу</a><span class="c-blue">2</span></li>
                                        <li><a href="#" data-group-id="1230924" data-group-link="#">Крепления в авто</a><span class="c-blue">28</span></li>
                                    </ul>
                                </li>
                                <li><a href="#" class="" data-group-id="44" data-group-link="/compatibles/8965143/kabeli_dlya_monitora_44/">Кабели для монитора</a><span class="c-blue">157</span></li>
                                <li><a href="#" data-group-id="15631" data-group-link="/compatibles/8965143/blok_pitaniya_1563/">Блок питания</a><span class="c-blue">47</span></li>
                                <li class="group"><a href="#" data-group-id="1565" data-group-link="/compatibles/8965143/iphone5accessories_1565/">Аксессуары для iPhone 5</a><span class="c-blue">47</span>
                                    <ul>
                                        <li><a href="#" data-group-id="1230922" data-group-link="#">Крепления на велосипед</a><span class="c-blue">13</span></li>
                                        <li><a href="#" data-group-id="1230923" data-group-link="#">Средства по уходу</a><span class="c-blue">2</span></li>
                                        <li><a href="#" data-group-id="1230924" data-group-link="#">Крепления в авто</a><span class="c-blue">28</span></li>
                                    </ul>
                                </li>
                                <li><a href="#" class="" data-group-id="44" data-group-link="/compatibles/8965143/kabeli_dlya_monitora_44/">Кабели для монитора</a><span class="c-blue">157</span></li>
                                <li><a href="#" data-group-id="15632" data-group-link="/compatibles/8965143/blok_pitaniya_1563/">Блок питания</a><span class="c-blue">47</span></li>
                                <li class="group"><a href="#" data-group-id="1565" data-group-link="/compatibles/8965143/iphone5accessories_1565/">Аксессуары для iPhone 5</a><span class="c-blue">47</span>
                                    <ul>
                                        <li><a href="#" data-group-id="1230922" data-group-link="#">Крепления на велосипед</a><span class="c-blue">13</span></li>
                                        <li><a href="#" data-group-id="1230923" data-group-link="#">Средства по уходу</a><span class="c-blue">2</span></li>
                                        <li><a href="#" data-group-id="1230924" data-group-link="#">Крепления в авто</a><span class="c-blue">28</span></li>
                                    </ul>
                                </li>
                                <li><a href="#" class="" data-group-id="44" data-group-link="/compatibles/8965143/kabeli_dlya_monitora_44/">Кабели для монитора</a><span class="c-blue">157</span></li>
                            </ul>
                        </div>
                        <div class="filter-box only-mobile accordion noselect"><a href="#filter-orders" data-toggle="accordion" class="opener">Сортировка <i class="icon ic-arrow-down"></i></a>
                            <div id="filter-orders">
                                <ul class="clearstyle checkbox-group radio-mode">
                                    <li class="asc" data-link="/compatibles/8965143/blok_pitaniya_1563/?sort=popularity&amp;dir=asc" data-sort="popularity" data-direction="asc">
                                        <input type="radio" name="order-type[]" value="popularity-asc"><span class="title">по популярности</span></li>
                                    <li class="asc" data-link="/compatibles/8965143/blok_pitaniya_1563/?sort=price&amp;dir=asc" data-sort="price" data-direction="asc">
                                        <input type="radio" name="order-type[]" value="price-asc"><span class="title">по цене</span></li>
                                    <li class="desc" data-link="/compatibles/8965143/blok_pitaniya_1563/?sort=price&amp;dir=desc" data-sort="price" data-direction="desc">
                                        <input type="radio" name="order-type[]" value="price-desc"><span class="title">по цене</span></li>
                                </ul>
                            </div>
                        </div>
                        <div class="filter-box accordion noselect"><a href="#filter-price" data-toggle="accordion" class="opener">Цена, р.<i class="icon ic-arrow-down"></i></a>
                            <div id="filter-price">
                                <div class="range inited noUi-target noUi-ltr noUi-horizontal" data-min="760" data-max="19470" data-start="760" data-finish="19470" query-pattern="/compatibles/8965143/blok_pitaniya_1563/?pr=price-from:price-to" data-name="price">
                                    <div class="inputs">
                                        <input type="text" class="t-min" value="760">
                                        <input type="text" class="t-max" value="19470">
                                    </div>
                                    <input type="hidden" class="min" name="pricemin" value="760">
                                    <input type="hidden" class="max" name="pricemax" value="19470">
                                    <div class="noUi-base">
                                        <div class="noUi-origin" style="left: 0%;">
                                            <div class="noUi-handle noUi-handle-lower" data-handle="0" style="z-index: 5;">
                                                <div class="noUi-tooltip">760<span><span class="rub">a</span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="noUi-connect noUi-draggable" style="left: 0%; right: 0%;"></div>
                                        <div class="noUi-origin" style="left: 100%;">
                                            <div class="noUi-handle noUi-handle-upper" data-handle="1" style="z-index: 4;">
                                                <div class="noUi-tooltip">19.5<span>т.р.</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="slide-hint" style="display: none;">Найдено товаров: <span>1024</span></div>
                                <div class="mobile-select-block">
                                    <div class="values">
                                        <input type="number" class="mobile-min" placeholder="От">
                                        <input type="number" class="mobile-max" placeholder="До">
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <!-- top -->
                
            </div>
        </section>
        <div class="window box-shadow" id="filter-window" style="display: none;"><a href="#" data-action="close" class="open-xs-filter ">Фильтр и сортировка <em></em></a>
            <div class="appendData"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>