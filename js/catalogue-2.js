$(function() {
/*
    setTimeout(function() {
        $(".js-catalog-rows").trigger("click");
    }, 100);
*/
    var hto = false;

    var numberKeyIsValid = function(ev) {
    	if (ev.ctrlKey) return true;
    	if (ev.which<32 || ev.which==46 || ev.which>47 && ev.which<58 && !ev.shiftKey || ev.which>=37 && ev.which<=40 || ev.which>=33 && ev.which<=36 || ev.which>=96 && ev.which<=105) return true;
    	return false;
    } 
    
    $(".-selector a.expander").on("click", function(e) {
        e.preventDefault();
        $(this).parent().toggleClass("expanded");
        return false;
    });
    $(window).on("click", function(e) {
        var _x = $(this).add($(this).parents(".-selector"));
        $(".-selector").not(_x).removeClass("expanded");
        $("#products-list .product-item .info .title-block").each(function(e) {
            var _a = $(this).find("a.title");
            if (_a.outerHeight()>=_a[0].scrollHeight-4) {
                $(this).addClass("not-expandable");
            }
        });
    });
    
    $("#products-list .product-item .info .title-block a.-expander").on("click", function(e) {
        e.preventDefault();
        $(this).parents(".title-block").toggleClass("expanded");
        return false;
    });
    
/*
    $(".filter-box").each(function() {
        var ft = $(this).find(".filter-footer");
        ft.append("<a href='#' class='-apply'>применить</a>");
    });
    $(".filter-box .checkbox-group > li").each(function() {
        var chk = $(this).find('input[type="checkbox"]');
        if (chk.length>0) {
            chk.each(function() {
                $(this).data("default", this.checked);
            });
        }
    });
*/


    $(".-mobile-pan .selected-filters").html($(".bottom-bar.row .top-bar.selected-filters").html());
    $("#-mobile-filters .selected-filters").html($(".bottom-bar.row .top-bar.selected-filters").html());

    $(".-new-cat div#products-list .product-item.cell .-rating strong").each(function() {
        var t = parseInt($(this).text());
        if (t>0) {
            t %= 100;
            if (t==1 || t>20 && t%10==1) $(this).parents(".-rating").addClass("t01");
            else if (t>1 && t<5 || t>20 && t%10>1 && t%10<5) $(this).parents(".-rating").addClass("t02");
            else $(this).parents(".-rating").addClass("t03");
        }
    });
    
    $("a.show-orders").on("click", function(e) {
        e.preventDefault();
        $("#-mobile-orders").addClass("visible");
        $("body").append("<div id='window-opener-background' class='-pan'></div>");
        $("#window-opener-background").fadeIn(200).on("click touchmove", function(e) {
            e.preventDefault();
            $("#-mobile-orders").removeClass("visible");
            $(this).fadeOut(200, function() { $(this).remove(); });
            return false;
        });
        return false;
    });
    $("a.show-filters").on("click", function(e) {
        e.preventDefault();
        $("#-mobile-filters").addClass("visible");
        $("body").append("<div id='window-opener-background' class='-pan'></div>");
        $("#window-opener-background").fadeIn(200).on("click touchmove", function(e) {
            e.preventDefault();
            $("#-mobile-filters").removeClass("visible");
            $(this).fadeOut(200, function() { $(this).remove(); });
            return false;
        });

        refreshMobileFilters();        
       
        return false;
    });
    
    window.refreshMobileFilters = function() {
        $(".-filters-list").html("");
        if ($("#-mobile-filters").hasClass("visible")) {
            $(".filter-box").each(function() {
                var cl = $(this).clone();
                    cl.appendTo(".-filters-list");
                $(this).find(".spoiler").data("id", $(this).find(".spoiler").attr("id")).attr("id", "");
                if (cl.find(".range").length>0) {
                    var r1 = cl.find(".range"), r0 = $(r1.attr("data-source"));
                    r0.insertAfter(r1);
                    r1.remove();
                    rangeInputsInit();
                }
            });
            if ($(".left-panel h3 em").text().trim()!="" && 1*$(".left-panel h3 em").text()>0) $(".left-panel h3 em").show(); 
            else $(".left-panel h3 em").hide(); 
        }
        return true;
    }
    
    $(".left-panel h3 a").on("click", function(e) {
        e.preventDefault();
        if ($(this).hasClass("-reset")) {
            $(".filter-box .btn-block.reset").trigger("click");
        } else {
            $(".filter-box.actions .display").trigger("click");
        }
        $("#window-opener-background").trigger("click");
        return false;
    });
    
    $("#-mobile-filters").on("scroll", function(e) {
        var t = $(this).scrollTop();
        $(this).find("h3").css({ top: t });
    });
    
    $(".left-panel .-accessories>ul>li>a").on("click", function(e) {
        $(this).parents("li").toggleClass("expanded");
    });
    
    $(".left-panel a.close").on("click", function(e) {
        e.preventDefault();
        $("#window-opener-background").trigger("click");
        return false;
    });
    
    $("body").on("click", ".-radio-set li", function(e) {
        e.preventDefault();
        $(this).parent().find(".checked").removeClass("checked");
        $(this).addClass("checked");
        
        //-------------------------------------------------------------------------
        //
        // loadProducts(hintLink, { scrollOnTop: true, li: _, hideFilters: true });
        //
        return false;
    });
    
    $(document).on("mouseenter touchstart", "#products-list.products-rows .product-item.cell .image-container .-thumbs img", function(e) {
        e.preventDefault();
        $(this).parents(".-thumbs").find("img").removeClass("selected");
        $(this).addClass("selected");
        var src = $(this).data("full");
        $(this).parents(".image-container").find("a>img").animate({ opacity: 0 }, 100, function() {
            this.src = src;
            $(this).animate({ opacity: 1 }, 100);
        });
        return false;
    });
    
    $(".-search-block .-block-expander").on("click", function(e) {
        e.preventDefault();
        $("#-search-section").css({ width: $(".catalog-products#goods").width() });
        $("#-search-section aside.groups").css({ width: $("#filter-sidebar").width() });
        $("#-search-section aside.list").css({ width: "calc(100% - " + ($("#filter-sidebar").width()+0) + "px)" });
        $("#-search-section").show(150, function() {
            $(window).trigger("scroll");
        }).css({ display: "flex" });
        $(".filter-box").addClass("-hdn");
        $(".-items-aside").css({ visibility: "hidden" });
        return false;
    });
    
    $("#-search-section aside.groups h3 a").on("click", function(e) {
        e.preventDefault();
        $("#-search-section").hide(150);
        $(".filter-box").removeClass("-hdn");
        $(".-items-aside").css({ visibility: "" });
        return false;
    });

    $(window).on("scroll resize", function(e) {
        if ($("#-search-section").length==0) return true;
        var mh = $("#-search-section").outerHeight(), hh = 50;
        if ($(".-items-aside").css("visibility")!="hidden") mh=1e9;
        if ($(window).width()<1024) hh = 0;
        $(".-items-aside").css({ "max-height": mh });
        var pn = $("#-search-section aside.groups"), t = $(window).scrollTop()+hh, sh = $(window).height() - hh,
            t0 = $("#-search-section").offset().top,
            t1 = t0 + $("#-search-section").outerHeight();
        if (t0>=t) {
            pn.css({ "transform": "" });
        } else if (t+sh >= t1 ) {
            
        } else {
            pn.css({ "transform": "translateY("+(t-t0)+"px)" });
        }
        if ($(window).width()>719 && $(window).width()<1024 && !$("div#products-list").hasClass("products-rows")) {
            $(".product-item.cell .price-block .price>.old-price").each(function() {
                $(this).appendTo($(this).parent().find("strong:not(.old-price)"));
            });
            $(".-new-cat div#products-list:not(.products-rows) .product-item.cell .-rt>.-bonus").each(function() {
                $(this).appendTo($(this).parents(".product-item").find(".-item-actions"));
            });
        } else {
            $(".product-item.cell .price-block .price .old-price").each(function() {
                $(this).insertAfter($(this).parents(".product-item.cell").find(".price-block .price>strong:not(.old-price)"));
            });
            $(".-item-actions .-bonus").each(function() {
                $(this).appendTo($(this).parents(".product-item").find(".-rt"));
            });
        }
        
    });
    
    $("#top-scroller").on("click", function(e) {
        e.preventDefault();
        $("html,body").stop().animate({scrollTop: 0}, 200);
        return false;
    });
    
    $(window).trigger("scroll");
    
    $("#-search-section")
        .on('click', 'aside.groups .-footer a.-apply', function(e) {
            e.preventDefault();
            // ***********************
            //  Get products by search-filters
            // ***********************
            
            $(this).parents("aside").find(".-close-block").trigger("click");
            
            return false;
        })
        .on("click", "aside.groups a.-scrl-to", function(e) {
            e.preventDefault();
            var to = $(this).data("to");
            var el = $(".-block[data-target="+to+"]");
            if (el.length>0) {
                $("html,body").stop().animate({ scrollTop: el.offset().top - 200 }, 150);
            }
            return false;
        })
        .on("click", "aside.list h5 a.-reset", function(e) {
            $(this).parents(".-block").find("li.checked").each(function() {
                $(this).removeClass("checked");
            });
            $(this).addClass("hidden");
        })
        .on("click", "aside.list .-block ul li", function(e) {
            $(this).toggleClass("checked");
            $(this).parents(".-block").find("h5 a.-reset").toggleClass("hidden", $(this).parents(".-block").find("li.checked").length==0);
        })
    
    $(".popular-sets").each(function() {
        var t = $(this).find(">*:not(.-toggle)"), y = -1, dn = -1;
        for (var i=0; i<t.length; i++) {
            if (t.eq(i).offset().top==y) continue;
            if (t.eq(i).offset().top>y && y<0) y = t.eq(i).offset().top;
            else {
                t.eq(i).addClass("-hidden");
                if (dn<0) dn = i;
            }
        }
        var clps = $(this).find(".-toggle a");
        if (dn>0) {
            t.eq(dn-1).addClass("-hidden");
            clps.text(clps.data("collapsed"));
            
            clps.on("click", function(e) {
                e.preventDefault();
                $(this).parents("ul").eq(0).toggleClass("-expanded");
                if ($(this).parents("ul").eq(0).hasClass('-expanded')) {
                    $(this).text($(this).data("expanded"));
                } else {
                    $(this).text($(this).data("collapsed"));
                }
                return false;
            });
            
        } else clps.hide();
    });
    
    $(".top-brands nav .-track").slick({
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			variableWidth: true,
			prevArrow: $(".top-brands nav .-prev"),
			nextArrow: $(".top-brands nav .-next"),
    });


    //----------------------
    // Отображение элементов
    //----------------------
	 if (exists('.popular-brands-list')) {
		 var $popBrands = $("section.bottom-elements .popular-brands-list ul li:not(.show-more)"), vis = true, y = $popBrands.eq(0).offset().top;
		 for (var i=0; i<$popBrands.length; i++) {
			try {
			  if (vis && typeof($popBrands.eq(i+1))!="undefined" && y<$popBrands.eq(i+1).offset().top) vis = false;
			  if (!vis) $popBrands.eq(i).hide();
			} catch(e) {}
		 }
		 if (vis) {
			$("section.bottom-elements .popular-brands-list li.show-more a").hide();
		 }
		 $("section.bottom-elements .popular-brands-list li.show-more a").on("click", function(ev) {
			  ev.preventDefault();
			  $(this).hide();
			  $(this).parents("ul").find("li:not(.show-more)").fadeIn(350);
			  return false;
		 });
	}

    initializeProductsProperties();
	 $('#products-list').on('click', '.product-item .product-params .show-more a', function(ev) {
		ev.preventDefault();
		$(this).parents(".product-params").toggleClass("expanded");
		return false;
	 });

    if ($("body").width()<768) {
        $(".open-xs-filter-wrapper").css({display: "block"}).appendTo("h1");
    }

    //-----------------------
    // Избранное и сравнение
    //-----------------------

    $("body").delegate(".product-item .board a", "click", function(ev) {
        ev.preventDefault();
        var $this = $(this);
        $this.toggleClass("selected");
        var value = $this.hasClass("selected"),
            showHints = !$this.parents("#products-list").hasClass("products-rows");
        if (hto) clearTimeout(hto);

        switch($this.data("type")) {
            case 1: 
                var $h = window.hint( value ? "<i>В избранном</i>" : "<i>Удалено из избранного</i>", $this, true );
                    if (showHints) $h.show();
                    hto = setTimeout(function() {
                        hto = false;
                        $h.close();
                    }, 2000);
            break;

            case 2:
                var $h = window.hint( value ? "<i>В сравнении</i>" : "<i>Удалено из сравнения</i>", $this, true );
                    if (showHints) $h.show();
                    hto = setTimeout(function() {
                        hto = false;
                        $h.close();
                    }, 2000);
            break;
        }
        return false;
    });

    //-----------------------
    // Сброс фильтров
    //-----------------------

    $(".top-bar.selected-filters ul li a").on("click", function(ev) {
        if ($(this).parent().hasClass("remove-all")) {
            $(this).parents("ul").find("li").fadeOut(200, function() { $(this).remove(); $(".top-bar.selected-filters").remove(); });
        } else {
            $(this).parent().fadeOut(200, function() { 
                var t = $(this).parents("ul").find("li").length-1;
                $(this).remove();
                if (t==1) $(".top-bar.selected-filters").remove();
            });
        }
    });

    //-----------------------
    // Узнать стоимость доставки
    //-----------------------

	$("body").delegate(".delivery-get-price", "click", function(ev) {
		ev.preventDefault();
		var _ = $(this);
		var wr = _.parent().parent();
		wr.find('.delivery-get-price').addClass('disabled');
		$.ajax({
			url: '/catalog/receipt-terms/',
			method: 'get',
			data: { id: wr.attr('data-product-id') },
			dataType: 'json',
			success: function(data) {
				if (data.success) {
					wr.html(data.content);
				} else {
					wr.html('<p>К сожалению, мы не можем доставить товар в указанный вами город</p>');
				}
			},
			complete: function() {
				wr.find('.delivery-get-price').removeClass('disabled');
			}
		});
		return false;
	});


    //-----------------------
    // Фильтр по категории в мобильной версии
    //-----------------------

    $("#filter-category ul li>a:not(.opener)").on("click", function(ev) {
        ev.preventDefault();
        $("#filter-category ul li.current").removeClass("current");
        $(this).parent().addClass('current');
        return false;
    });

    //-----------------------
    // Применение, сброс и закрытие фильтров в мобильной
    //-----------------------

    $("#filter-window .open-xs-filter em, #filter-window .filter-box.actions2 a").on("click", function(ev) {
        ev.preventDefault();
        //...

        $("#filter-window").fadeOut(300);
        return false;
    });


    //-----------------------
    // Текстовые поля в слайдере
    //-----------------------
    var setSliderValues = function(slider, values, inps) {
        var tmp;
        if (slider.options.start[0]*1>values[0]*1) values[0] = slider.options.start[0]; 
        if (slider.options.start[1]<values[1]) values[1] = slider.options.start[1]; 
        if (slider.options.start[1]*1<values[0]*1) values[0] = slider.options.start[1];
        if (slider.options.start[0]>values[1]) values[1] = slider.options.start[0]; 
        if (values[0]>values[1]) tmp = values[0], values[0]=values[1], values[1] = tmp;
        inps.eq(0).val(values[0]);
        inps.eq(1).val(values[1]);
        slider.set(values);

    }

    $(".range .inputs input").on("keyup keydown", function(ev) {
        if (ev.which==13) {
            ev.preventDefault();
            var vals = [0,0];
            if ($(this).hasClass("t-min")) {
                vals[0] = $(this).val()*1;
                vals[1] = $(this).parent().find(".t-max").val()*1;
            } else {
                vals[1] = $(this).val()*1;
                vals[0] = $(this).parent().find(".t-min").val()*1;
            }
            setSliderValues($(this).data('slider'), vals, $(this).parent().find(".t-min, .t-max"));
            return false;
        }
        if (!numberKeyIsValid(ev)) { ev.preventDefault(); return false; }
        return true;
    }).on("blur", function() {
            var vals = [0,0];
            if ($(this).hasClass("t-min")) {
                vals[0] = $(this).val()*1;
                vals[1] = $(this).parent().find(".t-max").val()*1;
            } else {
                vals[1] = $(this).val()*1;
                vals[0] = $(this).parent().find(".t-min").val()*1;
            }
            setSliderValues($(this).data('slider'), vals, $(this).parent().find(".t-min, .t-max"));
    });


    //-----------------------
    // Страничная навигация
    //-----------------------

    $(".pages-navigation .show-more2 a").on("click", function(ev) {
        ev.preventDefault();
        var $this = $(this);
        $this.parent().addClass('disabled');
        setTimeout(function() {
            $this.parent().removeClass('disabled');
            var items = $(".product-item");
            for (var i=0; i<6; i++) {
                var x = items.eq(i).clone();
                x.insertBefore(".pages-navigation");
            }
            $(".pages-navigation .pages-list li.current").removeClass("current").next().addClass('current');
        }, 1200);
        return false;
    });

	 $("header a.catalogue-toggle").on("click", function(ev) {
        ev.preventDefault();
        $(".catalog-menu").fadeIn(200).scrollTop(0);
        return false;
    });

    $(".catalog-menu .header a.close").on("click", function(ev) {
        ev.preventDefault();
        $(".catalog-menu").fadeOut(200);
        return false;
    });

    $(".catalog-menu .city-selector a").on("click", function(ev) {
		var _ = $(this);
        $(".catalog-menu").fadeOut(100, function() {
    		if(_.attr('href') === '#'){
    			e.preventDefault();
    			var data = _.data();
    			var $container = $(data.window);
    			if($container.length && !data.hover){
    				data.magnet = _;
    				$container.window('toggle', data);
    			}
    		}
        });
    });

	 $('body').on('click', '.filter-box.actions .reset', function() {
		var _ = $(this);
		var hintLink = _.attr('data-link');
		if (isMobileDevice()) {
			loadProducts(hintLink, { scrollOnTop: true, li: _, hideFilters: true });
		} else {
			//hintProducts(hintLink, _, { hideHint: true });
			loadProducts(hintLink, { scrollOnTop: true, li: _ });
		}
	 });
	 $('body').on('click', '.filter-box.actions .display', function() {
		var _ = $(this);
		var hintLink = _.attr('data-link');
		loadProducts(hintLink, { scrollOnTop: false, li: _, hideFilters: true });
		return false;
	 });
	 $('body').on('click', '.top-bar.selected-filters .remove-all a', function() {
		var _ = $('.filter-box.actions .reset');
		var hintLink = _.attr('data-link');
		if (isMobileDevice()) {
			loadProducts(hintLink, { scrollOnTop: true, li: _, hideFilters: true });
		} else {
			loadProducts(hintLink, { scrollOnTop: false, li: _ });
		}
		return false;
	 });
	 $('body').on('click', '.top-bar.selected-filters .remove-filter', function() {
		var _ = $(this);
		if (_.attr('data-target')) {
			var li = $('#' + _.attr('data-target'));
			loadProducts(li.attr('data-link'), { scrollOnTop: false });
		}
	 });

	 $('body').on('click', '.selected-filters .remove-filter-price', function() {
		var _ = $(this);
		if (_.attr('data-link')) {
			loadProducts(_.attr('data-link'), { scrollOnTop: false });
		}
	 });

	 $('body').on('click', '.pages-navigation .show-more', function() {
		var _ = $(this);
		if (_.attr('data-link')) {
			_.addClass('disabled');
			loadProducts(_.attr('data-link'), {
				scrollOnTop: false,
				appendData: true,
				success: function() {
					_.removeClass('disabled');
				}
			});
		}
	 });

    
});

function initializeProductsProperties() {
	$("#products-list .product-item .product-params:not(.initialized)").each(function() {
		var _ = $(this), $lis = _.find("li");
		if ($lis.length > 6) {
		    for (var i=6; i<$lis.length; i++) $lis.eq(i).addClass("-hidden");
			_.append("<li class='show-more'><a href='#'></a></li>");
		}
		_.addClass('initialized');
	});
}

$(document).ready(function() {
	if (exists('.catalog-products.subcategories #filter-sidebar')) {
		$('.catalog-products.subcategories #filter-sidebar .subcategories-categories').sticky({
			topSpacing: 70,
			bottomSpacing: 800
		});
	}
});