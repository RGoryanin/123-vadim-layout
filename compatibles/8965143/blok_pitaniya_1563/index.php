<?php

    header ("Content-Type: text/html; charset=utf-8", true);
?><section class="catalog-products container pb50 bg-white-xs" id="goods">
    <div class="bg-white">
        <div class="column col-lg-20 col-md-20 col-sm-30 col-xs-100 hidden-xs">
            <div id="filter-sidebar">
                <div class="filter-box only-mobile accordion noselect"><a href="#filter-orders" data-toggle="accordion" class="opener">Сортировка <i class="icon ic-arrow-down"></i></a>
                    <div id="filter-orders">
                        <ul class='clearstyle checkbox-group radio-mode'>
                            <li class="asc" data-link="/compatibles/8965143/blok_pitaniya_1563/?sort=popularity&dir=asc" data-sort="popularity" data-direction="asc">
                                <input type="radio" name="order-type[]" value="popularity-asc" /><span class="title">по популярности</span></li>
                            <li class="asc" data-link="/compatibles/8965143/blok_pitaniya_1563/?sort=price&dir=asc" data-sort="price" data-direction="asc">
                                <input type="radio" name="order-type[]" value="price-asc" /><span class="title">по цене</span></li>
                            <li class="desc" data-link="/compatibles/8965143/blok_pitaniya_1563/?sort=price&dir=desc" data-sort="price" data-direction="desc">
                                <input type="radio" name="order-type[]" value="price-desc" /><span class="title">по цене</span></li>
                        </ul>
                    </div>
                </div>
                <div class="filter-box noselect static"><label>Цена, р.</label>
                    <div id="filter-price">
                        <div class="range" data-min="760" data-max="19470" data-start="760" data-finish="19470" query-pattern="/compatibles/8965143/blok_pitaniya_1563/?pr=price-from:price-to" data-name="price"></div>
                        <div class="slide-hint" style="display: none;">Найдено товаров: <span>1024</span></div>
                        <div class="mobile-select-block">
                            <div class="values">
                                <input type="number" class="mobile-min" placeholder="От">
                                <input type="number" class="mobile-max" placeholder="До">
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        <!-- top -->
        <div class="column col-lg-80 col-md-80 col-sm-70 col-xs-100">
            <div class="bg-white">
                <div class="row top-bar -item">
                    <div class="open-xs-filter-wrapper hidden-lg hidden-md hidden-sm"><a href="#" id="filter-opener" class="open-xs-filter ">Фильтр</a></div>
                    <div class="clearfix"></div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="toolbar sort animate-links fleft visible-lg visible-md hidden-sm hidden-xs hide-in-js catalog-sorting"><!--<span>Сортировать по</span><a href="#" data-link="/compatibles/8965143/blok_pitaniya_1563/?sort=popularity&dir=asc" class="btn btn-default active" data-sort="popularity" data-direction="asc">Популярности</a><a href="#" data-link="/compatibles/8965143/blok_pitaniya_1563/?sort=price&dir=asc" class="btn btn-default" data-sort="price" data-direction="asc">Цене</a>--></div>
                    </div>
                    <div class="row top-bar selected-filters"></div>
                </div>
            </div>
            <!-- END top -->
            <div id="products-list" class="pi-4">
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80033956" data-product-external-id="708343">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-be_quiet/bp_atx_850_vt_be_quiet_dark_power_pro_11_bn253_art80033956/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80033956.jpg" class="lazy-img" alt="БП ATX 850 Вт Be quiet Dark Power Pro 11 BN253" title="БП ATX 850 Вт Be quiet Dark Power Pro 11 BN253" /></a>
                    </div>
                    <div class="info">
                        <div class="title-block"><a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-be_quiet/bp_atx_850_vt_be_quiet_dark_power_pro_11_bn253_art80033956/" class="title">БП ATX 850 Вт Be quiet Dark Power Pro 11 BN253</a>
                            <div class="product-rate">
                                <div class="art">Арт. 80033956</div>
                                <div class="sp-inline-rating sp-inline-rating-container-80033956"></div>
                            </div>
                        </div>
                        <div class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>14 700 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-80033956"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=80033956" data-product-id="80033956" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80033956 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="80033956"><span class="pick-up"><b>Самовывоз</b> (сегодня) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (сегодня) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="8811758" data-product-external-id="483878">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-exegate/bp_atx_450_vt_exegate_atx_xp450_art8811758/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/8811758.jpg" class="lazy-img" alt="БП ATX 450 Вт Exegate ATX-XP450" title="БП ATX 450 Вт Exegate ATX-XP450" /></a>
                    </div>
                    <ul class="stickers">
                        <li class="x-sale"><span>-9%</span></li>
                    </ul>
                    <div class="info">
                        <div class="title-block"><a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-exegate/bp_atx_450_vt_exegate_atx_xp450_art8811758/" class="title">БП ATX 450 Вт Exegate ATX-XP450</a>
                            <div class="product-rate">
                                <div class="art">Арт. 8811758</div>
                                <div class="sp-inline-rating sp-inline-rating-container-8811758"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-8811758" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>980 р.</strong><strong class="old-price">1 080 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-8811758"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=8811758" data-product-id="8811758" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-8811758 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="8811758"><span class="pick-up"><b>Самовывоз</b> (сегодня) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (сегодня) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="8072820" data-product-external-id="180171">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-corsair/blok_pitaniya_650_corsair_vs650_cp_9020051_eu_retail_art8072820/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/8072820.jpg" class="lazy-img" alt="БП ATX 650 Вт Corsair VS650 (CP-9020051-EU)" title="БП ATX 650 Вт Corsair VS650 (CP-9020051-EU)" /></a>
                    </div>
                    <ul class="stickers">
                        <li class="x-sale"><span>-7%</span></li>
                    </ul>
                    <div class="info">
                        <div class="title-block"><a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-corsair/blok_pitaniya_650_corsair_vs650_cp_9020051_eu_retail_art8072820/" class="title">БП ATX 650 Вт Corsair VS650 (CP-9020051-EU)</a>
                            <div class="product-rate">
                                <div class="art">Арт. 8072820</div>
                                <div class="sp-inline-rating sp-inline-rating-container-8072820"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-8072820" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>3 950 р.</strong><strong class="old-price">4 250 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-8072820"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=8072820" data-product-id="8072820" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-8072820 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="8072820"><span class="pick-up"><b>Самовывоз</b> (сегодня) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (сегодня) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="8072240" data-product-external-id="211993">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-fsp/blok_pitaniya_400_fsp_atx_400pnr_oem_art8072240/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/8072240.jpg" class="lazy-img" alt="БП ATX 400 Вт FSP ATX-400PNR" title="БП ATX 400 Вт FSP ATX-400PNR" /></a>
                    </div>
                    <ul class="stickers">
                        <li class="x-sale"><span>-10%</span></li>
                    </ul>
                    <div class="info">
                        <div class="title-block"><a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-fsp/blok_pitaniya_400_fsp_atx_400pnr_oem_art8072240/" class="title">БП ATX 400 Вт FSP ATX-400PNR</a>
                            <div class="product-rate">
                                <div class="art">Арт. 8072240</div>
                                <div class="sp-inline-rating sp-inline-rating-container-8072240"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-8072240" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>1 910 р.</strong><strong class="old-price">2 140 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-8072240"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=8072240" data-product-id="8072240" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-8072240 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="8072240"><span class="pick-up"><b>Самовывоз</b> (сегодня) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (сегодня) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80005288" data-product-external-id="679094">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-3cott/bp_atx_500_vt_3cott_3c_atx500w_art80005288/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80005288.jpg" class="lazy-img" alt="БП ATX 500 Вт 3Cott 3C-ATX500W" title="БП ATX 500 Вт 3Cott 3C-ATX500W" /></a>
                    </div>
                    <div class="info">
                        <div class="title-block"><a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-3cott/bp_atx_500_vt_3cott_3c_atx500w_art80005288/" class="title">БП ATX 500 Вт 3Cott 3C-ATX500W</a>
                            <div class="product-rate">
                                <div class="art">Арт. 80005288</div>
                                <div class="sp-inline-rating sp-inline-rating-container-80005288"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-80005288" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>1 254 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-80005288"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=80005288" data-product-id="80005288" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80005288 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="80005288"><span class="pick-up"><b>Самовывоз</b> (завтра) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (сегодня) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80033955" data-product-external-id="708342">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-be_quiet/bp_atx_1000_vt_be_quiet_dark_power_pro_11_bn254_art80033955/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80033955.jpg" class="lazy-img" alt="БП ATX 1000 Вт Be quiet Dark Power Pro 11 BN254" title="БП ATX 1000 Вт Be quiet Dark Power Pro 11 BN254" /></a>
                    </div>
                    <div class="info">
                        <div class="title-block"><a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-be_quiet/bp_atx_1000_vt_be_quiet_dark_power_pro_11_bn254_art80033955/" class="title">БП ATX 1000 Вт Be quiet Dark Power Pro 11 BN254</a>
                            <div class="product-rate">
                                <div class="art">Арт. 80033955</div>
                                <div class="sp-inline-rating sp-inline-rating-container-80033955"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-80033955" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>19 470 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-80033955"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=80033955" data-product-id="80033955" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80033955 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="80033955"><span class="pick-up"><b>Самовывоз</b> (завтра) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (сегодня) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="8624818" data-product-external-id="289851">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-fsp/fsp_dion_d400_art8624818/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/8624818.jpg" class="lazy-img" alt="БП ATX 450 Вт FSP Q-Dion QD-450 80Plus" title="БП ATX 450 Вт FSP Q-Dion QD-450 80Plus" /></a>
                    </div>
                    <ul class="stickers">
                        <li class="x-sale"><span>-8%</span></li>
                    </ul>
                    <div class="info">
                        <div class="title-block"><a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-fsp/fsp_dion_d400_art8624818/" class="title">БП ATX 450 Вт FSP Q-Dion QD-450 80Plus</a>
                            <div class="product-rate">
                                <div class="art">Арт. 8624818</div>
                                <div class="sp-inline-rating sp-inline-rating-container-8624818"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-8624818" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>2 110 р.</strong><strong class="old-price">2 300 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-8624818"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=8624818" data-product-id="8624818" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-8624818 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="8624818"><span class="pick-up"><b>Самовывоз</b> (завтра) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (сегодня) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80175469" data-product-external-id="850179">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-aerocool/_art80175469/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80175469.jpg" class="lazy-img" alt="Блок питания ATX 450 Вт Aerocool VX-450 PLUS" title="Блок питания ATX 450 Вт Aerocool VX-450 PLUS" /></a>
                    </div>
                    <div class="info">
                        <div class="title-block"><a href="/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-aerocool/_art80175469/" class="title">Блок питания ATX 450 Вт Aerocool VX-450 PLUS</a>
                            <div class="product-rate">
                                <div class="art">Арт. 80175469</div>
                                <div class="sp-inline-rating sp-inline-rating-container-80175469"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-80175469" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>1 860 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-80175469"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=80175469" data-product-id="80175469" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80175469 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="80175469"><span class="pick-up"><b>Самовывоз</b> (послезавтра) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (послезавтра) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80029036" data-product-external-id="703317">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-xilence/bp_atx_400_vt_xilence_xp400r6_xn041_art80029036/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80029036.jpg" class="lazy-img" alt="БП ATX 400 Вт Xilence XP400R6 XN041" title="БП ATX 400 Вт Xilence XP400R6 XN041" /></a>
                    </div>
                    <div class="info">
                        <div class="title-block"><a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-xilence/bp_atx_400_vt_xilence_xp400r6_xn041_art80029036/" class="title">БП ATX 400 Вт Xilence XP400R6 XN041</a>
                            <div class="product-rate">
                                <div class="art">Арт. 80029036</div>
                                <div class="sp-inline-rating sp-inline-rating-container-80029036"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-80029036" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>1 950 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-80029036"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=80029036" data-product-id="80029036" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80029036 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="80029036"><span class="pick-up"><b>Самовывоз</b> (послезавтра) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (послезавтра) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80029033" data-product-external-id="703314">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-xilence/bp_atx_400_vt_xilence_xp400r7_xn051_art80029033/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80029033.jpg" class="lazy-img" alt="БП ATX 400 Вт Xilence XP400R7 XN051" title="БП ATX 400 Вт Xilence XP400R7 XN051" /></a>
                    </div>
                    <div class="info">
                        <div class="title-block"><a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-xilence/bp_atx_400_vt_xilence_xp400r7_xn051_art80029033/" class="title">БП ATX 400 Вт Xilence XP400R7 XN051</a>
                            <div class="product-rate">
                                <div class="art">Арт. 80029033</div>
                                <div class="sp-inline-rating sp-inline-rating-container-80029033"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-80029033" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>1 530 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-80029033"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=80029033" data-product-id="80029033" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80029033 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="80029033"><span class="pick-up"><b>Самовывоз</b> (послезавтра) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (послезавтра) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="8992805" data-product-external-id="666465">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-inwin/bp_atx_400_vt_inwin_pm_400atx_art8992805/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/8992805.jpg" class="lazy-img" alt="БП ATX 400 Вт Powerman PM-400ATX" title="БП ATX 400 Вт Powerman PM-400ATX" /></a>
                    </div>
                    <div class="info">
                        <div class="title-block"><a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-inwin/bp_atx_400_vt_inwin_pm_400atx_art8992805/" class="title">БП ATX 400 Вт Powerman PM-400ATX</a>
                            <div class="product-rate">
                                <div class="art">Арт. 8992805</div>
                                <div class="sp-inline-rating sp-inline-rating-container-8992805"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-8992805" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>1 970 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-8992805"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=8992805" data-product-id="8992805" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-8992805 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="8992805"><span class="pick-up"><b>Самовывоз</b> (послезавтра) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (послезавтра) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80007208" data-product-external-id="681053">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-crown/bp_atx_650_vt_crown_cm_ps650w_smart_art80007208/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80007208.jpg" class="lazy-img" alt="БП ATX 650 Вт Crown CM-PS650W Smart" title="БП ATX 650 Вт Crown CM-PS650W Smart" /></a>
                    </div>
                    <div class="info">
                        <div class="title-block"><a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-crown/bp_atx_650_vt_crown_cm_ps650w_smart_art80007208/" class="title">БП ATX 650 Вт Crown CM-PS650W Smart</a>
                            <div class="product-rate">
                                <div class="art">Арт. 80007208</div>
                                <div class="sp-inline-rating sp-inline-rating-container-80007208"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-80007208" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>2 230 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-80007208"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=80007208" data-product-id="80007208" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80007208 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="80007208"><span class="pick-up"><b>Самовывоз</b> (послезавтра) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (послезавтра) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="8990266" data-product-external-id="663888">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-inwin/bp_atx_450_vt_inwin_rb_s450t7_0_art8990266/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/8990266.jpg" class="lazy-img" alt="БП ATX 450 Вт InWin RB-S450T7-0" title="БП ATX 450 Вт InWin RB-S450T7-0" /></a>
                    </div>
                    <div class="info">
                        <div class="title-block"><a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-inwin/bp_atx_450_vt_inwin_rb_s450t7_0_art8990266/" class="title">БП ATX 450 Вт InWin RB-S450T7-0</a>
                            <div class="product-rate">
                                <div class="art">Арт. 8990266</div>
                                <div class="sp-inline-rating sp-inline-rating-container-8990266"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-8990266" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>1 600 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-8990266"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=8990266" data-product-id="8990266" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-8990266 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="8990266"><span class="pick-up"><b>Самовывоз</b> (послезавтра) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (послезавтра) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80056126" data-product-external-id="731036">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-be_quiet/blok_pitaniya_be_quiet_tfx_power_2_300_vt_art80056126/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80056126.jpg" class="lazy-img" alt="БП TFX 300 Вт Be quiet TFX POWER 2 BN228" title="БП TFX 300 Вт Be quiet TFX POWER 2 BN228" /></a>
                    </div>
                    <div class="info">
                        <div class="title-block"><a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-be_quiet/blok_pitaniya_be_quiet_tfx_power_2_300_vt_art80056126/" class="title">БП TFX 300 Вт Be quiet TFX POWER 2 BN228</a>
                            <div class="product-rate">
                                <div class="art">Арт. 80056126</div>
                                <div class="sp-inline-rating sp-inline-rating-container-80056126"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-80056126" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>3 700 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-80056126"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=80056126" data-product-id="80056126" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80056126 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="80056126"><span class="pick-up"><b>Самовывоз</b> (послезавтра) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (послезавтра) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80111365" data-product-external-id="786219">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-raidmax/bp_atx_400_vt_raidmax_rx_400xt_art80111365/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80111365.jpg" class="lazy-img" alt="БП ATX 400 Вт Raidmax RX-400XT" title="БП ATX 400 Вт Raidmax RX-400XT" /></a>
                    </div>
                    <div class="info">
                        <div class="title-block"><a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-raidmax/bp_atx_400_vt_raidmax_rx_400xt_art80111365/" class="title">БП ATX 400 Вт Raidmax RX-400XT</a>
                            <div class="product-rate">
                                <div class="art">Арт. 80111365</div>
                                <div class="sp-inline-rating sp-inline-rating-container-80111365"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-80111365" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>1 520 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-80111365"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=80111365" data-product-id="80111365" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80111365 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="80111365"><span class="pick-up"><b>Самовывоз</b> (послезавтра) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (послезавтра) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80111382" data-product-external-id="786236">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-raidmax/bp_atx_500_vt_raidmax_rx_500xt_art80111382/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80111382.jpg" class="lazy-img" alt="БП ATX 500 Вт Raidmax RX-500XT" title="БП ATX 500 Вт Raidmax RX-500XT" /></a>
                    </div>
                    <div class="info">
                        <div class="title-block"><a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-raidmax/bp_atx_500_vt_raidmax_rx_500xt_art80111382/" class="title">БП ATX 500 Вт Raidmax RX-500XT</a>
                            <div class="product-rate">
                                <div class="art">Арт. 80111382</div>
                                <div class="sp-inline-rating sp-inline-rating-container-80111382"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-80111382" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>1 810 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-80111382"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=80111382" data-product-id="80111382" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80111382 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="80111382"><span class="pick-up"><b>Самовывоз</b> (послезавтра) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (послезавтра) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80175465" data-product-external-id="850175">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-aerocool/_art80175465/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80175465.jpg" class="lazy-img" alt="БП ATX 400 Вт Aerocool VX-400 PLUS" title="БП ATX 400 Вт Aerocool VX-400 PLUS" /></a>
                    </div>
                    <div class="info">
                        <div class="title-block"><a href="/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-aerocool/_art80175465/" class="title">БП ATX 400 Вт Aerocool VX-400 PLUS</a>
                            <div class="product-rate">
                                <div class="art">Арт. 80175465</div>
                                <div class="sp-inline-rating sp-inline-rating-container-80175465"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-80175465" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>1 900 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-80175465"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=80175465" data-product-id="80175465" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80175465 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="80175465"><span class="pick-up"><b>Самовывоз</b> (послезавтра) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (послезавтра) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80043496" data-product-external-id="718127">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-xilence/bp_atx_700_vt_xilence_xp700r7_xn054_art80043496/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80043496.jpg" class="lazy-img" alt="БП ATX 700 Вт Xilence XP700R7 XN054" title="БП ATX 700 Вт Xilence XP700R7 XN054" /></a>
                    </div>
                    <div class="info">
                        <div class="title-block"><a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-xilence/bp_atx_700_vt_xilence_xp700r7_xn054_art80043496/" class="title">БП ATX 700 Вт Xilence XP700R7 XN054</a>
                            <div class="product-rate">
                                <div class="art">Арт. 80043496</div>
                                <div class="sp-inline-rating sp-inline-rating-container-80043496"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-80043496" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>2 570 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-80043496"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=80043496" data-product-id="80043496" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80043496 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="80043496"><span class="pick-up"><b>Самовывоз</b> (послезавтра) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (послезавтра) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80033954" data-product-external-id="708341">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-be_quiet/bp_atx_1200_vt_be_quiet_dark_power_pro_11_bn255_art80033954/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80033954.jpg" class="lazy-img" alt="БП ATX 1200 Вт Be quiet Dark Power Pro 11 BN255" title="БП ATX 1200 Вт Be quiet Dark Power Pro 11 BN255" /></a>
                    </div>
                    <div class="info">
                        <div class="title-block"><a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-be_quiet/bp_atx_1200_vt_be_quiet_dark_power_pro_11_bn255_art80033954/" class="title">БП ATX 1200 Вт Be quiet Dark Power Pro 11 BN255</a>
                            <div class="product-rate">
                                <div class="art">Арт. 80033954</div>
                                <div class="sp-inline-rating sp-inline-rating-container-80033954"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-80033954" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>19 390 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-80033954"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=80033954" data-product-id="80033954" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80033954 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="80033954"><span class="pick-up"><b>Самовывоз</b> (послезавтра) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (послезавтра) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80018417" data-product-external-id="692387">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-be_quiet/bp_atx_750_vt_be_quiet_dark_power_pro_11_750w_bn252_art80018417/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80018417.jpg" class="lazy-img" alt="БП ATX 750 Вт Be quiet DARK POWER PRO 11 750W BN252" title="БП ATX 750 Вт Be quiet DARK POWER PRO 11 750W BN252" /></a>
                    </div>
                    <div class="info">
                        <div class="title-block"><a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-be_quiet/bp_atx_750_vt_be_quiet_dark_power_pro_11_750w_bn252_art80018417/" class="title">БП ATX 750 Вт Be quiet DARK POWER PRO 11 750W BN252</a>
                            <div class="product-rate">
                                <div class="art">Арт. 80018417</div>
                                <div class="sp-inline-rating sp-inline-rating-container-80018417"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-80018417" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>12 160 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-80018417"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=80018417" data-product-id="80018417" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80018417 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="80018417"><span class="pick-up"><b>Самовывоз</b> (послезавтра) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (послезавтра) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="80033957" data-product-external-id="708344">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-be_quiet/bp_atx_650_vt_be_quiet_dark_power_pro_11_bn251_art80033957/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/80033957.jpg" class="lazy-img" alt="БП ATX 650 Вт Be quiet Dark Power Pro 11 BN251" title="БП ATX 650 Вт Be quiet Dark Power Pro 11 BN251" /></a>
                    </div>
                    <div class="info">
                        <div class="title-block"><a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-be_quiet/bp_atx_650_vt_be_quiet_dark_power_pro_11_bn251_art80033957/" class="title">БП ATX 650 Вт Be quiet Dark Power Pro 11 BN251</a>
                            <div class="product-rate">
                                <div class="art">Арт. 80033957</div>
                                <div class="sp-inline-rating sp-inline-rating-container-80033957"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-80033957" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>11 340 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-80033957"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=80033957" data-product-id="80033957" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-80033957 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="80033957"><span class="pick-up"><b>Самовывоз</b> (послезавтра) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (послезавтра) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="8676689" data-product-external-id="395115">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-crown/blok_pitaniya_zalman_art8676689/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/8676689.jpg" class="lazy-img" alt="БП ATX 650 Вт Crown CM-PS650W" title="БП ATX 650 Вт Crown CM-PS650W" /></a>
                    </div>
                    <ul class="stickers">
                        <li class="x-sale"><span>-17%</span></li>
                    </ul>
                    <div class="info">
                        <div class="title-block"><a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-crown/blok_pitaniya_zalman_art8676689/" class="title">БП ATX 650 Вт Crown CM-PS650W</a>
                            <div class="product-rate">
                                <div class="art">Арт. 8676689</div>
                                <div class="sp-inline-rating sp-inline-rating-container-8676689"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-8676689" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>1 870 р.</strong><strong class="old-price">2 260 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-8676689"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=8676689" data-product-id="8676689" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-8676689 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="8676689"><span class="pick-up"><b>Самовывоз</b> (послезавтра) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (послезавтра) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="8809898" data-product-external-id="479803">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-exegate/bp_atx_500_vt_exegate_atx_xp500_ex219463rus_art8809898/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/8809898.jpg" class="lazy-img" alt="БП ATX 500 Вт Exegate ATX-XP500 EX219463RUS" title="БП ATX 500 Вт Exegate ATX-XP500 EX219463RUS" /></a>
                    </div>
                    <div class="info">
                        <div class="title-block"><a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-exegate/bp_atx_500_vt_exegate_atx_xp500_ex219463rus_art8809898/" class="title">БП ATX 500 Вт Exegate ATX-XP500 EX219463RUS</a>
                            <div class="product-rate">
                                <div class="art">Арт. 8809898</div>
                                <div class="sp-inline-rating sp-inline-rating-container-8809898"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-8809898" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>1 270 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-8809898"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=8809898" data-product-id="8809898" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-8809898 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="8809898"><span class="pick-up"><b>Самовывоз</b> (послезавтра) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (послезавтра) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="8809900" data-product-external-id="479805">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-exegate/bp_atx_500_vt_exegate_atx_500npxe_pfc_ex221638rus_art8809900/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/8809900.jpg" class="lazy-img" alt="БП ATX 500 Вт Exegate ATX-500NPXE +PFC EX221638RUS" title="БП ATX 500 Вт Exegate ATX-500NPXE +PFC EX221638RUS" /></a>
                    </div>
                    <div class="info">
                        <div class="title-block"><a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-exegate/bp_atx_500_vt_exegate_atx_500npxe_pfc_ex221638rus_art8809900/" class="title">БП ATX 500 Вт Exegate ATX-500NPXE +PFC EX221638RUS</a>
                            <div class="product-rate">
                                <div class="art">Арт. 8809900</div>
                                <div class="sp-inline-rating sp-inline-rating-container-8809900"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-8809900" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>1 610 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-8809900"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=8809900" data-product-id="8809900" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-8809900 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="8809900"><span class="pick-up"><b>Самовывоз</b> (послезавтра) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (послезавтра) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="8586580" data-product-external-id="232231">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-accord/blok_pitaniya_art8586580/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/8586580.jpg" class="lazy-img" alt="БП ATX 450 Вт Accord ACC-450-12" title="БП ATX 450 Вт Accord ACC-450-12" /></a>
                    </div>
                    <div class="info">
                        <div class="title-block"><a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-accord/blok_pitaniya_art8586580/" class="title">БП ATX 450 Вт Accord ACC-450-12</a>
                            <div class="product-rate">
                                <div class="art">Арт. 8586580</div>
                                <div class="sp-inline-rating sp-inline-rating-container-8586580"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-8586580" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>1 210 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-8586580"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=8586580" data-product-id="8586580" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-8586580 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="8586580"><span class="pick-up"><b>Самовывоз</b> (послезавтра) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (послезавтра) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="8586615" data-product-external-id="232055">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-aerocool/blok_pitaniya_art8586615/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/8586615.jpg" class="lazy-img" alt="БП ATX 400 Вт Aerocool VX-400" title="БП ATX 400 Вт Aerocool VX-400" /></a>
                    </div>
                    <ul class="stickers">
                        <li class="x-sale"><span>-10%</span></li>
                    </ul>
                    <div class="info">
                        <div class="title-block"><a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-aerocool/blok_pitaniya_art8586615/" class="title">БП ATX 400 Вт Aerocool VX-400</a>
                            <div class="product-rate">
                                <div class="art">Арт. 8586615</div>
                                <div class="sp-inline-rating sp-inline-rating-container-8586615"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-8586615" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>1 730 р.</strong><strong class="old-price">1 930 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-8586615"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=8586615" data-product-id="8586615" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-8586615 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="8586615"><span class="pick-up"><b>Самовывоз</b> (послезавтра) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (послезавтра) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="8548392" data-product-external-id="248276">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-hipro/blok_pitaniya_400_hipro_hp_e4009f5wp_oem_art8548392/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/8548392.jpg" class="lazy-img" alt="БП ATX 400 Вт Hipro HPE-400W" title="БП ATX 400 Вт Hipro HPE-400W" /></a>
                    </div>
                    <div class="info">
                        <div class="title-block"><a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-hipro/blok_pitaniya_400_hipro_hp_e4009f5wp_oem_art8548392/" class="title">БП ATX 400 Вт Hipro HPE-400W</a>
                            <div class="product-rate">
                                <div class="art">Арт. 8548392</div>
                                <div class="sp-inline-rating sp-inline-rating-container-8548392"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-8548392" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>1 660 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-8548392"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=8548392" data-product-id="8548392" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-8548392 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="8548392"><span class="pick-up"><b>Самовывоз</b> (послезавтра) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (послезавтра) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="8586613" data-product-external-id="231221">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-accord/blok_pitaniya_art8586613/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/8586613.jpg" class="lazy-img" alt="БП ATX 350 Вт Accord ACC-350-12" title="БП ATX 350 Вт Accord ACC-350-12" /></a>
                    </div>
                    <div class="info">
                        <div class="title-block"><a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-accord/blok_pitaniya_art8586613/" class="title">БП ATX 350 Вт Accord ACC-350-12</a>
                            <div class="product-rate">
                                <div class="art">Арт. 8586613</div>
                                <div class="sp-inline-rating sp-inline-rating-container-8586613"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-8586613" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>760 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-8586613"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=8586613" data-product-id="8586613" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-8586613 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="8586613"><span class="pick-up"><b>Самовывоз</b> (послезавтра) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (послезавтра) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="8400764" data-product-external-id="214230">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-fsp/blok_pitaniya_450_fsp_q_dion_qd450_retail_art8400764/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/8400764.jpg" class="lazy-img" alt="БП ATX 450 Вт FSP Q-Dion QD-450 (9PA400A401)" title="БП ATX 450 Вт FSP Q-Dion QD-450 (9PA400A401)" /></a>
                    </div>
                    <ul class="stickers">
                        <li class="x-sale"><span>-8%</span></li>
                    </ul>
                    <div class="info">
                        <div class="title-block"><a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-fsp/blok_pitaniya_450_fsp_q_dion_qd450_retail_art8400764/" class="title">БП ATX 450 Вт FSP Q-Dion QD-450 (9PA400A401)</a>
                            <div class="product-rate">
                                <div class="art">Арт. 8400764</div>
                                <div class="sp-inline-rating sp-inline-rating-container-8400764"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-8400764" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>1 850 р.</strong><strong class="old-price">2 030 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-8400764"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=8400764" data-product-id="8400764" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-8400764 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="8400764"><span class="pick-up"><b>Самовывоз</b> (послезавтра) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (послезавтра) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="product-item cell col-lg-25 col-md-25 col-sm-50 col-xs-100" data-product-id="8401883" data-product-external-id="214505">
                    <div class="hover"></div>
                    <div class="image-container">
                        <a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-fsp/blok_pitaniya_500_fsp_q_dion_qd500_retail_art8401883/"><img src="/i/blank.png" data-src="//www.123.ru/l_pics/8401883.jpg" class="lazy-img" alt="БП ATX 500 Вт FSP Q-Dion QD500" title="БП ATX 500 Вт FSP Q-Dion QD500" /></a>
                    </div>
                    <div class="info">
                        <div class="title-block"><a href="/accessories/komplektuyuschie_dlya_pk/bloki-pitaniya/brand-fsp/blok_pitaniya_500_fsp_q_dion_qd500_retail_art8401883/" class="title">БП ATX 500 Вт FSP Q-Dion QD500</a>
                            <div class="product-rate">
                                <div class="art">Арт. 8401883</div>
                                <div class="sp-inline-rating sp-inline-rating-container-8401883"></div>
                            </div>
                        </div>
                        <div id="sp-good-review-teaser-container-8401883" class="sp-product-inline-rating-widget"><div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-half"></div></div><div class="rating-count">14</div></div>
                    </div>
                    <div class="price-block">
                        <div class="price"><strong>1 900 р.</strong></div>
                        <div class="sp-inline-rating sp-inline-rating-container-8401883"></div>
                        <div class="buy-block">
                            <noindex><a href="#" data-link="/basket/add/?id=8401883" data-product-id="8401883" class="btn-primary pi-buy add-to-cart animate-all js-buyBtn add-to-cart-8401883 no-popup">в корзину</a></noindex>
                            <div class="board"></div>
                        </div>
                        <div class="delivery-block" data-product-id="8401883"><span class="pick-up"><b>Самовывоз</b> (послезавтра) – <b>бесплатно</b></span><span class="delivery"><b>Доставка</b> (послезавтра) – <b>от 299 р.</b></span></div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="pages-navigation">
                    <div class="show-more" data-link="/compatibles/8965143/blok_pitaniya_1563/page-2/"><a href="#">Показать еще <span>17</span> товаров</a></div>
                    <ul class="pages-list">
                        <li class="current"><span>1</span></li>
                        <li><a href="/compatibles/8965143/blok_pitaniya_1563/page-2/" data-pager="2" data-items-count="17">2</a></li>
                        <li class="arrow right">
                            <a href="/compatibles/8965143/blok_pitaniya_1563/page-2/" data-pager="2" data-items-count="17"></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</section>
<div class="window box-shadow" id="filter-window"><a href="#" data-action="close" class="open-xs-filter ">Фильтр и сортировка <em></em></a>
    <div class="appendData"></div>
</div>
<div class="clearfix"></div>