var CART_URL = "/!ajax/cart.php";

function updateOrderInsuranceAmounts() {
	if (0 == $('#body.order-page').length) return;

	$('.cart-item').each(function() {
		var _ = $(this);
		var quantity = parseInt(_.find('input[name="count"]').val());
		if (quantity) {
			var minAmount = 10000000;
			_.find('.valuable-item').each(function() {
				var li = $(this);
				var price = parseInt(li.find('input[type="checkbox"]').attr('data-price'));
				var amount = price * quantity;
				li.find('.c-blue').html(format_price(amount) + ' <span class="rub">a</span>');

				if (minAmount > amount) {
					minAmount = amount;
				}
			});
			console.log(minAmount);
			if (10000000 > minAmount) {
				_.find('.more-insurance span').text(format_price(minAmount));
			}
		}
	});
}
function updateOrderTotalAmount() {
	if (0 == $('#body.order-page').length) return;

	var totalBlock = $('.cart-total-wrapper');

	var totalAmount = 0;

	var productsAmount = 0;
	var productsCount = 0;
	var productsOriginalAmount = 0;
	var discountAmount = 0,
	   _bonus = 0;
	if (typeof(window.currentDiscount)!="undefined") discountAmount = -window.currentDiscount;
	$('.cart-item').each(function() {
		var _ = $(this);
		var quantity = parseInt(_.find('input[name="count"]').val());
		if (_.attr('data-price') && quantity) {
			var price = parseInt(_.attr('data-price'));
    		var _b = parseInt(_.attr("data-bonus"));
			var amount = price * quantity;
            _bonus += _b * quantity;
			productsCount += quantity;
			productsAmount += amount;
			_.find('.valuable-item input[type="checkbox"]:checked').each(function() {
				productsAmount += parseInt($(this).attr('data-price')) * quantity;
			});
			
			if (_.attr('data-promo-price')) {
				var promoPrice = parseInt(_.attr('data-promo-price'));
				var promoAmount = promoPrice * quantity;
				discountAmount += (promoAmount - amount);
			}

			productsOriginalAmount += parseInt(_.attr('data-original-price') * quantity);
		}
	});

	totalAmount += productsAmount;
	
	$(".-root-fix.promo-total .amounts .total-count strong").text(productsCount);
	$(".-root-fix.promo-total .amounts .discount strong").text(Math.abs(discountAmount)).parents("li").eq(0).toggle(discountAmount!=0);
	$(".-root-fix.promo-total .amounts .total strong").text(productsAmount-Math.abs(discountAmount));
	$(".-root-fix.promo-total .t-footer .bonuses b").text("+"+_bonus).parent().toggle(_bonus>0);

	// receipt
	var deliveryPrice = 0;
	if ('pickup' == $('input[name="receipt-type"]').val()) {
		deliveryPrice = parseInt($('.map-sidebar .btn-green').attr('data-price'));
	} else {
	   if ($('input[name="delivery-type"]:checked').length==0) deliveryPrice = 0;
	   else
       deliveryPrice = parseInt($('input[name="delivery-type"]:checked').attr('data-price'));
	}
	if (0 <= deliveryPrice) {
		totalAmount += deliveryPrice;
	}

	if (0 < deliveryPrice) {
		totalBlock.find('.val.delivery-line').html(format_price(deliveryPrice) + ' <span class="rub">a</span>');
		totalBlock.find('.delivery-line').css('display', 'block');
	} else {
		totalBlock.find('.val.delivery-line').html('0 <span class="rub">a</span>');
		totalBlock.find('.delivery-line').css('display', 'block');
	}
	// END receipt

	if (true || (0 < discountAmount) || (0 < deliveryPrice)) {
		totalBlock.find('.order-line').css('display', 'block');
		totalBlock.find('.val.order-line').html(format_price(productsOriginalAmount) + ' <span class="rub">a</span>');

		if (0 < discountAmount) {
			totalBlock.find('.discount-line').css('display', 'block');
			totalBlock.find('.discount-line.val').html(format_price(discountAmount) + ' <span class="rub">a</span>');
		}
	} else {
		totalBlock.find('.order-line').css('display', 'none');
		totalBlock.find('.discount-line').css('display', 'none');
	}

	$('.cart-total-wrapper .tot').html(format_price(totalAmount) + ' <span class="rub">a</span>');
	$('.cart-summ .price-lg').html(format_price(totalAmount) + ' <span class="rub">a</span>');
	$('#form-order input[name="total-amount"]').val(totalAmount);
}

function initPayer() {
	if (0 == $('#body.order-page').length) return;

	var payerType = $('.cart-list-tabs a.active').attr('data-payer-type');
	$('input[name="payer-type"]').val(payerType);
}

function filterPaymentTypes() {
	if (0 == $('#body.order-page').length) return;
    return;
	var userTypes = $('.user-info .tabs-list .active').attr('data-payment-types').split(',');
	var receiptTypes = $('input[name="receipt-payment-types"]').val().split(',');
	var types = intersect(userTypes, receiptTypes);
	if (0 == types.length) {
		$('.payment-info').hide();
	} else {
		$('.payment-info').show();
	}
	$('.cart-payment-types li').hide();
	$.each(types, function() {
		var type = this;
		$('.cart-payment-types input[value="' + type + '"]').parent().show();
	});
	$('.cart-payment-types li:visible').first().trigger(clickHandler);
}

window.orderAgentArrivalsLoading = false;
function updateProductsAvailability() {
	if (0 == $('#body.order-page').length) return;

	var type = $('input[name="receipt-type"]').val();
	var pickupType = $('input[name="pickup-type"]').val();
	var pointId = $('input[name="pickup-id"]').val();
	var deliveryId = $('input[name="delivery-service-id"]').val();

	var agent = '';
	var point = '';
	if ('pickup' == type) {
		if ('own' == pickupType) {
			agent = 'zakazberry';
			if (pointId) {
				point = pointId;
			}
		} else {
			agent = pickupType;
		}
	} else if ('delivery' == type) {
		agent = 'delivery';
		if (('own' == pickupType) && deliveryId) {
			point = deliveryId;
		} else {
			agent = pickupType;
		}
	}

	if (false == window.orderAgentArrivalsLoading) {
		window.orderAgentArrivalsLoading = true;
		$('.product-availability').removeClass('hidden').removeClass('loading').removeClass('red').removeClass('green').addClass('loading').html('');
		$.ajax({
			url: '/ordering/agentarrivals/',
			method: 'get',
			data: { agent: agent, point: point },
			dataType: 'json',
			success: function(data) {
				if (data.success && (0 < data.items.length)) {
					for (var i = 0; i < data.items.length; i++) {
						var item = data.items[i];
						var line = $('.product-availability-' + item.id);
						line.addClass(item.status);
						line.html(item.statusTitle);
					}
				}
			},
			complete: function() {
				$('.product-availability').removeClass('loading');
				window.orderAgentArrivalsLoading = false;
			}
		});
	}

	//console.log('-------------');
	//console.log('agent', agent);
	//console.log('point', point);
	//console.log('-------------');


	//console.log('type', type);
	//console.log('pickup-type', pickupType);
	//console.log('point-id', pointId);
	//console.log('delivery-id', deliveryId);
}

$(function(){

    rootFixInit();
    checksInit();

    $("li.-reset-order").on("click", function(ev) {
        ev.preventDefault();
        $("#order-steps").css({display:"none"});
        var _st = $(".auth-block");
            if (!_st.length) _st = $(".sections-joiner, .-mob-total");
        $("section.step-shopcart, h1").fadeIn(250, function() {
            _st.fadeOut(250);
            _st.removeClass('visible');
            $('html,body').stop().animate({ scrollTop: 0 }, 150);
        });
        window.onr();
        return false;
        
    });
    
    $("#order-steps li:not(.-reset-order)").on("click", function(e) {
        if ($(this).hasClass("disabled")) return false;
        var e = $($(this).data("section"));
        $(this).parent().find(".selected").removeClass('selected');
        $(this).addClass('selected');
        if ($(window).width()<760) {
            $("#order-steps").stop().animate({scrollLeft: $(this).position().left }, 100);
        }
        $('html,body').stop().animate({ scrollTop: e.offset().top - 50 }, 150);
    });


	var CountBox = {
		timerTimeout : false,
		timerInterval : false,
		count_inc : function ($input){
			var options = $input.data();
			var current = parseInt($input.val());
			if(current < options.max){
				current++;
			} else {
				current = options.max;
			}
			$input.val(current);
			$input.trigger('change');
		},
		count_dec : function ($input){
			var options = $input.data();
			var current = parseInt($input.val());
			if(current > options.min){
				current--;
			} else {
				current = options.min;
			}
			$input.val(current);
			$input.trigger('change');
		},
		init : function(){

			$(document).on('mousedown', '.countbox a', function(e){
				e.preventDefault();
				var $a = $(this);
				var $input = $a.closest('.countbox').find('input');
				if($a.hasClass('plus') && e.which === 1){
					CountBox.count_inc($input);
				}
				if($a.hasClass('minus') && e.which === 1){
					CountBox.count_dec($input);
				}
				clearTimeout(CountBox.timerTimeout);
				clearInterval(CountBox.timerInterval);
				// при удержании запускаем накрутку
				if($input.data('hold') && e.which === 1){
					CountBox.timerTimeout = setTimeout(function(){
						if($a.hasClass('plus')){
							CountBox.timerInterval = setInterval(function(){
								CountBox.count_inc($input);
							}, 100);
						} else {
							CountBox.timerInterval = setInterval(function(){
								CountBox.count_dec($input);
							}, 100);
						}
					}, 500);
				}

			}).on('mouseup', '.countbox a', function(e){
				e.preventDefault();

				clearTimeout(CountBox.timerTimeout);
				clearInterval(CountBox.timerInterval);

				var _ = $(this);
				var productId = _.parent().attr('data-product-id');
				var quantity = parseInt(_.parent().find('input[name="count"]').val());
				if (productId && (0 <= quantity)) {
					updateBasketQuantity(productId, quantity);
					updateOrderInsuranceAmounts();
					updateOrderTotalAmount();
				}
			}).on('keydown', '.countbox input', function(e){
				if( $.inArray(e.keyCode, [8,37,39,46,48,49,50,51,52,53,54,55,56,57]) < 0 ){
					e.preventDefault();
					return false;
				}
			}).on('keyup', '.countbox input', function(e){
				var _ = $(this);
				var productId = _.parent().attr('data-product-id');
				var quantity = parseInt(_.val());
				if (productId && (0 <= quantity)) {
					updateBasketQuantity(productId, quantity);
					updateOrderInsuranceAmounts();
					updateOrderTotalAmount();
				}
			}).on('blur', '.countbox input', function(){
				var $input = $(this);
				var options = $input.data();
				var current = parseInt($input.val());
				if(current > 0){
					(current > options.max) && (current = options.max);
					(current < options.min) && (current = options.min);
					$input.val(current);
				} else {
					current = 1;
				}
				$input.val(current);

				var _ = $(this);
				var productId = _.parent().attr('data-product-id');
				var quantity = parseInt(_.val());
				if (productId && (0 <= quantity)) {
					updateBasketQuantity(productId, quantity);
					updateOrderInsuranceAmounts();
					updateOrderTotalAmount();
				}
			});

		}
	};

	function updateBasketQuantity(productId, quantity) {
		$.ajax({
			method: 'get',
			url: '/basket/quantity/',
			data: {
				product: productId,
				quantity: quantity
			},
			dataType: 'json',
			success: function(data) {
				if (data.success) {
					header_recalculate_hint(data.count, data.amount);

					var product = data.product;
					if (product) {
						if (exists('.cart-item-' + productId)) {
							var line = $('.cart-item-' + productId);
							var price = parseInt(line.attr('data-price'));
							var amount = quantity * price;
							line.find('.price-res').html(format_price(amount) + ' <span class="rub">a</span>');
							if (line.attr('data-promo-price') && (0 <= parseInt(line.attr('data-promo-price')))) {
								var promoPrice = parseInt(line.attr('data-promo-price'));
								var promoAmount = promoPrice * quantity;
								line.find('.price-old').html(format_price(promoAmount) + ' <span class="rub">a</span>');

								var discountAmount = amount - promoAmount;
								line.find('.price-sale .c-blue').html(format_price(discountAmount) + ' <span class="rub">a</span>');
							}
						}
						if ('add' == data.mode) {
							GaEcommerceHelper.addProductToCart(
								product.id,
								product.name,
								product.category,
								product.price,
								product.quantity
							);
							if (product.rrId) {
								RRHelper.addToBasket([product.rrId]);
							}
							FlocktoryHelper.addToBasket(product.id, product.price, product.quantity);
						} else {
							GaEcommerceHelper.removeProductFromCart(
								product.id,
								product.name,
								product.category,
								product.price,
								product.quantity
							);
							FlocktoryHelper.removeFromBasket(product.id, product.quantity);
						}
					}
				}
			}
		});
	}

	CountBox.init();

	$(document).on(clickHandler, '.__select_point', function(e) {
		e.preventDefault();
		//window.cartMap.balloon.close();

		var _ = $(this);

		$('.map-sidebar li .btn-green').removeClass('btn-green').addClass('btn-primary').text('Заберу здесь');
		var listItem = $('.map-sidebar a[data-pickup-id="' + _.attr('data-pickup-id') + '"]');
		if (0 < listItem.length) {
			listItem.addClass('btn-green').removeClass('btn-primary').text('Выбрано');
		}

		$('.__select_point.btn-green').removeClass('btn-green').addClass('btn-primary').text('заберу здесь');
		_.addClass('btn-green').removeClass('btn-primary').text('выбрано');

		$('input[name="receipt-type"]').val('pickup');
		$('input[name="receipt-payment-types"]').val(_.attr('data-payment-types'));
		$('input[name="pickup-type"]').val(_.attr('data-pickup-type'));
		$('input[name="pickup-id"]').val(_.attr('data-pickup-id'));

		$('html, body').animate({scrollTop: $('.container.payment-info').offset().top + 'px'}, 200);
		filterPaymentTypes();
		updateProductsAvailability();
		updateOrderTotalAmount();
	}).on('cart-rebuild-map', function(e, data) {
		var pickupTypes = window.pickupTypes;

		if (data.key && (pickupTypes[data.key] || ('all' == data.key))) {
			var map = window.cartMap;
			map.geoObjects.removeAll();
			if (map.lastRoute) {
				map.geoObjects.remove(map.lastRoute);
			}

			var datakeys = {
				own: '/img/map-point.png',
				zakazberry: '/i/carriers/pointer-zakazberry.png',
				sdek: '/img/map-point-cdek.png',
				pickpoint: '/img/map-point-pickpoint.png',
				iml: '/img/map-point-iml.png',
				euroset: '/img/map-point-euroset.png',
				svyaznoy: '/img/map-point-svyaznoy.png',
				boxberry: '/img/map-point-boxberry.png',
				kh: '/i/nh-pointmap.png'
			};
			var datanames = {
				own: 'ПВЗ 123.ru',
				zakazberry: 'ZakazBerry',
				sdek: 'СДЭК',
				pickpoint: 'PickPoint',
				iml: 'IML',
				euroset: 'Евросеть',
				svyaznoy: 'Связной',
				boxberry: 'Boxberry',
				kh: 'Ноу-Хау'
			};

			if (isImlCity()) {
				datakeys.iml = datakeys.own;
				datanames.iml = '123/IML';
			}
			if (isSdekCity()) {
				datakeys.sdek = datakeys.own;
				datanames.sdek = '123/СДЭК';
			}

			function showPlacemarks(pickupType) {
				var map = window.cartMap;
				
				$.each(pickupType.points, function() {
					var point = this;
					var typeUid = pickupType.uid;
					if (('euroset' == typeUid) && point.isNumericCode) {
						typeUid = 'svyaznoy';
					}

					try {
						var paymentTypes = [];
						if (point.paymentTypes && (0 < point.paymentTypes.length)) {
							paymentTypes = point.paymentTypes;
						} else {
							paymentTypes = pickupType.paymentTypes;
						}
						var iconClass = typeUid;
						if (isImlCity() || isSdekCity()) {
							iconClass = 'own';
						}
						var placemark = new ymaps.Placemark([point.lat, point.lon], {
							pickupType: point.agent,
							showModalButton : true,
							modalText: str_replace('<br />', "\n", point.address),
							pid : point.id,
							paymentTypes: paymentTypes.join(','),
							price: point.price,
							icon: datakeys[typeUid],
							del_name: datanames[typeUid],
							icon_class: iconClass,
							b_addr: point.address,
							b_addr_parts: point.addressParts,
							b_contact_link: point.contactLink,
							b_shed: point.workhours,
							b_del: point.pickupTitle,
							b_price: (0 == point.price ? 'бесплатно' : point.price + ' руб.')
						}, {
							iconLayout : 'default#image',
							hasBalloon : true,
							hasHint : false,
							hideIconOnBalloonOpen : false,
							iconImageHref: datakeys[typeUid],
							iconImageOffset: [0, 0],
							iconImageSize : [46, 54],
							iconOffset : [-23, -50],
							balloonLayout: window.CartModalFree2,
							balloonPanelMaxMapArea: 0,
							balloonAutoPan: false,
						});
						placemark.events.add(clickHandler, function(){
							if($(window).width() < 400){
								console.log('mobile-version');
							}
							var coord = placemark.geometry.getCoordinates();
							map.setCenter([
								coord[0],
								coord[1]
							]);
							var position = map.getGlobalPixelCenter();
							map.setGlobalPixelCenter([
								position[0],
								position[1] - 70
							]);
						});
						map.geoObjects.add(placemark);
						/*placemark.events.add(clickHandler, function () {
							var coord = placemark.geometry.getCoordinates();
							window.cartMap.panTo([coord[0]+0.02,coord[1]], {
								delay: 0
							}); 
						});*/
					} catch (e) { console.log('error', e); }
				});
			}

			if ('all' == data.key) {
				for (uid in pickupTypes) {
					var pickupType = pickupTypes[uid];
					showPlacemarks(pickupType);
				}
			} else {
				showPlacemarks(pickupTypes[data.key]);

				// route
				try {
					if (false && $('#selfdelivery-radio li.own input[type="radio"]').prop('checked')) {
						var pointA = [55.782119, 37.705301],
							pointB = [55.783870, 37.708530],
							multiRoute = new ymaps.multiRouter.MultiRoute({
								referencePoints: [
									pointA,
									pointB
								],
								params: {
									routingMode: 'pedestrian'
								}
							}, {
								boundsAutoApply: true
							});
						map.lastRoute = multiRoute;
						map.geoObjects.add(multiRoute);
					} else if ($('#selfdelivery-radio li.own input[type="radio"]').prop('checked')) {
						window.cartMap.setCenter(['55.730901', '37.723888'], 11);
					} else if ($('#selfdelivery-radio li.zakazberry input[type="radio"]').prop('checked')) {
						console.log('map center', 'zakazberry');
						window.cartMap.setCenter(['55.730901', '37.723888'], 10);
					}
				} catch (e) {
					console.log('route error');
					console.log(e);
				}
				// END route
			}

			if (!$('#selfdelivery-radio li.own input[type="radio"]').prop('checked') && !$('#selfdelivery-radio li.zakazberry input[type="radio"]').prop('checked')) {
				var container = $('#selfdeliveryMap');
				window.cartMap.setCenter(
					[container.attr('data-center-longitude'), container.attr('data-center-latitude')],
					12
				);
				console.log('map center', 'common', [container.attr('data-center-longitude'), container.attr('data-center-latitude')]);
			}
		}
	}).on(clickHandler, '.baloon-close', function(e){
		e.preventDefault();
		window.cartMap.balloon.close();
		
	});

	$(document).on('click','.select-view',function(e){
		e.preventDefault();
		var _ = $(this);
		var target = _.attr('href');
		
		$('.select-view.active').toggleClass('active');
		_.toggleClass('active');
		
		if (target == '#selfdeliveryMap'){
			 $('.map-container').addClass('showed-map');
			 $('#map-suggest').css('visibility', 'visible');
		}else{
			 $('.map-container').removeClass('showed-map');
			 $('#map-suggest').css('visibility', 'hidden');
		}
		
		$('.view-viewport').hide();
		$(target).show();
		
		$('#selfdelivery-radio li.checked').click();
		
		return false;
  });

	$('.cart-items').on('mouseenter', '.cart-item', function(){
		$(this).find('.onhover').stop(1,1).fadeIn(100);
	}).on('mouseleave', '.cart-item', function(){
		$(this).find('.onhover').stop(1,1).fadeOut(100);
	});
	
	$('.radio-group').on(clickHandler, 'li', function(e){
		var _ = $(this);
		//e.stopPropagation();
		if( ! _.hasClass('checked')){
			var $input = _.children('input:radio');
			var $spoiler = _.children('.radio-spoiler');
			var $otherRadios = false;
			if($input.attr('name')){
				$otherRadios = $('[name="'+$input.attr('name')+'"]');
			} else if($input.data('name')){
				$otherRadios = $('[data-name="'+$input.data('name')+'"]');
			} else {
				$otherRadios = _.closest('.radio-group').children('li').children('input:radio');
			}
			
			_.closest('.radio-group').find('.radio-spoiler').slideUp(250);
			_.closest('.bordered').trigger(clickHandler);
			if($spoiler.length){
				$spoiler.slideDown(250);
			}
			$otherRadios.parent('.checked').removeClass('checked');
			$otherRadios.prop('checked', false);
			
			_.addClass('checked');
			
			$input.prop('checked', true);
			// set radio value in linked input
			if($input.data('name')){
				$('[name="'+$input.data('name')+'"]').val($input.val());
			}

			if ('delivery-time-id' == $input.attr('name')) {
				$(document).trigger('interval-changed', $input);
			}
			if ('delivery-type' == $input.attr('name')) {
				$(document).trigger('delivery-type-changed', $input);
			}
		}
	});

	$('.radio-group input:checked').each(function(){
		var _ = $(this);
		_.closest('.radio-group').children('li.checked').removeClass('checked');
		_.closest('li').trigger(clickHandler);

		// set radio value in linked input
		if(_.data('name')){
			$('[name="'+_.data('name')+'"]').val(_.val());
		}
	});

	// map listing
	var pickupTypes = window.pickupTypes;

	function getPickupTypeListingHtml(uid, pickupType) {
		var html = '';
		for (pointUid in pickupType.points) {
			var point = pickupType.points[pointUid];
			var className = uid;
			if (('euroset' == uid) && point.isNumericCode) {
				className = 'svyaznoy';
			}
			if (('iml' == className) && isImlCity()) {
				className = 'own';
			}
			if (('sdek' == className) && isSdekCity()) {
				className = 'own';
			}
			html += '<li class="' + className + '">';

			var priceTitle = format_price(point.price) + '&nbsp;<span class="rub">a</span>';
			if (0 == point.price) {
				priceTitle = 'бесплатно';
			}
			html += '<div class="row">';
			html += '<div class="col-sm-4 mls-addr">' + point.title + (point.workhours ? '<br />время работы: ' + point.workhours : '') + '</div>';

			if (0 <= point.pickupTitle.indexOf('Дата поступления')) {
				html += '<div class="col-sm-3 pickup">Дата поступления: <span class="c-blue">' + point.pickupTitle.replace('Дата поступления ', '') + '</span></div>';
			} else {
				html += '<div class="col-sm-3">Можно забрать: <span class="c-blue">' + point.pickupTitle + '</span></div>';
			}

			html += '<div class="col-sm-2"><b>' + priceTitle + '</b></div>';
			html += '<div class="col-sm-3"><a href="#" class="btn btn-primary" data-price="' + point.price + '" data-pickup-type="' + point.agent + '" data-pickup-id="' + point.id + '" data-payment-types="' + pickupType.paymentTypes.join(',') + '" data-pickup-datetime="' + pickupType.pickupDatetimeTimestamp + '">Заберу здесь</a></div>';
			html += '</div>';
			html += '</li>';
		}
		return html;
	}

	var html = '';
	html += '<ul id="show-all" class="clearstyle">';
	for (uid in pickupTypes) {
		var pickupType = pickupTypes[uid];
		html += getPickupTypeListingHtml(uid, pickupType);
	}
	html += '</ul>';
	for (uid in pickupTypes) {
		var pickupType = pickupTypes[uid];
		html += '<ul id="show-' + uid + '" class="clearstyle">';
		html += getPickupTypeListingHtml(uid, pickupType);
		html += '</ul>';
	}
	$('.map-container .nano-content').html(html);
	initNano();
	
	$('.map-container .nano-content ul').hide();
	if ($('#selfdelivery-radio li.checked').attr('data-key')) {
		var ul = $('#show-' + $('#selfdelivery-radio li.checked').attr('data-key'));
		if (ul.length) {
			ul.show();
			var minPickupDatetime = 0;
			var minPickupPoint = null;
			ul.find('a.btn-primary').each(function() {
				var btn = $(this);
				var pickupDatetime = btn.attr('data-pickup-datetime');
				if ((0 == minPickupDatetime) || (pickupDatetime < minPickupDatetime)) {
					minPickupDatetime = pickupDatetime;
					minPickupPoint = btn;
				}
			});
			if (minPickupPoint) {
				minPickupPoint.trigger(clickHandler);
			}
		}
	} else {
		var ul = $('#show-all');
		if (ul.length) {
			ul.show();
			ul.find('a.btn-primary:first').trigger(clickHandler);
		}
	}
	// END map listing

	if (window.ymaps && $('#selfdeliveryMap').length) {
		ymaps.ready(function() {
			var container = $('#selfdeliveryMap');
			var behaviors = ['dblClickZoom', 'rightMouseButtonMagnifier', 'multiTouch'];
			if (!isMobileDevice()) {
				behaviors.push('drag');
			}
			window.cartMap = new ymaps.Map("selfdeliveryMap", {
				center: [container.attr('data-center-longitude'), container.attr('data-center-latitude')],
				zoom: 12,
				behaviors: behaviors
			});
			var suggestView = new ymaps.SuggestView('map-suggest');
			suggestView.events.add('select', function (event) {
				var item = event.get('item');
				ymaps.geocode(item.value, {
				  results: 1
			 }).then(function (res) {
						// Выбираем первый результат геокодирования.
						var firstGeoObject = res.geoObjects.get(0),
							 // Координаты геообъекта.
							 coords = firstGeoObject.geometry.getCoordinates(),
							 // Область видимости геообъекта.
							 bounds = firstGeoObject.properties.get('boundedBy');

						firstGeoObject.options.set('preset', 'islands#darkBlueDotIconWithCaption');
						// Получаем строку с адресом и выводим в иконке геообъекта.
						firstGeoObject.properties.set('iconCaption', firstGeoObject.getAddressLine());

						// Добавляем первый найденный геообъект на карту.
						window.cartMap.geoObjects.add(firstGeoObject);
						// Масштабируем карту на область видимости геообъекта.
						window.cartMap.setBounds(bounds, {
							 // Проверяем наличие тайлов на данном масштабе.
							 checkZoomRange: true
						});
				  });
			 });
			
			window.CartModalFree = ymaps.templateLayoutFactory.createClass(
				[
					'<div class="map-balloon">',
						'{{ properties.modalText }} &ndash; <br><b>{{ properties.priceTitle }}</b>',
						'{% if properties.showModalButton %}',
							'<a href="#" class="btn btn-primary __select_point" data-price="{{ properties.price }}" data-pickup-type="{{ properties.pickupType }}" data-pickup-id="{{ properties.pid }}" data-payment-types="{{ properties.paymentTypes }}">заберу здесь</a>',
						'{% endif %}',
					'</div>'
				].join('')
			);

			window.CartModalFree2 = ymaps.templateLayoutFactory.createClass(
				[
					'<div class="map-balloon">',
                        '<a href="#" class="baloon-close" data-action="close"><svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 12 12"><svg xmlns:xlink="http://www.w3.org/1999/xlink" height="12" width="12" version="1.1" viewBox="0 0 12 12" id="close" y="0"><title>Close</title><desc/><g id="close-category_adaptive" fill-rule="evenodd" fill="none"><g id="close-05_Category-ot-320" fill="#9B9B9B" transform="translate(-92 -376)"><g id="" transform="translate(0 360)"><g id="" transform="translate(92 13)"><path id="close-Fill-20" d="m5.9998 10.692l-4.0537 4.054c-0.3876 0.388-1.0122 0.391-1.4097-0.006l-0.27654-0.277c-0.39099-0.391-0.39594-1.02-0.00627-1.409l4.0537-4.0542-4.0537-4.0537c-0.38754-0.3876-0.39127-1.0122 0.00626-1.4097l0.27656-0.2765c0.39099-0.391 1.02-0.396 1.4097-0.0063l4.0537 4.0537 4.0542-4.0537c0.387-0.3875 1.012-0.3913 1.409 0.0063l0.277 0.2765c0.391 0.391 0.396 1.02 0.006 1.4097l-4.0533 4.0537 4.0533 4.0542c0.388 0.387 0.392 1.012-0.006 1.409l-0.277 0.277c-0.391 0.391-1.02 0.396-1.409 0.006l-4.0542-4.054z"/></g></g></g></g></svg></svg></a>',
						'<i class="icon-paymethods {{ properties.icon_class }}"></i> <span class="del_name">{{ properties.del_name }}</span>',
						'{% if properties.b_addr_parts %}',
							'<div class="b-addr">',
							'{% for key, value in properties.b_addr_parts %}',
							'{{ value }}<br />',
							'{% endfor %}',
							'{% if properties.b_contact_link %}',
							'<br /><a href="{{ properties.b_contact_link }}" target="_blank" style="color: #2673c2;">как добраться</a>',
							'{% endif %}',
							'</div>',
						'{% else %}',
							'<div class="b-addr">{{ properties.b_addr }}</div>',
						'{% endif %}',
						'{% if !properties.b_addr_parts %}',
							'<div class="b-shed">{{ properties.b_shed }}</div>',
						'{% endif %}',
						'<div class="b-del">{{ properties.b_del }}</div>',
						'<div class="b-price">Стоимость - {{ properties.b_price }}</div>',
						'{% if properties.showModalButton %}',
							'<a href="#" class="btn btn-primary __select_point" data-price="{{ properties.price }}" data-pickup-type="{{ properties.pickupType }}" data-pickup-id="{{ properties.pid }}" data-payment-types="{{ properties.paymentTypes }}" data-type="{{ properties.typePoint }}" data-pid="{{ properties.pid }}">заберу здесь</a>',
						'{% endif %}',
					'</div>'
				].join(''));

			$(document).trigger('cart-rebuild-map', { key: $('#selfdelivery-radio li.checked').attr('data-key') });
		});
	}
	
	$('#daytime').on('mouseup', 'li', function(){
		$('#daytime').addClass('active');
		$('#interval').removeClass('active');
	});
	$('#interval').on('mouseup', 'li', function(){
		$('#interval').addClass('active');
		$('#daytime').removeClass('active');
	});
	$('input[name="delivery-time-id"]:visible').first().prop('checked', true);

	initNano();

    $('.cart-select').on(clickHandler,'.dropdown-menu a',function(){
        var href = $(this).attr('href');
        $('.cart-list-tabs').find('a[href="'+href+'"]').trigger(clickHandler);
    });
    $('.cart-selects-mob').on(clickHandler,'.dropdown-menu a',function(){
        var href = $(this).attr('href');
        $('.cart-delivery-types-cont').find('a[href="'+href+'"]').trigger(clickHandler);
    });
	
	$('#selfdelivery-radio').on(clickHandler, 'li', function(){
		var _ = $(this);
		var inputval = _.find('input:radio').val();
		if(inputval){
			var $ul = $('#show-'+inputval);
			if($ul.length){
				$ul.closest('.viewport').find('ul').hide();
				$ul.show();
				if($.fn.nanoScroller){
					$ul.closest('.nano').nanoScroller();
				}
			}
			$(document).trigger('cart-rebuild-map', _.data());
			if ($ul.length) {
				$ul.find('a.btn-primary:first').trigger(clickHandler);
			}
		}
	});
	
	$("a.onmap").on("click", function(ev) {
	   ev.preventDefault();
       $("a.get-shop").trigger("click");
	   $(".shop-selector")[0].contentWindow.setShopOnMap($(this).data("uid"));
	   return false;
	});
	
	$(".delivery-types").on("click", "li input[type=radio]:not([name=elevate-type])",  function(ev) {
	   $('.delivery-side-form').insertAfter($(this).parents("label")).show().find("input[name=delivery-street]").eq(0).focus();
	   return true;
	});    

});

function initInsurance() {
	$('.cart-item input[type="checkbox"]').each(function() {
		var _ = $(this);
		if (!_.hasClass('initialized')) {
			_.addClass('initialized');
			_.change(function() {
				updateOrderTotalAmount();
			});
		}
	});
}

$(document).ready(function() {
	if (0 < $('#body.order-page').length) {
		initInsurance();
	}

	$(document).on('insurance-changed', function(e, data) {
		var chk = $(data);
		if (chk.attr('data-uncheck') && chk.prop('checked')) {
			var ids = chk.attr('data-uncheck').split(',');
			for (var i = 0; i < ids.length; i++) {
				var id = ids[i];
				var el = $('#' + id);
				if (0 < el.length) {
					el.prop('checked', false);
					el.closest('li').removeClass('checked');
				}
			}
		}
		updateOrderTotalAmount();
	}).on('interval-changed', function(e, data) {
		var _ = $(data);
		$('.delivery-days a').removeClass('active').hide();
		$('.delivery-days a[data-sector="' + _.val() + '"]').show();
		$('input[name="delivery-service-id"]').val(_.attr('data-service-id'));
		$('.delivery-days a:visible').first().addClass('active');
		$('input[name="delivery-date"]').val($('.delivery-days a:visible').first().attr('data-value'));
		$('#delivery-type-own').attr('data-price', _.attr('data-price'));

		updateOrderTotalAmount();
		updateProductsAvailability();
	}).on('delivery-type-changed', function(e, data) {
		var _ = $(data);
		$('input[name="receipt-type"]').val('delivery');
		$('input[name="receipt-payment-types"]').val(_.attr('data-payment-types'));
		filterPaymentTypes();
		updateOrderTotalAmount();
	}).on(clickHandler, '.cart-list-tabs a', function(e) {
		var _ = $(this);
		$('input[name="payer-type"]').val(_.attr('data-payer-type'));
		filterPaymentTypes();
		updateOrderTotalAmount();
	}).on(clickHandler, '.tabs-list-receipt a', function(e) {
		if ($('#delivery').is(':visible')) {
			$('#delivery ul.delivery-type li:first').trigger(clickHandler);
			$('#delivery ul.delivery-type li:first ul li:first').trigger(clickHandler);
		}
	}).on("mouseup click keyup focus blur", ".step-receipt", function(e) {
	   setTimeout(function() {
    	   var dis = false;
    	   if ($("input[name=del-type]:checked").length==0) dis=true;
    	   if (!($("input[name=delivery-list-val]").val())) {
                var rq = $(".delivery-side-form label.require:not([for=delivery-date]):not([for=delivery-time])");
                for (var i=0; i<rq.length; i++) if (rq.eq(i).find("input").val().trim()=="") dis=true;	   
    	   }
    	   if ($("#delivery-date").val().trim()=="" || $("#delivery-time").val().trim()=="") dis = true;
    	   $(".-tab-delivery .form-bottom a.next-step").toggleClass("disabled", dis);
    	   $(".--total-aside .set-done a").toggleClass("disabled", $("#promo-code:visible").length==0 ||  $("a.next-step.disabled:visible").length!=0);
        }, 10);
	}).on("mouseup click keyup focus blur", ".step-payment", function(e) {
	   setTimeout(function() {
    	   var dis = false;
    	   if ($("input[name=payment-type]:checked").length==0) dis=true;
    	   else if ($("input[name=payment-type]:checked").data("formid")) {
    	        var f = $("#"+$('input[name=payment-type]:checked').data('formid'));
    	        var rq = f.find("label.require");
    	        var prst = rq.filter("label[for=jur-preset]");
    	        if (prst.length==0 || prst.length>0 && prst.find("input#jur-preset").val().trim()=="") {
    	            if (prst.length>0) rq = rq.not('label[for=jur-preset]');
    	            for (var i=0; i<rq.length; i++) if (rq.eq(i).find("input").val().trim()=="") dis=true;
    	        }
    	   }
    	   $(".step-payment .form-bottom a.next-step").toggleClass("disabled", dis);
    	   $(".--total-aside .set-done a").toggleClass("disabled", $("#promo-code:visible").length==0 || $("a.next-step.disabled:visible").length!=0);
        }, 10);
	}).on(clickHandler, "#form-order", function() {
	   setTimeout(function() {
    	   $(".--total-aside .set-done a").toggleClass("disabled", $("#promo-code:visible").length==0 ||  $("a.next-step.disabled:visible").length!=0);
        }, 200);
	}).on(clickHandler, ".step-receipt .-tab a.next-step", function(e) {
        e.preventDefault();
        if ($(this).hasClass("disabled")) return false;
        var rt = $(this).parents(".-tab"),
            data = { a: "set-delivery", type: rt.data("type") },
            ip = rt.find("input").not("input[type=radio]:not(:checked)");
            for (var i=0; i<ip.length; i++) data[ip.eq(i).attr("name")] = ip.eq(i).val();
        $.post(CART_URL, data, function(data) {
            if (data.state!="ok") {
                $("#order-create-error").window("open", { background: 1 });
            } else {
                var _st = $(".container.step-payment");
                _st.fadeIn(250);
                $('html,body').stop().animate({ scrollTop: _st.offset().top - 50 }, 150);
                $(".--total-aside .tpl-right").html(data.tplright);
                $(".sections-joiner").css({"min-height": $(".--total-aside").outerHeight() });
                $("#order-steps li.selected").removeClass("selected");
                $("#order-steps li:nth-child(3)").removeClass("disabled").addClass("selected");
                if ($(window).width()<760) {
                    $("#order-steps").stop().animate({scrollLeft: $("#order-steps li:nth-child(3)").position().left }, 100);
                }

                window.onr();
            }
        });

        return false;
    }).on(clickHandler, ".step-payment a.next-step", function(e) {
        e.preventDefault();
        var rt = $(".step-payment"),
            data = { a: "set-payment" },
            ip = rt.find("input").not("input[type=radio]:not(:checked)");
            for (var i=0; i<ip.length; i++) data[ip.eq(i).attr("name")] = ip.eq(i).val();
        $.post(CART_URL, data, function(data) {
            if (data.state!="ok") {
                $("#order-create-error").window("open", { background: 1 });
            } else {
                var _st = $(".container.step-balls");
                _st.fadeIn(250);
                $('html,body').stop().animate({ scrollTop: _st.offset().top - 50 }, 150);
                $(".--total-aside .tpl-right").html(data.tplright);
                $(".sections-joiner").css({"min-height": $(".--total-aside").outerHeight() });
                $("#order-steps li.selected").removeClass("selected");
                $("#order-steps li:nth-child(4)").removeClass("disabled").addClass("selected");
                if ($(window).width()<760) {
                    $("#order-steps").stop().animate({scrollLeft: 1500 }, 100);
                }

                window.onr();
                $("#form-order").trigger("click");
            }
        });

        return false;
    }).on(clickHandler, '#delivery ul.delivery-type li', function() {
		var _ = $(this);
		$('input[name="pickup-type"]').val(_.find('input[name="delivery-type"]').val());
		updateProductsAvailability();
	}).on(clickHandler, '.cart-item .order-delete', function(e) {
		e.preventDefault();
		window.removeItemButton = $(this);
        $("#remove-product-confirm").window("open", { magnet: $(this), background: true, position: "center" });
	});
	$(".window#remove-product-confirm div.contents a:not(.blue-button)").on("click", function(ev) {
	   ev.preventDefault();
	   $(".window#remove-product-confirm").window("close");
	   return false;
	});
	$(".window#remove-product-confirm div.contents a.blue-button").on("click", function(ev) {
	   ev.preventDefault();
	   $(".window#remove-product-confirm").window("close");
	   removeItem(removeItemButton);
	   return false;
	});
	
	var removeItem = function(_) {
		var productId = _.attr('data-product-id');
		if (productId) {
			$.ajax({
				method: 'get',
				url: '/basket/delete/',
				data: { product: productId },
				dataType: 'json',
				success: function(data) {
					if (data.success) {
						_.closest('.cart-item').remove();
						header_recalculate_hint(data.count, data.amount);
						header_recalculate_total();
						header_recalculate_quantity();

						if (0 == data.count) {
							$('header .cart-box').removeAttr('data-window').removeAttr('data-hover');
							$('header .header-top-menu-cart a').removeAttr('data-window').removeAttr('data-position');
							$('header .header-top-menu-cart .cnt').remove();
						}

						updateOrderTotalAmount();

						var product = data.product;
						if (product) {
							GaEcommerceHelper.removeProductFromCart(
								product.id,
								product.name,
								product.category,
								product.price,
								product.quantity
							);
							FlocktoryHelper.removeFromBasket(product.id, product.quantity);
						}
					}
				}
			});
		}
	}

	$('.apply-promo').click(function() {
		var _ = $(this);
		if (!_.hasClass('inprogress')) {
			_.addClass('inprogress');
			$('.promo-form .errors').html('Невозможно активировать промокод, попробуйте еще раз или укажите другой промокод').hide();

			var errorBlock = $('.promo-form .errors');

			var coupon = $('.promo-form input[name="coupon"]').val();
			if ('' == coupon) {
				errorBlock.html('Не указан промокод').show();
			}

			var url = '/ordering/apply-promo/';
			if (_.hasClass('btn-cancel')) {
				url = '/ordering/cancel-promo/';
			}

			$.ajax({
				url: url,
				method: 'get',
				data: { coupon: $('.promo-form input[name="coupon"]').val() },
				dataType: 'json',
				success: function(data) {
					if (data.success) {
						refreshPage();
					} else {
						if (data.errorMessage) {
							errorBlock.html(data.errorMessage);
						}
						errorBlock.show();
					}
				},
				complete: function() {
					_.removeClass('inprogress');
				},
				error: function() {
					errorBlock.show();
				}
			});
		}
		return false;
	});

    $("#ordererror>div a").on("click", function(ev) {
        ev.preventDefault();
        $("body").removeClass("-order-error");
        return false;
    });

	$('.btn-order-create').click(function(e) {
		e.stopPropagation();

		var _ = $(this);
		if (!_.hasClass('inprogress')) {
			_.addClass('inprogress');
			$("body").addClass("order-in-progress");

/*
			var wnd = $('#order-create');
			wnd.window('open', {
				background: true,
				responsive: true
			}, function() {
				var scrollTop = $(window).scrollTop();
				var wndHeight = wnd.height();
				var viewportHeight = $(window).height();
				var offsetTop = Math.ceil(scrollTop + ((viewportHeight - wndHeight) / 3));
				wnd.css('top', offsetTop + 'px');
			});
*/

			var frm = $('#form-order');
			frm.find('label.error').removeClass('error');
			frm.find('.hint').remove();
			frm.find('.alert').remove();
			frm.find('.note').remove();
			frm.removeClass('success').removeClass('error');

			if (0 < $('input[name="user-full-fio-placeholder"]:visible').length) {
				$('input[name="user-full-fio"]').val($('input[name="user-full-fio-placeholder"]:visible').first().val());
			}
			updateOrderTotalAmount();
			ga(function(tracker) {
				var clientId = '';
				try {
					var trackers = ga.getAll();
					var i, len;
					for (i = 0, len = trackers.length; i < len; i += 1) {
						if (!clientId && (trackers[i].get('trackingId') === "UA-25538461-2")) {
							clientId = trackers[i].get('clientId');
						}
					}
				} catch(e) {console.log('error', e);}
				$('#form-order input[name="ga-client-id"]').val(clientId);
			});
			$.ajax({
				method: 'post',
				url: '/ordering/create/',
				data: formData('#form-order'),
				dataType: 'json',
				success: function(data) {
        			$("body").removeClass("order-in-progress");
					if (data.success) {
						if (data.creditline) {
							$.getScript('https://onlineapproved.l-kredit.ru/assets_widget/l-kredit.js', function() {

								$('#creditline-btn').attr('success-url', data.creditline.successUrl);
								$('#creditline-btn').attr('cancelled-url', data.creditline.cancelledUrl);
								$('#creditline-btn').attr('idle-url', data.creditline.idleUrl);
								$('#creditline-btn').attr('data-has-callback', 0);

								var params = data.creditline.data;
								params.elm = 'creditline-btn';
								params.autostart = true;
								params.siteName = '123.RU';
								params.site = '123.ru';
								params.URLSuccess = data.creditline.successUrl;
								params.callbackPartner = creditlineCallback;
								console.log('creditline', params);
								CLObject.create(params);

								if (false) {
									var tmr = setInterval(function() {
										var isVisible = exists('.wgt_cl_modal:visible');
										if (!isVisible) {
											var hasCallback = (1 == parseInt($('#creditline-btn').attr('data-has-callback')));
											if (hasCallback) {
												clearInterval(tmr);
												redirect(data.creditline.cancelledUrl);
											}
										}
									}, 1000);
								}
							});
						} else {
							return redirect('/ordering/success/?id=' + data.order.number);
						}
					} else {
						if (data.errorMessage) {
							if (data.errorField) {
								wnd.window('close');

								var inp = frm.find('input[name="' + data.errorField + '"]');
								inp.parent().removeClass('success').addClass('error');
								inp.parent().find('.hint').remove();
								inp.parent().append('<i class="hint">' + data.errorMessage + '</i><i class="wrong-input"></i>');
								inp.trigger('focus');

								if (data.errorFields) {
									for (var i = 0; i < data.errorFields.length; i++) {
										var field = data.errorFields[i];
										var inp = frm.find('input[name="' + field.name + '"]');
										inp.parent().removeClass('success').addClass('error');
										inp.parent().find('.hint').remove();
										inp.parent().append('<i class="hint">' + field.message + '</i><i class="wrong-input"></i>');
									}
								}
							} else {
								showOrderError(data.errorMessage);
							}
						} else {
							showOrderError();
						}
					}
				},
				complete: function() {
					_.removeClass('inprogress');
        			$("body").removeClass("order-in-progress");
				},
				error: function() {
					showOrderError();
				}
			});
		}

		function showOrderError(text) {
		    return $("body").addClass("-order-error");
		
			text = text || 'Невозможно оформить заказ, попробуйте еще раз';
			$('#order-create-error .body').html(text);

			var wnd = $('#order-create-error');
			wnd.window('open', {
				background: true,
				responsive: true
			}, function() {
				var scrollTop = $(window).scrollTop();
				var wndHeight = wnd.height();
				var viewportHeight = $(window).height();
				var offsetTop = Math.ceil(scrollTop + ((viewportHeight - wndHeight) / 3));
				wnd.css('top', offsetTop + 'px');
			});
		}

		return false;
	});

	$('.tabs-viewport').on(clickHandler, '.legal-select .dropdown-menu li a', function() {
		var _ = $(this);
		$('#profile-form input[name="legal-company-name"]').val(_.attr('data-name'));
		$('#profile-form input[name="legal-address"]').val(_.attr('data-address'));
		$('#profile-form input[name="legal-inn"]').val(_.attr('data-inn'));
		$('#profile-form input[name="legal-kpp"]').val(_.attr('data-kpp'));
	});

	filterPaymentTypes();
	updateProductsAvailability();
	initPayer();

	if (1 == $('.tabs-list-receipt a').length) {
		$('.radio-group.delivery-type li:first').click();
		$('#daytime li:first').click();
		console.log(1038);
	}
});

function updateProfileForm(type) {
	var form = $('#profile-form').remove();
	$('#' + type).find('.form-fields').html('').append(form);
}
function updateNaturalProfile() {
	updateProfileForm('natural');
	$('.legal-field').hide();
}
function updateLegalProfile() {
	updateProfileForm('legal');
	$('.legal-field').show();
}

function creditlineCallback(status, number) {
	$('#creditline-btn').attr('data-has-callback', 1);
	var _ = $('#creditline-btn');
	if (0 == status) {
		redirect(_.attr('cancelled-url'));
	} else if (-1 == status) {
		redirect(_.attr('idle-url'));
	} else {
		redirect(_.attr('success-url'));
	}
}

function rootFixInit() {
    var ons = function() {
        $(".-root-fix").each(function() {
            var st = $(window).scrollTop(),
                wh = $(window).height(),
                rt = $(this).parent().offset().top,
                tt = $(this).offset().top,
                rh = $(this).parent().height(),
                th = $(this).height(),
                dy = parseInt($(this).data("topfix")),
                mnh = 70,
                y;
            if (isNaN(dy) || typeof(dy)=="undefined") dy = 0;
            if (rt-mnh+dy>=st || th>=rh) y=0;
            else if (rt+rh-mnh+dy<st+th) y = rh-th;
            else y = Math.abs(rt - mnh + dy - st);
            if (window.innerWidth<768) y = 0;
            if (y==0) $(this).css({"transform": ""});
                      else $(this).css({ "transform": "translateY("+y+"px)" });
        });
        $(".sections-joiner").css({"min-height": $(".--total-aside").outerHeight() });

        
        var _t = $(window).scrollTop(), _r = $("#order-steps"), _mn =80, _y = _r.outerHeight();
        _r.css({width: $(".sections-joiner.container").outerWidth()});
        if ($(window).width()>=760 && $(window).width()<1320) _mn=70;
        if ($(window).width()<760) _mn=115;
        if (_t<_mn) {
            var tr = _t;
            _r.css({ "transform": "translateY(-"+(tr)+"px)" });
        } else {
            _r.css({ "transform": "translateY(-"+(_mn)+"px)" });
        }
    }
    window.onr = function() {
        if (window.innerWidth<768) {
                $(".--total-aside").removeClass("-root-fix").appendTo(".-mob-total");
                $(".--total-aside .set-done a").removeClass("white-button").addClass("blue-button");
        } else {
                $(".--total-aside").addClass("-root-fix").prependTo(".sections-joiner");
                $(".--total-aside .set-done a").addClass("white-button").removeClass("blue-button");
        }
        if (window.innerWidth<425) {
            $(".cart-item .-price").each(function() {
                $(this).appendTo($(this).parents(".cart-item").find(".-body"));
            });
            $(".form-elements .-pform.toggleform label.half-wide.form-info").each(function() {
                $(this).insertBefore($(this).parents(".-pform").find("label").eq(0));
            });
            $(".-jur-form").insertAfter(".-paste-jur-form");
        } else if (window.innerWidth<769) {
            $(".form-elements .-pform.toggleform label.half-wide.form-info").each(function() {
                $(this).insertBefore($(this).parents(".-pform").find("label").eq(0));
            });        
        } else {
            $(".cart-item .-price").each(function() {
                $(this).appendTo($(this).parents(".cart-item"));
            });
            $(".form-elements .-pform.toggleform label.half-wide.form-info").each(function() {
                $(this).insertAfter($(this).parents(".-pform").find("label").eq(1));
            });
        }
        ons();
    }
    onr();
    window.addEventListener("scroll", ons);
    window.addEventListener("resize", onr);
}

function checksInit() {

    if (screen.width<375) {
        $("meta[name=viewport]").attr("content", "width=375,user-scalable=no");
    }

    $(".-set-tabs").each(function() {
        $(this).find("input[type=radio]").on("click", function() {
            var tb = $(this).parents(".-set-tabs").parent().find(".-tab"), key = $(this).val();
            if (key=="pickup" && !$("#pickup-radio + .block").hasClass("-selected")) {
                $(".form-elements .-tab.-tab-pickup a.get-shop").trigger("click");
                $("label[for=pickup-radio] input")[0].checked = false;
            }
            if (tb.filter(".-tab-"+key).hasClass("visible")) return true;
            if (tb.filter(".visible").length==0) {
                tb.filter(".-tab-"+key).addClass("visible").fadeIn(150);
            } else {
                tb.filter(".visible").fadeOut(150, function() {
                    $(this).removeClass("visible");
                    tb.filter(".-tab-"+key).addClass("visible").fadeIn(150);
                });
            }

        })
    });
    $("a.set-other-point").on("click", function(e) {
        e.preventDefault();
        return $('.form-elements .-tab.visible.-tab-pickup a.get-shop').trigger('click');
    });
    
    
    $("input.-selector").on("click", function(ev) {
        $("input.-selector").not(this).removeClass('-expanded');
        $(this).toggleClass("-expanded");
    }).each(function() {
        $(this).next("ul").addClass("--sel-list").find("li").on("click", function(ev) {
            ev.preventDefault();
            if ($(this).hasClass("empty")) {
                $(this).parents("ul").prev().val("").attr("data-val", $(this).data('value')).removeClass("-expanded").prev().val("");
            } else {
                $(this).parents("ul").prev().val($(this).text()).attr("data-val", $(this).data('value')).removeClass("-expanded").prev().val($(this).data('value'));
            }
            if ($(this).parents("ul").prev().attr("name")=="delivery-list") {
                $(".delivery-side-form>*").filter(".row.l-1, label[for=name]").toggle($(this).hasClass("empty"));
            }
            if ($(this).parents("ul").prev().attr("name")=="jur-id") {
                $(this).parents(".-pform").find(">*").not(".form-info, label[for=jur-preset]").toggle($(this).hasClass("empty"));
            }
            return false;
        });
        var ihv = $("<input type='hidden' name='"+$(this).attr("name")+"-val' value=''>");
            ihv.insertBefore(this);
    });
    
    
    
    $("input[type=radio]").on("click", function() {
        $(".stage-block").toggleClass("visible", $(this).hasClass("-show-stage"));
        if ($(this).hasClass("show-form")) {
            $(".-pform").hide();
            var sf = $(this).data("formid");
            if (sf) $("#"+sf).show();
        }
    });
    
    document.body.addEventListener("click", function(ev) {
        if (ev.target.tagName.toLowerCase()!="input" && !$(ev.target).hasClass("--sel-list") && $(ev.target).parents(".--sel-list").length==0) $(".--sel-list").prev().removeClass("-expanded");
    });
    
    $(".t-promo input[type=text]").on("keydown keyup", function(e) {
        if (e.which==13) {
            e.preventDefault();
            if (e.type=='keydown') $(this).next().trigger("click");
            return false;
        }
    });
    
    $(".step-balls .form-elements a, .t-promo a").on('click', function(e) {
        e.preventDefault();
        var val = $(this).parent().find("input").val(), $this = $(this);
        $.post(CART_URL, { a: "set-promo", v: val, type: $(this).data("type") }, function(data) {
            var tp = $this.parents("label");
            if (tp.length==0) tp = $this.parents(".t-promo");
            if (data.state=="ok") {
                tp.removeClass('error').toggleClass("selected", data.message!="").find("b").text(data.message);
                $this.parent().find("input").attr("readonly", data.message!="");
                window.currentDiscount = data.discount;
            } else {
                tp.removeClass('selected').toggleClass("error", data.message!="").find("em").text(data.message);
                $this.parent().find("input").attr('readonly', null);
                window.currentDiscount = 0;
            }
            updateOrderTotalAmount();
            $(".--total-aside .tpl-right").html(data.tplright);
            $(".sections-joiner").css({"min-height": $(".--total-aside").outerHeight() });
        });
        return false;
    });
    
    $(".window#registration .-continue").on("click", function(e) {
        e.preventDefault();
        $(".window#registration").window("close");
        $(".next-no-reg").trigger("click");
        return false;
    });
    
    window.loginFormDone = function(data) {
        $(".auth-block .-login").removeClass("busy");
        if (data.state!="ok") {
            $(".auth-block .-login").addClass('error').find(".error").text(data.message);
        } else {
            $(".auth-block").fadeOut(250, function() {
                $(".sections-joiner, .-mob-total").fadeIn(250);
                $(".auth-block").remove();
                $('html,body').stop().animate({ scrollTop: $(".sections-joiner").offset().top - 50 }, 150);
            });
            $(".--total-aside .tpl-right").html(data.tplright);
            $(".sections-joiner").css({"min-height": $(".--total-aside").outerHeight() });
            window.onr();
        }
    }
    
    $("a.set-login, .next-no-reg").on('click', function(e) {
        e.preventDefault();
        var noreg = $(this).hasClass("next-no-reg");
        if (noreg) {
        console.log($("label[for=balls-form]"));
            $("label[for=balls-form]").hide().next().trigger("click");
        }
        if ($(".auth-block .-login").hasClass("busy")) return false;
        $(".auth-block .-login").removeClass('error').addClass("busy");
        $.post(CART_URL, {
            a: "login",
            type: noreg ? "noreg" : ($(".auth-block .-tab.visible input[name=user-email]").length ? "email" : "phone"),
            login: noreg ? "" : $(".auth-block .-tab.visible input:not([type=password])").val(),
            password: noreg ? "" : $(".auth-block .-tab.visible input[type=password]").val(),
            agreement: noreg ? 0 : $(".auth-block .-use-data")[0].checked * 1,
            remember: noreg ? 0 : $(".auth-block .-member-me")[0].checked * 1
        }, loginFormDone);
        return false;
    });
    
    $(".t-footer .set-order, .-mmb.start-order a.blue-button").on("click", function(ev) {
        ev.preventDefault();
        var _st = $(".auth-block");
            if (!_st.length) _st = $(".sections-joiner, .-mob-total");
        $.post(CART_URL, { a: "start-order" }, function(data) {
            if (data.state!="ok") {
                $("#order-create-error").window("open", { background: 1 });
            } else {
                $("#order-steps").css({display:"block"});
                $("section.step-shopcart, h1").fadeOut(250, function() {
                    _st.fadeIn(250);
                    _st.addClass('visible');
                    $('html,body').stop().animate({ scrollTop: _st.offset().top - 40 }, 150);
                });
                $(".--total-aside .tpl-right").html(data.tplright);
                $(".sections-joiner").css({"min-height": $(".--total-aside").outerHeight() });
                window.onr();
            }
        });
        return false;
    });
       
}

var setPickupPoint = function(data) {
    $(".form-elements label.-radio .block>.-selected-point i").html(data.address);
    $(".form-elements label.-radio .block").addClass("-selected");
    $(".-tab-pickup").data("uid", data.uid)
        .find(".pickup-info h5").html(data.header);
    $("a.onmap").attr("href", data.mapLink).data("uid", data.uid);
    $(".-tab-pickup .pickup-info p").remove();
    $(data.description).insertBefore(".-tab-pickup .pickup-info a.get-shop");
    $("input[name=pickup-id]").val(data.uid);
    $(".form-elements .-tab.-tab-pickup a.next-step").removeClass("disabled");
    $("label[for=pickup-radio] input")[0].checked = true;
}