$(function() {

    var curPosition = 0;

    $(".-compare-header").clone().addClass("-clone").insertAfter(".-compare-header");
    ons = function() {
        var st = $(window).scrollTop(), pt = $(".-compare-header:not(.-clone)").offset().top, hg = $(".-compare-header:not(.-clone)").outerHeight(),
            bt = $(".compare-parameters").offset().top + $(".compare-parameters").outerHeight() - $(".compare-parameters li:last-child").outerHeight();
        var bh = window.innerWidth<760 ? 120 : 170, ih = bh - (st-pt);
            if (ih<0) ih = 0;

        $('body').toggleClass('-scrolled', st>=pt);
            $(".-compare-header.-clone>.-items .all-blocks ul li .image").css({ maxHeight: ih+"px" });
            $(".-compare-header.-clone").css({ height: 200+ih });
        var dt = 0;
            if (st>=bt-hg) dt = bt-hg-st;
        $(".-compare-header.-clone").css({ "transform": "translateY("+dt+"px)" });
    }
    
    ons()
    window.addEventListener("scroll", ons);
    window.addEventListener("resize", ons);

    var scrollItems = function(n) {
        var itms = $(".-compare-header:not(.-clone) li"), w0 = itms.eq(0).outerWidth(), trx;
        while (n > itms.length - Math.ceil( $(".-compare-header.-clone>.-items .all-blocks").outerWidth() / w0 )  ) n--;
        if (n<0) n = 0;
        trx = n*w0;
        $(".-compare-header li, .compare-parameters>.parameter ul li").css({ "transform": "translateX(-"+trx+"px)" });
        curPosition = n;
        $(".-compare-header>.container>a.-scroll.-right").toggleClass("disabled", n >= itms.length - Math.ceil( $(".-compare-header.-clone>.-items .all-blocks").outerWidth() / w0 ));
        $(".-compare-header>.container>a.-scroll.-left").toggleClass("disabled", n <= 0);
        return n;
    }
    
    $(".-compare-header>.container>a.-scroll").on("click", function(e) {
        e.preventDefault();
        var n = $(this).hasClass("-left") ? -1 : 1;
        scrollItems(curPosition+n);
        return false;
    });
    
    var sx, sy, sdx, lsx, lsy, mxxw = 0, sst=false;
    $(".-compare-header ul")
        .on("touchstart mousedown", function(e) {
            if (e.type=="mousedown") {
                if (e.buttons!=1) return true;
                sx = e.screenX;
                sy = e.screenY;
            } else {
                sx = e.touches[0].screenX;
                sy = e.touches[0].screenY;
            }
            var dt = $(".-compare-header li").eq(0).css("transform").replace(/^.*\((.*)\).*$/, '$1').replace(/\s/g, '').split(',');
            sdx = dt[4]*1;
            sst = true, lsx = 0,
            mxxw = $(".-compare-header:not(.-clone) li").length * $(".-compare-header li").eq(0).outerWidth() - $(".-compare-header>.-items .all-blocks").eq(0).outerWidth();
            $("body").addClass("-ondrag");
            return true;
        })
        .on("touchmove mousemove", function(e) {
            if (!sst) return true;
            var ddx = sdx, ssx, ssy;
            if (e.type=="mousemove") {
                ssx = e.screenX;
                ssy = e.screenY;
                ddx += ssx - sx;
            } else {
                ssx = e.touches[0].screenX;
                ssy = e.touches[0].screenY;
                if (Math.abs(ssx-sx)>=Math.abs(ssy-sy)) ddx += ssx - sx;;
            }
            if (ddx>0) return ddx = 0;
            if (Math.abs(ddx)>=mxxw) {
                ddx = -mxxw;
            }
            lsx = ssx;
            lsy = ssy;
            $(".-compare-header li, .compare-parameters>.parameter ul li").css({ "transform": "translateX("+ddx+"px)" });
            return true;
        })
        .on("touchend mouseup", function(e) {
            if (!sst) return true;
            if (Math.abs(lsx)>=20) e.preventDefault();
            var ddx = sdx, ssx = lsx, ssy = lsy;
            if (e.type=="mouseup") {
                ddx += ssx - sx;
            } else {
                ddx += ssx - sx;
            }
            var n = (-1 * ddx) / $(".-compare-header:not(.-clone) li").eq(0).outerWidth();
            if (ssx > sx) n = Math.floor(n); else n = Math.ceil(n);
            if (Math.abs(ssx-sx)<Math.abs(ssy-sy)) n = curPosition;
            $("body").removeClass("-ondrag");
            scrollItems(n);
            sst = false;
            return true; 
        });
        
    $(".-compare-header>.-items .all-blocks ul li a.-delete").on("click", function(e) {
        e.preventDefault();
        var ix = $(this).parents("ul").find("li").index($(this).parents('li').eq(0)), $this = $(this);
        $.post( "/!ajax/compare.php", { a: 'remove', id: $this.parents("li").eq(0).data("id") }, function(data) { 
            $(".-compare-header>.-items .all-blocks ul").each(function() {
                $(this).find("li").eq(ix).fadeOut(300, function() { $(this).remove(); scrollItems(curPosition) });
            });
            $(".compare-parameters>.parameter ul").each(function() {
                $(this).find("li").eq(ix).fadeOut(300, function() { $(this).remove(); });
            });
            var N = ($(".-compare-header:not(.-clone)>.-items .all-blocks ul li").length-1);
            $(".-compare-header>.-items .start-block h4 span").html("("+N+")");
            if (N==0) location.href = data.redirect;
        });
        return false;
    });

});