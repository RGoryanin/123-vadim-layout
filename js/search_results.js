$(function(){
	
	$(document).on('width-xs', function(){
		
		$('.search-results-header').addClass('opener collapsed');
		$('.search-results .one-row-slider').hide();
		
	}).on('width-sm width-md width-lg', function(){
		
		$('.search-results-header').removeClass('opener collapsed');
		$('.search-results .one-row-slider').show();
		
	}).on(clickHandler, '.search-results-header.opener', function(){
		
		var _ = $(this);
		if(_.hasClass('collapsed')){
			
			_.removeClass('collapsed');
			_.parent().find('.one-row-slider').stop(1,1).slideDown(300);
			
		} else {
			
			_.addClass('collapsed');
			_.parent().find('.one-row-slider').stop(1,1).slideUp(300);
		}
		
	});
	
	window.fire_resizeWidth();
	
});