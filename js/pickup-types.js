var pickupTypes = {
    "zakazberry": {
        "title": "ZakazBerry",
        "uid": "zakazberry",
        "points": {
            "zakazberry_pickup_124": {
                "id": 124,
                "uid": "zakazberry_pickup_124",
                "agent": "zakazberry",
                "price": 0,
                "title": "м. Водный Стадион",
                "address": "Москва, Кронштадтский б-р, д. 9, стр. 4<br />ст. м. Водный Стадион<br />ежедневно с 10 до 22<br />+7 (495) 221-6777",
                "addressParts": ["Москва, Кронштадтский б-р, д. 9, стр. 4, 2 этаж", "ст. м. Водный Стадион", "пн-пт с 10 до 22", "+7 (495) 221-6777"],
                "contactLink": null,
                "pickupDatetime": {
                    "date": "2018-09-05 12:00:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Ближайший самовывоз 05 сентября с 12:00",
                "lat": "55.843202",
                "lon": "37.488861",
                "workhours": "ежедневно с 10 до 22"
            },
            "zakazberry_pickup_71": {
                "id": 71,
                "uid": "zakazberry_pickup_71",
                "agent": "zakazberry",
                "price": 0,
                "title": "г. Москва, Барабанный пер., д. 4 стр. 2",
                "address": "Москва, Барабанный пер. д. 4 стр. 2<br />ст. м. Электрозаводская<br />ежедневно с 9 до 21<br />+7 (495) 221-6777<br /><a href=\"/shop/elektrozavodskaya/\" target=\"_blank\">как добраться</a>",
                "addressParts": ["Москва, Барабанный пер. д. 4 стр. 2", "ст. м. Электрозаводская", "ежедневно с 9 до 21", "+7 (495) 221-6777"],
                "contactLink": "/shop/elektrozavodskaya/",
                "pickupDatetime": {
                    "date": "2018-09-05 10:00:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Ближайший самовывоз 05 сентября с 10:00",
                "lat": "55.783870",
                "lon": "37.708530",
                "workhours": "ежедневно с 9 до 21"
            },
            "zakazberry_pickup_351": {
                "id": 351,
                "uid": "zakazberry_pickup_351",
                "agent": "zakazberry",
                "price": 0,
                "title": "г. Москва, Перерва ул., д. 43, (ТЦ «Экватор», 2 этаж)",
                "address": "Москва, улица Перерва, д. 43. ТЦ «Экватор», 2 этаж<br />ст. м. Братиславская<br />пн-пт с 10 до 21<br />+7 (495) 221-6777<br /><a href=\"/shop/bratislavskaya/\" target=\"_blank\">как добраться</a>",
                "addressParts": ["Москва, улица Перерва, д. 43. ТЦ «Экватор», 2 этаж", "ст. м. Братиславская", "пн-пт с 10 до 21", "+7 (495) 221-6777"],
                "contactLink": "/shop/bratislavskaya/",
                "pickupDatetime": {
                    "date": "2018-09-05 12:00:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Ближайший самовывоз 05 сентября с 12:00",
                "lat": "55.659622",
                "lon": "37.747738",
                "workhours": "пн-пт с 10 до 21"
            },
            "zakazberry_pickup_349": {
                "id": 349,
                "uid": "zakazberry_pickup_349",
                "agent": "zakazberry",
                "price": 0,
                "title": "г. Москва, Малая Тульская ул., д. 25",
                "address": "Москва, ул. Малая Тульская, д. 25<br />ст. м. Тульская<br />пн-пт с 10 до 21<br />+7 (495) 221-6777<br /><a href=\"/shop/tulskaya/\" target=\"_blank\">как добраться</a>",
                "addressParts": ["Москва, ул. Малая Тульская, д. 25", "ст. м. Тульская", "пн-пт с 10 до 21", "+7 (495) 221-6777"],
                "contactLink": "/shop/tulskaya/",
                "pickupDatetime": {
                    "date": "2018-09-05 11:00:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Ближайший самовывоз 05 сентября с 11:00",
                "lat": "55.707268",
                "lon": "37.618363",
                "workhours": "пн-пт с 10 до 21"
            }
        },
        "paymentTypes": [20, 1, 2, 9, 15, 25, 39, 48],
        "price": 0,
        "pickupDatetime": {
            "date": "2018-09-05 10:00:00.000000",
            "timezone_type": 3,
            "timezone": "Europe/Moscow"
        },
        "pickupDatetimeTimestamp": 1536130800,
        "pickupTitle": "Ближайший самовывоз 05 сентября с 10:00"
    },
    "iml": {
        "title": "Пункт выдачи IML",
        "uid": "iml",
        "code": "IML_PICKUP",
        "points": {
            "IML_PICKUP_МОСКВА_172": {
                "id": 32860,
                "uid": "IML_PICKUP_МОСКВА_172",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "3-я Мытищинская дом 3 стр. 1 офис 501",
                "address": "3-я Мытищинская дом 3 стр. 1 офис 501",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.805321",
                "lon": "37.642069",
                "workhours": "Пн-Вс 10:00-21:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_175": {
                "id": 34274,
                "uid": "IML_PICKUP_МОСКВА_175",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "5-я Кабельная, д. 2, стр. 1",
                "address": "5-я Кабельная, д. 2, стр. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.737725",
                "lon": "37.716827",
                "workhours": "Ср-Вс 11:00-20:00 (без п-ва), Пн Вт Вых"
            },
            "IML_PICKUP_МОСКВА_26": {
                "id": 19638,
                "uid": "IML_PICKUP_МОСКВА_26",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Алтуфьевское ш. д.80",
                "address": "Алтуфьевское ш. д.80",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.894323",
                "lon": "37.588242",
                "workhours": "Пн-Пт 10:00-20:30 (без п-ва), Сб-Вс 10:00-19:30 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_219": {
                "id": 40072,
                "uid": "IML_PICKUP_МОСКВА_219",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Багратионовский пр-д, дом № 7, кор 3, пав. D1-073A",
                "address": "Багратионовский пр-д, дом № 7, кор 3, пав. D1-073A",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.741490",
                "lon": "37.504043",
                "workhours": "Пн-Вс 10:00-21:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_203": {
                "id": 40325,
                "uid": "IML_PICKUP_МОСКВА_203",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Балаклавский проспект, д. 5",
                "address": "Балаклавский проспект, д. 5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.640421",
                "lon": "37.610260",
                "workhours": "ПН-ПТ 10-20, СБ-ВС 10-18"
            },
            "IML_PICKUP_МОСКВА_208": {
                "id": 40330,
                "uid": "IML_PICKUP_МОСКВА_208",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Варшавское шоссе, д. 76, корп. 2",
                "address": "Варшавское шоссе, д. 76, корп. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.654493",
                "lon": "37.617878",
                "workhours": "ПН-ПТ 10-21, СБ 10-19, ВС-выходной"
            },
            "IML_PICKUP_МОСКВА_178": {
                "id": 34734,
                "uid": "IML_PICKUP_МОСКВА_178",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "г. Видное, пр-т Ленинского Комсомола, д.9, кор.4",
                "address": "г. Видное, пр-т Ленинского Комсомола, д.9, кор.4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.546485",
                "lon": "37.710519",
                "workhours": "Пн-Пт 09:00-21:00 (без п-ва), Сб-Вс 10:00-21:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_155": {
                "id": 25203,
                "uid": "IML_PICKUP_МОСКВА_155",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "г. Волоколамск, ул. Школьная, д. 8а.",
                "address": "г. Волоколамск, ул. Школьная, д. 8а.",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "56.029360",
                "lon": "35.954997",
                "workhours": "Пн-Пт 09:00-19:00 (без п-ва), Сб 09:00-18:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_161": {
                "id": 25555,
                "uid": "IML_PICKUP_МОСКВА_161",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "г. Домодедово, ул. Гагарина, д. 45.",
                "address": "г. Домодедово, ул. Гагарина, д. 45.",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.450879",
                "lon": "37.743381",
                "workhours": "Пн-Сб 10:00-20:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_170": {
                "id": 32859,
                "uid": "IML_PICKUP_МОСКВА_170",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "г. Коломна, Окский проспект, д. 3а",
                "address": "г. Коломна, Окский проспект, д. 3а",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.080942",
                "lon": "38.812161",
                "workhours": "Пн-Пт 10:00-18:30 (без п-ва), Сб 10:00-15:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_164": {
                "id": 32854,
                "uid": "IML_PICKUP_МОСКВА_164",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "г. Монино, Новинское шоссе, д.2А",
                "address": "г. Монино, Новинское шоссе, д.2А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.843884",
                "lon": "36.200813",
                "workhours": "Пн-Сб 10:00-20:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_204": {
                "id": 40326,
                "uid": "IML_PICKUP_МОСКВА_204",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "г. Ногинск, ул. Самодеятельная, д.10",
                "address": "г. Ногинск, ул. Самодеятельная, д.10",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.869516",
                "lon": "38.430341",
                "workhours": "Пн-Пт 10:00-20:00 (без п-ва), Сб 10:00-18:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_163": {
                "id": 32853,
                "uid": "IML_PICKUP_МОСКВА_163",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "г. Одинцово, Можайское шоссе д.71  т/ц  Дубрава",
                "address": "г. Одинцово, Можайское шоссе д.71  т/ц  Дубрава",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.677539",
                "lon": "37.280569",
                "workhours": "Пн-Вс 10:00-21:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_135": {
                "id": 24323,
                "uid": "IML_PICKUP_МОСКВА_135",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "г. Подольск, Проспект Ленина, д. 10",
                "address": "г. Подольск, Проспект Ленина, д. 10",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.448622",
                "lon": "37.551295",
                "workhours": "Пн-Сб 10:00-22:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_112": {
                "id": 24109,
                "uid": "IML_PICKUP_МОСКВА_112",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "г. Подольск, ул. Кирова, д. 50/2",
                "address": "г. Подольск, ул. Кирова, д. 50/2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.425748",
                "lon": "37.526599",
                "workhours": "пн-пт с 09:00 до 19:00 (перерыв с 13:00 до 14:00); сб с 10.00 до 17.00; вс c 10:00 до 16:00"
            },
            "IML_PICKUP_МОСКВА_25": {
                "id": 19534,
                "uid": "IML_PICKUP_МОСКВА_25",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "г. Подольск, ул. Юбилейная, д. 11.",
                "address": "г. Подольск, ул. Юбилейная, д. 11.",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.423232",
                "lon": "37.498800",
                "workhours": "Пн-Пт 10:00-20:00 (14:00-15:00), Сб 10:00-18:00 (14:00-15:00), Вс 10:00-15:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_165": {
                "id": 32855,
                "uid": "IML_PICKUP_МОСКВА_165",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "г. Руза, Интернациональный переулок, д. 5.",
                "address": "г. Руза, Интернациональный переулок, д. 5.",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.706751",
                "lon": "36.208637",
                "workhours": "Пн-Чт 09:00-18:00 (без п-ва),Пт 09:00-17:00, Сб Вс Вых"
            },
            "IML_PICKUP_МОСКВА_59": {
                "id": 19546,
                "uid": "IML_PICKUP_МОСКВА_59",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "г. Химки, мкр.Сходня, ул.Кирова, д. 3А, офис 35.",
                "address": "г. Химки, мкр.Сходня, ул.Кирова, д. 3А, офис 35.",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.950257",
                "lon": "37.297278",
                "workhours": "Пн-Пт 10:00-20:00 (без п-ва), Сб 10:00-18:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_36": {
                "id": 19541,
                "uid": "IML_PICKUP_МОСКВА_36",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "г. Химки, ул. Ленинградская, д. 19, офис 5.",
                "address": "г. Химки, ул. Ленинградская, д. 19, офис 5.",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.892345",
                "lon": "37.434388",
                "workhours": "Пн-Пт 11:00-19:30 (15:00-16:00), Сб Вс Вых"
            },
            "IML_PICKUP_МОСКВА_145": {
                "id": 25196,
                "uid": "IML_PICKUP_МОСКВА_145",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "г. Чехов, ул. Первомайская, 33",
                "address": "г. Чехов, ул. Первомайская, 33",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.144567",
                "lon": "37.455319",
                "workhours": "Пн-Пт 09:30-19:30 (без п-ва), Сб-Вс 09:30-17:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_94": {
                "id": 19556,
                "uid": "IML_PICKUP_МОСКВА_94",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "г.Апрелевка, ул. Августовская, д40",
                "address": "г.Апрелевка, ул. Августовская, д40",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.553422",
                "lon": "37.079266",
                "workhours": "Пн-Сб 11:00-21:00 (без п-ва), Вс вых"
            },
            "IML_PICKUP_МОСКВА_137": {
                "id": 24695,
                "uid": "IML_PICKUP_МОСКВА_137",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "г.Домодедово Привокзальная пл., д. 7",
                "address": "г.Домодедово Привокзальная пл., д. 7",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.438549",
                "lon": "37.773149",
                "workhours": "Пн-Пт 09:00-20:00 (без п-ва), Сб-Вс 09:00-19:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_60": {
                "id": 19547,
                "uid": "IML_PICKUP_МОСКВА_60",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "г.Домодедово, ул. Лунная 9, пом. IV",
                "address": "г.Домодедово, ул. Лунная 9, пом. IV",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.426712",
                "lon": "37.756173",
                "workhours": "Пн-Пт 10:00-20:00 (без п-ва), Сб-Вс 10:00-18:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_169": {
                "id": 32858,
                "uid": "IML_PICKUP_МОСКВА_169",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "г.Егорьевск, 5микр., д.5, офис4",
                "address": "г.Егорьевск, 5микр., д.5, офис4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.377601",
                "lon": "39.050854",
                "workhours": "Пн-Пт 10:00-19:00 (без п-ва), Сб 10:00-16:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_167": {
                "id": 32856,
                "uid": "IML_PICKUP_МОСКВА_167",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "г.Лобня ул.Маяковского д.4а",
                "address": "г.Лобня ул.Маяковского д.4а",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "56.014101",
                "lon": "37.483481",
                "workhours": "Пн-Сб 10:00-20:00 (без п-ва), Вс 10:00-19:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_109": {
                "id": 20927,
                "uid": "IML_PICKUP_МОСКВА_109",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "г.Подольск,ЖД станция Силикатная,ул.Тепличная,д.7Б",
                "address": "г.Подольск,ЖД станция Силикатная,ул.Тепличная,д.7Б",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.468372",
                "lon": "37.553028",
                "workhours": "Пн-Пт 11:00-19:30 (14:00-15:00), Сб Вс Вых"
            },
            "IML_PICKUP_МОСКВА_35": {
                "id": 19540,
                "uid": "IML_PICKUP_МОСКВА_35",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "г.Подольск,Революционный проспект,д.18, пом. 2",
                "address": "г.Подольск,Революционный проспект,д.18, пом. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.432301",
                "lon": "37.558742",
                "workhours": "Пн-Пт 10:00-19:30 (без п-ва), Сб 11:00-18:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_136": {
                "id": 24694,
                "uid": "IML_PICKUP_МОСКВА_136",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "г.Фрязино пр-т Мира 18, ТЦ Чижово",
                "address": "г.Фрязино пр-т Мира 18, ТЦ Чижово",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.953901",
                "lon": "38.057280",
                "workhours": "Пн-Пт 10:00-20:00 (без п-ва), Сб-Вс 11:00-18:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_152": {
                "id": 25554,
                "uid": "IML_PICKUP_МОСКВА_152",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Заводская, 31",
                "address": "Заводская, 31",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.808189",
                "lon": "37.325674",
                "workhours": "Пн-Пт 10:00-19:00 (без п-ва), Сб,Вс Вых"
            },
            "IML_PICKUP_МОСКВА_53": {
                "id": 19518,
                "uid": "IML_PICKUP_МОСКВА_53",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Зеленоград корпус 1805",
                "address": "Зеленоград корпус 1805",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.979717",
                "lon": "37.162692",
                "workhours": "Пн-Пт 12:00-20:00 (без п-ва), Сб 10:00-18:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_29": {
                "id": 19598,
                "uid": "IML_PICKUP_МОСКВА_29",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Зеленоград, Георгиевский проспект, д.5",
                "address": "Зеленоград, Георгиевский проспект, д.5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.984257",
                "lon": "37.213484",
                "workhours": "Пн-Пт 10:00-20:00 (без п-ва), Сб 10:00-16:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_101": {
                "id": 20229,
                "uid": "IML_PICKUP_МОСКВА_101",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Зеленоград, корп.1627",
                "address": "Зеленоград, корп.1627",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.970745",
                "lon": "37.152295",
                "workhours": "Пн-Пт 10:00-20:00 (без п-ва), Сб-Вс 10:00-16:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_52": {
                "id": 19517,
                "uid": "IML_PICKUP_МОСКВА_52",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Зеленоград, корпус 900",
                "address": "Зеленоград, корпус 900",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.981424",
                "lon": "37.184297",
                "workhours": "Пн-Пт 12:30-20:30 (без п-ва), Сб Вс Вых"
            },
            "IML_PICKUP_МОСКВА_22": {
                "id": 19532,
                "uid": "IML_PICKUP_МОСКВА_22",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Комсомольская площадь, д. 6, этаж 2, пав. № 35",
                "address": "Комсомольская площадь, д. 6, этаж 2, пав. № 35",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.775864",
                "lon": "37.660413",
                "workhours": "Пн-Пт, Вс 10:00-21:30 (без п-ва), Сб 11:00-21:30 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_157": {
                "id": 25468,
                "uid": "IML_PICKUP_МОСКВА_157",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "корпус 435А",
                "address": "корпус 435А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.996572",
                "lon": "37.211134",
                "workhours": "Пн-Пт 12:00-20:00 (без п-ва), Сб 11:00-18:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_212": {
                "id": 40334,
                "uid": "IML_PICKUP_МОСКВА_212",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Красногорский бульвар, д. 17",
                "address": "Красногорский бульвар, д. 17",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.819767",
                "lon": "37.374039",
                "workhours": "ПН-ПТ 10-21, СБ 10-20, ВС 10-19"
            },
            "IML_PICKUP_МОСКВА_100": {
                "id": 20228,
                "uid": "IML_PICKUP_МОСКВА_100",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Лихачевское шоссе, д.6",
                "address": "Лихачевское шоссе, д.6",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.941027",
                "lon": "37.492750",
                "workhours": "Пн-Вс 10:00-20:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_202": {
                "id": 40324,
                "uid": "IML_PICKUP_МОСКВА_202",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Лучников переулок, д. 4, стр. 2, офис 37",
                "address": "Лучников переулок, д. 4, стр. 2, офис 37",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.758352",
                "lon": "37.631802",
                "workhours": "ПН-ПТ 10-20, СБ 10-18, ВС-выходной"
            },
            "IML_PICKUP_МОСКВА_151": {
                "id": 25200,
                "uid": "IML_PICKUP_МОСКВА_151",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "м. Нагатинская, Варшавское шоссе, д. 39",
                "address": "м. Нагатинская, Варшавское шоссе, д. 39",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.684473",
                "lon": "37.624121",
                "workhours": "Пн-Пт 11:00-20:00 (без п-ва), Сб 11:00-18:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_50": {
                "id": 19543,
                "uid": "IML_PICKUP_МОСКВА_50",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "м. Петр.-Разумовская,Коровинское шоссе,д.1, к.1",
                "address": "м. Петр.-Разумовская,Коровинское шоссе,д.1, к.1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.865207",
                "lon": "37.543452",
                "workhours": "Пн-Пт 10:00-20:00 (14:00-15:00), Сб 10:00-18:00 (14:00-15:00), Вс 10:00-15:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_49": {
                "id": 19542,
                "uid": "IML_PICKUP_МОСКВА_49",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "м. Сокол, Волоколамское шоссе, д. 1, стр. 1.",
                "address": "м. Сокол, Волоколамское шоссе, д. 1, стр. 1.",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.806727",
                "lon": "37.504830",
                "workhours": "Пн-Пт 10:00-19:00 (без п-ва), Сб 12:00-16:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_134": {
                "id": 24110,
                "uid": "IML_PICKUP_МОСКВА_134",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "м. Цветной бульвар, ул. Трубная,  д. 32, стр. 4.",
                "address": "м. Цветной бульвар, ул. Трубная,  д. 32, стр. 4.",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.771894",
                "lon": "37.626232",
                "workhours": "Пн-Пт 10:00-19:00 (14:00-15:00), Сб Вс Вых"
            },
            "IML_PICKUP_МОСКВА_168": {
                "id": 32857,
                "uid": "IML_PICKUP_МОСКВА_168",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "м.Жулебино,ул.Генерала Кузнецова,д.15,кор.1",
                "address": "м.Жулебино,ул.Генерала Кузнецова,д.15,кор.1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.686766",
                "lon": "37.858905",
                "workhours": "Пн-Пт 10:00-20:00 (без п-ва), Сб-Вс 10:00-18:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_189": {
                "id": 36695,
                "uid": "IML_PICKUP_МОСКВА_189",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "м.р-н Павлино, 15/1",
                "address": "м.р-н Павлино, 15/1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.728961",
                "lon": "37.967053",
                "workhours": "Пн-Пт 11:00-19:00 (без п-ва), Сб 11:00-16:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_105": {
                "id": 20924,
                "uid": "IML_PICKUP_МОСКВА_105",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "м.Филевский парк,ул.Большая Филевская,д.37,корп.1",
                "address": "м.Филевский парк,ул.Большая Филевская,д.37,корп.1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.742372",
                "lon": "37.479007",
                "workhours": "Пн-Пт 10:00-19:00 (14:00-15:00), Сб 10:00-17:00 (14:00-15:00), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_97": {
                "id": 19558,
                "uid": "IML_PICKUP_МОСКВА_97",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "микр. Финский, д. 3",
                "address": "микр. Финский, д. 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.938067",
                "lon": "37.973386",
                "workhours": "Пн-Сб 10:00-20:00 (без п-ва), Вс 10:00-18:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_143": {
                "id": 25194,
                "uid": "IML_PICKUP_МОСКВА_143",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "мкр. Солнцево, Солнцевский пр-кт, д. 11.",
                "address": "мкр. Солнцево, Солнцевский пр-кт, д. 11.",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.649591",
                "lon": "37.403998",
                "workhours": "Пн-Пт 10:00-20:00 (без п-ва), Сб-Вс 10:00-18:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_206": {
                "id": 40328,
                "uid": "IML_PICKUP_МОСКВА_206",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "мкр. Щелково-3, ул. Радиоцентр-5, д. 16, пом. 545",
                "address": "мкр. Щелково-3, ул. Радиоцентр-5, д. 16, пом. 545",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.899966",
                "lon": "38.065239",
                "workhours": "ПН-ПТ 10-20, СБ-ВС 11-16"
            },
            "IML_PICKUP_МОСКВА_217": {
                "id": 40339,
                "uid": "IML_PICKUP_МОСКВА_217",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Московское шоссе, д. 51",
                "address": "Московское шоссе, д. 51",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "54.939067",
                "lon": "37.404510",
                "workhours": "ПН-ПТ 10-19, СБ 11-17, ВС-выходной"
            },
            "IML_PICKUP_МОСКВА_17": {
                "id": 19529,
                "uid": "IML_PICKUP_МОСКВА_17",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Новоясеневский пр-т д.2А стр.1",
                "address": "Новоясеневский пр-т д.2А стр.1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.617886",
                "lon": "37.505906",
                "workhours": "Пн-Вс 10:00-21:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_28": {
                "id": 19640,
                "uid": "IML_PICKUP_МОСКВА_28",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Ореховый бульвар, д.14, стр. 3",
                "address": "Ореховый бульвар, д.14, стр. 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.610031",
                "lon": "37.722235",
                "workhours": "Пн-Вс 10:00-21:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_1": {
                "id": 19632,
                "uid": "IML_PICKUP_МОСКВА_1",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Палашевский Малый пер.,д.6",
                "address": "Палашевский Малый пер.,д.6",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.765200",
                "lon": "37.602705",
                "workhours": "Пн-Вс 10:00-22:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_11": {
                "id": 19523,
                "uid": "IML_PICKUP_МОСКВА_11",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Парковая 9-я ул., вл. 61А, стр.1, эт. 2-й, пав. 1.",
                "address": "Парковая 9-я ул., вл. 61А, стр.1, эт. 2-й, пав. 1.",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.809090",
                "lon": "37.798412",
                "workhours": "Пн-Вс 10:00-21:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_154": {
                "id": 25202,
                "uid": "IML_PICKUP_МОСКВА_154",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "поселение Воскресенское, д.39",
                "address": "поселение Воскресенское, д.39",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.531642",
                "lon": "37.446731",
                "workhours": "Пн, ср, пт: 11:00-19:30 (без п-ва), вт, чт, Сб, вс - вых"
            },
            "IML_PICKUP_МОСКВА_190": {
                "id": 37386,
                "uid": "IML_PICKUP_МОСКВА_190",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "поселок Коммунарка, дом 20",
                "address": "поселок Коммунарка, дом 20",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.571632",
                "lon": "37.473492",
                "workhours": "Пн-Пт 10:00-20:00 (без п-ва), Сб 10:00-18:00 (без п-ва) Вс Вых"
            },
            "IML_PICKUP_МОСКВА_42": {
                "id": 20223,
                "uid": "IML_PICKUP_МОСКВА_42",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "пр-кт Боголюбова, д. 16",
                "address": "пр-кт Боголюбова, д. 16",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "56.738710",
                "lon": "37.167063",
                "workhours": "Пн-Пт 10:00-20:00 (без п-ва), Сб-Вс 10:00-18:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_201": {
                "id": 38609,
                "uid": "IML_PICKUP_МОСКВА_201",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "пр-кт Космонавтов, 20 А, 3 эт., 316 пав-н",
                "address": "пр-кт Космонавтов, 20 А, 3 эт., 316 пав-н",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.914302",
                "lon": "37.866648",
                "workhours": "Ср-Вс 11:00-19:00 (без п-ва), Пн Вт Вых"
            },
            "IML_PICKUP_МОСКВА_227": {
                "id": 42730,
                "uid": "IML_PICKUP_МОСКВА_227",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "пр-кт Пацаева, д. 15А",
                "address": "пр-кт Пацаева, д. 15А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.948352",
                "lon": "37.494745",
                "workhours": "Пн-Пт 10:00-19:00, Сб-Вс 10:00-20:00"
            },
            "IML_PICKUP_МОСКВА_2": {
                "id": 42723,
                "uid": "IML_PICKUP_МОСКВА_2",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "пр-т Боголюбова, 44",
                "address": "пр-т Боголюбова, 44",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "56.729171",
                "lon": "37.140935",
                "workhours": "Пн-Пт 10:00-19:00 (без п-ва); Сб 10:00-14:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_130": {
                "id": 21860,
                "uid": "IML_PICKUP_МОСКВА_130",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "пр-т Ленина, 10 А",
                "address": "пр-т Ленина, 10 А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.796181",
                "lon": "37.939054",
                "workhours": "Пн-Пт 11:00-19:00 (без п-ва), Сб 11:00-16:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_38": {
                "id": 19563,
                "uid": "IML_PICKUP_МОСКВА_38",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "пр. Красной Армии д.91Б ком.10",
                "address": "пр. Красной Армии д.91Б ком.10",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "56.301983",
                "lon": "38.128983",
                "workhours": "Пн-Пт 10:00-19:00 (без п-ва), Сб 10:00-15:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_8": {
                "id": 19644,
                "uid": "IML_PICKUP_МОСКВА_8",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "пр.Вернадского д.86Б стр.1",
                "address": "пр.Вернадского д.86Б стр.1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.662273",
                "lon": "37.480301",
                "workhours": "Пн-Вс 10:00-20:30 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_95": {
                "id": 19557,
                "uid": "IML_PICKUP_МОСКВА_95",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Пролетарский пр-т, 4/1 оф.4",
                "address": "Пролетарский пр-т, 4/1 оф.4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.914978",
                "lon": "37.995880",
                "workhours": "Пн-Пт 09:00-21:00 (без п-ва), Сб-Вс 10:00-19:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_18": {
                "id": 19530,
                "uid": "IML_PICKUP_МОСКВА_18",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Проспект Андропова д. 36., пав. № 9",
                "address": "Проспект Андропова д. 36., пав. № 9",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.675844",
                "lon": "37.661841",
                "workhours": "Пн-Вс 10:00-22:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_186": {
                "id": 34742,
                "uid": "IML_PICKUP_МОСКВА_186",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "проспект Гагарина, д. 17/7, пом. 19",
                "address": "проспект Гагарина, д. 17/7, пом. 19",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.690364",
                "lon": "37.911986",
                "workhours": "Пн-Пт 10:00-20:00 (без п-ва), Сб 11:00-16:00 (без п-ва); Вс Вых"
            },
            "IML_PICKUP_МОСКВА_210": {
                "id": 40332,
                "uid": "IML_PICKUP_МОСКВА_210",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "р.п. Нахабино, ул. Школьная, д. 2",
                "address": "р.п. Нахабино, ул. Школьная, д. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.841807",
                "lon": "37.145804",
                "workhours": "ПН-ВС 10-20:30"
            },
            "IML_PICKUP_МОСКВА_13": {
                "id": 19634,
                "uid": "IML_PICKUP_МОСКВА_13",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Рязанский пр-т вл.101Б",
                "address": "Рязанский пр-т вл.101Б",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.714445",
                "lon": "37.819900",
                "workhours": "Пн-Вс 10:00-21:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_12": {
                "id": 19633,
                "uid": "IML_PICKUP_МОСКВА_12",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Свободный пр-т д.33А",
                "address": "Свободный пр-т д.33А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.752683",
                "lon": "37.818283",
                "workhours": "Пн-Пт 10:00-20:30 (без п-ва), Сб-Вс 10:00-19:30 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_181": {
                "id": 34737,
                "uid": "IML_PICKUP_МОСКВА_181",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Советская площадь, д. 4",
                "address": "Советская площадь, д. 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.062096",
                "lon": "38.756719",
                "workhours": "Пн-Пт 10:00-18:30 (без п-ва), Сб 11:00-15:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_146": {
                "id": 25197,
                "uid": "IML_PICKUP_МОСКВА_146",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Сосенское пос,ул. Александры Монаховой, д. 97",
                "address": "Сосенское пос,ул. Александры Монаховой, д. 97",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.541132",
                "lon": "37.490057",
                "workhours": "Пн-Пт 10:00-20:00 (без п-ва), Сб-Вс 10:00-18:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_40": {
                "id": 19568,
                "uid": "IML_PICKUP_МОСКВА_40",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Ступино, ул. Первомайская, д.18А",
                "address": "Ступино, ул. Первомайская, д.18А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "54.886880",
                "lon": "38.078390",
                "workhours": "Пн-Пт 09:00-21:00 (пер.15:00-16:00), Сб 10:00-18:00 (без п-ва), Вс 10:00-15:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_122": {
                "id": 24319,
                "uid": "IML_PICKUP_МОСКВА_122",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул Зубовский бульвар дом 16-20",
                "address": "ул Зубовский бульвар дом 16-20",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.738391",
                "lon": "37.588090",
                "workhours": "Пн-Пт 11:00-20:00 (без п-ва), Сб Вс Вых"
            },
            "IML_PICKUP_МОСКВА_124": {
                "id": 24320,
                "uid": "IML_PICKUP_МОСКВА_124",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул Кировоградская 22 г",
                "address": "ул Кировоградская 22 г",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.614821",
                "lon": "37.604552",
                "workhours": "Пн-Вс 10:00-21:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_119": {
                "id": 24316,
                "uid": "IML_PICKUP_МОСКВА_119",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул Розанова 4",
                "address": "ул Розанова 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.772913",
                "lon": "37.542683",
                "workhours": "Пн-Пт 11:00-20:00 (без п-ва), Сб Вс Вых"
            },
            "IML_PICKUP_МОСКВА_21": {
                "id": 19531,
                "uid": "IML_PICKUP_МОСКВА_21",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. 1-я Останкинская, д. 55",
                "address": "ул. 1-я Останкинская, д. 55",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.824005",
                "lon": "37.634434",
                "workhours": "Пн-Вс 10:00-22:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_205": {
                "id": 40327,
                "uid": "IML_PICKUP_МОСКВА_205",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. 40 лет Октября, д. 15/1",
                "address": "ул. 40 лет Октября, д. 15/1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.512036",
                "lon": "37.573663",
                "workhours": "Пн-Пт 09:00-21:00(без п-ва), Сб-Вс 10:00-18:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_117": {
                "id": 24314,
                "uid": "IML_PICKUP_МОСКВА_117",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Адмирала Лазарева, д.63 к.1",
                "address": "ул. Адмирала Лазарева, д.63 к.1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.537538",
                "lon": "37.505997",
                "workhours": "Пн-Пт 11:00-20:00 (без п-ва), Сб Вс Вых"
            },
            "IML_PICKUP_МОСКВА_128": {
                "id": 21859,
                "uid": "IML_PICKUP_МОСКВА_128",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Андреевка, вл. 13, стр. 2",
                "address": "ул. Андреевка, вл. 13, стр. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.981898",
                "lon": "37.146801",
                "workhours": "Пн-Пт 12:00-20:00 (без п-ва), Сб 11:00-18:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_160": {
                "id": 25469,
                "uid": "IML_PICKUP_МОСКВА_160",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Банковская, д.6, 1 этаж",
                "address": "ул. Банковская, д.6, 1 этаж",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "56.188257",
                "lon": "36.980191",
                "workhours": "Пн-Сб 09:00-20:00 (без п-ва), Вс 09:00-18:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_226": {
                "id": 42729,
                "uid": "IML_PICKUP_МОСКВА_226",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Белобородова, д. 3",
                "address": "ул. Белобородова, д. 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.923360",
                "lon": "37.753461",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-18:00, Вых Вс"
            },
            "IML_PICKUP_МОСКВА_218": {
                "id": 40340,
                "uid": "IML_PICKUP_МОСКВА_218",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Бирюлевская, д. 9",
                "address": "ул. Бирюлевская, д. 9",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.598512",
                "lon": "37.663854",
                "workhours": "ПН-ПТ 10-20, СБ 10-18, ВС 10-15"
            },
            "IML_PICKUP_МОСКВА_200": {
                "id": 37392,
                "uid": "IML_PICKUP_МОСКВА_200",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Ул. Большая Пионерская, д.4",
                "address": "Ул. Большая Пионерская, д.4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.729458",
                "lon": "37.634029",
                "workhours": "Пн-Пт 10:00-20:00 (без п-ва), Сб-Вс 10:00-18:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_173": {
                "id": 33972,
                "uid": "IML_PICKUP_МОСКВА_173",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Борисовские пруды, д. 10, кор. 4",
                "address": "ул. Борисовские пруды, д. 10, кор. 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.633025",
                "lon": "37.738596",
                "workhours": "Пн-Пт 10:00-20:00 (без п-ва), Сб-Вс 10:00-18:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_127": {
                "id": 24322,
                "uid": "IML_PICKUP_МОСКВА_127",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Брянская, д. 2",
                "address": "ул. Брянская, д. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.744389",
                "lon": "37.563853",
                "workhours": "Пн-Вс 10:00-21:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_108": {
                "id": 20926,
                "uid": "IML_PICKUP_МОСКВА_108",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Булатниковская, д.6А.",
                "address": "ул. Булатниковская, д.6А.",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.580981",
                "lon": "37.644998",
                "workhours": "Пн-Пт 10:00-20:00 (14:00-15:00), Сб 10:00-18:00 (14:00-15:00), Вс 10:00-15:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_184": {
                "id": 34740,
                "uid": "IML_PICKUP_МОСКВА_184",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Войкова, д. 3",
                "address": "ул. Войкова, д. 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.385720",
                "lon": "36.744742",
                "workhours": "Пн-Пт 10:00-20:00 (без п-ва), Сб 10:00-14:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_215": {
                "id": 40337,
                "uid": "IML_PICKUP_МОСКВА_215",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Вокзальная, д. 27",
                "address": "ул. Вокзальная, д. 27",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.963385",
                "lon": "38.038936",
                "workhours": "ПН-ПТ 10-20, СБ-ВС 10-18"
            },
            "IML_PICKUP_МОСКВА_148": {
                "id": 25198,
                "uid": "IML_PICKUP_МОСКВА_148",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Ворошилова, 140",
                "address": "ул. Ворошилова, 140",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "54.923631",
                "lon": "37.444341",
                "workhours": "Пн-Сб 10:00-19:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_141": {
                "id": 24803,
                "uid": "IML_PICKUP_МОСКВА_141",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Горького, д. 1А",
                "address": "ул. Горького, д. 1А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "54.922633",
                "lon": "37.438529",
                "workhours": "Пн-Пт 10:00-18:00 (без п-ва), Сб 10:00-17:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_174": {
                "id": 33973,
                "uid": "IML_PICKUP_МОСКВА_174",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Гурьянова, д. 30",
                "address": "ул. Гурьянова, д. 30",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.680707",
                "lon": "37.715821",
                "workhours": "Пн-Пт 10:00-20:00 (без п-ва), Сб-Вс 10:00-18:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_156": {
                "id": 25467,
                "uid": "IML_PICKUP_МОСКВА_156",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Звездная, д.8 а",
                "address": "ул. Звездная, д.8 а",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.826095",
                "lon": "37.955138",
                "workhours": "Пн-Пт 11:00-19:00 (без п-ва), Сб 11:00-16:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_110": {
                "id": 21253,
                "uid": "IML_PICKUP_МОСКВА_110",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Зеленая, д. 32",
                "address": "ул. Зеленая, д. 32",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.808205",
                "lon": "37.942832",
                "workhours": "Пн-Вс 10:30-20:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_211": {
                "id": 40333,
                "uid": "IML_PICKUP_МОСКВА_211",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Колонцова, д. 5, пав. 20",
                "address": "ул. Колонцова, д. 5, пав. 20",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.915094",
                "lon": "37.764474",
                "workhours": "ПН-ВС 10-20"
            },
            "IML_PICKUP_МОСКВА_214": {
                "id": 40336,
                "uid": "IML_PICKUP_МОСКВА_214",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Комсомольская, д. 5",
                "address": "ул. Комсомольская, д. 5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.929182",
                "lon": "37.993706",
                "workhours": "ПН-ПТ 10-20, СБ-ВС 10-18"
            },
            "IML_PICKUP_МОСКВА_193": {
                "id": 37460,
                "uid": "IML_PICKUP_МОСКВА_193",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Кооперативная, вл. 7-8",
                "address": "ул. Кооперативная, вл. 7-8",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.119342",
                "lon": "37.956983",
                "workhours": "Пн-Пт 10:00-19:00 (без п-ва), Сб 10:00-15:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_223": {
                "id": 42726,
                "uid": "IML_PICKUP_МОСКВА_223",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Коптевская, д. 26, кор. 5",
                "address": "ул. Коптевская, д. 26, кор. 5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.834710",
                "lon": "37.522863",
                "workhours": "Пн-Пт 10:00-20:00, Сб-Вс 10:00-16:00"
            },
            "IML_PICKUP_МОСКВА_176": {
                "id": 33974,
                "uid": "IML_PICKUP_МОСКВА_176",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Корнея Чуковского, д. 5",
                "address": "ул. Корнея Чуковского, д. 5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.637176",
                "lon": "37.328065",
                "workhours": "Пн-Пт 10:00-19:30 (без п-ва), Сб 10:00-16:30 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_54": {
                "id": 19566,
                "uid": "IML_PICKUP_МОСКВА_54",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Красная, д. 66",
                "address": "ул. Красная, д. 66",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "56.185813",
                "lon": "36.982014",
                "workhours": "Пн-Пт 12:00-20:00 (без п-ва), Сб 11:00-18:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_192": {
                "id": 37388,
                "uid": "IML_PICKUP_МОСКВА_192",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Кутузовская, дом 9",
                "address": "ул. Кутузовская, дом 9",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.703272",
                "lon": "37.326047",
                "workhours": "Пн-Вс 10:00-20:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_132": {
                "id": 21862,
                "uid": "IML_PICKUP_МОСКВА_132",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Ленина, 1а",
                "address": "ул. Ленина, 1а",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.753321",
                "lon": "37.857773",
                "workhours": "Пн-Пт 10:00-21:00 (13:00-14:00), Сб 11:00-20:00 (13:00-14:00), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_209": {
                "id": 40331,
                "uid": "IML_PICKUP_МОСКВА_209",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Лермонтова, д. 1",
                "address": "ул. Лермонтова, д. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.320125",
                "lon": "38.668233",
                "workhours": "ПН-ПТ 11-20, СБ 11-18, ВС-выходной"
            },
            "IML_PICKUP_МОСКВА_51": {
                "id": 19544,
                "uid": "IML_PICKUP_МОСКВА_51",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Лукинская, д.8",
                "address": "ул. Лукинская, д.8",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.650409",
                "lon": "37.343362",
                "workhours": "Пн-Пт 11:00-20:00 (без п-ва), Сб, Вс Вых"
            },
            "IML_PICKUP_МОСКВА_144": {
                "id": 25195,
                "uid": "IML_PICKUP_МОСКВА_144",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Люблинская, 27/2",
                "address": "ул. Люблинская, 27/2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.700694",
                "lon": "37.733419",
                "workhours": "Пн-Пт 12:00-19:00 (без п-ва), Сб 12:00-17:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_106": {
                "id": 20925,
                "uid": "IML_PICKUP_МОСКВА_106",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Малая Калитниковская, д. 7",
                "address": "ул. Малая Калитниковская, д. 7",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.732514",
                "lon": "37.675154",
                "workhours": "Пн-Пт 10:00-19:00 (14:00-15:00), Сб 10:00-17:00 (14:00-15:00), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_142": {
                "id": 25193,
                "uid": "IML_PICKUP_МОСКВА_142",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Мясницкая, д. 17, стр. 2, оф. 2",
                "address": "ул. Мясницкая, д. 17, стр. 2, оф. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.765468",
                "lon": "37.638719",
                "workhours": "Пн-Пт 11:00-19:00 (15:00-15:40), Сб Вс Вых"
            },
            "IML_PICKUP_МОСКВА_196": {
                "id": 37390,
                "uid": "IML_PICKUP_МОСКВА_196",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Новокосинская, д. 10, к. 1",
                "address": "ул. Новокосинская, д. 10, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.734679",
                "lon": "37.855590",
                "workhours": "Пн-Пт 10:00-20:00 (без п-ва), Сб 10:00-18:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_182": {
                "id": 34738,
                "uid": "IML_PICKUP_МОСКВА_182",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Островского, д. 38",
                "address": "ул. Островского, д. 38",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.540918",
                "lon": "37.082823",
                "workhours": "Пн-Пт 10:30-19:00 (без п-ва), Сб 10:30-16:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_188": {
                "id": 34744,
                "uid": "IML_PICKUP_МОСКВА_188",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Панфёрова д.16. корп.1",
                "address": "ул. Панфёрова д.16. корп.1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.681138",
                "lon": "37.544935",
                "workhours": "Пн-Пт 10:00-19:00 (14:00-15:00), Сб 10:00-17:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_222": {
                "id": 42725,
                "uid": "IML_PICKUP_МОСКВА_222",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Паустовского, д. 8, кор. 1",
                "address": "ул. Паустовского, д. 8, кор. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.599030",
                "lon": "37.538269",
                "workhours": "Пн-Пт 10:00-19:00 (пер 14:00-15:00), Сб 10:00-17:00, Вых Вс"
            },
            "IML_PICKUP_МОСКВА_191": {
                "id": 37387,
                "uid": "IML_PICKUP_МОСКВА_191",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Пржевальского, дом 2",
                "address": "ул. Пржевальского, дом 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.681758",
                "lon": "37.452144",
                "workhours": "Пн-Вс 10:00-20:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_27": {
                "id": 19639,
                "uid": "IML_PICKUP_МОСКВА_27",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Пятницкая, дом 3/4 стр. 2",
                "address": "ул. Пятницкая, дом 3/4 стр. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.745493",
                "lon": "37.627741",
                "workhours": "Пн-Вс 10:00-20:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_185": {
                "id": 34741,
                "uid": "IML_PICKUP_МОСКВА_185",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Рождественская, д. 23/33",
                "address": "ул. Рождественская, д. 23/33",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.705488",
                "lon": "37.920736",
                "workhours": "Пн-Пт 11:00-20:00 (без п-ва), Сб 11:00-16:00 (без п-ва); Вс Вых"
            },
            "IML_PICKUP_МОСКВА_116": {
                "id": 24313,
                "uid": "IML_PICKUP_МОСКВА_116",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Россошанская, 3к1Ас2",
                "address": "ул. Россошанская, 3к1Ас2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.595443",
                "lon": "37.606090",
                "workhours": "Пн-Сб 10:00-21:00 (без п-ва), Вс 10:00-20:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_213": {
                "id": 40335,
                "uid": "IML_PICKUP_МОСКВА_213",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Свердлова, д. 16/5",
                "address": "ул. Свердлова, д. 16/5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.821897",
                "lon": "37.969685",
                "workhours": "ПН-ПТ 10-20, СБ-ВС 10-18"
            },
            "IML_PICKUP_МОСКВА_30": {
                "id": 19645,
                "uid": "IML_PICKUP_МОСКВА_30",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Семашко ул. 35д., оф.19",
                "address": "ул. Семашко ул. 35д., оф.19",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.891891",
                "lon": "37.726036",
                "workhours": "Пн-Пт 09:00-21:00 (без п-ва), Сб-Вс 10:00-19:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_133": {
                "id": 21863,
                "uid": "IML_PICKUP_МОСКВА_133",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Советская, д.2/9, пом.76",
                "address": "ул. Советская, д.2/9, пом.76",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.795408",
                "lon": "37.934741",
                "workhours": "Пн-Пт 10:00-18:00 (без п-ва), Сб 10:00-17:00 (без п-ва), Вс 10:00-15:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_153": {
                "id": 25201,
                "uid": "IML_PICKUP_МОСКВА_153",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Сущевский Вал, д.5 стр.20, пав. Т-1",
                "address": "ул. Сущевский Вал, д.5 стр.20, пав. Т-1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.793930",
                "lon": "37.593345",
                "workhours": "Пн-Сб 10:00-20:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_121": {
                "id": 24318,
                "uid": "IML_PICKUP_МОСКВА_121",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Толбухина д.13, к.2",
                "address": "ул. Толбухина д.13, к.2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.723379",
                "lon": "37.398419",
                "workhours": "Пн-Пт 11:00-20:00 (без п-ва), Сб Вс Вых"
            },
            "IML_PICKUP_МОСКВА_44": {
                "id": 20240,
                "uid": "IML_PICKUP_МОСКВА_44",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Третьего Интернационала, 62",
                "address": "ул. Третьего Интернационала, 62",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.855906",
                "lon": "38.441471",
                "workhours": "Пн-Вс 10:00-21:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_225": {
                "id": 42728,
                "uid": "IML_PICKUP_МОСКВА_225",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Удальцова, д. 10",
                "address": "ул. Удальцова, д. 10",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.674630",
                "lon": "37.513444",
                "workhours": "Пн-Пт 10:00-20:00, Сб-Вс 10:00-16:00"
            },
            "IML_PICKUP_МОСКВА_194": {
                "id": 37389,
                "uid": "IML_PICKUP_МОСКВА_194",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Успенская, дом 32",
                "address": "ул. Успенская, дом 32",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.835552",
                "lon": "37.293248",
                "workhours": "Пн-Пт 10:00-20:00 (без п-ва), Сб 10:00-17:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_195": {
                "id": 37461,
                "uid": "IML_PICKUP_МОСКВА_195",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Фридриха Энгельса, д. 58, стр. 2",
                "address": "ул. Фридриха Энгельса, д. 58, стр. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.776958",
                "lon": "37.692663",
                "workhours": "Пн-Пт 10:00-19:00 (без п-ва), Сб 10:00-16:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_199": {
                "id": 37391,
                "uid": "IML_PICKUP_МОСКВА_199",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Хлебозаводская, дом 31/1",
                "address": "ул. Хлебозаводская, дом 31/1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.970831",
                "lon": "37.930824",
                "workhours": "Пн-Пт 10:00-20:00 (без п-ва), Сб 11:00-18:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_149": {
                "id": 25199,
                "uid": "IML_PICKUP_МОСКВА_149",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Центральная, д.25",
                "address": "ул. Центральная, д.25",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.919594",
                "lon": "37.981336",
                "workhours": "Пн-Пт 10:00-19:00 (без п-ва), Сб 10:00-18:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_221": {
                "id": 42724,
                "uid": "IML_PICKUP_МОСКВА_221",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Шарикоподшипниковская, д. 13, стр. 2",
                "address": "ул. Шарикоподшипниковская, д. 13, стр. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.717970",
                "lon": "37.677453",
                "workhours": "Пн-Пт 11:00-20:00 (без п-ва), Сб Вс Вых"
            },
            "IML_PICKUP_МОСКВА_216": {
                "id": 40338,
                "uid": "IML_PICKUP_МОСКВА_216",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул. Юбилейная, д. 36",
                "address": "ул. Юбилейная, д. 36",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.905755",
                "lon": "37.706955",
                "workhours": "ПН-ПТ 10-20, СБ-ВС 11-18"
            },
            "IML_PICKUP_МОСКВА_41": {
                "id": 19513,
                "uid": "IML_PICKUP_МОСКВА_41",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул.Аверьянова,д. 17(магазин Школьник)",
                "address": "ул.Аверьянова,д. 17(магазин Школьник)",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "56.351086",
                "lon": "37.530139",
                "workhours": "Пн-Сб 10:00-20:00 (без п-ва), Вс 10:00-18:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_4": {
                "id": 19641,
                "uid": "IML_PICKUP_МОСКВА_4",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул.Братиславская,д.16 корпус 2",
                "address": "ул.Братиславская,д.16 корпус 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.658469",
                "lon": "37.757467",
                "workhours": "Пн-Вс 10:00-20:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_31": {
                "id": 19597,
                "uid": "IML_PICKUP_МОСКВА_31",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул.Гагарина, д.85 оф.22",
                "address": "ул.Гагарина, д.85 оф.22",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.610041",
                "lon": "38.073764",
                "workhours": "Пн-Пт 09:00-21:00 (без п-ва), Сб-Вс 09:00-18:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_16": {
                "id": 19637,
                "uid": "IML_PICKUP_МОСКВА_16",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул.Героев Панфиловцев д.1А",
                "address": "ул.Героев Панфиловцев д.1А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.851561",
                "lon": "37.438224",
                "workhours": "Пн-Сб 10:00-21:00 (без п-ва), Вс 10:00-20:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_102": {
                "id": 20230,
                "uid": "IML_PICKUP_МОСКВА_102",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул.Ленина, д.45/20, оф.111",
                "address": "ул.Ленина, д.45/20, оф.111",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "56.337377",
                "lon": "36.724746",
                "workhours": "Пн-Пт 09:00-18:00 (без п-ва), Сб Вс Вых"
            },
            "IML_PICKUP_МОСКВА_6": {
                "id": 19643,
                "uid": "IML_PICKUP_МОСКВА_6",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул.Линии Октябрьской Железной Дороги, д.2,стр.2",
                "address": "ул.Линии Октябрьской Железной Дороги, д.2,стр.2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.838800",
                "lon": "37.572000",
                "workhours": "Пн-Вс 10:00-21:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_15": {
                "id": 19636,
                "uid": "IML_PICKUP_МОСКВА_15",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул.Митинская д.42",
                "address": "ул.Митинская д.42",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.848246",
                "lon": "37.359594",
                "workhours": "Пн-Сб 10:00-21:00 (без п-ва), Вс 10:00-20:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_5": {
                "id": 19642,
                "uid": "IML_PICKUP_МОСКВА_5",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул.Профсоюзная,д.45",
                "address": "ул.Профсоюзная,д.45",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.666599",
                "lon": "37.552148",
                "workhours": "Пн-Вс 10:00-20:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_14": {
                "id": 19635,
                "uid": "IML_PICKUP_МОСКВА_14",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул.Смольная д.24Г",
                "address": "ул.Смольная д.24Г",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.860543",
                "lon": "37.483164",
                "workhours": "Пн-Пт, Вс 10:00-21:30 (без п-ва), Сб 11:00-21:30 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_47": {
                "id": 19570,
                "uid": "IML_PICKUP_МОСКВА_47",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул.Советская , д. 12/1",
                "address": "ул.Советская , д. 12/1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.790469",
                "lon": "38.437411",
                "workhours": "Пн-Сб 10:00-20:00 (без п-ва), Вс Вых"
            },
            "IML_PICKUP_МОСКВА_24": {
                "id": 19533,
                "uid": "IML_PICKUP_МОСКВА_24",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "ул.Старокачаловская д.5А",
                "address": "ул.Старокачаловская д.5А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.569846",
                "lon": "37.579044",
                "workhours": "Пн-Вс 10:00-22:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_118": {
                "id": 24315,
                "uid": "IML_PICKUP_МОСКВА_118",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Чечерский проезд, д. 8",
                "address": "Чечерский проезд, д. 8",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.542054",
                "lon": "37.544575",
                "workhours": "Пн-Вс 10:00-21:00 (без п-ва)"
            },
            "IML_PICKUP_МОСКВА_207": {
                "id": 40329,
                "uid": "IML_PICKUP_МОСКВА_207",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Электролитный проезд, д. 16, корп. 1, офис 2",
                "address": "Электролитный проезд, д. 16, корп. 1, офис 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.672219",
                "lon": "37.611518",
                "workhours": "ПН-ПТ 10-20, СБ 10-17, ВС-выходной"
            },
            "IML_PICKUP_МОСКВА_224": {
                "id": 42727,
                "uid": "IML_PICKUP_МОСКВА_224",
                "isNumericCode": false,
                "agent": 3102730,
                "price": 200,
                "title": "Юбилейный пр-кт, д. 60",
                "address": "Юбилейный пр-кт, д. 60",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.889040",
                "lon": "37.414239",
                "workhours": "Пн-Пт 10:00-19:00, Сб 10:00-18:00, Вс 10:00-14:00"
            }
        },
        "paymentTypes": [20, 2, 9, 25, 39, 45, 48],
        "price": 200,
        "pickupDatetime": {
            "date": "2018-09-06 18:57:00.000000",
            "timezone_type": 3,
            "timezone": "Europe/Moscow"
        },
        "pickupDatetimeTimestamp": 1536249420,
        "pickupTitle": "Дата поступления 6 сентября"
    },
    "sdek": {
        "title": "Пункт выдачи СДЭК",
        "uid": "sdek",
        "code": "PICKUP_SDEK",
        "points": {
            "PICKUP_SDEK_MSK159": {
                "id": 40414,
                "uid": "PICKUP_SDEK_MSK159",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "1-й Варшавский проезд, 2 стр. 9а",
                "address": "1-й Варшавский проезд, 2 стр. 9а",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.644371",
                "lon": "37.626283",
                "workhours": "Пн-Пт 09:00-21:00, Сб-Вс 10:00-18:00"
            },
            "PICKUP_SDEK_MSK26": {
                "id": 8276,
                "uid": "PICKUP_SDEK_MSK26",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "б-р Кронштадтский, 7а, Цокольный этаж, 1",
                "address": "б-р Кронштадтский, 7а, Цокольный этаж, 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.842514",
                "lon": "37.486903",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK7": {
                "id": 1921,
                "uid": "PICKUP_SDEK_MSK7",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "б-р Мячковский, 11",
                "address": "б-р Мячковский, 11",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.6549377",
                "lon": "37.7563515",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK142": {
                "id": 37763,
                "uid": "PICKUP_SDEK_MSK142",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "Большой Николопесковский пер., .13 под.3, 2",
                "address": "Большой Николопесковский пер., .13 под.3, 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.750925",
                "lon": "37.589698",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK164": {
                "id": 40725,
                "uid": "PICKUP_SDEK_MSK164",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "Будайский проезд, 1",
                "address": "Будайский проезд, 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.834805",
                "lon": "37.664967",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK155": {
                "id": 38766,
                "uid": "PICKUP_SDEK_MSK155",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "Дмитровское ш., 85",
                "address": "Дмитровское ш., 85",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.86257",
                "lon": "37.548484",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK137": {
                "id": 37247,
                "uid": "PICKUP_SDEK_MSK137",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "Кутузовский пр-т, 30, пом.540",
                "address": "Кутузовский пр-т, 30, пом.540",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.741475",
                "lon": "37.535826",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK160": {
                "id": 40468,
                "uid": "PICKUP_SDEK_MSK160",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "Малый Левшинский пер., 5",
                "address": "Малый Левшинский пер., 5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.741206",
                "lon": "37.588916",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK55": {
                "id": 19795,
                "uid": "PICKUP_SDEK_MSK55",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "наб. Фрунзенская, 30, стр.2",
                "address": "наб. Фрунзенская, 30, стр.2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.7256432",
                "lon": "37.5835075",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK103": {
                "id": 34943,
                "uid": "PICKUP_SDEK_MSK103",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "Нагорный б-р, 5, корп. 2, пом. 4",
                "address": "Нагорный б-р, 5, корп. 2, пом. 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.674352",
                "lon": "37.594189",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK158": {
                "id": 40272,
                "uid": "PICKUP_SDEK_MSK158",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "Новокуркинское ш., 31",
                "address": "Новокуркинское ш., 31",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.900749",
                "lon": "37.395931",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK107": {
                "id": 34995,
                "uid": "PICKUP_SDEK_MSK107",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "пер. 1-й Волконский, 15",
                "address": "пер. 1-й Волконский, 15",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.774798",
                "lon": "37.617484",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK15": {
                "id": 3186,
                "uid": "PICKUP_SDEK_MSK15",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "пер. 2-й Вражский, 5",
                "address": "пер. 2-й Вражский, 5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.738884",
                "lon": "37.5720558",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK53": {
                "id": 19774,
                "uid": "PICKUP_SDEK_MSK53",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "пер. Большой Сухаревский, 15, стр.2",
                "address": "пер. Большой Сухаревский, 15, стр.2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.7710152",
                "lon": "37.6285667",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK96": {
                "id": 35065,
                "uid": "PICKUP_SDEK_MSK96",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "пер. Костомаровский, 3 стр.4",
                "address": "пер. Костомаровский, 3 стр.4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.751857",
                "lon": "37.666629",
                "workhours": "Пн-Вс 10:00-20:00"
            },
            "PICKUP_SDEK_MSK16": {
                "id": 4383,
                "uid": "PICKUP_SDEK_MSK16",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "пер. Лучников, 4/2",
                "address": "пер. Лучников, 4/2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.7583504",
                "lon": "37.6318016",
                "workhours": "Пн-Пт 10:00-20:00, Сб-Вс 10:00-18:00"
            },
            "PICKUP_SDEK_MSK14": {
                "id": 3862,
                "uid": "PICKUP_SDEK_MSK14",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "пер. Настасьинский, 8, стр.2, 10",
                "address": "пер. Настасьинский, 8, стр.2, 10",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.7671967",
                "lon": "37.6054993",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK32": {
                "id": 9833,
                "uid": "PICKUP_SDEK_MSK32",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "пер. Плетешковский, 7-9, стр.1",
                "address": "пер. Плетешковский, 7-9, стр.1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.7707825",
                "lon": "37.6765289",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-18:00, Вс 10:00-17:00"
            },
            "PICKUP_SDEK_MSK77": {
                "id": 25606,
                "uid": "PICKUP_SDEK_MSK77",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "пер. Товарищеский , 1, стр. 2",
                "address": "пер. Товарищеский , 1, стр. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.741577",
                "lon": "37.657844",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-18:00, Вс 10:00-17:00"
            },
            "PICKUP_SDEK_MSK88": {
                "id": 35063,
                "uid": "PICKUP_SDEK_MSK88",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "пл. Комсомольская, 1а, стр.1",
                "address": "пл. Комсомольская, 1а, стр.1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.776669",
                "lon": "37.653011",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-18:00, Вс 10:00-18:00"
            },
            "PICKUP_SDEK_MSK29": {
                "id": 9185,
                "uid": "PICKUP_SDEK_MSK29",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "пр-т Андропова, 15",
                "address": "пр-т Андропова, 15",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.6830635",
                "lon": "37.6662064",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK27": {
                "id": 8529,
                "uid": "PICKUP_SDEK_MSK27",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "пр-т Балаклавский, 5",
                "address": "пр-т Балаклавский, 5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.640419",
                "lon": "37.61026",
                "workhours": "Пн-Пт 10:00-20:00, Сб-Вс 10:00-18:00"
            },
            "PICKUP_SDEK_MSK3": {
                "id": 3,
                "uid": "PICKUP_SDEK_MSK3",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "пр-т Ленинградский, 2",
                "address": "пр-т Ленинградский, 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.7786293",
                "lon": "37.5827446",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK46": {
                "id": 13756,
                "uid": "PICKUP_SDEK_MSK46",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "пр-т Ленинградский, 75а",
                "address": "пр-т Ленинградский, 75а",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.8032913",
                "lon": "37.5122719",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK122": {
                "id": 35909,
                "uid": "PICKUP_SDEK_MSK122",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "пр-т Ленинский, 71",
                "address": "пр-т Ленинский, 71",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.688954",
                "lon": "37.549759",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK85": {
                "id": 35133,
                "uid": "PICKUP_SDEK_MSK85",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "пр-т Новоясеневский , 1Б, стр. 4 павильон  А-3",
                "address": "пр-т Новоясеневский , 1Б, стр. 4 павильон  А-3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.617315",
                "lon": "37.510816",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK33": {
                "id": 9873,
                "uid": "PICKUP_SDEK_MSK33",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "пр. 1-й Магистральный, 12, стр.1",
                "address": "пр. 1-й Магистральный, 12, стр.1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.764431",
                "lon": "37.5239677",
                "workhours": "Пн-Пт 09:00-21:00, Сб-Вс 10:00-18:00"
            },
            "PICKUP_SDEK_MSK66": {
                "id": 24453,
                "uid": "PICKUP_SDEK_MSK66",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "пр. 1-й Магистральный, 12, стр.1",
                "address": "пр. 1-й Магистральный, 12, стр.1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.763685",
                "lon": "37.527525",
                "workhours": "Пн-Пт 08:00-09:00"
            },
            "PICKUP_SDEK_MSK37": {
                "id": 10705,
                "uid": "PICKUP_SDEK_MSK37",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "пр. 6-й Рощинский, 1, стр.4",
                "address": "пр. 6-й Рощинский, 1, стр.4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.7081947",
                "lon": "37.6156883",
                "workhours": "Пн-Пт 09:00-21:00, Сб-Вс 10:00-16:00"
            },
            "PICKUP_SDEK_MSK167": {
                "id": 42375,
                "uid": "PICKUP_SDEK_MSK167",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "пр. Дмитровский, 4",
                "address": "пр. Дмитровский, 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.810024",
                "lon": "37.578198",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK169": {
                "id": 42706,
                "uid": "PICKUP_SDEK_MSK169",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "пр. Симферопольский, 18",
                "address": "пр. Симферопольский, 18",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.665162",
                "lon": "37.613952",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK166": {
                "id": 42268,
                "uid": "PICKUP_SDEK_MSK166",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "пр. Старопетровкий, 1 строение 2, пав. 47 (ТЦ Baby Store)",
                "address": "пр. Старопетровкий, 1 строение 2, пав. 47 (ТЦ Baby Store)",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.823687",
                "lon": "37.500944",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK86": {
                "id": 35186,
                "uid": "PICKUP_SDEK_MSK86",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "пр. Студеный, 4, корп.1",
                "address": "пр. Студеный, 4, корп.1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.893405",
                "lon": "37.655679",
                "workhours": "Пн-Пт 10:00-20:00, Сб-Вс 10:00-17:00"
            },
            "PICKUP_SDEK_MSK131": {
                "id": 36934,
                "uid": "PICKUP_SDEK_MSK131",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "пр. Черепановых, 36",
                "address": "пр. Черепановых, 36",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.840633",
                "lon": "37.530178",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK145": {
                "id": 38052,
                "uid": "PICKUP_SDEK_MSK145",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "пр. Черепановых, 74",
                "address": "пр. Черепановых, 74",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.846325",
                "lon": "37.55619",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK71": {
                "id": 25240,
                "uid": "PICKUP_SDEK_MSK71",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "пр. Юрловский, 14, корп.4, 11",
                "address": "пр. Юрловский, 14, корп.4, 11",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.879169",
                "lon": "37.611214",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK161": {
                "id": 40487,
                "uid": "PICKUP_SDEK_MSK161",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "пр. Якушкина, 10",
                "address": "пр. Якушкина, 10",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.860686",
                "lon": "37.612721",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK133": {
                "id": 37036,
                "uid": "PICKUP_SDEK_MSK133",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "Рязанский пр-т, 49, корпус 4",
                "address": "Рязанский пр-т, 49, корпус 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.72561",
                "lon": "37.765696",
                "workhours": "Пн-Пт 09:00-20:00, Сб 09:00-20:00, Вс 09:00-20:00"
            },
            "PICKUP_SDEK_MSK152": {
                "id": 38499,
                "uid": "PICKUP_SDEK_MSK152",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул.  Саянская, 7 к. 1",
                "address": "ул.  Саянская, 7 к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.767884",
                "lon": "37.831138",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK51": {
                "id": 19195,
                "uid": "PICKUP_SDEK_MSK51",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. 1-я Ямская, 3/7",
                "address": "ул. 1-я Ямская, 3/7",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.7943611",
                "lon": "37.6093445",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK134": {
                "id": 37097,
                "uid": "PICKUP_SDEK_MSK134",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. 26-ти Бакинских комиссаров, 14",
                "address": "ул. 26-ти Бакинских комиссаров, 14",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.661471",
                "lon": "37.487254",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK81": {
                "id": 29517,
                "uid": "PICKUP_SDEK_MSK81",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. 6-я Кожуховская, 29б, под.3, 22",
                "address": "ул. 6-я Кожуховская, 29б, под.3, 22",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.707367",
                "lon": "37.678812",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK146": {
                "id": 38073,
                "uid": "PICKUP_SDEK_MSK146",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. 6-я Радиальная, 3 корп. 6",
                "address": "ул. 6-я Радиальная, 3 корп. 6",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.605866",
                "lon": "37.664743",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK90": {
                "id": 35028,
                "uid": "PICKUP_SDEK_MSK90",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. 9-я Парковая, 52, корп.1 подвал с торца дома",
                "address": "ул. 9-я Парковая, 52, корп.1 подвал с торца дома",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.801243",
                "lon": "37.799643",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK24": {
                "id": 8268,
                "uid": "PICKUP_SDEK_MSK24",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. 9-я Рота, 14",
                "address": "ул. 9-я Рота, 14",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.7911148",
                "lon": "37.7126312",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK151": {
                "id": 38346,
                "uid": "PICKUP_SDEK_MSK151",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Авиаконструктора Петлякова, 31",
                "address": "ул. Авиаконструктора Петлякова, 31",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.626568",
                "lon": "37.31251",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK5": {
                "id": 17555,
                "uid": "PICKUP_SDEK_MSK5",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Авиамоторная, 67/8, стр.3",
                "address": "ул. Авиамоторная, 67/8, стр.3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.737845",
                "lon": "37.721369",
                "workhours": "Пн-Пт 09:00-21:00, Сб-Вс 10:00-18:00"
            },
            "PICKUP_SDEK_MSK36": {
                "id": 10374,
                "uid": "PICKUP_SDEK_MSK36",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Автозаводский 3-й проезд, 4",
                "address": "ул. Автозаводский 3-й проезд, 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.7070808",
                "lon": "37.6544571",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK140": {
                "id": 37623,
                "uid": "PICKUP_SDEK_MSK140",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Академика Анохина, 60",
                "address": "ул. Академика Анохина, 60",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.649709",
                "lon": "37.46887",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK101": {
                "id": 35127,
                "uid": "PICKUP_SDEK_MSK101",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Анатолия Живова, 6",
                "address": "ул. Анатолия Живова, 6",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.759932",
                "lon": "37.553253",
                "workhours": "Пн-Пт 10:00-20:00, Сб-Вс 10:00-16:00"
            },
            "PICKUP_SDEK_MSK150": {
                "id": 38310,
                "uid": "PICKUP_SDEK_MSK150",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Байкальская, 37",
                "address": "ул. Байкальская, 37",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.817861",
                "lon": "37.814285",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK126": {
                "id": 36357,
                "uid": "PICKUP_SDEK_MSK126",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Барклая, 7/1",
                "address": "ул. Барклая, 7/1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.740492",
                "lon": "37.499785",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK105": {
                "id": 34942,
                "uid": "PICKUP_SDEK_MSK105",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Бахрушина, 1 с 1",
                "address": "ул. Бахрушина, 1 с 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.736868",
                "lon": "37.634506",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK95": {
                "id": 34996,
                "uid": "PICKUP_SDEK_MSK95",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Бойцовая, 8с3",
                "address": "ул. Бойцовая, 8с3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.81087",
                "lon": "37.718336",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK128": {
                "id": 36632,
                "uid": "PICKUP_SDEK_MSK128",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Большая Переяславская, № 5, корпус 1",
                "address": "ул. Большая Переяславская, № 5, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.78161",
                "lon": "37.641728",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK76": {
                "id": 25496,
                "uid": "PICKUP_SDEK_MSK76",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Большая Пионерская, 40, строение 1",
                "address": "ул. Большая Пионерская, 40, строение 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.725478",
                "lon": "37.634056",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK127": {
                "id": 36507,
                "uid": "PICKUP_SDEK_MSK127",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Борисовские Пруды, 14 к. 5",
                "address": "ул. Борисовские Пруды, 14 к. 5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.632831",
                "lon": "37.747649",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK42": {
                "id": 13294,
                "uid": "PICKUP_SDEK_MSK42",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Братеевская, 16, корп.6",
                "address": "ул. Братеевская, 16, корп.6",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.6319695",
                "lon": "37.768177",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK30": {
                "id": 9325,
                "uid": "PICKUP_SDEK_MSK30",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Вавилова, 17а",
                "address": "ул. Вавилова, 17а",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.7011032",
                "lon": "37.5808029",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK97": {
                "id": 35043,
                "uid": "PICKUP_SDEK_MSK97",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Веерная, 24Г",
                "address": "ул. Веерная, 24Г",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.708615",
                "lon": "37.484794",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK136": {
                "id": 37249,
                "uid": "PICKUP_SDEK_MSK136",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Верхняя Масловка, 14",
                "address": "ул. Верхняя Масловка, 14",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.793815",
                "lon": "37.564676",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK138": {
                "id": 37497,
                "uid": "PICKUP_SDEK_MSK138",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Весенняя, 3, корп. 1",
                "address": "ул. Весенняя, 3, корп. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.871917",
                "lon": "37.51578",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK129": {
                "id": 36874,
                "uid": "PICKUP_SDEK_MSK129",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Вишневая, 9 к.1, пом.118А",
                "address": "ул. Вишневая, 9 к.1, пом.118А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.831231",
                "lon": "37.44603",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK63": {
                "id": 22294,
                "uid": "PICKUP_SDEK_MSK63",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Генерала Антонова, 3а, 2",
                "address": "ул. Генерала Антонова, 3а, 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.6461487",
                "lon": "37.5340462",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK125": {
                "id": 36314,
                "uid": "PICKUP_SDEK_MSK125",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Генерала Кузнецова, 18, корп. 2",
                "address": "ул. Генерала Кузнецова, 18, корп. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.688296",
                "lon": "37.854987",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK59": {
                "id": 20854,
                "uid": "PICKUP_SDEK_MSK59",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Гурьянова, 30",
                "address": "ул. Гурьянова, 30",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.6807098",
                "lon": "37.7158203",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK65": {
                "id": 24325,
                "uid": "PICKUP_SDEK_MSK65",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Динамовская, 1А, 110а",
                "address": "ул. Динамовская, 1А, 110а",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.732175",
                "lon": "37.66371",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK52": {
                "id": 19333,
                "uid": "PICKUP_SDEK_MSK52",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Дмитриевского, 23",
                "address": "ул. Дмитриевского, 23",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.7096176",
                "lon": "37.8957787",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK50": {
                "id": 18820,
                "uid": "PICKUP_SDEK_MSK50",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Дмитрия Ульянова, 36",
                "address": "ул. Дмитрия Ульянова, 36",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.6831322",
                "lon": "37.5854492",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK56": {
                "id": 20435,
                "uid": "PICKUP_SDEK_MSK56",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Добролюбова, 21А, корп.А, 4",
                "address": "ул. Добролюбова, 21А, корп.А, 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.8177223",
                "lon": "37.5903702",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK48": {
                "id": 13973,
                "uid": "PICKUP_SDEK_MSK48",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Домодедовская, 20, корп.3, первый офисный подъезд за почтой",
                "address": "ул. Домодедовская, 20, корп.3, первый офисный подъезд за почтой",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.612491",
                "lon": "37.703714",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK165": {
                "id": 42266,
                "uid": "PICKUP_SDEK_MSK165",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Живописная, 30к3",
                "address": "ул. Живописная, 30к3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.791962",
                "lon": "37.45654",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK12": {
                "id": 2414,
                "uid": "PICKUP_SDEK_MSK12",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Зеленодольская, 36, корп.2, 32",
                "address": "ул. Зеленодольская, 36, корп.2, 32",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.7092934",
                "lon": "37.7647362",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-18:00, Вс 10:00-16:00"
            },
            "PICKUP_SDEK_MSK92": {
                "id": 35052,
                "uid": "PICKUP_SDEK_MSK92",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Кантемировская, 53 корп. 1",
                "address": "ул. Кантемировская, 53 корп. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.637535",
                "lon": "37.650981",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK62": {
                "id": 22293,
                "uid": "PICKUP_SDEK_MSK62",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Кировоградская, 15, 1г-7",
                "address": "ул. Кировоградская, 15, 1г-7",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.609787",
                "lon": "37.6051331",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK154": {
                "id": 38684,
                "uid": "PICKUP_SDEK_MSK154",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Кольская, 8 стр. 50",
                "address": "ул. Кольская, 8 стр. 50",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.859979",
                "lon": "37.649022",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK38": {
                "id": 11133,
                "uid": "PICKUP_SDEK_MSK38",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Коминтерна, 4, 10",
                "address": "ул. Коминтерна, 4, 10",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.8608971",
                "lon": "37.6755104",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK139": {
                "id": 37555,
                "uid": "PICKUP_SDEK_MSK139",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Коненкова, 18",
                "address": "ул. Коненкова, 18",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.894727",
                "lon": "37.612299",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-17:00, Вс 10:00-15:00"
            },
            "PICKUP_SDEK_MSK104": {
                "id": 34941,
                "uid": "PICKUP_SDEK_MSK104",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Коцюбинского, 9 кор. 2",
                "address": "ул. Коцюбинского, 9 кор. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.730902",
                "lon": "37.428531",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK143": {
                "id": 37918,
                "uid": "PICKUP_SDEK_MSK143",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Кравченко, 11 пом. I-А ком.1",
                "address": "ул. Кравченко, 11 пом. I-А ком.1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.677661",
                "lon": "37.51392",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK100": {
                "id": 35152,
                "uid": "PICKUP_SDEK_MSK100",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Красностуденческий проезд, 7",
                "address": "ул. Красностуденческий проезд, 7",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.824299",
                "lon": "37.566252",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK108": {
                "id": 35016,
                "uid": "PICKUP_SDEK_MSK108",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Лермонтовский пр-т., 6",
                "address": "ул. Лермонтовский пр-т., 6",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.702378",
                "lon": "37.848314",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK54": {
                "id": 19775,
                "uid": "PICKUP_SDEK_MSK54",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Луганская, 5, 3",
                "address": "ул. Луганская, 5, 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.6260872",
                "lon": "37.6662712",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK119": {
                "id": 35336,
                "uid": "PICKUP_SDEK_MSK119",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Люблинская, 9, стр.3",
                "address": "ул. Люблинская, 9, стр.3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.712",
                "lon": "37.7327273",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK148": {
                "id": 38173,
                "uid": "PICKUP_SDEK_MSK148",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Малая Филевская, 30",
                "address": "ул. Малая Филевская, 30",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.735193",
                "lon": "37.460145",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK94": {
                "id": 35136,
                "uid": "PICKUP_SDEK_MSK94",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Маломосковская, 10",
                "address": "ул. Маломосковская, 10",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.81258",
                "lon": "37.645654",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK40": {
                "id": 12890,
                "uid": "PICKUP_SDEK_MSK40",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Марии Поливановой, 9",
                "address": "ул. Марии Поливановой, 9",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.6786919",
                "lon": "37.4532166",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK83": {
                "id": 31670,
                "uid": "PICKUP_SDEK_MSK83",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Маршала Новикова, 16",
                "address": "ул. Маршала Новикова, 16",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.805292",
                "lon": "37.461446",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK163": {
                "id": 40665,
                "uid": "PICKUP_SDEK_MSK163",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Маршала Рыбалко, 9, пом.2",
                "address": "ул. Маршала Рыбалко, 9, пом.2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.797361",
                "lon": "37.490353",
                "workhours": "Пн-Пт 09:00-20:00, Сб-Вс 10:00-18:00"
            },
            "PICKUP_SDEK_MSK135": {
                "id": 37107,
                "uid": "PICKUP_SDEK_MSK135",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Михневская, 8",
                "address": "ул. Михневская, 8",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.579811",
                "lon": "37.67131",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK78": {
                "id": 25717,
                "uid": "PICKUP_SDEK_MSK78",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Молодогвардейская, 54, корп.4",
                "address": "ул. Молодогвардейская, 54, корп.4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.735576",
                "lon": "37.397009",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK18": {
                "id": 4962,
                "uid": "PICKUP_SDEK_MSK18",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Нелидовская, 21, корп.1",
                "address": "ул. Нелидовская, 21, корп.1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.8450966",
                "lon": "37.4362984",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK149": {
                "id": 38171,
                "uid": "PICKUP_SDEK_MSK149",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Новокосинская, 8/1",
                "address": "ул. Новокосинская, 8/1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.733087",
                "lon": "37.855428",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK47": {
                "id": 13913,
                "uid": "PICKUP_SDEK_MSK47",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Новослободская, 31, стр.4",
                "address": "ул. Новослободская, 31, стр.4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.7819214",
                "lon": "37.5962715",
                "workhours": "Пн-Пт 09:00-20:00, Сб-Вс 10:00-16:00"
            },
            "PICKUP_SDEK_MSK45": {
                "id": 13702,
                "uid": "PICKUP_SDEK_MSK45",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Осенняя, 4, корп.1, П45.2",
                "address": "ул. Осенняя, 4, корп.1, П45.2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.7582626",
                "lon": "37.4004326",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK43": {
                "id": 13573,
                "uid": "PICKUP_SDEK_MSK43",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Первомайская, 1",
                "address": "ул. Первомайская, 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.7904968",
                "lon": "37.7711411",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK68": {
                "id": 25094,
                "uid": "PICKUP_SDEK_MSK68",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Перерва, 19, стр.5",
                "address": "ул. Перерва, 19, стр.5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.655579",
                "lon": "37.731155",
                "workhours": "Пн-Пт 09:00-21:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK118": {
                "id": 35191,
                "uid": "PICKUP_SDEK_MSK118",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. пр-т Свободный, 26",
                "address": "ул. пр-т Свободный, 26",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.7487328",
                "lon": "37.8152658",
                "workhours": "Пн-Пт 10:00-20:00, Сб-Вс 10:00-18:00"
            },
            "PICKUP_SDEK_MSK156": {
                "id": 40122,
                "uid": "PICKUP_SDEK_MSK156",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Промышленная, 11 стр. 3, пом. 1, 5",
                "address": "ул. Промышленная, 11 стр. 3, пом. 1, 5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.634087",
                "lon": "37.628219",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK98": {
                "id": 35114,
                "uid": "PICKUP_SDEK_MSK98",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Профсоюзная, 128, корп. 3",
                "address": "ул. Профсоюзная, 128, корп. 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.630609",
                "lon": "37.514715",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK13": {
                "id": 3142,
                "uid": "PICKUP_SDEK_MSK13",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Профсоюзная, 45, вход с левого торца",
                "address": "ул. Профсоюзная, 45, вход с левого торца",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.6665993",
                "lon": "37.5521469",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK130": {
                "id": 36873,
                "uid": "PICKUP_SDEK_MSK130",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Пятницкая, 18 стр. 4",
                "address": "ул. Пятницкая, 18 стр. 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.742037",
                "lon": "37.627337",
                "workhours": "Пн-Вс 10:00-20:00"
            },
            "PICKUP_SDEK_MSK60": {
                "id": 21533,
                "uid": "PICKUP_SDEK_MSK60",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Раменки, 23",
                "address": "ул. Раменки, 23",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.6898308",
                "lon": "37.4929848",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK11": {
                "id": 2413,
                "uid": "PICKUP_SDEK_MSK11",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Римского-Корсакова, 8, под.8",
                "address": "ул. Римского-Корсакова, 8, под.8",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.871685",
                "lon": "37.5963516",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK114": {
                "id": 35167,
                "uid": "PICKUP_SDEK_MSK114",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Россошанская, 6",
                "address": "ул. Россошанская, 6",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.59252",
                "lon": "37.612488",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK141": {
                "id": 37675,
                "uid": "PICKUP_SDEK_MSK141",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Садовая-Черногрязская, 3Б, стр1 под-д 9",
                "address": "ул. Садовая-Черногрязская, 3Б, стр1 под-д 9",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.767347",
                "lon": "37.652634",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK67": {
                "id": 24733,
                "uid": "PICKUP_SDEK_MSK67",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Свободы, 89, корп.5",
                "address": "ул. Свободы, 89, корп.5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.866047",
                "lon": "37.442308",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK117": {
                "id": 35130,
                "uid": "PICKUP_SDEK_MSK117",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Семеновская набережная, 3/1 корп. 7",
                "address": "ул. Семеновская набережная, 3/1 корп. 7",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.779779",
                "lon": "37.706895",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK2": {
                "id": 500,
                "uid": "PICKUP_SDEK_MSK2",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Сергия Радонежского, 29/31",
                "address": "ул. Сергия Радонежского, 29/31",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.746927",
                "lon": "37.679502",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-18:00, Вс 10:00-16:00"
            },
            "PICKUP_SDEK_MSK110": {
                "id": 35061,
                "uid": "PICKUP_SDEK_MSK110",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Смольная, 63Б корп. 1, М06",
                "address": "ул. Смольная, 63Б корп. 1, М06",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.869092",
                "lon": "37.469359",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK168": {
                "id": 42625,
                "uid": "PICKUP_SDEK_MSK168",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Стартовая, 23 корп. 1",
                "address": "ул. Стартовая, 23 корп. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.887041",
                "lon": "37.693122",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK93": {
                "id": 35019,
                "uid": "PICKUP_SDEK_MSK93",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Сущевский вал, 5, стр 15, 43",
                "address": "ул. Сущевский вал, 5, стр 15, 43",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.795494",
                "lon": "37.594584",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK121": {
                "id": 35857,
                "uid": "PICKUP_SDEK_MSK121",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Таганрогская, 11 корп. 3",
                "address": "ул. Таганрогская, 11 корп. 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.679094",
                "lon": "37.756425",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK31": {
                "id": 9605,
                "uid": "PICKUP_SDEK_MSK31",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Тарусская, 18, корп.1, 6",
                "address": "ул. Тарусская, 18, корп.1, 6",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.6029854",
                "lon": "37.5292664",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK109": {
                "id": 35117,
                "uid": "PICKUP_SDEK_MSK109",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Университетский проспект, 23 корп.4",
                "address": "ул. Университетский проспект, 23 корп.4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.7138466",
                "lon": "37.517006",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK144": {
                "id": 37968,
                "uid": "PICKUP_SDEK_MSK144",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Хлобыстова, 14 корп. 1",
                "address": "ул. Хлобыстова, 14 корп. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.718608",
                "lon": "37.806255",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK74": {
                "id": 25314,
                "uid": "PICKUP_SDEK_MSK74",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Цимлянская, 20",
                "address": "ул. Цимлянская, 20",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.670646",
                "lon": "37.770591",
                "workhours": "Пн-Пт 09:00-21:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK162": {
                "id": 40602,
                "uid": "PICKUP_SDEK_MSK162",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Часовая, 10/1",
                "address": "ул. Часовая, 10/1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.808179",
                "lon": "37.5327",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-18:00, Вс 10:00-16:00"
            },
            "PICKUP_SDEK_MSK87": {
                "id": 35163,
                "uid": "PICKUP_SDEK_MSK87",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Шаболовка, 38",
                "address": "ул. Шаболовка, 38",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.718617",
                "lon": "37.608913",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK89": {
                "id": 35116,
                "uid": "PICKUP_SDEK_MSK89",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Шарикоподшипниковская, 13 стр. А",
                "address": "ул. Шарикоподшипниковская, 13 стр. А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.716407",
                "lon": "37.678442",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK106": {
                "id": 35066,
                "uid": "PICKUP_SDEK_MSK106",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Шипиловская, 58, к.1",
                "address": "ул. Шипиловская, 58, к.1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.621211",
                "lon": "37.7456",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK113": {
                "id": 34979,
                "uid": "PICKUP_SDEK_MSK113",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул. Энергетиков, 22 корп. 2",
                "address": "ул. Энергетиков, 22 корп. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.644399",
                "lon": "37.831928",
                "workhours": "Пн-Вс 09:00-21:00"
            },
            "PICKUP_SDEK_MSK147": {
                "id": 38133,
                "uid": "PICKUP_SDEK_MSK147",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул.1я Владимирская, 29 корпус 2",
                "address": "ул.1я Владимирская, 29 корпус 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.753125",
                "lon": "37.779688",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK102": {
                "id": 35050,
                "uid": "PICKUP_SDEK_MSK102",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ул.Чертановская, 20, корп.2",
                "address": "ул.Чертановская, 20, корп.2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.625791",
                "lon": "37.595052",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK153": {
                "id": 38501,
                "uid": "PICKUP_SDEK_MSK153",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ш.  Алтуфьевское, 85",
                "address": "ш.  Алтуфьевское, 85",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.89137",
                "lon": "37.58552",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK124": {
                "id": 36312,
                "uid": "PICKUP_SDEK_MSK124",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ш. Варшавское, 160, корп. 2",
                "address": "ш. Варшавское, 160, корп. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.5838",
                "lon": "37.591467",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK115": {
                "id": 35189,
                "uid": "PICKUP_SDEK_MSK115",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ш. Дмитровское, 102. корп. 2, стр. 3, 1",
                "address": "ш. Дмитровское, 102. корп. 2, стр. 3, 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.882565",
                "lon": "37.553017",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK99": {
                "id": 35007,
                "uid": "PICKUP_SDEK_MSK99",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ш. Измайловское, 29",
                "address": "ш. Измайловское, 29",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.779449",
                "lon": "37.729988",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK157": {
                "id": 40270,
                "uid": "PICKUP_SDEK_MSK157",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ш. Каширское, 110 к. 4",
                "address": "ш. Каширское, 110 к. 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.607773",
                "lon": "37.713719",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-18:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK64": {
                "id": 22295,
                "uid": "PICKUP_SDEK_MSK64",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ш. Каширское, 22, корп.3, стр.11",
                "address": "ш. Каширское, 22, корп.3, стр.11",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.6580849",
                "lon": "37.6379128",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK69": {
                "id": 25239,
                "uid": "PICKUP_SDEK_MSK69",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ш. Можайское , 30",
                "address": "ш. Можайское , 30",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.719936",
                "lon": "37.419979",
                "workhours": "Пн-Пт 09:00-21:00, Сб 10:00-18:00, Вс 10:00-18:00"
            },
            "PICKUP_SDEK_MSK17": {
                "id": 4821,
                "uid": "PICKUP_SDEK_MSK17",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ш. Щелковское, 29",
                "address": "ш. Щелковское, 29",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.810215",
                "lon": "37.7804794",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            },
            "PICKUP_SDEK_MSK120": {
                "id": 35858,
                "uid": "PICKUP_SDEK_MSK120",
                "isNumericCode": false,
                "agent": 3206191,
                "price": 0,
                "title": "ш. Щёлковское, 3 стр. 1, 183",
                "address": "ш. Щёлковское, 3 стр. 1, 183",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.803742",
                "lon": "37.753146",
                "workhours": "Пн-Пт 10:00-20:00, Сб 10:00-16:00, Вс 10:00-14:00"
            }
        },
        "paymentTypes": [20, 2, 9, 25, 39, 23, 48],
        "price": 0,
        "pickupDatetime": {
            "date": "2018-09-06 18:57:00.000000",
            "timezone_type": 3,
            "timezone": "Europe/Moscow"
        },
        "pickupDatetimeTimestamp": 1536249420,
        "pickupTitle": "Дата поступления 6 сентября"
    },
    "pickpoint": {
        "title": "Постаматы PickPoint",
        "uid": "pickpoint",
        "code": "PICK_POINT",
        "points": {
            "PICK_POINT_7701-038": {
                "id": 201,
                "uid": "PICK_POINT_7701-038",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "1 Квесисская ул., д. 18",
                "address": "1 Квесисская ул., д. 18",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.794139",
                "lon": "37.578817",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-085": {
                "id": 13637,
                "uid": "PICK_POINT_7702-085",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "1 Новокузьминская ул., д. 25",
                "address": "1 Новокузьминская ул., д. 25",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.713659",
                "lon": "37.794361",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7705-042": {
                "id": 36618,
                "uid": "PICK_POINT_7705-042",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "1-й Стрелецкий проезд, д. 3Б",
                "address": "1-й Стрелецкий проезд, д. 3Б",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.797478",
                "lon": "37.607763",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-167": {
                "id": 9445,
                "uid": "PICK_POINT_7702-167",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "1905 года ул., д. 10,стр.1",
                "address": "1905 года ул., д. 10,стр.1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.767995",
                "lon": "37.558625",
                "workhours": "; пн-сб 08:00-19:00; вс 08:00-19:00"
            },
            "PICK_POINT_7702-068": {
                "id": 2718,
                "uid": "PICK_POINT_7702-068",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "2 Войковский пр., д. 2, стр. 11",
                "address": "2 Войковский пр., д. 2, стр. 11",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.817682",
                "lon": "37.496764",
                "workhours": "; пн-вс 09:00-20:50"
            },
            "PICK_POINT_7702-396": {
                "id": 37561,
                "uid": "PICK_POINT_7702-396",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "2 Вольская ул., д.22 к.1",
                "address": "2 Вольская ул., д.22 к.1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.685939",
                "lon": "37.922946",
                "workhours": "; пн-вс 10:00-20:00"
            },
            "PICK_POINT_7702-327": {
                "id": 25739,
                "uid": "PICK_POINT_7702-327",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "2-й Кабельный проезд, д. 1, офис 4",
                "address": "2-й Кабельный проезд, д. 1, офис 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.747723",
                "lon": "37.716297",
                "workhours": "; пн-сб 11:00-17:45; вс выходной"
            },
            "PICK_POINT_7702-086": {
                "id": 13638,
                "uid": "PICK_POINT_7702-086",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "26 Бакинских комиссаров ул., д. 14",
                "address": "26 Бакинских комиссаров ул., д. 14",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.661471",
                "lon": "37.487254",
                "workhours": "; пн-сб выходной; вс выходной"
            },
            "PICK_POINT_7705-085": {
                "id": 39914,
                "uid": "PICK_POINT_7705-085",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "3-я Владимирская ул., д. 24",
                "address": "3-я Владимирская ул., д. 24",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.756336",
                "lon": "37.78827",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-398": {
                "id": 37773,
                "uid": "PICK_POINT_7702-398",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "3-я Мытищинская ул., д. 3,  стр. 1,  офис 501",
                "address": "3-я Мытищинская ул., д. 3,  стр. 1,  офис 501",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.805321",
                "lon": "37.642069",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7701-281": {
                "id": 37937,
                "uid": "PICK_POINT_7701-281",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "3-я Парковая ул., д. 4",
                "address": "3-я Парковая ул., д. 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.788981",
                "lon": "37.784686",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-223": {
                "id": 33198,
                "uid": "PICK_POINT_7701-223",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "7 Кожуховская ул., д. 9",
                "address": "7 Кожуховская ул., д. 9",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.710687",
                "lon": "37.6751",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-148": {
                "id": 10756,
                "uid": "PICK_POINT_7701-148",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "9 Парковая ул., вл.61 А, стр.1",
                "address": "9 Парковая ул., вл.61 А, стр.1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.809086",
                "lon": "37.798396",
                "workhours": "; пн-вс 10:00-19:00 обед 14:00-15:00"
            },
            "PICK_POINT_7702-257": {
                "id": 21554,
                "uid": "PICK_POINT_7702-257",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Авиационная ул., д. 66",
                "address": "Авиационная ул., д. 66",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.810657",
                "lon": "37.459397",
                "workhours": "; пн-вс 09:00-20:45"
            },
            "PICK_POINT_7702-403": {
                "id": 40302,
                "uid": "PICK_POINT_7702-403",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Автозаводская ул., д. 23, с. 931, к. 2",
                "address": "Автозаводская ул., д. 23, с. 931, к. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.689811",
                "lon": "37.657224",
                "workhours": "; пн-сб 10:00-18:00; вс 10:00-18:00"
            },
            "PICK_POINT_7701-030": {
                "id": 531,
                "uid": "PICK_POINT_7701-030",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Адмирала Лазарева ул.,  д. 2",
                "address": "Адмирала Лазарева ул.,  д. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.546964",
                "lon": "37.543301",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-128": {
                "id": 9588,
                "uid": "PICK_POINT_7701-128",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Адмирала Лазарева ул., д. 24",
                "address": "Адмирала Лазарева ул., д. 24",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.546868",
                "lon": "37.532538",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7701-184": {
                "id": 21287,
                "uid": "PICK_POINT_7701-184",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Адмирала Макарова ул., д. 6, стр. 13",
                "address": "Адмирала Макарова ул., д. 6, стр. 13",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.835725",
                "lon": "37.490587",
                "workhours": "; пн-вс 08:00-22:00"
            },
            "PICK_POINT_7701-241": {
                "id": 35374,
                "uid": "PICK_POINT_7701-241",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Адмирала Руднева ул., д. 2",
                "address": "Адмирала Руднева ул., д. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.544003",
                "lon": "37.525802",
                "workhours": "; пн-вс 09:00-23:00"
            },
            "PICK_POINT_7701-097": {
                "id": 1982,
                "uid": "PICK_POINT_7701-097",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Азовская ул., д. 24, к. 3",
                "address": "Азовская ул., д. 24, к. 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.647807",
                "lon": "37.595598",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-194": {
                "id": 24504,
                "uid": "PICK_POINT_7701-194",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Академика Анохина ул., д. 2, к. 1 Б",
                "address": "Академика Анохина ул., д. 2, к. 1 Б",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.670671",
                "lon": "37.476842",
                "workhours": "; пн-вс 09:00-22:00"
            },
            "PICK_POINT_7702-240": {
                "id": 20516,
                "uid": "PICK_POINT_7702-240",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Академика Анохина ул., д. 2, к. 1 Б",
                "address": "Академика Анохина ул., д. 2, к. 1 Б",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.670671",
                "lon": "37.476842",
                "workhours": "; пн-сб 10:00-21:00; вс 10:00-20:00"
            },
            "PICK_POINT_7702-391": {
                "id": 36976,
                "uid": "PICK_POINT_7702-391",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Академика Бакулева ул., д. 10",
                "address": "Академика Бакулева ул., д. 10",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.641572",
                "lon": "37.485795",
                "workhours": "; пн-сб 12:00-19:00; вс выходной"
            },
            "PICK_POINT_7705-008": {
                "id": 25441,
                "uid": "PICK_POINT_7705-008",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Академика Опарина ул., д. 4, к. 1",
                "address": "Академика Опарина ул., д. 4, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.64671",
                "lon": "37.503776",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-292": {
                "id": 22304,
                "uid": "PICK_POINT_7702-292",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Александры Монаховой ул., д. 97",
                "address": "Александры Монаховой ул., д. 97",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.541132",
                "lon": "37.490057",
                "workhours": "; пн-сб 10:00-18:00; вс 10:00-18:00"
            },
            "PICK_POINT_7701-006": {
                "id": 22,
                "uid": "PICK_POINT_7701-006",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Алтуфьевское ш., д. 70, к. 1",
                "address": "Алтуфьевское ш., д. 70, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.888202",
                "lon": "37.588287",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7702-215": {
                "id": 10615,
                "uid": "PICK_POINT_7702-215",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Алтуфьевское ш., д. 80",
                "address": "Алтуфьевское ш., д. 80",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.894332",
                "lon": "37.588248",
                "workhours": "; пн-сб 10:00-19:00; вс выходной"
            },
            "PICK_POINT_7702-067": {
                "id": 2172,
                "uid": "PICK_POINT_7702-067",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Алтуфьевское ш., д. 86, к. 1",
                "address": "Алтуфьевское ш., д. 86, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.897841",
                "lon": "37.588306",
                "workhours": "; пн-вс 09:00-21:45"
            },
            "PICK_POINT_7701-088": {
                "id": 2835,
                "uid": "PICK_POINT_7701-088",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Алтуфьевское ш., д. 95",
                "address": "Алтуфьевское ш., д. 95",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.901954",
                "lon": "37.585661",
                "workhours": "; пн-вс 10:00-20:00"
            },
            "PICK_POINT_7701-291": {
                "id": 42677,
                "uid": "PICK_POINT_7701-291",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Алтуфьевское шоссе, д. 60",
                "address": "Алтуфьевское шоссе, д. 60",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.882508",
                "lon": "37.58761",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7701-212": {
                "id": 25756,
                "uid": "PICK_POINT_7701-212",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Амундсена ул., д. 1, к. 1",
                "address": "Амундсена ул., д. 1, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.84969",
                "lon": "37.658249",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7702-392": {
                "id": 37301,
                "uid": "PICK_POINT_7702-392",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Ангелов пер., д. 9",
                "address": "Ангелов пер., д. 9",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.851616",
                "lon": "37.346398",
                "workhours": "; пн-сб 11:00-16:00; вс выходной"
            },
            "PICK_POINT_7702-256": {
                "id": 21516,
                "uid": "PICK_POINT_7702-256",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Андропова пр-кт, д. 36",
                "address": "Андропова пр-кт, д. 36",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.675723",
                "lon": "37.661873",
                "workhours": "; пн-вс 09:00-20:45"
            },
            "PICK_POINT_7701-204": {
                "id": 42577,
                "uid": "PICK_POINT_7701-204",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Андропова пр-кт., д. 38",
                "address": "Андропова пр-кт., д. 38",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.674457",
                "lon": "37.661745",
                "workhours": "; пн-вс 08:00-21:00"
            },
            "PICK_POINT_7702-169": {
                "id": 9447,
                "uid": "PICK_POINT_7702-169",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Андропова просп., д. 15",
                "address": "Андропова просп., д. 15",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.683062",
                "lon": "37.666207",
                "workhours": "; пн-сб 08:00-19:00; вс 08:00-19:00"
            },
            "PICK_POINT_7701-133": {
                "id": 8993,
                "uid": "PICK_POINT_7701-133",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Багратионовский пр., д. 7, к. 1",
                "address": "Багратионовский пр., д. 7, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.742245",
                "lon": "37.501851",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7705-075": {
                "id": 39943,
                "uid": "PICK_POINT_7705-075",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Базовская ул., д. 15  к. 8",
                "address": "Базовская ул., д. 15  к. 8",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.878056",
                "lon": "37.507906",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7705-057": {
                "id": 37255,
                "uid": "PICK_POINT_7705-057",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Байкальскaя ул., д. 37",
                "address": "Байкальскaя ул., д. 37",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.817701",
                "lon": "37.814459",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7705-024": {
                "id": 36286,
                "uid": "PICK_POINT_7705-024",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Балаклавский пр-кт, д. 48, к. 1",
                "address": "Балаклавский пр-кт, д. 48, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.648336",
                "lon": "37.571758",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-294": {
                "id": 24418,
                "uid": "PICK_POINT_7702-294",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Балаклавский пр-кт, д. 5",
                "address": "Балаклавский пр-кт, д. 5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.640421",
                "lon": "37.61026",
                "workhours": "; пн-сб 10:00-18:00; вс выходной"
            },
            "PICK_POINT_7702-365": {
                "id": 35472,
                "uid": "PICK_POINT_7702-365",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Барклая ул., д. 12",
                "address": "Барклая ул., д. 12",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.744783",
                "lon": "37.498557",
                "workhours": "; пн-сб 10:00-20:45; вс 10:00-20:45"
            },
            "PICK_POINT_7701-077": {
                "id": 2879,
                "uid": "PICK_POINT_7701-077",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Барклая ул., д. 16, к. 1",
                "address": "Барклая ул., д. 16, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.745867",
                "lon": "37.496603",
                "workhours": "; пн-вс 00:00-23:59"
            },
            "PICK_POINT_7702-066": {
                "id": 9187,
                "uid": "PICK_POINT_7702-066",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Бауманская ул., д. 32",
                "address": "Бауманская ул., д. 32",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.772334",
                "lon": "37.678275",
                "workhours": "; пн-вс 10:00-21:45"
            },
            "PICK_POINT_7701-288": {
                "id": 40864,
                "uid": "PICK_POINT_7701-288",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Береговой проезд, д. 5А, к. 6",
                "address": "Береговой проезд, д. 5А, к. 6",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.754963",
                "lon": "37.510385",
                "workhours": "; пн-вс 08:00-22:00"
            },
            "PICK_POINT_7702-374": {
                "id": 35945,
                "uid": "PICK_POINT_7702-374",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Бескудниковский б-р, д. 6, к. 4",
                "address": "Бескудниковский б-р, д. 6, к. 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.858559",
                "lon": "37.564491",
                "workhours": "; пн-вс выходной"
            },
            "PICK_POINT_7702-091": {
                "id": 17536,
                "uid": "PICK_POINT_7702-091",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Бибиревская ул., д. 10  к. 2",
                "address": "Бибиревская ул., д. 10  к. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.88146",
                "lon": "37.602382",
                "workhours": "; пн-сб 10:00-21:00; вс 10:00-20:00"
            },
            "PICK_POINT_7701-180": {
                "id": 21737,
                "uid": "PICK_POINT_7701-180",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Бибиревская ул., д. 10, к. 1",
                "address": "Бибиревская ул., д. 10, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.881768",
                "lon": "37.601789",
                "workhours": "; пн-вс 00:00-23:59"
            },
            "PICK_POINT_7701-132": {
                "id": 8747,
                "uid": "PICK_POINT_7701-132",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Бирюлевская ул., д. 51, к.1",
                "address": "Бирюлевская ул., д. 51, к.1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.582655",
                "lon": "37.671507",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7702-345": {
                "id": 33063,
                "uid": "PICK_POINT_7702-345",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Большая Ордынка ул., д. 19, стр. 1",
                "address": "Большая Ордынка ул., д. 19, стр. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.741597",
                "lon": "37.625648",
                "workhours": "; пн-сб 10:00-20:00; вс 10:00-20:00"
            },
            "PICK_POINT_7702-399": {
                "id": 37867,
                "uid": "PICK_POINT_7702-399",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Большая Пионерская ул., д. 4",
                "address": "Большая Пионерская ул., д. 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.729458",
                "lon": "37.634029",
                "workhours": "; пн-сб 10:00-18:00; вс 10:00-18:00"
            },
            "PICK_POINT_7701-272": {
                "id": 36508,
                "uid": "PICK_POINT_7701-272",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Большая Филевская ул., д. 3",
                "address": "Большая Филевская ул., д. 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.751617",
                "lon": "37.5141",
                "workhours": "; пн-вс 08:00-22:00"
            },
            "PICK_POINT_7705-080": {
                "id": 38349,
                "uid": "PICK_POINT_7705-080",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Бориса Галушкина ул., д. 14, к. 1",
                "address": "Бориса Галушкина ул., д. 14, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.823479",
                "lon": "37.655077",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-027": {
                "id": 2556,
                "uid": "PICK_POINT_7701-027",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Борисовские пруды ул.,  д. 26",
                "address": "Борисовские пруды ул.,  д. 26",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.640056",
                "lon": "37.757968",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-137": {
                "id": 9786,
                "uid": "PICK_POINT_7701-137",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Борисовские пруды ул., вл.26,  кор.2",
                "address": "Борисовские пруды ул., вл.26,  кор.2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.639029",
                "lon": "37.759228",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7702-405": {
                "id": 40067,
                "uid": "PICK_POINT_7702-405",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Борисовские пруды ул., д. 10  к. 4",
                "address": "Борисовские пруды ул., д. 10  к. 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.63376",
                "lon": "37.7396",
                "workhours": "; пн-сб 10:00-18:00; вс 10:00-18:00"
            },
            "PICK_POINT_7702-385": {
                "id": 36905,
                "uid": "PICK_POINT_7702-385",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Боровское шоссе, д. 27, пав. 26",
                "address": "Боровское шоссе, д. 27, пав. 26",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.64475",
                "lon": "37.365748",
                "workhours": "; пн-вс 10:00-20:00"
            },
            "PICK_POINT_7701-296": {
                "id": 42766,
                "uid": "PICK_POINT_7701-296",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Боровское шоссе, д. 36",
                "address": "Боровское шоссе, д. 36",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.639876",
                "lon": "37.354363",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7701-216": {
                "id": 29581,
                "uid": "PICK_POINT_7701-216",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Боровское шоссе, д. 6",
                "address": "Боровское шоссе, д. 6",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.658794",
                "lon": "37.401905",
                "workhours": "; пн-вс 00:00-23:59"
            },
            "PICK_POINT_7701-115": {
                "id": 7210,
                "uid": "PICK_POINT_7701-115",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Братиславская ул., д. 14",
                "address": "Братиславская ул., д. 14",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.659183",
                "lon": "37.755113",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7701-276": {
                "id": 36779,
                "uid": "PICK_POINT_7701-276",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Братиславская ул., д. 22",
                "address": "Братиславская ул., д. 22",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.655173",
                "lon": "37.762048",
                "workhours": "; пн-вс 10:00-23:00"
            },
            "PICK_POINT_7705-005": {
                "id": 18234,
                "uid": "PICK_POINT_7705-005",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Буденного пр-т, д. 32",
                "address": "Буденного пр-т, д. 32",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.760793",
                "lon": "37.73518",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-262": {
                "id": 21400,
                "uid": "PICK_POINT_7702-262",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Булатниковская ул., д. 6, к. 1",
                "address": "Булатниковская ул., д. 6, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.580986",
                "lon": "37.64418",
                "workhours": "; пн-сб 10:00-18:00 обед 14:00-15:00; вс 10:00-15:00"
            },
            "PICK_POINT_7701-239": {
                "id": 33964,
                "uid": "PICK_POINT_7701-239",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Булатниковский проезд, д. 14, к. 7",
                "address": "Булатниковский проезд, д. 14, к. 7",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.580774",
                "lon": "37.657781",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-051": {
                "id": 2692,
                "uid": "PICK_POINT_7701-051",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Вавилова ул.,  д. 66",
                "address": "Вавилова ул.,  д. 66",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.683164",
                "lon": "37.548971",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7705-009": {
                "id": 24636,
                "uid": "PICK_POINT_7705-009",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Вавилова ул., д. 17",
                "address": "Вавилова ул., д. 17",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.701861",
                "lon": "37.580679",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-168": {
                "id": 21576,
                "uid": "PICK_POINT_7701-168",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Вавилова ул., д. 3",
                "address": "Вавилова ул., д. 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.706883",
                "lon": "37.592096",
                "workhours": "; пн-вс 09:00-23:00"
            },
            "PICK_POINT_7701-083": {
                "id": 2837,
                "uid": "PICK_POINT_7701-083",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Варшавское ш., д. 152 А",
                "address": "Варшавское ш., д. 152 А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.594561",
                "lon": "37.59913",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-119": {
                "id": 7165,
                "uid": "PICK_POINT_7701-119",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Варшавское ш., д. 160",
                "address": "Варшавское ш., д. 160",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.583022",
                "lon": "37.595262",
                "workhours": "; пн-вс 09:00-23:00"
            },
            "PICK_POINT_7701-158": {
                "id": 13714,
                "uid": "PICK_POINT_7701-158",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Варшавское ш., д. 26  стр.6",
                "address": "Варшавское ш., д. 26  стр.6",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.683808",
                "lon": "37.621435",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7702-081": {
                "id": 13141,
                "uid": "PICK_POINT_7702-081",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Варшавское ш., д. 26",
                "address": "Варшавское ш., д. 26",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.684503",
                "lon": "37.621947",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-136": {
                "id": 9794,
                "uid": "PICK_POINT_7701-136",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Варшавское ш., д. 87Б",
                "address": "Варшавское ш., д. 87Б",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.653644",
                "lon": "37.620698",
                "workhours": "; пн-вс 00:00-23:59"
            },
            "PICK_POINT_7702-312": {
                "id": 25047,
                "uid": "PICK_POINT_7702-312",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Варшавское шоссе, д. 39, офис 444",
                "address": "Варшавское шоссе, д. 39, офис 444",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.684473",
                "lon": "37.624121",
                "workhours": "; пн-сб 10:00-18:00; вс 10:00-18:00"
            },
            "PICK_POINT_7702-354": {
                "id": 36378,
                "uid": "PICK_POINT_7702-354",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Веневская ул., д. 4",
                "address": "Веневская ул., д. 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.548467",
                "lon": "37.542509",
                "workhours": "; пн-вс 10:00-21:00 обед 14:00-15:00"
            },
            "PICK_POINT_7701-246": {
                "id": 35635,
                "uid": "PICK_POINT_7701-246",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Вернадского пр-кт, д. 105",
                "address": "Вернадского пр-кт, д. 105",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.662954",
                "lon": "37.48583",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-059": {
                "id": 2739,
                "uid": "PICK_POINT_7701-059",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Вернадского пр-кт, д. 29",
                "address": "Вернадского пр-кт, д. 29",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.681658",
                "lon": "37.516296",
                "workhours": "; пн-вс 08:00-23:59"
            },
            "PICK_POINT_7702-344": {
                "id": 32914,
                "uid": "PICK_POINT_7702-344",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Вернадского пр-кт, д. 39",
                "address": "Вернадского пр-кт, д. 39",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.675861",
                "lon": "37.505181",
                "workhours": "; пн-сб 12:00-19:00; вс выходной"
            },
            "PICK_POINT_7705-068": {
                "id": 38054,
                "uid": "PICK_POINT_7705-068",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Вернадского пр-кт, д. 41",
                "address": "Вернадского пр-кт, д. 41",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.674519",
                "lon": "37.504591",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-258": {
                "id": 21594,
                "uid": "PICK_POINT_7702-258",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Вернадского пр-кт, д. 86 Б",
                "address": "Вернадского пр-кт, д. 86 Б",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.662206",
                "lon": "37.480269",
                "workhours": "; пн-вс 10:00-20:45"
            },
            "PICK_POINT_7705-053": {
                "id": 36782,
                "uid": "PICK_POINT_7705-053",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Верхняя Красносельская ул., д. 8, к. 3",
                "address": "Верхняя Красносельская ул., д. 8, к. 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.784587",
                "lon": "37.660323",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-066": {
                "id": 2756,
                "uid": "PICK_POINT_7701-066",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Вешняковская ул., д. 22 А",
                "address": "Вешняковская ул., д. 22 А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.720351",
                "lon": "37.822256",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7702-389": {
                "id": 36860,
                "uid": "PICK_POINT_7702-389",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Вешняковская ул., д. 9, к. 2",
                "address": "Вешняковская ул., д. 9, к. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.736883",
                "lon": "37.828829",
                "workhours": "; пн-вс 10:00-21:00 обед 14:00-15:00"
            },
            "PICK_POINT_7701-041": {
                "id": 2646,
                "uid": "PICK_POINT_7701-041",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Волгоградский пр-кт, д. 42, к. 23",
                "address": "Волгоградский пр-кт, д. 42, к. 23",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.710265",
                "lon": "37.708238",
                "workhours": "; пн-вс 08:00-22:00"
            },
            "PICK_POINT_7701-159": {
                "id": 17516,
                "uid": "PICK_POINT_7701-159",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Волгоградский просп., д. 1  стр. 1",
                "address": "Волгоградский просп., д. 1  стр. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.733112",
                "lon": "37.671669",
                "workhours": "; пн-вс 09:00-22:00"
            },
            "PICK_POINT_7702-250": {
                "id": 21474,
                "uid": "PICK_POINT_7702-250",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Волгоградский просп., д. 117, к. 1",
                "address": "Волгоградский просп., д. 117, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.706315",
                "lon": "37.763243",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-157": {
                "id": 17535,
                "uid": "PICK_POINT_7701-157",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Волгоградский просп., д. 32, к. 1",
                "address": "Волгоградский просп., д. 32, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.724109",
                "lon": "37.689024",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7701-151": {
                "id": 13048,
                "uid": "PICK_POINT_7701-151",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Волжский б., кв. 113А, к. 1",
                "address": "Волжский б., кв. 113А, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.699999",
                "lon": "37.752365",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7701-312": {
                "id": 42830,
                "uid": "PICK_POINT_7701-312",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Волжский бульвар, д. 44",
                "address": "Волжский бульвар, д. 44",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.699089",
                "lon": "37.750636",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7701-167": {
                "id": 29580,
                "uid": "PICK_POINT_7701-167",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Волоцкой пер., д. 9",
                "address": "Волоцкой пер., д. 9",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.836853",
                "lon": "37.352102",
                "workhours": "; пн-вс 00:00-23:59"
            },
            "PICK_POINT_7701-152": {
                "id": 13321,
                "uid": "PICK_POINT_7701-152",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Волочаевская ул.,д.12А, стр. 1",
                "address": "Волочаевская ул.,д.12А, стр. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.755925",
                "lon": "37.681973",
                "workhours": "; пн-вс 00:00-23:59"
            },
            "PICK_POINT_7702-358": {
                "id": 33710,
                "uid": "PICK_POINT_7702-358",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Воронежская ул., д. 36/1",
                "address": "Воронежская ул., д. 36/1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.610367",
                "lon": "37.740345",
                "workhours": "; пн-сб 12:00-19:00; вс выходной"
            },
            "PICK_POINT_7701-021": {
                "id": 427,
                "uid": "PICK_POINT_7701-021",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Воротынская ул.  (Куркино), д. 18",
                "address": "Воротынская ул.  (Куркино), д. 18",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.889888",
                "lon": "37.388515",
                "workhours": "; пн-вс 09:00-23:00"
            },
            "PICK_POINT_7705-013": {
                "id": 35553,
                "uid": "PICK_POINT_7705-013",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Габричевского ул., д. 10, к. 4",
                "address": "Габричевского ул., д. 10, к. 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.815463",
                "lon": "37.463403",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-014": {
                "id": 1524,
                "uid": "PICK_POINT_7701-014",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Гарибальди ул.,  д. 23",
                "address": "Гарибальди ул.,  д. 23",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.671498",
                "lon": "37.553792",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7701-181": {
                "id": 25559,
                "uid": "PICK_POINT_7701-181",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Генерала Белова ул., д. 20",
                "address": "Генерала Белова ул., д. 20",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.608155",
                "lon": "37.722558",
                "workhours": "; пн-вс 09:00-23:00"
            },
            "PICK_POINT_7702-363": {
                "id": 34063,
                "uid": "PICK_POINT_7702-363",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Генерала Кузнецова ул., д. 15, к. 1",
                "address": "Генерала Кузнецова ул., д. 15, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.686766",
                "lon": "37.858905",
                "workhours": "; пн-сб 10:00-18:00; вс выходной"
            },
            "PICK_POINT_7702-348": {
                "id": 33110,
                "uid": "PICK_POINT_7702-348",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Герасима Курина ул., д. 14, к. 1Б",
                "address": "Герасима Курина ул., д. 14, к. 1Б",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.731637",
                "lon": "37.475639",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7701-067": {
                "id": 2753,
                "uid": "PICK_POINT_7701-067",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Героев Панфиловцев ул., д. 1 А",
                "address": "Героев Панфиловцев ул., д. 1 А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.8516",
                "lon": "37.4381",
                "workhours": "; пн-вс 10:00-20:00"
            },
            "PICK_POINT_7701-143": {
                "id": 10254,
                "uid": "PICK_POINT_7701-143",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Героев Панфиловцев ул., д. 7",
                "address": "Героев Панфиловцев ул., д. 7",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.857144",
                "lon": "37.43207",
                "workhours": "; пн-вс 09:00-23:00"
            },
            "PICK_POINT_7702-279": {
                "id": 40550,
                "uid": "PICK_POINT_7702-279",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Героев Панфиловцев ул., д. 8,  к. 1",
                "address": "Героев Панфиловцев ул., д. 8,  к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.854835",
                "lon": "37.439275",
                "workhours": "; пн-сб 10:00-20:00; вс 10:00-20:00"
            },
            "PICK_POINT_7705-093": {
                "id": 40278,
                "uid": "PICK_POINT_7705-093",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Главмосстроя ул., д. 18",
                "address": "Главмосстроя ул., д. 18",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.655168",
                "lon": "37.407537",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7705-020": {
                "id": 35599,
                "uid": "PICK_POINT_7705-020",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Главмосстроя ул., д. 5",
                "address": "Главмосстроя ул., д. 5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.652832",
                "lon": "37.406989",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-258": {
                "id": 35436,
                "uid": "PICK_POINT_7701-258",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Главная ул., д. 29",
                "address": "Главная ул., д. 29",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.81997",
                "lon": "37.872335",
                "workhours": "; пн-вс 09:00-20:00"
            },
            "PICK_POINT_7702-400": {
                "id": 38155,
                "uid": "PICK_POINT_7702-400",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Гоголевский б-р, д. 8, стр. 2, каб 9",
                "address": "Гоголевский б-р, д. 8, стр. 2, каб 9",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.745904",
                "lon": "37.600864",
                "workhours": "; пн-сб 11:00-16:00; вс выходной"
            },
            "PICK_POINT_7702-252": {
                "id": 21515,
                "uid": "PICK_POINT_7702-252",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Головинское ш., д. 5, к. 1",
                "address": "Головинское ш., д. 5, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.839836",
                "lon": "37.492029",
                "workhours": "; пн-вс 10:00-21:45"
            },
            "PICK_POINT_7702-394": {
                "id": 37436,
                "uid": "PICK_POINT_7702-394",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Грузинская Б. ул., д. 20",
                "address": "Грузинская Б. ул., д. 20",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.765706",
                "lon": "37.579277",
                "workhours": "; пн-сб 10:00-19:00; вс выходной"
            },
            "PICK_POINT_7701-121": {
                "id": 7246,
                "uid": "PICK_POINT_7701-121",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Гурьянова ул., д. 2а",
                "address": "Гурьянова ул., д. 2а",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.693994",
                "lon": "37.723106",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-364": {
                "id": 34064,
                "uid": "PICK_POINT_7702-364",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Гурьянова ул., д. 30, офис 9",
                "address": "Гурьянова ул., д. 30, офис 9",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.680405",
                "lon": "37.71578",
                "workhours": "; пн-сб 10:00-18:00; вс 10:00-18:00"
            },
            "PICK_POINT_7701-310": {
                "id": 42776,
                "uid": "PICK_POINT_7701-310",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Гурьянова ул., д. 73",
                "address": "Гурьянова ул., д. 73",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.678997",
                "lon": "37.716939",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7701-076": {
                "id": 1147,
                "uid": "PICK_POINT_7701-076",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Дежнева проезд, д. 21",
                "address": "Дежнева проезд, д. 21",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.870491",
                "lon": "37.637872",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-042": {
                "id": 2630,
                "uid": "PICK_POINT_7701-042",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Декабристов ул., 15Б",
                "address": "Декабристов ул., 15Б",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.864889",
                "lon": "37.604133",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7702-404": {
                "id": 40129,
                "uid": "PICK_POINT_7702-404",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Декабристов ул., д. 27, пом. 401",
                "address": "Декабристов ул., д. 27, пом. 401",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.863091",
                "lon": "37.609514",
                "workhours": "; пн-сб 12:00-19:00; вс выходной"
            },
            "PICK_POINT_7701-254": {
                "id": 34120,
                "uid": "PICK_POINT_7701-254",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "дер. Картмазово , д. 7",
                "address": "дер. Картмазово , д. 7",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.62041",
                "lon": "37.387921",
                "workhours": "; пн-вс 00:00-23:59"
            },
            "PICK_POINT_7705-034": {
                "id": 36392,
                "uid": "PICK_POINT_7705-034",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Дмитриевского ул., д. 10",
                "address": "Дмитриевского ул., д. 10",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.708759",
                "lon": "37.885432",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7705-083": {
                "id": 38538,
                "uid": "PICK_POINT_7705-083",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Дмитриевского ул., д. 21",
                "address": "Дмитриевского ул., д. 21",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.709444",
                "lon": "37.894604",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-351": {
                "id": 33525,
                "uid": "PICK_POINT_7702-351",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Дмитрия Донского б-р, д. 1",
                "address": "Дмитрия Донского б-р, д. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.574945",
                "lon": "37.580232",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7705-091": {
                "id": 40627,
                "uid": "PICK_POINT_7705-091",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Дмитрия Донского б-р, д. 11",
                "address": "Дмитрия Донского б-р, д. 11",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.566807",
                "lon": "37.576385",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-238": {
                "id": 32536,
                "uid": "PICK_POINT_7701-238",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Дмитрия Донского б-р, д. 8, к. 1",
                "address": "Дмитрия Донского б-р, д. 8, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.567545",
                "lon": "37.573676",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7701-028": {
                "id": 2557,
                "uid": "PICK_POINT_7701-028",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Дмитровское ш. (Москва),  д. 89",
                "address": "Дмитровское ш. (Москва),  д. 89",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.863991",
                "lon": "37.545495",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-101": {
                "id": 2954,
                "uid": "PICK_POINT_7701-101",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Дмитровское ш. (Москва), д. 98",
                "address": "Дмитровское ш. (Москва), д. 98",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.879545",
                "lon": "37.547295",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7705-073": {
                "id": 38729,
                "uid": "PICK_POINT_7705-073",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Дмитровское ш., д. 90, к. 1",
                "address": "Дмитровское ш., д. 90, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.872329",
                "lon": "37.550037",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-248": {
                "id": 21398,
                "uid": "PICK_POINT_7702-248",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Дмитровское шоссе, д. 108 Б, стр. 1",
                "address": "Дмитровское шоссе, д. 108 Б, стр. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.886539",
                "lon": "37.543713",
                "workhours": "; пн-сб 10:00-20:00; вс 10:00-20:00"
            },
            "PICK_POINT_7701-102": {
                "id": 2965,
                "uid": "PICK_POINT_7701-102",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Днепропетровская ул., д. 2",
                "address": "Днепропетровская ул., д. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.623164",
                "lon": "37.603855",
                "workhours": "; пн-вс 09:00-22:00"
            },
            "PICK_POINT_7701-131": {
                "id": 8820,
                "uid": "PICK_POINT_7701-131",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Домодедовская ул., д. 28",
                "address": "Домодедовская ул., д. 28",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.604742",
                "lon": "37.712318",
                "workhours": "; пн-вс 09:00-23:00"
            },
            "PICK_POINT_7701-146": {
                "id": 10755,
                "uid": "PICK_POINT_7701-146",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Донецкая ул., д. 10, к. 1",
                "address": "Донецкая ул., д. 10, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.650891",
                "lon": "37.716171",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7705-041": {
                "id": 36615,
                "uid": "PICK_POINT_7705-041",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Донецкая ул., д. 23, стр. 2",
                "address": "Донецкая ул., д. 23, стр. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.6473",
                "lon": "37.715534",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-252": {
                "id": 34119,
                "uid": "PICK_POINT_7701-252",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Дорожная ул., д. 1, к. 1",
                "address": "Дорожная ул., д. 1, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.630721",
                "lon": "37.62493",
                "workhours": "; пн-вс 00:00-23:59"
            },
            "PICK_POINT_7702-161": {
                "id": 9028,
                "uid": "PICK_POINT_7702-161",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Дружинниковская ул., д. 15",
                "address": "Дружинниковская ул., д. 15",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.759694",
                "lon": "37.574893",
                "workhours": "; пн-сб выходной; вс выходной"
            },
            "PICK_POINT_7705-064": {
                "id": 37538,
                "uid": "PICK_POINT_7705-064",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Дубнинская ул., д. 52",
                "address": "Дубнинская ул., д. 52",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.897341",
                "lon": "37.55243",
                "workhours": "; пн-вс 09:00-20:00"
            },
            "PICK_POINT_7702-088": {
                "id": 13716,
                "uid": "PICK_POINT_7702-088",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Дубравная ул., д. 34, стр. 29",
                "address": "Дубравная ул., д. 34, стр. 29",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.846113",
                "lon": "37.358211",
                "workhours": "; пн-сб 10:00-21:00; вс 10:00-20:00"
            },
            "PICK_POINT_7701-156": {
                "id": 13156,
                "uid": "PICK_POINT_7701-156",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Дудинка ул., д. 3",
                "address": "Дудинка ул., д. 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.861954",
                "lon": "37.687713",
                "workhours": "; пн-вс 09:00-20:00"
            },
            "PICK_POINT_7705-072": {
                "id": 42270,
                "uid": "PICK_POINT_7705-072",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Елецкая ул.,  д. 35, к. 2A",
                "address": "Елецкая ул.,  д. 35, к. 2A",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.602673",
                "lon": "37.740633",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-020": {
                "id": 1041,
                "uid": "PICK_POINT_7701-020",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Енисейская ул., д. 19  к. 1",
                "address": "Енисейская ул., д. 19  к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.869141",
                "lon": "37.663233",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7702-072": {
                "id": 2722,
                "uid": "PICK_POINT_7702-072",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Енисейская ул., д. 19",
                "address": "Енисейская ул., д. 19",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.868703",
                "lon": "37.662397",
                "workhours": "; пн-вс 09:00-20:45"
            },
            "PICK_POINT_7705-038": {
                "id": 40644,
                "uid": "PICK_POINT_7705-038",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Жулебенский б-р, д. 26",
                "address": "Жулебенский б-р, д. 26",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.690765",
                "lon": "37.854773",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7705-106": {
                "id": 42838,
                "uid": "PICK_POINT_7705-106",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Жулебенский б-р, д. 40, к. 1",
                "address": "Жулебенский б-р, д. 40, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.684679",
                "lon": "37.846843",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-210": {
                "id": 42765,
                "uid": "PICK_POINT_7701-210",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Жулебинский б-р, д. 36, корпус 1",
                "address": "Жулебинский б-р, д. 36, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.684767",
                "lon": "37.847432",
                "workhours": "; пн-вс 07:30-22:30"
            },
            "PICK_POINT_7701-135": {
                "id": 9171,
                "uid": "PICK_POINT_7701-135",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Жулебинский б-р, д. 5А",
                "address": "Жулебинский б-р, д. 5А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.698553",
                "lon": "37.8449",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7702-242": {
                "id": 20489,
                "uid": "PICK_POINT_7702-242",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Жулебинский б., д. 5",
                "address": "Жулебинский б., д. 5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.698553",
                "lon": "37.8449",
                "workhours": "; пн-сб 12:00-19:00; вс выходной"
            },
            "PICK_POINT_7702-260": {
                "id": 21535,
                "uid": "PICK_POINT_7702-260",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "З. А. Космодемьянских ул., д. 4, к. 1 В",
                "address": "З. А. Космодемьянских ул., д. 4, к. 1 В",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.818119",
                "lon": "37.502141",
                "workhours": "; пн-сб 10:00-20:45; вс 10:00-20:45"
            },
            "PICK_POINT_7702-387": {
                "id": 36845,
                "uid": "PICK_POINT_7702-387",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Защитников Москвы пр-кт, д. 15",
                "address": "Защитников Москвы пр-кт, д. 15",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.703976",
                "lon": "37.923799",
                "workhours": "; пн-вс 10:00-21:00 обед 14:00-15:00"
            },
            "PICK_POINT_7705-023": {
                "id": 36977,
                "uid": "PICK_POINT_7705-023",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Звездный б-р, д. 40",
                "address": "Звездный б-р, д. 40",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.813278",
                "lon": "37.623645",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-024": {
                "id": 2540,
                "uid": "PICK_POINT_7701-024",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Звенигородское ш.,  д. 4",
                "address": "Звенигородское ш.,  д. 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.763978",
                "lon": "37.557893",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7701-134": {
                "id": 9127,
                "uid": "PICK_POINT_7701-134",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Зеленодольская ул., д. 44А",
                "address": "Зеленодольская ул., д. 44А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.700938",
                "lon": "37.764483",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7702-411": {
                "id": 40750,
                "uid": "PICK_POINT_7702-411",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Зеленый пр-кт, д. 24, офис 33",
                "address": "Зеленый пр-кт, д. 24, офис 33",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.750454",
                "lon": "37.789896",
                "workhours": "; пн-сб 11:00-18:00; вс 11:00-18:00"
            },
            "PICK_POINT_7702-370": {
                "id": 35723,
                "uid": "PICK_POINT_7702-370",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Земляной вал ул., д. 29",
                "address": "Земляной вал ул., д. 29",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.757491",
                "lon": "37.660808",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-266": {
                "id": 35896,
                "uid": "PICK_POINT_7701-266",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Знаменские садки ул., д. 5Б",
                "address": "Знаменские садки ул., д. 5Б",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.572078",
                "lon": "37.571257",
                "workhours": "; пн-вс 09:00-22:00"
            },
            "PICK_POINT_7705-088": {
                "id": 39918,
                "uid": "PICK_POINT_7705-088",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Зои и Александра Космодемьянских ул., д. 35/1",
                "address": "Зои и Александра Космодемьянских ул., д. 35/1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.825158",
                "lon": "37.525899",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7701-049": {
                "id": 2671,
                "uid": "PICK_POINT_7701-049",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Золоторожский Вал ул., д. 42",
                "address": "Золоторожский Вал ул., д. 42",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.747154",
                "lon": "37.679959",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7702-259": {
                "id": 21441,
                "uid": "PICK_POINT_7702-259",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Золоторожский Вал ул., д. 42",
                "address": "Золоторожский Вал ул., д. 42",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.747281",
                "lon": "37.680039",
                "workhours": "; пн-вс 10:00-20:45"
            },
            "PICK_POINT_7702-192": {
                "id": 9635,
                "uid": "PICK_POINT_7702-192",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Зубовский б., д. 16-20",
                "address": "Зубовский б., д. 16-20",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.738571",
                "lon": "37.588638",
                "workhours": "; пн-сб выходной; вс выходной"
            },
            "PICK_POINT_7701-110": {
                "id": 7164,
                "uid": "PICK_POINT_7701-110",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Зюзинская ул., д.3",
                "address": "Зюзинская ул., д.3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.667402",
                "lon": "37.57715",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-274": {
                "id": 36829,
                "uid": "PICK_POINT_7701-274",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Ивана Франко ул., д. 38",
                "address": "Ивана Франко ул., д. 38",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.72807",
                "lon": "37.416138",
                "workhours": "; пн-вс 09:00-22:00"
            },
            "PICK_POINT_7702-159": {
                "id": 8868,
                "uid": "PICK_POINT_7702-159",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Ивантеевская ул., д. 25А",
                "address": "Ивантеевская ул., д. 25А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.816844",
                "lon": "37.735395",
                "workhours": "; пн-сб 10:00-21:00; вс 10:00-20:00"
            },
            "PICK_POINT_7702-263": {
                "id": 21633,
                "uid": "PICK_POINT_7702-263",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Измайловский б., д. 43",
                "address": "Измайловский б., д. 43",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.79725",
                "lon": "37.798107",
                "workhours": "; пн-сб 11:00-18:00; вс 11:00-18:00"
            },
            "PICK_POINT_7705-048": {
                "id": 36791,
                "uid": "PICK_POINT_7705-048",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Измайловское шоссе, д. 69Д",
                "address": "Измайловское шоссе, д. 69Д",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.788414",
                "lon": "37.748008",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7701-245": {
                "id": 33921,
                "uid": "PICK_POINT_7701-245",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Инженерная ул., д. 5",
                "address": "Инженерная ул., д. 5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.879101",
                "lon": "37.579498",
                "workhours": "; пн-вс 09:00-22:00"
            },
            "PICK_POINT_7701-044": {
                "id": 2649,
                "uid": "PICK_POINT_7701-044",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Инициативная ул., д. 11",
                "address": "Инициативная ул., д. 11",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.715967",
                "lon": "37.456524",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7701-295": {
                "id": 42774,
                "uid": "PICK_POINT_7701-295",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Кастанаевская ул, д. 42, к. 2",
                "address": "Кастанаевская ул, д. 42, к. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.734805",
                "lon": "37.474",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7701-294": {
                "id": 42680,
                "uid": "PICK_POINT_7701-294",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Кастанаевская ул., д. 16, к. 1",
                "address": "Кастанаевская ул., д. 16, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.739133",
                "lon": "37.493184",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7705-084": {
                "id": 40036,
                "uid": "PICK_POINT_7705-084",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Кастанаевская ул., д. 38",
                "address": "Кастанаевская ул., д. 38",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.73587",
                "lon": "37.479654",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7705-087": {
                "id": 40099,
                "uid": "PICK_POINT_7705-087",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Каховка ул., д. 14, к. 3",
                "address": "Каховка ул., д. 14, к. 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.656499",
                "lon": "37.576124",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7701-029": {
                "id": 2544,
                "uid": "PICK_POINT_7701-029",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Каховка ул., д. 29 А",
                "address": "Каховка ул., д. 29 А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.656231",
                "lon": "37.569636",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7702-412": {
                "id": 40647,
                "uid": "PICK_POINT_7702-412",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Каширское ш., 16",
                "address": "Каширское ш., 16",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.662187",
                "lon": "37.630499",
                "workhours": "; пн-сб 10:00-16:00; вс 10:00-16:00"
            },
            "PICK_POINT_7705-097": {
                "id": 40579,
                "uid": "PICK_POINT_7705-097",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Каширское ш., д. 142  к. 1",
                "address": "Каширское ш., д. 142  к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.597398",
                "lon": "37.72263",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7705-047": {
                "id": 36582,
                "uid": "PICK_POINT_7705-047",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Каширское шоссе, д. 10, к. 1, стр. 1",
                "address": "Каширское шоссе, д. 10, к. 1, стр. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.666335",
                "lon": "37.628289",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-406": {
                "id": 40184,
                "uid": "PICK_POINT_7702-406",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Каширское шоссе, д. 122",
                "address": "Каширское шоссе, д. 122",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.605902",
                "lon": "37.71813",
                "workhours": "; пн-сб 10:00-19:00; вс 10:00-19:00"
            },
            "PICK_POINT_7705-043": {
                "id": 36578,
                "uid": "PICK_POINT_7705-043",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Каширское шоссе, д. 57, к. 6",
                "address": "Каширское шоссе, д. 57, к. 6",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.638734",
                "lon": "37.696858",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7705-012": {
                "id": 35552,
                "uid": "PICK_POINT_7705-012",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Керченская ул., д. 1Б",
                "address": "Керченская ул., д. 1Б",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.654874",
                "lon": "37.583329",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-182": {
                "id": 40519,
                "uid": "PICK_POINT_7701-182",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Кетчерская ул., 4А",
                "address": "Кетчерская ул., 4А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.742478",
                "lon": "37.820915",
                "workhours": "; пн-вс 09:00-22:00"
            },
            "PICK_POINT_7702-377": {
                "id": 36205,
                "uid": "PICK_POINT_7702-377",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Кировоградская ул., д. 15, пав. 1В-16",
                "address": "Кировоградская ул., д. 15, пав. 1В-16",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.609787",
                "lon": "37.605131",
                "workhours": "; пн-вс 10:00-20:00 обед 14:00-15:00"
            },
            "PICK_POINT_7701-257": {
                "id": 34062,
                "uid": "PICK_POINT_7701-257",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Кировоградская ул., д. 24А",
                "address": "Кировоградская ул., д. 24А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.612065",
                "lon": "37.603118",
                "workhours": "; пн-вс 10:00-23:59"
            },
            "PICK_POINT_7701-298": {
                "id": 42682,
                "uid": "PICK_POINT_7701-298",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Кировоградская ул., д. 42",
                "address": "Кировоградская ул., д. 42",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.599474",
                "lon": "37.597055",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7702-278": {
                "id": 40569,
                "uid": "PICK_POINT_7702-278",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Кировоградская ул., д. 9  к. 1",
                "address": "Кировоградская ул., д. 9  к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.625567",
                "lon": "37.61229",
                "workhours": "; пн-сб 10:00-21:00; вс 10:00-20:00"
            },
            "PICK_POINT_7701-031": {
                "id": 2550,
                "uid": "PICK_POINT_7701-031",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Кожевническая ул.,  д. 7, стр. 1",
                "address": "Кожевническая ул.,  д. 7, стр. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.729828",
                "lon": "37.645958",
                "workhours": "; пн-вс 10:00-19:00"
            },
            "PICK_POINT_7702-136": {
                "id": 21136,
                "uid": "PICK_POINT_7702-136",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Кожевническая ул., д. 7  стр. 1",
                "address": "Кожевническая ул., д. 7  стр. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.729877",
                "lon": "37.646172",
                "workhours": "; пн-вс 10:00-19:00"
            },
            "PICK_POINT_7701-232": {
                "id": 31718,
                "uid": "PICK_POINT_7701-232",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Колодезный пер., д. 3",
                "address": "Колодезный пер., д. 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.798176",
                "lon": "37.697433",
                "workhours": "; пн-вс 08:00-23:59"
            },
            "PICK_POINT_7701-147": {
                "id": 11461,
                "uid": "PICK_POINT_7701-147",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Коломенская ул., д. 17",
                "address": "Коломенская ул., д. 17",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.678253",
                "lon": "37.697348",
                "workhours": "; пн-вс 09:00-23:00"
            },
            "PICK_POINT_7705-046": {
                "id": 36625,
                "uid": "PICK_POINT_7705-046",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Комсомольская пл., д. 2",
                "address": "Комсомольская пл., д. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.773611",
                "lon": "37.65567",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-082": {
                "id": 13636,
                "uid": "PICK_POINT_7702-082",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Комсомольская пл., д. 6",
                "address": "Комсомольская пл., д. 6",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.775864",
                "lon": "37.660413",
                "workhours": "; пн-сб 10:00-20:00; вс 10:00-20:00"
            },
            "PICK_POINT_7702-381": {
                "id": 36610,
                "uid": "PICK_POINT_7702-381",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Комсомольская пл., д. 6, пав. 34",
                "address": "Комсомольская пл., д. 6, пав. 34",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.775864",
                "lon": "37.660413",
                "workhours": "; пн-вс 10:00-21:00 обед 14:00-15:00"
            },
            "PICK_POINT_7702-384": {
                "id": 36975,
                "uid": "PICK_POINT_7702-384",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Комсомольский пр-кт, д. 30",
                "address": "Комсомольский пр-кт, д. 30",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.725214",
                "lon": "37.578496",
                "workhours": "; пн-сб 10:00-18:00; вс выходной"
            },
            "PICK_POINT_7705-015": {
                "id": 35922,
                "uid": "PICK_POINT_7705-015",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Коненкова ул., д. 5А",
                "address": "Коненкова ул., д. 5А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.88872",
                "lon": "37.603981",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-019": {
                "id": 2538,
                "uid": "PICK_POINT_7701-019",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Корнейчука ул., д. 8",
                "address": "Корнейчука ул., д. 8",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.898754",
                "lon": "37.629088",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7702-386": {
                "id": 36833,
                "uid": "PICK_POINT_7702-386",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Коровинское шоссе, д. 23, к. 1",
                "address": "Коровинское шоссе, д. 23, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.877103",
                "lon": "37.52376",
                "workhours": "; пн-вс выходной"
            },
            "PICK_POINT_7705-031": {
                "id": 38419,
                "uid": "PICK_POINT_7705-031",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Костромская ул., д. 17",
                "address": "Костромская ул., д. 17",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.884813",
                "lon": "37.601053",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7701-316": {
                "id": 42831,
                "uid": "PICK_POINT_7701-316",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Красного маяка ул., д. 16Б",
                "address": "Красного маяка ул., д. 16Б",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.613552",
                "lon": "37.580844",
                "workhours": "; пн-вс 09:00-23:00"
            },
            "PICK_POINT_7705-029": {
                "id": 37778,
                "uid": "PICK_POINT_7705-029",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Краснодарская ул., д. 14",
                "address": "Краснодарская ул., д. 14",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.67693",
                "lon": "37.744944",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-124": {
                "id": 8312,
                "uid": "PICK_POINT_7701-124",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Краснодарская ул., д. 51, корп. 2",
                "address": "Краснодарская ул., д. 51, корп. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.677055",
                "lon": "37.765517",
                "workhours": "; пн-вс 00:00-23:59"
            },
            "PICK_POINT_7701-171": {
                "id": 24294,
                "uid": "PICK_POINT_7701-171",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Красносельская В. ул., д. 3 А",
                "address": "Красносельская В. ул., д. 3 А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.785513",
                "lon": "37.665408",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7701-032": {
                "id": 2568,
                "uid": "PICK_POINT_7701-032",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Кронштадтский б-р, д. 9, стр. 3",
                "address": "Кронштадтский б-р, д. 9, стр. 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.842667",
                "lon": "37.489411",
                "workhours": "; пн-вс 10:00-20:00"
            },
            "PICK_POINT_7702-380": {
                "id": 36608,
                "uid": "PICK_POINT_7702-380",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Кронштадтский б-р, д. 9, стр. 4, пав. М3",
                "address": "Кронштадтский б-р, д. 9, стр. 4, пав. М3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.843252",
                "lon": "37.488925",
                "workhours": "; пн-вс 10:00-20:45 обед 13:30-14:30"
            },
            "PICK_POINT_7702-209": {
                "id": 20194,
                "uid": "PICK_POINT_7702-209",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Кронштадтский б., д. 9  стр. 4",
                "address": "Кронштадтский б., д. 9  стр. 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.843252",
                "lon": "37.488924",
                "workhours": "; пн-сб 10:00-20:00; вс 10:00-20:00"
            },
            "PICK_POINT_7701-055": {
                "id": 2699,
                "uid": "PICK_POINT_7701-055",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Куликовская ул.,  д. 9",
                "address": "Куликовская ул.,  д. 9",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.56738",
                "lon": "37.561954",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7702-413": {
                "id": 40872,
                "uid": "PICK_POINT_7702-413",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Куусинена ул., д. 2, к. 1",
                "address": "Куусинена ул., д. 2, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.778284",
                "lon": "37.517599",
                "workhours": "; пн-сб 11:00-20:00; вс 11:00-20:00"
            },
            "PICK_POINT_7701-233": {
                "id": 32517,
                "uid": "PICK_POINT_7701-233",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Лавочкина ул., д. 42Г",
                "address": "Лавочкина ул., д. 42Г",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.86012",
                "lon": "37.489275",
                "workhours": "; пн-вс 09:00-23:00"
            },
            "PICK_POINT_7702-254": {
                "id": 21440,
                "uid": "PICK_POINT_7702-254",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Ленинградский пр-кт, д. 62",
                "address": "Ленинградский пр-кт, д. 62",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.801056",
                "lon": "37.533302",
                "workhours": "; пн-вс 09:00-20:45"
            },
            "PICK_POINT_7701-255": {
                "id": 34185,
                "uid": "PICK_POINT_7701-255",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Ленинградский пр-кт, д. 62А",
                "address": "Ленинградский пр-кт, д. 62А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.801723",
                "lon": "37.531465",
                "workhours": "; пн-вс 09:00-23:00"
            },
            "PICK_POINT_7702-070": {
                "id": 2720,
                "uid": "PICK_POINT_7702-070",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Ленинградский пр-кт, д. 75",
                "address": "Ленинградский пр-кт, д. 75",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.804999",
                "lon": "37.512172",
                "workhours": "; пн-вс 09:00-20:45"
            },
            "PICK_POINT_7701-247": {
                "id": 35386,
                "uid": "PICK_POINT_7701-247",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Ленинградский пр-кт, д. 76А",
                "address": "Ленинградский пр-кт, д. 76А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.805839",
                "lon": "37.515103",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7702-366": {
                "id": 35473,
                "uid": "PICK_POINT_7702-366",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Ленинградский пр-кт, д. 76А",
                "address": "Ленинградский пр-кт, д. 76А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.805827",
                "lon": "37.515146",
                "workhours": "; пн-вс 10:00-21:45"
            },
            "PICK_POINT_7701-084": {
                "id": 2906,
                "uid": "PICK_POINT_7701-084",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Ленинградский пр-кт, д. 80, к. 17",
                "address": "Ленинградский пр-кт, д. 80, к. 17",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.808131",
                "lon": "37.510698",
                "workhours": "; пн-вс 09:00-23:00"
            },
            "PICK_POINT_7702-264": {
                "id": 21635,
                "uid": "PICK_POINT_7702-264",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Ленинградское ш., д. 58, стр. 26",
                "address": "Ленинградское ш., д. 58, стр. 26",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.842327",
                "lon": "37.484379",
                "workhours": "; пн-сб 12:00-19:00; вс 12:00-19:00"
            },
            "PICK_POINT_7701-072": {
                "id": 866,
                "uid": "PICK_POINT_7701-072",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Ленинский пр-кт, д. 101",
                "address": "Ленинский пр-кт, д. 101",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.666644",
                "lon": "37.51528",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-297": {
                "id": 42681,
                "uid": "PICK_POINT_7701-297",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Ленинский пр-кт, д. 127",
                "address": "Ленинский пр-кт, д. 127",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.646178",
                "lon": "37.477713",
                "workhours": "; пн-вс 00:00-23:59"
            },
            "PICK_POINT_7705-070": {
                "id": 38407,
                "uid": "PICK_POINT_7705-070",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Ленинский пр-кт, д. 99",
                "address": "Ленинский пр-кт, д. 99",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.668613",
                "lon": "37.51793",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-173": {
                "id": 9450,
                "uid": "PICK_POINT_7702-173",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Ленинский просп., д. 45",
                "address": "Ленинский просп., д. 45",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.703033",
                "lon": "37.573312",
                "workhours": "; пн-сб 08:00-19:00; вс 08:00-19:00"
            },
            "PICK_POINT_7702-415": {
                "id": 42581,
                "uid": "PICK_POINT_7702-415",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Летниковская ул., д. 10  стр. 5",
                "address": "Летниковская ул., д. 10  стр. 5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.724266",
                "lon": "37.642806",
                "workhours": "; пн-сб выходной; вс выходной"
            },
            "PICK_POINT_7701-278": {
                "id": 40058,
                "uid": "PICK_POINT_7701-278",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Летчика Бабушкина ул., д. 30, стр. 1",
                "address": "Летчика Бабушкина ул., д. 30, стр. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.870198",
                "lon": "37.676351",
                "workhours": "; пн-вс 08:00-21:00"
            },
            "PICK_POINT_7701-231": {
                "id": 35740,
                "uid": "PICK_POINT_7701-231",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Летчика Ульянина ул., д. 5",
                "address": "Летчика Ульянина ул., д. 5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.625948",
                "lon": "37.307142",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-125": {
                "id": 8328,
                "uid": "PICK_POINT_7701-125",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Литовский б., д. 22",
                "address": "Литовский б., д. 22",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.612042",
                "lon": "37.537608",
                "workhours": "; пн-вс 09:00-22:00"
            },
            "PICK_POINT_7701-087": {
                "id": 1866,
                "uid": "PICK_POINT_7701-087",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Лобненская ул., д. 4 А",
                "address": "Лобненская ул., д. 4 А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.889755",
                "lon": "37.538422",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7701-224": {
                "id": 32965,
                "uid": "PICK_POINT_7701-224",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Локомотивный проезд, д. 29",
                "address": "Локомотивный проезд, д. 29",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.847422",
                "lon": "37.572953",
                "workhours": "; пн-вс 09:00-23:00"
            },
            "PICK_POINT_7701-091": {
                "id": 2951,
                "uid": "PICK_POINT_7701-091",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Локомотивный проезд, д. 4",
                "address": "Локомотивный проезд, д. 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.837877",
                "lon": "37.576847",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7702-276": {
                "id": 40568,
                "uid": "PICK_POINT_7702-276",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Лубянский проезд, д. 15, стр. 2",
                "address": "Лубянский проезд, д. 15, стр. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.756817",
                "lon": "37.632305",
                "workhours": "; пн-сб 10:00-21:00; вс 11:00-19:00"
            },
            "PICK_POINT_7701-100": {
                "id": 2947,
                "uid": "PICK_POINT_7701-100",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Луганская ул., д. 10",
                "address": "Луганская ул., д. 10",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.622582",
                "lon": "37.667603",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7702-409": {
                "id": 40479,
                "uid": "PICK_POINT_7702-409",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Луганская ул., д. 10",
                "address": "Луганская ул., д. 10",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.622436",
                "lon": "37.667582",
                "workhours": "; пн-сб 10:00-21:00; вс 10:00-20:00"
            },
            "PICK_POINT_7702-216": {
                "id": 10617,
                "uid": "PICK_POINT_7702-216",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Лукинская ул., д. 8",
                "address": "Лукинская ул., д. 8",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.650424",
                "lon": "37.343351",
                "workhours": "; пн-сб выходной; вс выходной"
            },
            "PICK_POINT_7702-362": {
                "id": 34039,
                "uid": "PICK_POINT_7702-362",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Лучников пер., д. 4, стр. 2, офис 37",
                "address": "Лучников пер., д. 4, стр. 2, офис 37",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.758352",
                "lon": "37.631802",
                "workhours": "; пн-сб 10:00-18:00; вс выходной"
            },
            "PICK_POINT_7701-277": {
                "id": 36830,
                "uid": "PICK_POINT_7701-277",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Льва Толстого ул., д. 16",
                "address": "Льва Толстого ул., д. 16",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.734165",
                "lon": "37.58831",
                "workhours": "; пн-вс 10:00-23:00"
            },
            "PICK_POINT_7705-061": {
                "id": 37438,
                "uid": "PICK_POINT_7705-061",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Люблинская ул., д. 102 А",
                "address": "Люблинская ул., д. 102 А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.652314",
                "lon": "37.742223",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-009": {
                "id": 514,
                "uid": "PICK_POINT_7701-009",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Люблинская ул., д. 153",
                "address": "Люблинская ул., д. 153",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.658959",
                "lon": "37.742099",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7702-052": {
                "id": 2691,
                "uid": "PICK_POINT_7702-052",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Люблинская ул., д. 169, к. 2",
                "address": "Люблинская ул., д. 169, к. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.648879",
                "lon": "37.745012",
                "workhours": "; пн-вс 10:00-21:45"
            },
            "PICK_POINT_7702-347": {
                "id": 33043,
                "uid": "PICK_POINT_7702-347",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Люблинская ул., д. 27/2",
                "address": "Люблинская ул., д. 27/2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.700694",
                "lon": "37.733419",
                "workhours": "; пн-сб 12:00-17:00; вс выходной"
            },
            "PICK_POINT_7701-243": {
                "id": 34184,
                "uid": "PICK_POINT_7701-243",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Люблинская ул., д. 4, к. 1А",
                "address": "Люблинская ул., д. 4, к. 1А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.707836",
                "lon": "37.729925",
                "workhours": "; пн-сб 09:00-20:00; вс 09:00-20:00"
            },
            "PICK_POINT_7705-044": {
                "id": 37074,
                "uid": "PICK_POINT_7705-044",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Люблинская ул., д. 59",
                "address": "Люблинская ул., д. 59",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.686305",
                "lon": "37.738513",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7705-077": {
                "id": 40356,
                "uid": "PICK_POINT_7705-077",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Малая Ботаническая ул., д. 24 A, стр. 1",
                "address": "Малая Ботаническая ул., д. 24 A, стр. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.831737",
                "lon": "37.58261",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-402": {
                "id": 38113,
                "uid": "PICK_POINT_7702-402",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Малая Пироговская ул., д. 8",
                "address": "Малая Пироговская ул., д. 8",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.72961",
                "lon": "37.570474",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-082": {
                "id": 2820,
                "uid": "PICK_POINT_7701-082",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Маленковская ул.,  д. 30",
                "address": "Маленковская ул.,  д. 30",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.787111",
                "lon": "37.674926",
                "workhours": "; пн-сб 09:00-20:00; вс 09:00-20:00"
            },
            "PICK_POINT_7701-153": {
                "id": 13322,
                "uid": "PICK_POINT_7701-153",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Малыгина ул., д. 7",
                "address": "Малыгина ул., д. 7",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.880298",
                "lon": "37.694747",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-134": {
                "id": 7180,
                "uid": "PICK_POINT_7702-134",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Марксистская ул., д. 3  стр. 1",
                "address": "Марксистская ул., д. 3  стр. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.738149",
                "lon": "37.661966",
                "workhours": "; пн-вс 10:00-20:00"
            },
            "PICK_POINT_7702-137": {
                "id": 7186,
                "uid": "PICK_POINT_7702-137",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Маросейка ул., д. 8",
                "address": "Маросейка ул., д. 8",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.757592",
                "lon": "37.636437",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7701-054": {
                "id": 486,
                "uid": "PICK_POINT_7701-054",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Маршала Бирюзова ул.,  д. 32",
                "address": "Маршала Бирюзова ул.,  д. 32",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.799512",
                "lon": "37.48298",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7702-376": {
                "id": 36139,
                "uid": "PICK_POINT_7702-376",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Маршала Бирюзова ул., д. 31",
                "address": "Маршала Бирюзова ул., д. 31",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.796526",
                "lon": "37.486859",
                "workhours": "; пн-вс 10:00-21:30 обед 14:00-15:00"
            },
            "PICK_POINT_7702-090": {
                "id": 13705,
                "uid": "PICK_POINT_7702-090",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Маршала Василевского ул., д. 13, к. 1",
                "address": "Маршала Василевского ул., д. 13, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.806611",
                "lon": "37.466557",
                "workhours": "; пн-сб 11:00-19:00; вс выходной"
            },
            "PICK_POINT_7701-183": {
                "id": 21636,
                "uid": "PICK_POINT_7701-183",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Маршала Жукова пр-кт д. 48, к. 1",
                "address": "Маршала Жукова пр-кт д. 48, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.777783",
                "lon": "37.465748",
                "workhours": "; пн-вс 09:00-23:00"
            },
            "PICK_POINT_7705-030": {
                "id": 36289,
                "uid": "PICK_POINT_7705-030",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Маршала Жукова пр-кт, д. 53, к. 1",
                "address": "Маршала Жукова пр-кт, д. 53, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.776791",
                "lon": "37.460798",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-071": {
                "id": 2721,
                "uid": "PICK_POINT_7702-071",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Маршала Катукова ул., д. 25",
                "address": "Маршала Катукова ул., д. 25",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.805494",
                "lon": "37.414442",
                "workhours": "; пн-вс 10:00-21:45"
            },
            "PICK_POINT_7702-320": {
                "id": 24999,
                "uid": "PICK_POINT_7702-320",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Маршала Малиновского ул., д. 8",
                "address": "Маршала Малиновского ул., д. 8",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.792645",
                "lon": "37.493587",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7705-082": {
                "id": 42839,
                "uid": "PICK_POINT_7705-082",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Маршала Полубоярова ул., д. 4, к. 1",
                "address": "Маршала Полубоярова ул., д. 4, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.688715",
                "lon": "37.862504",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-261": {
                "id": 35499,
                "uid": "PICK_POINT_7701-261",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Маршала Прошлякова ул., д. 14",
                "address": "Маршала Прошлякова ул., д. 14",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.791182",
                "lon": "37.388718",
                "workhours": "; пн-вс 00:00-23:59"
            },
            "PICK_POINT_7705-076": {
                "id": 40277,
                "uid": "PICK_POINT_7705-076",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Маршала Рокосcовского б-р, д. 30",
                "address": "Маршала Рокосcовского б-р, д. 30",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.81652",
                "lon": "37.717043",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-164": {
                "id": 19934,
                "uid": "PICK_POINT_7701-164",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Маршала Федоренко ул., д. 12",
                "address": "Маршала Федоренко ул., д. 12",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.881616",
                "lon": "37.48914",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7701-122": {
                "id": 7264,
                "uid": "PICK_POINT_7701-122",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Мастеркова ул., д. 6",
                "address": "Мастеркова ул., д. 6",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.707987",
                "lon": "37.656789",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7701-149": {
                "id": 11258,
                "uid": "PICK_POINT_7701-149",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Матвеевская ул., д. 2",
                "address": "Матвеевская ул., д. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.70536",
                "lon": "37.479155",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-215": {
                "id": 25503,
                "uid": "PICK_POINT_7701-215",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Медынская ул., д. 6Б",
                "address": "Медынская ул., д. 6Б",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.589839",
                "lon": "37.644953",
                "workhours": "; пн-вс 09:00-22:00"
            },
            "PICK_POINT_7701-259": {
                "id": 35477,
                "uid": "PICK_POINT_7701-259",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Менжинского ул., д. 32",
                "address": "Менжинского ул., д. 32",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.869619",
                "lon": "37.666666",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7702-189": {
                "id": 9387,
                "uid": "PICK_POINT_7702-189",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Менжинского ул., д. 36",
                "address": "Менжинского ул., д. 36",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.87045",
                "lon": "37.66327",
                "workhours": "; пн-сб 12:00-19:00; вс 12:00-19:00"
            },
            "PICK_POINT_7701-267": {
                "id": 35564,
                "uid": "PICK_POINT_7701-267",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Миклухо-Маклая ул., д. 18, к. 3",
                "address": "Миклухо-Маклая ул., д. 18, к. 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.644379",
                "lon": "37.520509",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-117": {
                "id": 7217,
                "uid": "PICK_POINT_7701-117",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Миклухо-Маклая ул., д. 36",
                "address": "Миклухо-Маклая ул., д. 36",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.640178",
                "lon": "37.532994",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7702-330": {
                "id": 33965,
                "uid": "PICK_POINT_7702-330",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Мира пр-кт, д. 11",
                "address": "Мира пр-кт, д. 11",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.775722",
                "lon": "37.631217",
                "workhours": "; пн-сб 10:00-18:00; вс 10:00-18:00"
            },
            "PICK_POINT_7701-253": {
                "id": 34234,
                "uid": "PICK_POINT_7701-253",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Мира пр-кт, д. 211, к. 1",
                "address": "Мира пр-кт, д. 211, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.844041",
                "lon": "37.66265",
                "workhours": "; пн-вс 00:00-23:59"
            },
            "PICK_POINT_7701-070": {
                "id": 2768,
                "uid": "PICK_POINT_7701-070",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Мира пр-кт, д. 92, стр. 1",
                "address": "Мира пр-кт, д. 92, стр. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.793254",
                "lon": "37.635604",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7702-180": {
                "id": 9456,
                "uid": "PICK_POINT_7702-180",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Мира просп., д. 116 А",
                "address": "Мира просп., д. 116 А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.80956",
                "lon": "37.638117",
                "workhours": "; пн-сб 08:00-19:00; вс 08:00-19:00"
            },
            "PICK_POINT_7701-016": {
                "id": 2185,
                "uid": "PICK_POINT_7701-016",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Митинская ул.,  д. 40",
                "address": "Митинская ул.,  д. 40",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.847832",
                "lon": "37.360287",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-282": {
                "id": 38053,
                "uid": "PICK_POINT_7701-282",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Митинская ул., д. 25",
                "address": "Митинская ул., д. 25",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.844195",
                "lon": "37.363055",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7702-375": {
                "id": 36137,
                "uid": "PICK_POINT_7702-375",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Митинская ул., д. 27",
                "address": "Митинская ул., д. 27",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.845107",
                "lon": "37.361732",
                "workhours": "; пн-вс 10:00-21:00 обед 14:30-15:30"
            },
            "PICK_POINT_7701-141": {
                "id": 10160,
                "uid": "PICK_POINT_7701-141",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Митинская ул., д. 51",
                "address": "Митинская ул., д. 51",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.852521",
                "lon": "37.351914",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7701-074": {
                "id": 941,
                "uid": "PICK_POINT_7701-074",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Мичуринский пр-кт, д. 21, стр. 6",
                "address": "Мичуринский пр-кт, д. 21, стр. 6",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.699453",
                "lon": "37.50605",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-262": {
                "id": 35498,
                "uid": "PICK_POINT_7701-262",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "МКАД, 104 км, д. 6",
                "address": "МКАД, 104 км, д. 6",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.811246",
                "lon": "37.837061",
                "workhours": "; пн-вс 00:00-23:59"
            },
            "PICK_POINT_7701-229": {
                "id": 29692,
                "uid": "PICK_POINT_7701-229",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "МКАД, 73 км., д. 7",
                "address": "МКАД, 73 км., д. 7",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.876476",
                "lon": "37.422279",
                "workhours": "; пн-вс 08:00-21:00"
            },
            "PICK_POINT_7705-018": {
                "id": 35550,
                "uid": "PICK_POINT_7705-018",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Можайское шоссе, д. 25",
                "address": "Можайское шоссе, д. 25",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.719717",
                "lon": "37.425094",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-269": {
                "id": 35722,
                "uid": "PICK_POINT_7701-269",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Мукомольный проезд, д. 9, к. 2",
                "address": "Мукомольный проезд, д. 9, к. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.75358",
                "lon": "37.522504",
                "workhours": "; пн-вт 12:00-22:00; ср-вс 12:00-22:00"
            },
            "PICK_POINT_7701-303": {
                "id": 42832,
                "uid": "PICK_POINT_7701-303",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Мячковский б-р, д. 13",
                "address": "Мячковский б-р, д. 13",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.654529",
                "lon": "37.756884",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7701-201": {
                "id": 25439,
                "uid": "PICK_POINT_7701-201",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Нагатинская ул., д. 16",
                "address": "Нагатинская ул., д. 16",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.67889",
                "lon": "37.642644",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7702-395": {
                "id": 37531,
                "uid": "PICK_POINT_7702-395",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Народного ополчения ул., д. 21  к. 1",
                "address": "Народного ополчения ул., д. 21  к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.77753",
                "lon": "37.477282",
                "workhours": "; пн-сб 10:00-21:00; вс 10:00-20:00"
            },
            "PICK_POINT_7702-177": {
                "id": 9454,
                "uid": "PICK_POINT_7702-177",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Народного ополчения ул., д. 29, кр. 1",
                "address": "Народного ополчения ул., д. 29, кр. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.781565",
                "lon": "37.479789",
                "workhours": "; пн-сб 08:00-19:00; вс 08:00-19:00"
            },
            "PICK_POINT_7705-096": {
                "id": 40454,
                "uid": "PICK_POINT_7705-096",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Наташи Ковшовой ул., д. 8А",
                "address": "Наташи Ковшовой ул., д. 8А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.682134",
                "lon": "37.450579",
                "workhours": "; пн-вс 00:00-23:59"
            },
            "PICK_POINT_7705-049": {
                "id": 37023,
                "uid": "PICK_POINT_7705-049",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Нижегородская ул., д. 32, стр. А",
                "address": "Нижегородская ул., д. 32, стр. А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.737238",
                "lon": "37.688602",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7701-206": {
                "id": 32734,
                "uid": "PICK_POINT_7701-206",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Новая ул., д. 9",
                "address": "Новая ул., д. 9",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.876325",
                "lon": "37.512682",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7705-011": {
                "id": 33939,
                "uid": "PICK_POINT_7705-011",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Нововатутинский пр-кт, д. 10",
                "address": "Нововатутинский пр-кт, д. 10",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.517949",
                "lon": "37.34956",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-076": {
                "id": 2885,
                "uid": "PICK_POINT_7702-076",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Новодмитровская Б. ул.,  д. 36/9",
                "address": "Новодмитровская Б. ул.,  д. 36/9",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.805528",
                "lon": "37.584451",
                "workhours": "; пн-сб 10:00-20:00 обед 14:00-14:40; вс 10:00-20:00 обед 14:00-14:40"
            },
            "PICK_POINT_7701-092": {
                "id": 2896,
                "uid": "PICK_POINT_7701-092",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Новокосинская ул., д. 22",
                "address": "Новокосинская ул., д. 22",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.740099",
                "lon": "37.861097",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7702-390": {
                "id": 36907,
                "uid": "PICK_POINT_7702-390",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Новокосинская ул., д.10, к. 1, пом. VI",
                "address": "Новокосинская ул., д.10, к. 1, пом. VI",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.734679",
                "lon": "37.85559",
                "workhours": "; пн-сб 10:00-18:00; вс выходной"
            },
            "PICK_POINT_7702-146": {
                "id": 7279,
                "uid": "PICK_POINT_7702-146",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Новокузнецкая ул., д. 42, стр. 5",
                "address": "Новокузнецкая ул., д. 42, стр. 5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.731562",
                "lon": "37.634689",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-255": {
                "id": 21417,
                "uid": "PICK_POINT_7702-255",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Новослободская ул., д. 3",
                "address": "Новослободская ул., д. 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.779155",
                "lon": "37.600594",
                "workhours": "; пн-вс 10:00-20:45"
            },
            "PICK_POINT_7705-006": {
                "id": 19034,
                "uid": "PICK_POINT_7705-006",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Новочеркасский б., д. 41, к. 3",
                "address": "Новочеркасский б., д. 41, к. 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.648677",
                "lon": "37.735566",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7701-248": {
                "id": 36903,
                "uid": "PICK_POINT_7701-248",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Новоясеневский пр-кт, вл. 7",
                "address": "Новоясеневский пр-кт, вл. 7",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.607611",
                "lon": "37.532427",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-003": {
                "id": 1369,
                "uid": "PICK_POINT_7701-003",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Новоясеневский пр-кт, д. 1",
                "address": "Новоясеневский пр-кт, д. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.619467",
                "lon": "37.50928",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7702-408": {
                "id": 40478,
                "uid": "PICK_POINT_7702-408",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Новоясеневский пр-кт, д. 7",
                "address": "Новоясеневский пр-кт, д. 7",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.607611",
                "lon": "37.532427",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7701-013": {
                "id": 863,
                "uid": "PICK_POINT_7701-013",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Овчинниковский Б. пер., д. 16",
                "address": "Овчинниковский Б. пер., д. 16",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.744487",
                "lon": "37.630079",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-033": {
                "id": 2576,
                "uid": "PICK_POINT_7701-033",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Озерная ул.,  д. 42",
                "address": "Озерная ул.,  д. 42",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.67154",
                "lon": "37.445552",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7705-081": {
                "id": 38412,
                "uid": "PICK_POINT_7705-081",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Озерная ул., д. 29",
                "address": "Озерная ул., д. 29",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.673057",
                "lon": "37.45124",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7705-002": {
                "id": 9491,
                "uid": "PICK_POINT_7705-002",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Окская ул., д. 1, кор.2",
                "address": "Окская ул., д. 1, кор.2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.712158",
                "lon": "37.749822",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-357": {
                "id": 33562,
                "uid": "PICK_POINT_7702-357",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Олимпийская деревня ул., д.4, к. 2, офис 21",
                "address": "Олимпийская деревня ул., д.4, к. 2, офис 21",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.675478",
                "lon": "37.469144",
                "workhours": "; пн-сб выходной; вс выходной"
            },
            "PICK_POINT_7705-078": {
                "id": 40505,
                "uid": "PICK_POINT_7705-078",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Олимпийский пр-кт, д. 26, стр. 1",
                "address": "Олимпийский пр-кт, д. 26, стр. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.790008",
                "lon": "37.622881",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7705-007": {
                "id": 19377,
                "uid": "PICK_POINT_7705-007",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Онежская ул., д. 34, к. 2",
                "address": "Онежская ул., д. 34, к. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.858811",
                "lon": "37.511481",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-047": {
                "id": 2676,
                "uid": "PICK_POINT_7701-047",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Ореховый б-р, д. 22 А",
                "address": "Ореховый б-р, д. 22 А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.611893",
                "lon": "37.732795",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-001": {
                "id": 1961,
                "uid": "PICK_POINT_7701-001",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Ореховый б., д. 14, к. 3",
                "address": "Ореховый б., д. 14, к. 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.609675",
                "lon": "37.720106",
                "workhours": "; пн-вс 10:00-23:59"
            },
            "PICK_POINT_7702-273": {
                "id": 40472,
                "uid": "PICK_POINT_7702-273",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Осенний б-р, д. 12, к. 1",
                "address": "Осенний б-р, д. 12, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.758468",
                "lon": "37.409325",
                "workhours": "; пн-сб 10:00-21:00; вс 10:00-20:00"
            },
            "PICK_POINT_7705-016": {
                "id": 36284,
                "uid": "PICK_POINT_7705-016",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Осенний б-р, д. 21",
                "address": "Осенний б-р, д. 21",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.763275",
                "lon": "37.405983",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-138": {
                "id": 10593,
                "uid": "PICK_POINT_7701-138",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Осенний б., д. 12",
                "address": "Осенний б., д. 12",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.759238",
                "lon": "37.409244",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7702-275": {
                "id": 40473,
                "uid": "PICK_POINT_7702-275",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Останкинская 1-я ул., д. 55",
                "address": "Останкинская 1-я ул., д. 55",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.824005",
                "lon": "37.634434",
                "workhours": "; пн-сб 10:00-20:00; вс 10:00-20:00"
            },
            "PICK_POINT_7705-019": {
                "id": 35589,
                "uid": "PICK_POINT_7705-019",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Островитянова ул., д. 13",
                "address": "Островитянова ул., д. 13",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.640848",
                "lon": "37.50275",
                "workhours": "; пн-вс 09:00-23:00"
            },
            "PICK_POINT_7701-010": {
                "id": 845,
                "uid": "PICK_POINT_7701-010",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Открытое ш.,  д. 9",
                "address": "Открытое ш.,  д. 9",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.815319",
                "lon": "37.737845",
                "workhours": "; пн-вс 10:30-21:00"
            },
            "PICK_POINT_7701-144": {
                "id": 10734,
                "uid": "PICK_POINT_7701-144",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Открытое ш., д. 5А",
                "address": "Открытое ш., д. 5А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.814095",
                "lon": "37.733504",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-265": {
                "id": 35934,
                "uid": "PICK_POINT_7701-265",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Открытое шоссе, д. 17, стр. 13",
                "address": "Открытое шоссе, д. 17, стр. 13",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.822691",
                "lon": "37.753814",
                "workhours": "; пн-вс 00:00-23:59"
            },
            "PICK_POINT_7705-039": {
                "id": 36581,
                "uid": "PICK_POINT_7705-039",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Первомайская ул., д. 121",
                "address": "Первомайская ул., д. 121",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.793844",
                "lon": "37.821499",
                "workhours": "; пн-вс 09:00-22:00"
            },
            "PICK_POINT_7701-162": {
                "id": 19374,
                "uid": "PICK_POINT_7701-162",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Первомайская ул., д. 42",
                "address": "Первомайская ул., д. 42",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.792265",
                "lon": "37.787111",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-208": {
                "id": 33181,
                "uid": "PICK_POINT_7701-208",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Перерва ул., д. 32",
                "address": "Перерва ул., д. 32",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.652771",
                "lon": "37.734174",
                "workhours": "; пн-вс 09:00-23:00"
            },
            "PICK_POINT_7702-208": {
                "id": 10499,
                "uid": "PICK_POINT_7702-208",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Перерва ул., д. 45",
                "address": "Перерва ул., д. 45",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.659998",
                "lon": "37.748483",
                "workhours": "; пн-сб 10:00-21:00; вс 10:00-20:00"
            },
            "PICK_POINT_7701-155": {
                "id": 13155,
                "uid": "PICK_POINT_7701-155",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Перовская ул., д. 31",
                "address": "Перовская ул., д. 31",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.747105",
                "lon": "37.774598",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7701-197": {
                "id": 25438,
                "uid": "PICK_POINT_7701-197",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Перовская ул., д. 61 А",
                "address": "Перовская ул., д. 61 А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.745275",
                "lon": "37.797442",
                "workhours": "; пн-вс 09:00-22:00"
            },
            "PICK_POINT_7701-193": {
                "id": 40275,
                "uid": "PICK_POINT_7701-193",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Петрозаводская ул., 24Б",
                "address": "Петрозаводская ул., 24Б",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.867071",
                "lon": "37.493587",
                "workhours": "; пн-вс 09:00-22:00"
            },
            "PICK_POINT_7702-043": {
                "id": 2658,
                "uid": "PICK_POINT_7702-043",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Планерная ул., д. 7",
                "address": "Планерная ул., д. 7",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.862897",
                "lon": "37.435041",
                "workhours": "; пн-сб 10:00-17:00; вс выходной"
            },
            "PICK_POINT_7701-104": {
                "id": 2950,
                "uid": "PICK_POINT_7701-104",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Покрышкина ул., д. 8, к. 3",
                "address": "Покрышкина ул., д. 8, к. 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.665227",
                "lon": "37.472086",
                "workhours": "; пн-вс 08:00-22:00"
            },
            "PICK_POINT_7702-410": {
                "id": 40865,
                "uid": "PICK_POINT_7702-410",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Полярная ул., д. 10, стр. 1",
                "address": "Полярная ул., д. 10, стр. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.874561",
                "lon": "37.63827",
                "workhours": "; пн-сб 11:00-18:00; вс 11:00-18:00"
            },
            "PICK_POINT_7701-244": {
                "id": 33782,
                "uid": "PICK_POINT_7701-244",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Полярная ул., д. 21",
                "address": "Полярная ул., д. 21",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.879228",
                "lon": "37.634973",
                "workhours": "; пн-вс 08:00-22:00"
            },
            "PICK_POINT_7701-170": {
                "id": 21286,
                "uid": "PICK_POINT_7701-170",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Преображенская пл., д. 8",
                "address": "Преображенская пл., д. 8",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.794887",
                "lon": "37.712812",
                "workhours": "; пн-вс 00:00-23:59"
            },
            "PICK_POINT_7705-035": {
                "id": 37019,
                "uid": "PICK_POINT_7705-035",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Преображенский Вал ул., д. 24, к. 2",
                "address": "Преображенский Вал ул., д. 24, к. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.791542",
                "lon": "37.714887",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-372": {
                "id": 35897,
                "uid": "PICK_POINT_7702-372",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Пржевальского ул., д. 2",
                "address": "Пржевальского ул., д. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.681894",
                "lon": "37.451339",
                "workhours": "; пн-вс 10:00-20:00"
            },
            "PICK_POINT_7701-299": {
                "id": 42835,
                "uid": "PICK_POINT_7701-299",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Привольная ул., д. 70  к. 1",
                "address": "Привольная ул., д. 70  к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.677908",
                "lon": "37.85449",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7701-235": {
                "id": 29623,
                "uid": "PICK_POINT_7701-235",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Пришвина ул., вл. 3Г",
                "address": "Пришвина ул., вл. 3Г",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.889951",
                "lon": "37.594269",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7701-228": {
                "id": 35365,
                "uid": "PICK_POINT_7701-228",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Пролетарский пр-кт, д. 30",
                "address": "Пролетарский пр-кт, д. 30",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.63017",
                "lon": "37.658762",
                "workhours": "; пн-вс 08:00-22:00"
            },
            "PICK_POINT_7701-234": {
                "id": 33182,
                "uid": "PICK_POINT_7701-234",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Просторная ул., д. 8",
                "address": "Просторная ул., д. 8",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.805038",
                "lon": "37.717514",
                "workhours": "; пн-вс 09:00-23:00"
            },
            "PICK_POINT_7701-105": {
                "id": 4724,
                "uid": "PICK_POINT_7701-105",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Профсоюзная ул., влад. 118",
                "address": "Профсоюзная ул., влад. 118",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.636224",
                "lon": "37.521076",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7702-261": {
                "id": 21457,
                "uid": "PICK_POINT_7702-261",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Профсоюзная ул., д. 102",
                "address": "Профсоюзная ул., д. 102",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.643658",
                "lon": "37.526232",
                "workhours": "; пн-вс 09:00-21:45"
            },
            "PICK_POINT_7702-154": {
                "id": 8378,
                "uid": "PICK_POINT_7702-154",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Профсоюзная ул., д. 109",
                "address": "Профсоюзная ул., д. 109",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.633013",
                "lon": "37.52156",
                "workhours": "; пн-сб 12:00-19:00; вс 12:00-19:00"
            },
            "PICK_POINT_7701-191": {
                "id": 22077,
                "uid": "PICK_POINT_7701-191",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Профсоюзная ул., д. 126, к. 3",
                "address": "Профсоюзная ул., д. 126, к. 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.63037",
                "lon": "37.516844",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7705-103": {
                "id": 42869,
                "uid": "PICK_POINT_7705-103",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Профсоюзная ул., д. 128, к. 3",
                "address": "Профсоюзная ул., д. 128, к. 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.63061",
                "lon": "37.514747",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-322": {
                "id": 25819,
                "uid": "PICK_POINT_7702-322",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Профсоюзная ул., д. 3, офис 120",
                "address": "Профсоюзная ул., д. 3, офис 120",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.685726",
                "lon": "37.572396",
                "workhours": "; пн-сб 11:00-18:00; вс выходной"
            },
            "PICK_POINT_7701-150": {
                "id": 11426,
                "uid": "PICK_POINT_7701-150",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Профсоюзная ул., д. 56",
                "address": "Профсоюзная ул., д. 56",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.670121",
                "lon": "37.552443",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7702-407": {
                "id": 40553,
                "uid": "PICK_POINT_7702-407",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Профсоюзная ул., д. 64/66",
                "address": "Профсоюзная ул., д. 64/66",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.665975",
                "lon": "37.549067",
                "workhours": "; пн-вс 10:00-20:00"
            },
            "PICK_POINT_7701-079": {
                "id": 843,
                "uid": "PICK_POINT_7701-079",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Пятницкий пер.,  д. 2",
                "address": "Пятницкий пер.,  д. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.743382",
                "lon": "37.629523",
                "workhours": "; пн-сб 09:00-19:00; вс 09:00-19:00"
            },
            "PICK_POINT_7701-063": {
                "id": 2742,
                "uid": "PICK_POINT_7701-063",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Пятницкое ш. (Митино),  д. 43",
                "address": "Пятницкое ш. (Митино),  д. 43",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.857057",
                "lon": "37.350185",
                "workhours": "; пн-вс 09:00-22:00"
            },
            "PICK_POINT_7701-161": {
                "id": 18913,
                "uid": "PICK_POINT_7701-161",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Пятницкое ш. (Митино), д. 39",
                "address": "Пятницкое ш. (Митино), д. 39",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.855153",
                "lon": "37.355498",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7705-033": {
                "id": 36390,
                "uid": "PICK_POINT_7705-033",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Раменки ул., д. 16",
                "address": "Раменки ул., д. 16",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.6901",
                "lon": "37.489724",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-349": {
                "id": 33238,
                "uid": "PICK_POINT_7702-349",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Рождественская ул., д. 23/33",
                "address": "Рождественская ул., д. 23/33",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.705488",
                "lon": "37.920736",
                "workhours": "; пн-сб 11:00-16:00; вс выходной"
            },
            "PICK_POINT_7701-173": {
                "id": 21155,
                "uid": "PICK_POINT_7701-173",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Рокотова ул., д. 5",
                "address": "Рокотова ул., д. 5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.610224",
                "lon": "37.551672",
                "workhours": "; пн-вс 10:00-20:00"
            },
            "PICK_POINT_7705-054": {
                "id": 36911,
                "uid": "PICK_POINT_7705-054",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Россошанская ул., д. 13, к. 1",
                "address": "Россошанская ул., д. 13, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.595852",
                "lon": "37.614302",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-286": {
                "id": 42771,
                "uid": "PICK_POINT_7701-286",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Россошанская ул., д. 2, стр. 1",
                "address": "Россошанская ул., д. 2, стр. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.594336",
                "lon": "37.60571",
                "workhours": "; пн-вс 00:00-23:59"
            },
            "PICK_POINT_7701-169": {
                "id": 20876,
                "uid": "PICK_POINT_7701-169",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Россошанский пр., д. 3",
                "address": "Россошанский пр., д. 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.600974",
                "lon": "37.609191",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7705-022": {
                "id": 35758,
                "uid": "PICK_POINT_7705-022",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Рублевское шоссе, д. 91, к. 1",
                "address": "Рублевское шоссе, д. 91, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.740755",
                "lon": "37.427246",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-280": {
                "id": 37838,
                "uid": "PICK_POINT_7701-280",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Рудневка ул., д. 5",
                "address": "Рудневка ул., д. 5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.715718",
                "lon": "37.881569",
                "workhours": "; пн-вс 09:00-22:00"
            },
            "PICK_POINT_7701-056": {
                "id": 1501,
                "uid": "PICK_POINT_7701-056",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Рудневка ул.(Кожухово),  д. 19",
                "address": "Рудневка ул.(Кожухово),  д. 19",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.716576",
                "lon": "37.887387",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7702-379": {
                "id": 36429,
                "uid": "PICK_POINT_7702-379",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Рязанский пр-кт, д. 24, к. 2",
                "address": "Рязанский пр-кт, д. 24, к. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.720722",
                "lon": "37.774463",
                "workhours": "; пн-вт 12:00-21:00 обед 15:00-16:00; ср-вс выходной"
            },
            "PICK_POINT_7701-114": {
                "id": 8330,
                "uid": "PICK_POINT_7701-114",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Рязанский пр-кт, д. 28, стр. 1",
                "address": "Рязанский пр-кт, д. 28, стр. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.719146",
                "lon": "37.778962",
                "workhours": "; пн-вс 00:00-23:59"
            },
            "PICK_POINT_7705-052": {
                "id": 36583,
                "uid": "PICK_POINT_7705-052",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Рязанский пр-кт, д. 32, к. 3",
                "address": "Рязанский пр-кт, д. 32, к. 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.717874",
                "lon": "37.78454",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-135": {
                "id": 7181,
                "uid": "PICK_POINT_7702-135",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Рязанский пр-кт, д. 99 а",
                "address": "Рязанский пр-кт, д. 99 а",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.712654",
                "lon": "37.816319",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7701-012": {
                "id": 2528,
                "uid": "PICK_POINT_7701-012",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Рязанский пр-кт., д. 2, к. 2",
                "address": "Рязанский пр-кт., д. 2, к. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.729833",
                "lon": "37.731021",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-120": {
                "id": 7225,
                "uid": "PICK_POINT_7701-120",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Саянская ул., д. 10 А",
                "address": "Саянская ул., д. 10 А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.767087",
                "lon": "37.830258",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-118": {
                "id": 5284,
                "uid": "PICK_POINT_7701-118",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Свободный пр-кт, д. 20 а",
                "address": "Свободный пр-кт, д. 20 а",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.751248",
                "lon": "37.816266",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-086": {
                "id": 7131,
                "uid": "PICK_POINT_7701-086",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Свободный пр-кт, д. 33",
                "address": "Свободный пр-кт, д. 33",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.752784",
                "lon": "37.819296",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7702-083": {
                "id": 13656,
                "uid": "PICK_POINT_7702-083",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Свободный пр-кт, д. 33, к. А",
                "address": "Свободный пр-кт, д. 33, к. А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.752683",
                "lon": "37.818283",
                "workhours": "; пн-вс 10:00-20:00"
            },
            "PICK_POINT_7702-044": {
                "id": 2674,
                "uid": "PICK_POINT_7702-044",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Свободный пр-кт., д. 33",
                "address": "Свободный пр-кт., д. 33",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.752784",
                "lon": "37.819307",
                "workhours": "; пн-вс 10:15-21:45"
            },
            "PICK_POINT_7705-028": {
                "id": 36389,
                "uid": "PICK_POINT_7705-028",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Свободы ул., д. 29",
                "address": "Свободы ул., д. 29",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.837025",
                "lon": "37.452875",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-368": {
                "id": 35516,
                "uid": "PICK_POINT_7702-368",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Свободы ул., д. 48  стр. 1",
                "address": "Свободы ул., д. 48  стр. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.848427",
                "lon": "37.454304",
                "workhours": "; пн-сб 10:00-18:00; вс выходной"
            },
            "PICK_POINT_7701-160": {
                "id": 21934,
                "uid": "PICK_POINT_7701-160",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Севастопольский пр-кт, д. 11 Е",
                "address": "Севастопольский пр-кт, д. 11 Е",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.688281",
                "lon": "37.604605",
                "workhours": "; пн-вс 09:00-23:00"
            },
            "PICK_POINT_7702-397": {
                "id": 37750,
                "uid": "PICK_POINT_7702-397",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Сельскохозяйственная ул., д. 17, к. 1",
                "address": "Сельскохозяйственная ул., д. 17, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.837414",
                "lon": "37.636356",
                "workhours": "; пн-сб 11:00-18:00; вс 11:00-18:00"
            },
            "PICK_POINT_7702-056": {
                "id": 2683,
                "uid": "PICK_POINT_7702-056",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Семеновская Б. ул., д. 16",
                "address": "Семеновская Б. ул., д. 16",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.781919",
                "lon": "37.704017",
                "workhours": "; пн-вс 10:00-21:45"
            },
            "PICK_POINT_7701-035": {
                "id": 2618,
                "uid": "PICK_POINT_7701-035",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Семеновская М. ул., д. 30, стр. 6",
                "address": "Семеновская М. ул., д. 30, стр. 6",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.78398",
                "lon": "37.717336",
                "workhours": "; пн-сб 10:00-19:00; вс 10:00-19:00"
            },
            "PICK_POINT_7705-014": {
                "id": 35750,
                "uid": "PICK_POINT_7705-014",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Сигнальный проезд, д. 6А",
                "address": "Сигнальный проезд, д. 6А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.848558",
                "lon": "37.590769",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7705-086": {
                "id": 38756,
                "uid": "PICK_POINT_7705-086",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Симферопольский бульвар, д. 11, к. 1",
                "address": "Симферопольский бульвар, д. 11, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.657992",
                "lon": "37.607493",
                "workhours": "; пн-вс 08:00-20:00"
            },
            "PICK_POINT_7702-213": {
                "id": 10556,
                "uid": "PICK_POINT_7702-213",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Симферопольский пр., д. 7",
                "address": "Симферопольский пр., д. 7",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.665004",
                "lon": "37.61549",
                "workhours": "; пн-сб выходной; вс выходной"
            },
            "PICK_POINT_7701-226": {
                "id": 32966,
                "uid": "PICK_POINT_7701-226",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Сколковское шоссе, д. 27",
                "address": "Сколковское шоссе, д. 27",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.707852",
                "lon": "37.404923",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7702-183": {
                "id": 9459,
                "uid": "PICK_POINT_7702-183",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Славянский бульв., д.5, к.1",
                "address": "Славянский бульв., д.5, к.1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.727009",
                "lon": "37.471587",
                "workhours": "; пн-сб 08:00-19:00; вс 08:00-19:00"
            },
            "PICK_POINT_7701-062": {
                "id": 2724,
                "uid": "PICK_POINT_7701-062",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Смольная ул., д. 24 Б",
                "address": "Смольная ул., д. 24 Б",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.860827",
                "lon": "37.483397",
                "workhours": "; пн-вс 09:00-20:00"
            },
            "PICK_POINT_7701-292": {
                "id": 42678,
                "uid": "PICK_POINT_7701-292",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Смольная ул., д. 7",
                "address": "Смольная ул., д. 7",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.845788",
                "lon": "37.499483",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7701-108": {
                "id": 4745,
                "uid": "PICK_POINT_7701-108",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Смольная ул., д.63Б",
                "address": "Смольная ул., д.63Б",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.870184",
                "lon": "37.469829",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7701-103": {
                "id": 2944,
                "uid": "PICK_POINT_7701-103",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Снежная ул., д. 16, к.1",
                "address": "Снежная ул., д. 16, к.1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.851249",
                "lon": "37.645933",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7701-127": {
                "id": 9488,
                "uid": "PICK_POINT_7701-127",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Снежная ул., д. 27",
                "address": "Снежная ул., д. 27",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.856351",
                "lon": "37.653334",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-205": {
                "id": 25123,
                "uid": "PICK_POINT_7701-205",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Соболевский проезд, д. 22, стр. 1",
                "address": "Соболевский проезд, д. 22, стр. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.833804",
                "lon": "37.523132",
                "workhours": "; пн-вс 00:00-23:59"
            },
            "PICK_POINT_7701-222": {
                "id": 29479,
                "uid": "PICK_POINT_7701-222",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Совхозная ул., д. 39",
                "address": "Совхозная ул., д. 39",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.673768",
                "lon": "37.760422",
                "workhours": "; пн-вс 09:00-23:00"
            },
            "PICK_POINT_7701-214": {
                "id": 25636,
                "uid": "PICK_POINT_7701-214",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Соколово-Мещерская ул., д. 14",
                "address": "Соколово-Мещерская ул., д. 14",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.896009",
                "lon": "37.390038",
                "workhours": "; пн-вс 09:00-22:00"
            },
            "PICK_POINT_7701-113": {
                "id": 7209,
                "uid": "PICK_POINT_7701-113",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Сокольническая пл., д. 4 А",
                "address": "Сокольническая пл., д. 4 А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.789991",
                "lon": "37.678568",
                "workhours": "; пн-вс 10:00-20:00"
            },
            "PICK_POINT_7702-058": {
                "id": 2703,
                "uid": "PICK_POINT_7702-058",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Сокольническая пл., д. 9",
                "address": "Сокольническая пл., д. 9",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.789917",
                "lon": "37.680809",
                "workhours": "; пн-вс 09:00-20:45"
            },
            "PICK_POINT_7705-079": {
                "id": 40238,
                "uid": "PICK_POINT_7705-079",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Сокольнический Вaл ул., д. 8",
                "address": "Сокольнический Вaл ул., д. 8",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.792129",
                "lon": "37.660135",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-244": {
                "id": 20739,
                "uid": "PICK_POINT_7702-244",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Солнцевский пр-т., д. 11",
                "address": "Солнцевский пр-т., д. 11",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.649664",
                "lon": "37.40403",
                "workhours": "; пн-сб 10:00-18:00; вс 10:00-18:00"
            },
            "PICK_POINT_7702-270": {
                "id": 40549,
                "uid": "PICK_POINT_7702-270",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Старокачаловская ул., д. 1Б",
                "address": "Старокачаловская ул., д. 1Б",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.568558",
                "lon": "37.585386",
                "workhours": "; пн-вс 10:00-20:00"
            },
            "PICK_POINT_7701-123": {
                "id": 8329,
                "uid": "PICK_POINT_7701-123",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Старокачаловская ул., д. 5А",
                "address": "Старокачаловская ул., д. 5А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.569932",
                "lon": "37.579062",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7705-090": {
                "id": 40130,
                "uid": "PICK_POINT_7705-090",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Старокрымская ул., д. 15 к. 1",
                "address": "Старокрымская ул., д. 15 к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.557948",
                "lon": "37.557268",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-007": {
                "id": 1741,
                "uid": "PICK_POINT_7701-007",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Строгинский б-р, д. 1",
                "address": "Строгинский б-р, д. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.806039",
                "lon": "37.395572",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7705-071": {
                "id": 38409,
                "uid": "PICK_POINT_7705-071",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Строгинский б-р, д. 9",
                "address": "Строгинский б-р, д. 9",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.804949",
                "lon": "37.400146",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-046": {
                "id": 723,
                "uid": "PICK_POINT_7701-046",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Сущевский Вал ул., д. 31, стр. 1",
                "address": "Сущевский Вал ул., д. 31, стр. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.793416",
                "lon": "37.604105",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7702-274": {
                "id": 40567,
                "uid": "PICK_POINT_7702-274",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Сущевский Вал ул., д. 46",
                "address": "Сущевский Вал ул., д. 46",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.792701",
                "lon": "37.612605",
                "workhours": "; пн-сб 10:00-21:00; вс 10:00-20:00"
            },
            "PICK_POINT_7702-378": {
                "id": 36426,
                "uid": "PICK_POINT_7702-378",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Сущевский Вал ул., д. 5, к. 1А, пав. 2F-29",
                "address": "Сущевский Вал ул., д. 5, к. 1А, пав. 2F-29",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.79434",
                "lon": "37.592626",
                "workhours": "; пн-вс 11:00-20:00 обед 14:00-15:00"
            },
            "PICK_POINT_7702-293": {
                "id": 24502,
                "uid": "PICK_POINT_7702-293",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Сущевский Вал ул., д. 5/ 20,  пав. Т-1",
                "address": "Сущевский Вал ул., д. 5/ 20,  пав. Т-1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.79393",
                "lon": "37.593345",
                "workhours": "; пн-вс выходной"
            },
            "PICK_POINT_7702-060": {
                "id": 2883,
                "uid": "PICK_POINT_7702-060",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Сходненская ул.,  д. 56",
                "address": "Сходненская ул.,  д. 56",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.850434",
                "lon": "37.44497",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-186": {
                "id": 22236,
                "uid": "PICK_POINT_7701-186",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Сходненская ул., д. 25",
                "address": "Сходненская ул., д. 25",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.845501",
                "lon": "37.438754",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7702-015": {
                "id": 2525,
                "uid": "PICK_POINT_7702-015",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Таганская ул., д. 1, стр. 1",
                "address": "Таганская ул., д. 1, стр. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.741772",
                "lon": "37.655975",
                "workhours": "; пн-вс 10:00-21:45"
            },
            "PICK_POINT_7702-185": {
                "id": 9461,
                "uid": "PICK_POINT_7702-185",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Таганская ул., д.31/22",
                "address": "Таганская ул., д.31/22",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.739676",
                "lon": "37.669639",
                "workhours": "; пн-сб 08:00-19:00; вс 08:00-19:00"
            },
            "PICK_POINT_7702-383": {
                "id": 36664,
                "uid": "PICK_POINT_7702-383",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Таллиннская ул., д. 26",
                "address": "Таллиннская ул., д. 26",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.799881",
                "lon": "37.407627",
                "workhours": "; пн-вс 10:00-20:45 обед 13:00-14:00"
            },
            "PICK_POINT_7702-356": {
                "id": 33881,
                "uid": "PICK_POINT_7702-356",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Таллиннская ул., д. 26, офис 17",
                "address": "Таллиннская ул., д. 26, офис 17",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.799456",
                "lon": "37.407852",
                "workhours": "; пн-сб 12:00-18:00; вс выходной"
            },
            "PICK_POINT_7702-241": {
                "id": 20486,
                "uid": "PICK_POINT_7702-241",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Таллинская ул.,д. 7",
                "address": "Таллинская ул.,д. 7",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.800468",
                "lon": "37.396129",
                "workhours": "; пн-сб 10:00-20:00; вс 10:00-20:00"
            },
            "PICK_POINT_7701-002": {
                "id": 1242,
                "uid": "PICK_POINT_7701-002",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Тараса Шевченко наб., д. 23 А",
                "address": "Тараса Шевченко наб., д. 23 А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.746278",
                "lon": "37.545895",
                "workhours": "; пн-вс 07:30-23:59"
            },
            "PICK_POINT_7705-089": {
                "id": 40065,
                "uid": "PICK_POINT_7705-089",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Татьянин Парк ул., д. 17, к. 1",
                "address": "Татьянин Парк ул., д. 17, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.657515",
                "lon": "37.424825",
                "workhours": "; пн-вс 09:00-23:00"
            },
            "PICK_POINT_7702-367": {
                "id": 35510,
                "uid": "PICK_POINT_7702-367",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Ташкентская ул., д. 9",
                "address": "Ташкентская ул., д. 9",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.708227",
                "lon": "37.819244",
                "workhours": "; пн-сб 12:00-19:00; вс выходной"
            },
            "PICK_POINT_7705-101": {
                "id": 42330,
                "uid": "PICK_POINT_7705-101",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Тверская ул.,  д. 9",
                "address": "Тверская ул.,  д. 9",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.759856",
                "lon": "37.610548",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-064": {
                "id": 2758,
                "uid": "PICK_POINT_7701-064",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Театральная аллея, д. 3, стр. 1",
                "address": "Театральная аллея, д. 3, стр. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.793206",
                "lon": "37.560055",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-227": {
                "id": 29608,
                "uid": "PICK_POINT_7701-227",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Теплый стан ул., д. 10",
                "address": "Теплый стан ул., д. 10",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.629094",
                "lon": "37.485861",
                "workhours": "; пн-вс 09:30-21:00"
            },
            "PICK_POINT_7701-060": {
                "id": 2003,
                "uid": "PICK_POINT_7701-060",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Тимирязевская ул.,  д. 2/3",
                "address": "Тимирязевская ул.,  д. 2/3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.807818",
                "lon": "37.573478",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7702-210": {
                "id": 20514,
                "uid": "PICK_POINT_7702-210",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Тимирязевская ул., д. 2/3",
                "address": "Тимирязевская ул., д. 2/3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.807909",
                "lon": "37.57351",
                "workhours": "; пн-сб 10:00-21:00; вс 10:00-20:00"
            },
            "PICK_POINT_7701-285": {
                "id": 40642,
                "uid": "PICK_POINT_7701-285",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Тимура Фрунзе ул., д. 11, стр. 13",
                "address": "Тимура Фрунзе ул., д. 11, стр. 13",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.734653",
                "lon": "37.588422",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-053": {
                "id": 2704,
                "uid": "PICK_POINT_7701-053",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Тишинская пл., д. 1",
                "address": "Тишинская пл., д. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.769144",
                "lon": "37.583151",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7702-147": {
                "id": 7323,
                "uid": "PICK_POINT_7702-147",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Трофимова ул., д. 35/20",
                "address": "Трофимова ул., д. 35/20",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.704817",
                "lon": "37.684862",
                "workhours": "; пн-сб выходной; вс выходной"
            },
            "PICK_POINT_7701-004": {
                "id": 653,
                "uid": "PICK_POINT_7701-004",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Тульская  Б. ул., д. 13",
                "address": "Тульская  Б. ул., д. 13",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.708729",
                "lon": "37.622091",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-142": {
                "id": 10188,
                "uid": "PICK_POINT_7701-142",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Тульская  Б. ул., д. 2",
                "address": "Тульская  Б. ул., д. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.709028",
                "lon": "37.620761",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7701-015": {
                "id": 2530,
                "uid": "PICK_POINT_7701-015",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Тушинская ул.,  д. 17",
                "address": "Тушинская ул.,  д. 17",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.826679",
                "lon": "37.445097",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-266": {
                "id": 40470,
                "uid": "PICK_POINT_7702-266",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Тушинская ул., д. 17",
                "address": "Тушинская ул., д. 17",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.82666",
                "lon": "37.44479",
                "workhours": "; пн-сб 10:00-20:00; вс 10:00-20:00"
            },
            "PICK_POINT_7705-066": {
                "id": 37776,
                "uid": "PICK_POINT_7705-066",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Удальцова ул., д. 75 А",
                "address": "Удальцова ул., д. 75 А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.678148",
                "lon": "37.49594",
                "workhours": "; пн-вс 00:00-23:59"
            },
            "PICK_POINT_7705-100": {
                "id": 42331,
                "uid": "PICK_POINT_7705-100",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Уланский пер., д. 14, к.Б",
                "address": "Уланский пер., д. 14, к.Б",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.76879",
                "lon": "37.640479",
                "workhours": "; пн-вс выходной"
            },
            "PICK_POINT_7702-045": {
                "id": 2675,
                "uid": "PICK_POINT_7702-045",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Уральская ул., д. 1 А",
                "address": "Уральская ул., д. 1 А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.810572",
                "lon": "37.797837",
                "workhours": "; пн-вс 10:00-20:45"
            },
            "PICK_POINT_7705-040": {
                "id": 36914,
                "uid": "PICK_POINT_7705-040",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Уральская ул., д. 6, к. 1",
                "address": "Уральская ул., д. 6, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.813723",
                "lon": "37.799328",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-328": {
                "id": 25728,
                "uid": "PICK_POINT_7702-328",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Ухтомского Ополчения ул., д. 1",
                "address": "Ухтомского Ополчения ул., д. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.701054",
                "lon": "37.924374",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7705-010": {
                "id": 24637,
                "uid": "PICK_POINT_7705-010",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Фабрициуса ул., д. 44, к. 1",
                "address": "Фабрициуса ул., д. 44, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.844308",
                "lon": "37.429824",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7705-063": {
                "id": 37332,
                "uid": "PICK_POINT_7705-063",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Ферганскaя ул., д. 8, к. 2, стр. 1",
                "address": "Ферганскaя ул., д. 8, к. 2, стр. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.708907",
                "lon": "37.803865",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-301": {
                "id": 42834,
                "uid": "PICK_POINT_7701-301",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Ферганская ул.,  д. 6, к. 2",
                "address": "Ферганская ул.,  д. 6, к. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.709695",
                "lon": "37.80311",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7702-084": {
                "id": 13715,
                "uid": "PICK_POINT_7702-084",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Фестивальная ул., д. 13, к. 1",
                "address": "Фестивальная ул., д. 13, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.85578",
                "lon": "37.477381",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-236": {
                "id": 32902,
                "uid": "PICK_POINT_7701-236",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Фестивальная ул., д. 2Б",
                "address": "Фестивальная ул., д. 2Б",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.855122",
                "lon": "37.478228",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-163": {
                "id": 19294,
                "uid": "PICK_POINT_7701-163",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Филевская М. ул., д. 13",
                "address": "Филевская М. ул., д. 13",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.736387",
                "lon": "37.467383",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7701-287": {
                "id": 40182,
                "uid": "PICK_POINT_7701-287",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Филевский б-р, д. 10",
                "address": "Филевский б-р, д. 10",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.76406",
                "lon": "37.487371",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-078": {
                "id": 2887,
                "uid": "PICK_POINT_7702-078",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Фридриха Энгельса ул., д. 23, стр. 4",
                "address": "Фридриха Энгельса ул., д. 23, стр. 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.773086",
                "lon": "37.683622",
                "workhours": "; пн-сб 10:00-20:00; вс 10:00-20:00"
            },
            "PICK_POINT_7705-027": {
                "id": 36285,
                "uid": "PICK_POINT_7705-027",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Фруктовая ул., д. 8, к. 3",
                "address": "Фруктовая ул., д. 8, к. 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.661674",
                "lon": "37.616512",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-237": {
                "id": 35370,
                "uid": "PICK_POINT_7701-237",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Фрунзенская наб., д.30, стр. 2",
                "address": "Фрунзенская наб., д.30, стр. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.72563",
                "lon": "37.583824",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7701-037": {
                "id": 2623,
                "uid": "PICK_POINT_7701-037",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Хабаровская ул., д. 15",
                "address": "Хабаровская ул., д. 15",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.822493",
                "lon": "37.823358",
                "workhours": "; пн-вс 09:00-22:00"
            },
            "PICK_POINT_7705-098": {
                "id": 40873,
                "uid": "PICK_POINT_7705-098",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Хабаровская ул., д. 6  к. 1",
                "address": "Хабаровская ул., д. 6  к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.817633",
                "lon": "37.828416",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-130": {
                "id": 8825,
                "uid": "PICK_POINT_7701-130",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Хабаровская ул., д.12/23",
                "address": "Хабаровская ул., д.12/23",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.820941",
                "lon": "37.826691",
                "workhours": "; пн-сб 09:00-19:00; вс 09:00-19:00"
            },
            "PICK_POINT_7701-293": {
                "id": 42679,
                "uid": "PICK_POINT_7701-293",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Хачатуряна ул., д.  7",
                "address": "Хачатуряна ул., д.  7",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.85813",
                "lon": "37.593589",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7705-021": {
                "id": 35946,
                "uid": "PICK_POINT_7705-021",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Хвалынский б-р, д. 7/11",
                "address": "Хвалынский б-р, д. 7/11",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.696915",
                "lon": "37.850362",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7705-102": {
                "id": 42579,
                "uid": "PICK_POINT_7705-102",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Херсонская ул., д. 43",
                "address": "Херсонская ул., д. 43",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.659231",
                "lon": "37.560835",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7702-127": {
                "id": 2926,
                "uid": "PICK_POINT_7702-127",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Холодильный пер., д. 3, ряд 1, пом. 11 А",
                "address": "Холодильный пер., д. 3, ряд 1, пом. 11 А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.708516",
                "lon": "37.625551",
                "workhours": "; пн-сб выходной; вс выходной"
            },
            "PICK_POINT_7702-388": {
                "id": 36857,
                "uid": "PICK_POINT_7702-388",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Часовая ул., д. 11/3, пав. 11",
                "address": "Часовая ул., д. 11/3, пав. 11",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.80847",
                "lon": "37.529826",
                "workhours": "; пн-вс 10:00-21:00 обед 13:30-14:30"
            },
            "PICK_POINT_7701-089": {
                "id": 1161,
                "uid": "PICK_POINT_7701-089",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Часовая ул., д. 16",
                "address": "Часовая ул., д. 16",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.808849",
                "lon": "37.530133",
                "workhours": "; пн-вс 10:00-20:00"
            },
            "PICK_POINT_7705-032": {
                "id": 36395,
                "uid": "PICK_POINT_7705-032",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Челябинская ул., д. 15",
                "address": "Челябинская ул., д. 15",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.778841",
                "lon": "37.827589",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7705-037": {
                "id": 36431,
                "uid": "PICK_POINT_7705-037",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Черепановых проезд, д. 68",
                "address": "Черепановых проезд, д. 68",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.845443",
                "lon": "37.550584",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7705-004": {
                "id": 18196,
                "uid": "PICK_POINT_7705-004",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Череповецкая ул., д. 12",
                "address": "Череповецкая ул., д. 12",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.897826",
                "lon": "37.572252",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-189": {
                "id": 22275,
                "uid": "PICK_POINT_7701-189",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Череповецкая ул., д. 4 А",
                "address": "Череповецкая ул., д. 4 А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.897988",
                "lon": "37.565793",
                "workhours": "; пн-вс 10:00-18:00"
            },
            "PICK_POINT_7702-253": {
                "id": 21593,
                "uid": "PICK_POINT_7702-253",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Черкизовская Б. ул., д. 3, к.1",
                "address": "Черкизовская Б. ул., д. 3, к.1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.797169",
                "lon": "37.717106",
                "workhours": "; пн-вс 09:00-20:45"
            },
            "PICK_POINT_7705-001": {
                "id": 8493,
                "uid": "PICK_POINT_7705-001",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Чертаново Северное мкр, д. 1а, к. 1",
                "address": "Чертаново Северное мкр, д. 1а, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.637327",
                "lon": "37.597702",
                "workhours": "; пн-вс 08:00-23:59"
            },
            "PICK_POINT_7701-302": {
                "id": 42833,
                "uid": "PICK_POINT_7701-302",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Чечерский проезд, д. 128",
                "address": "Чечерский проезд, д. 128",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.523903",
                "lon": "37.514838",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7705-045": {
                "id": 36952,
                "uid": "PICK_POINT_7705-045",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Чонгарский б-р, д. 18А",
                "address": "Чонгарский б-р, д. 18А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.653611",
                "lon": "37.608383",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-401": {
                "id": 37943,
                "uid": "PICK_POINT_7702-401",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Шаболовка ул., д. 34  стр. 6, офис 8",
                "address": "Шаболовка ул., д. 34  стр. 6, офис 8",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.718811",
                "lon": "37.606487",
                "workhours": "; пн-сб 12:00-19:00; вс выходной"
            },
            "PICK_POINT_7701-080": {
                "id": 2821,
                "uid": "PICK_POINT_7701-080",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Шарикоподшипниковская ул., д. 11, стр. 5",
                "address": "Шарикоподшипниковская ул., д. 11, стр. 5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.718784",
                "lon": "37.676733",
                "workhours": "; пн-вс 09:00-22:00"
            },
            "PICK_POINT_7705-094": {
                "id": 40192,
                "uid": "PICK_POINT_7705-094",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Шверника ул., д. 13, к. 1",
                "address": "Шверника ул., д. 13, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.691495",
                "lon": "37.589255",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7705-017": {
                "id": 35551,
                "uid": "PICK_POINT_7705-017",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Шверника ул., д. 8/1, к. 1",
                "address": "Шверника ул., д. 8/1, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.692627",
                "lon": "37.584299",
                "workhours": "; пн-вс 09:00-23:00"
            },
            "PICK_POINT_7705-026": {
                "id": 36391,
                "uid": "PICK_POINT_7705-026",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Шипиловская ул., д. 32А",
                "address": "Шипиловская ул., д. 32А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.619823",
                "lon": "37.718265",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-371": {
                "id": 35827,
                "uid": "PICK_POINT_7702-371",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Шипиловский проезд, вл. 39, к. 3А",
                "address": "Шипиловский проезд, вл. 39, к. 3А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.612413",
                "lon": "37.69763",
                "workhours": "; пн-вс 10:00-21:00 обед 14:00-15:00"
            },
            "PICK_POINT_7702-373": {
                "id": 35878,
                "uid": "PICK_POINT_7702-373",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Шипиловский проезд, д. 43, к. 2, каб. 1",
                "address": "Шипиловский проезд, д. 43, к. 2, каб. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.611373",
                "lon": "37.699112",
                "workhours": "; пн-сб 10:00-20:00; вс 10:00-20:00"
            },
            "PICK_POINT_7701-107": {
                "id": 4727,
                "uid": "PICK_POINT_7701-107",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Широкая ул., д. 9, кор. 1",
                "address": "Широкая ул., д. 9, кор. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.888275",
                "lon": "37.659377",
                "workhours": "; пн-вс 10:00-22:00"
            },
            "PICK_POINT_7705-092": {
                "id": 40037,
                "uid": "PICK_POINT_7705-092",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Шокальского проезд, д. 2",
                "address": "Шокальского проезд, д. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.872254",
                "lon": "37.646858",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-092": {
                "id": 17499,
                "uid": "PICK_POINT_7702-092",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Шокальского проезд, д. 61, пав. 9",
                "address": "Шокальского проезд, д. 61, пав. 9",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.883989",
                "lon": "37.666782",
                "workhours": "; пн-сб 10:00-19:00; вс 12:00-19:00"
            },
            "PICK_POINT_7705-069": {
                "id": 38114,
                "uid": "PICK_POINT_7705-069",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Шокальского ул., д. 31, к. 1",
                "address": "Шокальского ул., д. 31, к. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.881434",
                "lon": "37.648843",
                "workhours": "; пн-вс 09:00-22:00"
            },
            "PICK_POINT_7705-025": {
                "id": 36287,
                "uid": "PICK_POINT_7705-025",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Шолохова ул., д. 4",
                "address": "Шолохова ул., д. 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.636128",
                "lon": "37.358363",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-264": {
                "id": 35507,
                "uid": "PICK_POINT_7701-264",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Шоссейная ул., д. 2Б",
                "address": "Шоссейная ул., д. 2Б",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.700679",
                "lon": "37.726322",
                "workhours": "; пн-вс 00:00-23:59"
            },
            "PICK_POINT_7702-186": {
                "id": 9462,
                "uid": "PICK_POINT_7702-186",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Щелковское ш. (Москва), д. 89/2",
                "address": "Щелковское ш. (Москва), д. 89/2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.812509",
                "lon": "37.821777",
                "workhours": "; пн-сб 08:00-19:00; вс 08:00-19:00"
            },
            "PICK_POINT_7705-058": {
                "id": 37574,
                "uid": "PICK_POINT_7705-058",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Щелковское шоссе, вл. 91А, стр. 1",
                "address": "Щелковское шоссе, вл. 91А, стр. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.812904",
                "lon": "37.826089",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7702-361": {
                "id": 40570,
                "uid": "PICK_POINT_7702-361",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Щелковское шоссе, д. 68",
                "address": "Щелковское шоссе, д. 68",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.810061",
                "lon": "37.807144",
                "workhours": "; пн-сб 10:00-19:00; вс 10:00-19:00"
            },
            "PICK_POINT_7702-243": {
                "id": 20657,
                "uid": "PICK_POINT_7702-243",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Щербаковская ул., д. 7",
                "address": "Щербаковская ул., д. 7",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.782825",
                "lon": "37.726089",
                "workhours": "; пн-сб 10:00-20:00; вс 10:00-20:00"
            },
            "PICK_POINT_7701-073": {
                "id": 2765,
                "uid": "PICK_POINT_7701-073",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Энтузиастов ш., д. 31, стр. 39",
                "address": "Энтузиастов ш., д. 31, стр. 39",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.758813",
                "lon": "37.751938",
                "workhours": "; пн-вс 10:00-20:00"
            },
            "PICK_POINT_7701-071": {
                "id": 2759,
                "uid": "PICK_POINT_7701-071",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Южнобутовская ул.,  д. 58",
                "address": "Южнобутовская ул.,  д. 58",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.536834",
                "lon": "37.52924",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7705-051": {
                "id": 36624,
                "uid": "PICK_POINT_7705-051",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Южнобутовская ул., д. 97",
                "address": "Южнобутовская ул., д. 97",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.534444",
                "lon": "37.520626",
                "workhours": "; пн-вс 09:00-23:00"
            },
            "PICK_POINT_7701-230": {
                "id": 42676,
                "uid": "PICK_POINT_7701-230",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Юных ленинцев ул., д. 10/15  к. 2",
                "address": "Юных ленинцев ул., д. 10/15  к. 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.699011",
                "lon": "37.740459",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7705-036": {
                "id": 36396,
                "uid": "PICK_POINT_7705-036",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Юных ленинцев ул., д. 99",
                "address": "Юных ленинцев ул., д. 99",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.698548",
                "lon": "37.779663",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7705-059": {
                "id": 37173,
                "uid": "PICK_POINT_7705-059",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Юрьевский пер., д. 16A",
                "address": "Юрьевский пер., д. 16A",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.765144",
                "lon": "37.717411",
                "workhours": "; пн-вс 09:00-21:00"
            },
            "PICK_POINT_7701-154": {
                "id": 13254,
                "uid": "PICK_POINT_7701-154",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Яблочкова ул., д. 19-21",
                "address": "Яблочкова ул., д. 19-21",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.818903",
                "lon": "37.577202",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7701-065": {
                "id": 1381,
                "uid": "PICK_POINT_7701-065",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Яблочкова ул., д. 21 А",
                "address": "Яблочкова ул., д. 21 А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.819486",
                "lon": "37.57825",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7702-369": {
                "id": 35747,
                "uid": "PICK_POINT_7702-369",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Яна Райниса б-р, д. 19, к. 2, офис 2",
                "address": "Яна Райниса б-р, д. 19, к. 2, офис 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.848028",
                "lon": "37.427776",
                "workhours": "; пн-сб 10:00-15:00; вс выходной"
            },
            "PICK_POINT_7701-202": {
                "id": 24674,
                "uid": "PICK_POINT_7701-202",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Ярославское ш. (Москва), д. 69",
                "address": "Ярославское ш. (Москва), д. 69",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.867253",
                "lon": "37.704206",
                "workhours": "; пн-вс 10:00-21:00"
            },
            "PICK_POINT_7705-065": {
                "id": 37575,
                "uid": "PICK_POINT_7705-065",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Ярославское шоссе, д. 136",
                "address": "Ярославское шоссе, д. 136",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.874173",
                "lon": "37.716601",
                "workhours": "; пн-вс 08:00-23:00"
            },
            "PICK_POINT_7702-027": {
                "id": 2553,
                "uid": "PICK_POINT_7702-027",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Ярцевская ул., д. 25 А",
                "address": "Ярцевская ул., д. 25 А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.741047",
                "lon": "37.415331",
                "workhours": "; пн-вс 09:00-21:45"
            },
            "PICK_POINT_7702-188": {
                "id": 9464,
                "uid": "PICK_POINT_7702-188",
                "isNumericCode": false,
                "agent": 2248182,
                "price": 200,
                "title": "Ясногорская ул., д.21, к.1",
                "address": "Ясногорская ул., д.21, к.1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-06 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 6 сентября",
                "lat": "55.599118",
                "lon": "37.531208",
                "workhours": "; пн-сб 08:00-19:00; вс 08:00-19:00"
            }
        },
        "paymentTypes": [20, 2, 9, 25, 39],
        "price": 200,
        "pickupDatetime": {
            "date": "2018-09-06 18:57:00.000000",
            "timezone_type": 3,
            "timezone": "Europe/Moscow"
        },
        "pickupDatetimeTimestamp": 1536249420,
        "pickupTitle": "Дата поступления 6 сентября"
    },
    "boxberry": {
        "title": "Пункт выдачи Boxberry",
        "uid": "boxberry",
        "code": "PICKUP_BOXBERRY",
        "points": {
            "PICKUP_BOXBERRY_77441": {
                "id": 12135,
                "uid": "PICKUP_BOXBERRY_77441",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "101000, Москва г, Андропова пр-кт, д.36",
                "address": "101000, Москва г, Андропова пр-кт, д.36",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.6758437",
                "lon": "37.6618415",
                "workhours": "пн-сб:10.00-22.00, вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_77791": {
                "id": 12480,
                "uid": "PICKUP_BOXBERRY_77791",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "101000, Москва г, Звенигородская 2-я ул, д.12, строение 3, оф. 18",
                "address": "101000, Москва г, Звенигородская 2-я ул, д.12, строение 3, оф. 18",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.7612187",
                "lon": "37.5577534",
                "workhours": "пн-пт:10.00-19.00"
            },
            "PICKUP_BOXBERRY_77651": {
                "id": 12418,
                "uid": "PICKUP_BOXBERRY_77651",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "101000, Москва г, Кировоградская ул, д.22г",
                "address": "101000, Москва г, Кировоградская ул, д.22г",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.6147392",
                "lon": "37.6043585",
                "workhours": "пн-вс:10.00-21.00"
            },
            "PICKUP_BOXBERRY_19981": {
                "id": 36464,
                "uid": "PICKUP_BOXBERRY_19981",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "101000, Москва г, Коммунарка п., д.20",
                "address": "101000, Москва г, Коммунарка п., д.20",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.571632",
                "lon": "37.473492",
                "workhours": "пн-пт:10.00-20.00, сб:10.00-18.00"
            },
            "PICKUP_BOXBERRY_77581": {
                "id": 12402,
                "uid": "PICKUP_BOXBERRY_77581",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "101000, Москва г, Ленинградский пр-кт, д.74, корпус 5",
                "address": "101000, Москва г, Ленинградский пр-кт, д.74, корпус 5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.8064132",
                "lon": "37.5182275",
                "workhours": "пн-пт:11.00-19.30, сб:11.00-18.00"
            },
            "PICKUP_BOXBERRY_10.015": {
                "id": 12209,
                "uid": "PICKUP_BOXBERRY_10.015",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "101000, Москва г, Лубянский проезд, д.15, строение 4, оф. 12",
                "address": "101000, Москва г, Лубянский проезд, д.15, строение 4, оф. 12",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.756898",
                "lon": "37.63341",
                "workhours": "пн-пт:10.00-21.00, сб:10.00-18.00"
            },
            "PICKUP_BOXBERRY_77661": {
                "id": 12427,
                "uid": "PICKUP_BOXBERRY_77661",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "101000, Москва г, Люблинская ул, д.27/2",
                "address": "101000, Москва г, Люблинская ул, д.27/2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.7006942",
                "lon": "37.7334194",
                "workhours": "пн-пт:12.00-19.00, сб:12.00-17.00"
            },
            "PICKUP_BOXBERRY_79909": {
                "id": 37310,
                "uid": "PICKUP_BOXBERRY_79909",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "101000, Москва г, Мясницкая ул, д.17, строение 2",
                "address": "101000, Москва г, Мясницкая ул, д.17, строение 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.763584",
                "lon": "37.634506",
                "workhours": "пн-пт:11.00-19.00, обед:15.00-15.40"
            },
            "PICKUP_BOXBERRY_97241": {
                "id": 12610,
                "uid": "PICKUP_BOXBERRY_97241",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "101000, Москва г, Мясницкая ул, д.24/7, строение 1",
                "address": "101000, Москва г, Мясницкая ул, д.24/7, строение 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.7624447",
                "lon": "37.6355924",
                "workhours": "пн-вс:08.00-19.00"
            },
            "PICKUP_BOXBERRY_77400": {
                "id": 12310,
                "uid": "PICKUP_BOXBERRY_77400",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "101000, Москва г, Новокузнецкая ул, д.42, строение 5",
                "address": "101000, Москва г, Новокузнецкая ул, д.42, строение 5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.7314802",
                "lon": "37.6346675",
                "workhours": "пн-вс:09.00-21.00"
            },
            "PICKUP_BOXBERRY_77491": {
                "id": 12150,
                "uid": "PICKUP_BOXBERRY_77491",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "101000, Москва г, Профсоюзная ул, д.109",
                "address": "101000, Москва г, Профсоюзная ул, д.109",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.633012",
                "lon": "37.5215605",
                "workhours": "пн-пт:12.00-20.30, сб-вс:12.00-19.00"
            },
            "PICKUP_BOXBERRY_77541": {
                "id": 12378,
                "uid": "PICKUP_BOXBERRY_77541",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "101000, Москва г, Профсоюзная ул, д.96, оф. 4",
                "address": "101000, Москва г, Профсоюзная ул, д.96, оф. 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.6474677",
                "lon": "37.5278215",
                "workhours": "пн-вс:10.00-22.00"
            },
            "PICKUP_BOXBERRY_77551": {
                "id": 12379,
                "uid": "PICKUP_BOXBERRY_77551",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "101000, Москва г, Сокольническая пл, д.4а, оф. 306",
                "address": "101000, Москва г, Сокольническая пл, д.4а, оф. 306",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.7899522",
                "lon": "37.6785410",
                "workhours": "пн-вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_10.039": {
                "id": 12307,
                "uid": "PICKUP_BOXBERRY_10.039",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "101000, Москва г, Суздальская ул, д.26А",
                "address": "101000, Москва г, Суздальская ул, д.26А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.7430457",
                "lon": "37.8597945",
                "workhours": "пн-вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_10.002": {
                "id": 12285,
                "uid": "PICKUP_BOXBERRY_10.002",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "101000, Москва г, Тверская-Ямская 4-Я ул, д.2/11, строение 2",
                "address": "101000, Москва г, Тверская-Ямская 4-Я ул, д.2/11, строение 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.771884",
                "lon": "37.598411",
                "workhours": "пн-пт:10.00-21.00, сб:10.00-18.00"
            },
            "PICKUP_BOXBERRY_97011": {
                "id": 12562,
                "uid": "PICKUP_BOXBERRY_97011",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "105005, Москва г, Бауманская ул, д.32, строение 2",
                "address": "105005, Москва г, Бауманская ул, д.32, строение 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.7721627",
                "lon": "37.6783254",
                "workhours": "пн-вс:09.30-19.30"
            },
            "PICKUP_BOXBERRY_97661": {
                "id": 24135,
                "uid": "PICKUP_BOXBERRY_97661",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "105037, Москва г, Измайловский пр-кт, д.59",
                "address": "105037, Москва г, Измайловский пр-кт, д.59",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.788778",
                "lon": "37.782808",
                "workhours": "пн-пт:11.00-20.00"
            },
            "PICKUP_BOXBERRY_19956": {
                "id": 34399,
                "uid": "PICKUP_BOXBERRY_19956",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "105064, Москва г, Земляной Вал ул, д.29",
                "address": "105064, Москва г, Земляной Вал ул, д.29",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.757491",
                "lon": "37.660808",
                "workhours": "пн-вс:09.00-21.00"
            },
            "PICKUP_BOXBERRY_79966": {
                "id": 40483,
                "uid": "PICKUP_BOXBERRY_79966",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "105064, Москва г, Земляной Вал ул, д.7, строение 1",
                "address": "105064, Москва г, Земляной Вал ул, д.7, строение 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.762687",
                "lon": "37.657890",
                "workhours": "пн-вс:09.00-21.00"
            },
            "PICKUP_BOXBERRY_19920": {
                "id": 33594,
                "uid": "PICKUP_BOXBERRY_19920",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "105082, Москва г, Фридриха Энгельса ул, д.58, строение 2, оф. 582",
                "address": "105082, Москва г, Фридриха Энгельса ул, д.58, строение 2, оф. 582",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.776958",
                "lon": "37.692663",
                "workhours": "пн-пт:10.00-19.00, сб:10.00-16.00"
            },
            "PICKUP_BOXBERRY_19942": {
                "id": 33849,
                "uid": "PICKUP_BOXBERRY_19942",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "105118, Москва г, Будённого пр-кт, д.18Б, пав. А23",
                "address": "105118, Москва г, Будённого пр-кт, д.18Б, пав. А23",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.767387",
                "lon": "37.728999",
                "workhours": "пн-чт:09.00-20.00, пт:09.00-19.00, сб-вс:11.00-18.00"
            },
            "PICKUP_BOXBERRY_99801": {
                "id": 24277,
                "uid": "PICKUP_BOXBERRY_99801",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "105215, Москва г, Парковая 9-я ул, д.59А, корпус 3, строение 5",
                "address": "105215, Москва г, Парковая 9-я ул, д.59А, корпус 3, строение 5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.808507",
                "lon": "37.797497",
                "workhours": "пн-пт:11.00-19.30, сб:11.00-18.00"
            },
            "PICKUP_BOXBERRY_10.004": {
                "id": 12280,
                "uid": "PICKUP_BOXBERRY_10.004",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "105264, Москва г, Измайловский б-р, д.43",
                "address": "105264, Москва г, Измайловский б-р, д.43",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.7972502",
                "lon": "37.7981065",
                "workhours": "пн-пт:10.00-20.00, сб-вс:11.00-18.00"
            },
            "PICKUP_BOXBERRY_99481": {
                "id": 20457,
                "uid": "PICKUP_BOXBERRY_99481",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "105318, Москва г, Щербаковская ул, д.7",
                "address": "105318, Москва г, Щербаковская ул, д.7",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.782825",
                "lon": "37.726089",
                "workhours": "пн-пт:10.00-22.00, сб-вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_10.006": {
                "id": 12283,
                "uid": "PICKUP_BOXBERRY_10.006",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "107023, Москва г, Семёновская Б. ул, д.28",
                "address": "107023, Москва г, Семёновская Б. ул, д.28",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.781772",
                "lon": "37.707089",
                "workhours": "пн-пт:10.00-21.00, сб:10.00-18.00"
            },
            "PICKUP_BOXBERRY_77831": {
                "id": 12499,
                "uid": "PICKUP_BOXBERRY_77831",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "107045, Москва г, Сухаревская Б. пл, д.1/2, строение 1",
                "address": "107045, Москва г, Сухаревская Б. пл, д.1/2, строение 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.7730387",
                "lon": "37.6331404",
                "workhours": "пн-пт:11.00-20.00, сб:11.00-15.00"
            },
            "PICKUP_BOXBERRY_97841": {
                "id": 13860,
                "uid": "PICKUP_BOXBERRY_97841",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "107061, Москва г, Хромова ул, д.20",
                "address": "107061, Москва г, Хромова ул, д.20",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.799699",
                "lon": "37.718372",
                "workhours": "пн-пт:08.30-20.30, сб:10.00-18.00"
            },
            "PICKUP_BOXBERRY_19911": {
                "id": 33159,
                "uid": "PICKUP_BOXBERRY_19911",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "107078, Москва г, Каланчевская ул, д.11, строение 3",
                "address": "107078, Москва г, Каланчевская ул, д.11, строение 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.770826",
                "lon": "37.650235",
                "workhours": "пн-пт:08.30-19.00"
            },
            "PICKUP_BOXBERRY_97601": {
                "id": 13508,
                "uid": "PICKUP_BOXBERRY_97601",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "107140, Москва г, Комсомольская пл, д.6, павильон 41",
                "address": "107140, Москва г, Комсомольская пл, д.6, павильон 41",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.7758642",
                "lon": "37.6604130",
                "workhours": "пн-пт:09.00-21.00, сб-вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_19979": {
                "id": 36465,
                "uid": "PICKUP_BOXBERRY_19979",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "107143, Москва г, Открытое ш, д.24, корпус 11",
                "address": "107143, Москва г, Открытое ш, д.24, корпус 11",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.822837",
                "lon": "37.757260",
                "workhours": "пн-вс:10.00-21.00"
            },
            "PICKUP_BOXBERRY_77631": {
                "id": 12414,
                "uid": "PICKUP_BOXBERRY_77631",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "107150, Москва г, Ивантеевская ул, д.25А",
                "address": "107150, Москва г, Ивантеевская ул, д.25А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.8168442",
                "lon": "37.7353954",
                "workhours": "пн-пт:10.00-22.00, сб:10.00-21.00, вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_19957": {
                "id": 34402,
                "uid": "PICKUP_BOXBERRY_19957",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "107241, Москва г, Уральская ул, д.11",
                "address": "107241, Москва г, Уральская ул, д.11",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.817588",
                "lon": "37.797442",
                "workhours": "пн-сб:09.00-20.30, вс:09.00-19.30"
            },
            "PICKUP_BOXBERRY_19722": {
                "id": 24583,
                "uid": "PICKUP_BOXBERRY_19722",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "107497, Москва г, Щёлковское ш, д.89/2",
                "address": "107497, Москва г, Щёлковское ш, д.89/2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.812519",
                "lon": "37.821481",
                "workhours": "пн-пт:08.00-20.00, сб-вс:08.00-19.00"
            },
            "PICKUP_BOXBERRY_79953": {
                "id": 40268,
                "uid": "PICKUP_BOXBERRY_79953",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "107564, Москва г, Краснобогатырская ул, д.9",
                "address": "107564, Москва г, Краснобогатырская ул, д.9",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.817299",
                "lon": "37.692959",
                "workhours": "пн-вс:10.00-21.00"
            },
            "PICKUP_BOXBERRY_19908": {
                "id": 33121,
                "uid": "PICKUP_BOXBERRY_19908",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "107589, Москва г, Хабаровская ул, д.8",
                "address": "107589, Москва г, Хабаровская ул, д.8",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.818882",
                "lon": "37.827706",
                "workhours": "пн-сб:09.00-21.00, вс:09.00-20.00"
            },
            "PICKUP_BOXBERRY_19788": {
                "id": 32888,
                "uid": "PICKUP_BOXBERRY_19788",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "108811, Москва г, Корнея Чуковского ул, д.5",
                "address": "108811, Москва г, Корнея Чуковского ул, д.5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.637271",
                "lon": "37.325961",
                "workhours": "пн-пт:10.00-19.30, сб:10.00-16.30"
            },
            "PICKUP_BOXBERRY_79923": {
                "id": 38129,
                "uid": "PICKUP_BOXBERRY_79923",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "108820, Москва г, Тёплый Стан ул, д.11, корпус 1",
                "address": "108820, Москва г, Тёплый Стан ул, д.11, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.623127",
                "lon": "37.493030",
                "workhours": "пн-пт:09.00-19.00, сб:09.00-15.00"
            },
            "PICKUP_BOXBERRY_79946": {
                "id": 40085,
                "uid": "PICKUP_BOXBERRY_79946",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "109004, Москва г, Тетеринский пер, д.12",
                "address": "109004, Москва г, Тетеринский пер, д.12",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.745291",
                "lon": "37.652625",
                "workhours": "пн-вс:09.00-21.00"
            },
            "PICKUP_BOXBERRY_79902": {
                "id": 37296,
                "uid": "PICKUP_BOXBERRY_79902",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "109044, Москва г, Крутицкий 3-й пер, д.13",
                "address": "109044, Москва г, Крутицкий 3-й пер, д.13",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.730547",
                "lon": "37.663512",
                "workhours": "пн-пт:10.00-20.00, сб:10.00-19.00, вс:10.00-18.00"
            },
            "PICKUP_BOXBERRY_99871": {
                "id": 25106,
                "uid": "PICKUP_BOXBERRY_99871",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "109125, Москва г, Люблинская ул, д.7/2, корпус 1",
                "address": "109125, Москва г, Люблинская ул, д.7/2, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.713106",
                "lon": "37.731164",
                "workhours": "пн-пт:10.00-20.00, сб-вс:11.00-18.00"
            },
            "PICKUP_BOXBERRY_97341": {
                "id": 12651,
                "uid": "PICKUP_BOXBERRY_97341",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "109145, Москва г, Жулебинский б-р, д.5",
                "address": "109145, Москва г, Жулебинский б-р, д.5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.6985532",
                "lon": "37.8448999",
                "workhours": "пн-пт:12.00-20.30, сб:12.00-19.00"
            },
            "PICKUP_BOXBERRY_79959": {
                "id": 40293,
                "uid": "PICKUP_BOXBERRY_79959",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "109147, Москва г, Калитниковская М. ул, д.7",
                "address": "109147, Москва г, Калитниковская М. ул, д.7",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.737365",
                "lon": "37.678694",
                "workhours": "пн-пт:10.00-19.00, сб:10.00-17.00"
            },
            "PICKUP_BOXBERRY_10.027": {
                "id": 12291,
                "uid": "PICKUP_BOXBERRY_10.027",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "109147, Москва г, Марксистская ул, д.3",
                "address": "109147, Москва г, Марксистская ул, д.3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.738145",
                "lon": "37.661976",
                "workhours": "пн-вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_99141": {
                "id": 18897,
                "uid": "PICKUP_BOXBERRY_99141",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "109147, Москва г, Марксистская ул, д.5",
                "address": "109147, Москва г, Марксистская ул, д.5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.736878",
                "lon": "37.664689",
                "workhours": "пн-пт:09.00-21.00, сб-вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_97451": {
                "id": 25002,
                "uid": "PICKUP_BOXBERRY_97451",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "109147, Москва г, Таганская ул, д.31/22",
                "address": "109147, Москва г, Таганская ул, д.31/22",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.7396757",
                "lon": "37.6696384",
                "workhours": "пн-пт:08.00-20.00, сб-вс:08.00-19.00"
            },
            "PICKUP_BOXBERRY_77701": {
                "id": 12476,
                "uid": "PICKUP_BOXBERRY_77701",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "109156, Москва г, Генерала Кузнецова ул, д.15, корпус 1",
                "address": "109156, Москва г, Генерала Кузнецова ул, д.15, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.6867667",
                "lon": "37.8589044",
                "workhours": "пн-пт:10.00-20.00, сб-вс:10.00-18.00"
            },
            "PICKUP_BOXBERRY_79916": {
                "id": 38363,
                "uid": "PICKUP_BOXBERRY_79916",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "109263, Москва г, Волжский б-р, д.46, корпус 2",
                "address": "109263, Москва г, Волжский б-р, д.46, корпус 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.698914",
                "lon": "37.749670",
                "workhours": "пн-пт:10.00-19.00, сб:11.00-17.00"
            },
            "PICKUP_BOXBERRY_97951": {
                "id": 13873,
                "uid": "PICKUP_BOXBERRY_97951",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "109341, Москва г, Перерва ул, д.45",
                "address": "109341, Москва г, Перерва ул, д.45",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.659998",
                "lon": "37.748457",
                "workhours": "пн-пт:10.00-22.00, сб:10.00-21.00, вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_10.016": {
                "id": 12210,
                "uid": "PICKUP_BOXBERRY_10.016",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "109369, Москва г, Новочеркасский б-р, д.51",
                "address": "109369, Москва г, Новочеркасский б-р, д.51",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.648316",
                "lon": "37.739501",
                "workhours": "пн-пт:10.00-20.00, сб:10.00-18.00"
            },
            "PICKUP_BOXBERRY_97491": {
                "id": 13000,
                "uid": "PICKUP_BOXBERRY_97491",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "109377, Москва г, Новокузьминская 1-я ул, д.25",
                "address": "109377, Москва г, Новокузьминская 1-я ул, д.25",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.7136587",
                "lon": "37.7943604",
                "workhours": "пн-вс:10.00-21.00"
            },
            "PICKUP_BOXBERRY_19925": {
                "id": 33593,
                "uid": "PICKUP_BOXBERRY_19925",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "109388, Москва г, Гурьянова ул, д.30, оф. 009",
                "address": "109388, Москва г, Гурьянова ул, д.30, оф. 009",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.680707",
                "lon": "37.715821",
                "workhours": "пн-пт:10.00-20.00, сб-вс:10.00-18.00"
            },
            "PICKUP_BOXBERRY_19796": {
                "id": 33065,
                "uid": "PICKUP_BOXBERRY_19796",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "109431, Москва г, Жулебинский б-р, д.31",
                "address": "109431, Москва г, Жулебинский б-р, д.31",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.686376",
                "lon": "37.848044",
                "workhours": "пн-сб:08.00-23.50"
            },
            "PICKUP_BOXBERRY_77851": {
                "id": 12487,
                "uid": "PICKUP_BOXBERRY_77851",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "109443, Москва г, Зеленодольская ул, д.41, корпус 1",
                "address": "109443, Москва г, Зеленодольская ул, д.41, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.7029317",
                "lon": "37.7665934",
                "workhours": "пн-сб:10.00-20.30, вс:10.00-19.30"
            },
            "PICKUP_BOXBERRY_19726": {
                "id": 25003,
                "uid": "PICKUP_BOXBERRY_19726",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "109444, Москва г, Ташкентская ул, д.9",
                "address": "109444, Москва г, Ташкентская ул, д.9",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.708227",
                "lon": "37.819244",
                "workhours": "пн-пт:11.00-20.00, сб:12.00-19.00"
            },
            "PICKUP_BOXBERRY_10.003": {
                "id": 12282,
                "uid": "PICKUP_BOXBERRY_10.003",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "109451, Москва г, Братиславская ул, д.14",
                "address": "109451, Москва г, Братиславская ул, д.14",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.659119",
                "lon": "37.755086",
                "workhours": "пн-пт:10.00-21.00, сб:10.00-18.00"
            },
            "PICKUP_BOXBERRY_10.020": {
                "id": 12213,
                "uid": "PICKUP_BOXBERRY_10.020",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "109542, Москва г, Рязанский пр-кт, д.99А",
                "address": "109542, Москва г, Рязанский пр-кт, д.99А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.71408",
                "lon": "37.817708",
                "workhours": "пн-вс:10.00-21.00"
            },
            "PICKUP_BOXBERRY_19759": {
                "id": 25741,
                "uid": "PICKUP_BOXBERRY_19759",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "111024, Москва г, Кабельный 2-й проезд, д.1",
                "address": "111024, Москва г, Кабельный 2-й проезд, д.1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.747723",
                "lon": "37.716297",
                "workhours": "пн-пт:11.00-20.00, сб:11.00-17.45"
            },
            "PICKUP_BOXBERRY_99961": {
                "id": 24141,
                "uid": "PICKUP_BOXBERRY_99961",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "111394, Москва г, Перовская ул, д.66к5",
                "address": "111394, Москва г, Перовская ул, д.66к5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.744206",
                "lon": "37.801529",
                "workhours": "пн-пт:10.00-19.30, сб-вс:10.00-18.30"
            },
            "PICKUP_BOXBERRY_19773": {
                "id": 29629,
                "uid": "PICKUP_BOXBERRY_19773",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "111396, Москва г, Зелёный пр-кт, д.66, корпус 2",
                "address": "111396, Москва г, Зелёный пр-кт, д.66, корпус 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.749800",
                "lon": "37.823026",
                "workhours": "пн-пт:10.00-20.00, сб-вс:10.00-19.00"
            },
            "PICKUP_BOXBERRY_79937": {
                "id": 38624,
                "uid": "PICKUP_BOXBERRY_79937",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "111396, Москва г, Свободный пр-кт, д.20/58",
                "address": "111396, Москва г, Свободный пр-кт, д.20/58",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.751163",
                "lon": "37.816037",
                "workhours": "пн-вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_19983": {
                "id": 36552,
                "uid": "PICKUP_BOXBERRY_19983",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "111397, Москва г, Зелёный пр-кт, д.24, оф. 33",
                "address": "111397, Москва г, Зелёный пр-кт, д.24, оф. 33",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.750454",
                "lon": "37.789896",
                "workhours": "пн-пт:10.00-20.30, сб-вс:11.00-18.00"
            },
            "PICKUP_BOXBERRY_19980": {
                "id": 36268,
                "uid": "PICKUP_BOXBERRY_19980",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "111673, Москва г, Новокосинская ул, д.10, корпус 1",
                "address": "111673, Москва г, Новокосинская ул, д.10, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.734679",
                "lon": "37.855590",
                "workhours": "пн-пт:10.00-20.00, сб:10.00-18.00"
            },
            "PICKUP_BOXBERRY_19771": {
                "id": 29760,
                "uid": "PICKUP_BOXBERRY_19771",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "111674, Москва г, Рождественская ул, д.23 /33",
                "address": "111674, Москва г, Рождественская ул, д.23 /33",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.705488",
                "lon": "37.920736",
                "workhours": "пн-пт:11.00-20.00, сб:11.00-16.00"
            },
            "PICKUP_BOXBERRY_79951": {
                "id": 40286,
                "uid": "PICKUP_BOXBERRY_79951",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "111675, Москва г, Дмитриевского ул, д.17",
                "address": "111675, Москва г, Дмитриевского ул, д.17",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.709358",
                "lon": "37.893562",
                "workhours": "пн-пт:10.00-20.00, сб:10.00-18.00"
            },
            "PICKUP_BOXBERRY_19731": {
                "id": 25079,
                "uid": "PICKUP_BOXBERRY_19731",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "111675, Москва г, Руднёвка ул, д.19",
                "address": "111675, Москва г, Руднёвка ул, д.19",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.716615",
                "lon": "37.887354",
                "workhours": "пн-вс:10.00-21.00"
            },
            "PICKUP_BOXBERRY_19924": {
                "id": 33805,
                "uid": "PICKUP_BOXBERRY_19924",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "115035, Москва г, Пятницкая ул, д.18, строение 4",
                "address": "115035, Москва г, Пятницкая ул, д.18, строение 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.742037",
                "lon": "37.627337",
                "workhours": "пн-вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_79920": {
                "id": 37960,
                "uid": "PICKUP_BOXBERRY_79920",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "115088, Москва г, Шарикоподшипниковская ул, д.13, строение 2",
                "address": "115088, Москва г, Шарикоподшипниковская ул, д.13, строение 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.717974",
                "lon": "37.677454",
                "workhours": "пн-пт:11.00-20.00"
            },
            "PICKUP_BOXBERRY_79948": {
                "id": 38739,
                "uid": "PICKUP_BOXBERRY_79948",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "115088, Москва г, Шарикоподшипниковская ул, д.13, строение 65",
                "address": "115088, Москва г, Шарикоподшипниковская ул, д.13, строение 65",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.722071",
                "lon": "37.680311",
                "workhours": "пн-пт:11.00-19.00, сб:11.00-17.00, вс:11.00-15.00"
            },
            "PICKUP_BOXBERRY_19701": {
                "id": 24426,
                "uid": "PICKUP_BOXBERRY_19701",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "115114, Москва г, Кожевническая ул, д.7, строение 1",
                "address": "115114, Москва г, Кожевническая ул, д.7, строение 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.730497",
                "lon": "37.645214",
                "workhours": "пн-сб:10.00-21.00, вс:10.00-19.00"
            },
            "PICKUP_BOXBERRY_79981": {
                "id": 42493,
                "uid": "PICKUP_BOXBERRY_79981",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "115114, Москва г, Летниковская ул, д.10, строение 5",
                "address": "115114, Москва г, Летниковская ул, д.10, строение 5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.724266",
                "lon": "37.642806",
                "workhours": "пн-пт:10.00-20.00"
            },
            "PICKUP_BOXBERRY_10.012": {
                "id": 12207,
                "uid": "PICKUP_BOXBERRY_10.012",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "115191, Москва г, Холодильный пер, д.3, оф. 11А",
                "address": "115191, Москва г, Холодильный пер, д.3, оф. 11А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.7083892",
                "lon": "37.6254865",
                "workhours": "пн-пт:12.00-20.30"
            },
            "PICKUP_BOXBERRY_79926": {
                "id": 38362,
                "uid": "PICKUP_BOXBERRY_79926",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "115201, Москва г, Каширское ш, д.16",
                "address": "115201, Москва г, Каширское ш, д.16",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.662187",
                "lon": "37.630499",
                "workhours": "пн-пт:12.00-20.00, сб-вс:10.00-16.00"
            },
            "PICKUP_BOXBERRY_19757": {
                "id": 25527,
                "uid": "PICKUP_BOXBERRY_19757",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "115211, Москва г, Борисовские Пруды ул, д.10, корпус 4",
                "address": "115211, Москва г, Борисовские Пруды ул, д.10, корпус 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.633760",
                "lon": "37.739600",
                "workhours": "пн-пт:10.00-20.00, сб-вс:10.00-18.00"
            },
            "PICKUP_BOXBERRY_19978": {
                "id": 36550,
                "uid": "PICKUP_BOXBERRY_19978",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "115230, Москва г, Электролитный проезд, д.16, корпус 1",
                "address": "115230, Москва г, Электролитный проезд, д.16, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.672219",
                "lon": "37.611518",
                "workhours": "пн-пт:10.00-20.00, сб:10.00-17.00"
            },
            "PICKUP_BOXBERRY_79908": {
                "id": 37966,
                "uid": "PICKUP_BOXBERRY_79908",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "115280, Москва г, Автозаводская ул, д.23, строение 931, корпус 2",
                "address": "115280, Москва г, Автозаводская ул, д.23, строение 931, корпус 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.689811",
                "lon": "37.657224",
                "workhours": "пн-пт:09.00-21.00, сб-вс:10.00-22.00"
            },
            "PICKUP_BOXBERRY_99401": {
                "id": 19954,
                "uid": "PICKUP_BOXBERRY_99401",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "115280, Москва г, Мастеркова ул, д.6",
                "address": "115280, Москва г, Мастеркова ул, д.6",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.707902",
                "lon": "37.656784",
                "workhours": "пн-сб:10.00-21.00, вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_79947": {
                "id": 40116,
                "uid": "PICKUP_BOXBERRY_79947",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "115304, Москва г, Луганская ул, д.5",
                "address": "115304, Москва г, Луганская ул, д.5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.626086",
                "lon": "37.666270",
                "workhours": "пн-вс:09.00-21.00"
            },
            "PICKUP_BOXBERRY_79928": {
                "id": 38424,
                "uid": "PICKUP_BOXBERRY_79928",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "115404, Москва г, Бирюлёвская ул, д.9",
                "address": "115404, Москва г, Бирюлёвская ул, д.9",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.598512",
                "lon": "37.663854",
                "workhours": "пн-пт:10.00-20.00, сб:10.00-18.00, вс:10.00-15.00"
            },
            "PICKUP_BOXBERRY_79914": {
                "id": 37617,
                "uid": "PICKUP_BOXBERRY_79914",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "115419, Москва г, Шаболовка ул, д.34, строение 6, оф. 8",
                "address": "115419, Москва г, Шаболовка ул, д.34, строение 6, оф. 8",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.718811",
                "lon": "37.606487",
                "workhours": "пн-пт:12.00-20.30, сб:12.00-19.00"
            },
            "PICKUP_BOXBERRY_19753": {
                "id": 25649,
                "uid": "PICKUP_BOXBERRY_19753",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "115432, Москва г, Трофимова ул, д.35/20",
                "address": "115432, Москва г, Трофимова ул, д.35/20",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.704671",
                "lon": "37.684883",
                "workhours": "пн-пт:09.00-20.00"
            },
            "PICKUP_BOXBERRY_19721": {
                "id": 24582,
                "uid": "PICKUP_BOXBERRY_19721",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "115470, Москва г, Андропова пр-кт, д.15",
                "address": "115470, Москва г, Андропова пр-кт, д.15",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.683062",
                "lon": "37.666207",
                "workhours": "пн-пт:08.00-20.00, сб-вс:08.00-19.00"
            },
            "PICKUP_BOXBERRY_19947": {
                "id": 34287,
                "uid": "PICKUP_BOXBERRY_19947",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "115477, Москва г, Пролетарский пр-кт, д.31",
                "address": "115477, Москва г, Пролетарский пр-кт, д.31",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.634685",
                "lon": "37.658634",
                "workhours": "пн-пт:11.00-20.00"
            },
            "PICKUP_BOXBERRY_19762": {
                "id": 25688,
                "uid": "PICKUP_BOXBERRY_19762",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "115516, Москва г, Луганская ул, д.10",
                "address": "115516, Москва г, Луганская ул, д.10",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.622436",
                "lon": "37.667582",
                "workhours": "пн-пт:10.00-22.00, сб:10.00-21.00, вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_19728": {
                "id": 25074,
                "uid": "PICKUP_BOXBERRY_19728",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "115547, Москва г, Бирюлёвская ул, д.56, строение 2, оф. 203",
                "address": "115547, Москва г, Бирюлёвская ул, д.56, строение 2, оф. 203",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.582335",
                "lon": "37.669342",
                "workhours": "пн-пт:10.00-21.00, сб:11.00-18.00"
            },
            "PICKUP_BOXBERRY_19739": {
                "id": 25680,
                "uid": "PICKUP_BOXBERRY_19739",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "115551, Москва г, Ореховый б-р, д.7, корпус 1, стр 3",
                "address": "115551, Москва г, Ореховый б-р, д.7, корпус 1, стр 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.609441",
                "lon": "37.703882",
                "workhours": "пн-вс:10.00-19.00"
            },
            "PICKUP_BOXBERRY_77571": {
                "id": 12381,
                "uid": "PICKUP_BOXBERRY_77571",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "115551, Москва г, Шипиловский проезд, д.43, корпус 2, оф. 1",
                "address": "115551, Москва г, Шипиловский проезд, д.43, корпус 2, оф. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.611373",
                "lon": "37.699112",
                "workhours": "пн-пт:10.00-20.30, сб-вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_19986": {
                "id": 36593,
                "uid": "PICKUP_BOXBERRY_19986",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "115582, Москва г, Каширское ш, д.122",
                "address": "115582, Москва г, Каширское ш, д.122",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.605902",
                "lon": "37.718130",
                "workhours": "пн-пт:10.00-19.30, сб:10.00-17.00"
            },
            "PICKUP_BOXBERRY_99841": {
                "id": 21913,
                "uid": "PICKUP_BOXBERRY_99841",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "115597, Москва г, Воронежская ул, д.36, корпус 1",
                "address": "115597, Москва г, Воронежская ул, д.36, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.610367",
                "lon": "37.740345",
                "workhours": "пн-пт:12.00-20.30, сб:12.00-19.00"
            },
            "PICKUP_BOXBERRY_99251": {
                "id": 18855,
                "uid": "PICKUP_BOXBERRY_99251",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "115612, Москва г, Борисовские Пруды ул, д.26, корпус 2",
                "address": "115612, Москва г, Борисовские Пруды ул, д.26, корпус 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.639029",
                "lon": "37.759228",
                "workhours": "пн-пт:10.00-19.00, сб-вс:13.00-22.00"
            },
            "PICKUP_BOXBERRY_77601": {
                "id": 12431,
                "uid": "PICKUP_BOXBERRY_77601",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117036, Москва г, Дмитрия Ульянова ул, д.19",
                "address": "117036, Москва г, Дмитрия Ульянова ул, д.19",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.688969",
                "lon": "37.572621",
                "workhours": "пн-пт:10.00-20.00, сб-вс:10.00-18.00"
            },
            "PICKUP_BOXBERRY_97931": {
                "id": 13875,
                "uid": "PICKUP_BOXBERRY_97931",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117036, Москва г, Профсоюзная ул, д.3, оф. 120",
                "address": "117036, Москва г, Профсоюзная ул, д.3, оф. 120",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.685726",
                "lon": "37.572396",
                "workhours": "пн-пт:09.00-19.00, сб:11.00-18.00"
            },
            "PICKUP_BOXBERRY_79919": {
                "id": 37905,
                "uid": "PICKUP_BOXBERRY_79919",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117041, Москва г, Адмирала Лазарева ул, д.35",
                "address": "117041, Москва г, Адмирала Лазарева ул, д.35",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.543531",
                "lon": "37.524740",
                "workhours": "пн-пт:10.00-20.00, сб-вс:12.00-17.00"
            },
            "PICKUP_BOXBERRY_99331": {
                "id": 19810,
                "uid": "PICKUP_BOXBERRY_99331",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117041, Москва г, Адмирала Лазарева ул, д.63, корпус 1",
                "address": "117041, Москва г, Адмирала Лазарева ул, д.63, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.53666",
                "lon": "37.507834",
                "workhours": "пн-пт:11.00-20.00"
            },
            "PICKUP_BOXBERRY_79965": {
                "id": 42370,
                "uid": "PICKUP_BOXBERRY_79965",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117042, Москва г, Венёвская ул, д.4",
                "address": "117042, Москва г, Венёвская ул, д.4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.548467",
                "lon": "37.542509",
                "workhours": "пн-вс:09.00-21.00"
            },
            "PICKUP_BOXBERRY_97421": {
                "id": 12989,
                "uid": "PICKUP_BOXBERRY_97421",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117042, Москва г, Чечёрский проезд, д.8",
                "address": "117042, Москва г, Чечёрский проезд, д.8",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.542054",
                "lon": "37.544575",
                "workhours": "пн-вс:10.00-21.00"
            },
            "PICKUP_BOXBERRY_97621": {
                "id": 13435,
                "uid": "PICKUP_BOXBERRY_97621",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117105, Москва г, Варшавское ш, д.26, строение 6",
                "address": "117105, Москва г, Варшавское ш, д.26, строение 6",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.6838077",
                "lon": "37.6214354",
                "workhours": "пн-вс:10.00-22.00"
            },
            "PICKUP_BOXBERRY_19734": {
                "id": 25107,
                "uid": "PICKUP_BOXBERRY_19734",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117105, Москва г, Варшавское ш, д.39, оф. 444",
                "address": "117105, Москва г, Варшавское ш, д.39, оф. 444",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.684473",
                "lon": "37.624121",
                "workhours": "пн-пт:10.00-20.00, сб-вс:10.00-18.00"
            },
            "PICKUP_BOXBERRY_99631": {
                "id": 21198,
                "uid": "PICKUP_BOXBERRY_99631",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117186, Москва г, Ремизова ул, д.10",
                "address": "117186, Москва г, Ремизова ул, д.10",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.676220",
                "lon": "37.600657",
                "workhours": "пн-пт:10.00-20.00, сб:10.00-17.00"
            },
            "PICKUP_BOXBERRY_19707": {
                "id": 24432,
                "uid": "PICKUP_BOXBERRY_19707",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117216, Москва г, Старокачаловская ул, д.1Б",
                "address": "117216, Москва г, Старокачаловская ул, д.1Б",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.568558",
                "lon": "37.585386",
                "workhours": "пн-сб:10.00-21.00, вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_77521": {
                "id": 12166,
                "uid": "PICKUP_BOXBERRY_77521",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117321, Москва г, Профсоюзная ул, д.146, корпус 1",
                "address": "117321, Москва г, Профсоюзная ул, д.146, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.6236252",
                "lon": "37.5070075",
                "workhours": "пн-сб:10.00-20.00, вс:10.00-19.00"
            },
            "PICKUP_BOXBERRY_19790": {
                "id": 32800,
                "uid": "PICKUP_BOXBERRY_19790",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117342, Москва г, Введенского ул, д.29",
                "address": "117342, Москва г, Введенского ул, д.29",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.633572",
                "lon": "37.534478",
                "workhours": "пн-вс:09.00-21.00"
            },
            "PICKUP_BOXBERRY_99471": {
                "id": 20384,
                "uid": "PICKUP_BOXBERRY_99471",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117393, Москва г, Профсоюзная ул, д.64/66",
                "address": "117393, Москва г, Профсоюзная ул, д.64/66",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.665949",
                "lon": "37.548797",
                "workhours": "пн-сб:10.00-21.00, вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_99451": {
                "id": 20381,
                "uid": "PICKUP_BOXBERRY_99451",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117403, Москва г, Булатниковская ул, д.6А",
                "address": "117403, Москва г, Булатниковская ул, д.6А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.580981",
                "lon": "37.644998",
                "workhours": "пн-пт:10.00-20.00, сб:10.00-18.00, вс:10.00-15.00, обед:14.00-15.00"
            },
            "PICKUP_BOXBERRY_79935": {
                "id": 40114,
                "uid": "PICKUP_BOXBERRY_79935",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117405, Москва г, Варшавское ш, д.160, корпус 2",
                "address": "117405, Москва г, Варшавское ш, д.160, корпус 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.583800",
                "lon": "37.591467",
                "workhours": "пн-пт:09.00-21.00, сб:12.00-18.00"
            },
            "PICKUP_BOXBERRY_97871": {
                "id": 13796,
                "uid": "PICKUP_BOXBERRY_97871",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117437, Москва г, Миклухо-Маклая ул, д.18, корпус 2",
                "address": "117437, Москва г, Миклухо-Маклая ул, д.18, корпус 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.6449017",
                "lon": "37.5194225",
                "workhours": "пн-пт:10.00-22.00, сб:10.00-21.00, вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_97361": {
                "id": 12655,
                "uid": "PICKUP_BOXBERRY_97361",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117452, Москва г, Чонгарский б-р, д.21",
                "address": "117452, Москва г, Чонгарский б-р, д.21",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.6526027",
                "lon": "37.6044660",
                "workhours": "пн-пт:10.00-20.00, сб-вс:10.00-19.30"
            },
            "PICKUP_BOXBERRY_19991": {
                "id": 37053,
                "uid": "PICKUP_BOXBERRY_19991",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117461, Москва г, Каховка ул, д.20, строение 2",
                "address": "117461, Москва г, Каховка ул, д.20, строение 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.658276",
                "lon": "37.568075",
                "workhours": "пн-пт:10.00-20.00, сб:10.00-16.00"
            },
            "PICKUP_BOXBERRY_97471": {
                "id": 12992,
                "uid": "PICKUP_BOXBERRY_97471",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117513, Москва г, Академика Бакулева ул, д.10",
                "address": "117513, Москва г, Академика Бакулева ул, д.10",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.642164",
                "lon": "37.484137",
                "workhours": "пн-пт:12.00-20.30, сб:12.00-19.00"
            },
            "PICKUP_BOXBERRY_97381": {
                "id": 12664,
                "uid": "PICKUP_BOXBERRY_97381",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117535, Москва г, Россошанская ул, д.3к1Ас2",
                "address": "117535, Москва г, Россошанская ул, д.3к1Ас2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.595224",
                "lon": "37.607044",
                "workhours": "пн-сб:10.00-21.00, вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_19954": {
                "id": 34382,
                "uid": "PICKUP_BOXBERRY_19954",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117556, Москва г, Варшавское ш, д.76, корпус 2",
                "address": "117556, Москва г, Варшавское ш, д.76, корпус 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.654493",
                "lon": "37.617878",
                "workhours": "пн-пт:10.00-21.00, сб:10.00-19.00"
            },
            "PICKUP_BOXBERRY_10.029": {
                "id": 12297,
                "uid": "PICKUP_BOXBERRY_10.029",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117556, Москва г, Симферопольский проезд, д.7, оф. 8",
                "address": "117556, Москва г, Симферопольский проезд, д.7, оф. 8",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.6650097",
                "lon": "37.6154795",
                "workhours": "пн-пт:11.00-20.00"
            },
            "PICKUP_BOXBERRY_19941": {
                "id": 33848,
                "uid": "PICKUP_BOXBERRY_19941",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117574, Москва г, Новоясеневский пр-кт, д.7",
                "address": "117574, Москва г, Новоясеневский пр-кт, д.7",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.607489",
                "lon": "37.532367",
                "workhours": "пн-вс:10.00-21.00"
            },
            "PICKUP_BOXBERRY_77721": {
                "id": 12433,
                "uid": "PICKUP_BOXBERRY_77721",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117587, Москва г, Кировоградская ул, д.9, корпус 4, павильон 37",
                "address": "117587, Москва г, Кировоградская ул, д.9, корпус 4, павильон 37",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.6217192",
                "lon": "37.6117334",
                "workhours": "пн-пт:10.00-20.00, сб-вс:10.00-18.00"
            },
            "PICKUP_BOXBERRY_19717": {
                "id": 24445,
                "uid": "PICKUP_BOXBERRY_19717",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117587, Москва г, Кировоградская ул, д.9к1",
                "address": "117587, Москва г, Кировоградская ул, д.9к1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.625567",
                "lon": "37.612290",
                "workhours": "пн-пт:10.00-22.00, сб:10.00-21.00, вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_79977": {
                "id": 42858,
                "uid": "PICKUP_BOXBERRY_79977",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117624, Москва г, Изюмская ул, д.22",
                "address": "117624, Москва г, Изюмская ул, д.22",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.552581",
                "lon": "37.558364",
                "workhours": "пн-пт:09.00-22.00, сб-вс:10.00-21.00"
            },
            "PICKUP_BOXBERRY_79958": {
                "id": 40290,
                "uid": "PICKUP_BOXBERRY_79958",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117628, Москва г, Грина ул, д.40",
                "address": "117628, Москва г, Грина ул, д.40",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.569011",
                "lon": "37.565353",
                "workhours": "пн-пт:10.00-19.00, сб:10.00-17.00"
            },
            "PICKUP_BOXBERRY_79918": {
                "id": 37987,
                "uid": "PICKUP_BOXBERRY_79918",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117630, Москва г, Старокалужское ш, д.65",
                "address": "117630, Москва г, Старокалужское ш, д.65",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.660272",
                "lon": "37.539850",
                "workhours": "пн-сб:09.30-20.30"
            },
            "PICKUP_BOXBERRY_10.034": {
                "id": 12301,
                "uid": "PICKUP_BOXBERRY_10.034",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117639, Москва г, Балаклавский пр-кт, д.5",
                "address": "117639, Москва г, Балаклавский пр-кт, д.5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.6404212",
                "lon": "37.6102605",
                "workhours": "пн-пт:10.00-21.00, сб:10.00-18.00"
            },
            "PICKUP_BOXBERRY_99971": {
                "id": 24138,
                "uid": "PICKUP_BOXBERRY_99971",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "117639, Москва г, Балаклавский пр-кт, д.7",
                "address": "117639, Москва г, Балаклавский пр-кт, д.7",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.640746",
                "lon": "37.603271",
                "workhours": "пн-пт:10.00-21.00, сб-вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_99821": {
                "id": 21993,
                "uid": "PICKUP_BOXBERRY_99821",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "119002, Москва г, Карманицкий пер, д.9",
                "address": "119002, Москва г, Карманицкий пер, д.9",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.748275",
                "lon": "37.583571",
                "workhours": "пн-пт:09.00-19.30"
            },
            "PICKUP_BOXBERRY_79904": {
                "id": 37295,
                "uid": "PICKUP_BOXBERRY_79904",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "119019, Москва г, Гоголевский б-р, д.8, строение 2",
                "address": "119019, Москва г, Гоголевский б-р, д.8, строение 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.745904",
                "lon": "37.600864",
                "workhours": "пн-пт:10.30-19.30, сб:11.00-16.00"
            },
            "PICKUP_BOXBERRY_77951": {
                "id": 12543,
                "uid": "PICKUP_BOXBERRY_77951",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "119034, Москва г, Зубовский б-р, д.16-20",
                "address": "119034, Москва г, Зубовский б-р, д.16-20",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.7385707",
                "lon": "37.5886374",
                "workhours": "пн-пт:11.00-20.00"
            },
            "PICKUP_BOXBERRY_99561": {
                "id": 20699,
                "uid": "PICKUP_BOXBERRY_99561",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "119048, Москва г, Ефремова ул, д.23",
                "address": "119048, Москва г, Ефремова ул, д.23",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.721128",
                "lon": "37.567087",
                "workhours": "пн-вс:11.00-21.00"
            },
            "PICKUP_BOXBERRY_79941": {
                "id": 40112,
                "uid": "PICKUP_BOXBERRY_79941",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "119071, Москва г, Ленинский пр-кт, д.35",
                "address": "119071, Москва г, Ленинский пр-кт, д.35",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.711574",
                "lon": "37.587407",
                "workhours": "пн-вс:10.00-21.00"
            },
            "PICKUP_BOXBERRY_79942": {
                "id": 40598,
                "uid": "PICKUP_BOXBERRY_79942",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "119146, Москва г, Комсомольский пр-кт, д.30",
                "address": "119146, Москва г, Комсомольский пр-кт, д.30",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.725214",
                "lon": "37.578496",
                "workhours": "пн-пт:10.00-20.00, сб:10.00-18.00"
            },
            "PICKUP_BOXBERRY_19905": {
                "id": 33156,
                "uid": "PICKUP_BOXBERRY_19905",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "119146, Москва г, Фрунзенская 1-я ул, д.3а, строение 1",
                "address": "119146, Москва г, Фрунзенская 1-я ул, д.3а, строение 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.725432",
                "lon": "37.584802",
                "workhours": "пн-пт:10.00-20.00, сб:09.00-13.00"
            },
            "PICKUP_BOXBERRY_97701": {
                "id": 13476,
                "uid": "PICKUP_BOXBERRY_97701",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "119261, Москва г, Панферова ул, д.16, корпус 1",
                "address": "119261, Москва г, Панферова ул, д.16, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.681138",
                "lon": "37.544935",
                "workhours": "пн-пт:10.00-19.00, сб:10.00-17.00"
            },
            "PICKUP_BOXBERRY_19959": {
                "id": 34386,
                "uid": "PICKUP_BOXBERRY_19959",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "119311, Москва г, Вернадского пр-кт, д.9/10",
                "address": "119311, Москва г, Вернадского пр-кт, д.9/10",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.690141",
                "lon": "37.533727",
                "workhours": "пн-вс:09.00-21.00"
            },
            "PICKUP_BOXBERRY_99761": {
                "id": 21580,
                "uid": "PICKUP_BOXBERRY_99761",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "119330, Москва г, Дружбы ул, д.2/19, подъезд 7",
                "address": "119330, Москва г, Дружбы ул, д.2/19, подъезд 7",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.712198",
                "lon": "37.521255",
                "workhours": "пн-пт:10.00-20.00"
            },
            "PICKUP_BOXBERRY_97181": {
                "id": 12606,
                "uid": "PICKUP_BOXBERRY_97181",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "119334, Москва г, Ленинский пр-кт, д.45",
                "address": "119334, Москва г, Ленинский пр-кт, д.45",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.7030327",
                "lon": "37.5733124",
                "workhours": "пн-пт:08.00-20.00, сб-вс:08.00-19.00"
            },
            "PICKUP_BOXBERRY_19906": {
                "id": 33157,
                "uid": "PICKUP_BOXBERRY_19906",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "119361, Москва г, Пржевальского ул, д.2",
                "address": "119361, Москва г, Пржевальского ул, д.2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.681894",
                "lon": "37.451339",
                "workhours": "пн-сб:10.00-20.00, вс:10.00-19.00"
            },
            "PICKUP_BOXBERRY_19787": {
                "id": 32797,
                "uid": "PICKUP_BOXBERRY_19787",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "119415, Москва г, Вернадского пр-кт, д.39",
                "address": "119415, Москва г, Вернадского пр-кт, д.39",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.675544",
                "lon": "37.505894",
                "workhours": "пн-пт:12.00-20.30, сб:12.00-19.00"
            },
            "PICKUP_BOXBERRY_79962": {
                "id": 40461,
                "uid": "PICKUP_BOXBERRY_79962",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "119435, Москва г, Пироговская М. ул, д.8",
                "address": "119435, Москва г, Пироговская М. ул, д.8",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.72961",
                "lon": "37.570474",
                "workhours": "пн-вс:10.00-21.00"
            },
            "PICKUP_BOXBERRY_99591": {
                "id": 21196,
                "uid": "PICKUP_BOXBERRY_99591",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "119571, Москва г, 26-ти Бакинских Комиссаров ул, д.7, корпус 6",
                "address": "119571, Москва г, 26-ти Бакинских Комиссаров ул, д.7, корпус 6",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.655615",
                "lon": "37.484819",
                "workhours": "пн-вс:09.00-21.00"
            },
            "PICKUP_BOXBERRY_99381": {
                "id": 19958,
                "uid": "PICKUP_BOXBERRY_99381",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "119602, Москва г, Академика Анохина ул, д.2, корпус 1Б",
                "address": "119602, Москва г, Академика Анохина ул, д.2, корпус 1Б",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.670671",
                "lon": "37.476842",
                "workhours": "пн-пт:10.00-22.00, сб:10.00-21.00, вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_19917": {
                "id": 33161,
                "uid": "PICKUP_BOXBERRY_19917",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "119602, Москва г, Мичуринский проспект.Олимпийская деревня ул, д.4, корпус 2",
                "address": "119602, Москва г, Мичуринский проспект.Олимпийская деревня ул, д.4, корпус 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.675478",
                "lon": "37.469144",
                "workhours": "пн-пт:11.00-20.00"
            },
            "PICKUP_BOXBERRY_19918": {
                "id": 33596,
                "uid": "PICKUP_BOXBERRY_19918",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "119602, Москва г, Никулинская ул, д.15, корпус 1",
                "address": "119602, Москва г, Никулинская ул, д.15, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.668071",
                "lon": "37.455660",
                "workhours": "пн-пт:09.30-21.00, сб:11.00-18.00"
            },
            "PICKUP_BOXBERRY_97101": {
                "id": 12584,
                "uid": "PICKUP_BOXBERRY_97101",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "119602, Москва г, Покрышкина ул, д.8, корпус 3",
                "address": "119602, Москва г, Покрышкина ул, д.8, корпус 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.6652632",
                "lon": "37.4718654",
                "workhours": "пн-пт:10.00-20.30, сб:11.00-19.00"
            },
            "PICKUP_BOXBERRY_79933": {
                "id": 38585,
                "uid": "PICKUP_BOXBERRY_79933",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "119619, Москва г, Производственная ул, д.4",
                "address": "119619, Москва г, Производственная ул, д.4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.648407",
                "lon": "37.386750",
                "workhours": "пн-пт:10.00-20.00, сб-вс:10.00-18.00"
            },
            "PICKUP_BOXBERRY_77591": {
                "id": 12411,
                "uid": "PICKUP_BOXBERRY_77591",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "119620, Москва г, Солнцевский пр-кт, д.11, оф. 1",
                "address": "119620, Москва г, Солнцевский пр-кт, д.11, оф. 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.6495912",
                "lon": "37.4039974",
                "workhours": "пн-пт:10.00-20.00, сб-вс:10.00-18.00"
            },
            "PICKUP_BOXBERRY_19965": {
                "id": 34808,
                "uid": "PICKUP_BOXBERRY_19965",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "119633, Москва г, Боровское ш, д.27",
                "address": "119633, Москва г, Боровское ш, д.27",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.644750",
                "lon": "37.365748",
                "workhours": "пн-вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_19779": {
                "id": 32795,
                "uid": "PICKUP_BOXBERRY_19779",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "119634, Москва г, Боровское ш, д.36",
                "address": "119634, Москва г, Боровское ш, д.36",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.639756",
                "lon": "37.353791",
                "workhours": "пн-сб:10.00-20.00"
            },
            "PICKUP_BOXBERRY_97401": {
                "id": 12697,
                "uid": "PICKUP_BOXBERRY_97401",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "119634, Москва г, Лукинская ул, д.8",
                "address": "119634, Москва г, Лукинская ул, д.8",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.6504087",
                "lon": "37.3433614",
                "workhours": "пн-пт:11.00-20.00"
            },
            "PICKUP_BOXBERRY_99831": {
                "id": 21821,
                "uid": "PICKUP_BOXBERRY_99831",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "121059, Москва г, Брянская ул, д.2, оф. 208",
                "address": "121059, Москва г, Брянская ул, д.2, оф. 208",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.744389",
                "lon": "37.563853",
                "workhours": "пн-вс:10.00-21.00"
            },
            "PICKUP_BOXBERRY_99791": {
                "id": 21584,
                "uid": "PICKUP_BOXBERRY_99791",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "121087, Москва г, Барклая ул, д.8, оф. 425",
                "address": "121087, Москва г, Барклая ул, д.8, оф. 425",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.741135",
                "lon": "37.502786",
                "workhours": "пн-сб:10.00-20.00"
            },
            "PICKUP_BOXBERRY_19935": {
                "id": 33827,
                "uid": "PICKUP_BOXBERRY_19935",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "121087, Москва г, Береговой проезд, д.7",
                "address": "121087, Москва г, Береговой проезд, д.7",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.754942",
                "lon": "37.505121",
                "workhours": "пн-пт:10.00-21.00, сб-вс:10.00-17.00"
            },
            "PICKUP_BOXBERRY_19904": {
                "id": 33037,
                "uid": "PICKUP_BOXBERRY_19904",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "121108, Москва г, Герасима Курина ул, д.14, корпус 1Б",
                "address": "121108, Москва г, Герасима Курина ул, д.14, корпус 1Б",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.731637",
                "lon": "37.475639",
                "workhours": "пн-вс:10.00-21.00"
            },
            "PICKUP_BOXBERRY_19934": {
                "id": 33824,
                "uid": "PICKUP_BOXBERRY_19934",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "121108, Москва г, Ивана Франко ул, д.4, корпус 4",
                "address": "121108, Москва г, Ивана Франко ул, д.4, корпус 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.728145",
                "lon": "37.452085",
                "workhours": "пн-пт:09.00-20.00, сб:09.00-14.00"
            },
            "PICKUP_BOXBERRY_79944": {
                "id": 40084,
                "uid": "PICKUP_BOXBERRY_79944",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "121170, Москва г, Победы пл, д.2, корпус 1",
                "address": "121170, Москва г, Победы пл, д.2, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.737684",
                "lon": "37.521156",
                "workhours": "пн-вс:09.00-21.00"
            },
            "PICKUP_BOXBERRY_97481": {
                "id": 12994,
                "uid": "PICKUP_BOXBERRY_97481",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "121170, Москва г, Поклонная ул, д.11, строение 1, оф. 205",
                "address": "121170, Москва г, Поклонная ул, д.11, строение 1, оф. 205",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.7335732",
                "lon": "37.5234374",
                "workhours": "пн-пт:11.00-20.00"
            },
            "PICKUP_BOXBERRY_97231": {
                "id": 12609,
                "uid": "PICKUP_BOXBERRY_97231",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "121352, Москва г, Славянский б-р, д.5, корпус 1",
                "address": "121352, Москва г, Славянский б-р, д.5, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.7270092",
                "lon": "37.4715875",
                "workhours": "пн-пт:08.00-20.00, сб-вс:08.00-19.00, обед:14.00-14.30"
            },
            "PICKUP_BOXBERRY_97691": {
                "id": 13489,
                "uid": "PICKUP_BOXBERRY_97691",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "121433, Москва г, Филевская Б. ул, д.37, корпус 1",
                "address": "121433, Москва г, Филевская Б. ул, д.37, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.7423717",
                "lon": "37.4790074",
                "workhours": "пн-пт:10.00-19.00, сб:10.00-17.00"
            },
            "PICKUP_BOXBERRY_19743": {
                "id": 25282,
                "uid": "PICKUP_BOXBERRY_19743",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "121552, Москва г, Ярцевская ул, д.34, строение 1",
                "address": "121552, Москва г, Ярцевская ул, д.34, строение 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.742276",
                "lon": "37.419027",
                "workhours": "пн-пт:09.00-19.45, сб:09.00-17.45"
            },
            "PICKUP_BOXBERRY_99501": {
                "id": 20683,
                "uid": "PICKUP_BOXBERRY_99501",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "121596, Москва г, Толбухина ул, д.13, корпус 2",
                "address": "121596, Москва г, Толбухина ул, д.13, корпус 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.723379",
                "lon": "37.398419",
                "workhours": "пн-пт:11.00-20.00"
            },
            "PICKUP_BOXBERRY_19916": {
                "id": 33158,
                "uid": "PICKUP_BOXBERRY_19916",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "121609, Москва г, Осенний б-р, д.7, корпус 1",
                "address": "121609, Москва г, Осенний б-р, д.7, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.757769",
                "lon": "37.406864",
                "workhours": "пн-вс:09.00-19.45"
            },
            "PICKUP_BOXBERRY_99241": {
                "id": 18900,
                "uid": "PICKUP_BOXBERRY_99241",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "121609, Москва г, Рублёвское ш, д.62",
                "address": "121609, Москва г, Рублёвское ш, д.62",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.766668",
                "lon": "37.381585",
                "workhours": "пн-вс:10.00-22.00"
            },
            "PICKUP_BOXBERRY_19749": {
                "id": 25530,
                "uid": "PICKUP_BOXBERRY_19749",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "121614, Москва г, Крылатская ул, д.31, корпус 2",
                "address": "121614, Москва г, Крылатская ул, д.31, корпус 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.769980",
                "lon": "37.409055",
                "workhours": "пн-пт:09.00-19.00"
            },
            "PICKUP_BOXBERRY_99491": {
                "id": 20458,
                "uid": "PICKUP_BOXBERRY_99491",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "121614, Москва г, Осенний б-р, д.12, корпус 1",
                "address": "121614, Москва г, Осенний б-р, д.12, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.758468",
                "lon": "37.409325",
                "workhours": "пн-пт:10.00-22.00, сб:10.00-21.00, вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_77561": {
                "id": 12380,
                "uid": "PICKUP_BOXBERRY_77561",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "123007, Москва г, Розанова ул, д.4, оф. 4",
                "address": "123007, Москва г, Розанова ул, д.4, оф. 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.7729072",
                "lon": "37.5425540",
                "workhours": "пн-пт:11.00-20.00"
            },
            "PICKUP_BOXBERRY_99861": {
                "id": 21917,
                "uid": "PICKUP_BOXBERRY_99861",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "123022, Москва г, 1905 года ул, д.10, строение 1, оф. 511",
                "address": "123022, Москва г, 1905 года ул, д.10, строение 1, оф. 511",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.767995",
                "lon": "37.558625",
                "workhours": "пн-вс:09.30-19.30"
            },
            "PICKUP_BOXBERRY_19764": {
                "id": 29427,
                "uid": "PICKUP_BOXBERRY_19764",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "123154, Москва г, Генерала Глаголева ул, д.6, корпус 1",
                "address": "123154, Москва г, Генерала Глаголева ул, д.6, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.779914",
                "lon": "37.461589",
                "workhours": "пн-пт:09.00-21.00, сб-вс:10.00-21.00"
            },
            "PICKUP_BOXBERRY_99421": {
                "id": 19956,
                "uid": "PICKUP_BOXBERRY_99421",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "123154, Москва г, Народного Ополчения ул, д.21, корпус 1",
                "address": "123154, Москва г, Народного Ополчения ул, д.21, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.77753",
                "lon": "37.477282",
                "workhours": "пн-пт:10.00-22.00, сб:10.00-21.00, вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_97651": {
                "id": 13447,
                "uid": "PICKUP_BOXBERRY_97651",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "123182, Москва г, Маршала Василевского ул, д.13, корпус 1",
                "address": "123182, Москва г, Маршала Василевского ул, д.13, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.8066107",
                "lon": "37.4665564",
                "workhours": "пн-пт:11.00-19.45, сб:11.00-19.00"
            },
            "PICKUP_BOXBERRY_77691": {
                "id": 12421,
                "uid": "PICKUP_BOXBERRY_77691",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "123242, Москва г, Дружинниковская ул, д.15, оф. 110",
                "address": "123242, Москва г, Дружинниковская ул, д.15, оф. 110",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.7596942",
                "lon": "37.5748934",
                "workhours": "пн-пт:10.00-20.00"
            },
            "PICKUP_BOXBERRY_10.017": {
                "id": 12211,
                "uid": "PICKUP_BOXBERRY_10.017",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "123298, Москва г, Маршала Малиновского ул, д.6, корпус 1, офис 23",
                "address": "123298, Москва г, Маршала Малиновского ул, д.6, корпус 1, офис 23",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.790782",
                "lon": "37.494000",
                "workhours": "пн-пт:10.00-21.00, сб:10.00-18.00"
            },
            "PICKUP_BOXBERRY_97711": {
                "id": 13484,
                "uid": "PICKUP_BOXBERRY_97711",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "123308, Москва г, Хорошевское ш, д.90",
                "address": "123308, Москва г, Хорошевское ш, д.90",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.7779297",
                "lon": "37.5154244",
                "workhours": "пн-вс:09.30-19.30"
            },
            "PICKUP_BOXBERRY_19789": {
                "id": 32799,
                "uid": "PICKUP_BOXBERRY_19789",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "123423, Москва г, Народного Ополчения ул, д.34, строение 1",
                "address": "123423, Москва г, Народного Ополчения ул, д.34, строение 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.785539",
                "lon": "37.484882",
                "workhours": "пн-пт:09.00-19.00"
            },
            "PICKUP_BOXBERRY_99891": {
                "id": 21918,
                "uid": "PICKUP_BOXBERRY_99891",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "123458, Москва г, Таллинская ул, д.26",
                "address": "123458, Москва г, Таллинская ул, д.26",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.799456",
                "lon": "37.407852",
                "workhours": "пн-пт:12.00-20.00, сб:12.00-18.00"
            },
            "PICKUP_BOXBERRY_19716": {
                "id": 24444,
                "uid": "PICKUP_BOXBERRY_19716",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "123458, Москва г, Таллинская ул, д.7",
                "address": "123458, Москва г, Таллинская ул, д.7",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.800468",
                "lon": "37.396129",
                "workhours": "пн-пт:09.00-22.00, сб-вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_79927": {
                "id": 38361,
                "uid": "PICKUP_BOXBERRY_79927",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "123557, Москва г, Грузинская Б. ул, д.20",
                "address": "123557, Москва г, Грузинская Б. ул, д.20",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.765706",
                "lon": "37.579277",
                "workhours": "пн-пт:09.00-21.00, сб:10.00-19.00"
            },
            "PICKUP_BOXBERRY_19953": {
                "id": 34390,
                "uid": "PICKUP_BOXBERRY_19953",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "125047, Москва г, Тверская Застава пл, д.3",
                "address": "125047, Москва г, Тверская Застава пл, д.3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.775338",
                "lon": "37.581981",
                "workhours": "пн-сб:10.00-20.00"
            },
            "PICKUP_BOXBERRY_19760": {
                "id": 25687,
                "uid": "PICKUP_BOXBERRY_19760",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "125080, Москва г, Волоколамское ш, д.1, строение 1",
                "address": "125080, Москва г, Волоколамское ш, д.1, строение 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.806550",
                "lon": "37.504708",
                "workhours": "пн-пт:10.00-19.00, сб:12.00-16.00"
            },
            "PICKUP_BOXBERRY_77871": {
                "id": 12497,
                "uid": "PICKUP_BOXBERRY_77871",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "125171, Москва г, Зои и Александра Космодемьянских ул, д.4, корпус 1",
                "address": "125171, Москва г, Зои и Александра Космодемьянских ул, д.4, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.8181032",
                "lon": "37.5021294",
                "workhours": "пн-вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_77611": {
                "id": 24378,
                "uid": "PICKUP_BOXBERRY_77611",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "125212, Москва г, Кронштадтский б-р, д.9, строение 4",
                "address": "125212, Москва г, Кронштадтский б-р, д.9, строение 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.843252",
                "lon": "37.488925",
                "workhours": "пн-пт:10.00-21.00, сб-вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_10.042": {
                "id": 12312,
                "uid": "PICKUP_BOXBERRY_10.042",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "125212, Москва г, Ленинградское ш, д.58, строение 26, пав. 144",
                "address": "125212, Москва г, Ленинградское ш, д.58, строение 26, пав. 144",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.842327",
                "lon": "37.484379",
                "workhours": "пн-пт:11.30-20.00, сб-вс:12.00-19.00"
            },
            "PICKUP_BOXBERRY_19936": {
                "id": 33821,
                "uid": "PICKUP_BOXBERRY_19936",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "125239, Москва г, Коптевская ул, д.26, корпус 5",
                "address": "125239, Москва г, Коптевская ул, д.26, корпус 5",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.834714",
                "lon": "37.522863",
                "workhours": "пн-пт:10.00-20.00, сб:10.00-16.00"
            },
            "PICKUP_BOXBERRY_97311": {
                "id": 12988,
                "uid": "PICKUP_BOXBERRY_97311",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "125319, Москва г, Академика Ильюшина ул, д.16",
                "address": "125319, Москва г, Академика Ильюшина ул, д.16",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.8063272",
                "lon": "37.5470814",
                "workhours": "пн-пт:10.00-19.00"
            },
            "PICKUP_BOXBERRY_97891": {
                "id": 17539,
                "uid": "PICKUP_BOXBERRY_97891",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "125319, Москва г, улица Черняховского ул, д.4",
                "address": "125319, Москва г, улица Черняховского ул, д.4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.8020622",
                "lon": "37.5334989",
                "workhours": "пн-пт:10.00-20.30, сб:11.00-19.00"
            },
            "PICKUP_BOXBERRY_97861": {
                "id": 13795,
                "uid": "PICKUP_BOXBERRY_97861",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "125362, Москва г, Тушинская ул, д.17",
                "address": "125362, Москва г, Тушинская ул, д.17",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.8266602",
                "lon": "37.4447905",
                "workhours": "пн-пт:10.00-21.00, сб-вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_97531": {
                "id": 13002,
                "uid": "PICKUP_BOXBERRY_97531",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "125363, Москва г, Сходненская ул, д.52, корпус 1",
                "address": "125363, Москва г, Сходненская ул, д.52, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.8486852",
                "lon": "37.4407745",
                "workhours": "пн-вс:09.30-19.30"
            },
            "PICKUP_BOXBERRY_19962": {
                "id": 34795,
                "uid": "PICKUP_BOXBERRY_19962",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "125364, Москва г, Свободы ул, д.48, строение 1",
                "address": "125364, Москва г, Свободы ул, д.48, строение 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.848427",
                "lon": "37.454304",
                "workhours": "пн-пт:11.00-20.00, сб:10.00-18.00"
            },
            "PICKUP_BOXBERRY_19776": {
                "id": 32559,
                "uid": "PICKUP_BOXBERRY_19776",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "125368, Москва г, Ангелов пер, д.9",
                "address": "125368, Москва г, Ангелов пер, д.9",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.851616",
                "lon": "37.346398",
                "workhours": "пн-пт:11.00-20.00, сб:11.00-16.00"
            },
            "PICKUP_BOXBERRY_19702": {
                "id": 24427,
                "uid": "PICKUP_BOXBERRY_19702",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "125368, Москва г, Дубравная ул, д.34, строение 29",
                "address": "125368, Москва г, Дубравная ул, д.34, строение 29",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.846113",
                "lon": "37.358211",
                "workhours": "пн-пт:10.00-22.00, сб:10.00-21.00, вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_77471": {
                "id": 12143,
                "uid": "PICKUP_BOXBERRY_77471",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "125368, Москва г, Дубравная ул, д.36",
                "address": "125368, Москва г, Дубравная ул, д.36",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.845754",
                "lon": "37.355633",
                "workhours": "пн-пт:10.00-21.00, сб:10.00-18.00"
            },
            "PICKUP_BOXBERRY_19987": {
                "id": 36557,
                "uid": "PICKUP_BOXBERRY_19987",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "125412, Москва г, Коровинское ш, д.23, корпус 1",
                "address": "125412, Москва г, Коровинское ш, д.23, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.876945",
                "lon": "37.523420",
                "workhours": "пн-сб:10.00-20.00"
            },
            "PICKUP_BOXBERRY_97171": {
                "id": 12585,
                "uid": "PICKUP_BOXBERRY_97171",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "125414, Москва г, Петрозаводская ул, д.24Б",
                "address": "125414, Москва г, Петрозаводская ул, д.24Б",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.8670712",
                "lon": "37.4935864",
                "workhours": "пн-пт:10.30-20.30, сб:11.00-19.00"
            },
            "PICKUP_BOXBERRY_97411": {
                "id": 12719,
                "uid": "PICKUP_BOXBERRY_97411",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "125424, Москва г, Стратонавтов проезд, д.11, корпус 1",
                "address": "125424, Москва г, Стратонавтов проезд, д.11, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.8273982",
                "lon": "37.4350524",
                "workhours": "пн-пт:10.00-20.00, сб:10.00-18.00"
            },
            "PICKUP_BOXBERRY_19973": {
                "id": 34937,
                "uid": "PICKUP_BOXBERRY_19973",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "125430, Москва г, Пятницкое ш, д.14",
                "address": "125430, Москва г, Пятницкое ш, д.14",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.841625",
                "lon": "37.386166",
                "workhours": "пн-вс:10.00-21.00"
            },
            "PICKUP_BOXBERRY_99901": {
                "id": 21988,
                "uid": "PICKUP_BOXBERRY_99901",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "125430, Москва г, Пятницкое ш, д.27, корпус 1",
                "address": "125430, Москва г, Пятницкое ш, д.27, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.849054",
                "lon": "37.362999",
                "workhours": "пн-пт:10.00-20.00, сб-вс:12.00-17.00"
            },
            "PICKUP_BOXBERRY_79955": {
                "id": 42488,
                "uid": "PICKUP_BOXBERRY_79955",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "125445, Москва г, Смольная ул, д.63Б, корпус 1",
                "address": "125445, Москва г, Смольная ул, д.63Б, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.869092",
                "lon": "37.469359",
                "workhours": "пн-вс:10.00-21.00"
            },
            "PICKUP_BOXBERRY_79931": {
                "id": 38426,
                "uid": "PICKUP_BOXBERRY_79931",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "125459, Москва г, Яна Райниса б-р, д.19, корпус 2",
                "address": "125459, Москва г, Яна Райниса б-р, д.19, корпус 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.848028",
                "lon": "37.427776",
                "workhours": "пн-пт:09.00-20.00, сб:10.00-15.00"
            },
            "PICKUP_BOXBERRY_19712": {
                "id": 24440,
                "uid": "PICKUP_BOXBERRY_19712",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "125480, Москва г, Героев Панфиловцев ул, д.8, корпус 1",
                "address": "125480, Москва г, Героев Панфиловцев ул, д.8, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.854835",
                "lon": "37.439275",
                "workhours": "пн-пт:10.00-22.00, сб-вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_19994": {
                "id": 37554,
                "uid": "PICKUP_BOXBERRY_19994",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "125481, Москва г, Планерная ул, д.14, корпус 2",
                "address": "125481, Москва г, Планерная ул, д.14, корпус 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.863783",
                "lon": "37.437451",
                "workhours": "пн-вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_97961": {
                "id": 13874,
                "uid": "PICKUP_BOXBERRY_97961",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "125565, Москва г, Фестивальная ул, д.13, корпус 1",
                "address": "125565, Москва г, Фестивальная ул, д.13, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.85578",
                "lon": "37.477381",
                "workhours": "пн-вс:10.00-22.00"
            },
            "PICKUP_BOXBERRY_99261": {
                "id": 18922,
                "uid": "PICKUP_BOXBERRY_99261",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "127006, Москва г, Настасьинский пер, д.8, строение 2",
                "address": "127006, Москва г, Настасьинский пер, д.8, строение 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.767195",
                "lon": "37.605499",
                "workhours": "пн-вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_99461": {
                "id": 20383,
                "uid": "PICKUP_BOXBERRY_99461",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "127018, Москва г, Сущёвский Вал ул, д.46",
                "address": "127018, Москва г, Сущёвский Вал ул, д.46",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.792701",
                "lon": "37.612605",
                "workhours": "пн-пт:10.00-22.00, сб:10.00-21.00, вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_99781": {
                "id": 21643,
                "uid": "PICKUP_BOXBERRY_99781",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "127018, Москва г, Сущёвский Вал ул, д.5, строение 20, павильон Т-1",
                "address": "127018, Москва г, Сущёвский Вал ул, д.5, строение 20, павильон Т-1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.794143",
                "lon": "37.592824",
                "workhours": "пн-сб:10.00-20.00"
            },
            "PICKUP_BOXBERRY_99951": {
                "id": 22259,
                "uid": "PICKUP_BOXBERRY_99951",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "127051, Москва г, Трубная ул, д.32, строение 4",
                "address": "127051, Москва г, Трубная ул, д.32, строение 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.771894",
                "lon": "37.626232",
                "workhours": "пн-пт:10.00-19.00"
            },
            "PICKUP_BOXBERRY_19775": {
                "id": 31698,
                "uid": "PICKUP_BOXBERRY_19775",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "127055, Москва г, Новослободская ул, д.57/65",
                "address": "127055, Москва г, Новослободская ул, д.57/65",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.787948",
                "lon": "37.592105",
                "workhours": "пн-пт:09.00-19.00"
            },
            "PICKUP_BOXBERRY_19727": {
                "id": 25078,
                "uid": "PICKUP_BOXBERRY_19727",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "127106, Москва г, Алтуфьевское ш, д.27",
                "address": "127106, Москва г, Алтуфьевское ш, д.27",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.857498",
                "lon": "37.583131",
                "workhours": "пн-вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_79945": {
                "id": 40113,
                "uid": "PICKUP_BOXBERRY_79945",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "127106, Москва г, Гостиничный проезд, д.4а, строение 1",
                "address": "127106, Москва г, Гостиничный проезд, д.4а, строение 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.843404",
                "lon": "37.578442",
                "workhours": "пн-пт:09.00-21.00, сб-вс:10.00-21.00"
            },
            "PICKUP_BOXBERRY_79912": {
                "id": 37714,
                "uid": "PICKUP_BOXBERRY_79912",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "127221, Москва г, Полярная ул, д.10, строение 1",
                "address": "127221, Москва г, Полярная ул, д.10, строение 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.874561",
                "lon": "37.638270",
                "workhours": "пн-пт:11.00-20.00, сб-вс:11.00-18.00"
            },
            "PICKUP_BOXBERRY_97941": {
                "id": 13867,
                "uid": "PICKUP_BOXBERRY_97941",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "127224, Москва г, Шокальского проезд, д.61, пав. 9",
                "address": "127224, Москва г, Шокальского проезд, д.61, пав. 9",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.883989",
                "lon": "37.666782",
                "workhours": "пн-пт:10.00-20.30, сб:10.00-19.00, вс:12.00-19.00"
            },
            "PICKUP_BOXBERRY_79930": {
                "id": 38287,
                "uid": "PICKUP_BOXBERRY_79930",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "127247, Москва г, 800-летия Москвы ул, д.14",
                "address": "127247, Москва г, 800-летия Москвы ул, д.14",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.878016",
                "lon": "37.548941",
                "workhours": "пн-сб:10.00-20.00"
            },
            "PICKUP_BOXBERRY_19794": {
                "id": 33118,
                "uid": "PICKUP_BOXBERRY_19794",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "127254, Москва г, Руставели ул, д.3, корпус 7",
                "address": "127254, Москва г, Руставели ул, д.3, корпус 7",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.813430",
                "lon": "37.584972",
                "workhours": "пн-пт:10.00-22.00, сб-вс:11.00-21.00"
            },
            "PICKUP_BOXBERRY_99411": {
                "id": 19955,
                "uid": "PICKUP_BOXBERRY_99411",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "127322, Москва г, Яблочкова ул, д.21а",
                "address": "127322, Москва г, Яблочкова ул, д.21а",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.819489",
                "lon": "37.578271",
                "workhours": "пн-пт:10.00-21.00, сб-вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_19910": {
                "id": 33150,
                "uid": "PICKUP_BOXBERRY_19910",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "127322, Москва г, Яблочкова ул, д.35",
                "address": "127322, Москва г, Яблочкова ул, д.35",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.825947",
                "lon": "37.578666",
                "workhours": "пн-пт:09.30-20.00, сб:10.00-16.00"
            },
            "PICKUP_BOXBERRY_97151": {
                "id": 12577,
                "uid": "PICKUP_BOXBERRY_97151",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "127349, Москва г, Алтуфьевское ш, д.80",
                "address": "127349, Москва г, Алтуфьевское ш, д.80",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.8943232",
                "lon": "37.5882424",
                "workhours": "пн-пт:10.00-20.30, сб:10.00-19.00"
            },
            "PICKUP_BOXBERRY_77461": {
                "id": 12142,
                "uid": "PICKUP_BOXBERRY_77461",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "127422, Москва г, Тимирязевская ул, д.2/3",
                "address": "127422, Москва г, Тимирязевская ул, д.2/3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.8079212",
                "lon": "37.5735100",
                "workhours": "пн-пт:09.00-21.00, сб:10.00-21.00, вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_99091": {
                "id": 19348,
                "uid": "PICKUP_BOXBERRY_99091",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "127427, Москва г, Ботаническая ул, д.17, корпус 1",
                "address": "127427, Москва г, Ботаническая ул, д.17, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.826721",
                "lon": "37.600109",
                "workhours": "пн-вс:08.00-23.00"
            },
            "PICKUP_BOXBERRY_99101": {
                "id": 18833,
                "uid": "PICKUP_BOXBERRY_99101",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "127474, Москва г, Бескудниковский бульвар б-р, д.6, корпус 4",
                "address": "127474, Москва г, Бескудниковский бульвар б-р, д.6, корпус 4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.8585592",
                "lon": "37.5644904",
                "workhours": "пн-сб:10.00-20.00"
            },
            "PICKUP_BOXBERRY_97801": {
                "id": 13784,
                "uid": "PICKUP_BOXBERRY_97801",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "127486, Москва г, Коровинское ш, д.1, корпус 1",
                "address": "127486, Москва г, Коровинское ш, д.1, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.8652072",
                "lon": "37.5434524",
                "workhours": "пн-пт:10.00-20.00, сб:10.00-18.00, вс:10.00-15.00, обед:14.00-15.00"
            },
            "PICKUP_BOXBERRY_77971": {
                "id": 12544,
                "uid": "PICKUP_BOXBERRY_77971",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "127490, Москва г, Декабристов ул, д.27",
                "address": "127490, Москва г, Декабристов ул, д.27",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.863091",
                "lon": "37.609514",
                "workhours": "пн-пт:12.00-20.30, сб:12.00-19.00"
            },
            "PICKUP_BOXBERRY_19970": {
                "id": 35207,
                "uid": "PICKUP_BOXBERRY_19970",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "127495, Москва г, Дмитровское ш, д.165Е, корпус 12",
                "address": "127495, Москва г, Дмитровское ш, д.165Е, корпус 12",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.927937",
                "lon": "37.545438",
                "workhours": "пн-сб:10.00-20.00, вс:11.00-19.00"
            },
            "PICKUP_BOXBERRY_99581": {
                "id": 24281,
                "uid": "PICKUP_BOXBERRY_99581",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "127549, Москва г, Бибиревская ул, д.10, корпус 2",
                "address": "127549, Москва г, Бибиревская ул, д.10, корпус 2",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.881460",
                "lon": "37.602382",
                "workhours": "пн-пт:10.00-22.00, сб-вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_19744": {
                "id": 25283,
                "uid": "PICKUP_BOXBERRY_19744",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "127560, Москва г, Пришвина ул, д.22",
                "address": "127560, Москва г, Пришвина ул, д.22",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.885469",
                "lon": "37.602166",
                "workhours": "пн-вс:10.30-20.30"
            },
            "PICKUP_BOXBERRY_79932": {
                "id": 38531,
                "uid": "PICKUP_BOXBERRY_79932",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "127562, Москва г, Санникова ул, д.13",
                "address": "127562, Москва г, Санникова ул, д.13",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.861653",
                "lon": "37.597612",
                "workhours": "пн-пт:09.00-21.00, сб-вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_79956": {
                "id": 40287,
                "uid": "PICKUP_BOXBERRY_79956",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "127576, Москва г, Череповецкая ул, д.24",
                "address": "127576, Москва г, Череповецкая ул, д.24",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.897236",
                "lon": "37.584856",
                "workhours": "пн-вс:09.00-20.00"
            },
            "PICKUP_BOXBERRY_19713": {
                "id": 24441,
                "uid": "PICKUP_BOXBERRY_19713",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "127591, Москва г, Дмитровское ш, д.108Б, строение 1",
                "address": "127591, Москва г, Дмитровское ш, д.108Б, строение 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.886539",
                "lon": "37.543713",
                "workhours": "пн-пт:10.00-21.00, сб-вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_77451": {
                "id": 12136,
                "uid": "PICKUP_BOXBERRY_77451",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "129090, Москва г, Олимпийский пр-кт, д.16, строение 1",
                "address": "129090, Москва г, Олимпийский пр-кт, д.16, строение 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.781079",
                "lon": "37.626484",
                "workhours": "пн-сб:10.00-20.00"
            },
            "PICKUP_BOXBERRY_99661": {
                "id": 21240,
                "uid": "PICKUP_BOXBERRY_99661",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "129110, Москва г, Мира пр-кт, д.51, строение 1",
                "address": "129110, Москва г, Мира пр-кт, д.51, строение 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.784000",
                "lon": "37.634218",
                "workhours": "пн-пт:09.00-20.00, сб:10.00-18.00"
            },
            "PICKUP_BOXBERRY_19950": {
                "id": 34284,
                "uid": "PICKUP_BOXBERRY_19950",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "129110, Москва г, Мира пр-кт, д.73",
                "address": "129110, Москва г, Мира пр-кт, д.73",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.789072",
                "lon": "37.634272",
                "workhours": "пн-пт:10.00-20.00, сб-вс:11.00-17.00"
            },
            "PICKUP_BOXBERRY_79911": {
                "id": 38708,
                "uid": "PICKUP_BOXBERRY_79911",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "129164, Москва г, Ярославская ул, д.8, корпус 3",
                "address": "129164, Москва г, Ярославская ул, д.8, корпус 3",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.817643",
                "lon": "37.649696",
                "workhours": "пн-пт:10.00-21.00, сб-вс:11.00-18.00"
            },
            "PICKUP_BOXBERRY_79934": {
                "id": 38587,
                "uid": "PICKUP_BOXBERRY_79934",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "129226, Москва г, Сельскохозяйственная ул, д.17, корпус 1",
                "address": "129226, Москва г, Сельскохозяйственная ул, д.17, корпус 1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.837414",
                "lon": "37.636356",
                "workhours": "пн-пт:11.00-20.00, сб-вс:11.00-18.00"
            },
            "PICKUP_BOXBERRY_77821": {
                "id": 12474,
                "uid": "PICKUP_BOXBERRY_77821",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "129281, Москва г, Менжинского ул, д.36, оф. 40",
                "address": "129281, Москва г, Менжинского ул, д.36, оф. 40",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.8704502",
                "lon": "37.6632694",
                "workhours": "пн-пт:12.00-20.30, сб-вс:12.00-19.00"
            },
            "PICKUP_BOXBERRY_99941": {
                "id": 24140,
                "uid": "PICKUP_BOXBERRY_99941",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "129323, Москва г, Русанова проезд, д.35",
                "address": "129323, Москва г, Русанова проезд, д.35",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.857028",
                "lon": "37.637470",
                "workhours": "пн-сб:09.00-20.00, вс:10.00-18.00"
            },
            "PICKUP_BOXBERRY_19931": {
                "id": 33822,
                "uid": "PICKUP_BOXBERRY_19931",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "129323, Москва г, Серебрякова проезд, д.2, строение 1А",
                "address": "129323, Москва г, Серебрякова проезд, д.2, строение 1А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.846502",
                "lon": "37.646543",
                "workhours": "пн-пт:10.00-20.00, сб-вс:10.00-15.00"
            },
            "PICKUP_BOXBERRY_19768": {
                "id": 29633,
                "uid": "PICKUP_BOXBERRY_19768",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "129329, Москва г, Кольская ул, д.1, строение 10",
                "address": "129329, Москва г, Кольская ул, д.1, строение 10",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.857806",
                "lon": "37.651008",
                "workhours": "пн-пт:08.00-21.00, сб-вс:09.00-14.00"
            },
            "PICKUP_BOXBERRY_19985": {
                "id": 37054,
                "uid": "PICKUP_BOXBERRY_19985",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "129336, Москва г, Анадырский проезд, д.63А",
                "address": "129336, Москва г, Анадырский проезд, д.63А",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.880359",
                "lon": "37.708733",
                "workhours": "пн-пт:11.00-20.00, сб:11.00-16.00"
            },
            "PICKUP_BOXBERRY_19783": {
                "id": 32794,
                "uid": "PICKUP_BOXBERRY_19783",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "129344, Москва г, Печорская ул, д.1",
                "address": "129344, Москва г, Печорская ул, д.1",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.862949",
                "lon": "37.673286",
                "workhours": "пн-пт:09.00-19.00, сб:09.00-15.00"
            },
            "PICKUP_BOXBERRY_99271": {
                "id": 18923,
                "uid": "PICKUP_BOXBERRY_99271",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "129515, Москва г, Звёздный б-р, д.10, строение 1, оф. 34",
                "address": "129515, Москва г, Звёздный б-р, д.10, строение 1, оф. 34",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.817041",
                "lon": "37.63323",
                "workhours": "пн-вс:10.00-21.00"
            },
            "PICKUP_BOXBERRY_19754": {
                "id": 25681,
                "uid": "PICKUP_BOXBERRY_19754",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "129515, Москва г, Останкинская 1-я ул, д.41/9",
                "address": "129515, Москва г, Останкинская 1-я ул, д.41/9",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.823970",
                "lon": "37.629601",
                "workhours": "пн-сб:09.00-20.00, вс:09.00-19.00"
            },
            "PICKUP_BOXBERRY_99671": {
                "id": 21197,
                "uid": "PICKUP_BOXBERRY_99671",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "129515, Москва г, Останкинская 1-я ул, д.55",
                "address": "129515, Москва г, Останкинская 1-я ул, д.55",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.824005",
                "lon": "37.634434",
                "workhours": "пн-пт:10.00-21.00, сб-вс:10.00-20.00"
            },
            "PICKUP_BOXBERRY_10.035": {
                "id": 12302,
                "uid": "PICKUP_BOXBERRY_10.035",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "129626, Москва г, Староалексеевская ул, д.4, оф. 1П",
                "address": "129626, Москва г, Староалексеевская ул, д.4, оф. 1П",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.8088467",
                "lon": "37.6386825",
                "workhours": "пн-пт:11.00-19.00, обед:15.00-15.45"
            },
            "PICKUP_BOXBERRY_99651": {
                "id": 24598,
                "uid": "PICKUP_BOXBERRY_99651",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "142702, Москва г, Боровское ш, д.6",
                "address": "142702, Москва г, Боровское ш, д.6",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.658794",
                "lon": "37.401905",
                "workhours": "пн-вс:10.00-21.00"
            },
            "PICKUP_BOXBERRY_19736": {
                "id": 25213,
                "uid": "PICKUP_BOXBERRY_19736",
                "isNumericCode": true,
                "agent": 3271516,
                "price": 200,
                "title": "142750, Москва г, Летчика Ульянина ул, д.4",
                "address": "142750, Москва г, Летчика Ульянина ул, д.4",
                "addressParts": [],
                "contactLink": "",
                "pickupDatetime": {
                    "date": "2018-09-10 18:57:00.000000",
                    "timezone_type": 3,
                    "timezone": "Europe/Moscow"
                },
                "pickupTitle": "Дата поступления 10 сентября",
                "lat": "55.624967",
                "lon": "37.306540",
                "workhours": "пн-пт:10.00-19.00"
            }
        },
        "paymentTypes": [20, 2, 9, 25, 39, 42, 48],
        "price": 200,
        "pickupDatetime": {
            "date": "2018-09-10 18:57:00.000000",
            "timezone_type": 3,
            "timezone": "Europe/Moscow"
        },
        "pickupDatetimeTimestamp": 1536595020,
        "pickupTitle": "Дата поступления 10 сентября"
    }
};
