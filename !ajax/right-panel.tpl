              <div class='tpl-right'>
                <div class="--tools"><a href="?" class="ch-icons ch-goback">Вернуться в корзину</a></div>
                <ul class="items">
                    <li><div class="img"><img src="/img/cart-item-01.jpg"></div>
                        <div class="descr">
                            <span class="title">Смартфон Apple iPhone 6s 16 Gb 3A018RU Space Gray Night One...</span>
                            <div class="ttl"><i></i><em>1 шт</em><strong>63 580 <span class="rub">a</span></strong></div>
                        </div>
                    </li>
                    <li><div class="img"><img src="http://www.123.ru/m_pics/80068401.jpg"></div>
                        <div class="descr">
                            <span class="title">Смартфон Samsung Galaxy S9+ 64 Гб черный (SM-G965FZKDSER)</span>
                            <div class="ttl"><i><b>подарок</b></i><em>1 шт</em><strong>0 <span class="rub">a</span><small>58 070 <span class="rub">a</span></small></strong></div>
                        </div>
                    </li>
                    <li><div class="img"><img src="http://www.123.ru/m_pics/80004818.jpg"></div>
                        <div class="descr">
                            <span class="title">Видеокарта GigaByte Radeon RX 570 GV-RX570AORUS-4GD PCI-E 4096Mb 256 Bit...</span>
                            <div class="ttl"><i></i><em>2 шт</em><strong>17 790 <span class="rub">a</span></strong></div>
                        </div>
                    </li>
                </ul>
                <div class="parts">
                    <p class="bonuses"><b>+140</b> баллов на личный счет</p>
                    <p><b>Доставка:</b> самовывоз из пункта №12, м. Нагатинская</p>
                    <p><b>Метод оплаты:</b> наличные при получении</p>
                </div>
                <ul class="amounts">
                    <li class="total-count"><span>Итого в корзине товаров:</span><em><strong>3</strong> шт</em></li>
                    <li class="discount"><span>Скидка:</span><em>–<strong>435</strong> <span class="rub">a</span></em></li>
                    <li class="total"><span>Итоговая сумма:</span><em><strong>19200</strong> <span class="rub">a</span></em></li>
                </ul>
              </div>