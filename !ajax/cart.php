<?php

    header("Content-Type: application/json; charset=utf-8", true);
    $result = array();
    
    switch ($_REQUEST["a"]) {
    
        case "start-order":
                /*
                    начало процедуры оформления заказа
                    
                    $result["state"] = ok | error

                */        
                
                $result["state"] = "ok";
                $result["tplright"] = file_get_contents("right-panel.tpl");
                
            break;

        case "set-delivery":
                /*
                    выбор доставки/самовывоза
                    $_REQUEST["type"] => атрибут data-type вкладок доставки
                    $_REQUEST[".."] => значения полей вкладки
                    $result["state"] = ok | error

                */        
                
                $result["state"] = "ok";
                $result["tplright"] = file_get_contents("right-panel.tpl");
        
            break;
            
        case "set-payment":
                /*
                    выбор способа оплаты
                    $_REQUEST[".."] => значения полей блока
                    $result["state"] = ok | error

                */        
                
                $result["state"] = "ok";
                $result["tplright"] = file_get_contents("right-panel.tpl");
        
            break;

        case "login":
                /*
                    авторизация на странице заказа.
                    $_REQUEST["type"] => email / phone
                    $_REQUEST["login"]
                    $_REQUEST["password"]
                    $_REQUEST["agreement"] => согласие на обработку данных
                    $_REQUEST["remember"] => запомнить меня
                    
                    $result["state"] => ok | error
                    $result["message"] => сообщение об ошибке
                    $result["tplright"] => html-код правой панели
                */
                if (preg_match('/^9/', $_REQUEST["login"])) {
                    $result["state"] = "error";
                    $result["message"] = "Неверный логин";
                } else {
                    $result["state"] = "ok";
                }
                $result["tplright"] = file_get_contents("right-panel.tpl");
            break;

        case "set-promo":
                $discount = 10;
                /* отправка суммы списываемых баллов или промо-кода 
                   $_REQUEST["v"] => введенные юзером данные
                   $_REQUEST["type"] => значение атрибута data-type нажатой кнопки
                   
                   $result["state"] => ok | error
                   $result["discount"] => сумма скидки по промо-коду
                   $result["message"] => сообщение пользователю
                   $result["tplright"] => html-код правой панели
                */
                if (preg_match('/reset/', $_REQUEST["type"])) {
                    $result["state"] = "ok";
                    $result["message"] = "";
                    $discount = 0;
                } elseif (preg_match('/^9/', $_REQUEST["v"])) {
                    $result["state"] = "error";
                    if (preg_match('/balls/', $_REQUEST['type'])) $result["message"] = "Недостаточно баллов на счете"; else $result["message"] = "Промокод недействителен";
                } else {
                    $result["state"] = "ok";
                    if (preg_match('/balls/', $_REQUEST['type'])) $result["message"] = "Баллы успешно списаны"; else $result["message"] = "Промокод успешно применен";
                    $discount = rand(5,50)*10;
                }
                $tpl = file_get_contents("right-panel.tpl");
                $tpl = preg_replace('/435/uim', $discount, $tpl);
                $tpl = preg_replace('/19200/uim', (19200-$discount), $tpl);
                $result["tplright"] = $tpl;
                $result["discount"] = $discount;
            break;
    
        case "apply-promo":
                // $_REQUEST["promocode"]; => промо-код или 0 для отмены
                // $result => { newPrice: "11 000", newDiscount: "Скидка -10%", newDiscountAmount: "2 120 р." }
                
                if ($_REQUEST["promocode"] == 0) {
                    $result = array( "state"=>"ok", "newPrice"=>"11 000", "newDiscount"=> "", "newDiscountAmount"=> "" );
                } else {
                    $result = array( "state"=>"ok", "newPrice"=>"11 000", "newDiscount"=> "Скидка -10%", "newDiscountAmount"=> "2 120 р." );
                }
                
            break;
    }
    
    
    echo json_encode($result);