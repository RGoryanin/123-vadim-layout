$(function(){
    var voidf = function(ev) { ev.preventDefault(); return false; }

    var onr = function() {
        $("meta[name=viewport]").remove();
        if ($(window).width()>$(window).height()) {
            $("head").append('<meta name="viewport" content="width=600,user-scalable=no">');
            $("html").addClass("wide");
        } else {
            $("head").append('<meta name="viewport" content="width=400,user-scalable=no">');
            $("html").removeClass("wide");
        }
        return true;
    }
    onr();
    window.addEventListener("resize", onr);

    var panelInit = function(el) {
        var _x = 0, _y = 0, _bl = el.offset().left, _l, _st, _mk;
        el[0].addEventListener("touchstart", function(ev) {
            var touches = ev.changedTouches;
            if (touches.length>1) return true;
            _mk = performance.now();
            _x = touches[0].clientX, _y = touches[0].clientY, _l = el.offset().left, _st = el.find(">*:first-child").scrollTop(); 
            $(".nav-ovl").css({display: "block"});
            el.addClass("drg");
        });
        el[0].addEventListener("touchend", function(ev) {
            var touches = ev.changedTouches;
            if (touches.length>1) return true;
            var x = touches[0].clientX, y = touches[0].clientY;
            if (Math.abs(y-_y)>Math.abs(x-_x)) {
                var t = performance.now()-_mk;
                if (t<300) {
                    var dy = (y-_y)*(t-300)/100, c=el.find(">*:first-child");
console.log(dy);
                    c.animate({scrollTop: c.scrollTop()+dy}, 500);
                }
            } else {
                if (x-_x>el.width()*.2) {
                    el.css({left: 0});
                    $(".nav-ovl").css({opacity: 1, display: "block"});
                    $("main").css({left: el.find(">*:first-child").width()});
                } else if (x-_x<el.width()*-.2) {
                    el.css({left: ""});
                    $(".nav-ovl").css({opacity: 0});
                    setTimeout(function() { $(".nav-ovl").css({display: "none"}) }, 300);
                    $("main").css({left: 0});
                } else {
                    el.css({left: _l+"px"});
                    if (_l<0) {
                        $(".nav-ovl").css({opacity: 0});
                        setTimeout(function() { $(".nav-ovl").css({display: "none"}) }, 300);
                        $("main").css({left: 0});
                    } else {
                        $(".nav-ovl").css({opacity: 1, display: "block"});
                        $("main").css({left: el.find(">*:first-child").width()});
                    }
                }
                el.removeClass("drg");
            }
            return true;
        });
        el[0].addEventListener("touchmove", function(ev) {
            var touches = ev.changedTouches;
            if (touches.length>1) return true;
            var x = touches[0].clientX, y = touches[0].clientY;
            if (x<0) x=0;
            if (x>el.find(">*").eq(0).width()) x = el.find(">*:first-child").width();
            if (Math.abs(y-_y)>Math.abs(x-_x) && _l>=0) {
                x=_x;
                el.find(">*:first-child").scrollTop(_st-y+_y);
            }
            if (_l<0) {
                $(".nav-ovl").css({opacity: x/el.find(">*:first-child").width() });
                $("main").css({left: x});
                el.css({left: (_bl+x)+"px"});
            } else {
                var px = _l-_x+x;
                if (px>0) px=0;
                $("main").css({left: px+el.find(">*:first-child").width()});
                el.css({left: px+"px"});
            }
            ev.preventDefault();
            return false;
        });
        el[0].addEventListener("touchcancel", function(ev) {
            var touches = ev.changedTouches;
            if (touches.length>1) return true;
            ev.preventDefault();
            $(".nav-ovl").css({display: "none"});
            el.css({left: ""});
            el.removeClass("drg");
            return false;
        });
    }

    panelInit($(".left-panel"));
});
