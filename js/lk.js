 $(function () {
    $(document).ready(function(){
        $(document).on(clickHandler,'.lk-p-show-text',function(e){
            e.preventDefault;
            var $this = $(this),
                order = $this.data('order'),
                w320 = window.innerWidth >= 768 ? '' : '-320',
                block = $('#order-info-' + order + ' .lk-p-info-wrapper' + w320);
            console.log(w320 + ' ' + $this.parents('.lk-p-info').find('.lk-p-info-wrapper-320').is(':visible'));
            $this.html(!block.is(':visible') ? $this.data('text-collapsed') : $this.data('text'));
            block.slideToggle(function(){
                $this.toggleClass('lk-collapsed');
                $('#order-info-' + order + ' .lk-p-info-wrapper,#order-info-' + order + ' .lk-p-info-wrapper-320').toggleClass('lk-collapsed');
                block.attr('style','')
            });
            $('.lk-shadowed').removeClass('')
            $this.parents('.lk-p-320-block').toggleClass('lk-shadowed');
            return false;
        });
        $(document).on('mouseover','.lk-hint i',function(){
            var $this = $(this);
            $this.parents('.lk-hint').find('.lk-hint-text').fadeIn(250);
        });
        $(document).on('mouseleave','.lk-hint i',function(){
            var $this = $(this);
            $this.parents('.lk-hint').find('.lk-hint-text').fadeOut(250);
        });

			$(document).on(clickHandler, '.lk-button-cancel', function(e) {
				var _ = $(this);

				if ('#' == _.attr('href')) {
					e.stopPropagation();
					var number = _.attr('data-order');
					var agent = _.attr('data-agent');
					if (number && agent) {
						$.ajax({
							url: '/cabinet/reserves/delete/',
							action: 'get',
							data: { order: number, agent: agent },
							dataType: 'json',
							success: function(data) {
								if (data.success) {
									$('.reserve-' + number).remove();
								}
							}
						});
					}
					return false;
				}
			});
       
        $('.lk-pfb-input-date').on(clickHandler,'.icon',function(e){
            e.stopPropagation();
            $('.lk-pfb-input-date > input').trigger('focus');
        })
        if ($('.lk-pfb-phone input').length > 0){
            //var im = new Inputmask("+7 (999) 999 99-99");
            //im.mask($('.lk-pfb-phone input')[0]);
        }
        $('.footer-slick-slider').slick({
            slidesToShow: 5,
            arrows:false,
            dots:false,
            accessibility: false,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
                    dots: true,
                    arrows: false,
                    accessibility: false,
                    appendDots: $('.footer-slick-slider + .slick-pagination')
                }
            }]
        });
        $('.lk-home-menu .slick-wrapper').slick({
            slidesToShow: 4,
            arrows:false,
            dots:false,
            accessibility: false,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
                    dots: true,
                    arrows: false,
                    accessibility: false,
                    appendDots: $('.lk-home-menu .slick-pagination')
                }
            }]
        });
    });
});
