var RRHelper = {
	
	debug: true,

	info: function(event, data) {
		if (this.debug) {
			console.log('rr', 'info', event, data);
		}
	},

	warn: function(event, data) {
		if (this.debug) {
			console.log('rr', 'warning', event, data);
		}
	},

	addToBasket: function(products) {
		var event = 'add to basket';
		try {
			for (var i = 0, size = products.length; i < size; i++) {
				var productId = products[i];
				if (0 < productId) {
					rrApi.addToBasket(productId);
					this.info(event, productId);
				} else {
					this.warn(event, 'invalid product id = ' + productId);
				}
			}
		} catch (e) {
			this.warn(event, e);
		}
	},

	setEmail: function(email) {

        if(!this.validateEmail(email)) return false;

		var event = 'set email';
		try {
			rrApi.setEmail(email);
			this.info(event, email);
		} catch (e) {
			this.warn(event, e);
		}
	},

	welcomeSequence: function(email) {

        if(!this.validateEmail(email)) return false;

		var event = 'welcome sequence';
		try {
			rrApi.welcomeSequence(email);
			this.info(event, email);
		} catch (e) {
			this.warn(event, e);
		}
	},

	order: function(orderId, orderAmount, products) {
		var event = 'order';
		var obj = {
			transaction: orderId,
			items: []
		};
		for (var i = 0; i < products.length; i++) {
			var product = products[i];
			var item = {
				id: product.rrId,
				qnt: product.quantity,
				price: product.price
			};
			obj.items.push(item);
		}
		try {
			rrApi.order(obj);
			this.info(event, obj);
		} catch (e) {
			this.warn(event, e);
		}
	},

	search: function(phrase) {
		var event = 'search';
		try {
			rrApi.search(phrase);
			this.info(event, obj);
		} catch (e) {
			this.warn(event, e);
		}
	},

    validateEmail: function(email) {

        var result = true;

        if (-1 !== email.indexOf('@unknown.email')) {
            result = false;
        }

        return result;
    }

};