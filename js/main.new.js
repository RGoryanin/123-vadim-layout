/******/ (function(modules) { // webpackBootstrap
/******/ 	function hotDisposeChunk(chunkId) {
/******/ 		delete installedChunks[chunkId];
/******/ 	}
/******/ 	var parentHotUpdateCallback = window["webpackHotUpdate"];
/******/ 	window["webpackHotUpdate"] = 
/******/ 	function webpackHotUpdateCallback(chunkId, moreModules) { // eslint-disable-line no-unused-vars
/******/ 		hotAddUpdateChunk(chunkId, moreModules);
/******/ 		if(parentHotUpdateCallback) parentHotUpdateCallback(chunkId, moreModules);
/******/ 	} ;
/******/ 	
/******/ 	function hotDownloadUpdateChunk(chunkId) { // eslint-disable-line no-unused-vars
/******/ 		var head = document.getElementsByTagName("head")[0];
/******/ 		var script = document.createElement("script");
/******/ 		script.type = "text/javascript";
/******/ 		script.charset = "utf-8";
/******/ 		script.src = __webpack_require__.p + "" + chunkId + "." + hotCurrentHash + ".hot-update.js";
/******/ 		;
/******/ 		head.appendChild(script);
/******/ 	}
/******/ 	
/******/ 	function hotDownloadManifest(requestTimeout) { // eslint-disable-line no-unused-vars
/******/ 		requestTimeout = requestTimeout || 10000;
/******/ 		return new Promise(function(resolve, reject) {
/******/ 			if(typeof XMLHttpRequest === "undefined")
/******/ 				return reject(new Error("No browser support"));
/******/ 			try {
/******/ 				var request = new XMLHttpRequest();
/******/ 				var requestPath = __webpack_require__.p + "" + hotCurrentHash + ".hot-update.json";
/******/ 				request.open("GET", requestPath, true);
/******/ 				request.timeout = requestTimeout;
/******/ 				request.send(null);
/******/ 			} catch(err) {
/******/ 				return reject(err);
/******/ 			}
/******/ 			request.onreadystatechange = function() {
/******/ 				if(request.readyState !== 4) return;
/******/ 				if(request.status === 0) {
/******/ 					// timeout
/******/ 					reject(new Error("Manifest request to " + requestPath + " timed out."));
/******/ 				} else if(request.status === 404) {
/******/ 					// no update available
/******/ 					resolve();
/******/ 				} else if(request.status !== 200 && request.status !== 304) {
/******/ 					// other failure
/******/ 					reject(new Error("Manifest request to " + requestPath + " failed."));
/******/ 				} else {
/******/ 					// success
/******/ 					try {
/******/ 						var update = JSON.parse(request.responseText);
/******/ 					} catch(e) {
/******/ 						reject(e);
/******/ 						return;
/******/ 					}
/******/ 					resolve(update);
/******/ 				}
/******/ 			};
/******/ 		});
/******/ 	}
/******/
/******/ 	
/******/ 	
/******/ 	var hotApplyOnUpdate = true;
/******/ 	var hotCurrentHash = "a4f0b33172fade83972c"; // eslint-disable-line no-unused-vars
/******/ 	var hotRequestTimeout = 10000;
/******/ 	var hotCurrentModuleData = {};
/******/ 	var hotCurrentChildModule; // eslint-disable-line no-unused-vars
/******/ 	var hotCurrentParents = []; // eslint-disable-line no-unused-vars
/******/ 	var hotCurrentParentsTemp = []; // eslint-disable-line no-unused-vars
/******/ 	
/******/ 	function hotCreateRequire(moduleId) { // eslint-disable-line no-unused-vars
/******/ 		var me = installedModules[moduleId];
/******/ 		if(!me) return __webpack_require__;
/******/ 		var fn = function(request) {
/******/ 			if(me.hot.active) {
/******/ 				if(installedModules[request]) {
/******/ 					if(installedModules[request].parents.indexOf(moduleId) < 0)
/******/ 						installedModules[request].parents.push(moduleId);
/******/ 				} else {
/******/ 					hotCurrentParents = [moduleId];
/******/ 					hotCurrentChildModule = request;
/******/ 				}
/******/ 				if(me.children.indexOf(request) < 0)
/******/ 					me.children.push(request);
/******/ 			} else {
/******/ 				console.warn("[HMR] unexpected require(" + request + ") from disposed module " + moduleId);
/******/ 				hotCurrentParents = [];
/******/ 			}
/******/ 			return __webpack_require__(request);
/******/ 		};
/******/ 		var ObjectFactory = function ObjectFactory(name) {
/******/ 			return {
/******/ 				configurable: true,
/******/ 				enumerable: true,
/******/ 				get: function() {
/******/ 					return __webpack_require__[name];
/******/ 				},
/******/ 				set: function(value) {
/******/ 					__webpack_require__[name] = value;
/******/ 				}
/******/ 			};
/******/ 		};
/******/ 		for(var name in __webpack_require__) {
/******/ 			if(Object.prototype.hasOwnProperty.call(__webpack_require__, name) && name !== "e") {
/******/ 				Object.defineProperty(fn, name, ObjectFactory(name));
/******/ 			}
/******/ 		}
/******/ 		fn.e = function(chunkId) {
/******/ 			if(hotStatus === "ready")
/******/ 				hotSetStatus("prepare");
/******/ 			hotChunksLoading++;
/******/ 			return __webpack_require__.e(chunkId).then(finishChunkLoading, function(err) {
/******/ 				finishChunkLoading();
/******/ 				throw err;
/******/ 			});
/******/ 	
/******/ 			function finishChunkLoading() {
/******/ 				hotChunksLoading--;
/******/ 				if(hotStatus === "prepare") {
/******/ 					if(!hotWaitingFilesMap[chunkId]) {
/******/ 						hotEnsureUpdateChunk(chunkId);
/******/ 					}
/******/ 					if(hotChunksLoading === 0 && hotWaitingFiles === 0) {
/******/ 						hotUpdateDownloaded();
/******/ 					}
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 		return fn;
/******/ 	}
/******/ 	
/******/ 	function hotCreateModule(moduleId) { // eslint-disable-line no-unused-vars
/******/ 		var hot = {
/******/ 			// private stuff
/******/ 			_acceptedDependencies: {},
/******/ 			_declinedDependencies: {},
/******/ 			_selfAccepted: false,
/******/ 			_selfDeclined: false,
/******/ 			_disposeHandlers: [],
/******/ 			_main: hotCurrentChildModule !== moduleId,
/******/ 	
/******/ 			// Module API
/******/ 			active: true,
/******/ 			accept: function(dep, callback) {
/******/ 				if(typeof dep === "undefined")
/******/ 					hot._selfAccepted = true;
/******/ 				else if(typeof dep === "function")
/******/ 					hot._selfAccepted = dep;
/******/ 				else if(typeof dep === "object")
/******/ 					for(var i = 0; i < dep.length; i++)
/******/ 						hot._acceptedDependencies[dep[i]] = callback || function() {};
/******/ 				else
/******/ 					hot._acceptedDependencies[dep] = callback || function() {};
/******/ 			},
/******/ 			decline: function(dep) {
/******/ 				if(typeof dep === "undefined")
/******/ 					hot._selfDeclined = true;
/******/ 				else if(typeof dep === "object")
/******/ 					for(var i = 0; i < dep.length; i++)
/******/ 						hot._declinedDependencies[dep[i]] = true;
/******/ 				else
/******/ 					hot._declinedDependencies[dep] = true;
/******/ 			},
/******/ 			dispose: function(callback) {
/******/ 				hot._disposeHandlers.push(callback);
/******/ 			},
/******/ 			addDisposeHandler: function(callback) {
/******/ 				hot._disposeHandlers.push(callback);
/******/ 			},
/******/ 			removeDisposeHandler: function(callback) {
/******/ 				var idx = hot._disposeHandlers.indexOf(callback);
/******/ 				if(idx >= 0) hot._disposeHandlers.splice(idx, 1);
/******/ 			},
/******/ 	
/******/ 			// Management API
/******/ 			check: hotCheck,
/******/ 			apply: hotApply,
/******/ 			status: function(l) {
/******/ 				if(!l) return hotStatus;
/******/ 				hotStatusHandlers.push(l);
/******/ 			},
/******/ 			addStatusHandler: function(l) {
/******/ 				hotStatusHandlers.push(l);
/******/ 			},
/******/ 			removeStatusHandler: function(l) {
/******/ 				var idx = hotStatusHandlers.indexOf(l);
/******/ 				if(idx >= 0) hotStatusHandlers.splice(idx, 1);
/******/ 			},
/******/ 	
/******/ 			//inherit from previous dispose call
/******/ 			data: hotCurrentModuleData[moduleId]
/******/ 		};
/******/ 		hotCurrentChildModule = undefined;
/******/ 		return hot;
/******/ 	}
/******/ 	
/******/ 	var hotStatusHandlers = [];
/******/ 	var hotStatus = "idle";
/******/ 	
/******/ 	function hotSetStatus(newStatus) {
/******/ 		hotStatus = newStatus;
/******/ 		for(var i = 0; i < hotStatusHandlers.length; i++)
/******/ 			hotStatusHandlers[i].call(null, newStatus);
/******/ 	}
/******/ 	
/******/ 	// while downloading
/******/ 	var hotWaitingFiles = 0;
/******/ 	var hotChunksLoading = 0;
/******/ 	var hotWaitingFilesMap = {};
/******/ 	var hotRequestedFilesMap = {};
/******/ 	var hotAvailableFilesMap = {};
/******/ 	var hotDeferred;
/******/ 	
/******/ 	// The update info
/******/ 	var hotUpdate, hotUpdateNewHash;
/******/ 	
/******/ 	function toModuleId(id) {
/******/ 		var isNumber = (+id) + "" === id;
/******/ 		return isNumber ? +id : id;
/******/ 	}
/******/ 	
/******/ 	function hotCheck(apply) {
/******/ 		if(hotStatus !== "idle") throw new Error("check() is only allowed in idle status");
/******/ 		hotApplyOnUpdate = apply;
/******/ 		hotSetStatus("check");
/******/ 		return hotDownloadManifest(hotRequestTimeout).then(function(update) {
/******/ 			if(!update) {
/******/ 				hotSetStatus("idle");
/******/ 				return null;
/******/ 			}
/******/ 			hotRequestedFilesMap = {};
/******/ 			hotWaitingFilesMap = {};
/******/ 			hotAvailableFilesMap = update.c;
/******/ 			hotUpdateNewHash = update.h;
/******/ 	
/******/ 			hotSetStatus("prepare");
/******/ 			var promise = new Promise(function(resolve, reject) {
/******/ 				hotDeferred = {
/******/ 					resolve: resolve,
/******/ 					reject: reject
/******/ 				};
/******/ 			});
/******/ 			hotUpdate = {};
/******/ 			var chunkId = 0;
/******/ 			{ // eslint-disable-line no-lone-blocks
/******/ 				/*globals chunkId */
/******/ 				hotEnsureUpdateChunk(chunkId);
/******/ 			}
/******/ 			if(hotStatus === "prepare" && hotChunksLoading === 0 && hotWaitingFiles === 0) {
/******/ 				hotUpdateDownloaded();
/******/ 			}
/******/ 			return promise;
/******/ 		});
/******/ 	}
/******/ 	
/******/ 	function hotAddUpdateChunk(chunkId, moreModules) { // eslint-disable-line no-unused-vars
/******/ 		if(!hotAvailableFilesMap[chunkId] || !hotRequestedFilesMap[chunkId])
/******/ 			return;
/******/ 		hotRequestedFilesMap[chunkId] = false;
/******/ 		for(var moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				hotUpdate[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(--hotWaitingFiles === 0 && hotChunksLoading === 0) {
/******/ 			hotUpdateDownloaded();
/******/ 		}
/******/ 	}
/******/ 	
/******/ 	function hotEnsureUpdateChunk(chunkId) {
/******/ 		if(!hotAvailableFilesMap[chunkId]) {
/******/ 			hotWaitingFilesMap[chunkId] = true;
/******/ 		} else {
/******/ 			hotRequestedFilesMap[chunkId] = true;
/******/ 			hotWaitingFiles++;
/******/ 			hotDownloadUpdateChunk(chunkId);
/******/ 		}
/******/ 	}
/******/ 	
/******/ 	function hotUpdateDownloaded() {
/******/ 		hotSetStatus("ready");
/******/ 		var deferred = hotDeferred;
/******/ 		hotDeferred = null;
/******/ 		if(!deferred) return;
/******/ 		if(hotApplyOnUpdate) {
/******/ 			// Wrap deferred object in Promise to mark it as a well-handled Promise to
/******/ 			// avoid triggering uncaught exception warning in Chrome.
/******/ 			// See https://bugs.chromium.org/p/chromium/issues/detail?id=465666
/******/ 			Promise.resolve().then(function() {
/******/ 				return hotApply(hotApplyOnUpdate);
/******/ 			}).then(
/******/ 				function(result) {
/******/ 					deferred.resolve(result);
/******/ 				},
/******/ 				function(err) {
/******/ 					deferred.reject(err);
/******/ 				}
/******/ 			);
/******/ 		} else {
/******/ 			var outdatedModules = [];
/******/ 			for(var id in hotUpdate) {
/******/ 				if(Object.prototype.hasOwnProperty.call(hotUpdate, id)) {
/******/ 					outdatedModules.push(toModuleId(id));
/******/ 				}
/******/ 			}
/******/ 			deferred.resolve(outdatedModules);
/******/ 		}
/******/ 	}
/******/ 	
/******/ 	function hotApply(options) {
/******/ 		if(hotStatus !== "ready") throw new Error("apply() is only allowed in ready status");
/******/ 		options = options || {};
/******/ 	
/******/ 		var cb;
/******/ 		var i;
/******/ 		var j;
/******/ 		var module;
/******/ 		var moduleId;
/******/ 	
/******/ 		function getAffectedStuff(updateModuleId) {
/******/ 			var outdatedModules = [updateModuleId];
/******/ 			var outdatedDependencies = {};
/******/ 	
/******/ 			var queue = outdatedModules.slice().map(function(id) {
/******/ 				return {
/******/ 					chain: [id],
/******/ 					id: id
/******/ 				};
/******/ 			});
/******/ 			while(queue.length > 0) {
/******/ 				var queueItem = queue.pop();
/******/ 				var moduleId = queueItem.id;
/******/ 				var chain = queueItem.chain;
/******/ 				module = installedModules[moduleId];
/******/ 				if(!module || module.hot._selfAccepted)
/******/ 					continue;
/******/ 				if(module.hot._selfDeclined) {
/******/ 					return {
/******/ 						type: "self-declined",
/******/ 						chain: chain,
/******/ 						moduleId: moduleId
/******/ 					};
/******/ 				}
/******/ 				if(module.hot._main) {
/******/ 					return {
/******/ 						type: "unaccepted",
/******/ 						chain: chain,
/******/ 						moduleId: moduleId
/******/ 					};
/******/ 				}
/******/ 				for(var i = 0; i < module.parents.length; i++) {
/******/ 					var parentId = module.parents[i];
/******/ 					var parent = installedModules[parentId];
/******/ 					if(!parent) continue;
/******/ 					if(parent.hot._declinedDependencies[moduleId]) {
/******/ 						return {
/******/ 							type: "declined",
/******/ 							chain: chain.concat([parentId]),
/******/ 							moduleId: moduleId,
/******/ 							parentId: parentId
/******/ 						};
/******/ 					}
/******/ 					if(outdatedModules.indexOf(parentId) >= 0) continue;
/******/ 					if(parent.hot._acceptedDependencies[moduleId]) {
/******/ 						if(!outdatedDependencies[parentId])
/******/ 							outdatedDependencies[parentId] = [];
/******/ 						addAllToSet(outdatedDependencies[parentId], [moduleId]);
/******/ 						continue;
/******/ 					}
/******/ 					delete outdatedDependencies[parentId];
/******/ 					outdatedModules.push(parentId);
/******/ 					queue.push({
/******/ 						chain: chain.concat([parentId]),
/******/ 						id: parentId
/******/ 					});
/******/ 				}
/******/ 			}
/******/ 	
/******/ 			return {
/******/ 				type: "accepted",
/******/ 				moduleId: updateModuleId,
/******/ 				outdatedModules: outdatedModules,
/******/ 				outdatedDependencies: outdatedDependencies
/******/ 			};
/******/ 		}
/******/ 	
/******/ 		function addAllToSet(a, b) {
/******/ 			for(var i = 0; i < b.length; i++) {
/******/ 				var item = b[i];
/******/ 				if(a.indexOf(item) < 0)
/******/ 					a.push(item);
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// at begin all updates modules are outdated
/******/ 		// the "outdated" status can propagate to parents if they don't accept the children
/******/ 		var outdatedDependencies = {};
/******/ 		var outdatedModules = [];
/******/ 		var appliedUpdate = {};
/******/ 	
/******/ 		var warnUnexpectedRequire = function warnUnexpectedRequire() {
/******/ 			console.warn("[HMR] unexpected require(" + result.moduleId + ") to disposed module");
/******/ 		};
/******/ 	
/******/ 		for(var id in hotUpdate) {
/******/ 			if(Object.prototype.hasOwnProperty.call(hotUpdate, id)) {
/******/ 				moduleId = toModuleId(id);
/******/ 				var result;
/******/ 				if(hotUpdate[id]) {
/******/ 					result = getAffectedStuff(moduleId);
/******/ 				} else {
/******/ 					result = {
/******/ 						type: "disposed",
/******/ 						moduleId: id
/******/ 					};
/******/ 				}
/******/ 				var abortError = false;
/******/ 				var doApply = false;
/******/ 				var doDispose = false;
/******/ 				var chainInfo = "";
/******/ 				if(result.chain) {
/******/ 					chainInfo = "\nUpdate propagation: " + result.chain.join(" -> ");
/******/ 				}
/******/ 				switch(result.type) {
/******/ 					case "self-declined":
/******/ 						if(options.onDeclined)
/******/ 							options.onDeclined(result);
/******/ 						if(!options.ignoreDeclined)
/******/ 							abortError = new Error("Aborted because of self decline: " + result.moduleId + chainInfo);
/******/ 						break;
/******/ 					case "declined":
/******/ 						if(options.onDeclined)
/******/ 							options.onDeclined(result);
/******/ 						if(!options.ignoreDeclined)
/******/ 							abortError = new Error("Aborted because of declined dependency: " + result.moduleId + " in " + result.parentId + chainInfo);
/******/ 						break;
/******/ 					case "unaccepted":
/******/ 						if(options.onUnaccepted)
/******/ 							options.onUnaccepted(result);
/******/ 						if(!options.ignoreUnaccepted)
/******/ 							abortError = new Error("Aborted because " + moduleId + " is not accepted" + chainInfo);
/******/ 						break;
/******/ 					case "accepted":
/******/ 						if(options.onAccepted)
/******/ 							options.onAccepted(result);
/******/ 						doApply = true;
/******/ 						break;
/******/ 					case "disposed":
/******/ 						if(options.onDisposed)
/******/ 							options.onDisposed(result);
/******/ 						doDispose = true;
/******/ 						break;
/******/ 					default:
/******/ 						throw new Error("Unexception type " + result.type);
/******/ 				}
/******/ 				if(abortError) {
/******/ 					hotSetStatus("abort");
/******/ 					return Promise.reject(abortError);
/******/ 				}
/******/ 				if(doApply) {
/******/ 					appliedUpdate[moduleId] = hotUpdate[moduleId];
/******/ 					addAllToSet(outdatedModules, result.outdatedModules);
/******/ 					for(moduleId in result.outdatedDependencies) {
/******/ 						if(Object.prototype.hasOwnProperty.call(result.outdatedDependencies, moduleId)) {
/******/ 							if(!outdatedDependencies[moduleId])
/******/ 								outdatedDependencies[moduleId] = [];
/******/ 							addAllToSet(outdatedDependencies[moduleId], result.outdatedDependencies[moduleId]);
/******/ 						}
/******/ 					}
/******/ 				}
/******/ 				if(doDispose) {
/******/ 					addAllToSet(outdatedModules, [result.moduleId]);
/******/ 					appliedUpdate[moduleId] = warnUnexpectedRequire;
/******/ 				}
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// Store self accepted outdated modules to require them later by the module system
/******/ 		var outdatedSelfAcceptedModules = [];
/******/ 		for(i = 0; i < outdatedModules.length; i++) {
/******/ 			moduleId = outdatedModules[i];
/******/ 			if(installedModules[moduleId] && installedModules[moduleId].hot._selfAccepted)
/******/ 				outdatedSelfAcceptedModules.push({
/******/ 					module: moduleId,
/******/ 					errorHandler: installedModules[moduleId].hot._selfAccepted
/******/ 				});
/******/ 		}
/******/ 	
/******/ 		// Now in "dispose" phase
/******/ 		hotSetStatus("dispose");
/******/ 		Object.keys(hotAvailableFilesMap).forEach(function(chunkId) {
/******/ 			if(hotAvailableFilesMap[chunkId] === false) {
/******/ 				hotDisposeChunk(chunkId);
/******/ 			}
/******/ 		});
/******/ 	
/******/ 		var idx;
/******/ 		var queue = outdatedModules.slice();
/******/ 		while(queue.length > 0) {
/******/ 			moduleId = queue.pop();
/******/ 			module = installedModules[moduleId];
/******/ 			if(!module) continue;
/******/ 	
/******/ 			var data = {};
/******/ 	
/******/ 			// Call dispose handlers
/******/ 			var disposeHandlers = module.hot._disposeHandlers;
/******/ 			for(j = 0; j < disposeHandlers.length; j++) {
/******/ 				cb = disposeHandlers[j];
/******/ 				cb(data);
/******/ 			}
/******/ 			hotCurrentModuleData[moduleId] = data;
/******/ 	
/******/ 			// disable module (this disables requires from this module)
/******/ 			module.hot.active = false;
/******/ 	
/******/ 			// remove module from cache
/******/ 			delete installedModules[moduleId];
/******/ 	
/******/ 			// when disposing there is no need to call dispose handler
/******/ 			delete outdatedDependencies[moduleId];
/******/ 	
/******/ 			// remove "parents" references from all children
/******/ 			for(j = 0; j < module.children.length; j++) {
/******/ 				var child = installedModules[module.children[j]];
/******/ 				if(!child) continue;
/******/ 				idx = child.parents.indexOf(moduleId);
/******/ 				if(idx >= 0) {
/******/ 					child.parents.splice(idx, 1);
/******/ 				}
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// remove outdated dependency from module children
/******/ 		var dependency;
/******/ 		var moduleOutdatedDependencies;
/******/ 		for(moduleId in outdatedDependencies) {
/******/ 			if(Object.prototype.hasOwnProperty.call(outdatedDependencies, moduleId)) {
/******/ 				module = installedModules[moduleId];
/******/ 				if(module) {
/******/ 					moduleOutdatedDependencies = outdatedDependencies[moduleId];
/******/ 					for(j = 0; j < moduleOutdatedDependencies.length; j++) {
/******/ 						dependency = moduleOutdatedDependencies[j];
/******/ 						idx = module.children.indexOf(dependency);
/******/ 						if(idx >= 0) module.children.splice(idx, 1);
/******/ 					}
/******/ 				}
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// Not in "apply" phase
/******/ 		hotSetStatus("apply");
/******/ 	
/******/ 		hotCurrentHash = hotUpdateNewHash;
/******/ 	
/******/ 		// insert new code
/******/ 		for(moduleId in appliedUpdate) {
/******/ 			if(Object.prototype.hasOwnProperty.call(appliedUpdate, moduleId)) {
/******/ 				modules[moduleId] = appliedUpdate[moduleId];
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// call accept handlers
/******/ 		var error = null;
/******/ 		for(moduleId in outdatedDependencies) {
/******/ 			if(Object.prototype.hasOwnProperty.call(outdatedDependencies, moduleId)) {
/******/ 				module = installedModules[moduleId];
/******/ 				if(module) {
/******/ 					moduleOutdatedDependencies = outdatedDependencies[moduleId];
/******/ 					var callbacks = [];
/******/ 					for(i = 0; i < moduleOutdatedDependencies.length; i++) {
/******/ 						dependency = moduleOutdatedDependencies[i];
/******/ 						cb = module.hot._acceptedDependencies[dependency];
/******/ 						if(cb) {
/******/ 							if(callbacks.indexOf(cb) >= 0) continue;
/******/ 							callbacks.push(cb);
/******/ 						}
/******/ 					}
/******/ 					for(i = 0; i < callbacks.length; i++) {
/******/ 						cb = callbacks[i];
/******/ 						try {
/******/ 							cb(moduleOutdatedDependencies);
/******/ 						} catch(err) {
/******/ 							if(options.onErrored) {
/******/ 								options.onErrored({
/******/ 									type: "accept-errored",
/******/ 									moduleId: moduleId,
/******/ 									dependencyId: moduleOutdatedDependencies[i],
/******/ 									error: err
/******/ 								});
/******/ 							}
/******/ 							if(!options.ignoreErrored) {
/******/ 								if(!error)
/******/ 									error = err;
/******/ 							}
/******/ 						}
/******/ 					}
/******/ 				}
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// Load self accepted modules
/******/ 		for(i = 0; i < outdatedSelfAcceptedModules.length; i++) {
/******/ 			var item = outdatedSelfAcceptedModules[i];
/******/ 			moduleId = item.module;
/******/ 			hotCurrentParents = [moduleId];
/******/ 			try {
/******/ 				__webpack_require__(moduleId);
/******/ 			} catch(err) {
/******/ 				if(typeof item.errorHandler === "function") {
/******/ 					try {
/******/ 						item.errorHandler(err);
/******/ 					} catch(err2) {
/******/ 						if(options.onErrored) {
/******/ 							options.onErrored({
/******/ 								type: "self-accept-error-handler-errored",
/******/ 								moduleId: moduleId,
/******/ 								error: err2,
/******/ 								orginalError: err, // TODO remove in webpack 4
/******/ 								originalError: err
/******/ 							});
/******/ 						}
/******/ 						if(!options.ignoreErrored) {
/******/ 							if(!error)
/******/ 								error = err2;
/******/ 						}
/******/ 						if(!error)
/******/ 							error = err;
/******/ 					}
/******/ 				} else {
/******/ 					if(options.onErrored) {
/******/ 						options.onErrored({
/******/ 							type: "self-accept-errored",
/******/ 							moduleId: moduleId,
/******/ 							error: err
/******/ 						});
/******/ 					}
/******/ 					if(!options.ignoreErrored) {
/******/ 						if(!error)
/******/ 							error = err;
/******/ 					}
/******/ 				}
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// handle errors in accept handlers and self accepted module load
/******/ 		if(error) {
/******/ 			hotSetStatus("fail");
/******/ 			return Promise.reject(error);
/******/ 		}
/******/ 	
/******/ 		hotSetStatus("idle");
/******/ 		return new Promise(function(resolve) {
/******/ 			resolve(outdatedModules);
/******/ 		});
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {},
/******/ 			hot: hotCreateModule(moduleId),
/******/ 			parents: (hotCurrentParentsTemp = hotCurrentParents, hotCurrentParents = [], hotCurrentParentsTemp),
/******/ 			children: []
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, hotCreateRequire(moduleId));
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// __webpack_hash__
/******/ 	__webpack_require__.h = function() { return hotCurrentHash; };
/******/
/******/ 	// Load entry module and return exports
/******/ 	return hotCreateRequire("./src/js/main.js")(__webpack_require__.s = "./src/js/main.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/main.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


$(function () {
  timerMainPage();
  countboxHandler();
  showFastCart();
  initMobileMenu();

  // ALL MASK
  $('#phone-reg, #callback-phone, #callbackPhone-footer, #sadPhone-footer').inputmask({ "mask": "+7(999)999-99-99", "placeholder": "+7(___)___-__-__" });
  $('.countbox input').inputmask({ "regex": "[0-9]*" });

  //INIT SLICK (START)
  initSlickTopProduct();
  initProdDay();
  initMobCategory();
  initSlickCatalogList();
  initSlickAllCategory();
  // initSlickContentCategory();
  initSlickMainNews();
  initSlickBestProduct();
  //INIT SLICK (END)

  //INIT DOTS (START)
  // initDots();
  //INIT DOTS (END)

  modalWindow();
  siteCatalog();
  scrollHeader();
  subMenuFooter();
  categorySelect();
  activateHeaderCatalog();

  //Fake placeholder (START)
  $('#town-popup .fake-placeholder span').click(function () {
    $('.search-town-popup').focus();
  });

  $('.search-town-popup').focus(function () {
    AnimateElement({
      elem: $('#town-popup .fake-placeholder span'),
      duration: 300,
      animateProps: {
        opacity: 0
      },
      funcComplite: function funcComplite() {
        $(this).css('display', 'none');
      }
    });
  });

  $('.search-town-popup').focusout(function () {
    if ($(this).val().trim().length == 0) {
      AnimateElement({
        elem: $('#town-popup .fake-placeholder span'),
        duration: 300,
        animateProps: {
          opacity: 1
        },
        funcStart: function funcStart() {
          $(this).css('display', 'inline');
        },
        funcComplite: function funcComplite() {
          $(this).removeAttr('style');
        }
      });
    }
  });
  //Fake placeholder (END)

  if (navigator.appName == 'Microsoft Internet Explorer' || !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv:11/)) || typeof $.browser !== "undefined" && $.browser.msie == 1) {
    $('body').addClass('ie-fix');
  }
  new Catalog();

  $(document).scroll(function () {
    if ($('.catalog__sub', $('#main-category')).hasClass('open')) {
      if ($('.catalog__sub ._active', $('#main-category')).attr('style') != undefined) {
        var splitStyle = $('.catalog__sub ._active', $('#main-category')).css('marginTop').split('px');
        console.log(splitStyle[0], $(document).scrollTop());

        if (splitStyle[0] > $(document).scrollTop()) {
          $('.catalog__sub ._active', $('#main-category')).css({ marginTop: $(document).scrollTop() });
        } else if (splitStyle[0] == 0) {
          $('.catalog__sub ._active', $('#main-category')).removeAttr('style');
        }
      }
    }
  });
});

// ********** BASE FUNCTION ********** //  (START) 
var AnimateElement = function AnimateElement(elemParams) {
  var animateProps = elemParams.animateProps,
      elem = elemParams.elem,
      _elemParams$duration = elemParams.duration,
      duration = _elemParams$duration === undefined ? 150 : _elemParams$duration,
      _elemParams$queue = elemParams.queue,
      queue = _elemParams$queue === undefined ? false : _elemParams$queue,
      specialEasing = elemParams.specialEasing,
      funcStart = elemParams.funcStart,
      funcStep = elemParams.funcStep,
      funcProgress = elemParams.funcProgress,
      funcComplite = elemParams.funcComplite;


  $(elem).animate(animateProps, {
    specialEasing: specialEasing,
    queue: queue,
    duration: duration,
    start: funcStart,
    step: funcStep,
    progress: funcProgress,
    complete: funcComplite
  });
};

var modalWindow = function modalWindow() {
  $('a[href="#modal"]').click(function (e) {
    var modal__target = $(this).data('modal');
    e.preventDefault();

    $('.blackout-modal').css('display', 'block');

    // IF CLICK OUTSIDE (START)  *******  INIT 
    $(modal__target).mousedown(function (e) {
      var div = $(modal__target + ' .modal__content'); // ID or class element
      var menuButton = $(modal__target + ' .modal__content .close-popup');
      if (!div.is(e.target) // if the click was outside the element
      && div.has(e.target).length === 0 && !menuButton.is(e.target)) {
        // and not by it's child elements
        $(modal__target + ' .close-popup').click();
        $(modal__target).unbind('mousedown');
      }
    });
    // IF CLICK OUTSIDE (END) *******  INIT

    AnimateElement({
      elem: $(modal__target + ' .modal__content'),
      duration: 300,
      animateProps: {
        opacity: 1
      },
      funcStart: function funcStart() {
        $(this).addClass('in__wr');
        $(modal__target).addClass('open');
      }
    });

    if ($(this).attr('data-modal') == '#auth-popup') {
      var modal_show = $(this).data('auth');

      $(modal__target + ' .state').each(function (i, item) {
        $(this).removeClass('show end');
      });
      $(modal__target + " " + modal_show).addClass('show end');
    }

    $(modal__target + ' .close-popup').click(function () {
      AnimateElement({
        elem: $(modal__target + ' .modal__content'),
        duration: 300,
        animateProps: {
          opacity: 0
        },
        funcStart: function funcStart() {
          $(this).removeClass('in__wr');

          AnimateElement({
            queue: false,
            elem: $('.blackout-modal'),
            animateProps: {
              opacity: 0
            }
          });
        },
        funcComplite: function funcComplite() {
          $('.blackout-modal').removeAttr('style');
          $(modal__target).removeAttr('style').removeClass('open');
          $(this).removeAttr('style');
        }
      });
    });
  });

  //CHANGE INSIDE AUTH POPUP
  $('#auth-popup span.cng-pop').click(function () {
    $('#auth-popup .state').each(function (i, item) {
      $(this).removeClass('show end').removeAttr('style');
    });

    $('#auth-popup ' + $(this).data('change')).addClass('show');

    AnimateElement({
      elem: $('#auth-popup ' + $(this).data('change')),
      duration: 300,
      animateProps: {
        opacity: 1
      },
      funcComplite: function funcComplite() {
        $(this).addClass('end');
      }
    });
  });
};
// ********** BASE FUNCTION ********** //  (START) 

//SLICK (START)
// ***** TOP PRODUCT ***** (START)
var initSlickTopProduct = function initSlickTopProduct() {
	$('#main-product .top-product .product-top-list').addClass('processing');
  $('#main-product .product-top-list .list-prod').slick({
    dots: true,
    draggable: true,
    variableWidth: false,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 5000,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
	 lazyLoad: 'progressive',
    appendArrows: $('#main-product .product-top-list .slick-nav'),
    appendDots: $('#main-product .product-top-list .slick-nav'),
    prevArrow: '<button type="button" class="slick-prev slick-left"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 26 26"><path d="M16.7,22.7l9-9c0.2-0.2,0.3-0.5,0.3-0.7c0-0.3-0.1-0.5-0.3-0.7l-9-9C16.5,3.1,16.3,3,16,3s-0.5,0.1-0.7,0.3l-1.4,1.4  c-0.4,0.4-0.4,1,0,1.4l4,4c0.3,0.3,0.1,0.9-0.4,0.9H1c-0.6,0-1,0.4-1,1v2c0,0.6,0.4,1,1,1h16.6c0.4,0,0.7,0.5,0.4,0.9l-4,4  c-0.4,0.4-0.4,1,0,1.4l1.4,1.4c0.2,0.2,0.4,0.3,0.7,0.3C16.3,23,16.5,22.9,16.7,22.7z"/></svg></button>',
    nextArrow: '<button type="button" class="slick-next slick-right"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 26 26"><path d="M16.7,22.7l9-9c0.2-0.2,0.3-0.5,0.3-0.7c0-0.3-0.1-0.5-0.3-0.7l-9-9C16.5,3.1,16.3,3,16,3s-0.5,0.1-0.7,0.3l-1.4,1.4  c-0.4,0.4-0.4,1,0,1.4l4,4c0.3,0.3,0.1,0.9-0.4,0.9H1c-0.6,0-1,0.4-1,1v2c0,0.6,0.4,1,1,1h16.6c0.4,0,0.7,0.5,0.4,0.9l-4,4  c-0.4,0.4-0.4,1,0,1.4l1.4,1.4c0.2,0.2,0.4,0.3,0.7,0.3C16.3,23,16.5,22.9,16.7,22.7z"/></svg></button>',
    responsive: [{
      breakpoint: 768,
      settings: {
        variableWidth: false,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
		  adaptiveHeight: true
      }
    }]
  });
  $('#main-product .top-product .product-top-list').removeClass('processing');
};
// ***** TOP PRODUCT ***** (END)

// ***** LIST PRODUCT ***** (START)
var initProdDay = function initProdDay() {
	$('#main-product .wrapp-main .top-product .product-of-the-day').addClass('processing');
  $('#main-product .product-of-the-day .list-prod').slick({
    dots: true,
    draggable: true,
    variableWidth: false,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 5000,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    appendArrows: $('#main-product .product-of-the-day .slick-nav'),
    appendDots: $('#main-product .product-of-the-day .slick-nav'),
    prevArrow: '<button type="button" class="slick-prev slick-left"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 26 26"><path d="M16.7,22.7l9-9c0.2-0.2,0.3-0.5,0.3-0.7c0-0.3-0.1-0.5-0.3-0.7l-9-9C16.5,3.1,16.3,3,16,3s-0.5,0.1-0.7,0.3l-1.4,1.4  c-0.4,0.4-0.4,1,0,1.4l4,4c0.3,0.3,0.1,0.9-0.4,0.9H1c-0.6,0-1,0.4-1,1v2c0,0.6,0.4,1,1,1h16.6c0.4,0,0.7,0.5,0.4,0.9l-4,4  c-0.4,0.4-0.4,1,0,1.4l1.4,1.4c0.2,0.2,0.4,0.3,0.7,0.3C16.3,23,16.5,22.9,16.7,22.7z"/></svg></button>',
    nextArrow: '<button type="button" class="slick-next slick-right"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 26 26"><path d="M16.7,22.7l9-9c0.2-0.2,0.3-0.5,0.3-0.7c0-0.3-0.1-0.5-0.3-0.7l-9-9C16.5,3.1,16.3,3,16,3s-0.5,0.1-0.7,0.3l-1.4,1.4  c-0.4,0.4-0.4,1,0,1.4l4,4c0.3,0.3,0.1,0.9-0.4,0.9H1c-0.6,0-1,0.4-1,1v2c0,0.6,0.4,1,1,1h16.6c0.4,0,0.7,0.5,0.4,0.9l-4,4  c-0.4,0.4-0.4,1,0,1.4l1.4,1.4c0.2,0.2,0.4,0.3,0.7,0.3C16.3,23,16.5,22.9,16.7,22.7z"/></svg></button>',
    responsive: [{
      breakpoint: 768,
      settings: {
        variableWidth: false,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false
      }
    }]
  });
  $('#main-product .wrapp-main .top-product .product-of-the-day').removeClass('processing');
};
// ***** LIST PRODUCT ***** (END)

// ***** MOBILE CATEGORY ***** (START)
var initMobCategory = function initMobCategory() {
  $('#main-product .category-mob-list .wrapp-cat-mob').slick({
    dots: true,
    draggable: true,
    variableWidth: false,
    infinite: true,
    autoplay: false,
    autoplaySpeed: 4000,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 3,
    arrows: false
    // appendArrows: $('#main-product .product-of-the-day .slick-nav'),
    // appendDots: $('#main-product .product-of-the-day .slick-nav'),
    // prevArrow: '<button type="button" class="slick-prev slick-left"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 26 26"><path d="M16.7,22.7l9-9c0.2-0.2,0.3-0.5,0.3-0.7c0-0.3-0.1-0.5-0.3-0.7l-9-9C16.5,3.1,16.3,3,16,3s-0.5,0.1-0.7,0.3l-1.4,1.4  c-0.4,0.4-0.4,1,0,1.4l4,4c0.3,0.3,0.1,0.9-0.4,0.9H1c-0.6,0-1,0.4-1,1v2c0,0.6,0.4,1,1,1h16.6c0.4,0,0.7,0.5,0.4,0.9l-4,4  c-0.4,0.4-0.4,1,0,1.4l1.4,1.4c0.2,0.2,0.4,0.3,0.7,0.3C16.3,23,16.5,22.9,16.7,22.7z"/></svg></button>',
    // nextArrow: '<button type="button" class="slick-next slick-right"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 26 26"><path d="M16.7,22.7l9-9c0.2-0.2,0.3-0.5,0.3-0.7c0-0.3-0.1-0.5-0.3-0.7l-9-9C16.5,3.1,16.3,3,16,3s-0.5,0.1-0.7,0.3l-1.4,1.4  c-0.4,0.4-0.4,1,0,1.4l4,4c0.3,0.3,0.1,0.9-0.4,0.9H1c-0.6,0-1,0.4-1,1v2c0,0.6,0.4,1,1,1h16.6c0.4,0,0.7,0.5,0.4,0.9l-4,4  c-0.4,0.4-0.4,1,0,1.4l1.4,1.4c0.2,0.2,0.4,0.3,0.7,0.3C16.3,23,16.5,22.9,16.7,22.7z"/></svg></button>',
    // responsive: [
    //   {
    //     breakpoint: 768,
    //     settings: {
    //       variableWidth: false,
    //       dots: true,
    //       slidesToShow: 1,
    //       slidesToScroll: 1,
    //       arrows: false,
    //     }
    //   },
    // ]
  });
};
// ***** MOBILE CATEGORY ***** (END)

// ***** CATALOG LIST ***** (START)
var initSlickCatalogList = function initSlickCatalogList() {
	$('#main-product .wrapp-main .sale-product .wrapp-sales-item').addClass('processing');
  $('#main-product .list-category').slick({
    dots: true,
    draggable: true,
    variableWidth: true,
    infinite: true,
    autoplay: false,
    autoplaySpeed: 2000,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    appendArrows: $('#main-product .wrapp-sales-item .slick-nav'),
    appendDots: $('#main-product .wrapp-sales-item .slick-nav'),
    prevArrow: '<button type="button" class="slick-prev slick-left"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 26 26"><path d="M16.7,22.7l9-9c0.2-0.2,0.3-0.5,0.3-0.7c0-0.3-0.1-0.5-0.3-0.7l-9-9C16.5,3.1,16.3,3,16,3s-0.5,0.1-0.7,0.3l-1.4,1.4  c-0.4,0.4-0.4,1,0,1.4l4,4c0.3,0.3,0.1,0.9-0.4,0.9H1c-0.6,0-1,0.4-1,1v2c0,0.6,0.4,1,1,1h16.6c0.4,0,0.7,0.5,0.4,0.9l-4,4  c-0.4,0.4-0.4,1,0,1.4l1.4,1.4c0.2,0.2,0.4,0.3,0.7,0.3C16.3,23,16.5,22.9,16.7,22.7z"/></svg></button>',
    nextArrow: '<button type="button" class="slick-next slick-right"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 26 26"><path d="M16.7,22.7l9-9c0.2-0.2,0.3-0.5,0.3-0.7c0-0.3-0.1-0.5-0.3-0.7l-9-9C16.5,3.1,16.3,3,16,3s-0.5,0.1-0.7,0.3l-1.4,1.4  c-0.4,0.4-0.4,1,0,1.4l4,4c0.3,0.3,0.1,0.9-0.4,0.9H1c-0.6,0-1,0.4-1,1v2c0,0.6,0.4,1,1,1h16.6c0.4,0,0.7,0.5,0.4,0.9l-4,4  c-0.4,0.4-0.4,1,0,1.4l1.4,1.4c0.2,0.2,0.4,0.3,0.7,0.3C16.3,23,16.5,22.9,16.7,22.7z"/></svg></button>',
    responsive: [{
      breakpoint: 1000,
      settings: {
        variableWidth: false,
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: true
      }
    }]
  });
  $('#main-product .wrapp-main .sale-product .wrapp-sales-item').removeClass('processing');
};
// ***** CATALOG LIST ***** (END)

// ***** ALL CATEGORY ***** (START)
var initSlickAllCategory = function initSlickAllCategory() {
  $('#main-product .all-category-wrapp').on('init', function (event, slick, direction) {
    var select_slide = $(this).find('.slick-center').data('category');
    initSlickContentCategory(select_slide);
  });

  $('#main-product .all-category-wrapp').slick({
    dots: false,
    draggable: true,
    variableWidth: true,
    infinite: true,
    autoplay: false,
    autoplaySpeed: 2000,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    centerMode: true,
    prevArrow: '<button type="button" class="slick-prev slick-left"><i class="material-icons">keyboard_arrow_left</button>',
    nextArrow: '<button type="button" class="slick-next slick-right"><i class="material-icons">keyboard_arrow_right</i></button>',
    responsive: [{
      breakpoint: 768,
      settings: {
        variableWidth: true,
        dots: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false
      }
    }]
  });

  $('#main-product .all-category-wrapp').on('afterChange', function (event, slick, direction) {
    var select_slide = $(this).find('.slick-center').data('category');

    AnimateElement({
      elem: $('#main-product .show-category-content.slick-initialized'),
      duration: 150,
      animateProps: {
        opacity: 0
      },
      funcComplite: function funcComplite() {
        destroyContentCategoty();
        initSlickContentCategory(select_slide);
		  initLozad();
      }
    });
  });
};
// ***** ALL CATEGORY ***** (END)

// ***** CATEGORY CONTENT ***** (START)
var initSlickContentCategory = function initSlickContentCategory(initSlider) {
  var scrollPage = window.pageYOffset;

  $('#main-product .show-category-content' + initSlider).on('init', function (event, slick, direction) {
    $(this).css('display', 'block');
    $(window).scrollTop(scrollPage);

    AnimateElement({
      elem: $(this),
      duration: 150,
      animateProps: {
        opacity: 1
      },
      funcComplite: function funcComplite() {
        $(this).addClass('show').removeAttr('style');
      }
    });
  });

  $('#main-product .show-category-content' + initSlider).slick({
    dots: true,
    draggable: true,
    variableWidth: true,
    infinite: true,
    autoplay: false,
    autoplaySpeed: 2000,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 4,
    appendArrows: $('#main-product .wrapp-select-category .slick-nav'),
    appendDots: $('#main-product .wrapp-select-category .slick-nav'),
    prevArrow: '<button type="button" class="slick-prev slick-left"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 26 26"><path d="M16.7,22.7l9-9c0.2-0.2,0.3-0.5,0.3-0.7c0-0.3-0.1-0.5-0.3-0.7l-9-9C16.5,3.1,16.3,3,16,3s-0.5,0.1-0.7,0.3l-1.4,1.4  c-0.4,0.4-0.4,1,0,1.4l4,4c0.3,0.3,0.1,0.9-0.4,0.9H1c-0.6,0-1,0.4-1,1v2c0,0.6,0.4,1,1,1h16.6c0.4,0,0.7,0.5,0.4,0.9l-4,4  c-0.4,0.4-0.4,1,0,1.4l1.4,1.4c0.2,0.2,0.4,0.3,0.7,0.3C16.3,23,16.5,22.9,16.7,22.7z"/></svg></button>',
    nextArrow: '<button type="button" class="slick-next slick-right"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 26 26"><path d="M16.7,22.7l9-9c0.2-0.2,0.3-0.5,0.3-0.7c0-0.3-0.1-0.5-0.3-0.7l-9-9C16.5,3.1,16.3,3,16,3s-0.5,0.1-0.7,0.3l-1.4,1.4  c-0.4,0.4-0.4,1,0,1.4l4,4c0.3,0.3,0.1,0.9-0.4,0.9H1c-0.6,0-1,0.4-1,1v2c0,0.6,0.4,1,1,1h16.6c0.4,0,0.7,0.5,0.4,0.9l-4,4  c-0.4,0.4-0.4,1,0,1.4l1.4,1.4c0.2,0.2,0.4,0.3,0.7,0.3C16.3,23,16.5,22.9,16.7,22.7z"/></svg></button>',
    responsive: [{
      breakpoint: 768,
      settings: {
        variableWidth: true,
        dots: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false
      }
    }]
  });
};

function destroyContentCategoty() {
  $('.select-category .show-category-content.slick-initialized').slick('unslick');
  $('.select-category .show-category-content').removeClass('show').removeAttr('style');
}
// ***** CATEGORY CONTENT ***** (END)

// ***** MAIN NEWS ***** (START)
var initSlickMainNews = function initSlickMainNews() {
  $('#news .list-main-news').slick({
    dots: false,
    draggable: true,
    variableWidth: true,
    infinite: false,
    autoplay: false,
    autoplaySpeed: 2000,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 3,
    arrows: true,
    prevArrow: '<button type="button" class="slick-prev slick-left"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 26 26"><path d="M16.7,22.7l9-9c0.2-0.2,0.3-0.5,0.3-0.7c0-0.3-0.1-0.5-0.3-0.7l-9-9C16.5,3.1,16.3,3,16,3s-0.5,0.1-0.7,0.3l-1.4,1.4  c-0.4,0.4-0.4,1,0,1.4l4,4c0.3,0.3,0.1,0.9-0.4,0.9H1c-0.6,0-1,0.4-1,1v2c0,0.6,0.4,1,1,1h16.6c0.4,0,0.7,0.5,0.4,0.9l-4,4  c-0.4,0.4-0.4,1,0,1.4l1.4,1.4c0.2,0.2,0.4,0.3,0.7,0.3C16.3,23,16.5,22.9,16.7,22.7z"/></svg></button>',
    nextArrow: '<button type="button" class="slick-next slick-right"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 26 26"><path d="M16.7,22.7l9-9c0.2-0.2,0.3-0.5,0.3-0.7c0-0.3-0.1-0.5-0.3-0.7l-9-9C16.5,3.1,16.3,3,16,3s-0.5,0.1-0.7,0.3l-1.4,1.4  c-0.4,0.4-0.4,1,0,1.4l4,4c0.3,0.3,0.1,0.9-0.4,0.9H1c-0.6,0-1,0.4-1,1v2c0,0.6,0.4,1,1,1h16.6c0.4,0,0.7,0.5,0.4,0.9l-4,4  c-0.4,0.4-0.4,1,0,1.4l1.4,1.4c0.2,0.2,0.4,0.3,0.7,0.3C16.3,23,16.5,22.9,16.7,22.7z"/></svg></button>',
    responsive: [{
      breakpoint: 1000,
      settings: {
        variableWidth: false,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false
      }
    }]
  });
};
// ***** MAIN NEWS ***** (END)

// ***** Bestsellers ***** (START)
var initSlickBestProduct = function initSlickBestProduct() {
  $('#bestsellers .bestsellers-slick').slick({
    dots: false,
    draggable: true,
    variableWidth: false,
    infinite: true,
    autoplay: false,
    autoplaySpeed: 2000,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<button type="button" class="slick-prev slick-left"><i class="material-icons">keyboard_arrow_left</button>',
    nextArrow: '<button type="button" class="slick-next slick-right"><i class="material-icons">keyboard_arrow_right</i></button>'
  });
};
// ***** Bestsellers ***** (END)
//SLICK (END)

var countboxHandler = function countboxHandler() {

  $('.countbox .action').click(function () {
    var currentInput = $(this).siblings('input'),
        currentValue = parseInt($(currentInput).val()),
        minVal = parseInt($(currentInput).data('min')),
        maxVal = parseInt($(currentInput).data('max'));

    if ($(this).hasClass('minus')) {

      //Minus
      if (currentValue == minVal) {
        return;
      } else if (currentValue > minVal) {
        $(currentInput).val(currentValue - 1);
      }
    } else {

      //Plus
      if (currentValue == maxVal) {
        return;
      } else if (currentValue < maxVal) {
        $(currentInput).val(currentValue + 1);
      }
    }

    // CALC HERE
  });

  $('.countbox .input-base').focusout(function () {
    var currentInput = $(this),
        currentValue = parseInt($(currentInput).val()),
        minVal = parseInt($(currentInput).data('min')),
        maxVal = parseInt($(currentInput).data('max'));

    if (currentValue >= minVal && currentValue <= maxVal) {
      return;
    } else if (currentValue < minVal) {
      $(currentInput).val(minVal);
    } else if (currentValue > maxVal) {
      $(currentInput).val(maxVal);
    }

    // CALC HERE
  });
};

var timerMainPage = function timerMainPage() {
  $('.countdown').each(function (i, item) {
    var cnt_sec = $(this).find('.sec .num span'),
        cnt_min = $(this).find('.min .num span'),
        cnt_hrs = $(this).find('.hours .num span');

    function timer() {

      var ss = parseInt($(cnt_sec).text());
      var mm = parseInt($(cnt_min).text());
      var hh = parseInt($(cnt_hrs).text());
      var end = false;

      if (ss > 0) {
        ss--;
      } else {
        ss = 59;

        if (mm > 0) {
          mm--;
        } else {
          mm = 59;

          if (hh > 0) {
            hh--;
          } else {
            end = true;
          }
        }
      }

      if (end) {
        clearInterval(intervalCallback);
      } else {

        ss < 10 ? $(cnt_sec).text("0" + ss) : $(cnt_sec).text(ss);
        mm < 10 ? $(cnt_min).text("0" + mm) : $(cnt_min).text(mm);
        hh < 10 ? $(cnt_hrs).text("0" + hh) : $(cnt_hrs).text(hh);
      }
    }

    window.intervalCallback = setInterval(timer, 1000);
  });
};

var initDots = function initDots() {
  $(".link-prod-day", $('#main-product')).dotdotdot({
    ellipsis: '\u2026 ',
    height: 30
  });

  $(".select-category .item__name", $('#main-product')).dotdotdot({
    ellipsis: '\u2026 ',
    height: 30
  });
};

var siteCatalog = function siteCatalog() {
  $('.category .catalog__main a').hover(function () {
	try {
    var width_right_part = $('.wrapp-main', $('#main-product')).outerWidth(true),
        this_parents = $(this).parents('.category'),
        this_row = $(this),
        show_id = parseInt($(this).data('id')),

	 //Main prod (START)
    scroll_height = $(document).scrollTop(),
    //Main prod (END)
    show_id = parseInt($(this).data('id'));

	 if (0 < $('#main-category').length) {
		var main_cat_sub = $('.catalog__sub', $('#main-category ')).offset().top
	 }

    if ($(this).hasClass('_active')) {
      return;
    }

    window.mytimeout = setTimeout(function () {
      // if element has class '_active', delete it.
      $('.category .catalog__main a._active').removeClass('_active');
		$('.category .catalog__sub div._active').removeClass('_active').removeAttr('style');

      // add '_active' on hover element
      $(this_row).addClass('_active');

      $('.category .catalog__sub').find('div[data-id="' + show_id + '"]').addClass('_active');

      if ($(this_row).parents('#main-product').length != 0) {
        $('.category .catalog__sub', $('#main-product')).addClass('open').css('width', width_right_part);
      } else {
        $('.catalog__sub', $('#header-catalog')).addClass('open');
		  $('#header-catalog').css('width', '100%');
      }

		//Main prod (START)
		if ($(this_parents).is('#main-category')) {
			$('.category .catalog__sub').find('div[data-id="' + show_id + '"]').css({
				marginTop: checkNeededScroll() ? scroll_height : '',
				position: checkNeededScroll() ? 'relative' : '',
				top: checkNeededScroll() ? 0 : ''
			});
		}

		function checkNeededScroll() {
			return $('div[data-id="' + show_id + '"]', $('#main-category')).height() > $('.catalog__main', $('#main-category')).height();
		}
		//Main prod (END)
    }, 200);

	 } catch (e) {
		console.log('error', e);
  }
  }, function () {
    clearTimeout(window.mytimeout);
  });

  // Close menu
  $('.close-menu').click(function () {
    $('#main-category').mouseleave();
	 $('.wrapp-l-t-m').mouseleave();
  });

  $('#main-category').mouseleave(function () {
    $('.category .catalog__main a._active', $('#main-product')).removeClass('_active');
    $('.category .catalog__sub', $('#main-product')).removeClass('open').removeAttr('style');
    $('.category .catalog__sub div._active', $('#main-product')).removeClass('_active');
  });

  $('.wrapp-l-t-m').mouseleave(function (e) {
    $('.catalog__btn').removeClass('_active');
	 $('div.l-t', 'header').removeClass('open');
	 $('.blackout-catalog').remove();
	 $('.pc-wrapper-left .pc-slider').removeClass('shaded');
	 $('#products-list .product-item .stickers').removeClass('shaded');
	 $('#header-catalog').removeAttr('style');
    $('.catalog__main a._active', $('#header-catalog')).removeClass('_active');
    $('.catalog__sub', $('#header-catalog')).removeClass('open').removeAttr('style');
    $('.catalog__sub div._active', $('#header-catalog')).removeClass('_active');
  });
};

var scrollHeader = function scrollHeader() {

  $(document).scroll(function (e) {
	var header = $('header');
	if (!header.hasClass('no-scroll')) {
    if ($(document).scrollTop() > 0) {
      header.addClass('scroll');
    } else {
      header.removeClass('scroll');
    }
	}
  });
};

var activateHeaderCatalog = function activateHeaderCatalog() {
  $('.catalog__btn, .l-t').hover(function () {
	var _ = $(this);
	if (_.hasClass('l-t') && _.hasClass('mainpage')) {
		return;
	}

    if ($('.catalog__btn').hasClass('_active') || $('.catalog__btn').hasClass('click-for-close')) {
      $('.catalog__btn', 'header').each(function () {
        $(this).removeClass('click-for-close');
      });
      return;
    }

    $('.catalog__btn').addClass('_active');

    $('body').append('<div class="blackout-catalog"></div>');
    $('div.l-t', 'header').addClass('open');
	 $('.pc-wrapper-left .pc-slider').addClass('shaded');
	 $('#products-list .product-item .stickers').addClass('shaded');
  });

  $('.catalog__btn, .l-t').click(function () {
    if ($('.catalog__btn', $('header')).hasClass("_active")) {
      $('.catalog__btn', 'header').each(function () {
        $(this).addClass('click-for-close');
      });
      $('.wrapp-l-t-m').mouseleave();
    } else if ($(this).hasClass('click-for-close')) {
      $('.catalog__btn', 'header').each(function () {
        $(this).removeClass('click-for-close');
      });
      $('.catalog__btn').addClass('_active');

      $('body').append('<div class="blackout-catalog"></div>');
      $('div.l-t', 'header').addClass('open');
		$('.pc-wrapper-left .pc-slider').addClass('shaded');
		$('#products-list .product-item .stickers').addClass('shaded');
    }
  });
};

var showFastCart = function showFastCart() {
  var modalWindow = $('header .modal-add-position');

  $('header2 .add-to-cart:not(.empty)').hover(function () {

    if ($(this).hasClass('empty') || $(modalWindow).hasClass('show')) {
      return;
    }

    $(modalWindow).addClass('show');

    AnimateElement({
      elem: $(modalWindow),
      duration: 300,
      animateProps: {
        opacity: 1
      },
      funcComplite: function funcComplite() {
        $(this).removeAttr('style');
      }
    });
  });

  $('header .cart-camp').mouseleave(function () {
    AnimateElement({
      elem: $(modalWindow),
      duration: 150,
      animateProps: {
        opacity: 0
      },
      funcComplite: function funcComplite() {
        $(modalWindow).removeClass('show').removeAttr('style');
      }
    });
  });

  $('header .modal-add-position a[data-action="close"]').click(function () {
    $(modalWindow).css('display', 'none');
  });
};

var subMenuFooter = function subMenuFooter() {
  $('.footer-top .subm').click(function () {

    if (!$(this).hasClass('open')) {
      $(this).addClass('open');
    } else {
      $(this).removeClass('open');
    }
  });
};

var initMobileMenu = function initMobileMenu() {
  $('header .btn__mob-menu').click(function () {
    var height_menu = $('.mobile-site-menu', $('header')).height(),
        this_btn = $(this);

    if ($(this).hasClass('animate')) {
      return false;
    }

    function toggleMenu(height, btn, state) {
      if (state) {
        $('.blackout').css({ 'display': 'block' });
      }

      AnimateElement({
        elem: $('.mobile-site-menu', $('header')),
        animateProps: {
          height: state == true ? height : 0
        },
        duration: 300,
        funcStart: function funcStart() {
          $(btn).addClass('animate');

          if (state == true) {
            $('.blackout').css({ 'opacity': 0.5 });
          } else {
            $('.blackout').css({ 'opacity': 0 });
            $(btn).addClass('close');
          }
        },
        funcComplite: function funcComplite() {
          $(btn).removeClass('animate');

          if (state == true) {
            $(btn).addClass('open');
            $(this).addClass('showed');
            $(this).removeAttr('style');
          } else {
            $(btn).removeClass('open close');
            $('.blackout').removeAttr('style');
            $(this).removeClass('showed');
            $(this).removeAttr('style');
          }
        }
      });
    }

    if (!$(this).hasClass('open')) {
      $('.mobile-site-menu', $('header')).css({ 'display': 'block', 'height': '0px' });
      toggleMenu(height_menu, $(this_btn), true);
    } else {
      toggleMenu(height_menu, $(this_btn), false);
    }
  });

  $('header .mobile-site-menu .w-d-d').click(function (e) {
    e.preventDefault();
    var height_submenu = $(this).siblings('.submenu').height(),
        this_btn = $(this).parent('li');

    if ($(this).parent('li').hasClass('animate')) {
      return false;
    }

    function toggleSubMenu(height, btn, elem, state) {
      AnimateElement({
        elem: $(elem),
        animateProps: {
          height: state == true ? height : 0
        },
        duration: 300,
        funcStart: function funcStart() {
          $(btn).addClass('animate');
        },
        funcComplite: function funcComplite() {
          $(btn).removeClass('animate');

          if (state == true) {
            $(btn).addClass('open');
            $(elem).removeAttr('style');
          } else {
            $(btn).removeClass('open');
            $(elem).removeAttr('style');
          }
        }
      });
    }

    if (!$(this).parent('li').hasClass('open')) {
      $(this).siblings('.submenu').css({ 'display': 'block', 'height': '0px' });
      toggleSubMenu(height_submenu, $(this_btn), $(this).siblings('.submenu'), true);
    } else {
      toggleSubMenu(height_submenu, $(this_btn), $(this).siblings('.submenu'), false);
    }
  });
};

var categorySelect = function categorySelect() {

  $('.item', $('.all-category-wrapp')).click(function (e) {
    e.preventDefault();
    var slideno = $(this).data('slick-index');
    $('.all-category-wrapp').slick('slickGoTo', slideno);
  });
};

var Catalog = function Catalog(e) {
  this.init();
};

Catalog.prototype = {

  init: function init() {
    var self = this;

    self.$mainEl = $('#header-catalog.category');
    self.$mainBtn = self.$mainEl.find('.catalog__btn');
    self.$mainMain = self.$mainEl.find('.catalog__main ul');
    self.$mainSub = self.$mainEl.find('.catalog__sub div[data-id]');

    self.$mobileEl = $('.catalog._mobile');
    self.$mobileBtn = $('.mobile-site-menu .w-cat');
    // self.$mobileBtn    = self.$mobileEl.find('.catalog__btn');
    self.$mobileBack = self.$mobileEl.find('.catalog__back');
    self.$mobilePages = self.$mobileEl.find('.catalog__pages');

    // self.mainInit();
    self.mobileInit();
  },

  mainInit: function mainInit() {
    var self = this;

    self.timerMain = null;
    self.timerSub = null;

    // открыть

    self.$mainEl.on('mouseenter', function () {
      if (self.timerMain) {
        clearTimeout(self.timerMain);
      }
      self.timerMain = setTimeout(function () {
        self.$mainEl.addClass('_active');
      }, 100);
    });

    // закрыть

    self.$mainEl.on('mouseleave', function () {
      if (self.timerMain) {
        clearTimeout(self.timerMain);
      }
      self.timerMain = setTimeout(function () {
        self.$mainEl.removeClass('_active');
      }, 100);
    });

    // выбрать раздел

    self.$mainMain.on('mouseenter', 'a', function () {
      if ($(this).hasClass('_active')) {
        return;
      }
      var id = $(this).hasClass('_childs') ? $(this).attr('data-id') : 0;
      if (id) {
        self.timerSub = setTimeout(function () {
          self.mainSelect(id);
        }, 300);
      }
    });

    self.$mainMain.on('mouseleave', 'a', function () {
      if (self.timerSub) {
        clearTimeout(self.timerSub);
      }
    });

    // делаем сразу активным первый раздел

    self.mainSelect(self.$mainMain.find('a').attr('data-id'));

    // при скроле

    $(window).on('scroll', function () {
      self.mainSubReposition();
    });
  },

  /**
   * выбрать подкатегорию по id (первого уровня)
   */
  mainSelect: function mainSelect(id) {
    this.$mainMain.find('a').removeClass('_active');
    this.$mainMain.find('a[data-id="' + id + '"]').addClass('_active');
    this.$mainSub.removeClass('_active');
    this.$mainSub.filter('[data-id="' + id + '"]').addClass('_active');
    this.mainSubReposition();
  },

  /**
   * смещает подразделы при скроле
   */
  mainSubReposition: function mainSubReposition() {

    if (!this.$mainEl.is('._active')) {
      return;
    }

    var self = this,
        top = 0,
        offset = window.pageYOffset || document.documentElement.scrollTop,
        block = self.$mainSub.filter('._active').get(0),
        blockHeight = block.clientHeight,
        parent = block.parentNode,
        parentHeight = parent.clientHeight,
        parentTop = parent.getBoundingClientRect().top - document.body.getBoundingClientRect().top,
        parentBottom = parentTop + parentHeight;

    if (offset >= parentTop && offset <= parentBottom) {
      var s = getComputedStyle(parent);
      top = Math.min(offset - parentTop, parentHeight - parseFloat(s.paddingTop) - parseFloat(s.paddingBottom) - blockHeight);
    }

    block.style.top = top + 'px';
  },

  mobileInit: function mobileInit() {
    var self = this;

    self.history = [];

    // открыть

    self.$mobileBtn.on('click', function () {
		if (true) {
			self.mobileSelect();
			self.$mobileEl.addClass('_active');
			$('html, body').addClass('_lock');
		} else {
			//self.mobileSelect();
			//self.$mobileEl.addClass('_active');
			//$('html, body').addClass('_lock');
			$('.btn__mob-menu').click();
			//$('.catalog-menu .close').click();
			$('.catalog._mobile .catalog__btn').click();
		}
      return false;
    });

    // закрыть/назад

    self.$mobileBack.on('click', function () {
      if (0 === self.history.length) {
        self.$mobileEl.removeClass('_active');
        $('html, body').removeClass('_lock');
      } else {
        self.history.pop();
        self.mobileSelect(self.history[self.history.length - 1], 'prev');
      }
      return false;
    });

    // выбрать

    self.$mobilePages.on('click', 'a', function () {
      var id = $(this).hasClass('_childs') ? $(this).attr('data-id') : 0;
      if (id) {
        self.history.push(id);
        self.mobileSelect(id, 'next');
        return false;
      }
    });
  },

  /**
   * выбор пункта меню на мобильном (любого уровня)
   * клонируем по id из десктопного меню,
   * если id не передан - клонируем главное меню
   */
  mobileSelect: function mobileSelect(id, anim) {
    var self = this,
        $content = id ? self.$mainSub.filter('[data-id="' + id + '"]').children().clone() : self.$mainMain.clone().addClass('_img');

    $content = $content.wrapAll('<div class="catalog__page"></div>').parent();

    if ('prev' === anim) {
      self.$mobilePages.children().not('._current').remove();
      self.$mobilePages.append($content.addClass('_prev'));
      setTimeout(function () {
        self.$mobilePages.find('._current').removeClass('_current').addClass('_next');
        self.$mobilePages.find('._prev').removeClass('_prev').addClass('_current');
      }, 0);
    } else if ('next' === anim) {
      self.$mobilePages.children().not('._current').remove();
      self.$mobilePages.append($content.addClass('_next'));
      setTimeout(function () {
			//alert(self.$mobilePages.find('._current').length);
        self.$mobilePages.find('._current').removeClass('_current').addClass('_prev');
		  //alert(self.$mobilePages.find('._next').length);
        self.$mobilePages.find('._next').removeClass('_next').addClass('_current');
      }, 0);
    } else {
      self.$mobilePages.empty().append($content.addClass('_current'));
    }
  }
};

/***/ })

/******/ });


$(document).ready(function() {
	//$('#social-big .social').click(function() {
		//$(this).find('a').click();
		//return false;
	//});

	$('.add-to-cart.cart-box').click(function() {
		var _ = $(this);
		var count = parseInt(_.find('.global-count-cart').html());
		if (0 < count) {
			redirect(_.find('#cartlink').attr('href'));
		}
		return false;
	});
});