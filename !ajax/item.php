<?php

    header("Content-Type: application/json; charset=utf-8", true);
    session_start();
    if (!isset($_SESSION["i_states"])) $_SESSION["i_states"] = array("add-to-favorites"=>0, "add-to-compare"=>0);
    $result = array();
    
    switch ($_REQUEST["a"]) {
    
        case "add-to-favorites":
        case "add-to-compare":
        
                /*
                    $_REQUEST["id"] => item id
                    
                    $result["state"] = ok | error
                    $result["selected"] => 0/1
                    $result["button_text"] => обновление текста для кнопки действия

                */        
                
                $result["state"] = "ok";
                $_SESSION["i_states"][$_REQUEST["a"]] = 1 & ( 1 ^ $_SESSION["i_states"][$_REQUEST["a"]]);
                $result["selected"] = $_SESSION["i_states"][$_REQUEST["a"]];
                if ($_REQUEST["a"]=="add-to-compare") {
                    $result["button_text"] = $result["selected"] ? "В сравнении" : "Добавить в сравнение";
                } else {
                    $result["button_text"] = $result["selected"] ? "В избранном" : "В избранное";
                }

            break;
            
        case "share":

                /*
                    $_REQUEST["id"] => item id
                    
                    $result["state"] = ok | error
                    $result["url"] => share link
                    $result["title"] => share title
                    $result["description"] => share text

                */                    
                
                $result["state"] = "ok";
                $result["url"] = $_SERVER["HTTP_REFERER"];
                $result["title"] = "Видеокарта GigaByte GeForce GTX 1050";
                $result["description"] = "GigaByte GeForce GTX 1050 всего за 10000 руб на сайте www.123.ru";
        
        break;
    }
    
    
    echo json_encode($result);