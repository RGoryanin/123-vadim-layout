var _selected = -1;
$(function() {

    $(".view-type-switcher li a").on("click", function(e) {
        e.preventDefault();
        if ($(this).parent().hasClass("active")) return false;
        var p = $(this).data("page");
        $(this).parents("ul").find(".active").removeClass("active");
        $(this).parent().addClass("active");
        $(".-page.visible").fadeOut(150, function() {
            $(this).removeClass("visible");
            $(".-page#"+p).addClass("visible").fadeIn(150);
            if (p=="courier-view") {
                var ll = $("#courier-view .table-contents .line:last-of-type");
                top.setIframeHeight(ll.offset().top + ll.outerHeight())
            } else {
                top.setIframeHeight(0);
            }
            var w = $("#courier-view .table-contents")[0].scrollHeight, ov = w>$("#courier-view .table-contents").outerHeight();
            if (ov) {
                var pd = $("#courier-view .table-contents").outerWidth() - $("#courier-view .table-contents .line:first-of-type").width();
                $("#courier-view .table-header").css({ paddingRight: pd+"px" });
            } else {
                $("#courier-view .table-header").css({ paddingRight: "" });
            }
        });
        return false;
    });



	var datakeys = {
		own: '/img/map-point.png',
		zakazberry: '/i/carriers/pointer-zakazberry.png',
		sdek: '/img/map-point-cdek.png',
		pickpoint: '/img/map-point-pickpoint.png',
		iml: '/img/map-point-iml.png',
		euroset: '/img/map-point-euroset.png',
		svyaznoy: '/img/map-point-svyaznoy.png',
		boxberry: '/img/map-point-boxberry.png',
		kh: '/i/nh-pointmap.png'
	};
	var datanames = {
		own: 'ПВЗ 123.ru',
		zakazberry: 'ZakazBerry',
		sdek: 'СДЭК',
		pickpoint: 'PickPoint',
		iml: 'IML',
		euroset: 'Евросеть',
		svyaznoy: 'Связной',
		boxberry: 'Boxberry',
		kh: 'Ноу-Хау'
	};
	var specialTitles = {
	   deliv: "Доставка",
	   visa: "VISA",
	   mastercard: "MasterCard",
	   mir: "МИР",
	   gabarit: "Габаритный товар"
	};
	var maxPerPage = 15;
	
    function isImlCity() {
    	return (1 == $('#body').attr('data-is-iml-city'));
    }
    function isSdekCity() {
    	return (1 == $('#body').attr('data-is-sdek-city'));
    }

     _ready = false;
        _points = {},
        pickupTypes = null,
        placemarks = {},
        clickHandler = top.clickHandler;
    
    $("header .map-type ul li input").on("click", function(ev) {
        var sel = $($(this).data("block"));
        if (sel.hasClass("active")) return false;
        $("main>section.active").removeClass("active");
        sel.addClass("active");
        if (sel.attr("id")=="list-view") {
            var ll = $("#list-view .list-items");
            top.setIframeHeight(ll.position().top + ll.outerHeight() + 2);
        } else {
            top.setIframeHeight(0);
        }
    });

    var mapinit = function() {
    			orderMap = new ymaps.Map('map-view', {
    				center: $("#map-view").data('center'),
    				zoom: $("#map-view").data('zoom'),
    			}, {
                    minZoom: 9
                });
			
			window.CartModalFree2 = ymaps.templateLayoutFactory.createClass(
				[
					'<div class="map-balloon">',
                        '<a href="#" class="baloon-close" data-action="close"><svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 12 12"><svg xmlns:xlink="http://www.w3.org/1999/xlink" height="12" width="12" version="1.1" viewBox="0 0 12 12" id="close" y="0"><title>Close</title><desc/><g id="close-category_adaptive" fill-rule="evenodd" fill="none"><g id="close-05_Category-ot-320" fill="#9B9B9B" transform="translate(-92 -376)"><g id="" transform="translate(0 360)"><g id="" transform="translate(92 13)"><path id="close-Fill-20" d="m5.9998 10.692l-4.0537 4.054c-0.3876 0.388-1.0122 0.391-1.4097-0.006l-0.27654-0.277c-0.39099-0.391-0.39594-1.02-0.00627-1.409l4.0537-4.0542-4.0537-4.0537c-0.38754-0.3876-0.39127-1.0122 0.00626-1.4097l0.27656-0.2765c0.39099-0.391 1.02-0.396 1.4097-0.0063l4.0537 4.0537 4.0542-4.0537c0.387-0.3875 1.012-0.3913 1.409 0.0063l0.277 0.2765c0.391 0.391 0.396 1.02 0.006 1.4097l-4.0533 4.0537 4.0533 4.0542c0.388 0.387 0.392 1.012-0.006 1.409l-0.277 0.277c-0.391 0.391-1.02 0.396-1.409 0.006l-4.0542-4.054z"/></g></g></g></g></svg></svg></a>',
						'<i class="icon-paymethods {{ properties.icon_class }}"></i> <span class="del_name">{{ properties.del_name }}</span>',
						'<div class="metro line-{{ properties.metro_line }}">{{ properties.metro_name }}</div>',
                        '<div class="b-addr">{{ properties.full_info|raw }}</div>',
                        '<div class="special">{{ properties.specials_html|raw }}</div>',
						'{% if properties.showModalButton %}',
							'<a href="#" class="btn btn-primary __select_point" data-uid="{{ properties.uid }}">в корзину</a>',
						'{% endif %}',
					'</div>'
				].join(''));
            checkReady();
        }
    ymaps.ready(mapinit);
    if ($("#map-type-3").length!=0) {
        subwayMap.init($("#metro-view"));
    } else {
        for (var i in subwayMap) {
            subwayMap[i] = function() {}
        }
        $("#list-view").addClass("no-metro");
        subwayMap.ready = false;
    }
    
    var checkReady = function() {
        if (typeof(top.pickupTypes)=="undefined") return setTimeout(checkReady, 50);
        _ready = true;
        pickupTypes = top.pickupTypes;
        parsePickup();
    }
   
    var parsePickup = function() {
        _selected = top.$(top.document.body).find(".-tab-pickup").data("uid");
        
        $("#list-view .list-items ul").html("");
		orderMap.geoObjects.removeAll();
		subwayMap.clear();
		
        _points={}, _0_=[];
        for (var z in top.pickupTypes) {
            for (var i in top.pickupTypes[z].points) {
                var point = top.pickupTypes[z].points[i];
                _0_.push(point);
                if (checkFilters(point, top.pickupTypes[z])) {
                    if (typeof(point.rootID)=="undefined") point.rootID = z;
                    if (subwayMap.ready) point.metroName = subwayMap.stations[point.metroID][0];
                    renderPoint(point, top.pickupTypes[z]);
                }
            }
        }
        console.log(_0_);

        var pd = 0;
        $(".list-items").scrollTop(0);
        if ($(".list-items")[0].clientHeight - $(".list-items")[0].scrollHeight < 0) pd = top.window.innerWidth - $(top.document.body).width();
        if ($(".list-items li.hidden").length) {
            $(".list-items .show-more").remove();
            var N = $(".list-items li.hidden").length;
            if (N>maxPerPage) N = maxPerPage;
            $(".list-items").append("<div class='show-more'><a href='#'>Показать еще <em>"+N+"</em></a></div>");
            $(".list-items").find(".show-more a").on("click", function(e) {
                e.preventDefault();
                var hd = $(".list-items li.hidden");
                for (var i=0; i<maxPerPage; i++) if (hd.eq(i).length) hd.eq(i).removeClass("hidden");
                $('.list-items').animate({scrollTop: $('.list-items').scrollTop() + $('.list-items').height() - 100 }, 100);
                var N = $(".list-items li.hidden").length;
                if (N>maxPerPage) N = maxPerPage;
                $('.list-items').find(".show-more a em").text(N);
                if (N==0) $('.list-items').find(".show-more").remove();
                return false;
            }); 
        }
        $(".list-header").css({ paddingRight: pd + "px" });
        subwayMap.render();
    }
    var checkFilters = function(point, root) {
        var fl = false;
        if (point.uid == _selected) return true;
        if (!$("#-filters input:checked").length) fl = true;
        else {
            if ($("#-filters input[value="+root.uid+"]:checked").length) fl = true;
            if (!$("#-filters input:checked:not(.-srv)").length) fl = true;
        }
        if ($("#-filters input[value=free]:checked").length) {
            if (point.price!=0) fl = false;
        }
        if ($("#-filters input[value=bycard]:checked").length) {
            if (!(typeof(point.acceptCards)!='undefined' && point.acceptCards)) fl = false;
        }
        if ($("#-filters input[value=today]:checked").length && fl) {
            var d = new Date(point.pickupDatetime.date+" GMT"+(point.pickupDatetime.timezone_type>0?"+":"")+point.pickupDatetime.timezone_type),
                dt = new Date(Date.now() + 86400000);
            d.setHours(0), d.setMinutes(0), d.setSeconds(0), dt.setHours(0), dt.setMinutes(0), dt.setSeconds(0);
            fl = Math.abs(d-dt) < 86400000*2;
        }
        if (!fl) return false;
        var txt = $(".search-shop input[type=text]").val().trim().toLowerCase().replace(/[^А-Яа-я0-9A-Za-z]/g, "|"),
            ss  = typeof(point.addressParts[0])!='undefined' ? point.addressParts.join(" ") : point.address;
            ss = ss.toLowerCase().trim().replace(/[^А-Яа-я0-9A-Za-z]/g, ' ');
            if (txt=="") return true;
        var re = new RegExp('('+txt+')');
        if ((ss.trim()!="" && ss.match(re)) || point.metroName.match(re)) { console.log(ss, point.metroName); return true;}
        return false;
    }
    
    var renderPoint = function(point, root) {
        renderPointList(point, root);
        renderPointMap(point, root);
        if (subwayMap.ready) renderPointMetro(point, root);
        
        _points[point.uid] = point;
    }
    
    var renderPointList = function(point, root) {
        if (subwayMap.ready) {
            var 
                metroline = subwayMap.stations[point.metroID][1],
                station = subwayMap.stations[point.metroID][0];
        } else {
            metroline = 0;
            station = "";
        }
        var li = $("<li class='"+(_selected==point.uid?"selected ":"")+" "+root.uid+"'></li>");
            li.append('<span class="title" title="'+root.title+'">'+datanames[root.uid]+'</span>');
            li.append('<span class="address">'+(typeof(point.addressParts[0])!='undefined' ? point.addressParts[0] : point.address)+'</span>');
            li.append('<span class="metro line-'+metroline+'">'+station+'</span>');
            var $price = $('<span class="price">'+(point.price==0?"Бесплатно":"<b>"+point.price+" <span class='rub'>a</span></b>")+'</span>'),
                $work  = $('<span class="open">'+point.workhours+'<br>'+point.pickupTitle+'</span>');
            if (top.window.innerWidth<760) {
                li.append($work); 
                li.append($price);
            } else {
                li.append($price);
                li.append($work); 
            }
            li.append('<span class="set"><a href="#" data-uid="'+point.uid+'" class="set-shop blue-button">в корзину</a></span>');
        $("#list-view .list-items ul").append(li);
        if ($("#list-view .list-items ul li").length>maxPerPage) li.addClass('hidden');
    }
    
    var renderPointMap = function(point, root) {
        var typeUid = root.uid,
            map = orderMap,
            pickupType = root;
		if (('euroset' == typeUid) && point.isNumericCode) {
			typeUid = 'svyaznoy';
		}
		try {
			var paymentTypes = [];
			if (point.paymentTypes && (0 < point.paymentTypes.length)) {
				paymentTypes = point.paymentTypes;
			} else {
				paymentTypes = pickupType.paymentTypes;
			}
			var iconClass = typeUid;
			if (isImlCity() || isSdekCity()) {
				iconClass = 'own';
			}
			var _full = (typeof(point.addressParts[0])!='undefined' ? point.addressParts[0] : point.address)+"<br><br><b>Время работы:</b><br>"+point.workhours,
			    _specials = "", _sp = "";
			if (point.price>0) {
			    _full+= "<br><br><b>Стоимость:</b> "+point.price+" <span class='rub'>a</span>";
			}
            for (var i in point.specials) {
               if (point.specials[i]) {
                _sp+="<li class='tooltip-top' data-tooltip='"+specialTitles[i]+"'><img src='/img/icon_delivery/special/icon_"+i+".svg'></li>";
               }
            }
            if (_sp!="") _specials = "<br><b>Особенности</b><ul>"+_sp+"</ul>";
            if (subwayMap.ready) {
                var ml = subwayMap.stations[point.metroID][1],
                    mn = subwayMap.stations[point.metroID][0];
            } else {
                ml = 0;
                mn = "";
            }
			var placemark = new ymaps.Placemark([point.lat, point.lon], {
				pickupType: point.agent,
				showModalButton : true,
				modalText: point.address.replace(/\<br \/\>/, "\n"),
				pid : point.id,
				uid: point.uid,
				paymentTypes: paymentTypes.join(','),
				price: point.price,
				icon: datakeys[typeUid],
				del_name: datanames[typeUid],
				icon_class: iconClass,
				metro_line: ml,
				metro_name: mn,
				full_info: _full,
				specials_html: _specials,
				b_addr: point.address,
				b_addr_parts: point.addressParts,
				b_contact_link: point.contactLink,
				b_shed: point.workhours,
				b_del: point.pickupTitle,
				b_price: (0 == point.price ? 'бесплатно' : point.price + ' руб.')
			}, {
				iconLayout : 'default#image',
				hasBalloon : true,
				hasHint : false,
				hideIconOnBalloonOpen : false,
				iconImageHref: datakeys[typeUid],
				iconImageOffset: [0, 0],
				iconImageSize : [46, 54],
				iconOffset : [-23, -50],
				balloonLayout: window.CartModalFree2,
				balloonPanelMaxMapArea: 0,
				balloonAutoPan: false,
			});
			placemarks[point.uid] = placemark;
			placemark.events.add(clickHandler, function(){
				if($(window).width() < 400){
					console.log('mobile-version');
				}
				var coord = placemark.geometry.getCoordinates();
				map.setCenter([
					coord[0],
					coord[1]
				]);
				var position = map.getGlobalPixelCenter();
				map.setGlobalPixelCenter([
					position[0],
					position[1] - 70
				]);
			});
			map.geoObjects.add(placemark);
			/*placemark.events.add(clickHandler, function () {
				var coord = placemark.geometry.getCoordinates();
				window.cartMap.panTo([coord[0]+0.02,coord[1]], {
					delay: 0
				}); 
			});*/
        } catch (e) { console.log('error', e); }
    }
    
    var renderPointMetro = function(point, root) {
        subwayMap.addObject({
            metroID: point.metroID,
            type: root.uid,
            uid: point.uid
        });
//        console.log("METRO", point, root);
    }
    
    $(document.body).on("click", ".set-shop", function(e) {
        e.preventDefault();
        selectPoint($(this).data("uid"));
        $(this).parents("ul").find(".selected").removeClass("selected");
        $(this).parents("li").addClass('selected');
        return false;
    }).on('click', function(ev) {
        if ($(ev.target).parents("#-filters").length==0) $("#-filters").hide();
    }).on("click", '.baloon-close', function(e){
		e.preventDefault();
		orderMap.balloon.close();
	}).on("click", ".__select_point", function(e) {
		e.preventDefault();
		selectPoint($(this).data("uid"));
		return false;
	}).on("click", "#background .m-shop", function(e) {
	   e.preventDefault();
	   showDescription($(this).data("uid"));
	   return false;
	});
    
    $(".filter-switcher").on("click", function(e) {
        e.preventDefault();
        $("#-filters").toggle($(this).toggleClass("expanded").hasClass("expanded"));
        return false;
    });
    $("#-filters input[type=checkbox]").on("click", function(e) {
        setTimeout(parsePickup(), 5);
    });
    $("a.-search").on("click", function(e) {
        e.preventDefault();
        setTimeout(parsePickup(), 5);
        return false;
    });
    
    var showDescription = function(uid) {
        var point = _points[uid], st = subwayMap.stations[point.metroID], ml = st[1];
        $("#background .map-balloon").remove();
        var bl = $("<div class='map-balloon'><a href='#' class='baloon-close'></a><i class='icon-paymethods "+point.rootID+"'></i>\n<span class='del_name'>"+datanames[point.rootID]+"</span><div class='metro line-"+ml+"'>м. "+st[0]+"</div><div class='b-addr'>"+(typeof(point.addressParts[0])!='undefined' ? point.addressParts[0] : point.address)+"<br><br><b>Время работы:</b><br>"+point.workhours+(point.price>0?"<br><br><b>Стоимость:</b> "+point.price+" <span class='rub'>a</span>":"")+"</div><div class='special'><br><b>Особенности</b></div><a href='#' class='get-here blue-button' data-uid='"+uid+"'>в корзину</a></div>");
        var ul = $("<ul></ul>");
        for (var i in point.specials) {
           if (point.specials[i]) {
            ul.append("<li class='tooltip-top' data-tooltip='"+specialTitles[i]+"'><img src='/img/icon_delivery/special/icon_"+i+".svg'></li>");
           } 
        }
        if (ul.find("li").length) bl.find(".special").append(ul);
        else bl.find(".special").remove();
        bl.appendTo("#background");
        bl.find("a.get-here").on("click", function(ev) {
            ev.preventDefault();
            selectPoint($(this).data("uid"));
            $("#background .map-balloon").remove();
            return false;
        });
        bl.find("a.baloon-close").on("click", function(ev) {
            ev.preventDefault();
            $("#background .map-balloon").remove();
            return false;
        });
        var el = $(".m-shop[data-uid="+uid+"]"), y = el.position().top - bl.outerHeight(), x = el.position().left ;
        bl.css({ bottom: "calc("+(100 - 100*(y/$("#background").height()))+"% - "+(100*bl.outerHeight()/$("#background").height())+"% + 12px)", left: 100*(x/$("#background").width())+"%", top: "auto" });
        if (bl.position().top<0) {
            bl.css({ bottom: "calc("+(100 - 100*(y/$("#background").height()))+"% - "+(200*bl.outerHeight()/$("#background").height())+"% + 12px)" });
        }
        if (bl.position().top<$("#metro-container").scrollTop()) {
            $("#metro-container").animate({ scrollTop: tt = bl.position().top - 10 }, 100);
        }
        if (bl.position().left-100<$("#metro-container").scrollLeft()) {
            $("#metro-container").animate({ scrollLeft: bl.position().left - 110 }, 100);
        }
        if (bl.position().left+100>$("#metro-container").scrollLeft()+$("#metro-container").width()) {
            $("#metro-container").animate({ scrollLeft: bl.position().left + bl.width() - $("#metro-container").width()  }, 100);
        }
        
    }
    
    var selectPoint = function(uid) {
        var point = _points[uid], st = subwayMap.stations[point.metroID];
        var dsc = '<p class="metro line-'+st[1]+'">м. '+st[0]+'</p>';
            dsc+= '<p><b>Режим работы:</b> '+point.workhours+'</p>';
            dsc+= '<p><b>Можно забрать:</b> '+point.pickupTitle+'</p>';
            dsc+= '<p><b>Стоимость:</b> '+(point.price==0?"Бесплатно": point.price+" <span class='rub'>a</span>")+'</p>';
        var data = { header: "Самовывоз из магазина по адресу:<br>"+(typeof(point.addressParts[0])!='undefined' ? point.addressParts[0] : point.address),
                     mapLink: point.contactLink,
                     description: dsc,
                     uid: uid };
        top.setPickupPoint(data);
        top.$(top.document.body).find("#pickup-window").window("close");
    }
    
    window.setShopOnMap = function(uid) {
        if (typeof(placemarks[uid])=="undefined") return true;
        setTimeout(function() {
            $(".map-type ul input").eq(0).trigger("click");
            placemarks[uid].events.fire("click");
            placemarks[uid].balloon.open();
        }, 250);
    }
});

var subwayMap = {
    ready: false,
    zoom: 3,
    proportion: 1,
    maxWidth: 0,
    objects: [],
    objPerStation: {},
    stations: {},
    lines: {},
    init: function(el) {
        el.append("<div id='metro-container'><div id='background'></div><a href='#' class='-zoom in'></a><a href='#' class='-zoom out'></a></div>");
        this.root = el.find("#metro-container");
        var _dx = 0, _dy = 0, _bx = 0, _by = 0, wb = false, stch, bz;
        el.find("a.-zoom").on('click', function(e){ 
            e.preventDefault();
            e.stopPropagation();
            var cx = $("#metro-container").width()/2, cy = $("#metro-container").height()/2;
            if ($(this).hasClass("out")) {
                subwayMap.zoomOut(cx, cy);
            } else {
                subwayMap.zoomIn(cx, cy);
            }
            return false;
        });
        this.root
          .on("mousedown", function(e) {
            if (!(e.buttons&1)) return true;
            _dx = e.screenX, _dy = e.screenY, _bx = subwayMap.root.scrollLeft(), _by = subwayMap.root.scrollTop();
          })
          .on("mousemove", function(e) {
            if (e.buttons&1) {
                subwayMap.root.scrollLeft(_bx-e.screenX+_dx);
                subwayMap.root.scrollTop(_by-e.screenY+_dy);
            }
          })
          .on("wheel", function(e) {
            e.preventDefault();
            if (wb) return false;
            wb = true;
            setTimeout(function() { wb = false; } , 100);
            var dx = e.offsetX - subwayMap.root.scrollLeft();
            var dy = e.offsetY - subwayMap.root.scrollTop();
            if (e.originalEvent.deltaY>0) subwayMap.zoomOut(dx, dy);
            if (e.originalEvent.deltaY<0) subwayMap.zoomIn(dx, dy);
            return false;
          })
          .on("dblclick", function(e) {
            var dx = e.offsetX - subwayMap.root.scrollLeft();
            var dy = e.offsetY - subwayMap.root.scrollTop();
            subwayMap.zoomIn(dx, dy);
          })
          .on("touchstart", function(e) {
            stch = e.touches, _bx = subwayMap.root.scrollLeft(), _by = subwayMap.root.scrollTop();
            if (stch.length==2)  {
                var a = Math.abs(stch[0].clientX - stch[1].clientX), b = Math.abs(stch[0].clientY - stch[1].clientY);
                _dx = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
                bz = subwayMap.zoom;
            }
          })
          .on("touchmove", function(e) {
            e.preventDefault();
            if (e.touches.length==1) {
                subwayMap.root.scrollLeft(_bx-e.touches[0].clientX+stch[0].clientX);
                subwayMap.root.scrollTop(_by-e.touches[0].clientY+stch[0].clientY);
            } else if (e.touches.length==2 && stch.length==2) {
                var a = Math.abs(e.touches[0].clientX - e.touches[1].clientX), b = Math.abs(e.touches[0].clientY - e.touches[1].clientY),
                    cx = Math.floor((e.touches[0].clientX + e.touches[1].clientX)/2)-subwayMap.root.scrollLeft(), cy = Math.floor((e.touches[0].clientY + e.touches[1].clientY)/2) - subwayMap.root.offset().top-subwayMap.root.scrollTop();
                _dy = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
                subwayMap.setZoom( bz + (_dy-_dx)/40, cx, cy);
            }
            return false;
          
          })
          .on("touchend", function(e) {
            if (e.touches[0].clientX==stch[0].clientX && e.touches[0].clientY==stch[0].clientY) return true;
            e.preventDefault();
            if (e.touches.length==1) {
                subwayMap.root.scrollLeft(_bx-e.touches[0].clientX+stch[0].clientX);
                subwayMap.root.scrollLeft(_bx-e.touches[0].clientY+stch[0].clientY);
            }
            return false;
          });

        $.post("/!ajax/mos-metro.json", {}, function(data) {
        /* ------------------------------------------------ */
        /* REMOVE THIS FOR PRODUCTION !!!!!!!!!!!!!!!!!!!!! */
        /* ------------------------------------------------ */
            subwayMap._st = [];
            for (var i in data.stations) subwayMap._st.push(i);
            for (var j in top.window.pickupTypes) {
                for (i in top.window.pickupTypes[j].points) {
                    top.window.pickupTypes[j].points[i].metroID = subwayMap._st[Math.floor(Math.random()*subwayMap._st.length)];
                    var spec = { visa: false, mastercard: false, mir: false, deliv: false, gabarit: false };
                    for (var x in spec) spec[x] = Math.random()>.2;
                    top.window.pickupTypes[j].points[i].specials = spec;
                }
            }
        /* ------------------------------------------------ */
        /* // REMOVE THIS FOR PRODUCTION !!!!!!!!!!!!!!!!!! */
        /* ------------------------------------------------ */

            var img = new Image();
            img.onload = function() {
                subwayMap.ready = true;
                el.find("#background").css({backgroundImage: "url("+this.src+")"});
                subwayMap.proportion = this.width/this.height;
                subwayMap.maxWidth = this.width;
                subwayMap.render();
            }
            img.src = data.image;
            subwayMap.stations = data.stations;
            subwayMap.lines = data.lines;
        });
    },
    
    render: function() {
        this.setZoom(this.zoom);
        ppr = {};
        for (var i=0; i<this.objects.length; i++) {
            var mid = this.objects[i].metroID;
            if (!this.stations[mid] || typeof(this.stations[mid][2])=="undefined") continue;
            if (typeof(ppr[mid])=="undefined") ppr[mid] = 0; else ppr[mid]++;
            var x = 100 * this.stations[mid][2][0],
                y = 100 * this.stations[mid][2][1],
                dx = 0, dy = 0;
            var sh = $("<div class='m-shop t-"+this.objects[i].type+"' data-id='"+mid+"' data-uid='"+this.objects[i].uid+"'></div>");
            if (this.objPerStation[mid]>1) {
                var dx = Math.sin(ppr[mid]*2*Math.PI/this.objPerStation[mid])*9,
                    dy = Math.cos(ppr[mid]*2*Math.PI/this.objPerStation[mid])*9;
                sh.css({ zIndex: this.objPerStation[mid]-ppr[mid] });
            }
            sh.attr("title", this.stations[mid][0]);
                sh.appendTo(this.root.find("#background"));
                sh.css({left: "calc("+x+"% + "+dx+"px)", top: "calc("+y+"% + "+dy+"px)"});
        }
    },
    
    clear: function() {
        this.objPerStation = {};
        this.objects = [];
        this.root.find("#background").html("");
    },
    
    addObject: function( data ) {
        if (typeof(this.objPerStation[data.metroID])=="undefined") this.objPerStation[data.metroID] = 0;
        this.objPerStation[data.metroID]++;
        this.objects.push(data);
    },
    
    setZoom: function(n, cx, cy) {
        if (!cx) cx=0;
        if (!cy) cy=0;
        if (n<1) n=1;
        if (n>15) n=15;
        var bg = this.root.find("#background"), px = (this.root.scrollLeft()+cx)/bg.width(), py = (this.root.scrollTop()+cy)/bg.height();
        this.zoom = n;
        var w = this.maxWidth*(3+this.zoom/1.5)/10, h = w/this.proportion;
            this._w = w,
            this._h = h;
            bg.css({ width: w, height: h });
            this.root.finish().animate({
                scrollLeft: px*w-cx,
                scrollTop: py*h-cy
            }, 150, "linear");
        if (w<this.root.width()) {
            bg.css({ left: (this.root.width()-w)/2 });
        } else {
            bg.css({ left: "" });
        }
        return true;
    },
    
    zoomIn: function(cx, cy) {
        this.setZoom(this.zoom+2, cx, cy);
        $("a.-zoom.in").toggleClass("disabled", this.zoom>=15);
        $("a.-zoom.out").toggleClass("disabled", this.zoom<=1);
    },
    
    zoomOut: function(cx, cy) {
        this.setZoom(this.zoom-2, cx, cy);
        $("a.-zoom.in").toggleClass("disabled", this.zoom>=15);
        $("a.-zoom.out").toggleClass("disabled", this.zoom<=1);
    }
}
