window.addEventListener("scroll", function(e) {
    if ($) $("#top-scroller").toggleClass("visible", $(window).scrollTop()>0);
});

var clickHandler = ('ontouchstart' in document.documentElement ? "touchstart" : "click");
clickHandler = 'click';

function exists(identifier) {
	return (0 < $(identifier).length);
}

function isDev() {
	return (1 == $('#body').attr('data-is-dev'));
}

function isTerminal() {
	return (1 == $('#body').attr('data-is-terminal'));
}

function isHttps() {
	return (1 == $('#body').attr('data-is-https'));
}

function isDisableExternalResources() {
	return (1 == $('#body').attr('data-is-disable-external-resources'));
}

function isMobileDevice() {
	return $('#body').hasClass('mobile-device');
}

function isSearchEngine() {
	return (1 == $('#body').attr('data-is-search-engine'));
}

function getGeoCityId() {
	return $('#body').attr('data-geo-city-id');
}

function isImlCity() {
	return (1 == $('#body').attr('data-is-iml-city'));
}
function isSdekCity() {
	return (1 == $('#body').attr('data-is-sdek-city'));
}

function showHiddenHtml() {
	$('.hide-in-js').each(function() {
		var _ = $(this);
		if (!_.attr('html-initialized')) {
			var html = _.prop('innerHTML').slice(4, -3);
			_.prop('innerHTML', html);
			_.attr('html-initialized', true);
		}
	});
	if (!isSearchEngine()) {
		$('.hide-me').each(function() {
			var _ = $(this);
			var html = Base64.decode(window.seoContent[_.attr('data-hide-uid')]);
			if (html) {
				_.replaceWith(html);
			}
		});
	}
}

function getOrderAmount(number) {
	var amount = -1;
	$.ajax({
		method: 'get',
		async: false,
		url: '/ordering/pay/getamount/',
		data: { order: number },
		dataType: 'json',
		success: function(data) {
			if (data.success) {
				amount = data.amount;
			}
		}
	});
	return amount;
}

function spoilerToggle($spoiler, collapsed, speed, fade) {
	speed = speed || 200;
	fade = fade || false;
	if (collapsed) {
		if(fade){
			$spoiler.hide();
		} else {
			$spoiler.slideUp(speed);
			//$spoiler.animate({opacity:0}, speed);
		}
	} else {
		if(fade){
			$spoiler.fadeIn(speed);
		} else {
		    $spoiler.css({ height: "" });
			$spoiler.slideDown(speed);
			//$spoiler.animate({opacity:1}, speed);
		}
	}
}

function rangeInputsInit(){

	function format(value){

		var cur = '<span class="rub">a</span>';
		if(value >= 1000){
			value = (Math.round((value / 1000) * 10) / 10);
			cur = 'т.р.';
		} else {
			value = Math.round(value);
		}
		return value.toString() + '<span>' + cur + '</span>';
	}

	$('.range').not('.inited').each(function(){
		var _ = $(this);
		_.attr("data-source", $("<div>").append(_.clone()).html() );
		_.addClass('inited');
		var data = _.data();
		data.min = parseInt(data.min) || 0;
		data.max = parseInt(data.max) || 9999;
		data.start = parseInt(data.start) || data.min;
		data.finish = parseInt(data.finish) || data.max;
		data.padding = (data.max > 5000) ? (data.max / 10) : (data.max / 5);
		data.name = data.name || 'range';
		var append = '<div class="inputs"><input type="text" class="t-min" value="'+data.start+'"><input type="text" class="t-max" value="'+data.finish+'"></div><input type="hidden" class="min" name="'+data.name+'min" value="'+data.start+'">\
				<input type="hidden" class="max" name="'+data.name+'max" value="'+data.finish+'">';
		_.append(append);
		var isHide = false;
		if (data.min == data.max) {
			data.max = data.min + 1;
			isHide = true;
		}
		var slider = noUiSlider.create(this, {
			animate: true,
			animationDuration: 150,
			start: [data.start, data.finish],
			connect: true,
			tooltips: true,
			range: {
				min: data.min,
				max: data.max
			},
			behaviour: 'drag-tap',
			format : {
				to : format,
				from : Number
			}
		});
        _.find(".t-min, .t-max").data("slider", slider);
        
        _.parent().find(".-apply-element>a").on("click", function(e) {
            e.preventDefault();
            e.stopPropagation();
            var _d = $(this).parent().removeClass("visible").data("loadProducts");
            $(this).parents("li").addClass("checked").find("input[type=checkbox]")[0].checked = true;
            loadProducts(_d.hintLink, { scrollOnTop: _d.scrollOnTop, li: _d.li, element: _d.element })
            return false;
        });
        
        slider.on('set', function(points, handle, unencoded) {
			_.find('.min').val(points[0]);
			_.find('.max').val(points[1]);
			_.start = points[0];
			_.finish = points[1];
			var hintLink = _.attr('query-pattern').replace('price-from', parseInt(unencoded[0])).replace('price-to', parseInt(unencoded[1]));
            $(".slide-hint").removeClass('visible');
            _.parents(".filter-box").find(".slide-hint").addClass('visible').find("span").text(Math.round(Math.random()*500+2));
			if (isMobileDevice()) {
				hintProducts(hintLink, _, { hideHint: true, element: _ });
			} else {
				var scrollOnTop = true;
				if (1 == _.data('no-scroll-top')) {
					scrollOnTop = false;
				}
				if(!_.hasClass("on-apply-button") || _.parents("#-mobile-filters").length>0) {
    				loadProducts(hintLink, { scrollOnTop: scrollOnTop, li: _, element: _ });
    				if (_.parents("#-mobile-filters").length>0) {
    				    _.parents("li").addClass("checked").find("input[type=checkbox]")[0].checked = true;
    				}
    		    } else {
                    _.parent().find(".-apply-element").addClass("visible").data("loadProducts", { hintLink:hintLink, scrollOnTop:scrollOnTop, li:_, element:_ }) ;
    		    }
			}
		});
        slider.on("slide", function(a,b,c) {
            _.find(".t-min").val(Math.round(c[0]));
            _.find(".t-max").val(Math.round(c[1]));
        });
        _.parent().find('.mobile-select-block').each(function() {
            $(this).find(".values input").on("change blur", function() {
					var from = $(this).parent().find(".mobile-min").val();
					var to = $(this).parent().find(".mobile-max").val();
                slider.set([ from, to ]);
            });
            $(this).find("li").on("click", function() {
                var val = $(this).find("input").val().split(",");
					 var from = val[0];
					 var to = val[1];
                $(this).parents(".mobile-select-block").find(".mobile-min").val(from);
                $(this).parents(".mobile-select-block").find(".mobile-max").val(to);
					 slider.set([ from, to ]);
            });
        });
		if (isHide) {
			_.hide();
		}
	});

	function loadSliderData() {
		$('.range').each(function() {
			var _ = $(this);
			var hintLink = _.attr('query-pattern').replace('price-from', parseInt($('#i_from').val())).replace('price-to', parseInt($('#i_to').val()));
			if (isMobileDevice()) {
				hintProducts(hintLink, _, { hideHint: true, element: _ });
			} else {
				loadProducts(hintLink, { scrollOnTop: true, element: _ });
			}
		});
	}

	$('#i_from').on('change, focusout', function() {
		var _ = $(this);
		updateSliderFrom(_.val());
	});
	$('#i_from').on('keydown', function(e) {
		var _ = $(this);
		var val = parseInt(_.val());
		$('.range').each(function() {
			var slider = this.noUiSlider;
			var steps = slider.steps();
			console.log('steps', steps);
			var step = steps[0];
			console.log('step', step);

			// 13 is enter,
			// 38 is key up,
			// 40 is key down.
			console.log('which', e.which);
			switch (e.which) {
				case 13:
					updateSliderFrom(val);
					break;
				case 38:
					var position = step[1];
					if (false === position) {
						position = 10;
					}
					if (null !== position) {
						var newVal = val + position;
						_.val(newVal);
						updateSliderFrom(val);
					}
					break;
				case 40:
					var position = step[0];
					if (false === position) {
						position = 10;
					}
					if (null !== position) {
						var newVal = val - position;
						_.val(newVal);
						updateSliderFrom(newVal);
					}
					break;
			}
		});
	});
	$('#i_to').on('change, focusout', function() {
		var _ = $(this);
		updateSliderTo(_.val());
	});
	$('#i_to').on('keydown', function(e) {
		var _ = $(this);
		var val = parseInt(_.val());
		$('.range').each(function() {
			var slider = this.noUiSlider;
			var steps = slider.steps();
			console.log('steps', steps);
			var step = steps[1];
			console.log('step', step);

			// 13 is enter,
			// 38 is key up,
			// 40 is key down.
			console.log('which', e.which);
			switch (e.which) {
				case 13:
					updateSliderTo(val);
					break;
				case 38:
					var position = step[1];
					if (false === position) {
						position = 10;
					}
					if (null !== position) {
						var newVal = val + position;
						_.val(newVal);
						updateSliderTo(val);
					}
					break;
				case 40:
					var position = step[0];
					if (false === position) {
						position = 10;
					}
					if (null !== position) {
						var newVal = val - position;
						_.val(newVal);
						updateSliderTo(newVal);
					}
					break;
			}
		});
	});

	function updateSliderFrom(val) {
		$('.range').each(function() {
			var slider = this.noUiSlider;
			slider.set([val, null]);
			loadSliderData();
		});
	}
	function updateSliderTo(val) {
		$('.range').each(function() {
			var slider = this.noUiSlider;
			slider.set([null, val]);
			loadSliderData();
		});
	}
}

$(document).ready(function() {
	updateHeaderMenu();
	showHiddenHtml();

	initLozad();
	refreshCategoryTitle();
	initializeShoppilot();

	updateProductsDescriptionProperties();
	initGenericSlick();

	if (exists('.catalog-menu') && !isMobileDevice()) {
		var menu = $('.catalog-menu').remove();
		$('header .search-box').append(menu);
	}

	$("header a.catalogue-toggle").on("click", function(ev) {
        ev.preventDefault();
        $(".catalog-menu").fadeIn(200).scrollTop(0);
        return false;
    });

    $(".catalog-menu .header a.close").on("click", function(ev) {
        ev.preventDefault();
        $(".catalog-menu").fadeOut(200);
        return false;
    });

    $(".catalog-menu .city-selector a").on("click", function(ev) {
		var _ = $(this);
        $(".catalog-menu").fadeOut(100, function() {
    		if(_.attr('href') === '#'){
    			e.preventDefault();
    			var data = _.data();
    			var $container = $(data.window);
    			if($container.length && !data.hover){
    				data.magnet = _;
    				$container.window('toggle', data);
    			}
    		}
        });
    });

	 //-----------------------
    // Скролл вверх
    //----------------------- 

    $("a.scroll-top").on("click", function(ev) {
        ev.preventDefault();
        $("html,body").stop().animate({scrollTop: 0}, 200);
        return false;
    });

	$('.generic-banner-slider').each(function() {
		var _ = $(this);
		var slidesCount = parseInt(_.attr('data-slides'));
		_.find('.slider-wrapper').slick({
			infinite: false,
			slidesToShow: slidesCount,
			slidesToScroll: 1,
			draggable: true,
			prevArrow: _.find('.p-slick-control-prev'),
			nextArrow: _.find('.p-slick-control-next'),
			dots: false,
			appendDots: _.find('.slick-dots-wrapper'),
			responsive: [{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					dots: true,
					draggable: true,
					arrows: false
				}
			}]
		});
	});
});

function check_bf_size(resize){
    var bf = $('.bottom-fixed,.footer-bar');
    var tabs =  $('.footer-bar-bottom, .footer-bar .tabs-viewport >  .tab.active');
    var wh = $(window).innerHeight();
    //console.log($('.tab.active').length, $('.tab.active').outerHeight() + $('.footer-bar-top.tabs-list').outerHeight(), wh);
    var checker = resize ? bf.height() : $('.tab.active').outerHeight() + $('.footer-bar-top.tabs-list').outerHeight();
    if (checker > wh){
        bf.css('height', wh);
        tabs.css('height',bf.height() - $('.footer-bar-top.tabs-list').outerHeight() - parseInt($('.footer-bar-bottom').css('padding-top')) - parseInt($('.footer-bar-bottom').css('padding-bottom')));
    }else{
        if (!$('.footer-bar').hasClass('open')){
            bf.css('height', '60px');
        }else{
            bf.css('height', 'auto');
        }
        tabs.css('height', 'auto');
    }
    //console.log(wh,bf.height(),bf.height() - $('.footer-bar-top.tabs-list').height(),tabs.height());
}
function priceformat(intval){
	// price format
	intval = intval.toString().split('');
	intval.splice(-6, 0, ' ');
	intval.splice(-3, 0, ' ');
	return intval.join('');
}

function format_price(price) {
	var s = new String(price);
	var sInt = s;
	var sFloat = '';
	var dot = s.indexOf('.');
	if (0 <= dot) {
		var sInt = s.substring(0, dot);
		var sFloat = s.substr(dot);
		if (2 == sFloat.length) sFloat += '0';
	}
	var arr = sInt.split('');
	var res = [];
	for (var i = 0, length = arr.length; i < length; i++) {
		res.push(arr[i]);
		if ((length > (i + 1)) && (0 == ((length - i - 1) % 3))) res.push(' ');
	}
	return res.join('')+sFloat;
}

function intersect(a, b) {
	return $.grep(a, function(i) {
		return $.inArray(i, b) > -1;
	});
};

function header_recalculate_total(){
	var total = 0;
	$('#cart').find('.window-cart-items .countbox > input').each(function(){
		var _ = $(this);
		if(_.data('price')){
			total += parseInt(_.data('price') * _.val());
		}
	});
	$('#cart').find('.total > .price > span').text(priceformat(total));
	$('#cart').find('.total > .price > span.rub').text('a');
}
function header_recalculate_quantity(){
	var total = 0;
	$('#cart').find('.window-cart-items .countbox > input').each(function(){
		var _ = $(this);
		total += parseInt(_.val());
	});
	$('#cart').find('.header .c-gray').text(total + ' товар' + word_ending(total, '', 'а', 'ов'));
}

function header_recalculate_hint(count, amount) {
	var headerLink = $('header .cart-box');
	headerLink.attr('data-window', '#cart');
	headerLink.attr('data-hover', 'true');
	headerLink.attr('data-position', 'right');

	if (0 < count) {
		headerLink.removeClass('empty');
	} else if ((0 == count) && !headerLink.hasClass('empty')) {
		headerLink.addClass('empty');
	}

	headerLink.find('.global-count-cart').html((0 < count) ? count : '');
	headerLink.find('.cart-cash').html(format_price(amount) + '<span class="currency"> р.</span>');

	var mobileHeaderLink = $('header .header-top-menu-cart a');
	mobileHeaderLink.attr('data-window', '#cart');
	mobileHeaderLink.attr('data-position', 'responsive');
	if (0 == mobileHeaderLink.find('.cnt').length) {
		mobileHeaderLink.append($('<span class="cnt"><span></span></span>'));
	}
	var mobileCounter = mobileHeaderLink.find('.cnt');
	mobileCounter.find('span').html(count);

	$('.footbar-tab.shopcart .cl-blue').html(count);
}

function hintProducts(link, li, params) {
	var params = params || {};
	if (isMobileDevice()) {
		console.log(link, li, params);
	}
	if (true === window.hintProductsInprogress) {
		return;
	}
	window.hintProductsInprogress = true;
	$.ajax({
		method: 'get',
		url: link,
		data: { preview: 1 },
		dataType: 'json',
		complete: function(data) {
			window.hintProductsInprogress = false;
		},
		success: function(data) {
			// update price filter
			if (false) {
				$('.range.inited').each(function() {
					var _ = $(this);

					var valMin = parseInt(_.attr('data-start'));
					if (valMin < data.PRICE_MIN) {
						valMin = data.PRICE_MIN;
					}
					var valMax = parseInt(_.attr('data-finish'));
					if (valMax > data.PRICE_MAX) {
						valMax = data.PRICE_MAX;
					}
					if (data.PRICE_MIN < data.PRICE_MAX) {
						_.show();
						this.noUiSlider.updateOptions({
							range: {
								'min': data.PRICE_MIN,
								'max': data.PRICE_MAX
							},
							start: [valMin, valMax]
						});
					} else {
						_.hide();
					}
				});
			}

			// disable all filters
			$('.filter-box .checkbox-group li').addClass('disabled');
			$('.filter-box .checkbox-group li input[type="checkbox"]').prop('disabled', true);

			// enable & count filters
			$.each(data.NUM, function(propKey, property) {
				$.each(property.NUM, function(propertyValueId, propertyValue) {
					var key = propKey + '_' + propertyValueId;
					var input = $('#' + key);
					if (0 < input.length) {
						input.parent().removeClass('disabled');
						input.prop('disabled', false);
						input.attr('data-link', propertyValue.link);
						input.parent().find('.count').html('(' + propertyValue.count + ')');
					}
				});
			});

			$('.__btnSubmitFilters').attr('data-link', link);
			if (!params.hideHint) {
				var $item = $('<div>').html('Выбрано моделей: <strong>' + data.ALL_COUNT + '</strong>&nbsp;').append('<a href="#" class="__submitFilters" data-link="' + link + '">Показать</a>');
				window.hint($item.html(), li).show();
			}

			$('.filter-box.actions .display strong').html('<i>' + data.ALL_COUNT + '</i> товар' + word_ending(data.ALL_COUNT, '', 'а', 'ов'));
			$('.filter-box.actions .display').attr('data-link', data.DISPLAY_LINK);
		}
	});
}
function loadProducts(link, params) {
	params = params || {};
	params.success = params.success || function() {};
	params.scrollOnTop = params.scrollOnTop || false;
	params.hideFilters = params.hideFilters || false;
	params.appendData = params.appendData || false;
	params.updateHistory = (false === params.updateHistory ? false : true);

	if (1 == $('body').attr('data-no-history-update')) {
		params.updateHistory = false;
	}

	var productsContainer = $('#products-list');
	var sidebarContainer = $('#filter-sidebar');
	var sortingContainer = $('.catalog-sorting');
	var paginationContainer = $('.pagination-container');

	if (params.element) {
		var settingsEl = params.element.closest('.load-products-settings');
		if (0 < settingsEl.length) {
			if (settingsEl.attr('data-products-container-id')) {
				productsContainer = settingsEl.find(settingsEl.attr('data-products-container-id'));
			}
			if (settingsEl.attr('data-sidebar-container-id')) {
				sidebarContainer = settingsEl.find(settingsEl.attr('data-sidebar-container-id'));
			}
			if (settingsEl.attr('data-sorting-container-id')) {
				sortingContainer = settingsEl.find(settingsEl.attr('data-sorting-container-id'));
			}
			if (settingsEl.attr('data-pagination-container-id')) {
				paginationContainer = settingsEl.find(settingsEl.attr('data-pagination-container-id'));
			}
		}
	}

	//if (link && window.loadProductsXhr && (4 != window.loadProductsXhr.readyState)) {
	//	window.loadProductsXhr.abort();
	//	productsContainer.removeClass('loading');
	//}
	if (link && !productsContainer.hasClass('loading')) {
		productsContainer.addClass('loading');
		if (params.appendData) {
			productsContainer.addClass('append-data');
		} else {
			productsContainer.html('');
		}

		if (params.hideFilters) {
			$('#window-opener-background').trigger(clickHandler);
			$('#filter-window [data-action="close"] em').trigger(clickHandler);
		}

		if (params.scrollOnTop) {
			mainContainer = productsContainer.closest('.accessories-popup');
			if (0 < mainContainer.length) {
				mainContainer.find('.iziModal-wrap').animate({
					scrollTop: 0
				}, 300);
			} else {
				$('body,html').stop(true, true).animate(
					{ scrollTop: productsContainer.offset().top },
					300,
					'swing'
				);
			}
		}

		window.loadProductsXhr = $.ajax({
			method: 'get',
			url: link,
			data: {},
			dataType: 'json',
			success: function(data) {
			console.log(data);
				if (data.title) {
					var filterBtn = $(".open-xs-filter-wrapper").remove();
					$('.catalog-header h1').replaceWith(data.title);
					refreshCategoryTitle();
					$('.catalog-header h1').append(filterBtn);
				}

				if (params.appendData) {
					$('.pages-navigation').remove();
					productsContainer.append(data.products);
				} else {
					productsContainer.html(data.products);
				}
				if (data.sidebar) {
                    $(".-filters-list").html("");
					sidebarContainer.html(data.sidebar);
					sidebarContainer.find('.spoiler.collapsed').each(function () {
						spoilerToggle($(this), true, 1);
					});
					rangeInputsInit();
				}
				if (data.sorting || ('' == data.sorting)) {
					sortingContainer.html(data.sorting);
				}
				if (data.selectedCount || (0 < data.selectedCount)) {
					$(".left-panel h3 em").text(data.selectedCount).show();
				} else {
					$(".left-panel h3 em").text("").hide();
				}
				if (data.pagination || ('' == data.pagination)) {
					paginationContainer.html(data.pagination);
				}
				if ('undefined' != typeof(data.selectedFilters)) {
					$('.bottom-bar .top-bar.selected-filters').html(data.selectedFilters);
					try {
					    $("#-mobile-filters .selected-filters").html(data.selectedFilters);
					} catch(e) {}
				}
				showHiddenHtml();
				initializeProductsProperties();
				initLozad();

				if (0 < $('.search-tags').length) {
					refreshAccessoriesCategories();
				}
				
				try { 
					refreshMobileFilters();
				} catch(e) {}


				// shoppilot
				try {
					if (data.skus && (0 < data.skus.length)) {
						window._ShoppilotAPI.then(function (Shoppilot) {
							var ProductWidget = Shoppilot.require('product_widget');
							var MultiWidget = Shoppilot.require('multi_widget');
							var ratings = Array.prototype.slice.call(data.skus).map(
								function (sku) {
								console.log("!", sku);
									var obj = new ProductWidget({
										name: 'listing-inline-rating',
										product_id: sku,
										container: '.sp-inline-rating-container-' + sku
									});
									return obj;
								}
							);
							console.log('shoppilot', 'load', 'rating', 'count', ratings.length);
							MultiWidget.render(ratings);

							var reviews = Array.prototype.slice.call(data.skus).map(
								function (sku) {
									var obj = new ProductWidget({
										name: 'good-review-teaser',
										product_id: sku,
										container: '#sp-good-review-teaser-container-' + sku
									});
									return obj;
								}
							);
							console.log('shoppilot', 'load', 'reviews', 'count', reviews.length);
							MultiWidget.render(reviews);
						});
					}
				} catch (e) {
					console.log('error', e);
				}
				// END shoppilot

				if (params.updateHistory) {
					try {
						var historyLink = link;
						if (data.url) {
							//historyLink = data.url;
						}
						window.history.pushState({}, '', historyLink);
					} catch (e) { console.log(e); }
				}
				params.success(data);
			},
			complete: function(data) {
			
			console.log(data);
				productsContainer.removeClass('loading');
			}
		});
		window.hint().close();
	}
}

$(function () {

	/* TEST BOX (remove in production! )*/
	window.test = true;
	if(window.test){

		$('.pagination-mode').on(clickHandler, function(e) {
			e.preventDefault();

			var _ = $(this);
			var mode = _.find('.btn').hasClass('active') ? 'pages' : 'infinite';
			$.ajax({
				method: 'get',
				url: '/catalog/setup/view/',
				data: { scroll: mode },
				dataType: 'json',
				success: function(data) {
					if (data.success) {
						_.find('.btn').toggleClass('active');
						$('.mp-link-add').removeClass('mode-pages').removeClass('mode-infinite').addClass('mode-' + mode);
						initCatalogScroll(mode);
					}
				}
			});
		});

		$('.js-catalog-grid').on(clickHandler, function(e){
			e.preventDefault();
			
			var _ = $(this);
			$.ajax({
				method: 'get',
				url: '/catalog/setup/view/',
				data: { view: _.attr('data-view') },
				dataType: 'json',
				success: function(data) {
					if (data.success) {
						$('#products-list').removeClass('products-rows').removeClass('products-rows2').addClass('pi-4');
						_.parent().find('a').removeClass('active');
						_.addClass('active');
						$("#products-list .board").each(function() {
							$(this).appendTo($(this).parents(".product-item").find(".buy-block"));
						});
					}
				}
			});
		});
		$('.js-catalog-rows').on(clickHandler, function(e){
			e.preventDefault();
			
			var _ = $(this);
			$.ajax({
				method: 'get',
				url: '/catalog/setup/view/',
				data: { view: _.attr('data-view') },
				dataType: 'json',
				success: function(data) {
					if (data.success) {
						$('#products-list').addClass('products-rows').removeClass('products-rows2 pi-4');
						_.parent().find('a').removeClass('active');
						_.addClass('active');
						$("#products-list .board").each(function() {
							$(this).appendTo($(this).parents(".product-item").find(".product-rate"));
						});
						$(window).trigger('scroll');
					}
				}
			});
		});
		$('.js-catalog-rows2').on(clickHandler, function(e){
			e.preventDefault();

			var _ = $(this);
			$.ajax({
				method: 'get',
				url: '/catalog/setup/view/',
				data: { view: _.attr('data-view') },
				dataType: 'json',
				success: function(data) {
					if (data.success) {
						$('#products-list').addClass('products-rows2').removeClass('products-rows pi-4');
						_.parent().find('a').removeClass('active');
						_.addClass('active');
					}
				}
			});
		});
		
		$(document).on('slickProductsRemove', function(e, data){
			console.log(data);
		}).on(clickHandler, '.catalog-products .pagination ul a, .catalog-products .pages-navigation ul a, .pagination-place ul a', function(e){
			e.preventDefault();

			var _ = $(this);
			var link = _.attr('href');
			if ('#' != link) {
				var scrollOnTop = true;
				if (1 == _.data('no-scroll-top')) {
					scrollOnTop = false;
				}
				loadProducts(link, { scrollOnTop: scrollOnTop, element: _ });
			}
		}).on(clickHandler, '.catalog-sorting-mobile ul a', function(e){
			e.preventDefault();

			var _ = $(this);
			var link = _.data('link');
			var scrollOnTop = true;
			if (1 == _.data('no-scroll-top')) {
				scrollOnTop = false;
			}
			loadProducts(link, { scrollOnTop: scrollOnTop, element: _ });
		}).on(clickHandler, '.toolbar.sort .btn', function(e){
			e.preventDefault();
			var _ = $(this);
			$('.toolbar.sort a').removeClass('active').removeClass('up');
			var scrollOnTop = true;
			if (1 == _.attr('data-no-scroll-top')) {
				scrollOnTop = false;
			}
			loadProducts(_.attr('data-link'), {
				scrollOnTop: scrollOnTop,
				element: _,
				success: function() {
					_.addClass('active');
					if ('desc' == _.attr('data-direction')) {
						_.addClass('up');
					}
				}
			});
		}).on(clickHandler, '.tracking-delete', function(e) {
			e.preventDefault();

			var _ = $(this);
			var productId = _.attr('data-product-id');
			$.ajax({
				method: 'get',
				url: '/product/tracking/delete/',
				data: { product: productId },
				dataType: 'json',
				success: function(data) {
					if (data.success) {
						$('.tracking-product-' + productId).remove();
						$('.footbar-tab.tracked .cl-blue').html(data.count);
						$('header .favorites .count').html((0 < data.count) ? data.count : '');
					}
				}
			});
		}).on(clickHandler, '.add-to-cart, .small-cart', function(e) {
			e.preventDefault();

			var _ = $(this);
			var isMini = _.hasClass('small-cart');
			var isMainpage = _.hasClass('mp-cart');
			if (_.hasClass('btn-green') || (isMainpage && _.hasClass('mp-cart-in'))) {
				var link = (!isDev() ? 'https://' : 'http://') + window.location.host + '/ordering/';
				return redirect(link, '', 728);
			}
			$.ajax({
				method: 'get',
				url: _.attr('data-link'),
				data: {},
				dataType: 'json',
				success: function(data) {
					if (data.success) {
						if ($('#body').hasClass('order-page')) {
							if (true) {
								updateOrderProducts(1);
							} else {
								$.ajax({
									url: '/ordering/lines/',
									method: 'get',
									data: {},
									dataType: 'json',
									success: function(data) {
										if (data.success) {
											$('.container.step-shopcart .items').html(data.content);
										}
									}
								});
							}
						}

						$('#cart').html($(data.content).html());

						header_recalculate_hint(data.count, data.amount);

						var productId = _.attr('data-product-id');
						var wr = $('.add-to-cart-' + productId);
						if (wr.hasClass('btn-primary') || wr.hasClass('btn-orange')) {
							wr.text('в корзине').removeClass('btn-primary').removeClass('btn-orange').removeClass('btn-yellow').addClass('btn-green').addClass('in-basket');
						}
						if (wr.hasClass('standart-btn')) {
							wr.addClass('in-basket').addClass('btn-green');
							wr.find('span').text('в корзине');
						}
						if (wr.hasClass('btn-yellow')) {
							wr.removeClass('btn-yellow').addClass('btn-green').addClass('in-basket');
							wr.find('i.icon').removeClass('ic-cart-black').addClass('ic-cart-white');
							wr.find('span').text('в корзине');
						}
						if (isMainpage) {
							wr.text('в корзине').addClass('mp-cart-in');
						}

						var product = data.product;

						if (product) {
							if (!_.hasClass('no-popup') && !isMobileDevice()) {
								showBasketPreview(product);
							}
							if (_.attr('data-success-func')) {
								var successFunc = _.attr('data-success-func');
								successFunc(product.id);
							}
							GaEcommerceHelper.addProductToCart(
								product.id,
								product.name,
								product.category,
								product.price,
								product.quantity
							);
							if (product.rrId) {
								RRHelper.addToBasket([product.rrId]);
							}
							FlocktoryHelper.addToBasket(product.id, product.price, product.quantity);
						}

						if (_.hasClass('instant-order')) {
							var link = (!isDev() ? 'https://' : 'http://') + window.location.host + '/ordering/';
							return redirect(link, '', 789);
						}
					}
				}
			});
		}).on(clickHandler, '.add-to-check', function(){
			var _ = $(this);
			if (_.attr('data-product-id')) {
				var productId = _.attr('data-product-id');
				var mode = (_.hasClass('active') ? 'remove' : 'add');
				$.ajax({
					method: 'get',
					url: '/compare/' + mode + '/',
					data: { product: productId },
					dataType: 'json',
					success: function(data) {
						if (data.success) {
							var wr = $('.compare-product-' + productId);
							wr.toggleClass('active');
							if (wr.html() && wr.attr('data-title-active') && wr.attr('data-title-inactive')) {
								var title = ('add' == mode) ? wr.attr('data-title-active') : wr.attr('data-title-inactive');
								if (0 < wr.find('span').length) {
									wr.find('span').html(title);
								} else {
									wr.html(title);
								}
							}

							$('.footbar-tab.diagram span.cl-blue').html(data.count);
							$('.bottom-main-header .compare .count').html(data.count);
						}
					}
				});
			}
		}).on('add-to-check', function(e, data) {
			var _ = $(data);
			if (_.attr('data-product-id')) {
				var productId = _.attr('data-product-id');
				var mode = (!_.prop('checked') ? 'remove' : 'add');
				$.ajax({
					method: 'get',
					url: '/compare/' + mode + '/',
					data: { product: productId },
					dataType: 'json',
					success: function(data) {
						if (data.success) {
							var wr = $('.compare-product-' + productId);
							$('.footbar-tab.diagram span.cl-blue').html(data.count);
							$('.bottom-main-header .compare .count').html(data.count);
						}
					}
				});
			}
		}).on('add-to-compare', function(e, params) {
			$.ajax({
				method: 'get',
				url: '/compare/add/',
				data: { product: params.productId },
				dataType: 'json',
				success: function(data) {
					if (data.success) {
						var wr = $('.compare-product-' + params.productId);
						$('.footbar-tab.diagram span.cl-blue').html(data.count);
						$('.bottom-main-header .compare .count').html(data.count);
						if (params.successFunc) {
							params.successFunc(data);
						}
					}
				}
			});
		}).on('remove-from-compare', function(e, params) {
			$.ajax({
				method: 'get',
				url: '/compare/remove/',
				data: { product: params.productId },
				dataType: 'json',
				success: function(data) {
					if (data.success) {
						var wr = $('.compare-product-' + params.productId);
						$('.footbar-tab.diagram span.cl-blue').html(data.count);
						$('.bottom-main-header .compare .count').html(data.count);
						if (params.successFunc) {
							params.successFunc(data);
						}
					}
				}
			});
		}).on(clickHandler, '.add-to-favorites', function(){
			var _ = $(this);
			if (_.attr('data-product-id')) {
				var productId = _.attr('data-product-id');
				$.ajax({
					method: 'post',
					url: '/product/tracking/save/',
					data: { productId: productId },
					dataType: 'json',
					success: function(data) {
						if (data.success) {
							$('.footbar-tab.tracked span.cl-blue').html(data.count);
							$('header .favorites .count').html((0 < data.count) ? data.count : '');
						}
					}
				});
			}
		}).on('add-to-favorites', function(e, params) {
			$.ajax({
				method: 'post',
				url: '/product/tracking/save/',
				data: { productId: params.productId },
				dataType: 'json',
				success: function(data) {
					if (data.success) {
						$('.footbar-tab.tracked span.cl-blue').html(data.count);
						$('header .favorites .count').html((0 < data.count) ? data.count : '');
						if (params.successFunc) {
							params.successFunc(data);
						}
					}
				}
			});
		}).on('remove-from-favorites', function(e, params) {
			$.ajax({
				method: 'get',
				url: '/product/tracking/delete/',
				data: { product: params.productId },
				dataType: 'json',
				success: function(data) {
					if (data.success) {
						$('.footbar-tab.tracked span.cl-blue').html(data.count);
						$('header .favorites .count').html((0 < data.count) ? data.count : '');
						if (params.successFunc) {
							params.successFunc(data);
						}
					}
				}
			});
		}).on(clickHandler, '.__submitFilters', function(){
			var _ = $(this);
			loadProducts($(this).attr('data-link'), { scrollOnTop: true, element: _ });
		}).on(clickHandler, '.__btnSubmitFilters', function(){
			var _ = $(this);
			loadProducts($('.__btnSubmitFilters').attr('data-link'), { scrollOnTop: true, hideFilters: true, element: _ });
		}).on(clickHandler, '.__btnResetFilters', function(){
			var _ = $(this);
			loadProducts('?', { scrollOnTop: true, element: _ });
		}).on(clickHandler, '.__tracked', function(){
			$('#tracked').window('open', { position:'center', background: true });
		}).on(clickHandler, '.cart-header-new a', function(e){
			e.preventDefault();
			var _ = $(this);
			if(_.data('blocked') !== true){
				_.closest('.cart-header-new').find('a.active').removeClass('active');
				_.addClass('active');
				if (_.attr('data-step')) {
					var step = $('.step-' + _.attr('data-step'));
					if (step.length) {
						$('body,html').stop(true, true).animate(
							{ scrollTop: step.offset().top },
							300,
							'swing'
						);
					}
				}
			}
		}).on('clearcart', function(e, data){
			if (data.action && ('yes' == data.action)) {
				$.ajax({
					url: '/basket/clear/',
					action: 'get',
					data: {},
					dataType: 'json',
					success: function(data) {
						if (data.success) {
							var products = data.products;
							if (products && (0 < products.length)) {
								for (var i = 0, size = products.length; i < size; i++) {
									var product = products[i];
									GaEcommerceHelper.removeProductFromCart(
										product.id,
										product.name,
										product.category,
										product.price,
										product.quantity
									);
									FlocktoryHelper.removeFromBasket(product.id, product.quantity);
								}
							}

							redirect('/catalog/');
						}
					}
				});
			}
		}).on(clickHandler, '.map-sidebar li .btn-primary', function() {
			var _ = $(this);
			$('.map-sidebar li .btn-green').removeClass('btn-green').addClass('btn-primary').text('Заберу здесь');
			_.addClass('btn-green').removeClass('btn-primary').text('Выбрано');
			$('input[name="receipt-type"]').val('pickup');
			$('input[name="receipt-payment-types"]').val(_.attr('data-payment-types'));
			$('input[name="pickup-type"]').val(_.attr('data-pickup-type'));
			$('input[name="pickup-id"]').val(_.attr('data-pickup-id'));

			filterPaymentTypes();
			updateProductsAvailability();
			updateOrderTotalAmount();
		});

	}
	
	window.hint = function(html, magnet, vertical){
		var name = 'hintshow',
		    $hint = $('#'+name), $mg
		    vp = false;

        if (typeof(vertical)!="undefined" && vertical) {
            vp = true;
        }
		
		if($hint.length === 0){
			$hint = $('<span>').attr('id', name).addClass('hintshow').hide();
			$('body').append($hint.addClass('animate-all'));
		}
		
		if(html){
			$hint.html(html);
		}

        $hint.toggleClass("vertical", vp);

		if(magnet){
			magnet = $(magnet);
            $mg = magnet;
			$hint.css({
				position:'absolute',
				left:magnet.offset().left + magnet.outerWidth() + 12,
				top:magnet.offset().top,
			});
		}
		
		return {
			show : function(){
                $(".slide-hint.visible").removeClass("visible");
                if ($hint.hasClass("vertical")) {
                    setTimeout(function() {
            			$hint.css({
            				position:'absolute',
            				left:$mg.offset().left + $mg.outerWidth() - $hint.outerWidth()/2 - 10,
            				top:$mg.offset().top - $hint.outerHeight() - 6,
            			});
                    });
                }
				$hint.fadeIn(200);
			},
			close : function(){
                $(".slide-hint.visible").removeClass("visible");
				$hint.fadeOut(200);
			}
		};
	};

	rangeInputsInit();

	$('.spoiler.collapsed').each(function () {
		spoilerToggle($(this), true, 1);
	});

	$(document).on(clickHandler, '[data-toggle=accordion]', function (e) {

		e.preventDefault();
		var _ = $(this);
		var target = _.attr('href');
		if (_.data('target')) {
			target = _.data('target');
		}
		console.log("TG", target);
		if (_.parents(".filter-box").length>0) {
    		var $spoiler = _.parents(".filter-box").find(target);
		} else {
    		var $spoiler = $(target);   		
		}
		_.toggleClass('collapsed', !_.hasClass('collapsed'));
		$spoiler.toggleClass('collapsed', _.hasClass('collapsed'));
		if ($spoiler.length) {
			spoilerToggle($spoiler, _.hasClass('collapsed'));
		}

	}).on(clickHandler, '[data-toggle=showmore]', function (e) {

		e.preventDefault();
		var _ = $(this);
		var data = _.data();
		if(! data.starttext){
			data.starttext = _.text();
		}
		if(data.class){
			var $spoiler = _.parent().find('.'+data.class);
			if ($spoiler.length==0) {
    			$spoiler = _.parent().parent().find('.'+data.class);
			}
			var fade = (data.fade !== undefined);
			_.toggleClass('collapsed', !_.hasClass('collapsed'));
			$spoiler.toggleClass('collapsed', _.hasClass('collapsed'));
			if ($spoiler.length) {
				spoilerToggle($spoiler, _.hasClass('collapsed'), 200, fade);
			}
			if(data.endtext){
				if(_.hasClass('collapsed')){
					_.text(data.starttext);
				} else {
					_.text(data.endtext);
				}
			}
		}

	}).on(clickHandler, '.filter-box .checkbox-group > li a', function(e) {
		e.stopPropagation();
		e.preventDefault();
		var _ = $(this);
		_.parent().trigger(clickHandler);
	}).on(clickHandler, '.filter-box .checkbox-group > li', function(e) {
		//e.stopPropagation();
		//e.preventDefault();

		var li = this;
		var _ = $(this);
        if ($(e.target).parents('.range.on-apply-button').length>0) return true;
		var chk = $(this).find('input[type="checkbox"]');
		if ((0 < chk.length) && !chk.prop('disabled')) {
			var link = chk.attr('data-link');
			if (isMobileDevice()) {
				hintProducts(link, li, { hideHint: true, element: _ });
			} else {
				var scrollOnTop = true;
				if (1 == chk.data('no-scroll-top')) {
					scrollOnTop = false;
				}
/*				if (!$("body").hasClass("-new-cat")) { */
    				loadProducts(link, { scrollOnTop: scrollOnTop, element: _ });
/*    		    } else {
    		        var $this = this;
    		        setTimeout(function() {
        		        var y = $($this).parents(".filter-box").find("input[type=checkbox]"), ch = false;
        		        y.each(function() {
        		            if (this.checked!=$(this).data("default")) ch = true;
        		        });
                        $($this).parents(".filter-box").toggleClass("-changed", ch);
                    });
    		    }
*/
			}
		}
		return false;
	}).on(clickHandler, '.cycle-opener [data-toggle]', function(){

		$(this).parent().find('.t-center').toggleClass('dn');

	}).on(clickHandler, '.checkbox-group > li', function (e) {

		e.stopPropagation();
        if ($(e.target).parents('.range.on-apply-button').length>0) return true;
		if ('A' != e.target.nodeName) {
			var _ = $(this);
			var cb = _.find('input');
			if (!cb.prop('disabled')) {
				cb.prop('checked', !cb.prop('checked'));
				_.toggleClass('checked', cb.prop('checked'));

				if (!_.parent().hasClass('radio-mode')) {
					_.toggleClass('checked', cb.prop('checked'));
				} else {
					var li = _.parent().find("li");
					for (var i=0; i<li.length; i++) li.eq(i).toggleClass("checked", li.eq(i).find("input")[0].checked);
				}
			}

			if (cb.hasClass('insurance')) {
				$(document).trigger('insurance-changed', cb);
			}
			if (cb.hasClass('add-to-check')) {
				$(document).trigger('add-to-check', cb);
			}
			if (cb.hasClass('add-to-favorites')) {
				if (cb.prop('checked')) {
					$(document).trigger('add-to-favorites', { productId: cb.attr('data-product-id') });
				} else {
					$(document).trigger('remove-from-favorites', { productId: cb.attr('data-product-id') });
				}
			}
		}

	}).on(clickHandler, '._showAll', function(e){

		e.preventDefault();
		$(this).closest('.one-row-slider').find('.product-item.dn').fadeIn(300).removeClass('dn');

	}).on(clickHandler, '._showMore2', function(e){

		e.preventDefault();
		var $items = $(this).closest('#products-list').find('.product-item.dn');
		if($items.length > 10){
			$items.slice(0,10).fadeIn(300).removeClass('dn');
		} else {
			$items.slice(0,10).fadeIn(300).removeClass('dn');
			$(this).hide();
		}

	}).on('width-xs',function(){

		$('.one-row-slider').each(function(){
			var _ = $(this);
			var $items = _.find('.list .product-item');
			var $container = _.find('.slick-container');
			if($items.length && $container.length && ! $container.hasClass('inited')){
				$container.addClass('inited');
				_.find('.list').hide();
				$container.empty().append($items).slick({
					infinite: false,
					slidesToShow: 2,
					slidesToScroll: 1,
					draggable: true,
					arrows: false,
					dots: true,
					appendDots: _.find('.slick-dots-wrapper'),
					responsive: [{
						breakpoint: 480,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
							dots: true,
							draggable: true
						}
					}]
				});
			}
		});

		$('.generic-banner-slider2').each(function(){
			var _ = $(this);
			var $items = _.find('.slide');
			var $container = _.find('.slider-wrapper');
			if($items.length && $container.length && !$container.hasClass('inited')){
				$container.addClass('inited');
				//_.find('.list').hide();
				console.log($items);
				$container.empty().append($items).slick({
					infinite: false,
					slidesToShow: 1,
					slidesToScroll: 1,
					draggable: true,
					arrows: false
				});
			}
		});
		
		$('#filter-sidebar').children().appendTo($('#filter-window').find('.appendData'));

	}).on('width-sm width-md width-lg',function(){

		$('.one-row-slider').each(function(){
			var _ = $(this);
			var $container = _.find('.slick-container');
			if($container.hasClass('inited')){
				$container.slick('unslick');
				_.find('.list').append($container.find('.product-item')).show();
				$container.removeClass('inited');
			}
		});
		
		$('#filter-window').find('.appendData').children().appendTo($('#filter-sidebar'));
		$('#filter-window').window('close');

	}).on('width-xs', function(){
		check_bf_size(1);
		$('#products-list').each(function(){
			var _ = $(this);
			if(!_.data('class')){
				_.data('class', _.attr('class'));
				_.removeClass('products-rows products-rows2');
			}
			var $items = _.find('.product-item');
			if($items.length > 6) {
				var i = 1;
				$items.each(function(){
					if(i++ > 6){
						//$(this).addClass('dn');
					}
				});
			}
		});
		
	}).on('width-sm width-md width-lg', function(){
                check_bf_size(1);
		$('#products-list').each(function(){
			var _ = $(this);
			if(_.data('class')){
				_.addClass(_.data('class'));
				_.data('class', false);
			}
			_.find('.product-item').removeClass('dn');
		});
		
	}).on('dragstart', '.nodrag', function(e){

		e.preventDefault();
		e.stopPropagation();

	}).on(clickHandler, "a[href='#']", function(e){
		
		e.preventDefault();

	}).on(clickHandler, "#cart a.delete", function(e) {
		e.preventDefault();

		var _ = $(this);
		var productId = _.attr('data-product-id');
		if (productId) {
			$.ajax({
				method: 'get',
				url: '/basket/delete/',
				data: { product: productId },
				dataType: 'json',
				success: function(data) {
					if (data.success) {
						_.parent().remove();
						header_recalculate_hint(data.count, data.amount);
						header_recalculate_total();
						header_recalculate_quantity();

						if (0 == data.count) {
							$('header .cart-box').removeAttr('data-window').removeAttr('data-hover');
						}

						var product = data.product;
						if (product) {
							GaEcommerceHelper.removeProductFromCart(
								product.id,
								product.name,
								product.category,
								product.price,
								product.quantity
							);
							FlocktoryHelper.removeFromBasket(product.id, product.quantity);
						}
					}
				}
			});
		}
	}).on(clickHandler, '.tabs-list a', function(e){

		e.preventDefault();
		var _ = $(this);
		//js-can-deactivate - класс для табов которые можно деактивировать повторным нажатием.
		var canClose = _.hasClass('js-can-deactivate');
		var isActive = _.hasClass('active');
		if (isActive && !canClose) {
			return;
		}
		_.closest('.tabs-list').children('.active').removeClass('active');
		if (isActive && canClose) {

		} else {
			_.addClass('active');
		}
		var $tab = _.attr('href');
		if (_.attr('data-tab')) {
			$tab = '#' + _.attr('data-tab');
		}
		if ($tab) {
			$tab = $($tab);
			if ($tab.length) {
				if ($tab.attr('data-before-func')) {
					var func = $tab.attr('data-before-func');
					window[func]();
				}
				if (1 == _.attr('data-update-history')) {
					try {
						window.history.pushState({}, '', _.attr('href'));
					} catch (e) { console.log(e); }
				}
				$tab.closest('.tabs-viewport').children().removeClass('active').hide();
                                $tab.css('height','auto');
				if ($tab.hasClass('slided')) {
                                    
					$tab.closest('.tabs-viewport').stop(1, 1).animate({
						height: $tab.outerHeight()
					}, 100);
				}
				$tab.fadeIn(250, function () {
					if (isActive && canClose) {
						_.trigger('tabOpen', [_, false]);
					} else {
						$(this).addClass('active');
						_.trigger('tabOpen', [_, true]);
						if (!$tab.hasClass('initialized') && $tab.attr('data-source')) {
							$.ajax({
								url: $tab.attr('data-source'),
								method: 'get',
								data: {},
								dataType: 'json',
								success: function(data) {
									if (data.success) {
										$tab.html(data.content);
										$tab.removeClass('inprogress');
										$tab.trigger('redraw');
									}
								}
							});
						}
					}

					if ($tab.hasClass('tab-slider')){
						$('.fb-arrows').show();
					} else {
						$('.fb-arrows').hide();
						//check_bf_size();
					}
					$tab.trigger('redraw');

					if ('#tab-access' == _.attr('href')) {
						initSlick();
					}
					if ('#tab-service' == _.attr('href')) {
						initSlick();
					}
				});
			}
		}

	}).on(clickHandler, '[data-window]', function(e){
		var _ = $(this);
		if(_.attr('href') === '#'){
			e.preventDefault();
			var data = _.data();
			var $container = $(data.window);
			if($container.length && !data.hover){
				if (!data.magnet) data.magnet = _;
				else data.magnet = $(data.magnet);
				$container.window('toggle', data);
				var ifr = $container.find("iframe");
				if (ifr.length>0) {
                    if (!ifr.attr("src") && ifr.attr("data-source")) {
                        ifr.attr("src", ifr.attr("data-source"));
                    }
				}   

			}
		}
	}).on('mouseenter', '[data-window]', function(){
		clearTimeout(window.windowLeave);
        var data = $(this).data();
		if($(this).data('hover')){
			var $container = $($(this).data('window'));
			if($container.length){
				data.magnet = $(this);
				$container.off().on('mouseenter', function(){
					clearTimeout(window.windowLeave);
				}).on('mouseleave', function(){
					$container.window('close', data);
					//console.log($._data($container,'events'));
				});
				$container.window('open',data);
			}
		}
	}).on('mouseleave', '[data-window]', function(){
		if($(this).data('hover')){
			var $container = $($(this).data('window'));
			if($container.length){
				window.windowLeave = setTimeout(function(){
					$container.window('close');
				}, 500);
			}
		}
	}).on(clickHandler, '[data-confirm-trigger]', function(){
		var _ = $(this);
		var $confirm = $('#confirm');
		$confirm.find('a').off().on(clickHandler, function(){
			$(document).trigger(_.data('confirmTrigger'), $(this).data());
			$confirm.window('close');
		});
		$confirm.window('open', {background: true});
	}).on(clickHandler, '#filter-opener', function(){
		var _ = $(this);
		var $fw = $('#filter-window');
		if($fw.length){
			var t = $("header.third").outerHeight();
			$fw.css({ top: t, height: $(window).height() - t }).show();
			$fw.find(".open-xs-filter").css({top: t});
			/*$fw.window('open', {
				magnet: $(this),
				position: 'magnet',
				background: true
			}, function(win){
				win.width(_.outerWidth());
			});*/
		}
	}).on('mouseup', '.btn-group > .btn', function(e){
		e.preventDefault();
		e.stopPropagation();
		var _ = $(this);
		var $group = _.parent();
		_.parent().find('.active').removeClass('active');
		_.addClass('active');
		if($group.data('name')){
			$('[name="'+$group.data('name')+'"]').val(_.data('value'));
		}
	}).on(clickHandler, '.dropdown-menu a', function(e){
		e.preventDefault();
		var $select = $(this).closest('.dropdown-select');
		$select.find('span.current').text($(this).text());
		// set radio value in linked input
		if($select.data('name')){
			var val = $(this).text();
			if ($(this).attr('data-value')) {
				val = $(this).attr('data-value');
			}
			console.log(val);
			$('[name="'+$select.data('name')+'"]').val(val);
		}
	}).on('click touchstart', '.js-flip-footer-cap',function(){
        $(this).find('i').toggleClass('flip');
    }).on('mouseenter', '.video-item', function(){
		var $img = $(this).find('img');
		$img.attr('src', $img.data('gif'));
	}).on('mouseleave', '.video-item', function(){
		var $img = $(this).find('img');
		$img.attr('src', $img.data('src'));
	}).on('keypress', function(e){
		// page error form
		if(e.keyCode === 13 && e.shiftKey){
			$('#report').window('open', {position:'fixed', autofocus: true, background: true, onOpen : function(_){
					var selection = window.getSelection();
					if(selection){
						_.find('[name=sitepage]').val(window.location.pathname + window.location.hash);
						_.find('[name=errormessage]').val(selection);
					}
			}});
		}
		// user info form
		if (e.ctrlKey) {
			console.log(e.keyCode);
		}
		if (((2 == e.keyCode) || (1048 == e.keyCode)) && e.ctrlKey) {
			$('#userinfo').window('toggle', { responsize: true, background: true, position: 'center', onOpen: function(_) {
				var fld = $('#userinfo [name="message"]');
				var val = "\nViewort dimension: " + $(window).width() + 'x' + $(window).height();
				fld.val(fld.val() + val);
				fld.select();
			}});
		}
	}).on('redraw', '.tab-slider', function(){
		var $tab = $(this);
                $tab.find('.list.slick-initialized').slick('unslick');
        var sts = [ 5, 4, 1 ];
//		if( ! $tab.hasClass('inited')){
                        
			$tab.find('.list').not('.slick-initialized').slick({
				accessibility : true,
				adaptiveHeight : false,
				autoplay : false,
				//arrows : true,
				appendDots : $tab.find('.slick-dots-wrapper'),
				dots : false,
				rows: 1,
                                prevArrow: $('.fba-prev'),
                                nextArrow: $('.fba-next'),
				slidesToShow : sts[0],
				slidesToScroll : 1,
				swipeToSlide : false,
				infinite: false,
				responsive: [
					{
						breakpoint: 991,
						settings: {
							slidesToShow: sts[1],
							slidesToScroll: 1
						}
					},
					{
						breakpoint: 767,
						settings: {
							slidesToShow: sts[2],
							slidesToScroll: 1,
							dots: true,
							arrows:false
						}
					}
				],
			});
                        if ($tab.hasClass('tab-slider')){
                            check_bf_size();
                        }
//		}
	}).on(clickHandler, '[data-openspoiler]', function(e){
		e.preventDefault();
		var open = $(this).data('openspoiler');
		var close = $(this).data('closespoiler');
		$(open).fadeIn(100).find('input').first().focus();
		$(close).hide();
	}).on('change', '.window-cart-items .countbox > input', function(e){
		var _ = $(this);
		var data = _.data();
		var final = parseInt(_.val() * data.price);
		if(data.price){
			_.closest('.info').find('.price > span').text(priceformat(final));
			_.closest('.info').find('.price > span.rub').text('a');
		}
		header_recalculate_total();
		header_recalculate_quantity();
	}).find('.footer-bar .footbar-tab').each(function(){
		$(this).bind('tabOpen', function(e,_,open){
		  if(open) {
				_.closest('.footer-bar').addClass('open');
		  }else{
				_.closest('.footer-bar').removeClass('open');
		  }
		});
	});
	
	$('.checkbox-group input:checked').each(function(){
		var _ = $(this);
		//_.closest('.checkbox-group').find('li.checked').removeClass('checked');
		_.closest('li').addClass('checked');

		// set radio value in linked input
		if(_.data('name')){
			$('[name="'+_.data('name')+'"]').val(_.val());
		}
	});

	$('.tabs-viewport').each(function(){
		var _ = $(this);
		var $tab_active = _.children('.active.slided');
		if($tab_active.length){
			_.css({
				height : _.children('.active.slided').outerHeight()
			});
		}

	});
	
	$('.dropdown-select').each(function(){
		var _ = $(this);
		if(_.data('name')){
			$('[name="'+_.data('name')+'"]').val(_.find('.current').text());
		}
	});
	
	

	$('.radio-quality-review a').on(clickHandler, function(e){
		e.preventDefault();
		var _ = $(this);
		_.closest('.radio-quality-review').find('a').removeClass('active');
		_.addClass('active');
		_.closest('.radio-quality-review').find('input').val(_.text());
	});

	var slickProducts = $('.slick-products');

	if(slickProducts.length){
		function refresh_slick_products_properties() {
			$('.compare-cell').addClass('hidden');
			slickProducts.find('.slick-active').each(function() {
				var productId = $(this).attr('data-product-id');
				$('.compare .compare-cell-' + productId).removeClass('hidden');
			});
		}
		function reinit_slick_products(){
			slickProducts.off();
			if(slickProducts.hasClass('slick-slider')){
				slickProducts.slick('unslick');
			}
			slickProducts.on('init', function(event, slick) {
				refresh_slick_products_properties();
			});
			slickProducts.on('afterChange', function() {
				refresh_slick_products_properties();
			});
			slickProducts.slick({
				dots: false,
				infinite: false,
				speed: 300,
				slidesToShow: 3,
				slidesToScroll: 1,
				responsive: [
					{
						breakpoint: 767,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 2,
							dots: true
						}
					},
               {
						breakpoint: 350,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
							dots: true
						}
					}
				]
			});
			slickProducts.on(clickHandler, '.remove-item', function(e){
				e.preventDefault();
				var $item = $(this).closest('.product-item');
				$(window).trigger('slickProductsRemove', $item.data());
				$.ajax({
					method: 'get',
					url: '/compare/remove/',
					data: { product: $(this).attr('data-product-id') },
					dataType: 'json',
					success: function(data) {
						if (data.success) {
							$item.fadeOut(50, function() {
								$item.remove();
								reinit_slick_products();
							});

							$('.footbar-tab.diagram span.cl-blue').html(data.count);
						}
					}
				});
			});
		}
		reinit_slick_products();
	}

	$('.fake-placeholder').on(clickHandler, function(e){
		e.preventDefault();
		e.stopPropagation();
		$(this).find('input').focus();
	}).on('focus', 'input', function(){
		var _ph = $(this).closest('.fake-placeholder');
		$('.fake-placeholder').not(_ph).find('span').fadeIn(100);
		_ph.find('span').fadeOut(100);
	}).on('blur', 'input', function(){
        if (!$(this).val()){
            $(this).closest('.fake-placeholder').find('span').fadeIn(100);
        }
	});

	var resizeTimeout = false;
	window.fire_resizeWidth = function(){
		var ww = window.innerWidth;
		var steps = [
			//bootstrap grid
			[9999,1200,'lg'],[1200,992,'md'], [992,768,'sm'], [768,0,'xs']
		];
		steps.forEach(function(v){
			if(ww < v[0] && ww >= v[1]){

				$(document).trigger('width-'+v[2]);
			}
		});
	};
	window.fire_resizeWidth();
	$(window).on('resize', function(){
		clearTimeout(resizeTimeout);
		resizeTimeout = setTimeout(window.fire_resizeWidth, 50);
	});
	
	$('.catalog-link').on(clickHandler, function(){
		
		$(this).toggleClass('catalog-link-active');
		$('.catalog-menu').fadeToggle(300);

		//Задаем высоту столбов
		// var heightMenu = $('.catalog-menu').height();
		// $('.catalog-menu > div').height(heightMenu - 77);

		//Пересчитываем высоту столбов при клике на подменю
		// $('.catalog-menu a[data-toggle="accordion"]').click(function () {
		// $('.catalog-menu > div').css({"min-height":heightMenu,"height":"auto"});
		// });
		
	});
	
	var $fs = $('.form-search');
	$(document).on(clickHandler, function(event){
		if ($(event.target).closest('.catalog-menu').length === 0 && $(event.target).attr('id') !== 'cataloglink') {
			$('.catalog-menu').fadeOut(300);
			$('#cataloglink').removeClass('catalog-link-active');
		}
	}).on(clickHandler, '.header-top-menu-search a, .search-cart .btn_search', function (e) {
		e.preventDefault();
		$fs.removeClass('search-hidden').css('zIndex', 9999).find('input').attr('placeholder', 'Поиск').focus();
		$fs.window('getBackground').fadeIn(100);
		$('header.header .header-bottom').addClass('visible');
		return false;
	}).on(clickHandler, '.form-search .icon-close', function (e) {
		$fs.addClass('search-hidden');
		$fs.window('getBackground').fadeOut(50);
	}).on('blur', '.form-search input', function () {
		$fs.addClass('search-hidden');
		$fs.window('getBackground').fadeOut(50);
	}).on('keyup', '.form-search input', function (e) {
		if (e.which === 27) {
			$fs.addClass('search-hidden');
			$fs.window('getBackground').fadeOut(50);
		}
	});
	
	$('[type=file]').each(function(){
		var _ = $(this);
		_.hide();
		var $btn = $('<a>').addClass('btn btn-upload').attr('href', '#');
		$btn.on(clickHandler, function(e){
			e.preventDefault();
			_.trigger(clickHandler);
		});
		if(_.data('btnname')){
			$btn.text(_.data('btnname'));
		} else {
			$btn.text('Загрузить файл');
		}
		_.parent().append($btn);
	});

});

/*
 * Window Plugin
 * @author playmore
 * $(selector).window('open')
	$(selector).window('close')
	$(selector).window('toggle')
	$(selector).window('open', {settings}, _resizeCallback)
	{
		magnet : $() - селектор к которому нужно подтянуть окно
		position: - положение окна относительно magnet
			right - правой стороной по правому краю
			center - по центру
			magnet - точное положение magnet
			(по умолчанию окно будет в центре страницы)
		background : true - вывод бэкграунда под всплывающим окном (при клике закрывает окно)
	}
 */
(function( $ ) {

	$.fn.window = function(action, settings, _resizeCallback){

		var _ = this;

		settings = settings || {};
		function isObject(variable){
			return (typeof variable === 'object');
		}

		function position(type, $to){
			console.log('type', type);
			switch(type) {
				case 'right':  // if (x === 'value1')
					if(!isObject($to)) return;
					_.css({
						top: $to.offset().top + $to.outerHeight() + 10,
						//left: ($to.offset().left + $to.outerWidth()) - _.width() + 1,
						right: $(window).width() - ($to.offset().left + $to.outerWidth()) - 1,
						left: 'initial',
						marginLeft: 0
					}); break;

				case 'center':  // if (x === 'value2')
					if(!isObject($to)) return;
					_.css({
						top: $to.offset().top + $to.outerHeight() + 10,
						left: ($to.offset().left + ($to.outerWidth() / 2)) - (_.outerWidth() / 2),
						marginLeft: 0
					}); break;

				case 'magnet':
					console.log('magnet');
					if(!isObject($to)) return;
					_.css($to.offset());
					_.css({
						position: 'absolute',
						marginLeft: 0
					}); break;

				case 'magnet-bottom':
					console.log('magnet');
					if(!isObject($to)) return;
					_.css($to.offset());
					_.css({
						top: $to.offset().top - _.outerHeight() - 10,
						position: 'absolute',
						marginLeft: 0
					}); break;

				case 'magnet-offset':
					console.log('magnet');
					if(!isObject($to)) return;
					_.css($to.offset());
					_.css({
						top: $to.offset().top + $to.outerHeight() + 10,
						position: 'absolute',
						marginLeft: 0
					}); break;

				case 'responsive':
					if(!isObject($to)) return;
					console.log(settings);
					_.css({
						marginLeft: 0,
                        top:'48px',
                        left:'6.25%'
                        // width:'87.5%',
					});
					if ('fixed' == settings.position) {
						_.css({ position: 'fixed' })
					}
                    if (window.innerWidth < 768 && $('.header-top').length > 0){
                        _.css({position:'absolute',top:$('.header-top').offset().top + 48 + 'px'});
                        //console.log($('.header-top').offset().top + 48 + 'px' +' ' + _.css('position'));
                    }
                    break;

				case 'inside':
					_.parent().css({position: 'relative'});
					_.css({
						top: _.parent().outerHeight() +10,
						right: -1,
						left: 'initial',
						marginLeft: 0
					}); break;
					
				case 'fixed' : 
					var top = ($(window).height() / 2) - (_.outerHeight() / 2);
					if (0 > top) {
						top = 10;
					}
					_.css({
						top: top,
						left: ($(window).width() / 2) - (_.outerWidth() / 2),
						marginLeft: 0,
						position: 'fixed'
					});
                    break;

				default:
					break;
			}
		}
		
		
		var pos = settings.position;
		var methods = {
			toggle : function(){
				if(! _.hasClass('opened')){
					this.open();
				} else {
					this.close();
				}
			},
			open : function(completeFunc){
				completeFunc = completeFunc || function() {};
                methods.closeOther();
				_.addClass('opened').stop(1,1).fadeIn(300, function() {
					completeFunc();
					if (false && _.find('#pc-delivery')){
							$('#pc-delivery .responsive-table .row > div').each(function(){
								var _ = $(this);
								_.height(_.parent('.row').height() - parseInt(_.css('padding-top'))- parseInt(_.css('padding-bottom')));
							});
					  }
					  if (_.offset().top+_.outerHeight()>$(window).scrollTop()+window.innerHeight) {
					    var dy  = _.offset().top+_.outerHeight()-window.innerHeight+20;
					    if (!_.hasClass("noscroll")) $("html,body").stop().animate({scrollTop: dy}, 150);
					  }
				});
				if(settings.autofocus){
				    setTimeout(function() {
    					_.find('input[type="text"],input[type="password"],input[type="tel"]').eq(0).focus();
    		        }, 10);
				}
				if(settings.background){
					methods.getBackground().on(clickHandler, function(){
						methods.close();
					}).fadeIn(300);
				}
				if (settings.popuptitle) {
					_.find('.header span').html(settings.popuptitle);
				}
				methods.draw();

				if (_.find('.nano')){
						_.find('.nano').nanoScroller();
						
				 
				  
				  if(_.find('.pc-slider-wrapper').length > 0){
						_.find('.pc-slider-wrapper').slick('refresh');
				  }
				}

				$('.catalog-menu .close').click();

				initNano();
			},
			close : function(){
				_.removeClass('opened').stop(1,1).hide();
				$('#window-opener-background').off().fadeOut(150);

				if(settings.onClose && $.isFunction(settings.onClose)){
					settings.onClose(_);
				}
			},
			draw : function(){

                if (settings.responsive && window.innerWidth < 768){
                    pos = 'responsive';
                    if (settings.responsive === 'fixed'){
                        _.css('position','fixed');
                    }
                }

				position(pos, settings.magnet);
				if($.isFunction(_resizeCallback)){
					_resizeCallback(_);
				}
			},
            closeOther: function(){
                $('.window').hide().removeClass('opened');
				methods.getBackground().off().hide();
            },
			getBackground : function(){
				var $background = $('#window-opener-background');
				if($background.length < 1){
					$background = $('<div>').attr('id', 'window-opener-background').hide();
					$('body').append($background);
				}
				return $background;
			}
		};
		
		if(settings.onOpen && $.isFunction(settings.onOpen)){
			settings.onOpen(_);
		}

		if( ! _.data('inited')){
			$(window).on('resize', function(){
				methods.draw();
                if (pos === 'responsive' && _.hasClass('opened') && window.innerWidth >= 768){
                    methods.close();
                }
			});
			_.on(clickHandler, '[data-action]', function(){
				var action = $(this).data('action');
				if(action in methods){
					return methods[action].call(methods);
				}
			}).on(clickHandler, function(e){
				//e.stopPropagation();
			});
		}

		_.data('inited', true);

		if(action in methods){
			return methods[action].call(methods);
		}

	};

} (jQuery));

/*
    jQuery Masked Input Plugin
    Copyright (c) 2007 - 2014 Josh Bush (digitalbush.com)
    Licensed under the MIT license (http://digitalbush.com/projects/masked-input-plugin/#license)
    Version: 1.4.0
*/
!function(factory) {
    "function" == typeof define && define.amd ? define([ "jquery" ], factory) : factory("object" == typeof exports ? require("jquery") : jQuery);
}(function($) {
    var caretTimeoutId, ua = navigator.userAgent, iPhone = /iphone/i.test(ua), chrome = /chrome/i.test(ua), android = /android/i.test(ua);
    $.mask = {
        definitions: {
            "9": "[0-9]",
            a: "[A-Za-z]",
            "*": "[A-Za-z0-9]"
        },
        autoclear: !0,
        dataName: "rawMaskFn",
        placeholder: "_"
    }, $.fn.extend({
        caret: function(begin, end) {
            var range;
            if (0 !== this.length && !this.is(":hidden")) return "number" == typeof begin ? (end = "number" == typeof end ? end : begin, 
            this.each(function() {
                this.setSelectionRange ? this.setSelectionRange(begin, end) : this.createTextRange && (range = this.createTextRange(), 
                range.collapse(!0), range.moveEnd("character", end), range.moveStart("character", begin), 
                range.select());
            })) : (this[0].setSelectionRange ? (begin = this[0].selectionStart, end = this[0].selectionEnd) : document.selection && document.selection.createRange && (range = document.selection.createRange(), 
            begin = 0 - range.duplicate().moveStart("character", -1e5), end = begin + range.text.length), 
            {
                begin: begin,
                end: end
            });
        },
        unmask: function() {
            return this.trigger("unmask");
        },
        mask: function(mask, settings) {
            var input, defs, tests, partialPosition, firstNonMaskPos, lastRequiredNonMaskPos, len, oldVal;
            if (!mask && this.length > 0) {
                input = $(this[0]);
                var fn = input.data($.mask.dataName);
                return fn ? fn() : void 0;
            }
            return settings = $.extend({
                autoclear: $.mask.autoclear,
                placeholder: $.mask.placeholder,
                completed: null
            }, settings), defs = $.mask.definitions, tests = [], partialPosition = len = mask.length, 
            firstNonMaskPos = null, $.each(mask.split(""), function(i, c) {
                "?" == c ? (len--, partialPosition = i) : defs[c] ? (tests.push(new RegExp(defs[c])), 
                null === firstNonMaskPos && (firstNonMaskPos = tests.length - 1), partialPosition > i && (lastRequiredNonMaskPos = tests.length - 1)) : tests.push(null);
            }), this.trigger("unmask").each(function() {
                function tryFireCompleted() {
                    if (settings.completed) {
                        for (var i = firstNonMaskPos; lastRequiredNonMaskPos >= i; i++) if (tests[i] && buffer[i] === getPlaceholder(i)) return;
                        settings.completed.call(input);
                    }
                }
                function getPlaceholder(i) {
                    return settings.placeholder.charAt(i < settings.placeholder.length ? i : 0);
                }
                function seekNext(pos) {
                    for (;++pos < len && !tests[pos]; ) ;
                    return pos;
                }
                function seekPrev(pos) {
                    for (;--pos >= 0 && !tests[pos]; ) ;
                    return pos;
                }
                function shiftL(begin, end) {
                    var i, j;
                    if (!(0 > begin)) {
                        for (i = begin, j = seekNext(end); len > i; i++) if (tests[i]) {
                            if (!(len > j && tests[i].test(buffer[j]))) break;
                            buffer[i] = buffer[j], buffer[j] = getPlaceholder(j), j = seekNext(j);
                        }
                        writeBuffer(), input.caret(Math.max(firstNonMaskPos, begin));
                    }
                }
                function shiftR(pos) {
                    var i, c, j, t;
                    for (i = pos, c = getPlaceholder(pos); len > i; i++) if (tests[i]) {
                        if (j = seekNext(i), t = buffer[i], buffer[i] = c, !(len > j && tests[j].test(t))) break;
                        c = t;
                    }
                }
                function androidInputEvent() {
                    var curVal = input.val(), pos = input.caret();
                    if (curVal.length < oldVal.length) {
                        for (checkVal(!0); pos.begin > 0 && !tests[pos.begin - 1]; ) pos.begin--;
                        if (0 === pos.begin) for (;pos.begin < firstNonMaskPos && !tests[pos.begin]; ) pos.begin++;
                        input.caret(pos.begin, pos.begin);
                    } else {
                        for (checkVal(!0); pos.begin < len && !tests[pos.begin]; ) pos.begin++;
                        input.caret(pos.begin, pos.begin);
                    }
                    tryFireCompleted();
                }
                function blurEvent() {
                    checkVal(), input.val() != focusText && input.change();
                }
                function keydownEvent(e) {
                    if (!input.prop("readonly")) {
                        var pos, begin, end, k = e.which || e.keyCode;
                        oldVal = input.val(), 8 === k || 46 === k || iPhone && 127 === k ? (pos = input.caret(), 
                        begin = pos.begin, end = pos.end, end - begin === 0 && (begin = 46 !== k ? seekPrev(begin) : end = seekNext(begin - 1), 
                        end = 46 === k ? seekNext(end) : end), clearBuffer(begin, end), shiftL(begin, end - 1), 
                        e.preventDefault()) : 13 === k ? blurEvent.call(this, e) : 27 === k && (input.val(focusText), 
                        input.caret(0, checkVal()), e.preventDefault());
                    }
                }
                function keypressEvent(e) {
                    if (!input.prop("readonly")) {
                        var p, c, next, k = e.which || e.keyCode, pos = input.caret();
                        if (!(e.ctrlKey || e.altKey || e.metaKey || 32 > k) && k && 13 !== k) {
                            if (pos.end - pos.begin !== 0 && (clearBuffer(pos.begin, pos.end), shiftL(pos.begin, pos.end - 1)), 
                            p = seekNext(pos.begin - 1), len > p && (c = String.fromCharCode(k), tests[p].test(c))) {
                                if (shiftR(p), buffer[p] = c, writeBuffer(), next = seekNext(p), android) {
                                    var proxy = function() {
                                        $.proxy($.fn.caret, input, next)();
                                    };
                                    setTimeout(proxy, 0);
                                } else input.caret(next);
                                pos.begin <= lastRequiredNonMaskPos && tryFireCompleted();
                            }
                            e.preventDefault();
                        }
                    }
                }
                function clearBuffer(start, end) {
                    var i;
                    for (i = start; end > i && len > i; i++) tests[i] && (buffer[i] = getPlaceholder(i));
                }
                function writeBuffer() {
                    input.val(buffer.join(""));
                }
                function checkVal(allow) {
                    var i, c, pos, test = input.val(), lastMatch = -1;
                    for (i = 0, pos = 0; len > i; i++) if (tests[i]) {
                        for (buffer[i] = getPlaceholder(i); pos++ < test.length; ) if (c = test.charAt(pos - 1), 
                        tests[i].test(c)) {
                            buffer[i] = c, lastMatch = i;
                            break;
                        }
                        if (pos > test.length) {
                            clearBuffer(i + 1, len);
                            break;
                        }
                    } else buffer[i] === test.charAt(pos) && pos++, partialPosition > i && (lastMatch = i);
                    return allow ? writeBuffer() : partialPosition > lastMatch + 1 ? settings.autoclear || buffer.join("") === defaultBuffer ? (input.val() && input.val(""), 
                    clearBuffer(0, len)) : writeBuffer() : (writeBuffer(), input.val(input.val().substring(0, lastMatch + 1))), 
                    partialPosition ? i : firstNonMaskPos;
                }
                var input = $(this), buffer = $.map(mask.split(""), function(c, i) {
                    return "?" != c ? defs[c] ? getPlaceholder(i) : c : void 0;
                }), defaultBuffer = buffer.join(""), focusText = input.val();
                input.data($.mask.dataName, function() {
                    return $.map(buffer, function(c, i) {
                        return tests[i] && c != getPlaceholder(i) ? c : null;
                    }).join("");
                }), input.one("unmask", function() {
                    input.off(".mask").removeData($.mask.dataName);
                }).on("focus.mask", function() {
                    if (!input.prop("readonly")) {
                        clearTimeout(caretTimeoutId);
                        var pos;
                        focusText = input.val(), pos = checkVal(), caretTimeoutId = setTimeout(function() {
                            writeBuffer(), pos == mask.replace("?", "").length ? input.caret(0, pos) : input.caret(pos);
                        }, 10);
                    }
                }).on("blur.mask", blurEvent).on("keydown.mask", keydownEvent).on("keypress.mask", keypressEvent).on("input.mask paste.mask", function() {
                    
                    input.prop("readonly") || setTimeout(function() {
                        var pos = checkVal(!0);
                        input.caret(pos), tryFireCompleted();
                    }, 0);
                }), chrome && android && input.off("input.mask").on("input.mask", androidInputEvent), 
                checkVal();
            });
        }
    });
});

/* Start:/source/js/bootstrap.min.js*/
/**
* Bootstrap.js by @fat & @mdo
* plugins: bootstrap-typeahead.js
* Copyright 2013 Twitter, Inc.
* http://www.apache.org/licenses/LICENSE-2.0.txt
*/
!function(a){var b=function(b,c){this.$element=a(b),this.options=a.extend({},a.fn.typeahead.defaults,c),this.matcher=this.options.matcher||this.matcher,this.sorter=this.options.sorter||this.sorter,this.highlighter=this.options.highlighter||this.highlighter,this.updater=this.options.updater||this.updater,this.source=this.options.source,this.$menu=a(this.options.menu),this.shown=!1,this.listen()};b.prototype={constructor:b,select:function(){var a=this.$menu.find(".active").attr("data-value");return this.$element.val(this.updater(a)).change(),this.hide()},updater:function(a){return a},show:function(){var b=a.extend({},this.$element.position(),{height:this.$element[0].offsetHeight});return this.$menu.insertAfter(this.$element).css({top:b.top+b.height,left:b.left}).show(),this.shown=!0,this},hide:function(){return this.$menu.hide(),this.shown=!1,this},lookup:function(b){var c;return this.query=this.$element.val(),!this.query||this.query.length<this.options.minLength?this.shown?this.hide():this:(c=a.isFunction(this.source)?this.source(this.query,a.proxy(this.process,this)):this.source,c?this.process(c):this)},process:function(b){var c=this;return b=a.grep(b,function(a){return c.matcher(a)}),b=this.sorter(b),b.length?this.render(b.slice(0,this.options.items)).show():this.shown?this.hide():this},matcher:function(a){return~a.toLowerCase().indexOf(this.query.toLowerCase())},sorter:function(a){var b=[],c=[],d=[],e;while(e=a.shift())e.toLowerCase().indexOf(this.query.toLowerCase())?~e.indexOf(this.query)?c.push(e):d.push(e):b.push(e);return b.concat(c,d)},highlighter:function(a){var b=this.query.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&");return a.replace(new RegExp("("+b+")","ig"),function(a,b){return"<strong>"+b+"</strong>"})},render:function(b){var c=this;return b=a(b).map(function(b,d){return b=a(c.options.item).attr("data-value",d),b.find("a").html(c.highlighter(d)),b[0]}),b.first().addClass("active"),this.$menu.html(b),this},next:function(b){var c=this.$menu.find(".active").removeClass("active"),d=c.next();d.length||(d=a(this.$menu.find("li")[0])),d.addClass("active")},prev:function(a){var b=this.$menu.find(".active").removeClass("active"),c=b.prev();c.length||(c=this.$menu.find("li").last()),c.addClass("active")},listen:function(){this.$element.on("focus",a.proxy(this.focus,this)).on("blur",a.proxy(this.blur,this)).on("keypress",a.proxy(this.keypress,this)).on("keyup",a.proxy(this.keyup,this)),this.eventSupported("keydown")&&this.$element.on("keydown",a.proxy(this.keydown,this)),this.$menu.on("click",a.proxy(this.click,this)).on("mouseenter","li",a.proxy(this.mouseenter,this)).on("mouseleave","li",a.proxy(this.mouseleave,this))},eventSupported:function(a){var b=a in this.$element;return b||(this.$element.setAttribute(a,"return;"),b=typeof this.$element[a]=="function"),b},move:function(a){if(!this.shown)return;switch(a.keyCode){case 9:case 13:case 27:a.preventDefault();break;case 38:a.preventDefault(),this.prev();break;case 40:a.preventDefault(),this.next()}a.stopPropagation()},keydown:function(b){this.suppressKeyPressRepeat=~a.inArray(b.keyCode,[40,38,9,13,27]),this.move(b)},keypress:function(a){if(this.suppressKeyPressRepeat)return;this.move(a)},keyup:function(a){switch(a.keyCode){case 40:case 38:case 16:case 17:case 18:break;case 9:case 13:if(!this.shown)return;this.select();break;case 27:if(!this.shown)return;this.hide();break;default:this.lookup()}a.stopPropagation(),a.preventDefault()},focus:function(a){this.focused=!0},blur:function(a){this.focused=!1,!this.mousedover&&this.shown&&this.hide()},click:function(a){a.stopPropagation(),a.preventDefault(),this.select(),this.$element.focus()},mouseenter:function(b){this.mousedover=!0,this.$menu.find(".active").removeClass("active"),a(b.currentTarget).addClass("active")},mouseleave:function(a){this.mousedover=!1,!this.focused&&this.shown&&this.hide()}};var c=a.fn.typeahead;a.fn.typeahead=function(c){return this.each(function(){var d=a(this),e=d.data("typeahead"),f=typeof c=="object"&&c;e||d.data("typeahead",e=new b(this,f)),typeof c=="string"&&e[c]()})},a.fn.typeahead.defaults={source:[],items:8,menu:'<ul class="typeahead dropdown-menu"></ul>',item:'<li><a href="#"></a></li>',minLength:1},a.fn.typeahead.Constructor=b,a.fn.typeahead.noConflict=function(){return a.fn.typeahead=c,this},a(document).on("focus.typeahead.data-api",'[data-provide="typeahead"]',function(b){var c=a(this);if(c.data("typeahead"))return;c.typeahead(c.data())})}(window.jQuery)
/* End */

function redirect(href, addparam, from) {
	var add = '';
	if (addparam) {
		if (0 <= href.lastIndexOf('?')) {
			add = '&'+addparam;
		} else add = '?'+addparam;
	}
	location.href = href+add;
}

function refreshPage() {
	window.location.reload(true);
}

function word_ending(total, end1, end2, end3) {
	var total = new String(total);
	var last1 = parseInt(total.substr(total.length-1));
	var last2 = parseInt(total.substr(total.length-2));
	if (
		(2 <= last1) &&
		(4 >= last1) &&
		!(11 <= last2 && last2 <= 19)
	) return end2;
	if (
		(1 == last1) &&
		!(11 <= last2 && last2 <= 19)
	) return end1;
	return end3;
}

function $interval(delay, func) {
	return window.setInterval(func, delay * 1000);
}
function $timeout(delay, func) {
	return window.setTimeout(func, delay * 1000);
}
function $clear(tmr) {
	if (tmr) {
		window.clearInterval(tmr);
		window.clearTimeout(tmr);
	}
}

function get_cookie(name) {
	var c = document.cookie;
	if (1 > c.length) return false;

	var b = c.indexOf(name + '=');
	if (-1 == b) return false;

	b += (name.length + 1);
	var e = c.indexOf(';', b);

	return unescape((-1 == e) ? c.substring(b) : c.substring(b, e));
}

function set_cookie(name, value) {
	document.cookie = name + '=' + escape(value) + ';path=/';
}

function str_replace(search, replace, string) {
	while (-1 != string.indexOf(search)) {
		string = string.replace(search, replace);
	}
	return string;
}

function formData(form) {
	var o = {};
    var a = $(form).serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
}

function initMask() {
	$('input.masked').each(function() {
		$input = $(this);
		if (!$input.attr('mask-inialized') && $input.attr('mask')) {
			var mask = $input.attr('mask');
			var placeholder = $input.attr('mask-placeholder') || '';
			var params = {};
			if (placeholder) {
				params.placeholder = placeholder;
			}
			params.clearIfNotMatch = false;
			if ('legal-inn' == $input.attr('name')) {
				params.onKeyPress = function(cep, e, field, params) {
					var masks = ['9999999999', '999999999999'];
					var mask = (10 > cep.length) ? masks[0] : masks[1];
					$input.mask(mask, params);
				};
			}
			var res = $input.mask(mask, params);
			$input.attr('mask-initialized', 1);
		}
	});
}

function selectCity(code, returnUrl) {
	var data = { kladr: code, returnPath: returnUrl };
	$.ajax({
		method: 'post',
		url: '/geo/current/',
		data: data,
		dataType: 'json',
		success: function(data) {
			if (data.link) {
				redirect(returnUrl, '', 2239);
			} else {
				refreshPage();
			}
		}
	});
}

function submitForm(formId, params) {
	params = params || {};
	params.success = params.success || function(data) {};
	params.error = params.error || function(data) {};
	params.before = params.before || function(data) {};
	params.after = params.after || function(data) {};

	var frm = $(formId);

	var successDiv = frm.parent().find('.success');
	successDiv.hide();

	var errorDiv = frm.parent().find('.errors');
	errorDiv.hide();

	var submit = frm.find('.btn-submit');
	if (!submit.hasClass('inprogress')) {
		submit.addClass('inprogress');

		params.before();
		var frmAction = frm.attr('action');
		if (!frmAction && frm.attr('data-action')) {
			frmAction = frm.attr('data-action');
		}
		var dataForm = formData(frm);
		$.ajax({
			url: frmAction,
			method: frm.attr('method'),
			data: formData(frm),
			dataType: 'json',
			success: function(data) {
				data.form = frm;
				data.formData = dataForm;
				if (data.success) {
					params.success(data);
					successDiv.show();
				} else {
					params.error(data);
					if (data.errorMessage) {
						errorDiv.html(data.errorMessage);
					}
					errorDiv.show();
				}
			},
			complete: function(data) {
				submit.removeClass('inprogress');
			},
			error: function(data) {
				data.form = frm;
				params.error(data);
				errorDiv.show();
			}
		});

		params.after();
	}
}

$(document).ready(function() {

	initMask();

	// main breadcrumb
	if (false && (0 < $('.breadcrumbs ul li').length)) {
		$('.breadcrumbs ul').prepend('<li><a href="/">Главная</a></li><i class="icon ic-arrow-left"></i>');
	}
	// END main breadcrumb

	$('.payment-button-reserves-container .payment-button').click(function() {
		var btn = $(this);
		if (!btn.hasClass('inprogress')) {
			if (btn.hasClass('platron-button')) {
				return true;
			} else if (btn.hasClass('rfi-button')) {
				var documentNumber = $(this).parent().attr('data-document-number');
				var documentAmount = getOrderAmount(documentNumber);
				if (0 < documentAmount) {
					var form = $('#rfi-pay-form-' + documentNumber);
					form.find('input[name="cost"]').val(documentAmount);
					form.submit();
				}
			}
		}
		return false;
	});
	$('.payment-button-reserves-container .payment-button-tinkoff').click(function() {
		var btn = $(this);
		if (!btn.hasClass('inprogress')) {
			var documentNumber = $(this).parent().attr('data-document-number');
			var documentAmount = getOrderAmount(documentNumber);
			if (0 < documentAmount) {
				var form = $('#TinkoffPayForm-' + documentNumber);
				form.find('input[name="amount"]').val(documentAmount);
				form.submit();
			}
		}
		return false;
	});

	if (0 < $('.payment-button-reserves-container .payment-loader').length) {
		$('.payment-button-reserves-container').each(function() {
			var container = $(this);
			if (0 < container.find('.payment-loader').length) {
				checkOrderCompleted(container);
			}
		});
	}

	$('.payment-button-reserve-container .payment-button').click(function() {
		var btn = $(this);
		if (!btn.hasClass('inprogress')) {
			if (btn.hasClass('platron-button')) {
				return true;
			} else if (btn.hasClass('rfi-button')) {
				var form = $('#rfi-pay-form');
				var documentNumber = form.attr('data-number');
				var documentAmount = getOrderAmount(documentNumber);
				if (0 < documentAmount) {
					form.find('input[name="cost"]').val(documentAmount);
					form.submit();
				}
			}
		}
		return false;
	});

	$('.payment-button-reserve-container .payment-button-tinkoff').click(function() {
		var btn = $(this);
		if (!btn.hasClass('inprogress')) {
			var form = $('#TinkoffPayForm');
			var documentNumber = form.attr('data-number');
			var documentAmount = getOrderAmount(documentNumber);
			if (0 < documentAmount) {
				form.find('input[name="amount"]').val(documentAmount);
				form.submit();
			}
		}
		return false;
	});

	if (0 < $('.payment-button-reserve-container .payment-loader').length) {
		$('.payment-button-reserve-container').each(function() {
			var container = $(this);
			if (0 < container.find('.payment-loader').length) {
				var documentNumber = container.attr('data-document-number');
				var processing = false;
				var timerId = setInterval(function() {
					if (!processing) {
						processing = true;
						$.ajax({
							url: '/ordering/check-completed/',
							data: { number: documentNumber },
							dataType: 'json',
							method: 'get',
							success: function(data) {
								processing = false; 
								if (data.completed) {
									clearInterval(timerId);
									$('#rfi-pay-form input[name="cost"]').val(data.amount);
									container.find('.payment-button').removeClass('inprogress');
									container.find('.payment-loader').hide();

									$('#TinkoffPayForm input[name="amount"]').val(data.amount);
									container.find('.payment-button-tinkoff').removeClass('inprogress');
									container.find('.payment-loader').hide();
								}
							}
						});
					}
				}, 3000);
			}
		});
	}

	$('#footer-catalog li a.more').click(function(e) {
		var t = $(this);
		t.closest('ul').find('li.partially-hidden').each(function() {
			$(this).slideToggle('fast');
		});
		if ('показать еще' == t.html()) {
			t.html('свернуть');
		} else {
			t.html('показать еще');
		}
		e.preventDefault();
	});
	
	$('form[name="OptForm"]').submit(function() {
		var $this = $(this);
		var email = $this.find('input[name="OFC4_Optin_OptinField_EMail"]').val();
		if (email) {
			(window["rrApiOnReady"] = window["rrApiOnReady"] || []).push(function() { RRHelper.setEmail(email); RRHelper.welcomeSequence(email); });
			$.ajax({
				url: '/subscribe/confirm/',
				method: 'get',
				data: { email: email },
				success: function(data) {
					redirect('/mail_123/thanks.html');
				}
			});
		}
		return false;
	});

	$('#cities .clearstyle a').click(function(e) {
		var link = $(this);
		if ('#' != link.attr('href')) {
			redirect(link.attr('href'), '', 2421);
		} else {
			selectCity(link.attr('data-code'), link.attr('data-return-url'));
		}
		return false;
	});

	$('input[name="search-city"]').autocomplete({
		source: '/geo/suggest/',
		minLength: 2,
		select: function(event, ui) {
			if (ui.item.code) {
				selectCity(ui.item.code, ui.item.returnUrl);
			}
		}
	});

	$('#form-callback .btn-submit').click(function() {
		submitForm('#form-callback', {
			success: function(data) {
				data.form.hide();
			}
		});
		return false;
	});

	$('#form-content-abuse .btn-submit').click(function() {
		submitForm('#form-content-abuse', {
			success: function(data) {
				data.form.hide();
			}
		});
		return false;
	});

	$('#form-abuse-footer .btn-submit').click(function() {
		submitForm('#form-abuse-footer', {
			success: function(data) {
				data.form.hide();
			}
		});
		return false;
	});

	$('#form-abuse .btn-submit').click(function() {
		submitForm('#form-abuse', {
			success: function(data) {
				data.form.find('.fields').hide();
			}
		});
		return false;
	});

	$('#form-callback-footer .btn-submit').click(function() {
		submitForm('#form-callback-footer', {
			success: function(data) {
				data.form.hide();
			}
		});
		return false;
	});

	$('#form-poll .btn-submit').click(function() {
		submitForm('#form-poll', {
			success: function(data) {
				data.form.hide();
			}
		});
		return false;
	});

	$('#form-login .btn-submit').click(function() {
		submitForm('#form-login', {
			success: function(data) {
				RRHelper.setEmail(data.formData.login);
				refreshPage();
			}
		});
		return false;
	});
	$('#form-login').submit(function() {
		$('#form-login .btn-submit').trigger(clickHandler);
		return false;
	});

	$('#form-register .btn-submit').click(function() {
		submitForm('#form-register', {
			success: function(data) {
				RRHelper.setEmail(data.formData.email);
				RRHelper.welcomeSequence(data.formData.email);
				refreshPage();
			}
		});
		return false;
	});
    $('#registration .btn-submit').click(function() {
		submitForm('#form-order-register', {
			success: function(data) {
				RRHelper.setEmail(data.formData.email);
				RRHelper.welcomeSequence(data.formData.email);
				loginFormDone(data);
			}
		});
		return false;
	});
	$('#form-register').submit(function() {
		$('#form-register .btn-submit').trigger(clickHandler);
		return false;
	});

	$('.recovery .link').click(function() {
		$('.recovery').addClass('inprogress');
		$('.recovery .recovery-success').hide();
		$('.recovery .recovery-errors').hide();
		$.ajax({
			url: '/recovery/',
			method: 'post',
			data: { phone: $('#form-login input[name="login"]').val() },
			dataType: 'json',
			success: function(data) {
				if (data.success) {
					$('.recovery .recovery-success').show();
					$('#form-login input[name="password"]').focus();
				} else {
					$('.recovery .recovery-errors').html(data.errorMessage).show();
				}
			},
			complete: function(data) {
				$('.recovery').removeClass('inprogress');
			},
			error: function(data) {
				$('.recovery .recovery-errors').show();
			}
		});
		return false;
	});

	$('#form-cabinet-profile .btn-submit').click(function() {
		submitForm('#form-cabinet-profile', {
			success: function(data) {
				// do nothing
			}
		});
		return false;
	});
	$('#form-cabinet-profile').submit(function() {
		$('#form-cabinet-profile .btn-submit').trigger(clickHandler);
		return false;
	});

	$('#form-change-password .btn-submit').click(function() {
		submitForm('#form-change-password', {
			success: function(data) {
				// do nothing
			}
		});
		return false;
	});
	$('#form-change-password').submit(function() {
		$('#form-change-password .btn-submit').trigger(clickHandler);
		return false;
	});

	$('#form-add-tracking .btn-submit').click(function() {
		submitForm('#form-add-tracking', {
			success: function(data) {
				data.form.hide();
				$('.footbar-tab.tracked .cl-blue').html(data.count);

				var _ = $('.pc-follow a');
            _.find('span').html(_.data('text_' + (_.hasClass('active') ? 1 : 2)));
            _.toggleClass('active');
            _.find('i.icon').toggleClass('ic-eye-close').toggleClass('ic-eye-open');
			}
		});
		return false;
	});
	$('#form-add-tracking').submit(function() {
		$('#form-add-tracking .btn-submit').trigger(clickHandler);
		return false;
	});

	$('#switch-to-desktop').click(function() {
		set_cookie('show_desktop', 1);
		refreshPage();
	});

	$('.filter-box.actions').on(clickHandler, '.__btnResetFilters', function(e) {
		e.stopPropagation();

		var _ = $(this);
		if (_.data('link')) {
			loadProducts(_.data('link'), { scrollOnTop: true, element: _ });
		}

		return false;
	});

	if (exists('.pagination-mode .btn')) {
		var mode = $('.pagination-mode .btn').hasClass('active') ? 'infinite' : 'pages';
		initCatalogScroll(mode);
	}

	if (exists('.order-tooltip')) {
		  $(".order-tooltip").powerTip({
				placement: 'ne',
				manual: true
			});
			$('.order-tooltip').on('click', function() {
				$.powerTip.toggle(this);
				return false;
			});
	}

	$('#userinfo .btn-submit').click(function() {
		$('#userinfo [name="message"]').select();
		document.execCommand('copy');
	});

	loadAccessoriesPopups();

});



function checkOrderCompleted(container) {
	var documentNumber = container.attr('data-document-number');
	var processing = false;
	var timerId = setInterval(function() {
		if (!processing) {
			processing = true;
			$.ajax({
				url: '/ordering/check-completed/',
				data: { number: documentNumber },
				dataType: 'json',
				method: 'get',
				success: function(data) {
					processing = false; 
					if (data.completed) {
						clearInterval(timerId);
						$('#rfi-pay-form-' + documentNumber + ' input[name="cost"]').val(data.amount);
						container.find('.payment-button').removeClass('inprogress');
						container.find('.payment-loader').hide();

						$('#TinkoffPayForm-' + documentNumber + ' input[name="amount"]').val(data.amount);
						container.find('.payment-button-tinkoff').removeClass('inprogress');
						container.find('.payment-loader').hide();
					}
				}
			});
		}
	}, 3000);
}

function initCatalogScroll(mode) {
	var w = $(window);
	var marker = $('.mp-link-add');
	var nextPageUrl = marker.attr('data-next-page');
	if (nextPageUrl && ('#' != nextPageUrl)) {
		w.unbind('scroll.loop').bind('scroll.loop', function() {
			if ('infinite' == mode) {
				if ((0 < marker.length) && ((w.scrollTop() + w.height() - 100) >= marker.offset().top)) {
					w.unbind('scroll.loop');
					loadProducts(nextPageUrl, {
						appendData: true,
						scrollOnTop: false,
						success: function(data) {
							initCatalogScroll(mode);
						}
					});
				}
			}
		});
	}
}

function showBasketPreview(product) {
	if (isMobileDevice()) {
		return false;
	}
	var wndWidth = parseInt($(window).width());
	if (800 > wndWidth) {
		return false;
	}

	var wnd = $('#basket-preview');

    if (typeof(product.ratingHTML)=="undefined") product.ratingHTML="";
    if (typeof(product.oldPrice)=="undefined") product.oldPrice=2e4;
    if (typeof(product.discountPercent)=="undefined") product.discountPercent=5;
    if (typeof(product.discountAmount)=="undefined") product.discountAmount=2e3;
	wnd.find('.window-cart-items').html('<li><a href="'+product.url+'" class="-img"><img src="'+product.image+'"></a><div class="info"><div class="title"><a href="'+product.url+'">'+product.name+'</a></div><div class="-art-rating"><div class="vendor-code">Арт: '+product.id+'</div><div class="sp-listing-inline-rating-widget">' + product.ratingHTML + '</div></div><div class="price c-blue"><span>'+format_price(product.price)+'</span><em>'+format_price(product.oldPrice)+'</em><p class="-discount">Скидка -'+product.discountPercent+'% <b>('+product.discountAmount+' р.)</b></p></div><div class="-cntboxwrp"><div class="availability ' + (product.isPrepay ? 'orange' : 'green') + '">' + (product.isPrepay ? 'Под заказ' : 'В наличии') + '</div><div class="countbox" data-product-id="' + product.id + '"><a href="#" class="minus"></a><input type="text" name="count" value="1" data-min="1" data-max="' + product.maxQuantity + '" data-hold="true" data-price="' + product.price + '"><a href="#" class="plus"></a></div></div></div></li>');
	if (product.discountAmount==0) wnd.find(".-discount").hide();
//	wnd.find('.window-cart-items').html('<li><a href="' + product.url + '"><img src="' + product.image + '"></a><div class="info"><div class="title"><a href="' + product.url + '">' + product.name + '</a></div><div class="vendor-code">Арт: ' + product.id + '</div><div class="availability ' + (product.isPrepay ? 'orange' : 'green') + '">' + (product.isPrepay ? 'Под заказ' : 'В наличии') + '</div><div class="price c-blue"><span>' + format_price(product.price) + '</span> <span class="rub">a</span></div><div class="countbox" data-product-id="' + product.id + '"><a href="#" class="minus"></a><input type="text" name="count" value="1" data-min="1" data-max="' + product.maxQuantity + '" data-hold="true" data-price="' + product.price + '"><a href="#" class="plus"></a></div></div></li>');	

	var recBlock = wnd.find('.recommendations');
	recBlock.show();
	if (1 == recBlock.attr('data-reload')) {
		recBlock.html('');
		recBlock.addClass('loading');
	}
	if (recBlock.hasClass('loading')) {
		$.ajax({
			url: '/basket/recommendations/',
			method: 'get',
			data: { product: product.id },
			dataType: 'json',
			success: function(data) {
				recBlock.removeClass('loading');
				if (data.success) {
					recBlock.html(data.content);
					initializeBasketPreview();
				} else {
					recBlock.hide();
				}
			},
			complete: function(data) {
				recBlock.removeClass('loading');
			},
			error: function(data) {
				recBlock.removeClass('loading');
				recBlock.hide();
			}
		});
	}

	wnd.window('open', {
		background: true,
		responsive: true
	}, function() {
		var scrollTop = $(window).scrollTop();
		var wndHeight = wnd.height();
		var viewportHeight = $(window).height();
		var offsetTop = Math.ceil(scrollTop + ((viewportHeight - wndHeight) / 3));
		wnd.css('top', offsetTop + 'px');
		if (!wnd.hasClass('initialized')) {
			initializeBasketPreview();
			wnd.addClass('initialized');
		}
	});
}

function initializeBasketPreview() {
	$('#basket-preview .responsive-products').each(function(){
		var _ = $(this);
		/*
		if (_.hasClass('basket-preview-accessories')) {
			_.find('.slick-wrapper:visible').slick({
				 infinite: false,
				 slidesToShow: 4,
				 slidesToScroll: 1,
				 draggable: false,
				 prevArrow: _.find('.p-slick-control-prev'),
				 nextArrow: _.find('.p-slick-control-next')
			});
		}
		*/
  });

  $('#basket-preview .search-tags a').each(function() {
		var _ = $(this);
		var groupId = _.attr('data-group-id');
		if (groupId || _.parent().hasClass("group")) {
	   		_.on("click", function(e) {
		    	e.preventDefault();
			    if (_.parent().hasClass("group")) {
    			    _.parents("li.group").toggleClass("expanded");
    			    if (_.parents("li.group").hasClass('expanded')) {
    			        _.parent().find("ul").show(200);
    			    } else {
    			        _.parent().find("ul").hide(200);
    			    }
			    } else {
    				var wp = $('.acc-product-items.for-basket-acc-group-' + groupId + ' .slick-wrapper');
    				if (0 < wp.length) {
    					$('#basket-preview .search-tags a').removeClass('active');
    					_.addClass('active');
    
    					$('#basket-preview .p-slick-control').hide();
    
    					$('#basket-preview .acc-product-items').hide();
    					$('#basket-preview .slick-dots-wrapper').hide();
    					$('#basket-preview .for-basket-acc-group-' + groupId).show();
    
    					//initBasketPreviewSlick(wp, groupId);
    					$('#basket-preview .pi-hover-wrapper').html('');
    				}
                }
    			return false;
			});
		}
  });
}

function initNano() {
	$('.nano').each(function() {
		var _ = $(this);
		if (!_.hasClass('initialized')) {
			_.addClass('initialized');
			_.nanoScroller({
				iOSNativeScrolling : false,
				sliderMinHeight : 30,
				preventPageScrolling : true,
				alwaysVisible : true
			});
		}
	});
}

function renderRRBlock2(data, renderFn) {
	data = data || [];
	console.log('----------------- RR block -----------------------');
	console.log(data);
	console.log('----------------- END RR block -----------------------');
	renderFn(data);
}

$(document).ready(function() {
	$('.mobile-site-menu').on('click', '.catalog a', function() {
		$('.catalog-menu .close').click();
		$('.catalog._mobile .catalog__btn').click();
		return false;
	});
	$('.catalog-link-container a').click(function() {
		$('.mobile-site-menu .catalog a').click();
		return false;
	});
});

function initializeShoppilot() {
	if (window._shoppilot) {
		console.log('shoppilot', 'init', 'start');

		window._ShoppilotAPI = $.Deferred();

		_shoppilot.push(['_setOnReady', function (Shoppilot) {
			window._ShoppilotAPI.resolve(Shoppilot);
		}]);

		var store_id = window._shoppilotParams.store_id;
		var theme_id = window._shoppilotParams.theme_id;

		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.async = true;
		script.src = '//get.shoppilot.ru/f/v1/' + store_id + '/' + theme_id + '/app.js';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(script, s);

		console.log('shoppilot', 'init', 'end');
	}
}

function initLozad() {
	const observer = lozad('.lazy-img', {
		 threshold: 0.1
	});
	observer.observe();
}



// FOR ACCESSORIES POPUP (START)
function initAccessoriesPopup(identifier) {
	var popup = $(identifier);

	console.log('izi', 'start', identifier);
	try {
		popup.iziModal({
			width: 860,
			autoOpen: false,
			overlay: true,
			radius: 0,
			focusInput: false,
			onOpening: function onOpening(modal) {
				var categoriesBlock = popup.find('.list-product');
				var productsBlock = popup.find('.product-list');

				if (categoriesBlock.hasClass('loading')) {
					$.ajax({
						url: '/ordering/accessories/categories/',
						data: { product: popup.attr('data-product-id') },
						dataType: 'json',
						success: function(data) {
							if (data.success) {
								categoriesBlock.html(data.content);

								$('.list-product .show-all', popup).click(function () {
									$('ul.show-more', popup).addClass('open');
								});

								categoriesBlock.find('li a').click(function() {
									var _ = $(this);
									var link = _.attr('data-group-link');
									var id = _.attr('data-group-id');
									if (link && id) {
										loadAccessoriesPopupProducts(popup.attr('data-product-id'), link, id, function() {
											categoriesBlock.find('a').removeClass('active');
											_.addClass('active');
										});
										return false;
									}
								});
								categoriesBlock.find('a.active').click();
							}
						},
						complete: function(data) {
							categoriesBlock.removeClass('loading');
						},
						error: function(data) {
							categoriesBlock.removeClass('loading');
							productsBlock.removeClass('loading');
						}
					});
				}

				//popup.find('.range').removeClass('inited');
				//rangeInputsInit();
			}
		});

		$('.filter-btn', popup).click(function () {
			popup.addClass('open');

			// IF CLICK OUTSIDE (START)  *******  INIT 
			popup.mousedown(function (e) {
				var div = $(this); // ID or class element
				var menuButton = $('.aside-filter', popup);
				if (popup.hasClass('open') && menuButton.has(e.target).length === 0 && !menuButton.is(e.target)) {
					// and not by it's child elements
					popup.removeClass('open').unbind('mousedown');
				}
			});
			// IF CLICK OUTSIDE (END) *******  INIT
		});

		$('.close-popup', popup).click(function () {
			popup.iziModal('close');
		});
	} catch (e) {
		console.log('izi', 'error', e);
	}

	console.log('izi', 'stop', identifier);
}
// FOR ACCESSORIES POPUP (END)

function loadAccessoriesPopupProducts(productId, link, id, successFunc) {
	window.currentAccId = id;

	try {
	var blockProducts = $('#accessories-popup-' + productId + ' .product-list');
	var blockSidebar = $('#accessories-popup-' + productId + ' .sidebar');
	var blockSorting = $('#accessories-popup-' + productId + ' .sort-place');
	var blockPagination = $('#accessories-popup-' + productId + ' .pagination-place');

	blockProducts.addClass('loading').find('.load-list-result').html('');
	blockSidebar.html('');
	blockSorting.find('.toolbar').html('');
	blockPagination.html('');

	$.ajax({
		url: link,
		method: 'get',
		data: { popup: 1 },
		dataType: 'json',
		success: function(data) {
			blockProducts.find('.load-list-result').html(data.products);
			blockSidebar.html(data.sidebar);
			blockSorting.find('.toolbar').html(data.sorting);
			blockPagination.html(data.pagination);

			blockSidebar.find('.spoiler.collapsed').each(function () {
				spoilerToggle($(this), true, 1);
			});
			rangeInputsInit();
			showHiddenHtml();
			initializeProductsProperties();
			initLozad();

			successFunc();

			//refreshAccessoriesCategories();
		},
		complete: function() {
			blockProducts.removeClass('loading');
		},
		error: function() {
			blockProducts.removeClass('loading');
		}
	});
	} catch (e) {console.log(e);}
}

function loadAccessoriesPopups() {
	$('.more-accessories').each(function() {
		var _ = $(this);
		var identifier = _.attr('data-izimodal-open');
		initAccessoriesPopup(identifier);
	});
}

function updateOrderProducts(productId) {
	try {
	var itemsBlock = $('#form-order .cart-items');
	var totalBlock = $('#form-order .promo-total');
	itemsBlock.addClass('loading').html('');
	totalBlock.html('');
	$.ajax({
		url: '/ordering/products/',
		method: 'get',
		data: {},
		dataType: 'json',
		success: function(data) {
			if (data.success) {
				itemsBlock.html(data.products);
				totalBlock.html(data.total);

				loadAccessoriesPopups();
			}
		},
		complete: function(data) {
			itemsBlock.removeClass('loading');
		},
		error: function(data) {
			itemsBlock.removeClass('loading');
		}
	});
	} catch (e) {
		console.log('updateOrderProducts', 'error', e);
	}
}


function refreshCategoryTitle() {
	if (exists('.container.catalog-header')) {
		var _ = $('.container.catalog-header h1');
		_.html(_.data('full-content'));
	}
}

function updateHeaderMenu() {
	$('.catalog__content .leaf a').each(function() {
		var _ = $(this);
		_.html(_.html() + '&nbsp;<span>' + _.data('products-count') + '</span>');
	});
}


function updateProductsDescriptionProperties() {
	if (exists('.properties-source') && exists('.properties-target')) {
		var descripitonTabProperties = '';
		var index = 0;
		$('.properties-source .prop-desc').each(function() {
			var _ = $(this);
			descripitonTabProperties += '<div class="row">' + _.html() + '</div>';
		});
		$('.properties-target').html(descripitonTabProperties);
	}
}


function initGenericSlick() {
	var arrowLeft = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 10 17" style="enable-background:new 0 0 10 17;" xml:space="preserve"><path style="fill:#2673C2;" d="M4.2,8.7l5.3,5.4c0.6,0.7,0.6,1.7,0,2.4c-0.6,0.6-1.7,0.7-2.3,0c0,0,0,0,0,0 L0.5,9.7c-0.6-0.7-0.6-1.7,0-2.4l6.7-6.8c0.6-0.6,1.7-0.7,2.3,0c0,0,0,0,0,0c0.6,0.7,0.6,1.7,0,2.4L4.2,8.2"/></svg>';
	var arrowRight = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 9.9 16.8" style="enable-background:new 0 0 9.9 16.8;" xml:space="preserve"><path style="fill:#2673C2;" d="M5.7,8.7L0.5,14c-0.6,0.7-0.6,1.7,0,2.3c0.6,0.6,1.6,0.7,2.3,0c0,0,0,0,0,0 l6.6-6.7c0.6-0.7,0.6-1.7,0-2.3L2.8,0.5c-0.6-0.6-1.6-0.7-2.3,0c0,0,0,0,0,0c-0.6,0.7-0.6,1.7,0,2.3l5.2,5.3"/></svg>';
	
	$('.js-generic-slick:not(.initialized)').each(function() {
        var sts = [ 5, 4, 2 ];
		var _ = $(this);
        if (_.hasClass('-similar')) sts = [ 4, 3, 1 ];
		_.closest('.product__generic_slick').show();
		_.slick( {
			slidesToShow: sts[0],
			slidesToScroll: 1,
			dots: false,
			arrows: true,
			prevArrow: '<span class="_arrow _prev">' + arrowLeft + '</span>',
			nextArrow: '<span class="_arrow _next">' + arrowRight + '</span>',
			responsive: [
				{
					breakpoint: 1199,
					settings: {
						slidesToShow: 3,
						variableWidth: true,
            			arrows: true,
            			dots: false
					}
				},
				{
					breakpoint: 899,
					settings: {
						slidesToShow: 2,
						variableWidth: true,
						dots: false,
						arrows: true
					}
				},
				{
					breakpoint: 767,
					settings: {
						slidesToShow: sts[2],
						variableWidth: true,
						dots: true,
						arrows: false
					}
				}
			]
		});

		_.addClass('initialized');
		$(window).trigger("resize");
	});
	
    $(".-brand-links").slick({
      dots: false,
      speed: 100,
      slidesToShow: $(".-brand-links a").length-1,
      arrows: true,
      prevArrow: '<span class="_arrow _prev">' + arrowLeft + '</span>',
      nextArrow: '<span class="_arrow _next">' + arrowRight + '</span>',
      centerMode: false,
      variableWidth: true,
      infinite: true,
      responsive: [
				{
					breakpoint: 759,
					settings: {
						slidesToShow: 1,
						variableWidth: true,
						dots: false,
						arrows: false,
						centerMode: true
					}
				}
			]
    });
    
	initLozad();
}