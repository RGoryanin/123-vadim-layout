$(document).ready(function() {
	CriteoHelper.init();
});

var CriteoHelper = {

	init: function() {
		if (0 < $('#criteo-data').length) {
			this.dataContainer = $('#criteo-data');
		}
	},

	addToBasket: function(products) {
		var items = [];
		for (var i = 0; i < products.length; i++) {
			var product = products[i];
			var productId = product.id;
			if (this.getProductPrefix()) {
				productId = this.getProductPrefix() + '.' + productId;
			}
			var item = {
				id: productId,
				price: product.price,
				quantity: product.quantity
			};
			items.push(item);
		}
		var obj = {
			event: 'viewBasket',
			items: items
		};
		this.sendDataObject(obj);
	},

	postCheckout: function(orderId, orderAmount, products) {
		var items = [];
		for (var i = 0; i < products.length; i++) {
			var product = products[i];
			var productId = product.id;
			if (this.getProductPrefix()) {
				productId = this.getProductPrefix() + '.' + productId;
			}
			var item = {
				id: productId,
				price: product.price,
				quantity: product.quantity
			};
			items.push(item);
		}
		var obj = {
			event: 'trackTransaction',
			id: orderId,
			item: items
		};
		this.sendDataObject(obj);
	},

	getAttr: function(attrName) {
		return this.dataContainer.attr('data-' + attrName);
	},

	getProductPrefix: function() {
		return this.getAttr('product-prefix');
	},

	sendDataObject: function(dataObject) {
		console.log('criteo push', dataObject);
		window.criteo_q = window.criteo_q || []; window.criteo_q.push(dataObject);
	}

};