<?php
    $_rnd = rand(1e8,1e9);
?><!DOCTYPE HTML>
<html prefix="og: http://ogp.me/ns#">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="/libs/jquery-ui-1.12.1.custom/jquery-ui.min.css"></link>
    <link rel="stylesheet" type="text/css" href="/css/style.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/style.v2.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/style.new.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/icons.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/pickup-window-2.css?rnd=<?php echo $_rnd; ?>"></link>
    <script type="text/javascript" src="/js/jquery-3.1.1.js"></script>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script type="text/javascript" src="/js/shop-selector-2.js"></script>
</head>

<body data-is-iml-city="0" data-is-sdek-city="0" class="loading">
    <header>
        <div class="filters-block">
            <a href="#" class="filter-switcher blue-button">Фильтр</a>
            <div class="search-shop">
                <input type="text" placeholder="Поиск по 100 пунктам"><a href="#" class="-search blue-button">Найти</a>
            </div>
        </div>
        <div class="map-type">
            <ul>
                <li><input type="radio" name="map-type-radio" checked id="map-type-1" data-block="#map-view"><label for="map-type-1">
                        <b>Карта</b>
                    </label></li>
                <li><input type="radio" name="map-type-radio" id="map-type-2" data-block="#list-view"><label for="map-type-2">
                        <b>Список</b>
                    </label></li>
                <li><input type="radio" name="map-type-radio" id="map-type-3" data-block="#metro-view"><label for="map-type-3">
                        <b>Метро</b>
                    </label></li>
            </ul>
        </div>
    </header>
    
    <main>
        <div class="map-type-switcher">
            <a href='#' class='selected' data-id='#map-view'>Карта</a>
            <a href='#' data-id='#metro-view'>Метро</a>
        </div>
        <section id="map-view" class="active" data-center="[55.753994,37.622093]" data-zoom="13">
        </section>
        <section id="metro-view"></section>
        <section id="new-filters">
            <ul class='-f-el'>
                <li><input type="checkbox" id='-filter-1' checked value='zakazberry'><label for='-filter-1'>
                    <img src='/i/carriers/zakazberry.png'> Zakazberry
                </label></li>
                <li><input type="checkbox" id='-filter-2' value='sdek'><label for='-filter-2'>
                    <img src='/i/carriers/sdek.png'> СДЭК
                </label></li>
                <li><input type="checkbox" id='-filter-3' value='pickpoint'><label for='-filter-3'>
                    <img src='/i/carriers/pickpoint.png'> PickPoint
                </label></li>
                <li><input type="checkbox" id='-filter-4' value='iml'><label for='-filter-4'>
                    <img src='/i/carriers/iml.png'> IML
                </label></li>
                <li><input type="checkbox" id='-filter-5' value='boxberry'><label for='-filter-5'>
                    <img src='/i/carriers/boxberry.png'> Boxberry
                </label></li>
            </ul>
            <div id="view-switcher">
                <a href="#" class="show-list">Показать списком</a>
                <a href="#" class="show-map">Показать на карте</a>
                <a href="#" class="expand-filters">Фильтры</a>
            </div>
        </section>
        <section id="list-view">
            <div class="list-items">
                <ul>
                    <li>
                        <div class="list-view"></div>
                        <div class="full-view"></div>
                    </li>
                </ul>
            </div>
        </section>
    </main>
    
</body>
</html>