var CreditlineWidgetWatcher = {
	
	btn: null,
	condig: {},
	status: 'uninitialized',
	tmr: null,
	disableWindowCloseEvent: false,

	init: function(btnIdentifier, config) {
		this.log('init: start');
		this.log('init: parameters');
		this.log(config);

		this.btn = $(btnIdentifier);
		this.config = config;

		var $this = this;
		this.btn.click(function() {
			$this.status = 'new';
			$this.startTimer();
		});
		this.startTimer();

		$(window).on('beforeunload', function() {
			if (!$this.disableWindowCloseEvent) {
				$this.log('window close');
				if ('first-step' == $this.status) {
					$this.raiseFlowWindowClosed();
				} else {
					$this.raiseWindowClosed();
				}
			}
		});

		this.log('init: end');
	},

	callback: function(status, number) {
		this.log('status: ' + status);
		this.log('number: ' + number);
		if (1 == status) {				// first step successfully passed
			this.status = 'first-step';
			this.stopTimer();
		} else if (0 == status) {		// user closed
			this.status = 'widget-closed';
			this.raiseCancelled();
			this.stopTimer();
		} else if (-1 == status) {		// idle
			if ('new' == this.status) {
				this.raiseFirstStepIdle();
			} else {
				this.raiseFlowIdle();
			}
			this.status = 'idle';
			this.stopTimer();
		}
	},

	startTimer: function() {
		this.stopTimer();

		this.log('timer: start');
		var $this = this;
		var time = (15 * 60 * 1000);
		this.tmr = window.setTimeout(function() {
			$this.log('timer: raise');
			$this.raiseFirstStepIdle();
			return;
			if ('uninitialized' == $this.status) {
				$this.raiseWidgetFailed();
			} else if ('new' == $this.status) {
				$this.raiseFirstStepIdle();
			} else {
				$this.raiseFlowIdle();
			}
		}, time);
	},

	stopTimer: function() {
		if (this.tmr) {
			this.log('timer: stop');
			window.clearTimeout(this.tmr);
			this.tmr = null;
		}
	},

	raiseWidgetFailed: function() {
		this.log('raise: widget failer');
		this.disableWindowCloseEvent = true;
		window.location.href = this.config.widgetFailedUrl;
		return false;
	},

	raiseCancelled: function() {
		this.log('raise: cancelled');
		this.disableWindowCloseEvent = true;
		window.location.href = this.config.cancelledUrl;
		return false;
	},

	raiseWindowClosed: function() {
		this.log('raise: window closed');
		$.ajax({
			method: 'get',
			url: this.config.windowClosedUrl,
			data: {}
		});
		return false;
	},

	raiseFirstStepIdle: function() {
		this.log('raise: first step idle');
		this.stopTimer();
		this.disableWindowCloseEvent = true;
		window.location.href = this.config.firstStepIdleUrl;
		return false;
	},

	raiseFirstStepIdleYes: function() {
		this.log('raise: first step idle yes');
		this.stopTimer();
		this.disableWindowCloseEvent = true;
		window.location.href = this.config.firstStepIdleYesUrl;
		return false;
	},

	raiseFirstStepIdleNo: function() {
		this.log('raise: first step idle no');
		this.stopTimer();
		this.disableWindowCloseEvent = true;
		window.location.href = this.config.firstStepIdleNoUrl;
		return false;
	},

	raiseFirstStepSuccess: function() {
		this.stopTimer();
		this.disableWindowCloseEvent = true;
		window.location.href = this.config.firstStepSuccessUrl;
		return false;
	},

	raiseFlowIdle: function() {
		this.log('raise: flow idle');
		this.stopTimer();
		this.disableWindowCloseEvent = true;
		window.location.href = this.config.flowIdleUrl;
		return false;
	},

	raiseFlowWindowClosed: function() {
		this.log('raise: flow window closed');
		this.stopTimer();
		$.ajax({
			method: 'get',
			url: this.config.flowWindowClosedUrl,
			data: {}
		});
		return false;
	},

	log: function(message) {
		console.log('creditline', message);
	}

}