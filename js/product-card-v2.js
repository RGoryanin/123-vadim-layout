$(function () {

    window.setIframeHeight = function(hg) {
        if (hg==0) hg = ""; else hg+="px";
        $(".tabs-viewport iframe.delivery-frame").css({ maxHeight: hg });
        return true;
    }
    
    $(".-similar .line>*").each(function() {
        var w = $(this).width();
        $(this).html("<div>"+$(this).html()+"</div>");
        $(this).find("div").css({width: w+"px"});
    });
    if ($(".pc-buy-block .sep01 .mobile-flex-group").length==1) {
        $(".pc-buy-block .sep01").addClass("-single");
    }

    
    $(".pc-wrapper-header-mobile h1, .pc-wrapper-middle h1").on("click", function() {
        $(this).toggleClass("expanded");
    }).each(function() {
        var t = $(this).html().replace(/([^A-Za-z0-9А-Яа-я\s])/g, '$1&#x200B;');
        $(this).html(t);
        if ($(this).outerHeight()>=this.scrollHeight-4){
            $(this).addClass("disable-toggle");
        };
    });
    $(".toggle-chars>li>ul li.plus").on("click", function() {
        $(this).toggleClass("minus");
        if ($(this).hasClass("minus")) {
            $(this).parent().find("li.hidden").removeClass("hidden");
        } else {
            var bz = $(this).parent().find("li.selected");
                bz=bz.add($(this).parent().find("li:not(.selected):not(.plus)").eq(0));
                if (bz.length<2) {
                    bz=bz.add($(this).parent().find("li:not(.selected):not(.plus)").eq(1));
                }
                bz=bz.add($(this).parent().find(".plus"));
            $(this).parent().find("li").not(bz).addClass("hidden");
        }
    });
    $(".toggle-chars>li>ul li:not(.plus)").on("click", function() {
        $(this).parent().find(".selected").removeClass("selected");
        $(this).addClass("selected");
    });

    $(".tabs-viewport #tab-char .short-links a.show-all.-expand").on("click", function(e) {
        e.preventDefault();
        $(this).parents(".tab").find(".-hidden").show('fast').css({ display: "flex" });
        $(this).css({ visibility: 'hidden' });
        return false;
    });
    
    $("#tab-desc a.show-all").on("click", function(e) {
        e.preventDefault();
        $("a.show-chars").trigger("click");
        $("html,body").stop().animate({scrollTop: $("a.show-chars").offset().top - 85}, 150);
    });
    
    $("a.access-link").on("click", function(e) {
        $("#products-list>.-slick-wrapper").slick("refresh");
    });
    
    $("body").on("click", "a.-show-tab", function(e) {
        e.preventDefault();
        $tabs = $('.tabs-list.item-tabs>a');
        var tab = $($(this).data("tab"));
        tab.trigger("click");
        $("html,body").stop().animate({scrollTop: tab.offset().top - 80 }, 150);
        charIX = $tabs.index(tab);
        charPosition = $tabs.eq(charIX).position().left;
        setCharPosition();
        if ($(this).data("tabpart")) {
            $(".delivery-frame")[0].contentWindow.$($(this).data("tabpart")).trigger("click");
        }
        return false;
    });
    
    var charPosition = 0, charIX = 0, _service = false;
    var setCharPosition = function() {
        var sw = $(".tabs-list.item-tabs").outerWidth(),
            w = $(".tabs-list-wrap").width(),
            sl = charPosition,
            bw = $(".tabs-list-wrap a.scroll").eq(0).outerWidth();
        if (sw<=w) return $(".tabs-list-wrap>a.scroll").addClass("disabled");
        $(".tabs-list-wrap>a.scroll").removeClass("disabled")
        
        sw += bw*2;
        if (sl+w>=sw) sl = charPosition = sw-w;
        if (sl<0) sl = 0;

        $(".tabs-list-wrap>a.scroll.left").toggleClass("inactive", sl==0);
        $(".tabs-list-wrap>a.scroll.right").toggleClass("inactive", sl+w>=sw);

        var cps = -1*sl;
            cps += bw;
        $(".tabs-list.item-tabs").css({ transform: "translateX("+cps+"px)" }).data("-trx", cps);
    }
    
    
    var tldx = 0, $trrt = $(".pc-tabs-wrapper .tabs-list");
    $trrt.on("touchstart", function(e) {
        tldx = e.originalEvent.touches[0].screenX;
    }).on("touchmove", function(e) {
        var dx = $trrt.data("-trx") + e.originalEvent.touches[0].screenX - tldx;
        if (dx>40) dx = 40;
        if (dx<($trrt.width()-$(".tabs-list-wrap").width()-40)*-1) dx = -1*($trrt.width()-$(".tabs-list-wrap").width()-40);
        $trrt.css({ transform: "translateX("+dx+"px)" });
    }).on("touchend", function(e) {
        var dx = $trrt.data("-trx") + e.originalEvent.touches[0].screenX - tldx;
        if (dx>40) dx = 40;
        if (dx<($trrt.width()-$(".tabs-list-wrap").width()-40)*-1) dx = -1*($trrt.width()-$(".tabs-list-wrap").width()-40);

        if (dx<20) {
            setCharPosition();
            $(".tabs-list-wrap>a.scroll.left").trigger("click");
        } else if (dx>20) {
            setCharPosition();
            $(".tabs-list-wrap>a.scroll.right").trigger("click");
        } else {
            setCharPosition();
        }
    });

    
    $(".tabs-list a").on("click", function(e) {
        charIX = $(this).parent().find("a").index(this);
        _service = true;
        $(this).parent().parent().find("a.scroll").eq(0).trigger("click");
        _service = false;
    });
    
    var itemOnResize = function() {
        setCharPosition();
        if ($(window).width()<760) {
            $("#tab-desc .rst-icons").appendTo("#tab-desc aside.right");
            $(".tabs-viewport #tab-char a.show-all.-expand").appendTo("#tab-char aside.left");
            $("#basket-preview-credit.window .window-cart-items .info > .countbox").insertBefore("#basket-preview-credit.window .window-cart-items .info > .price");
        } else if ($(window).width()<=900) {
            $("#tab-desc .rst-icons").appendTo("#tab-desc aside.right");
            $(".tabs-viewport #tab-char a.show-all.-expand").appendTo("#tab-char aside.left");
            $("#basket-preview-credit.window .window-cart-items .info > .countbox").insertAfter("#basket-preview-credit.window .window-cart-items .info > .price");
        } else {
            $("#tab-desc .rst-icons").appendTo("#tab-desc aside.left");
            $(".tabs-viewport #tab-char a.show-all.-expand").appendTo("#tab-char aside.right");
            $("#basket-preview-credit.window .window-cart-items .info > .countbox").insertAfter("#basket-preview-credit.window .window-cart-items .info > .price");
        }
    }
    
    $(".tabs-list-wrap>a.scroll").on("click", function(e) {
        e.preventDefault();
        var step = 1, $tabs = $('.tabs-list.item-tabs>a');
        if (_service) step = 0;
        if ($(this).hasClass("left")) step*=-1;
        charIX+=step;
        if (charIX<0) charIX=0;
        else if (charIX>=$tabs.length) charIX=$tabs.length-1;
        charPosition = $tabs.eq(charIX).position().left;
        setCharPosition();
        return false;
    });
    
    itemOnResize();
    window.addEventListener("resize", itemOnResize);

    $("a.credit-open").on("click", function(e) {
        if ($(window).width()<768) {
            setTimeout(function() {
                $("#basket-preview-credit.window").css({top: $(window).scrollTop() + 10 });
            }, 20);
        } else {
            $("#basket-preview-credit.window").css({top: "" });
        }
    });
    $("a.-fast-buy").on("click", function(e) {
        if ($(window).width()<768) {
            setTimeout(function() {
                $("#pc-quickbuy").css({top: $(window).scrollTop() + 10 });
            }, 20);
        } else {
            $("#pc-quickbuy.window").css({top: "" });
        }
    });
    
    $("#pc-quickbuy .window-cart-items .-promo  a.apply-promo").on("click", function(e) {
        e.preventDefault();
        $.post("/!ajax/cart.php", { a:"apply-promo", promocode: $(".-upd.window .window-cart-items .-promo input").val().trim() } , function(data) {
            if (data.state=="ok") {
                $("#pc-quickbuy .window-cart-items .info > .price > span").text(data.newPrice);
                if (data.newDiscount!="") {
                    $("#pc-quickbuy .window-cart-items .info > .price > .-discount").html(data.newDiscount+" <b>("+data.newDiscountAmount+")</b>");
                    $("#pc-quickbuy .window-cart-items .info > .price > p.-discount").show();
                } else {
                    $("#pc-quickbuy .window-cart-items .info > .price > p.-discount").hide();
                }
                $(".-upd.window .window-cart-items .-promo").addClass("applied").find("input").attr("readonly", true);
            } else return alert(data.message);
        });
        return false;
    });
    $("#pc-quickbuy .window-cart-items .-promo span.ok a").on("click", function(e) {
        e.preventDefault();
        $.post("/!ajax/cart.php", { a:"apply-promo", promocode: 0 } , function(data) {
            if (data.state=="ok") {
                $("#pc-quickbuy .window-cart-items .info > .price > span").text(data.newPrice);
                if (data.newDiscount!="") {
                    $("#pc-quickbuy .window-cart-items .info > .price > .-discount").html(data.newDiscount+" <b>("+data.newDiscountAmount+")</b>");
                    $("#pc-quickbuy .window-cart-items .info > .price > p.-discount").show();
                } else {
                    $("#pc-quickbuy .window-cart-items .info > .price > p.-discount").hide();
                }
                $(".-upd.window .window-cart-items .-promo").removeClass("applied").find("input").attr("readonly", null);
            } else return alert(data.message);
        });
        return false;
    });
    
    $(".-item-actions a").on("click", function(e) {
        e.preventDefault();
        var $this = $(this), type = $(this).data("action"), $id= $(this).data("id");
        $.post("/!ajax/item.php", { a: type, id: $id }, function(data) {
            if (data.state!="ok") return alert(data.message);
            if (typeof(data.selected)!="undefined") {
                if (data.selected) $this.addClass("selected");
                    else $this.removeClass("selected")
                if (typeof(data.button_text)!="undefined") {
                    $this.text(data.button_text);
                }
            }
            if (type=="share") {
                var showDialog = true;
                
                if (navigator.share) {
                    if (location.protocol.match(/https/)) {
                        showDialog = false;
                        navigator.share({
                              title: data.title,
                              text: data.description,
                              url: data.url,
                          })
                        .then(function() { /* shared */ })
                        .catch(function() { /* cancelled or anything error */ });
                    }
                }
                if (showDialog) {

                    // Показ диалога окна "поделиться", если устройство/браузер не поддерживают системный механизм шаринга.

                }
            }
        });
        return false;
    });
    
    $("#pc-productzoom").each(function() {
        var th = $(this).find("aside.-thumbs"), vp = $(this).find(".-viewport"), itm = th.find("li"), $root = $(this);
        for (var i=0; i<itm.length; i++) {
            vp.append("<div class='-one-image'><img src='"+itm.eq(i).data("image")+"'></div>");
        }
        var idx = itm.index(itm.filter(".current"));
        
        
        vp.slick({
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			prevArrow: $(this).find('.-prev'),
			nextArrow: $(this).find('.-next'),
			initialSlide: idx,
        });
        
        vp.on("afterChange", function(e, slick, cs) {
            itm.removeClass("current").eq(cs).addClass("current");
        });
        vp.on("beforeChange", function(e, slick, cs) {
            setTimeout(function() {
                $root.find(".-zoom-nav a.-reset").trigger("click");
            }, 1);
            return true;
        });
        
        itm.on("click", function(e) {
            var ix = itm.index(this);
            vp.slick("slickGoTo", ix);
        });

        $(this).find("a.-zoom").on("click", function(e) {
            e.preventDefault();
            var z = vp.data("zoom") || 1, st = 1;
            var trns = vp.data("trns") || "0,0";
            if ($(this).hasClass("-out")) st*=-1;
            z += st;
            if (z<=1) z=1;
            if (z>10) z=10;
            $(this).parent().find(".-zoom.-in").toggleClass("disabled", z>=10);
            $(this).parent().find(".-zoom.-out, .-reset").toggleClass("disabled", z<=1);
            vp.data("zoom", z);
            var scl = z*.25+.5;
            vp.find(".slick-active, .-zoomed").css({
                "transform": "scale("+scl+") translate("+trns+")"
            }).toggleClass("-zoomed", z!=1);
            return false;
        });
        
        $(this).find("a.-reset").on("click", function(e) {
            e.preventDefault();
            vp.data("zoom", 1).data("trns", "0,0");
            $(this).parent().find(".-zoom.-out").trigger("click");
            return fasle;
        });

        var scren = false, slsx, slsy, cx, cy, mnx, mny, mxx, mxy, z;
        var onmd = function(e) {
            if (typeof(e.touches)!="undefined") {
                e.screenX = e.touches[0].screenX;
                e.screenY = e.touches[0].screenY;
            }
            var trns = vp.data("trns") || "0,0";
            cx = parseInt(trns.replace(/^(.*)\,.*$/, '$1')),
            cy = parseInt(trns.replace(/^.*\,(.*)$/, '$1'));
            z = vp.data("zoom") || 1;
            slsx = e.screenX, slsy = e.screenY;
            var bb1 = $(this).parent()[0].getBoundingClientRect(), bb2 = vp[0].getBoundingClientRect();
            scren = bb1.x<bb2.x || bb1.y<bb2.y;
            if (scren) {
                mxx = bb2.x-bb1.x;
                mxy = bb2.y-bb1.y;
                mnx = bb2.right-bb1.right;
                mny = bb2.bottom-bb1.bottom;
            }
        }
        onmm = function(e) {
            if (typeof(e.touches)!="undefined") {
                e.screenX = e.touches[0].screenX;
                e.screenY = e.touches[0].screenY;
            }
            if ((typeof(e.touches)!="undefined" || e.buttons==1) && scren) {
                e.preventDefault();
                e.stopPropagation();
                var ddx = e.screenX - slsx;
                var ddy = e.screenY - slsy;
                if (ddx>mxx) ddx = mxx;
                if (ddy>mxy) ddy = mxy;
                if (ddx<mnx) ddx = mnx;
                if (ddy<mny) ddy = mny;
                var dx = ddx + cx,
                    dy = ddy + cy;
                var scl = z*.25+.5,
                    trns = dx+"px,"+dy+"px";
                $(this).parent().css({
                    "transform": "scale("+scl+") translate("+trns+")"
                }).toggleClass("-zoomed", z!=1);
                vp.data("trns", trns);
                return false;
            }
        }
        $(".-one-image img")
        .on("mousedown", onmd)
        .on("touchstart", onmd)
        .on("mousemove", onmm)
        .on("touchmove", onmm);
        
    });
    
    $(".pc-gallery-top .pc-gallery-top-wrapper").on("click", function(e) {
        setTimeout(function() {
            $("#pc-productzoom").each(function() {
                $(this).find(".slick-track").each(function() {
                    $(this).css({ "max-height": $(this).parent().height() });
                });
                var vp = $(this).find(".-viewport");
                vp.slick("refresh");
                $(this).find(".slick-track").each(function() {
                    $(this).add($(this).find(".-one-image")).add($(this).find(".-one-image img")).css({ "height": $(this).parent().height() });
                });                
            });
        }, 100);
    });
    
    $(".-create-review").on("click", function(e) {
        e.preventDefault();
        return $(".sp-summary-actions-primary-button").trigger("click");
    });


    $(document).ready(function(){

		if (isMobileDevice() && (exists('#tab-access') || exists('#tab-service'))) {
			var tabAccess = $('#tab-access');
			var tabServices = $('#tab-service');
			$(function () {
				$.ajax({
					url: '/product/accessories2/',
					method: 'get',
					data: { product: tabAccess.attr('data-product-id') },
					dataType: 'json',
					success: function(data) {
						if (data.success) {
							tabAccess.html(data.accessories);
							//tabServices.html(data.services);
							$('.accessories-count').html(data.accessoriesCount);
							//$('.services-count').html(data.servicesCount);
							initSlick();
							initAccessoriesTabs();
						}
					},
					complete: function(data) {
						tabAccess.removeClass('loading');
						//tabServices.removeClass('loading');
					},
					error: function(data) {

					}
				});
			});
		}
		if (!isMobileDevice() && (exists('#tab-access') || exists('#tab-service'))) {
			var tabAccess = $('#tab-access');
			var tabServices = $('#tab-service');
			$(function () {
				$.ajax({
					url: '/product/accessories3/',
					method: 'get',
					data: { product: tabAccess.attr('data-product-id') },
					dataType: 'json',
					success: function(data) {
						if (data.success) {
							tabAccess.html(data.accessories);
							// tabServices.html(data.services);
							$('.accessories-count').html(data.accessoriesCount);
							// $('.services-count').html(data.servicesCount);
							var searchTags = tabAccess.find('.search-tags');
							var searchTagsHtml = searchTags.html();
							searchTags.hide();

							tabAccess.find('.search-tags li:not(.group) a').click(function() {
								var _ = $(this);
								var link = _.attr('data-group-link');
								var id = _.attr('data-group-id');
								loadAccessoriesProducts(link, id, function() {});
								return false;
							});
    						tabAccess.find('.search-tags li.group a').on("click", function(e) {
    						    e.preventDefault();
    						    $(this).parents("li.group").eq(0).toggleClass("expanded");
    						    if ($(this).parents("li.group").eq(0).hasClass('expanded')) {
    						        $(this).parents("li.group").eq(0).find("ul").show("fast");
    						    } else {
    						        $(this).parents("li.group").eq(0).find("ul").hide("fast");
    						    }
    						    return false;
    						});
							tabAccess.find('.search-tags.-side-items .active').click();
						}
					},
					complete: function(data) {
						tabAccess.removeClass('loading');
						//tabServices.removeClass('loading');
					},
					error: function(data) {
						console.log(">>ER", data);
					}
				});
			});
		}

		if (exists('.recommendations-container')) {
			$(function () {
				var container = $('.recommendations-container');
				$.ajax({
					url: '/product/recommendations/',
					method: 'get',
					data: { product: container.attr('data-product-id') },
					dataType: 'json',
					success: function(data) {
						if (data.success) {
							container.html(data.content);
							initSlick();
						}
					},
					complete: function(data) {
						container.removeClass('loading');
					},
					error: function(data) {
						container.hide();
					}
				});
			});
		}

		$('.nedo-tabs-list a').click(function(e) {
			e.stopPropagation();
			e.preventDefault();
			var _ = $(this);
			var href = _.attr('href');
			var target = null;
			if (exists('.pc-tabs-wrapper a[href="' + href + '"]')) {
				target = $('.pc-tabs-wrapper a[href="' + href + '"]');
			}
			if (href) {
				tab = href.replace('#', '');
				if (exists('.pc-tabs-wrapper a[data-tab="' + tab + '"]')) {
					target = $('.pc-tabs-wrapper a[data-tab="' + tab + '"]');
				}
			}
			if (target) {
				$('body,html').stop(true, true).animate(
					{ scrollTop: $('.pc-tabs-wrapper').offset().top },
					300,
					'swing',
					function() {
						target.trigger(clickHandler);
					}
				);
			}
			return false;
		});

		$('.show-all-props').click(function() {
			if (isMobileDevice()) {
				$('.tab-head-320 a[href="#tab-char"]').parent().trigger(clickHandler);
			} else {
				$('body,html').stop(true, true).animate(
					{ scrollTop: $('.pc-tabs-wrapper').offset().top },
					200,
					'swing',
					function() {
						$('.pc-tabs-wrapper .tabs-list a[href="#tab-char"]').trigger(clickHandler);
					}
				);
			}
			return false;
	  });

		if (!isTerminal() && exists('.pc-delivery-data')) {
			var productId = $('.pc-main').attr('data-product-id') || $('.pc-product-main').attr('data-product-id');

			$.ajax({
				type: 'get',
				url: '/product/delivery2/',
				data: { product: productId },
				dataType: 'json',
				success : function(data) {
					if (data.success && data.short) {
						$('.pc-delivery-load').hide();
						$('.pc-delivery-data').html(data.short);
						if (data.extendedPickup) {
							$('#pc-selfdelivery').html(data.extendedPickup);
							initNano();
						}
						if (data.extendedDelivery) {
							$('#pc-delivery').html(data.extendedDelivery);
							initNano();
						}
						if (data.params.hasFreeSalon) {
							$('.pc-wrapper-left .product-action.hidden').removeClass('hidden').show();
						}
					} else {
						$('.pc-delivery-data').html('К сожалению, мы не можем доставить товар в указанный вами город');
					}
				},
				error: function(xhr, status, error) {
					$('.pc-delivery-data').html('К сожалению, мы не можем доставить товар в указанный вами город');
				}
			});
		}

		if (0 <= window.location.search.indexOf('add-new')) {
			$('#poll').window('toggle', { responsize: true, background: true, position: 'center', onOpen: function() { $('#poll').css('left', '0px').css('top', '0px').css('marginLeft', '0px'); } });
			//var left = '0px';
			//$('#poll').css('left', left).css('top', '0px').css('width', '507px').css('marginLeft', '0px');
		}
		if (exists('#pc-notavail')) {
			$('#pc-notavail').window(
				'toggle',
				{
					responsize: true,
					background: true,
					position: 'fixed',
					onOpen: function() {
						// ga track
						var productId = $('#pc-notavail').attr('data-product-id');
						if (productId) {
							var dataLayer = window.dataLayer = window.dataLayer || []
							dataLayer.push({
								event: "banner_out_view",
								eventCategory: "banner_out_view",
								eventAction: productId
							});
							console.log('banner_out_view');

							$('#pc-notavail .product-item').each(function() {
								var slProduct = $(this);
								var slProductId = slProduct.attr('data-product-id');
								slProduct.find('a').click(function() {
									var dataLayer = window.dataLayer = window.dataLayer || []
									dataLayer.push({
										event: "banner_out_click",
										eventCategory: "banner_out_click",
										eventAction: productId,
										eventLabel: slProductId
									});
									console.log('banner_out_click');
								});
							});
						}

						var _ = $('#pc-notavail');
						_.find('.slick-wrapper').slick({
							infinite: false,
							slidesToShow: 3,
							slidesToScroll: 1,
							draggable: false,
							prevArrow: _.find('.p-slick-control-prev'),
							nextArrow: _.find('.p-slick-control-next'),
							responsive: [
								{
									breakpoint: 990,
									settings: {
										slidesToShow: 3,
										slidesToScroll: 1,
									}
								},
								{
									breakpoint: 768,
									settings: {
										slidesToShow: 1,
										slidesToScroll: 1,
										dots: true,
										appendDots: _.find('.slick-dots-wrapper'),
										draggable: true
									}
								}
							]
						});
					},
					onClose: function() {
						// ga track
						var productId = $('#pc-notavail').attr('data-product-id');
						if (productId) {
							var dataLayer = window.dataLayer = window.dataLayer || []
							dataLayer.push({
								event: "banner_out_cross",
								eventCategory: "banner_out_cross",
								eventAction: productId
							});
							console.log('banner_out_cross');
						}
					}
				}
			);
		}
        
        $('.pc-gallery-thumbs .slick-slide:first').addClass('active');
        
        $(document).on(clickHandler, '.tab-head-320 a', function(e){
            e.preventDefault();
        });
        $(document).on(clickHandler, '.pc-mod-compare-320 a', function(e){
            e.stopPropagation();
            e.preventDefault();
        });
        $(document).on(clickHandler, '.pc-follow a', function(e){
            e.preventDefault();
            var _ = $(this);
            _.find('span').html(_.data('text_' + (_.hasClass('active') ? 1 : 2)));
            _.toggleClass('active');
            _.find('i.icon').toggleClass('ic-eye-close').toggleClass('ic-eye-open');
            return false;
        });
		  $(document).on(clickHandler, '.pc-compare a', function(e){
            e.preventDefault();
            var _ = $(this);
            _.find('span').html(_.data('text_' + (_.hasClass('active') ? 1 : 2)));
            _.toggleClass('active');
            _.find('i.icon').toggleClass('ic-compare').toggleClass('ic-compare-active');
            return false;
        });
        $(document).on(clickHandler, '.tab-head-320', function(e){
            e.preventDefault();
            var _ = $(this);
            var $tab = _.find('a').attr('href');
            if($tab){
                $tab = $($tab);
                if($tab.length){
                    $tab.slideToggle(function(){
                        $tab.toggleClass('active');
                    });
                }
					 if ($tab.find('.slick-wrapper').length > 0 && !$tab.hasClass('active')){
                    $tab.find('.slick-wrapper').slick('refresh');
                }
            }
            _.toggleClass('active');
        });
		  initSlick();

			initAccessoriesTabs();
		  function initAccessoriesTabs() {
				$('.pc-accessories .search-tags a').each(function() {
					var _ = $(this);
					var groupId = _.attr('data-group-id');
					if (groupId) {
						_.click(function() {
							var wp = $('.acc-product-items.for-acc-group-' + groupId + ' .slick-wrapper');
							if (0 < wp.length) {
								$('.pc-accessories .search-tags a').removeClass('active');
								_.addClass('active');

								$('.pc-accessories .p-slick-control').hide();

								$('.pc-accessories .acc-product-items').hide();
								$('.pc-accessories .slick-dots-wrapper').hide();
								$('.pc-accessories .for-acc-group-' + groupId).show();

								initAccessoriesSlick(wp, groupId);
								$('.pc-accessories .pi-hover-wrapper').html('');
							}
						});
					}
			  });
		  }
        
        $('.pc-slider-320 .slick-wrapper').slick({
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: true,
            appendDots: $('.pc-slider-320 .slick-dots-wrapper'),
				lazyLoad: 'progressive'
        });
		  $('.pc-slider-320').removeClass('inprogress');

    	var arrowLeft = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 10 17" style="enable-background:new 0 0 10 17;" xml:space="preserve"><path style="fill:#2673C2;" d="M4.2,8.7l5.3,5.4c0.6,0.7,0.6,1.7,0,2.4c-0.6,0.6-1.7,0.7-2.3,0c0,0,0,0,0,0 L0.5,9.7c-0.6-0.7-0.6-1.7,0-2.4l6.7-6.8c0.6-0.6,1.7-0.7,2.3,0c0,0,0,0,0,0c0.6,0.7,0.6,1.7,0,2.4L4.2,8.2"/></svg>';
 	    var arrowRight = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 9.9 16.8" style="enable-background:new 0 0 9.9 16.8;" xml:space="preserve"><path style="fill:#2673C2;" d="M5.7,8.7L0.5,14c-0.6,0.7-0.6,1.7,0,2.3c0.6,0.6,1.6,0.7,2.3,0c0,0,0,0,0,0 l6.6-6.7c0.6-0.7,0.6-1.7,0-2.3L2.8,0.5c-0.6-0.6-1.6-0.7-2.3,0c0,0,0,0,0,0c-0.6,0.7-0.6,1.7,0,2.3l5.2,5.3"/></svg>';

        $('.pc-gallery-thumbs .pc-slider-wrapper,.pc-gallery-thumbs-popup .pc-slider-wrapper').slick({
            infinite: false,
            slidesToShow: 4,
            slidesToScroll: 4,
            vertical: false,
            draggable: false,
			prevArrow: '<span class="_arrow _prev slick-prev">' + arrowLeft + '</span>',
			nextArrow: '<span class="_arrow _next slick-next">' + arrowRight + '</span>',
			responsive: [
				{
					breakpoint: 1119,
					settings: {
					    slidesToShow: 3,
					    slidesToScroll: 1,
					    draggable: true,
					    dots: false,
					    infinite: true
					}
		        }
		    ]
        });
        
        $('.pc-main').on('mouseover','.responsive-products .product-item', function() {
            var _ = $(this),
                resp = _.parents('.responsive-products');
            if (resp.find('.pi-hover-wrapper').children().length == 0 && !resp.find('.slick-track').hasClass('animating') && window.innerWidth > 768){
                var left = _.offset().left - _.parents('.pi-5').offset().left;
                resp.find('.pi-hover-wrapper').append( _.clone())
                resp.find('.pi-hover-wrapper').css('left',left+'px').children().removeClass('slick-active slick-current slick-slide');
            }
        }).on('mouseleave','.pi-hover-wrapper',function(){
            $(this).empty();
        });

			$(document).on(clickHandler, '[data-window="#pc-quickbuy"]', function() {
				var _ = $('#pc-quickbuy');

				var productId = parseInt(_.attr('data-product-id'));
				if (0 < productId) {
					var products = [];
					products.push({
						id: productId,
						price: _.attr('data-product-price'),
						quantity: _.attr('data-product-quantity')
					});
					CriteoHelper.addToBasket(products);
				}

				var rrProductId = parseInt(_.attr('data-product-rr-id'));
				RRHelper.addToBasket([rrProductId]);
			});
			$('#form-quick-order .btn-submit').click(function() {
				submitForm('#form-quick-order', {
					success: function(data) {
						if (data.order) {
							var order = data.order;
							var user = data.user;

							GaEcommerceHelper.trackQuickOrderPage(order);
							CriteoHelper.postCheckout(order.id, order.amount, order.products);
							RRHelper.order(order.id, order.amount, order.products);
							FlocktoryHelper.postCheckout((user ? user.name : ''), (user ? user.email : ''), order.id, order.amount, order.products);
						}

						$('#pc-quickbuy .wsd-header').html('Спасибо за заказ');
						$('#pc-quickbuy .typ').show();
						$('#form-quick-order').hide();
					}
				});
				return false;
			});
			$('#form-quick-order').submit(function() {
				$('#form-quick-order .btn-submit').trigger(clickHandler);
				return false;
			});

        //timer
        pc_timer();
    });
    
    $(document).on(clickHandler,'.pc-conf .pc-conf-elem',function(){
        var $this = $(this);
        $this.parents('.pc-conf-body').find('.pc-conf-elem.active').toggleClass('active');
        $this.toggleClass('active');
    });

	 $(document).on(clickHandler,'.pc-club-rect',function(){
        //var $this = $(this);
        //$this.toggleClass('active');
    });
    
    $(document).on('mouseover','.pc-hint i',function(){
        var $this = $(this);
        $this.parents('.pc-hint').find('.pc-hint-text').fadeIn(250);
    });
    $(document).on('mouseleave','.pc-hint i',function(){
        var $this = $(this);
        $this.parents('.pc-hint').find('.pc-hint-text').fadeOut(250);
    });

	 var priceHintTimer = null;
	 $(document).on('mouseover','.pc-price-hint-text',function() {
		clearTimeout(priceHintTimer);
	 });
	 $(document).on('mouseover','.pc-price-hint i',function() {
			clearTimeout(priceHintTimer);
        var $this = $(this);
        $this.parents('.pc-price-hint').find('.pc-price-hint-text').fadeIn(250);
    });
    $(document).on('mouseleave','.pc-price-hint i, .pc-price-hint-text',function() {
			var $this = $(this);
			priceHintTimer = setTimeout(function() {
	        $this.parents('.pc-price-hint').find('.pc-price-hint-text').fadeOut(250);
			}, 1000);
    });
	 $(document).on('click', '.pc-conf-elem', function() {
		var _ = $(this);
		if (_.attr('data-link')) {
			redirect(_.attr('data-link'));
		}
	 });

	 $(document).on(clickHandler, '.plus, .minus', function() {
		var _ = $(this);
		var inp = _.parent().find('input[type="text"]');
		var amount = parseInt(inp.val()) * parseInt(inp.attr('data-price'));
		_.closest('.w-product-mini').find('.price-res').html(format_price(amount) + ' <span class="rub">a</span>');
	 });

	 $(document).on('click','.pc-one-slide a',function(){
        var _ = $(this);
        var img = _.data('image');
        _.parents('.pc-slider').find('.pc-gallery-top-wrapper img').attr({src:img});
        _.parents('.pc-slider-wrapper').find('a.active').removeClass('active');
        _.addClass('active');
    });
    
    //timer
    function pc_timer(){
        if ($('.pc-delivery-stock').length > 0){
            var time_begin = $('.pc-delivery-stock').data('time_begin'),
                time_end = $('.pc-delivery-stock').data('time_end');
                pc_timer_persecond(time_begin,time_end);
                timer = setInterval(function(){
                    pc_timer_persecond(time_begin,time_end);
                    time_begin++;
                    if (time_begin >= time_end){
                        clearInterval('timer');
                    }
                }, 1000);
        }
    }
    function pc_timer_persecond(time_begin,time_end){
        var time_left = time_end - time_begin,
            h = Math.floor(time_left / 3600);
            m = Math.floor((time_left - 3600 * h) / 60);
            s = time_left - 3600 * h - 60 * m;
        $('.pc-timer-hour').html(pc_timer_format(h));
        $('.pc-timer-hour').next().html(sklon(h, 'час', '', 'а', 'ов'));
        $('.pc-timer-minute').html(pc_timer_format(m));
        $('.pc-timer-minute').next().html(sklon(m, 'минут', 'а', 'ы', ''));
        $('.pc-timer-second').html(pc_timer_format(s));
        $('.pc-timer-second').next().html(sklon(s, 'секунд', 'а', 'ы', ''));
    }
    function pc_timer_format(i){
        if (i < 10){
            return '0'+i;
        }
        return i;
    }
    function sklon(num, basic, ed, do4, posle5){
        var slovco = basic;
        if(num-Math.floor(num) != 0) slovco += do4;
        else{
            var sht;
            if(num>=100){
                sht = num*0.01;
                sht = Math.round(sht);
                sht = num - sht*100;
            }else   sht = num;
            if(sht>=5 && sht<=20) slovco+=posle5;
            else{
                if(sht%10 == 1) slovco+=ed;
                else{
                    if(sht%10 > 4) slovco+=posle5;			
                    else{
                        if(sht%10 == 0) slovco+=posle5;
                        else{
                            slovco+=do4;
                        }
                    }
                }
            }
        }
        return slovco;
    }

	if (exists('.base64-encoded') && !isSearchEngine()) {
		$('.base64-encoded').each(function() {
			var _ = $(this);
			var content = Base64.decode(_.html());
			_.parent().html(content);
			_.removeClass('hidden');
		});

		window._ShoppilotAPI.then(function (Shoppilot) {
			var ProductWidget = Shoppilot.require('product_widget');
			var MultiWidget = Shoppilot.require('multi_widget');

			var container = $('#sp-product-reviews-container');
			var opts = {
				name: 'product-reviews',
				product_id: container.attr('data-product-id'),
				container: '#sp-product-reviews-container'
			};
			if (container.attr('data-page')) {
				opts.page = container.attr('data-page');
			}
			if (container.attr('data-sort')) {
				opts.sort = container.attr('data-sort');
			}
			var reviewObj = new ProductWidget(opts);
			console.log(opts, reviewObj);
			MultiWidget.render([reviewObj]);
		});
	}

});


function initBasketPreviewSlick(wp, groupId) {
	var _ = $('#basket-preview .responsive-products');
	if (!wp.hasClass('slick-initialized') && wp.is(':visible')) {
		console.log('init basket slick', wp, groupId);
		try {
			wp.slick({
				 infinite: false,
				 slidesToShow: 4,
				 slidesToScroll: 1,
				 draggable: false,
				 prevArrow: _.find('.p-slick-control.for-basket-acc-group-' + groupId + ' .p-slick-control-prev'),
				 nextArrow: _.find('.p-slick-control.for-basket-acc-group-' + groupId + ' .p-slick-control-next'),
			});
		} catch(e) {
			console.log(e);
		}
		//wp.addClass('initialized');
		console.log('end init basket slick', wp, groupId);
	}
}

function initSlick() {
	$('.responsive-products:visible').each(function() {
		var _ = $(this);
		if (!_.hasClass('initialized')) {
			if (_.hasClass('basket-preview-accessories')) {
				
			} else if (!_.hasClass('accessories') && !_.hasClass('notavail')) {
				_.find('.slick-wrapper').slick({
					 infinite: false,
					 slidesToShow: 5,
					 slidesToScroll: 1,
					 draggable: false,
					 prevArrow: _.find('.p-slick-control-prev'),
					 nextArrow: _.find('.p-slick-control-next'),
					  responsive: [{
						  breakpoint: 990,
						  settings: {
								slidesToShow: 4,
								slidesToScroll: 1,
						  }
					 },{
						  breakpoint: 768,
						  settings: {
								slidesToShow: 1,
								slidesToScroll: 1,
								dots: true,
								appendDots: _.find('.slick-dots-wrapper'),
								draggable: true
						  }
					 }
					 ]
				});
			} else {
				_.find('.slick-wrapper').each(function() {
					var wp = $(this);
					var groupId = wp.attr('data-group-id');
					initAccessoriesSlick(wp, groupId);
				});
			}
			_.addClass('initialized');
		}
  });
}

function initAccessoriesSlick(wp, groupId) {
	var _ = $('.pc-accessories .responsive-products.accessories');
	if (!wp.hasClass('initialized') && !wp.hasClass('slick-initialized') && wp.is(':visible')) {
		wp.slick({
			 infinite: false,
			 slidesToShow: 5,
			 slidesToScroll: 1,
			 draggable: false,
			 prevArrow: _.find('.p-slick-control.for-acc-group-' + groupId + ' .p-slick-control-prev'),
			 nextArrow: _.find('.p-slick-control.for-acc-group-' + groupId + ' .p-slick-control-next'),
			  responsive: [{
				  breakpoint: 990,
				  settings: {
						slidesToShow: 4,
						slidesToScroll: 1,
				  }
			 },{
				  breakpoint: 768,
				  settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						dots: true,
						appendDots: _.find('.slick-dots-wrapper.for-acc-group-' + groupId + ' .slick-dots-wrapper'),
						draggable: true
				  }
			 }
			 ]
		});
		wp.addClass('initialized');
	}
}

window.currentAccId = 0;
function loadAccessoriesProducts(link, id, successFunc) {
	window.currentAccId = id;
	var tabAccess = $('#tab-access');

	var blockProducts = tabAccess.find('.pc-accessories-products');
	blockProducts.html('').addClass('loading');
	$.ajax({
		url: link,
		method: 'get',
		data: { embed: 1 },
		success: function(data) {
			blockProducts.html(data);
			if ($(window).width()<760) {
			    $("#products-list").prepend("<div class='-slick-wrapper'></div>");
			    $("#products-list .product-item").slice(0, 10).appendTo("#products-list>.-slick-wrapper");
			    $("#products-list>.product-item").remove();
			}
			$('#filter-sidebar .spoiler.collapsed').each(function () {
				spoilerToggle($(this), true, 1);
			});
			rangeInputsInit();
			showHiddenHtml();
			initializeProductsProperties();
			initLozad();

			successFunc();

			refreshAccessoriesCategories();
		},
		complete: function() {
			blockProducts.removeClass('loading');
			if ($(window).width()<760) {
    			$("#products-list>.-slick-wrapper").slick({
        			slidesToShow: 2,
					variableWidth: true,
					dots: true,
					arrows: false,
					infinite: true,
					centerMode: true
    			});
			}
		},
		error: function() {
			blockProducts.removeClass('loading');
		}
	});
}

function refreshAccessoriesCategories() {
	try {
		if (0 < $('#filter-sidebar .categories-list').length) {
			return;
		}

		var tabAccess = $('#tab-access');
		var searchTags = tabAccess.find('.search-tags');
		var searchTagsHtml = searchTags.html();

		var categoriesList = $('<div class="filter-box static"><label class="categories">Нужные аксессуары<br><small>Подбери свой</small></label><ul class="clearstyle categories-list list-a"></ul></div>');
		categoriesList.find('ul').html(searchTagsHtml);
		/*
		if (15 < categoriesList.find('li').length) {
			var hiddenListObj = $('<ul class="clearstyle categories-list list-a spoiler collapsed __showMore"></ul>');
			var index = 0;
			categoriesList.find('li').each(function() {
				if (15 <= index) {
					hiddenListObj.append($(this).remove());
				}
				index++;
			});
			categoriesList.find('ul').after(hiddenListObj);
			categoriesList.find('ul.collapsed').after('<a href="#" class="more collapsed" data-toggle="showmore" data-class="__showMore" data-endtext="скрыть категории" style="margin-left: 0; margin-top: 10px;">показать еще</a>');
		}
		*/
		$('#filter-sidebar').prepend(categoriesList);
		$('#filter-sidebar .categories-list a').click(function() {
			var _ = $(this);
			var link = _.attr('data-group-link');
			var id = _.attr('data-group-id');
			if ($(this).parent().hasClass("group")) {
			    $(this).parent().toggleClass("expanded");
                if ($(this).parent().hasClass("expanded")) {
    			    $(this).parent().find("ul").show("fast");
                } else {
    			    $(this).parent().find("ul").hide("fast");
                };
			} else {
    			loadAccessoriesProducts(link, id, function() {});
            }
			return false;
		});
		categoriesList.find('a').removeClass('active');
		categoriesList.find('a[data-group-id="' + window.currentAccId + '"]').addClass('active');

		// spoilers
		$('#filter-sidebar .categories-list.spoiler.collapsed').each(function () {
			spoilerToggle($(this), true, 1);
		});
	} catch (e) {
		console.log(e);
	}
}