var FlocktoryHelper = {
	
	debug: true,

	info: function(event, data) {
		if (this.debug) {
			console.log('flocktory', 'info', event, data);
		}
	},

	warn: function(event, data) {
		if (this.debug) {
			console.log('flocktory', 'warning', event, data);
		}
	},

	addToBasket: function(id, price, quantity) {
		if (null == quantity) {
			return;
		}
		var event = 'add to basket';
		var item = {
			"id": id,
			"price": price,
			"count": quantity
		};
		try {
			window.flocktory = window.flocktory || [];
			window.flocktory.push(['addToCart', {
				item: item
			}]);
			this.info('add', item);
		} catch (e) {
			this.warn(event, e);
		}
	},

	removeFromBasket: function(id, quantity) {
		if ((null == quantity) || (0 == quantity)) {
			return;
		}
		var event = 'remove from basket';
		var item = {
			id: id,
			count: quantity
		};
		try {
			window.flocktory = window.flocktory || [];
			window.flocktory.push(['removeFromCart', {
				item: item
			}]);
			this.info(event, item);
		} catch (e) {
			this.warn(event, e);
		}
	},

	postCheckout: function(userName, userEmail, orderId, orderAmount, products) {
		var event = 'post-checkout';
		var items = [];
		for (var i = 0; i < products.length; i++) {
			var product = products[i];
			var item = {
				id: product.id,
				title: product.name,
				price: product.price,
				count: product.quantity
			};
			items.push(item);
		}
		try {
			window.flocktory = window.flocktory || [];
			var obj = {
				user: {
					name: userName,
					email: userEmail
				},
				order: {
					id: orderId,
					price: orderAmount,
					items: items
				}
			};
			window.flocktory.push(['postcheckout', obj]);
			this.info(event, obj);
		} catch (e) {
			this.warn(event, e);
		}
	}

};