<?php

$authenticated = false;
				
if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
	$user = $_SERVER['PHP_AUTH_USER'];
	$pass = $_SERVER['PHP_AUTH_PW'];
	if ($user == "123" && $pass == "enter") {
		$authenticated = true;
	}
}

if (!$authenticated) {
	header('WWW-Authenticate: Basic realm="Restricted Area"');
	header('HTTP/1.1 401 Unauthorized');
	echo ("Access denied.");
	exit();
}

?><?php
    session_start();
    if (isset($_SESSION["i_states"])) {
        $fav_state = $_SESSION["i_states"]["add-to-favorites"] ? " selected" : "";
        $fav_text  = $_SESSION["i_states"]["add-to-favorites"] ? "В избранном" : "В избранное";
        $cmp_state = $_SESSION["i_states"]["add-to-compare"] ? " selected" : "";
        $cmp_text = $_SESSION["i_states"]["add-to-compare"] ? "В сравнении" : "Добавить в сравнение";
    } else {
        $fav_state = $cmp_state = "";
        $fav_text  = "В избранное";
        $cmp_text =  "Добавить в сравнение";
    }
    $_rnd = rand(1e8,1e9);
?><!DOCTYPE HTML>
<html prefix="og: http://ogp.me/ns#">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Видеокарта GigaByte GeForce GTX 1050 Ti GV-N105TOC-4GL PCI-E 4096Mb 128 Bit Retail (GV-N105TOC-4GL) — купить недорого с доставкой — характеристики, фото | Интернет-магазин 123.ru</title>
    <meta content="width=device-width" name="viewport">
    <meta name="keywords" content="Видеокарта  GigaByte GV-N105TOC-4GL 4096Mb GeForce GTX 1050 Ti  GDDR5, отзывы, обзор, характеристики, цена, стоимость, купить">
    <meta name="description" content="123.ru ➤ Видеокарта GigaByte GeForce GTX 1050 Ti GV-N105TOC-4GL PCI-E 4096Mb 128 Bit Retail (GV-N105TOC-4GL) купить у официального дилера. ✔ Гарантия от GigaByte. ✔ Товар на складе. ✔ Без предоплаты. ➤ Прямые поставки — мы дешевле! ☎ 8 (495) 225-9-123. ">
    <meta name='yandex-verification' content='5963dc5637ee1578' />
    <meta name="yandex-verification" content="a73fb8e52f1a0972" />
    <meta name="yandex-verification" content="8b521d40af40902c" />
    <meta name="google-site-verification" content="ifHMSh15xugtEKNlZukNw3XTsUaDsOcYcx6A3zRKiBo" />
    <meta name="referer" content="https://www.123.ru/" />
    <link rel="canonical" href="https://www.123.ru/accessories/komplektuyuschie_dlya_pk/videokarti/brand-gigabyte/videokarta_gigabyte_gv_n105toc_4gl_4096_geforce_gtx_1050_ti_gddr5_art8990358/" />
    <link rel="stylesheet" type="text/css" href="/libs/jquery-ui-1.12.1.custom/jquery-ui.min.css"></link>
    <link rel="stylesheet" type="text/css" href="/css/style.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/style.shoppilot.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/style.v2.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/style.new.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/icons.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/catalog.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/search_results.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/pages.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/forms.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/lk.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/selections.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/product-card-v2.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/product-card-v2-resp.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/construct.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/main.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/cart.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/sitemap.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/contacts.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/responsive.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/lk.responsive.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/product-card.responsive.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/style.responsive.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/main.responsive.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/pages.responsive.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/search_results.responsive.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/selections.responsive.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/css/cart.responsive.css?rnd=<?php echo $_rnd; ?>"></link>
    <link rel="stylesheet" type="text/css" href="/js/jquery.bxslider/jquery.bxslider.css"></link>
    <link rel="stylesheet" type="text/css" href="/libs/datepicker/datepicker123.css"></link>
    <link rel="stylesheet" type="text/css" href="/libs/nanoscroller/nanoscroller.css"></link>
    <link rel="stylesheet" type="text/css" href="/libs/slick/slick.css"></link>
    
    <link rel="stylesheet" type="text/css" href="/css/compare-new.css?rnd=<?php echo $_rnd; ?>"></link>
    
    <link rel="shortcut icon" href="/i/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="/i/favicon.png" type="image/png" />
    <meta property="fb:admins" content="100001870821652" />
    <meta property="fb:app_id" content="1509412935940835" />
    <meta property="og:locale" content="ru_RU" />
    <meta property="og:title" content="Видеокарта GigaByte GeForce GTX 1050 Ti GV-N105TOC-4GL PCI-E 4096Mb 128 Bit Retail (GV-N105TOC-4GL) — купить недорого с доставкой — характеристики, фото | Интернет-магазин 123.ru" />
    <meta property="og:description" content="123.ru ➤ Видеокарта GigaByte GeForce GTX 1050 Ti GV-N105TOC-4GL PCI-E 4096Mb 128 Bit Retail (GV-N105TOC-4GL) купить у официального дилера. ✔ Гарантия от GigaByte. ✔ Товар на складе. ✔ Без предоплаты. ➤ Прямые поставки — мы дешевле! ☎ 8 (495) 225-9-123. " />
    <meta property="og:image:url" content="https://www.123.ru/xl_pics/8990358_1.jpg" />
    <meta property="og:url" content="http://www.123.ru/accessories/komplektuyuschie_dlya_pk/videokarti/brand-gigabyte/videokarta_gigabyte_gv_n105toc_4gl_4096_geforce_gtx_1050_ti_gddr5_art8990358/" />
    <script type="text/javascript">
        var _gaq = window._gaq || [];
        window.onerror = function(msg, url, line) {
            var preventErrorAlert = true;
            _gaq.push(['_trackEvent', 'JS Error', msg, navigator.userAgent + ' -> ' + url + " : " + line, 0, true]);
            return preventErrorAlert;
        };
    </script>
    <script type="text/javascript">
        dataLayer = [{
            "pageType": "product",
            "clientID": "1b878e80-317e-c43d-147a-4167cc9d0dd8",
            "userID": 0,
            "auth": 0,
            "city": "Москва",
            "ecommerce": {
                "detail": {
                    "products": [{
                        "id": "8990358",
                        "name": "Видеокарта GigaByte GeForce GTX 1050 Ti GV-N105TOC-4GL PCI-E 4096Mb 128 Bit Retail (GV-N105TOC-4GL)",
                        "category": "Видеокарты",
                        "price": 12630,
                        "dimension18": 1
                    }]
                }
            },
            "dimension18": 1
        }];
    </script>
    <script type="text/javascript" src="/js/pickup-types.js"></script>
    <script type="text/javascript">
        var dataLayer = window.dataLayer = window.dataLayer || [];
        dataLayer.push({
            'ab_variation': "new"
        });
    </script>
    <script type="text/javascript">
        var dataLayer = window.dataLayer = window.dataLayer || [];
        dataLayer.push({
            expId: "f-KHvCBDR7aR2kQYbEsj_Q",
            expVar: 0
        });
    </script>
    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-KBL5ZV');
    </script>
    <!-- End Google Tag Manager -->
</head>

<body id="body" class="bootstrap-extends item-new" data-no-history-update="1" data-city-name="Москва" data-is-search-engine="0" data-geo-city-id="15238" data-is-iml-city="0" data-is-sdek-city="0">
    <div style="display: none;">
        <!-- Top100 (Kraken) Widget --><span id="top100_widget"></span>
        <!-- END Top100 (Kraken) Widget -->
        <!-- Top100 (Kraken) Counter -->
        <script>
            (function(w, d, c) {
                (w[c] = w[c] || []).push(function() {
                    var options = {
                        project: 2802743,
                        element: 'top100_widget',
                    };
                    try {
                        w.top100Counter = new top100(options);
                    } catch (e) {}
                });
                var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function() {
                        n.parentNode.insertBefore(s, n);
                    };
                s.type = "text/javascript";
                s.async = true;
                s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//st.top100.ru/top100/top100.js";
                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else {
                    f();
                }
            })(window, document, "_top100q");
        </script>
        <noscript> <img src="//counter.rambler.ru/top100.cnt?pid=2802743" alt="Топ-100" /> </noscript>
        <!-- END Top100 (Kraken) Counter -->
    </div>
    <script type="text/javascript">
        var rrPartnerId = "52e0e8141e994426487779d9";
        var rrApi = {};
        var rrApiOnReady = [];
        rrApi.addToBasket = rrApi.order = rrApi.categoryView = rrApi.view =
            rrApi.recomMouseDown = rrApi.recomAddToCart = rrApi.recomTrack = function() {};
        (function(d) {
            var ref = d.getElementsByTagName('script')[0];
            var apiJs, apiJsId = 'rrApi-jssdk';
            if (d.getElementById(apiJsId)) return;
            apiJs = d.createElement('script');
            apiJs.id = apiJsId;
            apiJs.async = true;
            apiJs.src = "//cdn.retailrocket.ru/Content/JavaScript/tracking.js";
            ref.parentNode.insertBefore(apiJs, ref);
        }(document));
    </script>
    <script type="text/javascript">
        var digiScript = document.createElement('script');
        digiScript.src = '//cdn.diginetica.net/123/client.js?ts=' + Date.now();
        digiScript.defer = true;
        digiScript.async = true;
        document.body.appendChild(digiScript);
    </script>
    <header class="second third no-scroll simple">
        <div class="top-wrapp top-wrapp-second">
            <div class="container">
                <div class="content-cnt">
                    <section class="top-header">
                        <div class="wrapp-top">
                          <div class='left-side'>
                            <a href="/" class="logo-s"></a>
                            <ul class="link-menu">
                                <li class="_js-dismiss">
                                    <div class="select-city"><a href="#" data-window="#cities" data-background="true" data-responsive="true" id="#citysel">Москва<svg  version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="8px" height="7.41px" viewBox="6 8.59 12 7.41" enable-background="new 6 8.59 12 7.41" xml:space="preserve" fill="#ffffff"><path fill="black" d="M16.59,8.59L12,13.17L7.41,8.59L6,10l6,6l6-6L16.59,8.59z"></path><path fill="none" d="M0,0h24v24H0V0z"></path></svg></a></div>
                                </li>
                                <li>
                                    <div><a href="/ncontact/">Контакты</a></div>
                                </li>
                                <li class="with-drop">
                                    <div class="link"><a href="#">Покупателям</a>
                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="8px" height="7.41px" viewBox="6 8.59 12 7.41" enable-background="new 6 8.59 12 7.41" xml:space="preserve" fill="#8891c2">
                                            <path d="M16.59,8.59L12,13.17L7.41,8.59L6,10l6,6l6-6L16.59,8.59z"></path>
                                            <path fill="none" d="M0,0h24v24H0V0z"></path>
                                        </svg>
                                    </div>
                                    <ul class="submenu">
                                        <li><a href="/about/delivery.php/">Доставка</a></li>
                                        <li><a href="/pay/">Оплата</a></li>
                                        <li><a href="/credit/">Покупка в кредит</a></li>
                                        <li><a href="/sovest/">Рассрочка с картой Совесть</a></li>
                                        <li><a href="/halva/">Рассрочка с картой Халва</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <div class=""><a href="/warranty/">Гарантия</a></div>
                                </li>
                                <li class="with-drop">
                                    <div class="link"><a href="#">Корпоративным клиентам</a>
                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="8px" height="7.41px" viewBox="6 8.59 12 7.41" enable-background="new 6 8.59 12 7.41" xml:space="preserve" fill="#8891c2">
                                            <path d="M16.59,8.59L12,13.17L7.41,8.59L6,10l6,6l6-6L16.59,8.59z"></path>
                                            <path fill="none" d="M0,0h24v24H0V0z"></path>
                                        </svg>
                                    </div>
                                    <ul class="submenu">
                                        <li><a href="/b2b/">B2B: Почему мы?</a></li>
                                        <li><a href="/api/">API - новый импульс для ваших продаж!</a></li>
                                        <li><a href="//st.123.ru/files/123_All_Dealer.zip">Скачать прайс <img src="/i/zip.svg" width="20" height="20" style="position: relative; top: -3px; left: 5px;" /></a></li>
                                    </ul>
                                </li>
                                <li>
                                    <div class=""><a href="/franchise/">Франчайзинг</a></div>
                                </li>
                                <li>
                                    <div class=""><a href="/quick-pay/">Оплатить</a></div>
                                </li>
                            </ul>
                          </div>
                          <div class="right-side">
                            <ul class="user-menu">
                                <li class="wr__tel"><a href="tel:8 (495) 225-9-123">8 (495) 225-9-123</a></li>
                                <li class="wr__callback"><a href="#" data-window="#callback" data-background="true" data-position="right" data-autofocus="true">Обратный звонок</a></li>
                                <!--
                                <li class="wr__auth"><a href="#" data-window="#login" data-position="right" data-autofocus="true" data-background="true">Войти</a></li>
                                <li class="wr__reg"><a href="#" data-window="#login" data-position="right" data-autofocus="true" data-background="true" data-openspoiler="#spoiler-register" data-closespoiler="#spoiler-login">Регистрация</a></li>
                                -->
                                <li class="wr_auth-done with-drop"><div class="link"><a href="/cabinet/">rgoryanin@it-solo.com</a></div><ul class="submenu"><li><a href="/cabinet/reserves/">История заказов</a></li><li><a href="/cabinet/profile/">Учетная запись</a></li><li><a href="/bad/">Задать вопрос</a></li><li><a href="/logout/">Выйти</a></li></ul></li>
                            </ul>
                          </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div class="bottom-wrapp">
            <div class="container">
                <div class="content-cnt">
                    <section class="bottom-header">
                        <div class="wrapp-l-t-m">
                            <div class="l-t">
                                <a class="logo" href="/"><img src="/i/main_logo_white.svg" alt=""></a>
                                <a href="javascript:void(0)" class="catalog__btn"><img src="/i/icon_arrow_down_white.svg" height="15px"></a>
                            </div>
                            <div class="l-t-m"><a href="javascript:void(0)" class="catalog__btn">Каталог</a>
                                <div id="header-catalog" class="category">
                                    <div class="catalog__content">
                                        <div class="catalog__main">
                                            <ul>
                                                <li>
                                                    <a href="/smartfoni_plansheti_gadjeti/" class="_childs" data-id="3">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 66.18 69.94">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <title>1</title>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="lifestyle">
                                                                    <path class="cls-1" d="M30,8H20a2,2,0,0,0,0,4H30a2,2,0,0,0,0-4Z" />
                                                                    <path class="cls-1" d="M50,22h0c-.43-.07-.85.09-1.28,0C49.14,22.2,49.55,22,50,22Z" />
                                                                    <path class="cls-1" d="M35.93,60a9.1,9.1,0,0,0,0,1.44c0-.48,0-1,0-1.44Z" />
                                                                    <path class="cls-1" d="M6,60a2,2,0,0,1-2-2V6A2,2,0,0,1,6,4H44a2,2,0,0,1,2,2V22h0c.93.17,1.83-.19,2.72,0-.76-.37-1.51,0-2.53,0,1,0,1.77-.37,2.53,0,.42.09.85-.07,1.28,0h0V6a6,6,0,0,0-6-6H6A6,6,0,0,0,0,6V58a6,6,0,0,0,6,6H35.93c0-.85,0-1.7,0-2.56a9.1,9.1,0,0,1,0-1.44Z" />
                                                                    <path class="cls-1" d="M50,22c-.45,0-.86.2-1.28,0-.9-.19-1.79.17-2.72,0" />
                                                                    <path class="cls-1" d="M35.93,60h0c0,.48,0,1,0,1.44,0,.18,0,.35,0,.53Z" />
                                                                    <path class="cls-1" d="M35.93,64V62c0-.18,0-.35,0-.53,0,.85,0,1.71,0,2.56h0c-.18-.68.15-1.35,0-2Z" />
                                                                    <path class="cls-1" d="M35.93,60v2c.15.68-.18,1.35,0,2" />
                                                                    <circle class="cls-1" cx="25" cy="53" r="3" />
                                                                    <path class="cls-1" d="M54.49,26.29h-7.8a1.56,1.56,0,1,0,0,3.12h7.8a1.56,1.56,0,0,0,0-3.12Z" />
                                                                    <path class="cls-1" d="M61.5,20.06H39.68A4.69,4.69,0,0,0,35,24.73V65.27a4.69,4.69,0,0,0,4.68,4.68H61.5a4.69,4.69,0,0,0,4.68-4.68V24.73A4.69,4.69,0,0,0,61.5,20.06Zm1.56,45.21a1.56,1.56,0,0,1-1.56,1.56H39.68a1.56,1.56,0,0,1-1.56-1.56V24.73a1.56,1.56,0,0,1,1.56-1.56H61.5a1.56,1.56,0,0,1,1.56,1.56Z" />
                                                                    <circle class="cls-1" cx="50.59" cy="61.37" r="2.34" />
                                                                </g>
                                                            </g>
                                                        </svg>Смартфоны, планшеты, гаджеты</a>
                                                </li>
                                                <li>
                                                    <a href="/kompyutery_noutbuki_soft/" class="_childs" data-id="146">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 36">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <title>2</title>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="lifestyle">
                                                                    <circle class="cls-1" cx="10" cy="26" r="2" />
                                                                    <path class="cls-1" d="M62,0H26a2,2,0,0,0-2,2V26a2,2,0,0,0,2,2H42v4H38a2,2,0,0,0,0,4H50a2,2,0,0,0,0-4H46V28H62a2,2,0,0,0,2-2V2A2,2,0,0,0,62,0ZM60,24H28V4H60Z" />
                                                                    <path class="cls-1" d="M18,0H2A2,2,0,0,0,0,2V34a2,2,0,0,0,2,2H18a2,2,0,0,0,2-2V2A2,2,0,0,0,18,0ZM4,10H16v2H4ZM16,4V6H4V4Zm0,28H4V16H16Z" />
                                                                </g>
                                                            </g>
                                                        </svg>Компьютеры, ноутбуки, аксессуары</a>
                                                </li>
                                                <li>
                                                    <a href="/komplektuyuschie_dlya_pk/" class="_childs" data-id="41">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <title>3</title>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="communication">
                                                                    <path class="cls-1" d="M40,20H24a4,4,0,0,0-4,4V40a4,4,0,0,0,4,4H40a4,4,0,0,0,4-4V24A4,4,0,0,0,40,20Zm0,20H24V24H40Z" />
                                                                    <path class="cls-1" d="M62,26a2,2,0,0,0,0-4H56V18h6a2,2,0,0,0,0-4H56a6,6,0,0,0-6-6V2a2,2,0,1,0-4,0V8H42V2a2,2,0,1,0-4,0V8H34V2a2,2,0,1,0-4,0V8H26V2a2,2,0,1,0-4,0V8H18V2a2,2,0,1,0-4,0V8a6,6,0,0,0-6,6H2a2,2,0,0,0,0,4H8v4H2a2,2,0,0,0,0,4H8v4H2a2,2,0,0,0,0,4H8v4H2a2,2,0,0,0,0,4H8v4H2a2,2,0,0,0,0,4H8a6,6,0,0,0,6,6v6a2,2,0,1,0,4,0V56h4v6a2,2,0,1,0,4,0V56h4v6a2,2,0,1,0,4,0V56h4v6a2,2,0,1,0,4,0V56h4v6a2,2,0,1,0,4,0V56a6,6,0,0,0,6-6h6a2,2,0,0,0,0-4H56V42h6a2,2,0,0,0,0-4H56V34h6a2,2,0,0,0,0-4H56V26ZM52,50a2,2,0,0,1-2,2H14a2,2,0,0,1-2-2V14a2,2,0,0,1,2-2H50a2,2,0,0,1,2,2Z" />
                                                                </g>
                                                            </g>
                                                        </svg>Комплектующие для ПК</a>
                                                </li>
                                                <li>
                                                    <a href="/komputernaya_periferiya/" class="_childs" data-id="341">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 64">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <title>4</title>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="lifestyle">
                                                                    <path class="cls-1" d="M38,0a2,2,0,0,0-2,2V8a4,4,0,0,1-8,0A8,8,0,0,0,12,8V20.16A14,14,0,0,0,0,34V50a14,14,0,0,0,28,0V34A14,14,0,0,0,16,20.16V8a4,4,0,0,1,8,0A8,8,0,0,0,40,8V2A2,2,0,0,0,38,0ZM24,34V50A10,10,0,0,1,4,50V34a10,10,0,0,1,20,0Z" />
                                                                    <path class="cls-1" d="M12,32v4a2,2,0,0,0,4,0V32a2,2,0,0,0-4,0Z" />
                                                                </g>
                                                            </g>
                                                        </svg>Компьютерная периферия</a>
                                                </li>
                                                <li>
                                                    <a href="/communication-equipment/" class="_childs" data-id="353">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 56">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <title>5</title>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="communication">
                                                                    <circle class="cls-1" cx="10" cy="42" r="2" />
                                                                    <circle class="cls-1" cx="16" cy="42" r="2" />
                                                                    <circle class="cls-1" cx="22" cy="42" r="2" />
                                                                    <path class="cls-1" d="M58,28H52V16a2,2,0,0,0-4,0V28H6a6,6,0,0,0-6,6V46a6,6,0,0,0,6,6h6v2a2,2,0,0,0,4,0V52H48v2a2,2,0,0,0,4,0V52h6a6,6,0,0,0,6-6V34A6,6,0,0,0,58,28ZM4,46V34a2,2,0,0,1,2-2H40V48H6A2,2,0,0,1,4,46Zm56,0a2,2,0,0,1-2,2H44V32H58a2,2,0,0,1,2,2Z" />
                                                                    <path class="cls-1" d="M50,6a8,8,0,0,0-8,8,2,2,0,0,0,4,0,4,4,0,0,1,8,0,2,2,0,0,0,4,0A8,8,0,0,0,50,6Z" />
                                                                    <path class="cls-1" d="M38,16a2,2,0,0,0,2-2,10,10,0,0,1,20,0,2,2,0,0,0,4,0,14,14,0,0,0-28,0A2,2,0,0,0,38,16Z" />
                                                                </g>
                                                            </g>
                                                        </svg>Сетевое оборудование</a>
                                                </li>
                                                <li>
                                                    <a href="/audio-video_i_tv/" class="_childs" data-id="84">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 48">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <title>6</title>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="media">
                                                                    <path class="cls-1" d="M58,8H41a2,2,0,0,1-2-2,6,6,0,0,0-6-6H19a6,6,0,0,0-6,6,2,2,0,0,1-2,2H6a6,6,0,0,0-6,6V42a6,6,0,0,0,6,6H58a6,6,0,0,0,6-6V14A6,6,0,0,0,58,8Zm2,34a2,2,0,0,1-2,2H6a2,2,0,0,1-2-2V14a2,2,0,0,1,2-2h5a6,6,0,0,0,6-6,2,2,0,0,1,2-2H33a2,2,0,0,1,2,2,6,6,0,0,0,6,6H58a2,2,0,0,1,2,2Z" />
                                                                    <path class="cls-1" d="M27,16A12,12,0,1,0,39,28,12,12,0,0,0,27,16Zm0,20a8,8,0,1,1,8-8A8,8,0,0,1,27,36Z" />
                                                                    <circle class="cls-1" cx="50" cy="20" r="4" />
                                                                </g>
                                                            </g>
                                                        </svg>Телевизоры, фото, аудио/видео техника</a>
                                                </li>
                                                <li>
                                                    <a href="/vse_dlya_doma_i_sada/" class="_childs" data-id="7">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 40">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <title>7</title>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="eco">
                                                                    <path class="cls-1" d="M63.73,9A2,2,0,0,0,62,8H54a2,2,0,0,0-1.34.51L45.46,15,44,1.78A2,2,0,0,0,42,0H18a2,2,0,0,0-2,1.78L15.54,6H14a14,14,0,0,0-1.56,27.91L12,37.78A2,2,0,0,0,14,40H46a2,2,0,0,0,1.85-1.24L63.74,11A2,2,0,0,0,63.73,9ZM4,20A10,10,0,0,1,14,10h1.1L12.88,29.93A10,10,0,0,1,4,20ZM16.23,36,19.79,4H40.21l3.56,32Zm31.08-4.32L46,19.89,54.77,12h3.79Z" />
                                                                </g>
                                                            </g>
                                                        </svg>Все для дома и сада</a>
                                                </li>
                                                <li>
                                                    <a href="/remont_i_stroitelstvo/" class="_childs" data-id="9">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 64">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <title>8</title>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="real_estate" data-name="real estate">
                                                                    <path class="cls-1" d="M46,0H12A6,6,0,0,0,6,6V8a6,6,0,0,0-6,6V25a2,2,0,0,0,1.43,1.92L27,34.49V41H26a2,2,0,0,0-2,2V62a2,2,0,0,0,2,2h6a2,2,0,0,0,2-2V43a2,2,0,0,0-2-2H31V33a2,2,0,0,0-1.43-1.91h0L4,23.51V14a2,2,0,0,1,2-2v2a6,6,0,0,0,6,6H46a6,6,0,0,0,6-6V6A6,6,0,0,0,46,0ZM30,60H28V45h2ZM48,14a2,2,0,0,1-2,2H12a2,2,0,0,1-2-2V6a2,2,0,0,1,2-2H46a2,2,0,0,1,2,2Z" />
                                                                </g>
                                                            </g>
                                                        </svg>Ремонт и строительство</a>
                                                </li>
                                                <li>
                                                    <a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/" class="_childs" data-id="10">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 46">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <title>9</title>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="food_and_drink" data-name="food and drink">
                                                                    <path class="cls-1" d="M56,14H50a2,2,0,0,0,0,4h6a2,2,0,0,0,0-4Z" />
                                                                    <path class="cls-1" d="M56,8H50a2,2,0,0,0,0,4h6a2,2,0,0,0,0-4Z" />
                                                                    <path class="cls-1" d="M58,0H6A6,6,0,0,0,0,6V36a6,6,0,0,0,6,6v2a2,2,0,0,0,4,0V42H54v2a2,2,0,0,0,4,0V42a6,6,0,0,0,6-6V6A6,6,0,0,0,58,0Zm2,36a2,2,0,0,1-2,2H6a2,2,0,0,1-2-2V6A2,2,0,0,1,6,4H58a2,2,0,0,1,2,2Z" />
                                                                    <path class="cls-1" d="M17.4,28.69a1,1,0,0,0,1.41,0l14-14a1,1,0,0,0-1.41-1.41l-14,14A1,1,0,0,0,17.4,28.69Z" />
                                                                    <path class="cls-1" d="M31.4,21.29l-6,6a1,1,0,1,0,1.41,1.41l6-6a1,1,0,0,0-1.41-1.41Z" />
                                                                    <path class="cls-1" d="M18,21a1,1,0,0,0,.71-.29l6-6a1,1,0,0,0-1.41-1.41l-6,6A1,1,0,0,0,18,21Z" />
                                                                    <circle class="cls-1" cx="53" cy="32" r="2" />
                                                                    <circle class="cls-1" cx="53" cy="26" r="2" />
                                                                    <path class="cls-1" d="M44,6H8A2,2,0,0,0,6,8V34a2,2,0,0,0,2,2H44a2,2,0,0,0,2-2V8A2,2,0,0,0,44,6ZM42,32H10V10H42Z" />
                                                                </g>
                                                            </g>
                                                        </svg>Климатическая, крупная и мелкая бытовая техника</a>
                                                </li>
                                                <li>
                                                    <a href="/car-electronics/" class="_childs" data-id="61">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 49">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <title>10</title>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="transport">
                                                                    <path class="cls-1" d="M62,5H53.71A7,7,0,0,0,40.29,5H23.71A7,7,0,0,0,10.29,5H2A2,2,0,0,0,0,7V47a2,2,0,0,0,2,2H62a2,2,0,0,0,2-2V7A2,2,0,0,0,62,5ZM4,9H60v6H4ZM4,45V19H60V45Z" />
                                                                    <path class="cls-1" d="M25,30H23V28a2,2,0,0,0-4,0v2H17a2,2,0,0,0,0,4h2v2a2,2,0,0,0,4,0V34h2a2,2,0,1,0,0-4Z" />
                                                                    <path class="cls-1" d="M48,30H40a2,2,0,1,0,0,4h8a2,2,0,0,0,0-4Z" />
                                                                </g>
                                                            </g>
                                                        </svg>Всё для авто</a>
                                                </li>
                                                <li>
                                                    <a href="/kids/" class="_childs" data-id="12969">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 54">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <title>11</title>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="kids">
                                                                    <path class="cls-1" d="M48.56,22,48,15.91V8a8,8,0,0,0-8-8H34a2,2,0,0,0,0,4h6a4,4,0,0,1,4,4v6c-9.32.29-19.3,2.65-26.54,8-2.46-.63-3.85-3.3-4.3-6.05H15a2,2,0,0,0,0-4H7a2,2,0,0,0,0,4H9.13c.44,3.56,2,7.1,5,8.94a24,24,0,0,0-5.66,9.19,10,10,0,1,0,4.18.24C17.25,22.9,31.38,18.42,44.18,18l.4,4.35a16,16,0,1,0,4-.35ZM16,44a6,6,0,1,1-6-6h0A6,6,0,0,1,16,44Zm32,6a12,12,0,0,1-3.06-23.59l.76,8.33a4,4,0,1,0,4-.36L48.92,26A12,12,0,0,1,48,50Z" />
                                                                </g>
                                                            </g>
                                                        </svg>Товары для детей</a>
                                                </li>
                                                <li>
                                                    <a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/" class="_childs" data-id="11">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 56 64">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <title>12</title>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="shopping_and_biz" data-name="shopping and biz">
                                                                    <path class="cls-1" d="M34,24.84V22h9a7,7,0,0,0,7-6.25,8,8,0,1,0-4.05,0A3,3,0,0,1,43,18H34V14a2,2,0,0,0-2-2H22a2,2,0,0,0-2,2V24.84C8.5,27.65,0,37.19,0,48.5a22.45,22.45,0,0,0,1.57,8.23A2,2,0,0,0,3.43,58H6v4a2,2,0,0,0,2,2H46a2,2,0,0,0,2-2V58h2.57a2,2,0,0,0,1.86-1.27A22.42,22.42,0,0,0,54,48.5C54,37.19,45.5,27.65,34,24.84ZM44,8a4,4,0,1,1,4,4A4,4,0,0,1,44,8ZM24,24V16h6v8ZM49.16,54H46a2,2,0,0,0-2,2v4H10V56a2,2,0,0,0-2-2H4.84A18.34,18.34,0,0,1,4,48.5C4,37.2,14.32,28,27,28s23,9.2,23,20.5A18.35,18.35,0,0,1,49.16,54Z" />
                                                                </g>
                                                            </g>
                                                        </svg>Красота и здоровье, косметика, бытовая химия</a>
                                                </li>
                                                <li>
                                                    <a href="/office-seti/" class="_childs" data-id="12769">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 56 64">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <title>13</title>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="design_tools" data-name="design tools">
                                                                    <path class="cls-1" d="M50,40H38V36a2,2,0,0,0-2-2H34V22.39a12,12,0,1,0-12,0V34H20a2,2,0,0,0-2,2v4H6a6,6,0,0,0-6,6v6a6,6,0,0,0,6,6,6,6,0,0,0,6,6H44a6,6,0,0,0,6-6,6,6,0,0,0,6-6V46A6,6,0,0,0,50,40ZM24.8,19.33a8,8,0,1,1,6.4,0A2,2,0,0,0,30,21.16V34H26V21.16A2,2,0,0,0,24.8,19.33ZM22,38H34v2H22ZM44,60H12a2,2,0,0,1-2-2H46A2,2,0,0,1,44,60Zm8-8a2,2,0,0,1-2,2H6a2,2,0,0,1-2-2V46a2,2,0,0,1,2-2H50a2,2,0,0,1,2,2Z" />
                                                                </g>
                                                            </g>
                                                        </svg>Офис, канцелярия</a>
                                                </li>
                                                <li>
                                                    <a href="/uslugi/" class="_childs" data-id="12341">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 54">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <title>14</title>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="shopping_and_biz" data-name="shopping and biz">
                                                                    <path class="cls-1" d="M55.06,8.77A2.68,2.68,0,0,0,53.18,8a4.25,4.25,0,0,0-3.31,2.12c-1.23,1.72-2.15,4.4-1.43,6.48a41.15,41.15,0,0,0,1.64,3.92c-1.21.05-2.84.72-4.07,3.51-.41.92-1,2.15-1.6,3.5-3.3,7.15-4.4,9.75-4.4,11,0,3.38,2.3,12.84,2.56,13.9A2,2,0,0,0,44.5,54h10a2,2,0,0,0,2-2V44C58.73,40.19,64,29.59,64,26.46S56.92,10.57,55.06,8.77ZM52.8,42.37a2,2,0,0,0-.3,1.05V50H46.08c-.79-3.38-2-9.2-2.08-11.35.22-1.19,2.7-6.54,4-9.44.63-1.37,1.21-2.62,1.63-3.56a3.87,3.87,0,0,1,.64-1.07,4.82,4.82,0,0,1,1.11,1.2l-2.89,6.3a2,2,0,1,0,3.63,1.67l3.33-7.27a2,2,0,0,0,.1-.29c.68-1.83-.33-4-1.61-6.79a42.64,42.64,0,0,1-1.75-4.1A4.19,4.19,0,0,1,53,12.6c2.16,3.16,6.84,12.1,7,13.86C60,28.2,55.58,37.87,52.8,42.37Z" />
                                                                    <path class="cls-1" d="M18,24c-1.23-2.78-2.87-3.46-4.07-3.51a41.41,41.41,0,0,0,1.64-3.92c.72-2.09-.2-4.76-1.43-6.48A4.25,4.25,0,0,0,10.82,8a2.68,2.68,0,0,0-1.88.77C7.08,10.57,0,23.19,0,26.46S5.27,40.19,7.5,44v8a2,2,0,0,0,2,2h10a2,2,0,0,0,1.94-1.52C21.7,51.41,24,41.95,24,38.57c0-1.29-1.1-3.89-4.4-11C19,26.19,18.41,25,18,24Zm-.07,26H11.5V43.42a2,2,0,0,0-.3-1.05C8.42,37.87,4,28.2,4,26.49c.14-1.79,4.83-10.73,7-13.89a4.19,4.19,0,0,1,.79,2.71A42.66,42.66,0,0,1,10,19.41c-1.29,2.75-2.29,5-1.61,6.79a2,2,0,0,0,.1.29l3.33,7.27a2,2,0,1,0,3.63-1.67l-2.89-6.3a5.08,5.08,0,0,1,1.09-1.22,3.71,3.71,0,0,1,.65,1.09c.42.94,1,2.19,1.63,3.56,1.34,2.89,3.81,8.24,4,9.44C20,40.8,18.71,46.62,17.92,50Z" />
                                                                    <path class="cls-1" d="M45,22V8a2,2,0,0,0-1-1.75l-11-6a2,2,0,0,0-1.92,0l-11,6a2,2,0,0,0-1,1.75H19V22a2,2,0,0,0,1,1.75l11,6a2,2,0,0,0,1.92,0l11-6A2,2,0,0,0,45,22ZM32,4.28,38.84,8,32,11.72,25.25,8ZM32,25.72l-9-4.91V11.33l8.08,4.43a2,2,0,0,0,1.92,0l8-4.38v9.44Z" />
                                                                </g>
                                                            </g>
                                                        </svg>Услуги</a>
                                                </li>
                                                <li>
                                                    <a href="/123ru-ucenca/" class="_childs" data-id="15394">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        fill: #231f20;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <title>15</title>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="shopping_and_biz" data-name="shopping and biz">
                                                                    <path class="cls-1" d="M35.61,34.6a7,7,0,1,0,9.9,0A7,7,0,0,0,35.61,34.6Zm7.07,7.07a3,3,0,1,1,0-4.24A3,3,0,0,1,42.68,41.68Z" />
                                                                    <path class="cls-1" d="M29.95,28.95a7,7,0,1,0-9.9,0A7,7,0,0,0,29.95,28.95Zm-7.07-7.07a3,3,0,1,1,0,4.24A3,3,0,0,1,22.88,21.88Z" />
                                                                    <path class="cls-1" d="M43.69,20.86a2,2,0,0,0-2.83,0l-19,19a2,2,0,1,0,2.83,2.83l19-19A2,2,0,0,0,43.69,20.86Z" />
                                                                    <path class="cls-1" d="M60,32l3.64-5.58a2,2,0,0,0-.64-2.8l-5.79-3.5.8-6.57a2,2,0,0,0-1.78-2.23l-6.81-.71L47.24,4.31a2,2,0,0,0-2.54-1.22L38.22,5.33,33.4.58a2,2,0,0,0-2.81,0L25.77,5.33,19.3,3.08a2,2,0,0,0-2.54,1.22L14.55,10.6l-6.81.71A2,2,0,0,0,6,13.54l.8,6.57L1,23.61a2,2,0,0,0-.64,2.8L4,32,.32,37.58A2,2,0,0,0,1,40.39l5.79,3.5L6,50.46a2,2,0,0,0,1.78,2.23l6.81.71,2.21,6.29a2,2,0,0,0,2.54,1.23l6.47-2.25,4.82,4.75a2,2,0,0,0,2.81,0l4.82-4.75,6.47,2.25a2,2,0,0,0,2.54-1.23l2.21-6.29,6.81-.71A2,2,0,0,0,58,50.46l-.8-6.57L63,40.39a2,2,0,0,0,.64-2.8ZM56,33.09,59.19,38l-5.12,3.1a2,2,0,0,0-1,2l.71,5.84-6,.63a2,2,0,0,0-1.68,1.33l-2,5.6-5.76-2a2,2,0,0,0-2.06.46L32,59.19l-4.3-4.24a2,2,0,0,0-2.06-.46l-5.76,2-2-5.6a2,2,0,0,0-1.68-1.33l-6-.63.71-5.84a2,2,0,0,0-1-2L4.81,38,8,33.09a2,2,0,0,0,0-2.19L4.81,26l5.12-3.1a2,2,0,0,0,1-2l-.71-5.84,6-.63a2,2,0,0,0,1.68-1.32l2-5.61,5.76,2a2,2,0,0,0,2.06-.47L32,4.81l4.3,4.24a2,2,0,0,0,2.06.47l5.76-2,2,5.61a2,2,0,0,0,1.68,1.32l6,.63-.71,5.84a2,2,0,0,0,1,2L59.19,26,56,30.91A2,2,0,0,0,56,33.09Z" />
                                                                </g>
                                                            </g>
                                                        </svg>123.RU:Уценка</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="catalog__sub"><i class="close-menu material-icons">close</i>
                                            <div data-id="3" data-items-per-column="10">
                                                <ul>
                                                    <li><a href="/smartfoni_plansheti_gadjeti/telefoniya/" data-id="122">Смартфоны&nbsp;<span>487</span></a></li>
                                                    <li><a href="/smartfoni_plansheti_gadjeti/sotovie-telefoni/" data-id="12766">Телефоны&nbsp;<span>138</span></a></li>
                                                    <li><a href="/smartfoni_plansheti_gadjeti/apple/" data-id="681">Продукция Apple&nbsp;<span>2385</span></a>
                                                        <div data-id="681">
                                                            <ul>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/apple/macbook/" data-id="682" data-products-count="17">Ноутбуки Apple</a></li>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/apple/imac/" data-id="683" data-products-count="19">iMac</a></li>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/apple/mac-mini/" data-id="3343" data-products-count="7">Mac Mini</a></li>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/apple/ipad/" data-id="684" data-products-count="53">iPad</a></li>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/apple/telefony_apple/" data-id="3350" data-products-count="47">iPhone</a></li>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/apple/ipod/" data-id="686" data-products-count="9">iPod</a></li>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/apple/apple-tv/" data-id="12646" data-products-count="3">Apple TV</a></li>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/apple/time_capsule/" data-id="3357" data-products-count="1">Time Capsule</a></li>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/apple/ustroystva_vvoda_apple/" data-id="3682" data-products-count="9">Устройства ввода Apple</a></li>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/apple/aksessuf/" data-id="3358" data-products-count="1458">Аксессуары Apple</a></li>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/apple/sumki_i_chely_dlya_apple/" data-id="3621" data-products-count="759">Сумки,чехлы и защитные пленки для Apple</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/smartfoni_plansheti_gadjeti/clipboards/" data-id="149">Планшеты&nbsp;<span>186</span></a></li>
                                                    <li><a href="/smartfoni_plansheti_gadjeti/gadjeti/" data-id="12767">Гаджеты&nbsp;<span>41</span></a>
                                                        <div data-id="12767">
                                                            <ul>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/gadjeti/electronnaya-sigareta/" data-id="15233" data-products-count="16">Электронные сигареты</a></li>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/gadjeti/monokolesa/" data-id="12478" data-products-count="20">Моноколеса</a></li>
                                                                <li><a href="/smartfoni_plansheti_gadjeti/gadjeti/">Все гаджеты&nbsp;<span>41</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/smartfoni_plansheti_gadjeti/smart_chasi/" data-id="5">Smart-часы&nbsp;<span>73</span></a></li>
                                                    <li><a href="/smartfoni_plansheti_gadjeti/ebooks/" data-id="132">Электронные книги&nbsp;<span>2</span></a></li>
                                                    <li><a href="/smartfoni_plansheti_gadjeti/dect_telefoniya/" data-id="125">DECT-телефоны&nbsp;<span>115</span></a></li>
                                                    <li><a href="/smartfoni_plansheti_gadjeti/ofisnie_telefoni/" data-id="4031">Проводные телефоны&nbsp;<span>64</span></a></li>
                                                    <li><a href="/smartfoni_plansheti_gadjeti/aksesuari_dlya_smartfonov_planshetov/" data-id="4">Аксесуары для смартфонов/планшетов&nbsp;<span>3899</span></a>
                                                        <div data-id="4">
                                                            <ul>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/aksesuari_dlya_smartfonov_planshetov/aksessuary_dlya_telefonov/" data-id="126" data-products-count="2950">Аксессуары для телефонов</a></li>
                                                                <li class="leaf"><a href="/smartfoni_plansheti_gadjeti/aksesuari_dlya_smartfonov_planshetov/aksessuary_dlya_planshetov/" data-id="321" data-products-count="948">Аксессуары для планшетов</a></li>
                                                                <li><a href="/smartfoni_plansheti_gadjeti/aksesuari_dlya_smartfonov_planshetov/">Все аксесуары для смартфонов/планшетов&nbsp;<span>3899</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/smartfoni_plansheti_gadjeti/3gmodemy/" data-id="4033">Модемы 3G/4G&nbsp;<span>12</span></a></li>
                                                    <li><a href="/smartfoni_plansheti_gadjeti/radiostancii/" data-id="11816">Радиостанции&nbsp;<span>2</span></a></li>
                                                </ul>
                                            </div>
                                            <div data-id="146" data-items-per-column="11">
                                                <ul>
                                                    <li><a href="/kompyutery_noutbuki_soft/notebooks/" data-id="147">Ноутбуки&nbsp;<span>1918</span></a></li>
                                                    <li><a href="/kompyutery_noutbuki_soft/ultrabuki/" data-id="861">Ультрабуки&nbsp;<span>70</span></a></li>
                                                    <li><a href="/kompyutery_noutbuki_soft/desktopy/" data-id="13408">Системные блоки&nbsp;<span>299</span></a></li>
                                                    <li><a href="/kompyutery_noutbuki_soft/monitory/" data-id="351">Мониторы&nbsp;<span>918</span></a></li>
                                                    <li><a href="/kompyutery_noutbuki_soft/monoblocks/" data-id="150">Моноблоки&nbsp;<span>585</span></a></li>
                                                    <li><a href="/kompyutery_noutbuki_soft/nettopy/" data-id="151">Неттопы&nbsp;<span>183</span></a></li>
                                                    <li><a href="/kompyutery_noutbuki_soft/maining-ferma/" data-id="15774">Фермы&nbsp;<span>20</span></a></li>
                                                    <li><a href="/kompyutery_noutbuki_soft/kibersport/" data-id="15553">Киберспорт&nbsp;<span>954</span></a>
                                                        <div data-id="15553">
                                                            <ul>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/kibersport/igrovye-sistemnye-bloki/" data-id="15580" data-products-count="11">Игровые системные блоки</a></li>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/kibersport/igrovye-noutbuki/" data-id="15581" data-products-count="240">Игровые ноутбуки</a></li>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/kibersport/igrovye-garnitury-i-mikrofony/" data-id="15573" data-products-count="139">Игровые гарнитуры и микрофоны</a></li>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/kibersport/manipulyatory-igrovye/" data-id="15574" data-products-count="489">Манипуляторы</a></li>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/kibersport/gejmerskie-kresla/" data-id="15579" data-products-count="69">Геймерские кресла</a></li>
                                                                <li><a href="/kompyutery_noutbuki_soft/kibersport/">Все киберспорт&nbsp;<span>954</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/kompyutery_noutbuki_soft/aksessuary_notebook/" data-id="153">Аксессуары для ноутбуков, ультрабуков&nbsp;<span>3445</span></a>
                                                        <div data-id="153">
                                                            <ul>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/aksessuary_notebook/usb-fleshki/" data-id="11322" data-products-count="758">USB флешки</a></li>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/aksessuary_notebook/setevye_adaptery_pitaniya_i_zaryadnye_us/" data-id="160" data-products-count="225">Зарядные устройства для ноутбуков</a></li>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/aksessuary_notebook/dok_stantsiya/" data-id="161" data-products-count="26">Док-станция</a></li>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/aksessuary_notebook/cooling-stand-for-notebooks/" data-id="327" data-products-count="79">Подставки для ноутбуков</a></li>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/aksessuary_notebook/sumki_i_chehly_dlya_noutbukov/" data-id="11862" data-products-count="674">Сумки и чехлы для ноутбуков</a></li>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/aksessuary_notebook/trosy_bezopasnosti/" data-id="163" data-products-count="18">Тросы безопасности</a></li>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/aksessuary_notebook/interfeysnye_kabeli/" data-id="156" data-products-count="563">Интерфейсные кабели</a></li>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/aksessuary_notebook/chistyaschie_sredstva/" data-id="157" data-products-count="149">Чистящие средства</a></li>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/aksessuary_notebook/setevye_filtry/" data-id="688" data-products-count="696">Сетевые фильтры</a></li>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/aksessuary_notebook/akkukmulyatory_dlya_noutbukov/" data-id="329" data-products-count="235">Аккумуляторы для ноутбуков</a></li>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/aksessuary_notebook/3g_modemi_lte/" data-id="11807" data-products-count="12">3G модемы и LTE</a></li>
                                                                <li><a href="/kompyutery_noutbuki_soft/aksessuary_notebook/">Все аксессуары для ноутбуков, ультрабуков&nbsp;<span>3445</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/kompyutery_noutbuki_soft/software/" data-id="155">Программное обеспечение&nbsp;<span>54</span></a>
                                                        <div data-id="155">
                                                            <ul>
                                                                <li class="leaf"><a href="/kompyutery_noutbuki_soft/software/antivirusnye_programmy/" data-id="331" data-products-count="42">Антивирусные программы</a></li>
                                                                <li><a href="/kompyutery_noutbuki_soft/software/">Все программное обеспечение&nbsp;<span>54</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div data-id="41" data-items-per-column="16">
                                                <ul>
                                                    <li><a href="/komplektuyuschie_dlya_pk/videokarti/" data-id="12482">Видеокарты&nbsp;<span>389</span></a></li>
                                                    <li><a href="/komplektuyuschie_dlya_pk/protsessory/" data-id="421">Процессоры&nbsp;<span>202</span></a></li>
                                                    <li><a href="/komplektuyuschie_dlya_pk/operativnaya-pamyat/" data-id="422">Оперативная память&nbsp;<span>759</span></a>
                                                        <div data-id="422">
                                                            <ul>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/operativnaya-pamyat/dimm_ddr_dlya_pc/" data-id="541" data-products-count="621">Оперативная память для компьютера</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/operativnaya-pamyat/so-dimm_dlya_noutbuka/" data-id="542" data-products-count="138">Оперативная память для ноутбука</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/komplektuyuschie_dlya_pk/materinskie_platy/" data-id="42">Материнские платы&nbsp;<span>404</span></a></li>
                                                    <li><a href="/komplektuyuschie_dlya_pk/zhestkie_diski/" data-id="481">Жесткие диски&nbsp;<span>1235</span></a>
                                                        <div data-id="481">
                                                            <ul>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/zhestkie_diski/hdd_25/" data-id="3588" data-products-count="40">Жесткие диски для ноутбуков</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/zhestkie_diski/hdd_35/" data-id="3587" data-products-count="151">Жесткие диски для компьютера</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/zhestkie_diski/zhestkie_diski_hdd/" data-id="483" data-products-count="357">Серверные жесткие диски</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/zhestkie_diski/ssd-diski/" data-id="12547" data-products-count="426">SSD диски</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/zhestkie_diski/aksessuary_dlya_hdd/" data-id="485" data-products-count="261">Аксессуары для жестких дисков</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/komplektuyuschie_dlya_pk/bloki-pitaniya/" data-id="12507">Блоки питания&nbsp;<span>554</span></a></li>
                                                    <li><a href="/komplektuyuschie_dlya_pk/korpusa/" data-id="543">Корпуса для компьютеров&nbsp;<span>807</span></a></li>
                                                    <li><a href="/komplektuyuschie_dlya_pk/kontrollery/" data-id="621">Контроллеры&nbsp;<span>104</span></a></li>
                                                    <li><a href="/komplektuyuschie_dlya_pk/kulery_dlya_kompyutera/" data-id="545">Системы охлаждения&nbsp;<span>531</span></a>
                                                        <div data-id="545">
                                                            <ul>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/kulery_dlya_kompyutera/sistemy-ohlazhdeniya/" data-id="626" data-products-count="220">Охлаждение для корпусов</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/kulery_dlya_kompyutera/ohlajdenie_dlya_processora/" data-id="12483" data-products-count="264">Охлаждение для процессоров</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/kulery_dlya_kompyutera/termopasty-termoklei/" data-id="14478" data-products-count="29">Термопасты, термоклеи</a></li>
                                                                <li><a href="/komplektuyuschie_dlya_pk/kulery_dlya_kompyutera/">Все системы охлаждения&nbsp;<span>531</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/komplektuyuschie_dlya_pk/dvd-rw-bd-rw/" data-id="661">Оптические накопители&nbsp;<span>147</span></a>
                                                        <div data-id="661">
                                                            <ul>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/dvd-rw-bd-rw/nacopiteli_dlya_pc/" data-id="662" data-products-count="14">Оптические накопители для ПК</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/dvd-rw-bd-rw/optical-storage/" data-id="664" data-products-count="28">Внешние оптические накопители</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/dvd-rw-bd-rw/kartridery/" data-id="665" data-products-count="100">Картридеры</a></li>
                                                                <li><a href="/komplektuyuschie_dlya_pk/dvd-rw-bd-rw/">Все оптические накопители&nbsp;<span>147</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/komplektuyuschie_dlya_pk/networkcards/" data-id="546">Сетевые карты&nbsp;<span>45</span></a></li>
                                                    <li><a href="/komplektuyuschie_dlya_pk/zvukovye_karty/" data-id="547">Звуковые карты&nbsp;<span>32</span></a></li>
                                                    <li><a href="/komplektuyuschie_dlya_pk/tv-tuneri/" data-id="1601">TV-тюнеры&nbsp;<span>4</span></a>
                                                        <div data-id="1601">
                                                            <ul>
                                                                <li><a href="/komplektuyuschie_dlya_pk/tv-tuneri/">Все tv-тюнеры&nbsp;<span>4</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/komplektuyuschie_dlya_pk/servernoe-oborudovanie/" data-id="549">Серверное оборудование&nbsp;<span>1961</span></a>
                                                        <div data-id="549">
                                                            <ul>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/servernoe-oborudovanie/servernye_korpusa/" data-id="551" data-products-count="118">Серверные корпуса</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/servernoe-oborudovanie/materinskie_platy/" data-id="552" data-products-count="92">Серверные материнские платы</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/servernoe-oborudovanie/server-processors/" data-id="553" data-products-count="131">Серверные процессоры</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/servernoe-oborudovanie/servers-memory/" data-id="554" data-products-count="122">Серверная оперативная память</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/servernoe-oborudovanie/servernie_kontrollery/" data-id="557" data-products-count="96">Серверные контроллеры</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/servernoe-oborudovanie/accessories-for-servers/" data-id="558" data-products-count="666">Аксессуары для серверов</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/servernoe-oborudovanie/servers/" data-id="1806" data-products-count="447">Серверы</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/servernoe-oborudovanie/severnie-shkafi/" data-id="12586" data-products-count="155">Серверные шкафы</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/servernoe-oborudovanie/servernie-bloki-pitaniya/" data-id="13288" data-products-count="85">Блоки питания</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/servernoe-oborudovanie/diskovie-polky/" data-id="15107" data-products-count="49">Дисковые полки</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/komplektuyuschie_dlya_pk/kabeli-i-shleifi/" data-id="12529">Кабели и переходники&nbsp;<span>813</span></a>
                                                        <div data-id="12529">
                                                            <ul>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/kabeli-i-shleifi/kabeli-i-perehodniki-usb/" data-id="12588" data-products-count="658">Кабели и переходники USB</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/kabeli-i-shleifi/kabeli-pitaniya/" data-id="12601" data-products-count="88">Кабели питания</a></li>
                                                                <li class="leaf"><a href="/komplektuyuschie_dlya_pk/kabeli-i-shleifi/shleifi-i-perehodniki/" data-id="12604" data-products-count="67">Шлейфы и переходники</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div data-id="341" data-items-per-column="17">
                                                <ul>
                                                    <li><a href="/komputernaya_periferiya/input-devices-mouse-steering-wheels-dzhostik/" data-id="344">Устройства ввода&nbsp;<span>1324</span></a>
                                                        <div data-id="344">
                                                            <ul>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/input-devices-mouse-steering-wheels-dzhostik/mouse/" data-id="349" data-products-count="859">Мыши</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/input-devices-mouse-steering-wheels-dzhostik/keyboards/" data-id="348" data-products-count="308">Клавиатуры</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/input-devices-mouse-steering-wheels-dzhostik/komplekt_klaviatura_mysh/" data-id="350" data-products-count="87">Комплект клавиатура + мышь</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/input-devices-mouse-steering-wheels-dzhostik/graphic-tablets1/" data-id="354" data-products-count="39">Графические планшеты</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/input-devices-mouse-steering-wheels-dzhostik/pulty_du/" data-id="4028" data-products-count="26">Пульты ДУ</a></li>
                                                                <li><a href="/komputernaya_periferiya/input-devices-mouse-steering-wheels-dzhostik/">Все устройства ввода&nbsp;<span>1324</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/komputernaya_periferiya/storage-media/" data-id="355">Носители информации&nbsp;<span>1616</span></a>
                                                        <div data-id="355">
                                                            <ul>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/storage-media/sistemi_hraneniya_dannih_shd/" data-id="54" data-products-count="44">Системы хранения данных (СХД)</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/storage-media/external-hard-drives/" data-id="381" data-products-count="308">Внешние жесткие диски</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/storage-media/flash-drives1/" data-id="382" data-products-count="758">USB Флешки</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/storage-media/diski_i_diskety/" data-id="383" data-products-count="80">Диски и дискеты</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/storage-media/memory-cards/" data-id="384" data-products-count="272">Карты памяти</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/storage-media/portmone_i_konverti/" data-id="11499" data-products-count="54">Конверты и портмоне для дисков</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/storage-media/cardreaders/" data-id="13430" data-products-count="100">Картридеры</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/komputernaya_periferiya/zaschita_pitaniya/" data-id="342">Защита питания&nbsp;<span>1721</span></a>
                                                        <div data-id="342">
                                                            <ul>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/zaschita_pitaniya/istochniki_bespereboynogo_pitaniya/" data-id="343" data-products-count="475">Источники бесперебойного питания (ИБП)</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/zaschita_pitaniya/setevye_filtry/" data-id="379" data-products-count="696">Сетевые фильтры и удлинители</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/zaschita_pitaniya/stabilizatory_napryazheniya/" data-id="105" data-products-count="243">Стабилизаторы напряжения</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/zaschita_pitaniya/akkumulyatory/" data-id="102" data-products-count="214">Батареи для ИБП</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/zaschita_pitaniya/komplekty_dlya_montazha/" data-id="3675" data-products-count="93">Комплекты для монтажа</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/komputernaya_periferiya/ustroystva_pechati/" data-id="352">Устройства печати, расходные материалы&nbsp;<span>7286</span></a>
                                                        <div data-id="352">
                                                            <ul>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/ustroystva_pechati/mfu/" data-id="666" data-products-count="261">МФУ</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/ustroystva_pechati/printery/" data-id="641" data-products-count="218">Принтеры</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/ustroystva_pechati/kopiry/" data-id="669" data-products-count="14">Копиры</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/ustroystva_pechati/plottery/" data-id="14913" data-products-count="17">Плоттеры</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/ustroystva_pechati/skanery/" data-id="670" data-products-count="85">Сканеры</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/ustroystva_pechati/optsii/" data-id="668" data-products-count="180">Опции для принтеров, копиров и МФУ</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/ustroystva_pechati/pechatnye_nositeli/" data-id="3741" data-products-count="613">Печатные носители</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/ustroystva_pechati/toners_chernila_zip/" data-id="3761" data-products-count="786">Расходные материалы для принтеров</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/ustroystva_pechati/plenka_dlya_laminirovaniya/" data-id="12485" data-products-count="87">Пленка для ламинирования</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/ustroystva_pechati/cartridji/" data-id="12506" data-products-count="4858">Картриджи</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/ustroystva_pechati/rashodka-dlya-broshuratorov/" data-id="12508" data-products-count="129">Расходка для брошюраторов</a></li>
                                                                <li><a href="/komputernaya_periferiya/ustroystva_pechati/">Все устройства печати, расходные материалы&nbsp;<span>7286</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/komputernaya_periferiya/akusticheskie_sistemy/" data-id="356">Акустические системы&nbsp;<span>764</span></a>
                                                        <div data-id="356">
                                                            <ul>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/akusticheskie_sistemy/akusticheskie_sistemy_2_0/" data-id="385" data-products-count="266">Акустические системы 2.0</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/akusticheskie_sistemy/akusticheskie_sistemy_2_1/" data-id="386" data-products-count="155">Акустические системы 2.1</a></li>
                                                                <li class="leaf"><a href="/komputernaya_periferiya/akusticheskie_sistemy/portativnye_kolonki/" data-id="633" data-products-count="333">Портативные колонки</a></li>
                                                                <li><a href="/komputernaya_periferiya/akusticheskie_sistemy/">Все акустические системы&nbsp;<span>764</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/komputernaya_periferiya/webcam/" data-id="361">Вэб-камеры&nbsp;<span>65</span></a></li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/komputernaya_periferiya/garnitury/" data-id="362">Наушники и гарнитуры для компьютера&nbsp;<span>1124</span></a></li>
                                                    <li><a href="/komputernaya_periferiya/mikrofony/" data-id="3696">Микрофоны&nbsp;<span>49</span></a></li>
                                                    <li><a href="/komputernaya_periferiya/batareyki/" data-id="4026">Батарейки и аккумуляторы&nbsp;<span>470</span></a></li>
                                                    <li><a href="/komputernaya_periferiya/kardridery/" data-id="158">Карт-ридеры&nbsp;<span>100</span></a></li>
                                                    <li><a href="/komputernaya_periferiya/usb_igrushki/" data-id="11388">USB игрушки&nbsp;<span>14</span></a></li>
                                                    <li><a href="/komputernaya_periferiya/kovriki_dlya_mishsi/" data-id="12484">Коврики для мыши&nbsp;<span>202</span></a></li>
                                                </ul>
                                            </div>
                                            <div data-id="353" data-items-per-column="18">
                                                <ul>
                                                    <li><a href="/communication-equipment/besprovodnoe_setevoe_oborudovanie/" data-id="49">Беспроводное сетевое оборудование&nbsp;<span>510</span></a>
                                                        <div data-id="49">
                                                            <ul>
                                                                <li class="leaf"><a href="/communication-equipment/besprovodnoe_setevoe_oborudovanie/tochki_dosutpa/" data-id="373" data-products-count="359">Wi-Fi точки доступа</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/besprovodnoe_setevoe_oborudovanie/wi-fi-retraslyatory/" data-id="15109" data-products-count="26">Wi-Fi ретрансляторы</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/besprovodnoe_setevoe_oborudovanie/adapnety-poe/" data-id="15494" data-products-count="34">Адаптеры PoE</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/besprovodnoe_setevoe_oborudovanie/powerline/" data-id="3697" data-products-count="23">Оборудование Powerline</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/besprovodnoe_setevoe_oborudovanie/3gmodemy/" data-id="3706" data-products-count="12">Модемы 3G/4G</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/besprovodnoe_setevoe_oborudovanie/wi_fi_antenny/" data-id="369" data-products-count="46">Антенны</a></li>
                                                                <li><a href="/communication-equipment/besprovodnoe_setevoe_oborudovanie/">Все беспроводное сетевое оборудование&nbsp;<span>510</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/communication-equipment/provodnoe_setevoe_oborudovanie/" data-id="50">Проводное сетевое оборудование&nbsp;<span>583</span></a>
                                                        <div data-id="50">
                                                            <ul>
                                                                <li class="leaf"><a href="/communication-equipment/provodnoe_setevoe_oborudovanie/kommutatory_ethernet/" data-id="367" data-products-count="417">Коммутаторы</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/provodnoe_setevoe_oborudovanie/routers-wifi/" data-id="372" data-products-count="52">Маршрутизаторы</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/provodnoe_setevoe_oborudovanie/adsl_oborudovanie/" data-id="368" data-products-count="14">Модемы</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/provodnoe_setevoe_oborudovanie/adaptery/" data-id="371" data-products-count="100">Адаптеры</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/communication-equipment/passivnoe_setevoe_oborudovanie/" data-id="51">Пассивное сетевое оборудование&nbsp;<span>2412</span></a>
                                                        <div data-id="51">
                                                            <ul>
                                                                <li class="leaf"><a href="/communication-equipment/passivnoe_setevoe_oborudovanie/instrumenty/" data-id="365" data-products-count="207">Инструменты</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/passivnoe_setevoe_oborudovanie/external-network-storage/" data-id="366" data-products-count="148">Внешние сетевые хранилища</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/passivnoe_setevoe_oborudovanie/krepej_soedeniteli_kabelya/" data-id="647" data-products-count="385">Крепеж, соеденители кабеля</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/passivnoe_setevoe_oborudovanie/kabel_dlya_montaja_sistem_svyazi_i_signalizacii/" data-id="655" data-products-count="66">Кабель для монтажа систем связи и сигнализации</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/passivnoe_setevoe_oborudovanie/kabeli_patchcord/" data-id="363" data-products-count="703">Кабели сетевые(Патч-корд)</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/passivnoe_setevoe_oborudovanie/konnektori/" data-id="12134" data-products-count="24">Коннекторы</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/passivnoe_setevoe_oborudovanie/rozetki-setevie/" data-id="13313" data-products-count="46">Розетки сетевые</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/passivnoe_setevoe_oborudovanie/kabeli-v-buhtah/" data-id="13488" data-products-count="200">Кабель в бухтах</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/passivnoe_setevoe_oborudovanie/markeri_dlya_kabelya/" data-id="474" data-products-count="24">Маркеры для кабеля</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/passivnoe_setevoe_oborudovanie/kommunikacionnie-shkafi/" data-id="13289" data-products-count="605">Коммуникационные шкафы</a></li>
                                                                <li><a href="/communication-equipment/passivnoe_setevoe_oborudovanie/">Все пассивное сетевое оборудование&nbsp;<span>2412</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/communication-equipment/ip-phone/" data-id="3699">IP телефония&nbsp;<span>133</span></a>
                                                        <div data-id="3699">
                                                            <ul>
                                                                <li class="leaf"><a href="/communication-equipment/ip-phone/ip-phone/" data-id="376" data-products-count="88">IP-телефоны</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/ip-phone/bloki_pitaniya_voip/" data-id="3700" data-products-count="14">Блоки питания для IP телефонов</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/ip-phone/garnitury_dlya_voip/" data-id="3701" data-products-count="15">Гарнитуры для IP телефонов</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/ip-phone/shlyuzy_voip/" data-id="3704" data-products-count="12">Шлюзы для IP телефонии</a></li>
                                                                <li><a href="/communication-equipment/ip-phone/">Все ip телефония&nbsp;<span>133</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/communication-equipment/perekluchateli_perehodniki_moduli/" data-id="52">Переключатели, переходники, модули&nbsp;<span>322</span></a>
                                                        <div data-id="52">
                                                            <ul>
                                                                <li class="leaf"><a href="/communication-equipment/perekluchateli_perehodniki_moduli/kvm/" data-id="364" data-products-count="142">KVM-переключатели</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/perekluchateli_perehodniki_moduli/moduli_sfp/" data-id="375" data-products-count="106">Модули SFP</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/perekluchateli_perehodniki_moduli/mediakonvertery/" data-id="370" data-products-count="55">Медиаконвертеры</a></li>
                                                                <li><a href="/communication-equipment/perekluchateli_perehodniki_moduli/">Все переключатели, переходники, модули&nbsp;<span>322</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/communication-equipment/prochee_setevoe_oborudovanie/" data-id="53">Прочее сетевое оборудование&nbsp;<span>318</span></a>
                                                        <div data-id="53">
                                                            <ul>
                                                                <li class="leaf"><a href="/communication-equipment/prochee_setevoe_oborudovanie/oborudovanie-dlya-peredachi-signala/" data-id="15293" data-products-count="169">Оборудование для передачи сигнала</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/prochee_setevoe_oborudovanie/setevye-ustrojstva-razlichnogo-naznacheniya/" data-id="15354" data-products-count="63">Сетевые устройства различного назначения</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/prochee_setevoe_oborudovanie/gradozashita/" data-id="15698" data-products-count="39">Грозозащита</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/prochee_setevoe_oborudovanie/videousiliteli-i-videokommutatory/" data-id="15814" data-products-count="47">Видеоусилители и видеокоммутаторы</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/communication-equipment/setevie_vidoeregistratori/" data-id="11482">Видеонаблюдение&nbsp;<span>742</span></a>
                                                        <div data-id="11482">
                                                            <ul>
                                                                <li class="leaf"><a href="/communication-equipment/setevie_vidoeregistratori/gotovie_komplekti/" data-id="55" data-products-count="14">Готовые комплекты</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/setevie_vidoeregistratori/kameri_videonabludeniya/" data-id="57" data-products-count="99">Камеры видеонаблюдения</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/setevie_vidoeregistratori/ip_camers/" data-id="4023" data-products-count="315">IP камеры</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/setevie_vidoeregistratori/imitaciya_videonabludeniya/" data-id="56" data-products-count="12">Имитация видеонаблюдения</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/setevie_vidoeregistratori/domofoni_i_videoglazki/" data-id="59" data-products-count="51">Домофоны и видеоглазки</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/setevie_vidoeregistratori/videoregistratori/" data-id="60" data-products-count="85">Видеорегистраторы</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/setevie_vidoeregistratori/kronshteyni_dlya_kamer_videonabludeniya/" data-id="62" data-products-count="79">Кронштейны для камер видеонаблюдения</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/setevie_vidoeregistratori/aksessuari_dlya_videonabludeniya/" data-id="58" data-products-count="57">Аксессуары для видеонаблюдения</a></li>
                                                                <li class="leaf"><a href="/communication-equipment/setevie_vidoeregistratori/bloki_pitaniya_dlya_videokamer/" data-id="66" data-products-count="30">Блоки питания для видеокамер</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div data-id="84" data-items-per-column="20">
                                                <ul>
                                                    <li><a href="/audio-video_i_tv/televizori/" data-id="12481">Телевизоры&nbsp;<span>380</span></a></li>
                                                    <li><a href="/audio-video_i_tv/aksessuary_dlya_televizorov/" data-id="85">Аксессуары для телевизоров&nbsp;<span>1026</span></a>
                                                        <div data-id="85">
                                                            <ul>
                                                                <li class="leaf"><a href="/audio-video_i_tv/aksessuary_dlya_televizorov/perehodniki_i_konnektori_dlya_tv/" data-id="397" data-products-count="158">Переходники и коннекторы для TV</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/aksessuary_dlya_televizorov/kronshteyny/" data-id="108" data-products-count="502">Кронштейны</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/aksessuary_dlya_televizorov/pulti_du/" data-id="4241" data-products-count="28">Пульты ДУ</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/aksessuary_dlya_televizorov/cleaning_goods/" data-id="4245" data-products-count="149">Чистящие средства</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/aksessuary_dlya_televizorov/tv_tyunery/" data-id="89" data-products-count="80">TV-тюнеры</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/aksessuary_dlya_televizorov/antenny/" data-id="15613" data-products-count="109">Антенны</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/audio-video_i_tv/igrovye_pristavki/" data-id="145">Игровые приставки&nbsp;<span>6</span></a></li>
                                                    <li><a href="/audio-video_i_tv/proektsionnoe_oborudovanie/" data-id="92">Проекционное оборудование&nbsp;<span>377</span></a>
                                                        <div data-id="92">
                                                            <ul>
                                                                <li class="leaf"><a href="/audio-video_i_tv/proektsionnoe_oborudovanie/proektsionnoe_oborudovanie/" data-id="116" data-products-count="151">Проекторы</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/proektsionnoe_oborudovanie/ekrany_na_shtative/" data-id="111" data-products-count="156">Экраны для проекторов</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/proektsionnoe_oborudovanie/aksessuary_dlja_proekcionnogo_oborudovanija/" data-id="4027" data-products-count="64">Кронштейны для проекторов</a></li>
                                                                <li><a href="/audio-video_i_tv/proektsionnoe_oborudovanie/">Все проекционное оборудование&nbsp;<span>377</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/audio-video_i_tv/mediaplayers/" data-id="90">Мультимедийные плееры&nbsp;<span>2</span></a></li>
                                                    <li><a href="/audio-video_i_tv/kabeli_dlja_video/" data-id="4021">Кабели для видео&nbsp;<span>844</span></a>
                                                        <div data-id="4021">
                                                            <ul>
                                                                <li class="leaf"><a href="/audio-video_i_tv/kabeli_dlja_video/kabeli-hdmi-hdmi/" data-id="13550" data-products-count="390">HDMI-HDMI</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/kabeli_dlja_video/kabeli-hdmi-dvi/" data-id="13551" data-products-count="52">HDMI-DVI</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/kabeli_dlja_video/kabeli-2rca-2rca/" data-id="13553" data-products-count="21">2RCA-2RCA</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/kabeli_dlja_video/kabeli-3rca-3rca/" data-id="13552" data-products-count="20">3RCA-3RCA</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/kabeli_dlja_video/kabeli-vga-vga/" data-id="13554" data-products-count="130">VGA-VGA</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/kabeli_dlja_video/kabeli-scart-3rca/" data-id="13556" data-products-count="13">SCART-3RCA</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/kabeli_dlja_video/kabeli-dvi-dvi/" data-id="13557" data-products-count="38">DVI-DVI</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/kabeli_dlja_video/kabeli-hdmi-microhdmi/" data-id="13558" data-products-count="44">HDMI - microHDMI</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/kabeli_dlja_video/kabeli-displayport/" data-id="13560" data-products-count="106">DisplayPort</a></li>
                                                                <li><a href="/audio-video_i_tv/kabeli_dlja_video/">Все кабели для видео&nbsp;<span>844</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/audio-video_i_tv/aksessuary_dlya_igrovykh_pristavok/" data-id="3676">Аксессуары для игровых приставок&nbsp;<span>6</span></a>
                                                        <div data-id="3676">
                                                            <ul>
                                                                <li><a href="/audio-video_i_tv/aksessuary_dlya_igrovykh_pristavok/">Все аксессуары для игровых приставок&nbsp;<span>6</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/audio-video_i_tv/audio-technika/" data-id="12768">Аудио техника&nbsp;<span>1049</span></a>
                                                        <div data-id="12768">
                                                            <ul>
                                                                <li class="leaf"><a href="/audio-video_i_tv/audio-technika/mp3/" data-id="143" data-products-count="53">MP3-плееры</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/audio-technika/naushniki/" data-id="11861" data-products-count="581">Наушники</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/audio-technika/muzykalnye_tsentry/" data-id="96" data-products-count="62">Музыкальные центры</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/audio-technika/magnitoly/" data-id="98" data-products-count="97">Магнитолы</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/audio-technika/domashnie_kinoteatry/" data-id="95" data-products-count="11">Домашние кинотеатры</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/audio-technika/radiobudilniki/" data-id="11225" data-products-count="164">Радиобудильники</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/audio-technika/hi_fi_akustika/" data-id="3673" data-products-count="24">Hi-Fi акустика</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/audio-technika/proigrovateli/" data-id="3641" data-products-count="32">Проигрыватели DVD и Blu-Ray</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/audio-technika/diktofony/" data-id="131" data-products-count="13">Диктофоны</a></li>
                                                                <li><a href="/audio-video_i_tv/audio-technika/">Все аудио техника&nbsp;<span>1049</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/audio-video_i_tv/tsifrovoe_tv/" data-id="88">Оборудование для спутникового тв&nbsp;<span>13</span></a></li>
                                                    <li><a href="/audio-video_i_tv/kabeli_antennye_i_televizionnye/" data-id="87">Кабели антенные и телевизионные&nbsp;<span>49</span></a></li>
                                                    <li><a href="/audio-video_i_tv/kabeli_dlya_audio/" data-id="94">Кабели для аудио&nbsp;<span>331</span></a></li>
                                                    <li><a href="/audio-video_i_tv/elektronika_i_foto/" data-id="63">Фото и видео&nbsp;<span>558</span></a>
                                                        <div data-id="63">
                                                            <ul>
                                                                <li class="leaf"><a href="/audio-video_i_tv/elektronika_i_foto/soap/" data-id="76" data-products-count="91">Компактные фотоаппараты</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/elektronika_i_foto/mirror/" data-id="64" data-products-count="28">Зеркальные фотоаппараты</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/elektronika_i_foto/camcorders/" data-id="81" data-products-count="15">Видеокамеры</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/elektronika_i_foto/action-camera/" data-id="4024" data-products-count="36">Экшен-камеры</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/elektronika_i_foto/lenses/" data-id="107" data-products-count="84">Объективы</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/elektronika_i_foto/aksessuary_dlya_fotoapparatov/" data-id="121" data-products-count="164">Аксессуары для фотоаппаратов</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/elektronika_i_foto/aksessuary_dlja_jekshen-kamer/" data-id="4025" data-products-count="11">Аксессуары для экшен-камер</a></li>
                                                                <li class="leaf"><a href="/audio-video_i_tv/elektronika_i_foto/kamery_videonablyudeniya/" data-id="13309" data-products-count="125">Камеры видеонаблюдения</a></li>
                                                                <li><a href="/audio-video_i_tv/elektronika_i_foto/">Все фото и видео&nbsp;<span>558</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div data-id="7" data-items-per-column="19">
                                                <ul>
                                                    <li><a href="/vse_dlya_doma_i_sada/sad_i_ogorod/" data-id="8">Сад и огород&nbsp;<span>1769</span></a>
                                                        <div data-id="8">
                                                            <ul>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/sad_i_ogorod/saovaya_technica/" data-id="11703" data-products-count="1445">Садовая техника</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/sad_i_ogorod/fonari/" data-id="11679" data-products-count="201">Фонари</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/sad_i_ogorod/sadovyi-inventary/" data-id="15533" data-products-count="89">Садовый инвентарь</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/sad_i_ogorod/ystroystva_dlya_borbi_s_vreditelyami/" data-id="646" data-products-count="34">Устройства для борьбы с вредителями</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/vse_dlya_doma_i_sada/posuda/" data-id="11362">Посуда&nbsp;<span>2796</span></a>
                                                        <div data-id="11362">
                                                            <ul>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/nozhi_tochilki/" data-id="11375" data-products-count="123">Кухонные ножи</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/chayniki/" data-id="457" data-products-count="114">Чайники</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/tochilki_dlya_nojey/" data-id="469" data-products-count="15">Точилки для ножей</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/vse_dlya_chaya/" data-id="11372" data-products-count="236">Все для чая</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/nabory_posudy/" data-id="11364" data-products-count="152">Наборы посуды</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/kastruli/" data-id="11365" data-products-count="567">Кастрюли</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/kazani/" data-id="12889" data-products-count="18">Казаны</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/kovshi/" data-id="11366" data-products-count="63">Ковши</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/kryshki/" data-id="11367" data-products-count="62">Крышки</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/skovorody/" data-id="11368" data-products-count="654">Сковороды</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/soteiniki/" data-id="11369" data-products-count="69">Сотейники</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/kuhonnyi_pribor/" data-id="11371" data-products-count="400">Кухонные приборы</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/stolovie_pribori/" data-id="297" data-products-count="12">Столовые приборы</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/protivni/" data-id="11486" data-products-count="15">Противни</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/farfor/" data-id="300" data-products-count="104">Фарфор</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/formi_dlya_zapekaniz/" data-id="11487" data-products-count="130">Формы для запекания</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/posuda/doski_razdelochnie/" data-id="11601" data-products-count="47">Доски разделочные</a></li>
                                                                <li><a href="/vse_dlya_doma_i_sada/posuda/">Все посуда&nbsp;<span>2796</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/vse_dlya_doma_i_sada/turizm_i_otdih/" data-id="12397">Туризм и отдых&nbsp;<span>856</span></a>
                                                        <div data-id="12397">
                                                            <ul>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/turizm_i_otdih/tovary-dlya-bani-i-sayni/" data-id="14634" data-products-count="243">Товары для бани и сауны</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/turizm_i_otdih/termosi-termokrujki/" data-id="14481" data-products-count="302">Термосы, термокружки и сумки-термосы</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/turizm_i_otdih/dorojnie-i-sportivnie-sumki/" data-id="12399" data-products-count="51">Дорожные и спортивные сумки</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/turizm_i_otdih/mebel-dlya-aktivnogo-otdiha/" data-id="12400" data-products-count="20">Мебель для активного отдыха</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/turizm_i_otdih/nabor-dlya-piknika/" data-id="12401" data-products-count="25">Наборы для пикника</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/turizm_i_otdih/shepa-dlya-kopcheniya/" data-id="12868" data-products-count="14">Щепа для копчения</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/turizm_i_otdih/shampuri-nabori-shampurov/" data-id="12869" data-products-count="26">Шампуры, наборы шампуров</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/turizm_i_otdih/posuda-odnorazovaya/" data-id="12870" data-products-count="12">Посуда одноразовая</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/turizm_i_otdih/mangali-koptilni/" data-id="12872" data-products-count="43">Мангалы.Коптильни</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/turizm_i_otdih/grili-reshetki-jarovni/" data-id="12873" data-products-count="63">Грили.Решетки.Жаровни</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/turizm_i_otdih/vse-dlya-rozjiga/" data-id="12874" data-products-count="37">Все для розжига</a></li>
                                                                <li><a href="/vse_dlya_doma_i_sada/turizm_i_otdih/">Все туризм и отдых&nbsp;<span>856</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/vse_dlya_doma_i_sada/lyustry_i_svetilniki/" data-id="15113">Люстры и светильники&nbsp;<span>1467</span></a>
                                                        <div data-id="15113">
                                                            <ul>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/lyustry_i_svetilniki/lustry/" data-id="15173" data-products-count="17">Люстры</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/lyustry_i_svetilniki/svetilniki/" data-id="15193" data-products-count="83">Светильники</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/lyustry_i_svetilniki/bra_i_podsvetki/" data-id="15115" data-products-count="12">Бра и подсветки</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/lyustry_i_svetilniki/lampy_nastolnye/" data-id="15132" data-products-count="137">Лампы настольные</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/lyustry_i_svetilniki/lampy/" data-id="15140" data-products-count="1041">Лампы</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/lyustry_i_svetilniki/ulichnye_svetilniki/" data-id="15155" data-products-count="170">Уличные светильники</a></li>
                                                                <li><a href="/vse_dlya_doma_i_sada/lyustry_i_svetilniki/">Все люстры и светильники&nbsp;<span>1467</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/vse_dlya_doma_i_sada/tv_shop/" data-id="216">ТВ-ШОП&nbsp;<span>1234</span></a>
                                                        <div data-id="216">
                                                            <ul>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/tv_shop/suveniri/" data-id="261" data-products-count="51">Сувениры</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/tv_shop/tovari_dlya_doma/" data-id="262" data-products-count="178">Товары для дома</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/tv_shop/tovari_dlya_krasoti_i_zdorovya/" data-id="272" data-products-count="194">Товары для красоты и здоровья</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/tv_shop/tovari_dlya_kuhni/" data-id="281" data-products-count="123">Товары для кухни</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/tv_shop/tovari_dlya_fitnesa_i_sporta/" data-id="292" data-products-count="215">Товары для фитнеса и спорта</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/tv_shop/aksessuari/" data-id="217" data-products-count="309">Аксессуары</a></li>
                                                                <li class="leaf"><a href="/vse_dlya_doma_i_sada/tv_shop/detskiy_transport/" data-id="246" data-products-count="164">Детский транспорт</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div data-id="9" data-items-per-column="47">
                                                <ul>
                                                    <li><a href="/remont_i_stroitelstvo/electrika/" data-id="12808">Электрика&nbsp;<span>2380</span></a>
                                                        <div data-id="12808">
                                                            <ul>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/electrika/knopki_tumbleri_vikluchateli/" data-id="359" data-products-count="280">Кнопки, тумблеры, выключатели</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/electrika/datchik-dvijeniya/" data-id="12809" data-products-count="12">Датчики движения</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/electrika/zvonki/" data-id="12810" data-products-count="38">Звонки</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/electrika/kabel-kanali-gofririvannie-trubi/" data-id="12811" data-products-count="368">Кабель, кабель-каналы, гофрированные трубы</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/electrika/rozetki-vilkuchateli-dimmeri/" data-id="12817" data-products-count="1049">Розетки, выключатели, диммеры</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/electrika/electrika-soputstvushie-tovari/" data-id="12827" data-products-count="57">Сопутствующие товары</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/electrika/shitki-avtomati-schetchiki/" data-id="12839" data-products-count="390">Щитки, автоматика, счетчики</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/electrika/koroba-uly-plintusy/" data-id="15110" data-products-count="106">Короба, углы и плинтусы</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/electrika/komplektuyushhie_k_elektricheskim_shkafam/" data-id="15255" data-products-count="75">Комплектующие к электрическим шкафам</a></li>
                                                                <li><a href="/remont_i_stroitelstvo/electrika/">Все электрика&nbsp;<span>2380</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/" data-id="77">Электроинструмент&nbsp;<span>3551</span></a>
                                                        <div data-id="77">
                                                            <ul>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/dreli_perforatori_otboinie_molotki/" data-id="11664" data-products-count="486">Дрели.Перфораторы.Отбойные молотки</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/shurupoverti_vintoverti_gaikoverti/" data-id="11681" data-products-count="575">Шуруповерты.Винтоверты.Гайковерты</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/pili_i_lobziki/" data-id="11670" data-products-count="534">Пилы и лобзики</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/shlifovalnyye_i_polirovalnyye_mashiny/" data-id="244" data-products-count="540">Шлифовальные и полировальные машины</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/feny_i_termopistolety/" data-id="11697" data-products-count="112">Фены и термопистолеты</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/graveri/" data-id="11662" data-products-count="28">Граверы</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/izmeritelnii_instrument/" data-id="11668" data-products-count="683">Измерительный инструмент</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/kraskopulti/" data-id="11669" data-products-count="78">Краскопульты</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/rubanki/" data-id="11676" data-products-count="68">Рубанки</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/payalniki/" data-id="395" data-products-count="124">Паяльники</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/stepleri_stroitelnie/" data-id="11677" data-products-count="65">Степлеры строительные</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/tochila/" data-id="11678" data-products-count="83">Точила</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/frezeri/" data-id="11680" data-products-count="50">Фрезеры</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/promishlennie_pilesosi/" data-id="11741" data-products-count="51">Промышленные пылесосы</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/vityajnie-ventilyatory/" data-id="13530" data-products-count="74">Вытяжные вентиляторы</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/remont_i_stroitelstvo/pnevmaticheskoe_oborudovanie/" data-id="419">Пневматическое оборудование&nbsp;<span>369</span></a>
                                                        <div data-id="419">
                                                            <ul>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/pnevmaticheskoe_oborudovanie/kompressori/" data-id="418" data-products-count="222">Компрессоры</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/pnevmaticheskoe_oborudovanie/gaykoverti/" data-id="423" data-products-count="18">Гайковерты</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/pnevmaticheskoe_oborudovanie/kraskopult/" data-id="424" data-products-count="15">Краскопульт</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/pnevmaticheskoe_oborudovanie/pistoleti/" data-id="425" data-products-count="75">Пистолеты</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/pnevmaticheskoe_oborudovanie/shlifmashinki/" data-id="427" data-products-count="29">Шлифмашинки</a></li>
                                                                <li><a href="/remont_i_stroitelstvo/pnevmaticheskoe_oborudovanie/">Все пневматическое оборудование&nbsp;<span>369</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/remont_i_stroitelstvo/stroitelnoe_oborudovanie/" data-id="447">Строительное оборудование&nbsp;<span>99</span></a>
                                                        <div data-id="447">
                                                            <ul>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/stroitelnoe_oborudovanie/betonomeshalki/" data-id="448" data-products-count="30">Бетономешалки</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/stroitelnoe_oborudovanie/vibropliti_i_vibrotrambovki/" data-id="449" data-products-count="29">Виброплиты и вибротрамбовки</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/stroitelnoe_oborudovanie/motopompi/" data-id="450" data-products-count="40">Мотопомпы</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/remont_i_stroitelstvo/stanki/" data-id="451">Станки&nbsp;<span>165</span></a>
                                                        <div data-id="451">
                                                            <ul>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/stanki/kamnereznie_stanki/" data-id="452" data-products-count="13">Камнерезные станки</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/stanki/stanki_raspilnie/" data-id="453" data-products-count="17">Станки распильные</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/stanki/stanki_tochilnie/" data-id="455" data-products-count="83">Станки точильные</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/stanki/stanki_sverilnie/" data-id="477" data-products-count="24">Станки сверильные</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/stanki/stanki_tokarnie/" data-id="478" data-products-count="16">Станки токарные</a></li>
                                                                <li><a href="/remont_i_stroitelstvo/stanki/">Все станки&nbsp;<span>165</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/" data-id="11686">Расходные материалы и аксессуары&nbsp;<span>6939</span></a>
                                                        <div data-id="11686">
                                                            <ul>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/vedra_stroitelnie/" data-id="659" data-products-count="13">Ведра строительные</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/akkumulyatori_dlya_elektroinstrumenta/" data-id="416" data-products-count="221">Аккумуляторы для электроинструмента</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/cepi/" data-id="678" data-products-count="74">Цепи</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/skobi_dlya_stroitelnogo_steplera/" data-id="679" data-products-count="81">Скобы для строительного степлера</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/buri/" data-id="11687" data-products-count="462">Буры</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/metizi_i_krepejnie_izdeliya/" data-id="687" data-products-count="548">Метизы и крепежные изделия</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/promishlennaya_himiya/" data-id="433" data-products-count="50">Промышленная химия</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/cepi/" data-id="456" data-products-count="74">Цепи</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/izolenta/" data-id="459" data-products-count="99">Изолента</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/skobi_dlya_stroitelnogo_steplera/" data-id="471" data-products-count="81">Скобы для строительного степлера</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/aksessuary_dlya_elektroinstrumenta/" data-id="11688" data-products-count="504">Аксессуары для электроинструмента</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/zubila/" data-id="11689" data-products-count="129">Зубила</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/diski_frezy_krugi/" data-id="11690" data-products-count="2275">Диски.Фрезы.Круги</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/nabory_bit_i_sverel/" data-id="11691" data-products-count="529">Наборы бит и сверл</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/osnastka_k_sadovoy_tekhnike/" data-id="11693" data-products-count="65">Оснастка к садовой технике</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/pilki_i_polotna/" data-id="11694" data-products-count="361">Пилки и полотна</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/sverla/" data-id="11695" data-products-count="839">Сверла</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/shlifovalnyye_lenty_i_listy/" data-id="11696" data-products-count="253">Шлифовальные ленты и листы</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/sredstva_individualnoy_zashiti/" data-id="460" data-products-count="278">Средства индивидуальной защиты</a></li>
                                                                <li><a href="/remont_i_stroitelstvo/raschodnie_materiali_i_aksessuari/">Все расходные материалы и аксессуары&nbsp;<span>6939</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/remont_i_stroitelstvo/ruchnoy_instrument/" data-id="12049">Ручной инструмент&nbsp;<span>3120</span></a>
                                                        <div data-id="12049">
                                                            <ul>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/ruchnoy_instrument/nabori_torcevih_kluchey/" data-id="392" data-products-count="219">Наборы торцевых ключей</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/ruchnoy_instrument/pinceti/" data-id="394" data-products-count="31">Пинцеты</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/ruchnoy_instrument/gubcevii-instrument/" data-id="12687" data-products-count="632">Губцевый инструмент</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/ruchnoy_instrument/otvertki/" data-id="415" data-products-count="443">Отвертки</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/ruchnoy_instrument/pistolet_dlya_montajnoy_peni/" data-id="417" data-products-count="19">Пистолет для монтажной пены</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/ruchnoy_instrument/noji_i_lezviya/" data-id="463" data-products-count="173">Ножи и лезвия</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/ruchnoy_instrument/pili_ruchnie/" data-id="464" data-products-count="86">Пилы ручные</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/ruchnoy_instrument/ydarniy_i_richajniy_instrument/" data-id="480" data-products-count="294">Ударный и рычажный инструмент</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/ruchnoy_instrument/cluchi/" data-id="12686" data-products-count="622">Ключи</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/ruchnoy_instrument/pistoleti_dlya_germetika/" data-id="491" data-products-count="18">Пистолеты для герметика</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/ruchnoy_instrument/lestnici-stremyanki-verstaki/" data-id="12688" data-products-count="83">Лестницы, стремянки, верстаки</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/ruchnoy_instrument/sumki-i-yashiki-dlya-instrumenta/" data-id="12690" data-products-count="190">Сумки и ящики для инструментов</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/ruchnoy_instrument/shtukaturno-molyarnii-instrument/" data-id="12691" data-products-count="232">Штукатурно-малярный инструмент</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/ruchnoy_instrument/lopaty/" data-id="15259" data-products-count="72">Лопаты</a></li>
                                                                <li><a href="/remont_i_stroitelstvo/ruchnoy_instrument/">Все ручной инструмент&nbsp;<span>3120</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/remont_i_stroitelstvo/injenernaya-santehnika/" data-id="14833">Инженерная сантехника&nbsp;<span>2816</span></a>
                                                        <div data-id="14833">
                                                            <ul>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/kotly-otopleniya/" data-id="15693" data-products-count="117">Котлы отопления</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/gorelki/" data-id="640" data-products-count="52">Горелки</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/spesitely/" data-id="15753" data-products-count="117">Смесители</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/polotencesushiteli/" data-id="430" data-products-count="22">Полотенцесушители</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/kuhonnye-moiky/" data-id="15594" data-products-count="65">Мойки</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/teplie-poli/" data-id="13549" data-products-count="255">Теплые полы</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/baki_membrannye_i_emkosti/" data-id="14835" data-products-count="43">Баки мембранные и емкости</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/vodozapornaya_armatura/" data-id="14836" data-products-count="286">Водозапорная арматура</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/kotelnoe_i_nasosnoe_oborudovanie/" data-id="14855" data-products-count="428">Котельное и насосное оборудование</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/krepezh_i_instrument/" data-id="14861" data-products-count="16">Крепеж и инструмент</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/podvodka_gibkaya/" data-id="14867" data-products-count="43">Подводка гибкая</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/radiatory_otopleniya/" data-id="14870" data-products-count="447">Радиаторы отопления</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/rezbovye_fitingi_i_kollektory/" data-id="14889" data-products-count="308">Резьбовые фитинги и коллекторы</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/shetchiky-kip/" data-id="14954" data-products-count="52">Счетчики, КИП, предохранительно-регулир. арматура</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/teplyj_pol/" data-id="14971" data-products-count="86">Теплый пол</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/truby_metallopolimernye_i_fitingi/" data-id="14977" data-products-count="173">Трубы металлополимерные и фитинги</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/truby_polipropilenovye_i_fitingi/" data-id="14983" data-products-count="211">Трубы полипропиленовые и фитинги</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/injenernaya-santehnika/truby_stalnye_i_fitingi/" data-id="14986" data-products-count="85">Трубы стальные и фитинги</a></li>
                                                                <li><a href="/remont_i_stroitelstvo/injenernaya-santehnika/">Все инженерная сантехника&nbsp;<span>2816</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/remont_i_stroitelstvo/silovaya_technica/" data-id="11700">Силовая техника&nbsp;<span>635</span></a>
                                                        <div data-id="11700">
                                                            <ul>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/silovaya_technica/elektrodi/" data-id="431" data-products-count="216">Электроды</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/silovaya_technica/generatori/" data-id="11701" data-products-count="142">Генераторы</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/silovaya_technica/svarochnie_apparati/" data-id="11702" data-products-count="175">Сварочные аппараты</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/silovaya_technica/svarochnie_maski/" data-id="14753" data-products-count="102">Сварочные маски</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/remont_i_stroitelstvo/avtogarajnoe_oborudovanie/" data-id="434">Автогаражное оборудование&nbsp;<span>250</span></a>
                                                        <div data-id="434">
                                                            <ul>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/avtogarajnoe_oborudovanie/balonnie_kluchi/" data-id="436" data-products-count="22">Балонные ключи</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/avtogarajnoe_oborudovanie/diagnosticheskoe_oborudovanie/" data-id="438" data-products-count="15">Диагностическое оборудование</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/avtogarajnoe_oborudovanie/domkrati/" data-id="439" data-products-count="99">Домкраты</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/avtogarajnoe_oborudovanie/instrument_dlya_montajno_demontajnih_rabot_semniki/" data-id="440" data-products-count="28">Инструмент для монтажно-демонтажных работ (съемники)</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/avtogarajnoe_oborudovanie/oborudovanie_dlya_gsm/" data-id="442" data-products-count="42">Оборудование для ГСМ</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/avtogarajnoe_oborudovanie/shlangi_fitingi/" data-id="446" data-products-count="11">Шланги, фитинги</a></li>
                                                                <li><a href="/remont_i_stroitelstvo/avtogarajnoe_oborudovanie/">Все автогаражное оборудование&nbsp;<span>250</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/" data-id="500">Запчасти для инструмента&nbsp;<span>471</span></a>
                                                        <div data-id="500">
                                                            <ul>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/nakonechniki/" data-id="537" data-products-count="34">Наконечники</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/patroni_cangovie_i_cangi/" data-id="562" data-products-count="18">Патроны цанговые и цанги</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/remni/" data-id="581" data-products-count="37">Ремни</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/svechi_zajiganiya/" data-id="588" data-products-count="19">Свечи зажигания</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/skobi/" data-id="589" data-products-count="14">Скобы</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/sopla/" data-id="591" data-products-count="116">Сопла</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/trubki_i_shlangi/" data-id="599" data-products-count="49">Трубки и шланги</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/filtri_prochie/" data-id="600" data-products-count="128">Фильтры прочие</a></li>
                                                                <li class="leaf"><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/zaryadnie_ustroystva/" data-id="504" data-products-count="35">Зарядные устройства</a></li>
                                                                <li><a href="/remont_i_stroitelstvo/zapchasti_dlya_instrumenta/">Все запчасти для инструмента&nbsp;<span>471</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/remont_i_stroitelstvo/nabory_instrumenta/" data-id="242">Наборы инструментов&nbsp;<span>400</span></a></li>
                                                </ul>
                                            </div>
                                            <div data-id="10" data-items-per-column="32">
                                                <ul>
                                                    <li><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/" data-id="75">Климатическая техника&nbsp;<span>1697</span></a>
                                                        <div data-id="75">
                                                            <ul>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/maslyannye-radistory/" data-id="240" data-products-count="78">Масляные радиаторы</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/teploventilyatory-teplovie-pushki/" data-id="241" data-products-count="189">Тепловентиляторы.Тепловые пушки</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/vityajnie-ventilyatory/" data-id="13528" data-products-count="74">Вытяжные вентиляторы</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/ventilyatory/" data-id="231" data-products-count="13">Вентиляторы</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/electrokamini/" data-id="11141" data-products-count="12">Электрокамины</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/konvectory/" data-id="238" data-products-count="102">Конвекторы</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/ifrakrasnye-obogrevateli/" data-id="229" data-products-count="63">Инфракрасные обогреватели</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/teplovye-zavesy/" data-id="237" data-products-count="36">Тепловые завесы</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/split-sistemy/" data-id="228" data-products-count="96">Сплит-системы</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/uvlajniteli/" data-id="230" data-products-count="84">Увлажнители воздуха</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/vodonagrevateli/" data-id="235" data-products-count="554">Водонагреватели</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/ochestiteli-vozduha/" data-id="236" data-products-count="16">Очистители воздуха</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/pogodnie_stancii/" data-id="12441" data-products-count="59">Погодные станции</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/teplie-poli/" data-id="13548" data-products-count="255">Теплые полы</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/termoregulyatory/" data-id="14815" data-products-count="40">Терморегуляторы</a></li>
                                                                <li><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/klimaticheskaya_tehnika/">Все климатическая техника&nbsp;<span>1697</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/krupnaya-bitovaya-tehnika/" data-id="12181">Крупная бытовая техника&nbsp;<span>1856</span></a>
                                                        <div data-id="12181">
                                                            <ul>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/krupnaya-bitovaya-tehnika/aksessuari_dlya_bitivoi_tehniki/" data-id="14794" data-products-count="43">Аксессуары для бытовой техники</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/krupnaya-bitovaya-tehnika/stiralnie-mashini/" data-id="12185" data-products-count="238">Стиральные машины</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/krupnaya-bitovaya-tehnika/sushilnie-mashini/" data-id="12189" data-products-count="13">Сушильные машины</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/krupnaya-bitovaya-tehnika/posudomoechnie-mashini/" data-id="12184" data-products-count="62">Посудомоечные машины</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/krupnaya-bitovaya-tehnika/gazovie-plati/" data-id="12183" data-products-count="308">Газовые плиты</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/krupnaya-bitovaya-tehnika/electricheskie-pliti/" data-id="12191" data-products-count="133">Электрические плиты</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/krupnaya-bitovaya-tehnika/vityajki/" data-id="12182" data-products-count="412">Вытяжки</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/krupnaya-bitovaya-tehnika/bitovaya-tehnika-holodilniki/" data-id="12190" data-products-count="493">Холодильники</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/krupnaya-bitovaya-tehnika/morozilnie-kameri/" data-id="12192" data-products-count="129">Морозильные камеры</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/krupnaya-bitovaya-tehnika/vinnie_shkafi/" data-id="13284" data-products-count="25">Винные шкафы</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/bytovaya_texnika_vstraivaemaya/" data-id="12201">Бытовая техника встраиваемая&nbsp;<span>941</span></a>
                                                        <div data-id="12201">
                                                            <ul>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/bytovaya_texnika_vstraivaemaya/varochnye_paneli/" data-id="12202" data-products-count="394">Варочные панели</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/bytovaya_texnika_vstraivaemaya/duxovki/" data-id="12213" data-products-count="247">Духовые шкафы</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/bytovaya_texnika_vstraivaemaya/vytyazhki/" data-id="12210" data-products-count="181">Вытяжки</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/bytovaya_texnika_vstraivaemaya/vstraivaemye_stiralnye_mashiny/" data-id="12208" data-products-count="11">Встраиваемые стиральные машины</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/bytovaya_texnika_vstraivaemaya/vstraivaemye_posudomoechnye_mashiny/" data-id="12207" data-products-count="43">Встраиваемые посудомоечные машины</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/bytovaya_texnika_vstraivaemaya/vstraivaemye_mikrovolnovye_pechi/" data-id="12206" data-products-count="27">Встраиваемые микроволновые печи</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/bytovaya_texnika_vstraivaemaya/vstraeviemie-holodilniki/" data-id="12949" data-products-count="37">Встраиваемые холодильники</a></li>
                                                                <li><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/bytovaya_texnika_vstraivaemaya/">Все бытовая техника встраиваемая&nbsp;<span>941</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/" data-id="69">Техника для кухни&nbsp;<span>3866</span></a>
                                                        <div data-id="69">
                                                            <ul>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/aerogrill/" data-id="195" data-products-count="86">Аэрогрили и сушки</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/chayniki/" data-id="194" data-products-count="694">Чайники</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/termopoty/" data-id="187" data-products-count="39">Термопоты</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/vakuumnii_upakovschiki/" data-id="14735" data-products-count="43">Вакуумные упаковщики</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/aksessuari_dlya_vakuumnih_upakovschikov/" data-id="14736" data-products-count="103">Аксессуары для вакуумных упаковщиков</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/prigotovlenie_kofe/" data-id="210" data-products-count="233">Кофеварки</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/aksi-kofevarki/" data-id="12301" data-products-count="36">Аксессуары для кофеварок</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/kofemolki/" data-id="461" data-products-count="81">Кофемолки</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/kappuchinatory/" data-id="283" data-products-count="17">Капучинаторы</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/multivarki/" data-id="191" data-products-count="99">Мультиварки</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/aksessuary_dlya_multivarok/" data-id="301" data-products-count="21">Аксессуары для мультиварок</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/mikrovolnovye_pechi/" data-id="200" data-products-count="254">Микроволновые печи</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/miksery/" data-id="203" data-products-count="100">Миксеры</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/kuhooye-kombainy/" data-id="186" data-products-count="62">Кухонные комбайны</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/aksessuary-dlya-kuhonnih-kombainov/" data-id="14673" data-products-count="29">Аксессуары к кухонным комбайнам</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/myasorubki/" data-id="201" data-products-count="160">Мясорубки</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/askessuary-dlya-myasorubok/" data-id="14482" data-products-count="24">Аксессуары для мясорубок</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/blendery/" data-id="189" data-products-count="351">Блендеры</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/parovarki/" data-id="192" data-products-count="14">Пароварки</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/hlebopechi/" data-id="193" data-products-count="42">Хлебопечки</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/kuhonnye_vesy/" data-id="205" data-products-count="201">Кухонные весы</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/tostery/" data-id="196" data-products-count="133">Тостеры</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/sokovyzhimalki/" data-id="199" data-products-count="104">Соковыжималки</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/skorovarki/sokovarki/" data-id="13448" data-products-count="14">Скороварки/Соковарки</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/filtry_dlya_vody/" data-id="183" data-products-count="98">Фильтры для воды</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/kartridjy-dlya-vody/" data-id="14693" data-products-count="161">Картриджи для фильтров</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/pribory_dlya_hot_dogov/" data-id="206" data-products-count="322">Приборы для приготовления хот-догов</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/yogurt/" data-id="208" data-products-count="33">Йогуртницы\\Мороженицы</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/elektrogrili/" data-id="185" data-products-count="105">Электрогрили\\Шашлычницы</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/varochnye_plitki/" data-id="209" data-products-count="145">Варочные плитки</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/kronsteini-dlya-svch/" data-id="14394" data-products-count="19">Кронштейны для микроволновых печей</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/yacavarky/" data-id="14793" data-products-count="21">Яйцеварки</a></li>
                                                                <li><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/kitchen-appliances/">Все техника для кухни&nbsp;<span>3866</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/" data-id="73">Техника для дома&nbsp;<span>2055</span></a>
                                                        <div data-id="73">
                                                            <ul>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/pylesosy/" data-id="224" data-products-count="319">Пылесосы</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/roboti-pilesosi/" data-id="12786" data-products-count="105">Роботы-пылесосы и электровеники</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/aksessuary_dlya_pylesosov/" data-id="227" data-products-count="177">Аксессуары для пылесосов</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/utyugi/" data-id="13431" data-products-count="227">Утюги</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/otparivateli/" data-id="223" data-products-count="133">Отпариватели</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/chasy_budilniki/" data-id="226" data-products-count="58">Часы-будильники</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/uchod_za_odezdoi_i_obuv/" data-id="11436" data-products-count="32">Уход за одеждой и обувью</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/sushilki_dlya_belya/" data-id="11498" data-products-count="20">Сушилки для белья</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/sumki-holodilniki/" data-id="627" data-products-count="71">Сумки-холодильники</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/sheinie-mashini/" data-id="12002" data-products-count="249">Швейные машины</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/hoz-inventar/" data-id="12081" data-products-count="275">Хозяйственный инвентарь</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/chasi/" data-id="12486" data-products-count="283">Часы</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/gladilnie-doski/" data-id="13509" data-products-count="34">Гладильные доски</a></li>
                                                                <li class="leaf"><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/distillyatory/" data-id="14989" data-products-count="69">Дистилляторы</a></li>
                                                                <li><a href="/klimaticheskaya_krupnaya_i_melkaya_bitovaya_tehnika/tehnika_dlya_doma/">Все техника для дома&nbsp;<span>2055</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div data-id="61" data-items-per-column="16">
                                                <ul>
                                                    <li><a href="/car-electronics/avtomobilnie-shini/" data-id="12221">Шины&nbsp;<span>4456</span></a>
                                                        <div data-id="12221">
                                                            <ul>
                                                                <li class="leaf"><a href="/car-electronics/avtomobilnie-shini/letnie/" data-id="298" data-products-count="644">Летние</a></li>
                                                                <li class="leaf"><a href="/car-electronics/avtomobilnie-shini/zimnie/" data-id="299" data-products-count="1447">Зимние</a></li>
                                                                <li class="leaf"><a href="/car-electronics/avtomobilnie-shini/vsesezonnie_shini/" data-id="338" data-products-count="82">Всесезонные шины</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/car-electronics/avtomobilnie-diski/" data-id="12222">Диски&nbsp;<span>321</span></a>
                                                        <div data-id="12222">
                                                            <ul>
                                                                <li class="leaf"><a href="/car-electronics/avtomobilnie-diski/litie_diski/" data-id="357" data-products-count="14">Литые диски</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/car-electronics/avtohimiya_i_avtokosmetika/" data-id="78">Автохимия и автокосметика&nbsp;<span>731</span></a>
                                                        <div data-id="78">
                                                            <ul>
                                                                <li class="leaf"><a href="/car-electronics/avtohimiya_i_avtokosmetika/masla_i_smazki/" data-id="79" data-products-count="289">Масла и смазки</a></li>
                                                                <li class="leaf"><a href="/car-electronics/avtohimiya_i_avtokosmetika/avtokosmetika/" data-id="97" data-products-count="57">Автокосметика</a></li>
                                                                <li class="leaf"><a href="/car-electronics/avtohimiya_i_avtokosmetika/avtohimiya_potrebitelskaya/" data-id="99" data-products-count="145">Автохимия потребительская</a></li>
                                                                <li class="leaf"><a href="/car-electronics/avtohimiya_i_avtokosmetika/avtohimiya_professionalnaya/" data-id="120" data-products-count="77">Автохимия профессиональная</a></li>
                                                                <li class="leaf"><a href="/car-electronics/avtohimiya_i_avtokosmetika/lodochnaya_programma/" data-id="140" data-products-count="29">Лодочная программа</a></li>
                                                                <li class="leaf"><a href="/car-electronics/avtohimiya_i_avtokosmetika/motocikletnaya_programma/" data-id="162" data-products-count="88">Мотоциклетная программа</a></li>
                                                                <li class="leaf"><a href="/car-electronics/avtohimiya_i_avtokosmetika/specialnie_jidkosti/" data-id="204" data-products-count="20">Специальные жидкости</a></li>
                                                                <li><a href="/car-electronics/avtohimiya_i_avtokosmetika/">Все автохимия и автокосметика&nbsp;<span>731</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/car-electronics/avtomagnitoli/" data-id="11791">Автомагнитолы&nbsp;<span>147</span></a></li>
                                                    <li><a href="/car-electronics/navigatory/" data-id="72">Автонавигаторы&nbsp;<span>18</span></a></li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/car-electronics/videoregistratory/" data-id="80">Видеорегистраторы&nbsp;<span>104</span></a></li>
                                                    <li><a href="/car-electronics/radar-detectors/" data-id="110">Радары-детекторы&nbsp;<span>21</span></a></li>
                                                    <li><a href="/car-electronics/parkovochnye_radary/" data-id="3664">Парктроники&nbsp;<span>16</span></a></li>
                                                    <li><a href="/car-electronics/avtokompressory/" data-id="109">Компрессоры&nbsp;<span>92</span></a></li>
                                                    <li><a href="/car-electronics/invertory/" data-id="113">Автомобильные инверторы&nbsp;<span>95</span></a></li>
                                                    <li><a href="/car-electronics/avtoakustika/" data-id="112">Автоакустика&nbsp;<span>185</span></a>
                                                        <div data-id="112">
                                                            <ul>
                                                                <li class="leaf"><a href="/car-electronics/avtoakustika/usiliteli_zvuka/" data-id="3672" data-products-count="18">Автомобильные усилители звука</a></li>
                                                                <li class="leaf"><a href="/car-electronics/avtoakustika/akkustucheskie-kabeli/" data-id="12587" data-products-count="15">Акустические кабели</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/car-electronics/minimoyki/" data-id="3662">Минимойки&nbsp;<span>74</span></a></li>
                                                    <li><a href="/car-electronics/aksessuari_dlya_minimoek/" data-id="643">Аксессуары для минимоек&nbsp;<span>63</span></a></li>
                                                    <li><a href="/car-electronics/aksessuari_i_oborudovanie/" data-id="71">Аксессуары и оборудование&nbsp;<span>679</span></a>
                                                        <div data-id="71">
                                                            <ul>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/avtomobilnoe_osveshenie/" data-id="390" data-products-count="35">Автомобильное освещение</a></li>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/avtomobilnie_predohraniteli/" data-id="391" data-products-count="20">Автомобильные предохранители</a></li>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/stekloochistiteli/" data-id="432" data-products-count="74">Стеклоочистители</a></li>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/fm_transmittery/" data-id="70" data-products-count="21">FM трансмиттеры</a></li>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/auto_zaryadnye_ustroyatva/" data-id="74" data-products-count="164">Автомобильные зарядные устройства</a></li>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/skrebki-i-shetki/" data-id="13708" data-products-count="12">Скребки и щётки</a></li>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/kamery_zadnego_vida/" data-id="3663" data-products-count="21">Камеры заднего вида</a></li>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/avtomobilnye_monitory_televizory/" data-id="3666" data-products-count="16">Автомобильные мониторы, телевизоры</a></li>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/avtomobilnye_pylesosy/" data-id="3667" data-products-count="24">Пылесосы для авто</a></li>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/avtomobilnye_signalizatsii/" data-id="3668" data-products-count="12">Автомобильные сигнализации</a></li>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/avtomobilnye_kholodilniki/" data-id="3669" data-products-count="71">Автомобильные холодильники</a></li>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/avtoazvetviteli/" data-id="3671" data-products-count="26">Разветвители</a></li>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/manometri/" data-id="11419" data-products-count="23">Автомобильные манометры</a></li>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/puskozaryadnie_ustroistva/" data-id="11527" data-products-count="63">Пускозарядные устройства</a></li>
                                                                <li class="leaf"><a href="/car-electronics/aksessuari_i_oborudovanie/other/" data-id="12527" data-products-count="83">Прочее</a></li>
                                                                <li><a href="/car-electronics/aksessuari_i_oborudovanie/">Все аксессуары и оборудование&nbsp;<span>679</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div data-id="12969" data-items-per-column="68">
                                                <ul>
                                                    <li><a href="/kids/detskie-kolyaski/" data-id="13160">Детские коляски&nbsp;<span>3321</span></a>
                                                        <div data-id="13160">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/detskie-kolyaski/vse-kolyaski/" data-id="14031" data-products-count="1283">Все коляски</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-kolyaski/dlya-novorozhdennyh/" data-id="13161" data-products-count="54">Коляски для новорожденных</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-kolyaski/kolyaski-2-v-1/" data-id="13164" data-products-count="451">Коляски 2 в 1</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-kolyaski/kolyaski-3-v-1/" data-id="13165" data-products-count="163">Коляски 3 в 1</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-kolyaski/kolyaski-trosli/" data-id="13999" data-products-count="173">Коляски-трости</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-kolyaski/progulochnye/" data-id="13162" data-products-count="407">Прогулочные коляски</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-kolyaski/kolyaski-dlya-dvoih-detei/" data-id="13167" data-products-count="35">Коляски для двоих детей</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-kolyaski/aksessuari-dlya-kolyasok/" data-id="13954" data-products-count="679">Аксессуары для колясок</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-kolyaski/complektuyshie-dlya-kolyasok/" data-id="13948" data-products-count="76">Комплектующие колясок</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/detskij-transport/" data-id="13193">Детский транспорт&nbsp;<span>954</span></a>
                                                        <div data-id="13193">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/detskij-transport/samokaty/" data-id="13200" data-products-count="519">Детские самокаты</a></li>
                                                                <li class="leaf"><a href="/kids/detskij-transport/velobalansiry/" data-id="13198" data-products-count="40">Беговелы (велобалансиры) для детей</a></li>
                                                                <li class="leaf"><a href="/kids/detskij-transport/velosipedy-dvukhkolesnye/" data-id="13196" data-products-count="64">Двухколесные велосипеды для детей</a></li>
                                                                <li class="leaf"><a href="/kids/detskij-transport/trehkolesnye-velosipedy/" data-id="13197" data-products-count="107">Детские трехколесные велосипеды</a></li>
                                                                <li class="leaf"><a href="/kids/detskij-transport/pedalnaya-mashina/" data-id="13195" data-products-count="22">Детская педальная машина</a></li>
                                                                <li class="leaf"><a href="/kids/detskij-transport/roliki/" data-id="13205" data-products-count="31">Детские ролики</a></li>
                                                                <li class="leaf"><a href="/kids/detskij-transport/electromobili/" data-id="13194" data-products-count="30">Электромобили и мопеды для детей</a></li>
                                                                <li class="leaf"><a href="/kids/detskij-transport/aksessuari_dlya_elektromobiley/" data-id="645" data-products-count="11">Аксессуары для электромобилей</a></li>
                                                                <li class="leaf"><a href="/kids/detskij-transport/aksessuary-dlya-velosipedy/" data-id="13199" data-products-count="52">Аксессуары для детских велосипедов</a></li>
                                                                <li class="leaf"><a href="/kids/detskij-transport/detskie-shlemy/" data-id="13219" data-products-count="26">Детские шлемы</a></li>
                                                                <li class="leaf"><a href="/kids/detskij-transport/skejtbordy/" data-id="14992" data-products-count="52">Скейтборды</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/avtokresla/" data-id="13169">Детские автокресла&nbsp;<span>997</span></a>
                                                        <div data-id="13169">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/avtokresla/vse-avokresla/" data-id="14032" data-products-count="444">Все автокресла</a></li>
                                                                <li class="leaf"><a href="/kids/avtokresla/gruppa-0-plus-s-rojeniya/" data-id="13973" data-products-count="77">Группа 0+ (0-13кг/от рождения до 1 года)</a></li>
                                                                <li class="leaf"><a href="/kids/avtokresla/gruppa-0-1/" data-id="13974" data-products-count="69">Группа 0+/1 (0-18кг/от рождения до 3.5-4 лет)</a></li>
                                                                <li class="leaf"><a href="/kids/avtokresla/gruppa-0-1-2/" data-id="13975" data-products-count="14">Группа 0+/1/2 (0-25 кг/от рождения до 7 лет)</a></li>
                                                                <li class="leaf"><a href="/kids/avtokresla/gruppa-1/" data-id="13976" data-products-count="63">Группа 1 (9-18 кг/от 1 до 3.5-4 лет)</a></li>
                                                                <li class="leaf"><a href="/kids/avtokresla/gruppa-1-2/" data-id="13977" data-products-count="16">Группа 1/2 (от 9-25кг/от 1 до7 лет)</a></li>
                                                                <li class="leaf"><a href="/kids/avtokresla/gruppa-1-2-3/" data-id="13978" data-products-count="96">Группа 1/2/3 (от 9-36кг/от 1 до 12 лет)</a></li>
                                                                <li class="leaf"><a href="/kids/avtokresla/gruppa-2-3/" data-id="13979" data-products-count="104">Группа 2/3 (от 15-36кг/от 3 до 12 лет)</a></li>
                                                                <li class="leaf"><a href="/kids/avtokresla/aksessuary-dlya-avtokresel/" data-id="13174" data-products-count="109">Аксессуары для автокресел</a></li>
                                                                <li><a href="/kids/avtokresla/">Все детские автокресла&nbsp;<span>997</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/yhod_za_detmi/" data-id="215">Уход за детьми&nbsp;<span>1079</span></a>
                                                        <div data-id="215">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/yhod_za_detmi/kosmetika-i-bitovaya-himiya/" data-id="12970" data-products-count="604">Косметика и бытовая химия</a></li>
                                                                <li class="leaf"><a href="/kids/yhod_za_detmi/podguzniki/" data-id="12971" data-products-count="227">Подгузники, трусики, пеленки</a></li>
                                                                <li class="leaf"><a href="/kids/yhod_za_detmi/kupanie-rebenka/" data-id="13011" data-products-count="248">Купание ребенка</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/lego-konstruktory/" data-id="14028">Товары Lego&nbsp;<span>697</span></a>
                                                        <div data-id="14028">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/lego-konstruktory/konstruktor-lego/" data-id="14233" data-products-count="343">Конструкторы LEGO</a></li>
                                                                <li><a href="/kids/lego-konstruktory/">Все товары lego&nbsp;<span>697</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/detskie-igrushki/" data-id="13045">Детские игрушки&nbsp;<span>18914</span></a>
                                                        <div data-id="13045">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/konstruktory-dlya-detej/" data-id="13057" data-products-count="3756">Конструкторы для детей</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/interaktivnye-igrushki/" data-id="13051" data-products-count="851">Интерактивные игрушки</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/myagkie-detskie-igrushki/" data-id="13049" data-products-count="877">Мягкие игрушки</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/nabory-dlya-devochek/" data-id="13089" data-products-count="656">Игровые наборы для девочек</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/kosmetika-ukrasheniya/" data-id="14069" data-products-count="160">Косметика и украшения для девочек</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/kukly/" data-id="13052" data-products-count="2141">Куклы</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/nabory-dlya-malchikov/" data-id="13090" data-products-count="930">Игровые наборы для мальчиков</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/igrushechnyi-transport/" data-id="14116" data-products-count="2954">Транспортная игрушка</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/radioupravlyaemye-igrushki/" data-id="13054" data-products-count="463">Радиоуправляемые игрушки</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/roboty/" data-id="13055" data-products-count="231">Игрушки "Роботы"</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/igrushechnoe-oruzhie/" data-id="13056" data-products-count="403">Игрушечное оружие</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/detskaya_bytovaya_texnika/" data-id="13432" data-products-count="96">Детская бытовая техника</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/music/" data-id="13083" data-products-count="321">Музыкальные инструменты</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/igrushki-dlya-malyshey/" data-id="13048" data-products-count="2991">Игрушки для малышей</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/razvivayushie_igrushki/" data-id="14270" data-products-count="819">Развивающие игрушки для малышей</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/rolgames/" data-id="13075" data-products-count="751">Детские сюжетно - ролевые игры</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/figurki-detskie/" data-id="13093" data-products-count="438">Детские фигурки</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/igrushki-dlja-ulicy/" data-id="13088" data-products-count="41">Игрушки для улицы</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-igrushki/hranenie-igrushek/" data-id="13688" data-products-count="30">Хранение игрушек</a></li>
                                                                <li><a href="/kids/detskie-igrushki/">Все детские игрушки&nbsp;<span>18914</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/derevjannye-igrushki/" data-id="13091">Деревянные игрушки&nbsp;<span>466</span></a>
                                                        <div data-id="13091">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/derevjannye-igrushki/derevyannie-kubiki-pazli-konstruktory/" data-id="14454" data-products-count="216">Деревянные кубики, пазлы, конструкторы для детей</a></li>
                                                                <li class="leaf"><a href="/kids/derevjannye-igrushki/razvivayshie-igrushki-iz-dereva/" data-id="14460" data-products-count="165">Развивающие игрушки из дерева</a></li>
                                                                <li class="leaf"><a href="/kids/derevjannye-igrushki/derevyannie-katalki-dlya-maloshei/" data-id="14444" data-products-count="11">Деревянные каталки для малышей</a></li>
                                                                <li class="leaf"><a href="/kids/derevjannye-igrushki/nastolnie-igrri-iz-dereva/" data-id="14447" data-products-count="13">Настольные игры из дерева для детей</a></li>
                                                                <li class="leaf"><a href="/kids/derevjannye-igrushki/shnurovki-dlya-malishei/" data-id="14449" data-products-count="24">Шнуровки для малышей</a></li>
                                                                <li class="leaf"><a href="/kids/derevjannye-igrushki/derevyannie-piramidki-dlya-detei/" data-id="14452" data-products-count="20">Деревянные пирамидки для детей</a></li>
                                                                <li class="leaf"><a href="/kids/derevjannye-igrushki/prochie-derevyannie-igrushki/" data-id="14451" data-products-count="12">Прочие</a></li>
                                                                <li><a href="/kids/derevjannye-igrushki/">Все деревянные игрушки&nbsp;<span>466</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/kormlenie/" data-id="13110">Питание и кормление&nbsp;<span>1241</span></a>
                                                        <div data-id="13110">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/kormlenie/butylochki-dlya-kormleniya/" data-id="13156" data-products-count="213">Бутылочки для кормления</a></li>
                                                                <li class="leaf"><a href="/kids/kormlenie/poilniki/" data-id="14501" data-products-count="145">Поильники</a></li>
                                                                <li class="leaf"><a href="/kids/kormlenie/aksessuary-dlya-butylochek-i-poilnikov/" data-id="14502" data-products-count="24">Аксессуары для бутылочек и поильников</a></li>
                                                                <li class="leaf"><a href="/kids/kormlenie/soski/" data-id="13154" data-products-count="175">Соски</a></li>
                                                                <li class="leaf"><a href="/kids/kormlenie/pustyshki/" data-id="13153" data-products-count="317">Пустышки</a></li>
                                                                <li class="leaf"><a href="/kids/kormlenie/aksessuary-dlya-pustyshek-i-sosok/" data-id="14503" data-products-count="52">Аксессуары для пустышек и сосок</a></li>
                                                                <li class="leaf"><a href="/kids/kormlenie/dlya-mytya-butylochek/" data-id="13151" data-products-count="25">Все для мытья бутылочек</a></li>
                                                                <li class="leaf"><a href="/kids/kormlenie/posuda-dlya-kormleniya/" data-id="13152" data-products-count="206">Детская посуда для кормления</a></li>
                                                                <li class="leaf"><a href="/kids/kormlenie/slyunyavchiki-nagrudniki/" data-id="13157" data-products-count="54">Слюнявчики и нагрудники</a></li>
                                                                <li class="leaf"><a href="/kids/kormlenie/kontejnery-pakety-dlya-hraneniya/" data-id="14504" data-products-count="26">Контейнеры, пакеты для хранения</a></li>
                                                                <li><a href="/kids/kormlenie/">Все питание и кормление&nbsp;<span>1241</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/kids/dlya-beremennih/" data-id="12997">Товары для беременных и кормящих&nbsp;<span>67</span></a>
                                                        <div data-id="12997">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/dlya-beremennih/molokootsosy/" data-id="14490" data-products-count="17">Молокоотсосы</a></li>
                                                                <li class="leaf"><a href="/kids/dlya-beremennih/nakladki-na-sosok/" data-id="14491" data-products-count="11">Накладки на сосок</a></li>
                                                                <li class="leaf"><a href="/kids/dlya-beremennih/prokladki-dlya-grudi/" data-id="14492" data-products-count="16">Прокладки для груди</a></li>
                                                                <li><a href="/kids/dlya-beremennih/">Все товары для беременных и кормящих&nbsp;<span>67</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/stulchiki-dlya-kormleniya-i-aksessuary/" data-id="13111">Стульчики для кормления и аксессуары к ним&nbsp;<span>315</span></a>
                                                        <div data-id="13111">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/stulchiki-dlya-kormleniya-i-aksessuary/aksessuary/" data-id="13133" data-products-count="65">Аксессуары к стульчикам для кормления</a></li>
                                                                <li class="leaf"><a href="/kids/stulchiki-dlya-kormleniya-i-aksessuary/stulchiki-dlya-kormleniya/" data-id="14030" data-products-count="250">Стульчики для кормления</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/detskie-perenoski/" data-id="13175">Детские переноски&nbsp;<span>58</span></a>
                                                        <div data-id="13175">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/detskie-perenoski/sumki-i-korzini-dlya-perenoski/" data-id="13176" data-products-count="16">Сумки и корзины для переноски</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-perenoski/rukzak-kenguru/" data-id="13993" data-products-count="17">Рюкзаки-кенгуру</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-perenoski/konverty-na-vupisky/" data-id="14130" data-products-count="25">Конверты на выписку</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/hodunki-i-prigunki/" data-id="13897">Ходунки и прыгунки&nbsp;<span>52</span></a>
                                                        <div data-id="13897">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/hodunki-i-prigunki/hodunki/" data-id="13898" data-products-count="52">Ходунки</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/detskaya-komnata/" data-id="13017">Мебель для детской комнаты&nbsp;<span>2741</span></a>
                                                        <div data-id="13017">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/detskaya-komnata/kolibely/" data-id="13822" data-products-count="83">Колыбели</a></li>
                                                                <li class="leaf"><a href="/kids/detskaya-komnata/krovati/" data-id="13825" data-products-count="1820">Кровати</a></li>
                                                                <li class="leaf"><a href="/kids/detskaya-komnata/komodi/" data-id="13850" data-products-count="372">Комоды</a></li>
                                                                <li class="leaf"><a href="/kids/detskaya-komnata/shkafi/" data-id="13861" data-products-count="49">Шкафы</a></li>
                                                                <li class="leaf"><a href="/kids/detskaya-komnata/kresla-kachalki/" data-id="13901" data-products-count="20">Кресла-качалки</a></li>
                                                                <li class="leaf"><a href="/kids/detskaya-komnata/kacheli/" data-id="13902" data-products-count="24">Качели</a></li>
                                                                <li class="leaf"><a href="/kids/detskaya-komnata/nochniki_i_proektory/" data-id="14268" data-products-count="28">Ночники и проекторы</a></li>
                                                                <li class="leaf"><a href="/kids/detskaya-komnata/bezopasnost-malisha/" data-id="13903" data-products-count="148">Безопасность малыша</a></li>
                                                                <li class="leaf"><a href="/kids/detskaya-komnata/stoli-i-parti/" data-id="13872" data-products-count="149">Столы и парты</a></li>
                                                                <li class="leaf"><a href="/kids/detskaya-komnata/stellaji/" data-id="13865" data-products-count="11">Стеллажи</a></li>
                                                                <li class="leaf"><a href="/kids/detskaya-komnata/tumbi/" data-id="13860" data-products-count="14">Тумбы</a></li>
                                                                <li class="leaf"><a href="/kids/detskaya-komnata/polki/" data-id="13929" data-products-count="15">Полки</a></li>
                                                                <li><a href="/kids/detskaya-komnata/">Все мебель для детской комнаты&nbsp;<span>2741</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/dlya-ulicy/" data-id="13159">Спорт и отдых&nbsp;<span>1819</span></a>
                                                        <div data-id="13159">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/dlya-ulicy/leto/" data-id="13206" data-products-count="849">Для лета</a></li>
                                                                <li class="leaf"><a href="/kids/dlya-ulicy/zima/" data-id="13181" data-products-count="279">Для зимы</a></li>
                                                                <li class="leaf"><a href="/kids/dlya-ulicy/sport-games/" data-id="13180" data-products-count="260">Спортивные детские игры</a></li>
                                                                <li class="leaf"><a href="/kids/dlya-ulicy/domiki-palatki/" data-id="13218" data-products-count="201">Детские домики - палатки</a></li>
                                                                <li class="leaf"><a href="/kids/dlya-ulicy/velosipedy/" data-id="14226" data-products-count="225">Велосипеды и беговелы</a></li>
                                                                <li><a href="/kids/dlya-ulicy/">Все спорт и отдых&nbsp;<span>1819</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/school-art/" data-id="13220">Творчество и хобби&nbsp;<span>4318</span></a>
                                                        <div data-id="13220">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/school-art/3druchka/" data-id="15013" data-products-count="31">3D Ручки</a></li>
                                                                <li class="leaf"><a href="/kids/school-art/rukodelie/" data-id="13238" data-products-count="502">Рукоделие</a></li>
                                                                <li class="leaf"><a href="/kids/school-art/lepka/" data-id="13230" data-products-count="680">Все для лепки</a></li>
                                                                <li class="leaf"><a href="/kids/school-art/nabory/" data-id="13244" data-products-count="1287">Наборы для творчества</a></li>
                                                                <li class="leaf"><a href="/kids/school-art/nabory-khudozhestvennye/" data-id="13276" data-products-count="556">Художественные наборы</a></li>
                                                                <li class="leaf"><a href="/kids/school-art/pazly-dlja-detej/" data-id="13228" data-products-count="1032">Пазлы</a></li>
                                                                <li class="leaf"><a href="/kids/school-art/mozaika/" data-id="14154" data-products-count="36">Мозаика</a></li>
                                                                <li class="leaf"><a href="/kids/school-art/sbornie-modeli/" data-id="14051" data-products-count="186">Масштабные сборные модели</a></li>
                                                                <li><a href="/kids/school-art/">Все творчество и хобби&nbsp;<span>4318</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/dlya-prazdnika/" data-id="13629">Товары для праздника&nbsp;<span>4142</span></a>
                                                        <div data-id="13629">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/dlya-prazdnika/novyi-god/" data-id="13630" data-products-count="1894">Новый Год</a></li>
                                                                <li class="leaf"><a href="/kids/dlya-prazdnika/karnavalnye-kostumy/" data-id="13633" data-products-count="284">Карнавальные костюмы</a></li>
                                                                <li class="leaf"><a href="/kids/dlya-prazdnika/mylnye-puzyri/" data-id="13631" data-products-count="41">Мыльные пузыри</a></li>
                                                                <li class="leaf"><a href="/kids/dlya-prazdnika/atributy-prazdnika/" data-id="13632" data-products-count="360">Атрибуты для праздника</a></li>
                                                                <li class="leaf"><a href="/kids/dlya-prazdnika/servirovka-stola-svechi/" data-id="13634" data-products-count="292">Сервировка стола и свечи</a></li>
                                                                <li class="leaf"><a href="/kids/dlya-prazdnika/podarochnye-pakety/" data-id="13635" data-products-count="746">Подарочные пакеты</a></li>
                                                                <li class="leaf"><a href="/kids/dlya-prazdnika/novogonie-suveniry/" data-id="15374" data-products-count="518">Новогодние сувениры</a></li>
                                                                <li><a href="/kids/dlya-prazdnika/">Все товары для праздника&nbsp;<span>4142</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/razvitie-shkola/" data-id="13768">Развивающие пособия и игры&nbsp;<span>1332</span></a>
                                                        <div data-id="13768">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/razvitie-shkola/detskie-planshety/" data-id="13224" data-products-count="40">Детские компьютеры и планшеты</a></li>
                                                                <li class="leaf"><a href="/kids/razvitie-shkola/nabory-opytov/" data-id="14147" data-products-count="339">Исследования, опыты и эксперименты</a></li>
                                                                <li class="leaf"><a href="/kids/razvitie-shkola/metodika-umnitca/" data-id="14148" data-products-count="64">Методика развития Умница</a></li>
                                                                <li class="leaf"><a href="/kids/razvitie-shkola/obuchayushhie-materialy/" data-id="13226" data-products-count="234">Обучающие материалы для детей</a></li>
                                                                <li class="leaf"><a href="/kids/razvitie-shkola/razvitie/" data-id="13225" data-products-count="42">Обучающие плакаты для детей</a></li>
                                                                <li class="leaf"><a href="/kids/razvitie-shkola/posobiya-dlya-detei/" data-id="13279" data-products-count="76">Учимся читать, писать, считать</a></li>
                                                                <li class="leaf"><a href="/kids/razvitie-shkola/detskie-nabori/" data-id="13223" data-products-count="530">Развивающие настольные игры</a></li>
                                                                <li><a href="/kids/razvitie-shkola/">Все развивающие пособия и игры&nbsp;<span>1332</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/detskie-knigi/" data-id="13277">Книги для детей и родителей&nbsp;<span>1224</span></a>
                                                        <div data-id="13277">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/detskie-knigi/dlya-malyshej/" data-id="13278" data-products-count="515">Книги для малышей</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-knigi/govoryashie-knigi/" data-id="14078" data-products-count="152">Говорящие книги</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-knigi/dlya-doshkolnika/" data-id="13280" data-products-count="146">Книги для дошкольника</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-knigi/dlya-shkolnika/" data-id="13281" data-products-count="84">Книги для школьника</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-knigi/knigi-dlya-dosuga/" data-id="13638" data-products-count="204">Книги для досуга</a></li>
                                                                <li class="leaf"><a href="/kids/detskie-knigi/raskraski/" data-id="13248" data-products-count="110">Раскраски для детей</a></li>
                                                                <li><a href="/kids/detskie-knigi/">Все книги для детей и родителей&nbsp;<span>1224</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/nabori-focusi/" data-id="13222">Наборы юного фокусника&nbsp;<span>30</span></a></li>
                                                    <li><a href="/kids/nastolnye-igry-dlya-detej/" data-id="13221">Настольные игры&nbsp;<span>1194</span></a>
                                                        <div data-id="13221">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/nastolnye-igry-dlya-detej/hasbro-mattel-games/" data-id="14058" data-products-count="84">Игры Hasbro и Mattel</a></li>
                                                                <li class="leaf"><a href="/kids/nastolnye-igry-dlya-detej/game-hobby-world/" data-id="14059" data-products-count="20">Игры Hobby World</a></li>
                                                                <li class="leaf"><a href="/kids/nastolnye-igry-dlya-detej/nastolnye_igry_magellan/" data-id="14991" data-products-count="35">Игры Magellan</a></li>
                                                                <li class="leaf"><a href="/kids/nastolnye-igry-dlya-detej/logicheskie-nastolnye-igry/" data-id="14396" data-products-count="48">Логические игры</a></li>
                                                                <li class="leaf"><a href="/kids/nastolnye-igry-dlya-detej/igry-dlya-malyshei/" data-id="14063" data-products-count="111">Игры для малышей</a></li>
                                                                <li class="leaf"><a href="/kids/nastolnye-igry-dlya-detej/igry-dlya-kompanii/" data-id="14062" data-products-count="369">Игры для компании</a></li>
                                                                <li class="leaf"><a href="/kids/nastolnye-igry-dlya-detej/golovolomki-dlja-detej/" data-id="13229" data-products-count="108">Головоломки для детей</a></li>
                                                                <li class="leaf"><a href="/kids/nastolnye-igry-dlya-detej/nastolnye-igry-brodilki/" data-id="14397" data-products-count="125">Настольные игры бродилки</a></li>
                                                                <li class="leaf"><a href="/kids/nastolnye-igry-dlya-detej/loto_domino_shashki_i_shahmaty/" data-id="14269" data-products-count="185">Лото, домино, шашки и шахматы</a></li>
                                                                <li class="leaf"><a href="/kids/nastolnye-igry-dlya-detej/sportivnye_nastolnye_igry/" data-id="14541" data-products-count="82">Спортивные настольные игры</a></li>
                                                                <li><a href="/kids/nastolnye-igry-dlya-detej/">Все настольные игры&nbsp;<span>1194</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/kids/school/" data-id="14128">Школьные товары&nbsp;<span>1244</span></a>
                                                        <div data-id="14128">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/school/globusy/" data-id="13820" data-products-count="43">Глобусы</a></li>
                                                                <li class="leaf"><a href="/kids/school/molberty/" data-id="13264" data-products-count="135">Мольберты и доски для детей</a></li>
                                                                <li class="leaf"><a href="/kids/school/rukzaki/" data-id="13257" data-products-count="746">Ранцы, рюкзаки и сумки</a></li>
                                                                <li class="leaf"><a href="/kids/school/meshki-dlya-obuvi/" data-id="14133" data-products-count="52">Мешки для обуви</a></li>
                                                                <li class="leaf"><a href="/kids/school/penaly/" data-id="13258" data-products-count="263">Пеналы и папки</a></li>
                                                                <li><a href="/kids/school/">Все школьные товары&nbsp;<span>1244</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/bitovaya-tehnika-i-electronika/" data-id="13134">Бытовая техника и электроника&nbsp;<span>34</span></a>
                                                        <div data-id="13134">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/bitovaya-tehnika-i-electronika/podogrevateli-detskogo-pitanija/" data-id="13135" data-products-count="13">Подогреватели для детского питания</a></li>
                                                                <li><a href="/kids/bitovaya-tehnika-i-electronika/">Все бытовая техника и электроника&nbsp;<span>34</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/odezhda-dlja-detej/" data-id="13095">Одежда для детей&nbsp;<span>663</span></a>
                                                        <div data-id="13095">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/odezhda-dlja-detej/karnaval/" data-id="14076" data-products-count="284">Карнавальные костюмы</a></li>
                                                                <li class="leaf"><a href="/kids/odezhda-dlja-detej/verhnyaya-odezhda-dlya-detej/" data-id="13098" data-products-count="164">Верхняя одежда для детей</a></li>
                                                                <li class="leaf"><a href="/kids/odezhda-dlja-detej/vyazannyj-trikotazh/" data-id="13099" data-products-count="14">Вязаный трикотаж для детей</a></li>
                                                                <li class="leaf"><a href="/kids/odezhda-dlja-detej/dlya-novorozhdennyh/" data-id="13102" data-products-count="180">Одежда для новорожденных</a></li>
                                                                <li class="leaf"><a href="/kids/odezhda-dlja-detej/progulochnye-konverty/" data-id="13100" data-products-count="13">Прогулочные конверты</a></li>
                                                                <li><a href="/kids/odezhda-dlja-detej/">Все одежда для детей&nbsp;<span>663</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/sumki-i-rukzaki/" data-id="13178">Сумки и рюкзаки&nbsp;<span>87</span></a>
                                                        <div data-id="13178">
                                                            <ul>
                                                                <li class="leaf"><a href="/kids/sumki-i-rukzaki/sumki-dlya-mami-papi/" data-id="13994" data-products-count="34">Сумки для мамы и папы</a></li>
                                                                <li class="leaf"><a href="/kids/sumki-i-rukzaki/detskie-rukzachki/" data-id="13996" data-products-count="28">Детские рюкзачки</a></li>
                                                                <li class="leaf"><a href="/kids/sumki-i-rukzaki/detskie-chemodani/" data-id="13997" data-products-count="13">Детские чемоданы</a></li>
                                                                <li><a href="/kids/sumki-i-rukzaki/">Все сумки и рюкзаки&nbsp;<span>87</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/kids/kresla-kachalki-dlyu-mamu/" data-id="14348">Кресла-качалки для мамы&nbsp;<span>23</span></a></li>
                                                </ul>
                                            </div>
                                            <div data-id="11" data-items-per-column="9">
                                                <ul>
                                                    <li><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/" data-id="67">Красота и здоровье&nbsp;<span>1126</span></a>
                                                        <div data-id="67">
                                                            <ul>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/feny/" data-id="170" data-products-count="196">Фены</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/fen-shetki/" data-id="13328" data-products-count="45">Фены-щетки</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/schiptsy_dlya_zavivki/" data-id="178" data-products-count="120">Щипцы для завивки</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/elektrobigudi/" data-id="171" data-products-count="17">Электробигуди/Стайлеры</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/vypryamiteli_dlya_volos/" data-id="175" data-products-count="97">Выпрямители для волос</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/pribory_dlya_ukladki_volos/" data-id="179" data-products-count="120">Приборы для укладки волос</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/sushilki_dlya_ruk/" data-id="167" data-products-count="21">Сушилки для рук</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/massazhery/" data-id="169" data-products-count="32">Массажёры</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/elektrobritva/" data-id="172" data-products-count="78">Электробритвы</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/aksessuari-dlya-electrobritv/" data-id="12770" data-products-count="45">Аксессуары для электробритв</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/zubnye_schetki/" data-id="173" data-products-count="33">Зубные щетки</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/nasadki-dlya-zubnih-shetok/" data-id="14473" data-products-count="31">Насадки для зубных щеток</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/epilyatory/" data-id="177" data-products-count="55">Эпиляторы</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/mahinki-dlya-strijki/" data-id="12666" data-products-count="222">Машинки для стрижки</a></li>
                                                                <li><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/krasota-i-zdorove/">Все красота и здоровье&nbsp;<span>1126</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/bytovaya-ximiya-i-kosmetika/" data-id="15877">Бытовая химия и косметика&nbsp;<span>7029</span></a>
                                                        <div data-id="15877">
                                                            <ul>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/bytovaya-ximiya-i-kosmetika/bytovaya-ximiya/" data-id="15855" data-products-count="908">Бытовая химия</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/bytovaya-ximiya-i-kosmetika/dekorativnaya-kosmetika/" data-id="15894" data-products-count="698">Декоративная косметика</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/bytovaya-ximiya-i-kosmetika/lichnyj-uxod-i-gigiena/" data-id="15940" data-products-count="4162">Личный уход и гигиена</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/bytovaya-ximiya-i-kosmetika/galantereya/" data-id="15884" data-products-count="710">Галантерея</a></li>
                                                                <li class="leaf"><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/bytovaya-ximiya-i-kosmetika/domashnee-xozyajstvo/" data-id="15929" data-products-count="551">Домашнее хозяйство</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/napolnye_vesy/" data-id="174">Напольные весы&nbsp;<span>205</span></a></li>
                                                    <li><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/gidromassazhnye_vanny/" data-id="181">Гидромассажные ванны&nbsp;<span>15</span></a></li>
                                                    <li><a href="/krasota_i_zdorove_kosmetika_bitovaya_himiya/manikyurnye_nabory/" data-id="182">Маникюрные наборы&nbsp;<span>40</span></a></li>
                                                </ul>
                                            </div>
                                            <div data-id="12769" data-items-per-column="9">
                                                <ul>
                                                    <li><a href="/office-seti/ofisnaya_tehnika/" data-id="801">Офисная техника&nbsp;<span>300</span></a>
                                                        <div data-id="801">
                                                            <ul>
                                                                <li class="leaf"><a href="/office-seti/ofisnaya_tehnika/schreder/" data-id="4061" data-products-count="168">Уничтожители бумаги</a></li>
                                                                <li class="leaf"><a href="/office-seti/ofisnaya_tehnika/laminatory/" data-id="4062" data-products-count="63">Ламинаторы</a></li>
                                                                <li class="leaf"><a href="/office-seti/ofisnaya_tehnika/broshuratory/" data-id="4063" data-products-count="36">Брошюровщики</a></li>
                                                                <li class="leaf"><a href="/office-seti/ofisnaya_tehnika/rezaky/" data-id="15495" data-products-count="25">Резаки</a></li>
                                                                <li><a href="/office-seti/ofisnaya_tehnika/">Все офисная техника&nbsp;<span>300</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/office-seti/kancelyarskie-tovari/" data-id="12381">Канцелярские товары&nbsp;<span>7244</span></a>
                                                        <div data-id="12381">
                                                            <ul>
                                                                <li class="leaf"><a href="/office-seti/kancelyarskie-tovari/magnitno_markernie_doski/" data-id="638" data-products-count="27">Магнитно маркерные доски</a></li>
                                                                <li class="leaf"><a href="/office-seti/kancelyarskie-tovari/probkovie_doski/" data-id="639" data-products-count="12">Пробковые доски</a></li>
                                                                <li class="leaf"><a href="/office-seti/kancelyarskie-tovari/kartoteki_podvesnaya_registratura/" data-id="667" data-products-count="37">Картотеки, подвесная регистратура</a></li>
                                                                <li class="leaf"><a href="/office-seti/kancelyarskie-tovari/biznes-suveniri/" data-id="13568" data-products-count="126">Бизнес сувениры</a></li>
                                                                <li class="leaf"><a href="/office-seti/kancelyarskie-tovari/izdelie-iz-bumagi/" data-id="12382" data-products-count="1208">Изделия из бумаги</a></li>
                                                                <li class="leaf"><a href="/office-seti/kancelyarskie-tovari/nabor-dlya-tvorchestva/" data-id="12386" data-products-count="26">Набор для детского творчества</a></li>
                                                                <li class="leaf"><a href="/office-seti/kancelyarskie-tovari/nastolnie-prinadlejnosti/" data-id="12387" data-products-count="1222">Настольные принадлежности</a></li>
                                                                <li class="leaf"><a href="/office-seti/kancelyarskie-tovari/papki-faili-sistemi-arhivacii/" data-id="12445" data-products-count="914">Папки, файлы, системы архивации</a></li>
                                                                <li class="leaf"><a href="/office-seti/kancelyarskie-tovari/pishushie-prinadlejnosti/" data-id="12458" data-products-count="3315">Пишущие принадлежности</a></li>
                                                                <li class="leaf"><a href="/office-seti/kancelyarskie-tovari/tablichki/" data-id="12473" data-products-count="206">Таблички</a></li>
                                                                <li class="leaf"><a href="/office-seti/kancelyarskie-tovari/kraski/" data-id="14138" data-products-count="151">Краски и кисти</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/office-seti/rfid-oborudovanie/" data-id="15260">RFID-оборудование&nbsp;<span>2</span></a>
                                                        <div data-id="15260">
                                                            <ul>
                                                                <li><a href="/office-seti/rfid-oborudovanie/">Все rfid-оборудование&nbsp;<span>2</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/office-seti/audio-videokonferencii-i-kongress-oborudovanie/" data-id="15733">Аудио-видеоконференции и конгресс оборудование&nbsp;<span>9</span></a></li>
                                                    <li><a href="/office-seti/kamery_videonablyudeniya/" data-id="101">Камеры видеонаблюдения&nbsp;<span>125</span></a></li>
                                                    <li><a href="/office-seti/stoli_komputernie/" data-id="11496">Офисная мебель&nbsp;<span>144</span></a></li>
                                                    <li><a href="/office-seti/calkulyatori/" data-id="11804">Калькуляторы&nbsp;<span>185</span></a></li>
                                                    <li><a href="/office-seti/bankovskoe-oborudovanie/" data-id="12363">Банковское оборудование&nbsp;<span>3</span></a></li>
                                                    <li><a href="/office-seti/aksessuari_dlya_videonabludeniya/" data-id="11469">Аксессуары для видеонаблюдения&nbsp;<span>169</span></a></li>
                                                </ul>
                                            </div>
                                            <div data-id="12341" data-items-per-column="4">
                                                <ul>
                                                    <li><a href="/uslugi/diagnostika-oborudovaniya/" data-id="14936">Диагностика оборудования, сборка, модернизация&nbsp;<span>26</span></a></li>
                                                    <li><a href="/uslugi/nakleika-plenok/" data-id="14943">Наклейка пленок&nbsp;<span>2</span></a></li>
                                                    <li><a href="/uslugi/proverka-na-bitie-pixely/" data-id="14935">Проверка на битые пиксели&nbsp;<span>6</span></a></li>
                                                    <li><a href="/uslugi/rabota-s-nasitelyami-informacii/" data-id="14937">Работа с носителями информации&nbsp;<span>4</span></a></li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/uslugi/ustanovka-paketov-programm/" data-id="14938">Установка пакетов программ&nbsp;<span>6</span></a>
                                                        <div data-id="14938">
                                                            <ul>
                                                                <li><a href="/uslugi/ustanovka-paketov-programm/">Все установка пакетов программ&nbsp;<span>6</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="/uslugi/ustanovka-po/" data-id="14934">Установка ПО&nbsp;<span>11</span></a></li>
                                                    <li><a href="/uslugi/proche-uslugy/" data-id="14939">Прочее&nbsp;<span>9</span></a></li>
                                                </ul>
                                            </div>
                                            <div data-id="15394" data-items-per-column="6">
                                                <ul>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_kompyutery_noutbuki_soft/" data-id="15436">Уценка: Компьютеры, ноутбуки, софт&nbsp;<span>7</span></a></li>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_kids/" data-id="15453">Уценка: Товары для детей&nbsp;<span>53</span></a></li>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_komputernaya_periferiya/" data-id="15454">Уценка: Компьютерная периферия&nbsp;<span>26</span></a></li>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_komplektuyuschie_dlya_pk/" data-id="15455">Уценка: Комплектующие для ПК&nbsp;<span>86</span></a></li>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_apple/" data-id="15456">Уценка: Продукция Apple&nbsp;<span>4</span></a></li>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_telefoniya/" data-id="15457">Уценка: Смартфоны и гаджеты&nbsp;<span>22</span></a></li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_audio-video_i_tv/" data-id="15458">Уценка: Телевизоры и видео&nbsp;<span>1</span></a></li>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_audio-technika/" data-id="15459">Уценка: Аудио техника&nbsp;<span>4</span></a></li>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_office-seti/" data-id="15461">Уценка: Офис и сети&nbsp;<span>34</span></a></li>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_bytovaya_tehnika/" data-id="15462">Уценка: Товары и техника для дома&nbsp;<span>38</span></a></li>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_klimaticheskaya_tehnika/" data-id="15464">Уценка: Климатическая техника&nbsp;<span>10</span></a></li>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_krupnaya-bitovaya-tehnika/" data-id="15466">Уценка: Крупная бытовая техника&nbsp;<span>1</span></a></li>
                                                </ul>
                                                <ul>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_bytovaya_texnika_vstraivaemaya/" data-id="15467">Уценка: Бытовая техника встраиваемая&nbsp;<span>2</span></a></li>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_car-electronics/" data-id="15469">Уценка: Всё для авто&nbsp;<span>12</span></a></li>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_instrumenti_sad_ogorod/" data-id="15471">Уценка: Инструменты.Сад.Огород&nbsp;<span>6</span></a></li>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_injenernaya-santehnika/" data-id="15472">Уценка: Инженерная сантехника&nbsp;<span>1</span></a></li>
                                                    <li><a href="/123ru-ucenca/123ru-ucenca_neremont/" data-id="15473">Уценка: Неремонтопригодные товары&nbsp;<span>84</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="bottom-main-header">
                            <div class="srch-city">
                                <div class="wrapp-search-field">
                                    <form action="/search/" method="get">
                                        <label for="searchinput"></label>
                                        <input class="input-base" id="searchinput" name="q" type="text" placeholder="Поиск по 132 095 товарам в наличии">
                                        <div class="srch-city__link">
                                            <ul>
                                                <li><a href="/kids/">детские товары</a></li>
                                                <li><a href="/komplektuyuschie_dlya_pk/">комплектующие</a></li>
                                                <li><a href="/kompyutery_noutbuki_soft/notebooks/">ноутбуки</a></li>
                                                <li><a href="/remont_i_stroitelstvo/instrumenti_sad_ogorod/">электроинструменты</a></li>
                                                <li><a href="/vse_dlya_doma_i_sada/sad_i_ogorod/saovaya_technica/">садовая техника</a></li>
                                                <li class="last"><a href="/car-electronics/avtomobilnie-shini/">шины</a></li>
                                            </ul>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="cart-camp">
                                <div class="compare"><a href="#" data-window="#empty-compare" data-position="right" data-autofocus="true" data-background="true">Сравнение</a><span class="count"></span></div>
                                <div class="favorites"><a href="#" data-window="#login" data-position="right" data-autofocus="true" data-background="true" data-popuptitle="Авторизуйтесь и добавьте товар в избранное для отслеживания">Избранное</a><span class="count"></span></div>
                                <div class="add-to-cart cart-box" data-window="#cart" data-hover="true" data-position="right">
                                    <a href="https://www.123.ru/ordering/" data-window="#cart" data-position="responsive" class="standart-btn" id="cartlink">
                                        <div class="icon-cart">
                                            <svg width="20px" height="16px" viewBox="0 0 20 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                <defs></defs>
                                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <g id="Group" transform="translate(1.000000, 1.000000)" stroke="#2673C2" stroke-width="2">
                                                        <path d="M11.5,0.5 L13.5,3.5" id="Line" stroke-linecap="square"></path>
                                                        <path d="M4.5,0.5 L6.5,3.5" id="Line" stroke-linecap="square" transform="translate(5.500000, 2.000000) scale(-1, 1) translate(-5.500000, -2.000000) "></path>
                                                        <path d="M0.5,4 L17.5,4 C17.5,6.66666667 17.1666667,8.83333333 16.5,10.5 C15.8333333,12.1666667 15.1666667,13.3333333 14.5,14 L3.4875078,14 C2.99319543,13.7499048 2.33069283,12.5832381 1.5,10.5 C0.669307173,8.41676188 0.33597384,6.25009521 0.5,4 Z" id="Path-2"></path>
                                                    </g>
                                                </g>
                                            </svg><span class="global-count-cart">3</span></div><span class="cart-cash">72 790<span class="currency"> р.</span></span>
                                    </a>
                                    <div class="modal-add-position">
                                        <div class="header"><span>Корзина</span>&nbsp;<span class="c-gray">3 товара</span><a href="#" data-action="close"><i class="material-icons">close</i></a></div>
                                        <div class="body">
                                            <div class="window-cart-items"></div>
                                            <div class="total"><span>Всего товаров на сумму:</span>
                                                <div class="price"><span>72 790</span><span class="cur">р.</span></div>
                                            </div>
                                        </div>
                                        <div class="bottom"><a href="https://www.123.ru/ordering/" class="btn">оформить заказ</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mobile__header">
                            <div class="btn__mob-menu">
                                <div class="wrapp-hamb"><span></span></div>
                            </div>
                            <div class="img">
                                <a href="/"><img src="/i/main_logo.svg" height="40px"></a>
                            </div>
                            <div class="search-cart">
                                <div class="btn_search"><i class="material-icons">search</i></div>
                                <div class="btn__cart">
                                    <a href="/ordering/">
                                        <svg width="25px" height="21px" viewBox="0 0 20 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                            <defs></defs>
                                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <g id="Group" transform="translate(1.000000, 1.000000)" stroke="#797979" stroke-width="2">
                                                    <path d="M11.5,0.5 L13.5,3.5" id="Line" stroke-linecap="square"></path>
                                                    <path d="M4.5,0.5 L6.5,3.5" id="Line" stroke-linecap="square" transform="translate(5.500000, 2.000000) scale(-1, 1) translate(-5.500000, -2.000000) "></path>
                                                    <path d="M0.5,4 L17.5,4 C17.5,6.66666667 17.1666667,8.83333333 16.5,10.5 C15.8333333,12.1666667 15.1666667,13.3333333 14.5,14 L3.4875078,14 C2.99319543,13.7499048 2.33069283,12.5832381 1.5,10.5 C0.669307173,8.41676188 0.33597384,6.25009521 0.5,4 Z" id="Path-2"></path>
                                                </g>
                                            </g>
                                        </svg><span class="global_count">3</span></a>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div class="mobile-site-menu">
            <ul>
                <li><a href="#" data-window="#cities" data-background="true" data-responsive="true" class="link-sel w-a" id="#citysel">Ваш город: Москва</a></li>
                <li>
                    <div class="callback-mob"><a class="tel" href="tel:8 (495) 225-9-123">8 (495) 225-9-123</a><a href="#" class="call_back" data-window="#callback" data-background="true" data-position="right" data-autofocus="true">Обратный звонок</a></div>
                </li>
                <li><a href="#" data-window="#login" data-position="right" data-autofocus="true" data-background="true" class="link-sel w w-a w-cab">Вход в личный кабинет</a></li>
                <li class="catalog"><a class="w w-a w-cat" href="/catalog/">Каталог</a></li>
                <li><a class="w w-a w-camp" href="#">Сравнение </a></li>
                <li><a class="w w-a w-fav" href="#" data-window="#login" data-position="right" data-autofocus="true" data-background="true" data-popuptitle="Авторизуйтесь и добавьте товар в избранное для отслеживания">Избранное </a></li>
                <li><a class="w" href="/about/contacts/">Контакты</a></li>
                <li><a class="w w-a w-d-d" href="#">Покупателям</a>
                    <ul class="submenu">
                        <li><a href="/about/delivery.php/">Доставка</a></li>
                        <li><a href="/pay/">Оплата</a></li>
                        <li><a href="/credit/">Покупка в кредит</a></li>
                        <li><a href="/sovest/">Рассрочка с картой Совесть</a></li>
                        <li><a href="/halva/">Рассрочка с картой Халва</a></li>
                    </ul>
                </li>
                <li><a class="w" href="/warranty/">Гарантия</a></li>
                <li><a class="w" href="/b2b/">Корпоративным клиентам</a></li>
                <li><a class="w" href="/supplier/">Поставщикам</a></li>
                <li><a class="w w-pay" href="/quick-pay/">Оплатить</a></li>
            </ul>
        </div>
    </header>
    <div class="search-box-new">
        <form class="form-search search-hidden" action="/search/" method="get">
            <label for="searchinput">Поиск</label>
            <input type="text" name="q" id="searchinput" placeholder=""><i class="icon icon-close"></i></form>
    </div>
    <div class="catalog-link-container"><a href="#" class="catalog-header-link">Каталог товаров</a></div>
    <div class="clearfix"></div>
    <section class="breadcrumbs container hidden-xs item-parts">
        <ul>
            <li><a href="/komplektuyuschie_dlya_pk/" title="Комплектующие для ПК">Комплектующие для ПК</a><i class="icon ic-arrow-left"></i></li>
            <li><a href="/komplektuyuschie_dlya_pk/videokarti/" title="Видеокарты">Видеокарты</a><i class="icon ic-arrow-left"></i></li>
            <li><a href="/komplektuyuschie_dlya_pk/videokarti/brand-gigabyte/" title="Видеокарты GigaByte">Видеокарты GigaByte</a><i class="icon ic-arrow-left"></i></li>
            <li>видеокарта GigaByte GV-N105TOC-4GL</li>
        </ul>
        <div class="-item-actions">
            <a href='#' class='-add-to-favorites<?php echo $fav_state; ?>' data-action="add-to-favorites" data-id="482394"><?php echo $fav_text;?></a>
            <a href='#' class='-add-to-compare<?php echo $cmp_state; ?>' data-action="add-to-compare" data-id="482394"><?php echo $cmp_text;?></a>
            <a href='#' class='-share' data-id="482394" data-action="share">Поделиться</a>
        </div>
        <script type="application/ld+json">{"@context":"http://schema.org","@type":"BreadcrumbList","itemListElement":[{"@type":"ListItem","position":1,"item":{"@id":"http://www.123.ru/komplektuyuschie_dlya_pk/","name":"Комплектующие для ПК"}},{"@type":"ListItem","position":2,"item":{"@id":"http://www.123.ru/komplektuyuschie_dlya_pk/videokarti/","name":"Видеокарты"}},{"@type":"ListItem","position":3,"item":{"@id":"http://www.123.ru/komplektuyuschie_dlya_pk/videokarti/brand-gigabyte/","name":"Видеокарты GigaByte"}},{"@type":"ListItem","position":4,"item":{"@id":"http://www.123.ru","name":"видеокарта GigaByte GV-N105TOC-4GL 4096Mb GeForce GTX 1050 Ti GDDR5"}}]}</script>
    </section>
    <section class="breadscrumbs container only-mobile">
        <span class="upper"><a href="#">Samsung Galaxy S9+ 64</a></span>
        <span class="art">0679254</span>
    </section>
    
    <section class="-compare-content">
        <div class="container">
            <h1>Сравнение продуктов</h1>        
        </div>
        <div class="container bg-white all-categories">
            <ul>
                <li class="current"><a href="?smartphones">Смартфоны</a></li>
                <li><a href="?notebooks">Ноутбуки</a></li>
                <li><a href="?ps4">PS4</a></li>
            </ul>
        </div>
        <div class="-compare-header">
            <div class="container -items">
                <div class="start-block">
                    <h4>Смартфоны <span>(6)</span></h4>
                </div>
                <div class="all-blocks">
                    <ul>
                        <li data-id="3493"><a href="#" class="-delete"></a>
                            <div class="image"><img src="http://www.123.ru/l_pics/80054955.jpg"></div>
                            <span class="-brand-line" style="background-image: url(/img/asus.png)"></span>
                            <a href="/gadgets/communicators/brand-asus/smartfon_asus_zenfone_3_max_zc553kl_16_gb_seriy_art80054955/" class="title">Смартфон ASUS ZenFone 3 Max ZC553KL 16 Гб серый (90AX00D2-M01780) Смартфон ASUS ZenFone 3 Max ZC553KL 16 Гб серый (90AX00D2-M01780) Смартфон ASUS ZenFone 3 Max ZC553KL 16 Гб серый (90AX00D2-M01780)</a>
                            <div class="price">
                                <strong>10 280 <i class="rub">a</i></strong><strong class="old-price">12 390 <i class="rub">a</i></strong>
                                
                                <div class="-item-actions">
                                    <a href="#" class="-add-to-favorites" data-action="add-to-favorites" data-id="15293205"></a>
                                </div>
                            </div>
                            <a href="/item.php" class="item-page">посмотреть детали</a>
                        </li>
                        <li data-id="3497"><a href="#" class="-delete"></a><div class="image"><img src="http://www.123.ru/l_pics/80053717.jpg"></div><span class="-brand-line" style="background-image: url(/img/samsung.png)"></span><a href="/gadgets/communicators/brand-asus/smartfon_asus_zenfone_3_max_zc553kl_16_gb_seriy_art80054955/" class="title">Смартфон Alcatel Pixi 4 Plus Power 5023F 16 Гб белый (5023F-2BALRU2)</a><div class="price"><strong>10 280 <i class="rub">a</i></strong><strong class="old-price">12 390 <i class="rub">a</i></strong><div class="-item-actions"><a href="#" class="-add-to-favorites" data-action="add-to-favorites" data-id="15293205"></a></div></div><a href="/item.php" class="item-page">посмотреть детали</a></li>
                        <li data-id="3498"><a href="#" class="-delete"></a><div class="image"><img src="http://www.123.ru/l_pics/80188194.jpg"></div><span class="-brand-line" style="background-image: url(/img/asus.png)"></span><a href="/gadgets/communicators/brand-asus/smartfon_asus_zenfone_3_max_zc553kl_16_gb_seriy_art80054955/" class="title">Смартфон Alcatel Pixi 4 Plus Power 5023F 16 Гб белый (5023F-2BALRU2)</a><div class="price"><strong>10 280 <i class="rub">a</i></strong><strong class="old-price">12 390 <i class="rub">a</i></strong><div class="-item-actions"><a href="#" class="-add-to-favorites" data-action="add-to-favorites" data-id="15293205"></a></div></div><a href="/item.php" class="item-page">посмотреть детали</a></li>
                        <li data-id="3499"><a href="#" class="-delete"></a><div class="image"><img src="http://www.123.ru/l_pics/80185424.jpg"></div><span class="-brand-line" style="background-image: url(/img/samsung.png)"></span><a href="/gadgets/communicators/brand-asus/smartfon_asus_zenfone_3_max_zc553kl_16_gb_seriy_art80054955/" class="title">Смартфон Alcatel Pixi 4 Plus Power 5023F 16 Гб белый (5023F-2BALRU2)</a><div class="price"><strong>10 280 <i class="rub">a</i></strong><strong class="old-price">12 390 <i class="rub">a</i></strong><div class="-item-actions"><a href="#" class="-add-to-favorites" data-action="add-to-favorites" data-id="15293205"></a></div></div><a href="/item.php" class="item-page">посмотреть детали</a></li>
                        <li data-id="3491"><a href="#" class="-delete"></a><div class="image"><img src="http://www.123.ru/l_pics/80188188.jpg"></div><span class="-brand-line" style="background-image: url(/img/asus.png)"></span><a href="/gadgets/communicators/brand-asus/smartfon_asus_zenfone_3_max_zc553kl_16_gb_seriy_art80054955/" class="title">Смартфон Alcatel Pixi 4 Plus Power 5023F 16 Гб белый (5023F-2BALRU2)</a><div class="price"><strong>10 280 <i class="rub">a</i></strong><strong class="old-price">12 390 <i class="rub">a</i></strong><div class="-item-actions"><a href="#" class="-add-to-favorites" data-action="add-to-favorites" data-id="15293205"></a></div></div><a href="/item.php" class="item-page">посмотреть детали</a></li>
                        <li data-id="3492"><a href="#" class="-delete"></a><div class="image"><img src="http://www.123.ru/l_pics/80056332.jpg"></div><span class="-brand-line" style="background-image: url(/img/samsung.png)"></span><a href="/gadgets/communicators/brand-asus/smartfon_asus_zenfone_3_max_zc553kl_16_gb_seriy_art80054955/" class="title">Смартфон Alcatel Pixi 4 Plus Power 5023F 16 Гб белый (5023F-2BALRU2)</a><div class="price"><strong>10 280 <i class="rub">a</i></strong><strong class="old-price">12 390 <i class="rub">a</i></strong><div class="-item-actions"><a href="#" class="-add-to-favorites" data-action="add-to-favorites" data-id="15293205"></a></div></div><a href="/item.php" class="item-page">посмотреть детали</a></li>
                    </ul>
                </div>
                <a href="#" class="-scroll -left disabled"></a>
                <a href="#" class="-scroll -right"></a>
            </div>
        </div>
        <div class="compare-parameters container">
            <div class="parameter">
                <label>Рейтинг</label>
                <ul class="values">
                    <li><span class="-stars -l4"></span><a href='#'>212</a></li>
                    <li><span class="-stars -l2"></span><a href='#'>346</a></li>
                    <li><span class="-stars -l5"></span><a href='#'>11</a></li>
                    <li><span class="-stars -l5"></span><a href='#'>53</a></li>
                    <li><span class="-stars -l3"></span><a href='#'>2</a></li>
                    <li><span class="-stars -l4"></span><a href='#'>84</a></li>
                </ul>
            </div>
            <div class="parameter"><label>Операционная система</label><ul class="values"><li>Windows 8</li><li>Android 8.1</li><li>ChromeOS</li><li>Windows 8</li><li>Android 8.1</li><li>Android 7.0</li></ul></div><div class="parameter"><label>Диагональ экрана</label><ul class="values"><li>4.7"</li><li>5.0"</li><li>5.4"</li><li>5.0"</li><li>5.7"</li><li>4.2"</li></ul></div><div class="parameter"><label>CPU</label><ul class="values"><li>Intel Core i7-8550U</li><li>Intel Core i7-8550U</li><li>Intel Core i7-8550U</li><li>Intel Core i7-8550U</li><li>Intel Core i7-8550U</li><li>Intel Core i7-8550U</li></ul></div><div class="parameter"><label>Частота CPU</label><ul class="values"><li>2.4 GHz</li><li>1.9 GHz</li><li>2.5 GHz</li><li>2.5 GHz</li><li>2.0 GHz</li><li>2.3 GHz</li></ul></div><div class="parameter"><label>Разрешение экрана</label><ul class="values"><li>1080x1920</li><li>800x1280</li><li>1080x1920</li><li>800x1280</li><li>1080x1920</li><li>800x1280</li></ul></div><div class="parameter"><label>MicroSD</label><ul class="values"><li>до 64 Гб</li><li>–</li><li>до 64 Гб</li><li>до 32 Гб</li><li>–</li><li>–</li></ul></div><div class="parameter"><label>Операционная система</label><ul class="values"><li>Windows 8</li><li>Android 8.1</li><li>ChromeOS</li><li>Windows 8</li><li>Android 8.1</li><li>Android 7.0</li></ul></div><div class="parameter"><label>Диагональ экрана</label><ul class="values"><li>4.7"</li><li>5.0"</li><li>5.4"</li><li>5.0"</li><li>5.7"</li><li>4.2"</li></ul></div><div class="parameter"><label>CPU</label><ul class="values"><li>Intel Core i7-8550U</li><li>Intel Core i7-8550U</li><li>Intel Core i7-8550U</li><li>Intel Core i7-8550U</li><li>Intel Core i7-8550U</li><li>Intel Core i7-8550U</li></ul></div><div class="parameter"><label>Частота CPU</label><ul class="values"><li>2.4 GHz</li><li>1.9 GHz</li><li>2.5 GHz</li><li>2.5 GHz</li><li>2.0 GHz</li><li>2.3 GHz</li></ul></div><div class="parameter"><label>Разрешение экрана</label><ul class="values"><li>1080x1920</li><li>800x1280</li><li>1080x1920</li><li>800x1280</li><li>1080x1920</li><li>800x1280</li></ul></div><div class="parameter"><label>MicroSD</label><ul class="values"><li>до 64 Гб</li><li>–</li><li>до 64 Гб</li><li>до 32 Гб</li><li>–</li><li>–</li></ul></div><div class="parameter"><label>Операционная система</label><ul class="values"><li>Windows 8</li><li>Android 8.1</li><li>ChromeOS</li><li>Windows 8</li><li>Android 8.1</li><li>Android 7.0</li></ul></div><div class="parameter"><label>Диагональ экрана</label><ul class="values"><li>4.7"</li><li>5.0"</li><li>5.4"</li><li>5.0"</li><li>5.7"</li><li>4.2"</li></ul></div><div class="parameter"><label>CPU</label><ul class="values"><li>Intel Core i7-8550U</li><li>Intel Core i7-8550U</li><li>Intel Core i7-8550U</li><li>Intel Core i7-8550U</li><li>Intel Core i7-8550U</li><li>Intel Core i7-8550U</li></ul></div><div class="parameter"><label>Частота CPU</label><ul class="values"><li>2.4 GHz</li><li>1.9 GHz</li><li>2.5 GHz</li><li>2.5 GHz</li><li>2.0 GHz</li><li>2.3 GHz</li></ul></div><div class="parameter"><label>Разрешение экрана</label><ul class="values"><li>1080x1920</li><li>800x1280</li><li>1080x1920</li><li>800x1280</li><li>1080x1920</li><li>800x1280</li></ul></div><div class="parameter"><label>MicroSD</label><ul class="values"><li>до 64 Гб</li><li>–</li><li>до 64 Гб</li><li>до 32 Гб</li><li>–</li><li>–</li></ul></div>
        </div>
    </section>
    <div class="window box-shadow pc-del-window" id="pc-productzoom">
        <div class="header-wrapper">
            <div class="header">
                Видеокарта GigaByte GeForce GTX 1050 Ti GV-N105TOC-4GL
                <a href="#" data-action="close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 12 12">
                        <svg xmlns:xlink="http://www.w3.org/1999/xlink" height="12" width="12" version="1.1" viewBox="0 0 12 12" id="close" y="0">
                            <title>Close</title>
                            <desc></desc>
                            <g id="close-category_adaptive" fill-rule="evenodd" fill="none">
                                <g id="close-05_Category-ot-320" fill="#9B9B9B" transform="translate(-92 -376)">
                                    <g id="" transform="translate(0 360)">
                                        <g id="" transform="translate(92 13)">
                                            <path id="close-Fill-20" d="m5.9998 10.692l-4.0537 4.054c-0.3876 0.388-1.0122 0.391-1.4097-0.006l-0.27654-0.277c-0.39099-0.391-0.39594-1.02-0.00627-1.409l4.0537-4.0542-4.0537-4.0537c-0.38754-0.3876-0.39127-1.0122 0.00626-1.4097l0.27656-0.2765c0.39099-0.391 1.02-0.396 1.4097-0.0063l4.0537 4.0537 4.0542-4.0537c0.387-0.3875 1.012-0.3913 1.409 0.0063l0.277 0.2765c0.391 0.391 0.396 1.02 0.006 1.4097l-4.0533 4.0537 4.0533 4.0542c0.388 0.387 0.392 1.012-0.006 1.409l-0.277 0.277c-0.391 0.391-1.02 0.396-1.409 0.006l-4.0542-4.054z"></path>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </svg>
                </a>
            </div>
            <div class="body">
                <div class="-slide-container">
                    <aside class="full-img">
                        <a href="#" class="-scroll -prev"></a>
                        <a href="#" class="-scroll -next"></a>
                        <div class="-viewport"></div>
                        <div class="-zoom-nav">
                            <span>Масштаб</span>
                            <a href='#' class='-zoom -in'>+</a>
                            <a href='#' class='-zoom -out disabled'>–</a>
                            <a href='#' class='-reset disabled'>Сбросить</a>
                        </div>
                    </aside>
                    <aside class="-thumbs">
                        <ul>
                            <li class="current" data-image="//www.123.ru/xl_pics/8990358_1.jpg"><img src="//www.123.ru/xl_pics/8990358_1.jpg"></li>
                            <li data-image="//www.123.ru/xl_pics/8990358.jpg"><img src="//www.123.ru/xl_pics/8990358.jpg"></li>
                            <li data-image="//www.123.ru/xl_pics/8990358_2.jpg"><img src="//www.123.ru/xl_pics/8990358_2.jpg"></li>
                            <li data-image="//www.123.ru/xl_pics/8990358_3.jpg"><img src="//www.123.ru/xl_pics/8990358_3.jpg"></li>
                        </ul>
                    </aside>
                </div>
            </div>
        </div>
    </div>
    <div class="-upd window box-shadow pc-del-window noscroll" id="pc-quickbuy" data-product-id="8990358" data-product-price="12630" data-product-quantity="1" data-product-rr-id="663989">
        <div class="header-wrapper">
            <div class="header"><span class="wsd-header noPadding">Быстрый заказ</span>
                <a href="#" data-action="close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 12 12">
                        <svg xmlns:xlink="http://www.w3.org/1999/xlink" height="12" width="12" version="1.1" viewBox="0 0 12 12" id="close" y="0">
                            <title>Close</title>
                            <desc></desc>
                            <g id="close-category_adaptive" fill-rule="evenodd" fill="none">
                                <g id="close-05_Category-ot-320" fill="#9B9B9B" transform="translate(-92 -376)">
                                    <g id="" transform="translate(0 360)">
                                        <g id="" transform="translate(92 13)">
                                            <path id="close-Fill-20" d="m5.9998 10.692l-4.0537 4.054c-0.3876 0.388-1.0122 0.391-1.4097-0.006l-0.27654-0.277c-0.39099-0.391-0.39594-1.02-0.00627-1.409l4.0537-4.0542-4.0537-4.0537c-0.38754-0.3876-0.39127-1.0122 0.00626-1.4097l0.27656-0.2765c0.39099-0.391 1.02-0.396 1.4097-0.0063l4.0537 4.0537 4.0542-4.0537c0.387-0.3875 1.012-0.3913 1.409 0.0063l0.277 0.2765c0.391 0.391 0.396 1.02 0.006 1.4097l-4.0533 4.0537 4.0533 4.0542c0.388 0.387 0.392 1.012-0.006 1.409l-0.277 0.277c-0.391 0.391-1.02 0.396-1.409 0.006l-4.0542-4.054z"></path>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </svg>
                </a>
            </div>
        </div>
        <div class="typ">В ближайшее время, по указанному вами номеру телефона, с вами свяжется наш менеджер для уточнения деталей заказа.</div>
        <form method="post" action="/ordering/quick/" name="form-quick-order" id="form-quick-order">
            <input type="hidden" name="products[]" value="8990358" />
            <input type="hidden" name="total-amount" value="12630" />
            <div class="body">
                <div class="items">
                <ul class="window-cart-items clearstyle">
                    <li><a href="/item.php" class='-img'><img src='http://www.123.ru/xl_pics/8990358_1.jpg'></a>
                        <div class="info">
                            <div class="title"><a href="/item/">Видеокарта GigaByte GeForce GTX 1050 Ti GV-N105TWF2OC-4GD PCI-E 4096Mb</a></div>
                            <div class="-art-rating">
                                <div class="vendor-code">Арт: 8602063</div>
                                <div class="sp-listing-inline-rating-widget">
                                    <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-off"></div></div>
                                    <div class="rating-count">15</div>
                                </div>
                            </div>
                            <div class="price c-blue">
                                <span>12 630</span>
                                <em>12 860</em>
                                <p class="-discount">Скидка -6% <b>(2 120 р.)</b></p>
                            </div>
                            <div class="countbox" data-product-id="80032863">
                                <a href="#" class="minus"></a>
                                <input type="text" name="count" value="1" data-min="1" data-max="1" data-hold="true" data-price="12630">
                                <a href="#" class="plus"></a>
                            </div>
                            
                            <div class="-promo">
                                <input type="text" id="-promocode" placeholder="Введите промокод"><a href="#" class="apply-promo">Применить</a><span class="ok">Промо-код применен. <a href='#' class='cancelpromo'>Отменить</a></span>
                            </div>
                        </div>
                     </li>
                </ul>
                </div>
            </div>
            <div class="bottom form-elements">
                <div class="row">
                    <div class="form-desc">Тип доставки</div>
                    <div class="form-input">
                        <input id="DELIVERY_1" type="radio" checked name="receipt" value="pickup" />
                        <label for="DELIVERY_1">Самовывоз</label>
                        <input id="DELIVERY_2" type="radio" name="receipt" value="delivery" />
                        <label for="DELIVERY_2">Курьерская доставка</label>
                    </div>
                </div>
                <div class="row">
                    <div class="form-desc">Введите Ваше имя</div>
                    <div class="form-input">
                        <input type="text" name="fio" value="">
                    </div>
                </div>
                <div class="row">
                    <div class="form-desc">Моб. телефон</div>
                    <div class="form-input">
                        <input id="phone" type="tel" name="phone" class="masked" mask="+7 (999) 999-99-99" mask-placeholder="_" placeholder="+7 (___) ___-__-__" value="" />
                    </div>
                </div>
                <div class="row">
                    <input type="button" class="sendform btn btn-yellow btn-lg btn-submit" value="Отправить заказ" />
                    <input type="submit" value="" style="display: none;" />
                </div>
                <div class="form-description">* Нажимая кнопку "Отправить заказ", я подтверждаю свое согласие на получение информации об оформлении и получении заказа, согласие на обработку персональных данных в соответствии с указаным <a href="/publicoffer/#privacy" target="_blank">здесь</a> текстом.</div>
            </div>
        </form>
    </div>
    <div class="window box-shadow pc-del-window" id="pc-credit">
        <div class="header-wrapper">
            <div class="header">
                <a href="#" data-action="close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 12 12">
                        <svg xmlns:xlink="http://www.w3.org/1999/xlink" height="12" width="12" version="1.1" viewBox="0 0 12 12" id="close" y="0">
                            <title>Close</title>
                            <desc></desc>
                            <g id="close-category_adaptive" fill-rule="evenodd" fill="none">
                                <g id="close-05_Category-ot-320" fill="#9B9B9B" transform="translate(-92 -376)">
                                    <g id="" transform="translate(0 360)">
                                        <g id="" transform="translate(92 13)">
                                            <path id="close-Fill-20" d="m5.9998 10.692l-4.0537 4.054c-0.3876 0.388-1.0122 0.391-1.4097-0.006l-0.27654-0.277c-0.39099-0.391-0.39594-1.02-0.00627-1.409l4.0537-4.0542-4.0537-4.0537c-0.38754-0.3876-0.39127-1.0122 0.00626-1.4097l0.27656-0.2765c0.39099-0.391 1.02-0.396 1.4097-0.0063l4.0537 4.0537 4.0542-4.0537c0.387-0.3875 1.012-0.3913 1.409 0.0063l0.277 0.2765c0.391 0.391 0.396 1.02 0.006 1.4097l-4.0533 4.0537 4.0533 4.0542c0.388 0.387 0.392 1.012-0.006 1.409l-0.277 0.277c-0.391 0.391-1.02 0.396-1.409 0.006l-4.0542-4.054z"></path>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </svg>
                </a>
            </div>
            <div class="row"><b>Кредит от Альфабанка, ОТП Банка, Ренессанс Кредит, Home Credit Bank,Paylate и Kviku</b></div>
            <div class="desc">Выберите способ оплаты “Онлайн-кредит” в корзине и заполните заявку. Банк ответит через несколько минут<a href="/credit/">подробнее</a></div>
        </div>
    </div>
    <div class="window box-shadow pc-del-window" id="pc-club">
        <div class="hide-me" data-hide-uid="5bc6cfee9d48f6.54107617"></div>
    </div>
    <div class="window box-shadow pc-del-window" id="pc-selfdelivery"></div>
    <div class="window box-shadow pc-del-window pc-del-w" id="pc-delivery"></div>
    <section id="advantages">
        <div class="container">
            <div class="content-cnt">
                <div class="wrapp-advantages">
                    <div class="item">
                        <div class="image"><img src="/i/icon_avnt1.svg" height="77px"></div>
                        <p>6779 пунктов
                            <br />самовывоза</p>
                    </div>
                    <div class="item">
                        <div class="image"><img src="/i/icon_avnt2.svg" height="77px"></div>
                        <p>Более 110 тысяч
                            <br> товаров</p>
                    </div>
                    <div class="item">
                        <div class="image"><img src="/i/icon_avnt4.svg" height="77px"></div>
                        <p>Бонусная программа</p>
                    </div>
                    <div class="item">
                        <div class="image"><img src="/i/icon_avnt3.svg" height="77px"></div>
                        <p>Официальная
                            <br> гарантия</p>
                    </div>
                    <div class="item">
                        <div class="image"><img src="/i/icon_avnt5.svg" height="77px"></div>
                        <p>Курьерская доставка в
                            <br> день заказа</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="window box-shadow" id="poll">
        <div class="header"><span>Оставить отзыв</span><a href="#" data-action="close"><i class="icon ic-close"></i></a></div>
        <div class="body box-side">
            <div class="success">Ваш отзыв успешно добавлен!</div>
            <div class="errors">Невозможно добавить отзыв, попробуйте еще раз</div>
            <form class="form-elements form-poll" data-action="/product/review/" method="post" id="form-poll">
                <input type="hidden" name="sluid" value="176ce29617caf03d4af9d50e63cc93827037c0bb05f96f60d8a0b6cbcb9f36f2" />
                <input type="hidden" name="product_id" value="8990358" />
                <div class="wp-row">
                    <div class="pf-wrapper-name">
                        <label for="name" class="require"><span>Имя</span>
                            <input name="name" type="text">
                        </label>
                    </div>
                    <div class="pf-wrapper-exp">
                        <label for=""><span>Опыт использования</span>
                            <div class="dropdown-select dropdown-border" data-name="opit">
                                <input type="hidden" name="opit" value="1" /><a href="#" data-toggle="dropdown"><span class="current">менее месяца</span><i class="icon ic-arrow-down animate-all "></i></a>
                                <ul class="dropdown-menu extended">
                                    <li><a href="#" data-value="1">менее месяца</a></li>
                                    <li><a href="#" data-value="2">несколько месяцев</a></li>
                                    <li><a href="#" data-value="3">более года</a></li>
                                </ul>
                            </div>
                        </label>
                    </div>
                    <div class="pf-wrapper-rate">
                        <label for="name" class="require"><span>Оценка</span></label>
                        <div class="lk-clear"></div>
                        <div class="poll-rate-stars">
                            <input id="poll-rate-5" value="5" type="radio" name="stars">
                            <label for="poll-rate-5"></label>
                            <input id="poll-rate-4" value="4" type="radio" name="stars">
                            <label for="poll-rate-4"></label>
                            <input id="poll-rate-3" value="3" type="radio" name="stars">
                            <label for="poll-rate-3"></label>
                            <input id="poll-rate-2" value="2" type="radio" name="stars">
                            <label for="poll-rate-2"></label>
                            <input id="poll-rate-1" value="1" type="radio" name="stars">
                            <label for="poll-rate-1"></label>
                            <div class="lk-clear"></div>
                        </div>
                    </div>
                    <div class="lk-clear"></div>
                </div>
                <div class="wp-row">
                    <div class="pf-wrapper-plus">
                        <div class="icon-plus-wrapper"><i class="icon ic-plus-white"></i></div>
                        <label for="plus"><span>Плюсы</span>
                            <input name="review_comment_plus" type="text">
                        </label>
                    </div>
                </div>
                <div class="wp-row">
                    <div class="pf-wrapper-minus">
                        <div class="icon-minus-wrapper"><i class="icon ic-minus-white"></i></div>
                        <label for="minus"><span>Минусы</span>
                            <input name="review_comment_minus" type="text">
                        </label>
                    </div>
                </div>
                <div class="wp-row">
                    <div class="pf-wrapper-comment">
                        <div class="icon-comment-wrapper"><i class="icon ic-comment-white"></i></div>
                        <label for="comment"><span>Комментарий</span>
                            <textarea rows="7" name="review_comment"></textarea>
                        </label>
                    </div>
                    <div class="w-poll-button"><a href="#" class="btn btn-primary btn-submit">отправить</a></div>
                </div>
            </form>
        </div>
    </div>
    <script type="text/javascript">
        (window["rrApiOnReady"] = window["rrApiOnReady"] || []).push(function() {
            retailrocket.products.post({
                "id": 663989,
                "name": "Видеокарта GigaByte GeForce GTX 1050 Ti GV-N105TOC-4GL PCI-E 4096Mb 128 Bit Retail (GV-N105TOC-4GL)",
                "price": 12630,
                "pictureUrl": "https://www.123.ru/xl_pics/8990358_1.jpg",
                "url": "http://www.123.ru/accessories/komplektuyuschie_dlya_pk/videokarti/brand-gigabyte/videokarta_gigabyte_gv_n105toc_4gl_4096_geforce_gtx_1050_ti_gddr5_art8990358/",
                "isAvailable": true,
                "categoryPaths": [
                    "Комплектующие для ПК/Видеокарты"
                ],
                "description": " ",
                "vendor": "GigaByte",
                "model": "GV-N105TOC-4GL",
                "typePrefix": "",
                "oldPrice": 12860,
                "buyUrl": "",
                "params": {
                    "price69": 12630,
                    "price70": 12630,
                    "price71": 12630,
                    "itemValue": 41,
                    "new": 0,
                    "marking": "8990358",
                    "hasPhoto": true,
                    "isPrepay": 0
                }
            });
            rrApi.view(663989);
        });
    </script>
    <footer>
        <div class="container">
            <div class="content-cnt">
                <div class="wrapp-footer">
                    <div class="footer-top">
                        <div class="subm">
                            <p class="title">Интернет-магазин</p>
                            <ul>
                                <li><a href="/about/">О нас</a></li>
                                <li><a href="/why/">Почему мы</a></li>
                                <li><a href="/about/corporate.php/">Оптовым и корпоративным клиентам</a></li>
                                <li><a href="/franchise/">Франчайзинг</a></li>
                                <li><a href="/supplier/">Для поставщиков</a></li>
                                <li><a href="/vacancy/">Вакансии</a></li>
                                <li><a href="/actions/">Акции</a></li>
                                <li><a href="/news/">Новости</a></li>
                            </ul>
                        </div>
                        <div class="subm">
                            <p class="title">Преимущества 123.ру</p>
                            <ul>
                                <li><a href="/credit/">Купить в кредит</a></li>
                                <li><a href="/sovest/">Карта рассрочки Совесть</a></li>
                                <li><a href="/halva/">Карта рассрочки Халва</a></li>
                                <li><a href="/insurance/">Страхование товара</a></li>
                                <li><a href="/pay/">Способы оплаты</a></li>
                            </ul>
                        </div>
                        <div class="subm">
                            <p class="title">Наши услуги</p>
                            <ul>
                                <li><a href="/warranty/">Гарантия</a></li>
                                <li><a href="/about/delivery.php/">Доставка</a></li>
                                <li><a href="/sborka/">Сборка ПК</a></li>
                            </ul>
                        </div>
                        <div>
                            <ul class="social-link">
                                <li>
                                    <a href="https://www.facebook.com/123.ru"><img src="/i/social_facebook_circle.svg" /></a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/123_shop"><img src="/i/social_twitter_circle.svg" /></a>
                                </li>
                                <li>
                                    <a href="https://vk.com/razdvatri_shop"><img src="/i/social_vk_circle.svg" /></a>
                                </li>
                                <li>
                                    <a href="https://www.instagram.com/123.ru_official/"><img src="/i/social_instagram_circle.svg" /></a>
                                </li>
                                <li>
                                    <a href="https://ok.ru/razdvatrishop"><img src="/i/social_ok_circle.svg" /></a>
                                </li>
                                <li>
                                    <a href="https://t.me/official123ru"><img src="/i/social_telegram_circle.svg" /></a>
                                </li>
                            </ul><a class="phone" href="tel:8(495)225 9 123">8 (495) 225 9 123</a><a class="phone" href="tel:8(800)333 9 123">8 (800) 333 9 123</a><a class="call-backform" href="#modal" data-modal="#bottom-popup">Обратная связь</a><a class="email-footer" href="mailto:info@123.ru">info@123.ru</a><a class="partner-q" href="/supplier/">По вопросам сотрудничества</a></div>
                    </div>
                    <div class="footer-bottom">
                        <div>
                            <p class="correct">
                                <div class="hide-me" data-hide-uid="5bc6cfeea08ef5.62205689"></div>
                            </p>
                        </div>
                        <div>
                            <a class="sad-client" href="/bad/">
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="17.995px" height="18px" viewBox="3.005 4 17.995 18" enable-background="new 3.005 4 17.995 18" xml:space="preserve">
                                    <path d="M12,4c4.971,0,9,4.03,9,9c0,4.971-4.029,9-9,9c-4.97,0-9-4.029-9-9C3,8.03,7.03,4,12,4z M12,5.8c-3.977,0-7.2,3.223-7.2,7.2c0,3.977,3.223,7.199,7.2,7.199c3.977,0,7.199-3.223,7.199-7.199C19.199,9.023,15.977,5.8,12,5.8z M12,13.9c-0.497,0-0.9-0.402-0.9-0.9V9.4c0-0.498,0.404-0.9,0.9-0.9c0.498,0,0.9,0.403,0.9,0.9V13C12.9,13.498,12.498,13.9,12,13.9zM12,17.5c-0.497,0-0.9-0.402-0.9-0.9c0-0.497,0.404-0.9,0.9-0.9c0.498,0,0.9,0.403,0.9,0.9C12.9,17.098,12.498,17.5,12,17.5z" />
                                </svg>Пожаловаться</a>
                        </div>
                        <div>
                            <a href="http://iconix.ru/" target="_blank" class="partner"></a><a class="" href="/catalog/">Карта сайта</a><span class="copy">123.ru &copy; 2010-2018</span></div>
                    </div>
                    <div class="footer-result">
                        <p>© 2010-2018. Все права защищены. Компания www.123.ru — ваш гипермаркет электроники.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="catalog _mobile"><a href="javascript:void(0)" class="catalog__btn">Каталог</a>
        <div class="catalog__content"><a href="javascript:void(0)" class="catalog__back">Назад</a>
            <div class="catalog__pages">
                <!-- .catalog__page -->
            </div>
        </div>
    </div>
    <div class="bottom-fixed">
        <div class="footer-bar bg-white">
            <div class="footer-bar-top tabs-list "><a class="footbar-tab js-can-deactivate review" href="#tab-review"><span><i class="icon ic-mess"></i><span class="hidden-sm hidden-xs">Обратная связь</span></span></a>
                <div class="footbar-tab bay hidden-sm hidden-xs"><span><a href="/quick-pay/" class="btn btn-primary" onclick="return redirect('/quick-pay/');">оплата заказа</a></span></div><a class="footbar-tab js-can-deactivate tracked nobefore" href="#tab-tracked"><span><i class="icon ic-search-ht hidden-lg hidden-md"></i><span class="hidden-sm hidden-xs">Отслеживаемые</span> <span class="cl-blue">0</span></span></a><a class="footbar-tab js-can-deactivate look" href="#tab-look"><span><i class="icon ic-clock hidden-lg hidden-md"></i><span class="hidden-sm hidden-xs">Просмотренные</span> <span class="cl-blue">1</span></span></a><a class="footbar-tab js-can-deactivate diagram" href="#tab-diagram"><span><i class="icon ic-compare"></i><span class="cl-blue">0</span></span></a><a class="footbar-tab js-can-deactivate shopcart" href="#tab-shopcart"><span><i class="icon ic-cart"></i><span class="cl-blue">0</span></span></a></div>
            <div class="footer-bar-bottom">
                <div class="tabs-viewport">
                    <div class="tab tab-slider" id="tab-review">
                        <div class="footer-feedback">
                            <div class="form-elements">
                                <div class="col-lg-4 form-bad hide-in-js">
                                    <!--<div class="success">Ваш жалоба зарегистрирована!</div><div class="errors">Невозможно зарегистрировать жалобу, попробуйте еще раз</div><form class="form-elements" data-action="/bad/create/" method="post" id="form-abuse-footer2"><input type="hidden" name="sluid" value="176ce29617caf03d4af9d50e63cc93827037c0bb05f96f60d8a0b6cbcb9f36f2" /><div class="h3">Зарегистрировать жалобу</div><p>Жалуйтесь без колебаний. Разберемся, виновного накажем.</p><label for="email" class="require"><span>Ваш E-Mail для ответа на жалобу:</span><input name="email" type="text" value=""></label><label for="phone" class="require"><span>Телефон для связи</span><input name="phone" type="tel" class="masked" value="" mask="+7 (999) 999-99-99" mask-placeholder="_" placeholder="+7 (___) ___-__-__" /></label><label for="comment" class="require"><span>Жалоба</span><textarea rows="5" name="comment"></textarea></label><a href="#" class="btn btn-primary btn-submit">Отправить</a><div class="require-text">Поля, обязательные для заполнения</div></form>--></div>
                                <div class="col-lg-4 form-phone hide-in-js">
                                    <!--<div class="success">Ваш обратный звонок зарегистрирован!</div><div class="errors">Невозможно заказать звонок, попробуйте еще раз</div><form class="form-elements" id="form-callback-footer2" data-action="/callback/" method="post"><input type="hidden" name="sluid" value="176ce29617caf03d4af9d50e63cc93827037c0bb05f96f60d8a0b6cbcb9f36f2" /><div class="h3">Обратный звонок</div><p>Укажите свой контактный телефон, и мы перезвоним вам в ближайшие несколько минут:</p><label for="phone" class="require"><span>Телефон для связи</span><input type="tel" name="phone" class="masked" mask="+7 (999) 999-99-99" mask-placeholder="_" placeholder="+7 (___) ___-__-__" value="" /></label><a href="#" class="btn btn-primary btn-submit">Отправить</a><div class="require-text">Поля, обязательные для заполнения</div></form>--></div>
                                <div class="col-lg-4 form-social">
                                    <div class="h3">Мы в социальных сетях</div><a href="https://ru-ru.facebook.com/123.ru" class="btn fs-fb"><i></i>123.RU в Facebook</a>
                                    <br><a href="https://vk.com/club21883822" class="btn fs-vk"><i></i>123.RU в Вконтакте</a>
                                    <br><a href="https://twitter.com/123_shop" class="btn fs-tw"><i></i>123.RU в Twitter</a>
                                    <br><a href="http://www.youtube.com/user/www123ru" class="btn fs-yt"><i></i>123.RU на YouTube</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="tab tab-slider inprogress" id="tab-tracked" data-source="/footer-panel/tracking/"></div>
                    <div class="tab tab-slider inprogress" id="tab-look" data-source="/footer-panel/viewed/"></div>
                    <div class="tab tab-slider inprogress" id="tab-diagram" data-source="/footer-panel/compare/"></div>
                    <div class="tab tab-slider inprogress" id="tab-shopcart" data-source="/footer-panel/basket/"></div>
                </div>
                <div class="hidden-lg hidden-md footbar-mob-btn"><a href="/quick-pay/" class="btn btn-primary">оплата заказа</a></div>
                <button type="button" class="fb-arrows fba-prev slick-prev">Prev</button>
                <button type="button" class="fb-arrows fba-next slick-next">Next</button>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="window box-shadow" id="login">
        <div class="hide-me" data-hide-uid="5bc6cfeea0d320.85461495"></div>
    </div>
    <div class="window box-shadow cities-new" id="cities">
        <div class="header"><span>Укажите ваш город</span><a href="#" data-action="close"><i class="icon ic-close"></i></a></div>
        <form class="bottom bg-darkblue2 form-elements" action="#" method="POST">
            <div class="fake-placeholder"><span>Доставка по всей России<span class="hidden-xs">, например в город <b>Москва</b></span></span>
                <input type="text" name="search-city" /><a href="javascript:void(0)" class="btn btn-primary">Сохранить</a></div>
        </form>
        <div class="body city-list">
            <div class="row">
                <div class="col-lg-12 relative cities-big noselect">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="small-text">Крупные города:</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-lg-12">
                            <ul class="clearstyle">
                                <li><a href="#" data-type="г" data-id="15238" data-code="7700000000000" data-city-name="Москва" data-return-url="https://www.123.ru/accessories/komplektuyuschie_dlya_pk/videokarti/brand-gigabyte/videokarta_gigabyte_gv_n105toc_4gl_4096_geforce_gtx_1050_ti_gddr5_art8990358/">Москва</a></li>
                                <li><a href="https://spb.123.ru/accessories/komplektuyuschie_dlya_pk/videokarti/brand-gigabyte/videokarta_gigabyte_gv_n105toc_4gl_4096_geforce_gtx_1050_ti_gddr5_art8990358/" data-type="г" data-id="4364" data-code="7800000000000" data-city-name="Санкт-Петербург">Санкт-Петербург</a></li>
                                <li><a href="#" data-type="г" data-id="2480" data-code="2200000100000" data-city-name="Барнаул" data-return-url="https://www.123.ru/accessories/komplektuyuschie_dlya_pk/videokarti/brand-gigabyte/videokarta_gigabyte_gv_n105toc_4gl_4096_geforce_gtx_1050_ti_gddr5_art8990358/">Барнаул</a></li>
                                <li><a href="https://vgg.123.ru/accessories/komplektuyuschie_dlya_pk/videokarti/brand-gigabyte/videokarta_gigabyte_gv_n105toc_4gl_4096_geforce_gtx_1050_ti_gddr5_art8990358/" data-type="г" data-id="6153" data-code="3400000100000" data-city-name="Волгоград">Волгоград</a></li>
                                <li><a href="https://vrn.123.ru/accessories/komplektuyuschie_dlya_pk/videokarti/brand-gigabyte/videokarta_gigabyte_gv_n105toc_4gl_4096_geforce_gtx_1050_ti_gddr5_art8990358/" data-type="г" data-id="6490" data-code="3600000100000" data-city-name="Воронеж">Воронеж</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-lg-12">
                            <ul class="clearstyle">
                                <li><a href="https://ekb.123.ru/accessories/komplektuyuschie_dlya_pk/videokarti/brand-gigabyte/videokarta_gigabyte_gv_n105toc_4gl_4096_geforce_gtx_1050_ti_gddr5_art8990358/" data-type="г" data-id="6104" data-code="6600000100000" data-city-name="Екатеринбург">Екатеринбург</a></li>
                                <li><a href="https://kzn.123.ru/accessories/komplektuyuschie_dlya_pk/videokarti/brand-gigabyte/videokarta_gigabyte_gv_n105toc_4gl_4096_geforce_gtx_1050_ti_gddr5_art8990358/" data-type="г" data-id="14055" data-code="1600000100000" data-city-name="Казань">Казань</a></li>
                                <li><a href="#" data-type="г" data-id="18474" data-code="2300000100000" data-city-name="Краснодар" data-return-url="https://www.123.ru/accessories/komplektuyuschie_dlya_pk/videokarti/brand-gigabyte/videokarta_gigabyte_gv_n105toc_4gl_4096_geforce_gtx_1050_ti_gddr5_art8990358/">Краснодар</a></li>
                                <li><a href="https://kry.123.ru/accessories/komplektuyuschie_dlya_pk/videokarti/brand-gigabyte/videokarta_gigabyte_gv_n105toc_4gl_4096_geforce_gtx_1050_ti_gddr5_art8990358/" data-type="г" data-id="284" data-code="2400000100000" data-city-name="Красноярск">Красноярск</a></li>
                                <li><a href="https://nnov.123.ru/accessories/komplektuyuschie_dlya_pk/videokarti/brand-gigabyte/videokarta_gigabyte_gv_n105toc_4gl_4096_geforce_gtx_1050_ti_gddr5_art8990358/" data-type="г" data-id="4187" data-code="5200000100000" data-city-name="Нижний Новгород">Нижний Новгород</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-lg-12">
                            <ul class="clearstyle">
                                <li><a href="https://nsk.123.ru/accessories/komplektuyuschie_dlya_pk/videokarti/brand-gigabyte/videokarta_gigabyte_gv_n105toc_4gl_4096_geforce_gtx_1050_ti_gddr5_art8990358/" data-type="г" data-id="16465" data-code="5400000100000" data-city-name="Новосибирск">Новосибирск</a></li>
                                <li><a href="https://omsk.123.ru/accessories/komplektuyuschie_dlya_pk/videokarti/brand-gigabyte/videokarta_gigabyte_gv_n105toc_4gl_4096_geforce_gtx_1050_ti_gddr5_art8990358/" data-type="г" data-id="10024" data-code="5500000100000" data-city-name="Омск">Омск</a></li>
                                <li><a href="https://prm.123.ru/accessories/komplektuyuschie_dlya_pk/videokarti/brand-gigabyte/videokarta_gigabyte_gv_n105toc_4gl_4096_geforce_gtx_1050_ti_gddr5_art8990358/" data-type="г" data-id="6444" data-code="5900000100000" data-city-name="Пермь">Пермь</a></li>
                                <li><a href="https://rnd.123.ru/accessories/komplektuyuschie_dlya_pk/videokarti/brand-gigabyte/videokarta_gigabyte_gv_n105toc_4gl_4096_geforce_gtx_1050_ti_gddr5_art8990358/" data-type="г" data-id="6188" data-code="6100000100000" data-city-name="Ростов-на-Дону">Ростов-на-Дону</a></li>
                                <li><a href="https://sam.123.ru/accessories/komplektuyuschie_dlya_pk/videokarti/brand-gigabyte/videokarta_gigabyte_gv_n105toc_4gl_4096_geforce_gtx_1050_ti_gddr5_art8990358/" data-type="г" data-id="20348" data-code="6300000100000" data-city-name="Самара">Самара</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-lg-12">
                            <ul class="clearstyle">
                                <li><a href="https://yfa.123.ru/accessories/komplektuyuschie_dlya_pk/videokarti/brand-gigabyte/videokarta_gigabyte_gv_n105toc_4gl_4096_geforce_gtx_1050_ti_gddr5_art8990358/" data-type="г" data-id="10554" data-code="0200000100000" data-city-name="Уфа">Уфа</a></li>
                                <li><a href="https://chl.123.ru/accessories/komplektuyuschie_dlya_pk/videokarti/brand-gigabyte/videokarta_gigabyte_gv_n105toc_4gl_4096_geforce_gtx_1050_ti_gddr5_art8990358/" data-type="г" data-id="5530" data-code="7400000100000" data-city-name="Челябинск">Челябинск</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix" style="height: 25px"></div>
        </div>
    </div>
    <div class="window box-shadow" id="cart">
        <div class="header"><span>Корзина</span>&nbsp;<span class="c-gray">0 товаров</span><a href="#" data-action="close" class="visible-xs"><i class="icon ic-close"></i></a></div>
        <div class="body">
            <ul class="window-cart-items clearstyle"></ul>
            <div class="total"><span>Всего товаров на сумму:</span>
                <div class="price c-blue"><span>0</span> <span class="rub">a</span></div>
            </div>
        </div>
        <div class="bottom"><a href="https://www.123.ru/ordering/" class="btn btn-fw btn-primary btn-lg">оформить заказ</a></div>
    </div>
    <div class="window box-shadow" id="callback">
        <div class="hide-me" data-hide-uid="5bc6cfeea17f17.97141606"></div>
    </div>
    <div class="window box-shadow" id="report">
        <div class="hide-me" data-hide-uid="5bc6cfeea186f3.92085244"></div>
    </div>
    <div class="window box-shadow" id="empty-compare">
        <div class="hide-me" data-hide-uid="5bc6cfeea18c23.72280468"></div>
    </div>
    <div id="bottom-popup" class="modal__wr center-p">
        <div class="container">
            <div class="modal__content fade__wr wrapp-bottom">
                <div class="wrapp-popup">
                    <div class="close-popup"><span></span><span></span></div>
                    <div class="body-popup">
                        <div class="callback-side">
                            <div class="form-phone">
                                <p class="title">Заказать обратный звонок</p>
                                <p class="do-this-slogan">Укажите ваш контактный телефон и мы перезвоним вам в течение нескольких минут!</p>
                                <div class="success">Ваш обратный звонок зарегистрирован!</div>
                                <div class="errors">Невозможно заказать звонок, попробуйте еще раз</div>
                                <form id="form-callback-footer" data-action="/callback/" method="post">
                                    <input type="hidden" name="sluid" value="176ce29617caf03d4af9d50e63cc93827037c0bb05f96f60d8a0b6cbcb9f36f2" />
                                    <div class="line">
                                        <div class="inpt-wr">
                                            <label for="callbackName-footer">Ваше имя *</label>
                                            <input class="input-base" type="text" name="callbackName-footer" placeholder="Введите ваше имя">
                                        </div>
                                        <div class="inpt-wr">
                                            <label for="callbackPhone-footer">Номер телефона *</label>
                                            <input class="input-base masked" type="tel" id="callbackPhone-footer" name="phone" placeholder="+7 (___) ___-__-__" mask="+7 (999) 999-99-99" mask-placeholder="_" placeholder="+7 (___) ___-__-__" value="" />
                                        </div>
                                    </div>
                                    <button class="btn btn-primary btn-submit" type="submit">Заказать звонок</button>
                                </form>
                            </div>
                            <div class="req-text"><span>* – поля обязательные для заполнения</span></div>
                        </div>
                        <div class="angry-side form-bad">
                            <p class="title">Зарегистрировать жалобу</p>
                            <p class="do-this-slogan">Жалуйтесь без колебаний! Разберемся быстро, виновного накажем.</p>
                            <div class="success">Ваш жалоба зарегистрирована!</div>
                            <div class="errors">Невозможно зарегистрировать жалобу, попробуйте еще раз</div>
                            <form data-action="/bad/create/" method="post" id="form-abuse-footer">
                                <input type="hidden" name="sluid" value="176ce29617caf03d4af9d50e63cc93827037c0bb05f96f60d8a0b6cbcb9f36f2" />
                                <div class="line">
                                    <div class="inpt-wr">
                                        <label for="sadEmail-footer">Ваше email для ответа на жалобу *</label>
                                        <input class="input-base" type="email" name="email" id="sadEmail-footer" placeholder="Введите ваш email" value="" />
                                    </div>
                                    <div class="inpt-wr">
                                        <label for="sadPhone-footer">Номер телефона *</label>
                                        <input class="input-base masked" type="tel" id="sadPhone-footer" name="phone" value="" mask="+7 (999) 999-99-99" mask-placeholder="_" placeholder="+7 (___) ___-__-__" />
                                    </div>
                                </div>
                                <div class="line">
                                    <div class="inpt-wr-full">
                                        <label for="sadText-footer">Жалоба *</label>
                                        <textarea name="comment" id="sadText-footer" placeholder="Введите вашу жалобу"></textarea>
                                    </div>
                                </div>
                                <button class="btn btn-primary btn-submit" type="submit">Отправить</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="ga-ecommerce-data" data-page-type="product" data-client-id="1b878e80-317e-c43d-147a-4167cc9d0dd8" data-auth="0" data-user-id="0" data-city="Москва" data-availability="1" data-product-id="8990358" data-product-name="Видеокарта GigaByte GeForce GTX 1050 Ti GV-N105TOC-4GL PCI-E 4096Mb 128 Bit Retail (GV-N105TOC-4GL)" data-product-category="Видеокарты" data-product-price="12630"></div>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function(d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    window.dataLayerYa = window.dataLayerYa || [];
                    dataLayerYa.push({
                        "ecommerce": {
                            "currencyCode": "RUB",
                            "detail": {
                                "products": [{
                                    "id": 8990358,
                                    "name": "Видеокарта GigaByte GeForce GTX 1050 Ti GV-N105TOC-4GL PCI-E 4096Mb 128 Bit Retail (GV-N105TOC-4GL)",
                                    "price": 12630,
                                    "brand": "GigaByte",
                                    "category": "Видеокарты"
                                }]
                            }
                        }
                    });
                    w.yaCounter2207821 = new Ya.Metrika({
                        "id": 2207821,
                        "webvisor": "true",
                        "clickmap": "true",
                        "trackLinks": "true",
                        "accurateTrackBounce": "true",
                        "ecommerce": "dataLayerYa"
                    });
                } catch (e) {}
            });
            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function() {
                    n.parentNode.insertBefore(s, n);
                };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";
            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript>
        <div><img src="//mc.yandex.ru/watch/2207821" style="position:absolute; left:-9999px;" alt="" /></div>
    </noscript>
    <script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true"></script>
    <script type="text/javascript">
        window.criteo_q = window.criteo_q || [];
        window.criteo_q.push({
            "event": "setAccount",
            "account": 16192
        }, {
            "event": "setSiteType",
            "type": "d"
        }, {
            "event": "viewItem",
            "item": 8990358
        });
    </script>
    <div id="criteo-data" data-product-prefix="msk"></div>
    <script type="text/javascript">
        dataLayer = dataLayer || [];
        dataLayer.push({
            "ecomm_totalvalue": 12630,
            "ecomm_prodid": 8990358
        });
    </script>
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KBL5ZV" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
    <script type="text/javascript" src="//api.flocktory.com/v2/loader.js?site_id=66" async="async"></script>
    <div class="i-flocktory" data-fl-action="track-item-view" data-fl-item-id="8990358"></div>
    <script type="text/javascript" id="advcakeAsync">
        window.advcake_push_data = window.advcake_push_data || function(advcake_data_array) {};
        window.advcake_data = {
            "pageType": 2,
            "currentProduct": {
                "id": 8990358,
                "name": "Видеокарта GigaByte GeForce GTX 1050 Ti GV-N105TOC-4GL PCI-E 4096Mb 128 Bit Retail (GV-N105TOC-4GL)",
                "price": 12630
            },
            "currentCategory": {
                "id": 12482,
                "name": "Видеокарты"
            }
        };
        window.advcake_push_data(window.advcake_data);
    </script>
    <script type="text/javascript">
        (function() {
            var ra = document.createElement('script');
            ra.type = 'text/javascript';
            ra.async = true;
            ra.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'bn.adblender.ru/c/123ru/all.js?' + Math.floor((new Date()).valueOf() / 1000 / 3600);
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ra, s);
        })();
    </script>
    <!--LiveInternet counter-->
    <script type="text/javascript">
        <!--
        document.write("<div style='display:none;'><a rel='nofollow' href='http://www.liveinternet.ru/click' " +
                "target=_blank><img src='//counter.yadro.ru/hit?t44.6;r" +
                escape(document.referrer) + ((typeof(screen) == "undefined") ? "" :
                    ";s" + screen.width + "*" + screen.height + "*" + (screen.colorDepth ?
                        screen.colorDepth : screen.pixelDepth)) + ";u" + escape(document.URL) +
                ";" + Math.random() +
                "' alt='' title='LiveInternet' " +
                "border='0' width='31' height='31'><\/a></div>")
            //-->
    </script>
    <!--/LiveInternet-->
    <div class="window box-shadow" id="pay-menu">
        <div class="body box-side">
            <ul class="clearstyle">
                <li><a href="/pay/">Всё об оплате</a></li>
                <li><a href="/credit/">Покупка в кредит</a></li>
                <li><a href="/sovest/" class="">Рассрочка с картой Совесть</a></li>
                <li><a href="/halva/">Рассрочка от Халвы</a></li>
            </ul>
        </div>
    </div>
    <div class="-upd window box-shadow noscroll" id="basket-preview">
        <div class="header"><span>Товар добавлен в корзину</span>
            <a href="#" data-action="close">
                <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 12 12">
                    <svg xmlns:xlink="http://www.w3.org/1999/xlink" height="12" width="12" version="1.1" viewBox="0 0 12 12" id="close" y="0">
                        <title>Close</title>
                        <desc></desc>
                        <g id="close-category_adaptive" fill-rule="evenodd" fill="none">
                            <g id="close-05_Category-ot-320" fill="#9B9B9B" transform="translate(-92 -376)">
                                <g id="" transform="translate(0 360)">
                                    <g id="" transform="translate(92 13)">
                                        <path id="close-Fill-20" d="m5.9998 10.692l-4.0537 4.054c-0.3876 0.388-1.0122 0.391-1.4097-0.006l-0.27654-0.277c-0.39099-0.391-0.39594-1.02-0.00627-1.409l4.0537-4.0542-4.0537-4.0537c-0.38754-0.3876-0.39127-1.0122 0.00626-1.4097l0.27656-0.2765c0.39099-0.391 1.02-0.396 1.4097-0.0063l4.0537 4.0537 4.0542-4.0537c0.387-0.3875 1.012-0.3913 1.409 0.0063l0.277 0.2765c0.391 0.391 0.396 1.02 0.006 1.4097l-4.0533 4.0537 4.0533 4.0542c0.388 0.387 0.392 1.012-0.006 1.409l-0.277 0.277c-0.391 0.391-1.02 0.396-1.409 0.006l-4.0542-4.054z"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                </svg>
            </a>
        </div>
        <div class="body">
            <div class="items">
                <ul class="window-cart-items clearstyle"></ul>
                <div class="buttons"><a href="https://www.123.ru/ordering/" class="btn-yellow">Перейти в корзину</a><a href="#" data-action="close" class="btn-primary">Продолжить покупки</a>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="recommendations loading" data-reload="1"></div>
        </div>
    </div>
    <div class="-upd window box-shadow noscroll" id="basket-preview-credit" >
        <div class="header"><span>Купить в кредит</span>
            <a href="#" data-action="close">
                <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 12 12">
                    <svg xmlns:xlink="http://www.w3.org/1999/xlink" height="12" width="12" version="1.1" viewBox="0 0 12 12" id="close" y="0">
                        <title>Close</title>
                        <desc></desc>
                        <g id="close-category_adaptive" fill-rule="evenodd" fill="none">
                            <g id="close-05_Category-ot-320" fill="#9B9B9B" transform="translate(-92 -376)">
                                <g id="" transform="translate(0 360)">
                                    <g id="" transform="translate(92 13)">
                                        <path id="close-Fill-20" d="m5.9998 10.692l-4.0537 4.054c-0.3876 0.388-1.0122 0.391-1.4097-0.006l-0.27654-0.277c-0.39099-0.391-0.39594-1.02-0.00627-1.409l4.0537-4.0542-4.0537-4.0537c-0.38754-0.3876-0.39127-1.0122 0.00626-1.4097l0.27656-0.2765c0.39099-0.391 1.02-0.396 1.4097-0.0063l4.0537 4.0537 4.0542-4.0537c0.387-0.3875 1.012-0.3913 1.409 0.0063l0.277 0.2765c0.391 0.391 0.396 1.02 0.006 1.4097l-4.0533 4.0537 4.0533 4.0542c0.388 0.387 0.392 1.012-0.006 1.409l-0.277 0.277c-0.391 0.391-1.02 0.396-1.409 0.006l-4.0542-4.054z"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                </svg>
            </a>
        </div>
        <div class="body">
            <div class="items">
                <ul class="window-cart-items clearstyle">
                    <li><a href="/item.php" class='-img'><img src='http://www.123.ru/xl_pics/8990358_1.jpg'></a>
                        <div class="info">
                            <div class="title"><a href="/item/">Видеокарта GigaByte GeForce GTX 1050 Ti GV-N105TWF2OC-4GD PCI-E 4096Mb</a></div>
                            <div class="-art-rating">
                                <div class="vendor-code">Арт: 8602063</div>
                                <div class="sp-listing-inline-rating-widget">
                                    <div class="sp-inline-rating-stars"><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-on"></div><div class="sp-star-off"></div></div>
                                    <div class="rating-count">15</div>
                                </div>
                            </div>
                            <div class="price c-blue">
                                <span>12 630</span>
                                <em>12 860</em>
                                <p class="-discount">Скидка -6% <b>(2 120 р.)</b></p>
                            </div>
                            <div class="countbox" data-product-id="80032863">
                                <a href="#" class="minus"></a>
                                <input type="text" name="count" value="1" data-min="1" data-max="1" data-hold="true" data-price="12630">
                                <a href="#" class="plus"></a>
                            </div>
                        </div>
                    </li>
                </ul>
                <div class="credit-description">
                    <h5>Покупка в кредит очень простая!</h5>
                    <ul>
                        <li>Наполните корзину товарами</li>
                        <li>Выберите способ оплаты “В кредит”</li>
                        <li>Заполните<br>онлайн-заявку</li>
                        <li>Подпишите кредитный договор</li>
                    </ul>
                    <p><a href="#usl" target="_blank">Подробнее об условиях кредитования читайте здесь</a></p>
                    <p>Также вы можете использовать <a class="xs-line" href="#">иные способы оплаты</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="window box-shadow" id="userinfo">
        <div class="header"><span>Информация о посетителе</span><a href="#" data-action="close"><i class="icon ic-close"></i></a></div>
        <div class="body box-side box-top">
            <div class="form-elements">
                <textarea name="message" readonly>User agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.67 Safari/537.36 URL страницы: https://www.123.ru/accessories/komplektuyuschie_dlya_pk/videokarti/brand-gigabyte/videokarta_gigabyte_gv_n105toc_4gl_4096_geforce_gtx_1050_ti_gddr5_art8990358/ Город: Москва</textarea>
                <div class="tcenter" style="padding: 15px 0 25px 0;"><a href="#" class="btn btn-fw btn-primary btn-lg btn-submit">Копировать</a></div>
            </div>
        </div>
    </div>
    <script type="application/ld+json">
        { "@context": "http://schema.org", "@type": "Organization", "url": "https://www.123.ru", "logo": "https://www.123.ru/img/logo.png" }
    </script>
    <script type="application/ld+json">
        { "@context" : "http://schema.org", "@type" : "Organization", "name" : "www.123.ru", "url" : "https://www.123.ru", "sameAs" : [ "https://www.instagram.com/123.ru_official/", "https://twitter.com/123_shop", "https://vk.com/razdvatri_shop", "https://www.facebook.com/123.ru", "https://ok.ru/razdvatrishop", "https://t.me/official123ru" ] }
    </script>
    <script>
        (function() {
            window._shoppilot = window._shoppilot || [];
            _shoppilot.push(["_addStyles", "widgets"]);
            _shoppilot.push(["_setProductId", "8990358"]);
            _shoppilot.push(["_addProductWidget", "product-reviews", "#sp-product-reviews-container"]);
            _shoppilot.push(["_addProductWidget", "inline-rating", "#sp-inline-rating-container"]);
            _shoppilot.push(["_addProductWidget", "product-questions", "#sp-product-questions-container"]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80035426", {
                "product_id": "80035426"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-8968466", {
                "product_id": "8968466"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-8965145", {
                "product_id": "8965145"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-80026075", {
                "product_id": "80026075"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-8962902", {
                "product_id": "8962902"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-8965721", {
                "product_id": "8965721"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-8083132", {
                "product_id": "8083132"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-8653006", {
                "product_id": "8653006"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-8664177", {
                "product_id": "8664177"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-8697352", {
                "product_id": "8697352"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-8842312", {
                "product_id": "8842312"
            }]);
            _shoppilot.push(["_addProductWidget", "listing-inline-rating", ".sp-inline-rating-container-8842341", {
                "product_id": "8842341"
            }]);
            window._shoppilotParams = {};
            window._shoppilotParams.store_id = '5aae2c2d339215000a5f8261';
            window._shoppilotParams.theme_id = 'default';
        })();
    </script>
    <script type="text/javascript">
        var seoContent = {
            "5bc6cfee9bc5c2.27672861": "PGkgY2xhc3M9ImljLXBjLXF1ZXN0aW9uIHZpc2libGUtbGciPjwvaT48ZGl2IGNsYXNzPSJwYy1oaW50LXRleHQiIHN0eWxlPSJkaXNwbGF5OiBub25lOyI+0KXQvtGH0LXRiNGMINC/0L7QutGD0L/QsNGC0Ywg0LTQtdGI0LXQstC70LU/INCS0YHRgtGD0L/QsNC5INCyINC60LvRg9CxIDEyMy7RgNGDINC4INC/0YDQuNC+0LHRgNC10YLQsNC5INGC0L7QstCw0YDRiyDQv9C+INGB0LDQvNGL0Lwg0LLQutGD0YHQvdGL0Lwg0YbQtdC90LDQvC4g0JrQu9GD0LHQvdCw0Y8g0YbQtdC90LAg0LTQvtGB0YLRg9C/0L3QsCDQv9C+0YHQu9C1INC/0L7QutGD0L/QutC4INC+0YIgMTAgMDAwINGALjwvZGl2Pg==",
            "5bc6cfee9d48f6.54107617": "PGRpdiBjbGFzcz0iaGVhZGVyLXdyYXBwZXIiPjxkaXYgY2xhc3M9ImhlYWRlciI+PHNwYW4gY2xhc3M9IndzZC1oZWFkZXIiPjxpIGNsYXNzPSJpYy1kLWNsdWIiPjwvaT7QmtC70YPQsSAxMjM8L3NwYW4+PGEgaHJlZj0iIyIgZGF0YS1hY3Rpb249ImNsb3NlIj48c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjE1IiBoZWlnaHQ9IjE1IiB2aWV3Qm94PSIwIDAgMTIgMTIiPjxzdmcgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIGhlaWdodD0iMTIiIHdpZHRoPSIxMiIgdmVyc2lvbj0iMS4xIiB2aWV3Qm94PSIwIDAgMTIgMTIiIGlkPSJjbG9zZSIgeT0iMCI+PHRpdGxlPkNsb3NlPC90aXRsZT48ZGVzYz48L2Rlc2M+PGcgaWQ9ImNsb3NlLWNhdGVnb3J5X2FkYXB0aXZlIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIGZpbGw9Im5vbmUiPjxnIGlkPSJjbG9zZS0wNV9DYXRlZ29yeS1vdC0zMjAiIGZpbGw9IiM5QjlCOUIiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC05MiAtMzc2KSI+PGcgaWQ9IiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCAzNjApIj48ZyBpZD0iIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSg5MiAxMykiPjxwYXRoIGlkPSJjbG9zZS1GaWxsLTIwIiBkPSJtNS45OTk4IDEwLjY5MmwtNC4wNTM3IDQuMDU0Yy0wLjM4NzYgMC4zODgtMS4wMTIyIDAuMzkxLTEuNDA5Ny0wLjAwNmwtMC4yNzY1NC0wLjI3N2MtMC4zOTA5OS0wLjM5MS0wLjM5NTk0LTEuMDItMC4wMDYyNy0xLjQwOWw0LjA1MzctNC4wNTQyLTQuMDUzNy00LjA1MzdjLTAuMzg3NTQtMC4zODc2LTAuMzkxMjctMS4wMTIyIDAuMDA2MjYtMS40MDk3bDAuMjc2NTYtMC4yNzY1YzAuMzkwOTktMC4zOTEgMS4wMi0wLjM5NiAxLjQwOTctMC4wMDYzbDQuMDUzNyA0LjA1MzcgNC4wNTQyLTQuMDUzN2MwLjM4Ny0wLjM4NzUgMS4wMTItMC4zOTEzIDEuNDA5IDAuMDA2M2wwLjI3NyAwLjI3NjVjMC4zOTEgMC4zOTEgMC4zOTYgMS4wMiAwLjAwNiAxLjQwOTdsLTQuMDUzMyA0LjA1MzcgNC4wNTMzIDQuMDU0MmMwLjM4OCAwLjM4NyAwLjM5MiAxLjAxMi0wLjAwNiAxLjQwOWwtMC4yNzcgMC4yNzdjLTAuMzkxIDAuMzkxLTEuMDIgMC4zOTYtMS40MDkgMC4wMDZsLTQuMDU0Mi00LjA1NHoiPjwvcGF0aD48L2c+PC9nPjwvZz48L2c+PC9zdmc+PC9zdmc+PC9hPjwvZGl2PjwvZGl2PjxkaXYgY2xhc3M9ImJvZHkiPjxwPjxiPtCl0L7RgtC40YLQtSDQv9C+0LrRg9C/0LDRgtGMINCy0YvQs9C+0LTQvdC10LU/PC9iPjwvcD48cD7Qn9C+0LvRg9GH0LjRgtGMINC60LvRg9Cx0L3Rg9GOINGG0LXQvdGDINC+0YfQtdC90Ywg0L/RgNC+0YHRgtC+LiDQnNGLINC/0YDQtdC00LvQsNCz0LDQtdC8INC10ZEg0LTQu9GPINCy0YHQtdGFLCDQutGC0L4g0YPQttC1INGB0L7QstC10YDRiNC40Lsg0L/QvtC60YPQv9C60YMg0L3QsCAxMCAwMDAg0YDRg9Cx0LvQtdC5LiDQntC00L3QuNC8INC40LvQuCDQvdC10YHQutC+0LvRjNC60LjQvNC4INC30LDQutCw0LfQsNC80LguINCh0L7QstGB0LXQvCDRgdC60L7RgNC+INGN0YLQsCDRhtC10L3QsCDRgtC+0LbQtSDQsdGD0LTQtdGCINCy0LDRiNC10LkuPC9wPjxwPjxhIGhyZWY9Ii9hY3Rpb25zL3NhbGVfZm9yX3RoZWlyX293bi8iIHRhcmdldD0iX2JsYW5rIj7Qv9C+0LTRgNC+0LHQvdC10LU8L2E+PC9wPjwvZGl2Pg==",
            "5bc6cfeea08ef5.62205689": "0JLRiyDQvNC+0LbQtdGC0LUg0YHQvtC+0LHRidC40YLRjCDQviDQvdC10YLQvtGH0L3QvtGB0YLQuCDQsiDQvtC/0LjRgdCw0L3QuNC4IOKAlCDQstGL0LTQtdC70LjRgtC1INC10ZEg0Lgg0L3QsNC20LzQuNGC0LUgPHNwYW4+U0hJRlQrRU5URVI8L3NwYW4+",
            "5bc6cfeea0d320.85461495": "PGRpdiBpZD0ic3BvaWxlci1sb2dpbiIgY2xhc3M9InRvb2dsZS1zcG9pbGVyIj48ZGl2IGNsYXNzPSJoZWFkZXIiPjxzcGFuPtCS0YXQvtC0PC9zcGFuPjxhIGhyZWY9IiMiIGRhdGEtYWN0aW9uPSJjbG9zZSI+PGkgY2xhc3M9Imljb24gaWMtY2xvc2UiPjwvaT48L2E+PC9kaXY+PGRpdiBjbGFzcz0iYm9keSBib3gtc2lkZSBib3gtdG9wIj48ZGl2IGNsYXNzPSJlcnJvcnMiPtCd0LXQstC+0LfQvNC+0LbQvdC+INCw0LLRgtC+0YDQuNC30L7QstCw0YLRjNGB0Y8sINC/0L7Qv9GA0L7QsdGD0LnRgtC1INC10YnQtSDRgNCw0Lc8L2Rpdj48Zm9ybSBjbGFzcz0iZm9ybS1lbGVtZW50cyBmb3JtLWxvZ2luIiBpZD0iZm9ybS1sb2dpbiIgZGF0YS1hY3Rpb249Ii9sb2dpbi9hdXRoLyIgbWV0aG9kPSJwb3N0Ij48aW5wdXQgdHlwZT0iaGlkZGVuIiBuYW1lPSJzbHVpZCIgdmFsdWU9IjE3NmNlMjk2MTdjYWYwM2Q0YWY5ZDUwZTYzY2M5MzgyNzAzN2MwYmIwNWY5NmY2MGQ4YTBiNmNiY2I5ZjM2ZjIiIC8+PGxhYmVsIGZvcj0ibG9naW4iPjxzcGFuPtCa0L7QtCDQutC70LjQtdC90YLQsCAvINCi0LXQu9C10YTQvtC9IC8gRS1tYWlsPC9zcGFuPjxpbnB1dCB0eXBlPSJ0ZXh0IiBuYW1lPSJsb2dpbiIgLz48L2xhYmVsPjxsYWJlbCBmb3I9InBhc3N3b3JkIj48c3Bhbj7Qn9Cw0YDQvtC70Yw8L3NwYW4+PGlucHV0IHR5cGU9InBhc3N3b3JkIiBuYW1lPSJwYXNzd29yZCIgLz48L2xhYmVsPjxkaXYgY2xhc3M9InJlY292ZXJ5Ij48ZGl2IGNsYXNzPSJyZWNvdmVyeS1zdWNjZXNzIj7Qn9Cw0YDQvtC70Ywg0LLQvtGB0YHRgtCw0L3QvtCy0LvQtdC9PC9kaXY+PGRpdiBjbGFzcz0icmVjb3ZlcnktZXJyb3JzIj7QndC10LLQvtC30LzQvtC20L3QviDQstC+0YHRgdGC0LDQvdC+0LLQuNGC0Ywg0L/QsNGA0L7Qu9GMLCDQv9C+0L/RgNC+0LHRg9C50YLQtSDQtdGJ0LUg0YDQsNC3PC9kaXY+PGEgaHJlZj0iIyIgY2xhc3M9InBzZXVkbyBjLWJsdWUgbGluayI+0J/QvtC70YPRh9C40YLRjCDQv9Cw0YDQvtC70Ywg0L/QviBTTVM8L2E+PC9kaXY+PGRpdiBjbGFzcz0idGNlbnRlciI+PGEgaHJlZj0iIyIgY2xhc3M9ImJ0biBidG4tZncgYnRuLXByaW1hcnkgYnRuLWxnIGJ0bi1zdWJtaXQiPtCy0L7QudGC0Lg8L2E+PGlucHV0IHR5cGU9InN1Ym1pdCIgc3R5bGU9ImRpc3BsYXk6IG5vbmU7IiAvPjxhIGhyZWY9IiMiIGNsYXNzPSJiYXNlIHJlZ2lzdGVyIiBkYXRhLW9wZW5zcG9pbGVyPSIjc3BvaWxlci1yZWdpc3RlciIgZGF0YS1jbG9zZXNwb2lsZXI9IiNzcG9pbGVyLWxvZ2luIj7QoNC10LPQuNGB0YLRgNCw0YbQuNGPPC9hPjwvZGl2PjwvZm9ybT48L2Rpdj48L2Rpdj48ZGl2IGlkPSJzcG9pbGVyLXJlZ2lzdGVyIiBjbGFzcz0idG9vZ2xlLXNwb2lsZXIgZG4iPjxkaXYgY2xhc3M9ImhlYWRlciI+PHNwYW4+0KDQtdCz0LjRgdGC0YDQsNGG0LjRjzwvc3Bhbj48YSBocmVmPSIjIiBkYXRhLWFjdGlvbj0iY2xvc2UiPjxpIGNsYXNzPSJpY29uIGljLWNsb3NlIj48L2k+PC9hPjwvZGl2PjxkaXYgY2xhc3M9ImJvZHkgYm94LXNpZGUgYm94LXRvcCI+PGRpdiBjbGFzcz0iZXJyb3JzIj7QndC10LLQvtC30LzQvtC20L3QviDQt9Cw0YDQtdCz0LjRgdGC0YDQuNGA0L7QstCw0YLRjNGB0Y8sINC/0L7Qv9GA0L7QsdGD0LnRgtC1INC10YnQtSDRgNCw0Lc8L2Rpdj48Zm9ybSBjbGFzcz0iZm9ybS1lbGVtZW50cyBmb3JtLWxvZ2luIiBpZD0iZm9ybS1yZWdpc3RlciIgZGF0YS1hY3Rpb249Ii9yZWdpc3Rlci8iIG1ldGhvZD0icG9zdCI+PGlucHV0IHR5cGU9ImhpZGRlbiIgbmFtZT0ic2x1aWQiIHZhbHVlPSIxNzZjZTI5NjE3Y2FmMDNkNGFmOWQ1MGU2M2NjOTM4MjcwMzdjMGJiMDVmOTZmNjBkOGEwYjZjYmNiOWYzNmYyIiAvPjxsYWJlbCBmb3I9InItbG9naW4iPjxzcGFuPtCk0JjQnjwvc3Bhbj48aW5wdXQgdHlwZT0idGV4dCIgbmFtZT0iZmlvIiAvPjwvbGFiZWw+PGxhYmVsIGZvcj0ici1wYXNzd29yZCI+PHNwYW4+0KLQtdC70LXRhNC+0L08L3NwYW4+PGlucHV0IG5hbWU9InBob25lIiB0eXBlPSJ0ZWwiIGNsYXNzPSJtYXNrZWQiIG1hc2s9Iis3ICg5OTkpIDk5OS05OS05OSIgbWFzay1wbGFjZWhvbGRlcj0iXyIgcGxhY2Vob2xkZXI9Iis3IChfX18pIF9fXy1fXy1fXyIgLz48L2xhYmVsPjxsYWJlbCBmb3I9InItZW1haWwiPjxzcGFuPkVtYWlsPC9zcGFuPjxpbnB1dCB0eXBlPSJ0ZXh0IiBuYW1lPSJlbWFpbCIgLz48L2xhYmVsPjxkaXYgY2xhc3M9InRjZW50ZXIiPjxhIGhyZWY9IiMiIGNsYXNzPSJidG4gYnRuLWZ3IGJ0bi1wcmltYXJ5IGJ0bi1sZyBidG4tc3VibWl0Ij7Qt9Cw0YDQtdCz0LjRgdGC0YDQuNGA0L7QstCw0YLRjNGB0Y88L2E+PGlucHV0IHR5cGU9InN1Ym1pdCIgc3R5bGU9ImRpc3BsYXk6IG5vbmU7IiAvPjxhIGhyZWY9IiMiIGNsYXNzPSJiYXNlIHJlZ2lzdGVyIiBkYXRhLW9wZW5zcG9pbGVyPSIjc3BvaWxlci1sb2dpbiIgZGF0YS1jbG9zZXNwb2lsZXI9IiNzcG9pbGVyLXJlZ2lzdGVyIj7QktC+0LnRgtC4PC9hPjwvZGl2PjwvZm9ybT48L2Rpdj48L2Rpdj4=",
            "5bc6cfeea17f17.97141606": "PGRpdiBjbGFzcz0iaGVhZGVyIj48c3Bhbj7Ql9Cw0LrQsNC30LDRgtGMINC30LLQvtC90L7Qujwvc3Bhbj48YSBocmVmPSIjIiBkYXRhLWFjdGlvbj0iY2xvc2UiPjxpIGNsYXNzPSJpY29uIGljLWNsb3NlIj48L2k+PC9hPjwvZGl2PjxkaXYgY2xhc3M9ImJvZHkgYm94LXNpZGUgYm94LXRvcCI+PGRpdiBjbGFzcz0ic3VjY2VzcyI+0JLQsNGIINC+0LHRgNCw0YLQvdGL0Lkg0LfQstC+0L3QvtC6INC30LDRgNC10LPQuNGB0YLRgNC40YDQvtCy0LDQvSE8L2Rpdj48ZGl2IGNsYXNzPSJlcnJvcnMiPtCd0LXQstC+0LfQvNC+0LbQvdC+INC30LDQutCw0LfQsNGC0Ywg0LfQstC+0L3QvtC6LCDQv9C+0L/RgNC+0LHRg9C50YLQtSDQtdGJ0LUg0YDQsNC3PC9kaXY+PGZvcm0gY2xhc3M9ImZvcm0tZWxlbWVudHMgZm9ybS1sb2dpbiIgaWQ9ImZvcm0tY2FsbGJhY2siIGRhdGEtYWN0aW9uPSIvY2FsbGJhY2svIiBtZXRob2Q9InBvc3QiPjxpbnB1dCB0eXBlPSJoaWRkZW4iIG5hbWU9InNsdWlkIiB2YWx1ZT0iMTc2Y2UyOTYxN2NhZjAzZDRhZjlkNTBlNjNjYzkzODI3MDM3YzBiYjA1Zjk2ZjYwZDhhMGI2Y2JjYjlmMzZmMiIgLz48bGFiZWwgZm9yPSJsb2dpbiI+PHNwYW4+0KLQtdC70LXRhNC+0L0g0LTQu9GPINGB0LLRj9C30Lg8L3NwYW4+PGlucHV0IHR5cGU9InRlbCIgbmFtZT0icGhvbmUiIGNsYXNzPSJtYXNrZWQiIG1hc2s9Iis3ICg5OTkpIDk5OS05OS05OSIgbWFzay1wbGFjZWhvbGRlcj0iXyIgcGxhY2Vob2xkZXI9Iis3IChfX18pIF9fXy1fXy1fXyIgdmFsdWU9IiIgLz48L2xhYmVsPjxkaXYgY2xhc3M9InRjZW50ZXIiIHN0eWxlPSJwYWRkaW5nOiAwIDAgMjVweDsiPjxhIGhyZWY9IiMiIGNsYXNzPSJidG4gYnRuLWZ3IGJ0bi1wcmltYXJ5IGJ0bi1sZyBidG4tc3VibWl0Ij7Qt9Cw0LrQsNC30LDRgtGMINC30LLQvtC90L7QujwvYT48L2Rpdj48L2Zvcm0+PC9kaXY+",
            "5bc6cfeea186f3.92085244": "PGRpdiBjbGFzcz0iaGVhZGVyIj48c3Bhbj7QktGLINC80L7QttC10YLQtSDRgdC+0L7QsdGJ0LjRgtGMINC+INC90LXRgtC+0YfQvdC+0YHRgtC4INCyINC+0L/QuNGB0LDQvdC40Lg6PC9zcGFuPjxhIGhyZWY9IiMiIGRhdGEtYWN0aW9uPSJjbG9zZSI+PGkgY2xhc3M9Imljb24gaWMtY2xvc2UiPjwvaT48L2E+PC9kaXY+PGRpdiBjbGFzcz0iYm9keSBib3gtc2lkZSBib3gtdG9wIj48ZGl2IGNsYXNzPSJzdWNjZXNzIj7QktCw0YjQtSDQvtCx0YDQsNGJ0LXQvdC40LUg0LfQsNGA0LXQs9C40YHRgtGA0LjRgNC+0LLQsNC90L4hPC9kaXY+PGRpdiBjbGFzcz0iZXJyb3JzIj7QndC10LLQvtC30LzQvtC20L3QviDQt9Cw0YDQtdCz0LjRgdGC0YDQuNGA0L7QstCw0YLRjCDQvtCx0YDQsNGJ0LXQvdC40LUsINC/0L7Qv9GA0L7QsdGD0LnRgtC1INC10YnQtSDRgNCw0Lc8L2Rpdj48Zm9ybSBjbGFzcz0iZm9ybS1lbGVtZW50cyBmb3JtLWxvZ2luIiBkYXRhLWFjdGlvbj0iL2JhZC9oZWxwZGVzay8iIG1ldGhvZD0icG9zdCIgaWQ9ImZvcm0tY29udGVudC1hYnVzZSI+PGlucHV0IHR5cGU9ImhpZGRlbiIgbmFtZT0ic2x1aWQiIHZhbHVlPSIxNzZjZTI5NjE3Y2FmMDNkNGFmOWQ1MGU2M2NjOTM4MjcwMzdjMGJiMDVmOTZmNjBkOGEwYjZjYmNiOWYzNmYyIiAvPjxsYWJlbCBmb3I9InNpdGVwYWdlIj48c3Bhbj7QkNC00YDQtdGBINGB0YLRgNCw0L3QuNGG0Ys8L3NwYW4+PGlucHV0IHR5cGU9InRleHQiIG5hbWU9InNpdGVwYWdlIiAvPjwvbGFiZWw+PGxhYmVsIGZvcj0iZXJyb3JtZXNzYWdlIj48c3Bhbj7QntGI0LjQsdC60LA8L3NwYW4+PHRleHRhcmVhIG5hbWU9ImVycm9ybWVzc2FnZSIgcGxhY2Vob2xkZXI9ItCe0L/QuNGI0LjRgtC1INC+0YjQuNCx0LrRgyI+PC90ZXh0YXJlYT48L2xhYmVsPjxsYWJlbCBmb3I9ImVycm9yY29tbWVudCI+PHNwYW4+0JrQvtC80LzQtdC90YLQsNGA0LjQuTwvc3Bhbj48dGV4dGFyZWEgbmFtZT0iZXJyb3Jjb21tZW50IiBwbGFjZWhvbGRlcj0i0JTQvtC/0L7Qu9C90LjRgtC10LvRjNC90LDRjyDQuNC90YTQvtGA0LzQsNGG0LjRjyI+PC90ZXh0YXJlYT48L2xhYmVsPjxsYWJlbCBmb3I9ImxvZ2luIj48c3Bhbj5FLW1haWw8L3NwYW4+PGlucHV0IHR5cGU9InRleHQiIG5hbWU9ImxvZ2luIiB2YWx1ZT0iIi8+PC9sYWJlbD48ZGl2IGNsYXNzPSJ0Y2VudGVyIiBzdHlsZT0icGFkZGluZzogMCAwIDI1cHg7Ij48YSBocmVmPSIjIiBjbGFzcz0iYnRuIGJ0bi1mdyBidG4tcHJpbWFyeSBidG4tbGcgYnRuLXN1Ym1pdCI+0L7RgtC/0YDQsNCy0LjRgtGMPC9hPjwvZGl2PjwvZm9ybT48L2Rpdj4=",
            "5bc6cfeea18c23.72280468": "PGRpdiBjbGFzcz0iaGVhZGVyIj48c3Bhbj7QodGA0LDQstC90LXQvdC40LU8L3NwYW4+PGEgaHJlZj0iIyIgZGF0YS1hY3Rpb249ImNsb3NlIj48aSBjbGFzcz0iaWNvbiBpYy1jbG9zZSI+PC9pPjwvYT48L2Rpdj48ZGl2IGNsYXNzPSJib2R5IGJveC1zaWRlIGJveC10b3AiPjxwPtCU0L7QsdCw0LLRjNGC0LUg0YLQvtCy0LDRgNGLINC00LvRjyDRgdGA0LDQstC90LXQvdC40Y88L3A+PGRpdiBjbGFzcz0idGNlbnRlciIgc3R5bGU9InBhZGRpbmc6IDI1cHggMCAyNXB4IDA7Ij48YSBocmVmPSIvY2F0YWxvZy8iIGNsYXNzPSJidG4gYnRuLWZ3IGJ0bi1wcmltYXJ5IGJ0bi1sZyBidG4tc3VibWl0Ij7Qv9C10YDQtdC50YLQuCDQsiDQutCw0YLQsNC70L7QszwvYT48L2Rpdj48L2Rpdj4="
        };
    </script>
    <script type="text/javascript" src="/js/jquery-3.1.1.js"></script>
    <script type="text/javascript" src="/js/underscore-min.js"></script>
    <script type="text/javascript" src="/js/jquery.dotdotdot.js"></script>
    <script type="text/javascript" src="/js/jquery.inputmask.min.js"></script>
    <script type="text/javascript" src="/js/base64.js?rnd=3897051187"></script>
    <script type="text/javascript" src="/js/main.v2.js?rnd=3897051187"></script>
    <script type="text/javascript" src="/js/main.js?rnd=3897051187"></script>
    <script type="text/javascript" src="/js/main.new.js?rnd=3897051187"></script>
    <script type="text/javascript" src="/js/lozad.js?rnd=3897051187"></script>
    <script type="text/javascript" src="/js/lk.js?rnd=3897051187"></script>
    <script type="text/javascript" src="/js/product-card-v2.js?rnd=3897051187"></script>
    <script type="text/javascript" src="/js/cart.js?rnd=3897051187"></script>
    <script type="text/javascript" src="/js/construct.js?rnd=3897051187"></script>
    <script type="text/javascript" src="/js/contacts.js?rnd=3897051187"></script>
    <script type="text/javascript" src="/js/search_results.js?rnd=3897051187"></script>
    <script type="text/javascript" src="/js/ga.helper.js?rnd=3897051187"></script>
    <script type="text/javascript" src="/js/creditline.helper.js?rnd=3897051187"></script>
    <script type="text/javascript" src="/js/criteo.helper.js?rnd=3897051187"></script>
    <script type="text/javascript" src="/js/rr.helper.js?rnd=3897051187"></script>
    <script type="text/javascript" src="/js/flocktory.helper.js?rnd=3897051187"></script>
    <script type="text/javascript" src="/js/catalogue-2.js?rnd=3897051187"></script>
    <script type="text/javascript" src="/libs/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/js/jquery.bxslider/jquery.bxslider.js"></script>
    <script type="text/javascript" src="/js/lib/nouislider.min.js"></script>
    <script type="text/javascript" src="/js/lib/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/lib/slick.min.js"></script>
    <script type="text/javascript" src="/js/lib/inputmask.js"></script>
    <script type="text/javascript" src="/js/lib/swiper.min.js"></script>
    <script type="text/javascript" src="/libs/datepicker/datepicker.min.js"></script>
    <script type="text/javascript" src="/libs/nanoscroller/jquery.nanoscroller.min.js"></script>
    <script type="text/javascript" src="/js/compare-new.js?rnd=<?php echo $_rnd; ?>"></script>
    <style type="text/css">
        @keyframes progress-bar-stripes {
            from {
                background-position: 40px 0
            }
            to {
                background-position: 0 0
            }
        }
        
        .progress {
            overflow: hidden;
            height: 20px;
            margin-bottom: 20px;
            background-color: #f5f5f5;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
            box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1)
        }
        
        .progress-bar {
            float: left;
            width: 0%;
            height: 100%;
            font-size: 12px;
            line-height: 20px;
            color: #fff;
            text-align: center;
            background-color: #337ab7;
            -webkit-box-shadow: inset 0 -1px 0 rgba(0, 0, 0, 0.15);
            box-shadow: inset 0 -1px 0 rgba(0, 0, 0, 0.15);
            -webkit-transition: width .6s ease;
            -o-transition: width .6s ease;
            transition: width .6s ease
        }
        
        .progress-striped .progress-bar,
        .progress-bar-striped {
            background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
            background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
            background-image: linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
            -webkit-background-size: 40px 40px;
            background-size: 40px 40px
        }
        
        .progress.active .progress-bar,
        .progress-bar.active {
            -webkit-animation: progress-bar-stripes 2s linear infinite;
            -o-animation: progress-bar-stripes 2s linear infinite;
            animation: progress-bar-stripes 2s linear infinite
        }
        
        .progress-bar-success {
            background-color: #5cb85c
        }
        
        .progress-striped .progress-bar-success {
            background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
            background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
            background-image: linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent)
        }
        
        .progress-bar-info {
            background-color: #5bc0de
        }
        
        .progress-striped .progress-bar-info {
            background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
            background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
            background-image: linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent)
        }
        
        .progress-bar-warning {
            background-color: #f0ad4e
        }
        
        .progress-striped .progress-bar-warning {
            background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
            background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
            background-image: linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent)
        }
        
        .progress-bar-danger {
            background-color: #d9534f
        }
        
        .progress-striped .progress-bar-danger {
            background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
            background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
            background-image: linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent)
        }
        
        .modal-open {
            overflow: hidden
        }
        
        .modal {
            display: none;
            overflow: hidden;
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 1050;
            -webkit-overflow-scrolling: touch;
            outline: 0
        }
        
        .modal.fade .modal-dialog {
            -webkit-transform: translate(0, -25%);
            -ms-transform: translate(0, -25%);
            -o-transform: translate(0, -25%);
            transform: translate(0, -25%);
            -webkit-transition: -webkit-transform 0.3s ease-out;
            -o-transition: -o-transform 0.3s ease-out;
            transition: transform 0.3s ease-out
        }
        
        .modal.in .modal-dialog {
            -webkit-transform: translate(0, 0);
            -ms-transform: translate(0, 0);
            -o-transform: translate(0, 0);
            transform: translate(0, 0)
        }
        
        .modal-open .modal {
            overflow-x: hidden;
            overflow-y: auto
        }
        
        .modal-dialog {
            position: relative;
            width: auto;
            margin: 10px
        }
        
        .modal-content {
            position: relative;
            background-color: #fff;
            border: 1px solid #999;
            border: 1px solid rgba(0, 0, 0, 0.2);
            border-radius: 6px;
            -webkit-box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
            box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
            -webkit-background-clip: padding-box;
            background-clip: padding-box;
            outline: 0
        }
        
        .modal-backdrop {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 1040;
            background-color: #000
        }
        
        .modal-backdrop.fade {
            opacity: 0;
            filter: alpha(opacity=0)
        }
        
        .modal-backdrop.in {
            opacity: .5;
            filter: alpha(opacity=50)
        }
        
        .modal-header {
            padding: 15px;
            border-bottom: 1px solid #e5e5e5
        }
        
        .modal-header .close {
            margin-top: -2px
        }
        
        .modal-title {
            margin: 0;
            line-height: 1.42857143
        }
        
        .modal-body {
            position: relative;
            padding: 15px
        }
        
        .modal-footer {
            padding: 15px;
            text-align: right;
            border-top: 1px solid #e5e5e5
        }
        
        .modal-footer .btn+.btn {
            margin-left: 5px;
            margin-bottom: 0
        }
        
        .modal-footer .btn-group .btn+.btn {
            margin-left: -1px
        }
        
        .modal-footer .btn-block+.btn-block {
            margin-left: 0
        }
        
        .modal-scrollbar-measure {
            position: absolute;
            top: -9999px;
            width: 50px;
            height: 50px;
            overflow: scroll
        }
    </style>
<a href="#" id="top-scroller"></a>    
</body>

</html>